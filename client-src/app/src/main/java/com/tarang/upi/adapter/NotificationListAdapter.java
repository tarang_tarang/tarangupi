package com.tarang.upi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tarang.upi.R;
import com.tarang.upi.pojo.NotificationPOjo;

import java.util.ArrayList;

public class NotificationListAdapter extends BaseAdapter{

	Context context;
	ArrayList<NotificationPOjo> list = new ArrayList();
	LayoutInflater inflater;
	
	public NotificationListAdapter(Context context,ArrayList<NotificationPOjo> list) {
		this.context = context;
		this.list = list;
		inflater = LayoutInflater.from(this.context);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		MyViewHolder mViewHolder;
		
		 if (convertView == null) {
	            convertView = inflater.inflate(R.layout.notificationlistview_layout, parent, false);
	            mViewHolder = new MyViewHolder(convertView);
	            convertView.setTag(mViewHolder);
	        } else {
	            mViewHolder = (MyViewHolder) convertView.getTag();
	        }
		 mViewHolder.noti_text.setText(list.get(position).getNotificationMessage());
		 
		return convertView;
	}
	}

	class MyViewHolder {
        TextView noti_text;

        public MyViewHolder(View item) {
        	noti_text = (TextView) item.findViewById(R.id.noti_text);
        	
        }
}
