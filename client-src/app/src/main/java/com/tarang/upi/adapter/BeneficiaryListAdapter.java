package com.tarang.upi.adapter;

import java.util.List;

import psp.mobile.model.response.BeneficiarySummary;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tarang.upi.R;

public class BeneficiaryListAdapter extends BaseAdapter {

	private Context context;
	private List<BeneficiarySummary> list;
	private LayoutInflater inflater;
	
	public BeneficiaryListAdapter(Context context, List<BeneficiarySummary> list) {
		this.context = context;
		this.list = list;
		inflater = LayoutInflater.from(this.context);
	}
	
	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		MyViewHolder mViewHolder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.beneficiary_listview, parent, false);
			mViewHolder = new MyViewHolder(convertView);
			convertView.setTag(mViewHolder);
		} 
		else {
			mViewHolder = (MyViewHolder) convertView.getTag();
		}
		mViewHolder.nickName.setText("Nick Name : " + list.get(position).getBenNickName());
		mViewHolder.name.setText("Name : " + list.get(position).getBenName());
		mViewHolder.virtualAddress.setText("Address : " + list.get(position).getBenVirtualAddr());
		return convertView;
	}

	private class MyViewHolder {
        TextView nickName,name,virtualAddress;
        public MyViewHolder(View item) {
        	nickName = (TextView) item.findViewById(R.id.bene_nick_name);
        	name = (TextView) item.findViewById(R.id.benef_name_);
        	virtualAddress = (TextView) item.findViewById(R.id.bene_virtual_addr);
        }
    }
	
}