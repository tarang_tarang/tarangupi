package com.tarang.upi.pojo;

/**
 * Created by tripatys on 18/3/16.
 */
public class NotificationPOjo {

    public String getNotificationtType() {
        return notificationtType;
    }

    public void setNotificationtType(String notificationtType) {
        this.notificationtType = notificationtType;
    }

    public String getNotificationMessage() {
        return notificationMessage;
    }

    public void setNotificationMessage(String notificationMessage) {
        this.notificationMessage = notificationMessage;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    String notificationtType;
    String notificationMessage;
    String notificationId;
}
