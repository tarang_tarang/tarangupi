package com.tarang.upi.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.tarang.upi.R;
import com.tarang.upi.adapter.BeneficiaryListAdapter;
import com.tarang.upi.utils.ServiceCallback;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import psp.mobile.model.request.GetBeneficiaryListRequest;
import psp.mobile.model.request.GetBeneficiaryRequest;
import psp.mobile.model.response.BeneficiarySummary;
import psp.mobile.model.response.GetBeneficiaryListResponse;
import psp.mobile.model.response.GetBeneficiaryResponse;

public class BeneficiaryGetActivity  extends UpiAbstractActivity {

	private TextView title;
	private ListView ben_list;
	private List<BeneficiarySummary> mlist;
	private BeneficiaryListAdapter adapter;
	private TextView txn_no_items;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_get_merchant_list);
		title = (TextView) findViewById(R.id.title);
		title.setText("Beneficiary List");
		ben_list = (ListView) findViewById(R.id.merchant_list);
		txn_no_items = (TextView) findViewById(R.id.txn_no_items);
		getBeneficiaryList();
		ben_list.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				getBeneficiarySummary(mlist.get(position).getBenNickName());
			}
		});
        mlist=new ArrayList<BeneficiarySummary>();
	}
	
	public void getBeneficiaryList() {
		GetBeneficiaryListRequest gbflReq = new GetBeneficiaryListRequest();
		gbflReq.setUserName(getStringFromSharedPreferences(UPIMacros.PREF_USER_NAME));
		gbflReq.setDevice(getMobileDevice());
		getWebServiceCall().triggerWebServiceCall(UPIMacros.GET_BENEFICIARY_LIST, gbflReq.toJsonString(), new ServiceCallback() {
			@Override
			public void SuccessCallbak(String resp) {
				GetBeneficiaryListResponse gbflResp = new GetBeneficiaryListResponse();
                try {
                    JSONObject jobj=new JSONObject(resp);

                    if(jobj.getInt("statusCode")==200) {
                        JSONArray jArray =jobj.getJSONArray("benefSummaries");

                        for(int i=0;i<jArray.length();i++) {
                            BeneficiarySummary pojo=new BeneficiarySummary();

                            pojo.setBenName(jArray.getJSONObject(i).getString("benName"));
                            pojo.setBenNickName(jArray.getJSONObject(i).getString("benNickName"));
                            pojo.setBenVirtualAddr(jArray.getJSONObject(i).getString("benVirtualAddr"));

                            mlist.add(pojo);
                        }

                        if (!mlist.isEmpty() || mlist.size() > 0) {
                            adapter = new BeneficiaryListAdapter(BeneficiaryGetActivity.this, mlist);
                            ben_list.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        }
                        if (mlist == null || mlist.isEmpty()) {
                            ben_list.setVisibility(View.GONE);
                            txn_no_items.setVisibility(View.VISIBLE);
                        }
                    } else if(jobj.getInt("statusCode")==50114) {
                        Toast.makeText(BeneficiaryGetActivity.this, "Session expired, please login again.", Toast.LENGTH_LONG).show();
                        Intent intent=new Intent(BeneficiaryGetActivity.this,LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    /*if (gbflResp.getStatu sCode().equals("200")) {
                        mlist = gbflResp.getBenefSummaries();
                        if (!mlist.isEmpty() || mlist.size() > 0) {
                            adapter = new BeneficiaryListAdapter(BeneficiaryGetActivity.this, mlist);
                            ben_list.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        }
                        if (mlist == null || mlist.isEmpty()) {
                            ben_list.setVisibility(View.GONE);
                            txn_no_items.setVisibility(View.VISIBLE);
                        }
                    }*/
                }catch(Exception e) {

                }
			}
			
			@Override
			public void ErrorCallbak(String resp) {
				
			}
		}, "");
	}
	
	public void getBeneficiarySummary(String nickName) {
		GetBeneficiaryRequest gbfReq = new GetBeneficiaryRequest();
		gbfReq.setUserName(getStringFromSharedPreferences(UPIMacros.PREF_USER_NAME));
		gbfReq.setNickName(nickName);
		gbfReq.setDevice(getMobileDevice());
		getWebServiceCall().triggerWebServiceCall(UPIMacros.GET_BENEFICIARY, gbfReq.toJsonString(),
			new ServiceCallback() {
				@Override
				public void SuccessCallbak(String resp) {
					GetBeneficiaryResponse res = GetBeneficiaryResponse.constructGetBeneficiaryResponse(resp);
					if(res.getStatusCode().equals("200")) {
						beneficiaryDetailPopUp(res);
					}if (resp != null) {

                    } else if(res.getStatusCode().equals("50114")) {
                        Toast.makeText(BeneficiaryGetActivity.this, "Session expired, please login again.", Toast.LENGTH_LONG).show();
                        Intent intent=new Intent(BeneficiaryGetActivity.this,LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
				}
				@Override
				public void ErrorCallbak(String resp) {
				}
			}, "");
	}
	
	public void beneficiaryDetailPopUp(final GetBeneficiaryResponse res) {
		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setContentView(R.layout.merchant_details);

		TextView title_merchant=(TextView) dialog.findViewById(R.id.title_merchant);
		TextView txtname = (TextView) dialog.findViewById(R.id.merchant_name);
		TextView merchant_nick_name = (TextView) dialog.findViewById(R.id.merchant_nick_name);
		TextView merchant_virtual_address = (TextView) dialog.findViewById(R.id.merchant_virtual_address);
		TextView merchant_desc = (TextView) dialog.findViewById(R.id.merchant_desc);
		TextView bene_accc_number = (TextView) dialog.findViewById(R.id.bene_accc_number);
		TextView bene_ifsc = (TextView) dialog.findViewById(R.id.bene_ifsc);
		
		LinearLayout ll_acc = (LinearLayout) dialog.findViewById(R.id.ll_acc_number);
		LinearLayout ll_ifsc = (LinearLayout) dialog.findViewById(R.id.ll_ifsc);
		LinearLayout ll_virtual_address = (LinearLayout) dialog.findViewById(R.id.ll_virtual_address);
		ll_acc.setVisibility(View.VISIBLE);
		ll_ifsc.setVisibility(View.VISIBLE);
		
		Button cancel=(Button) dialog.findViewById(R.id.merchant_cancel);
		Button pay=(Button) dialog.findViewById(R.id.merchant_Pay);
		Window window = dialog.getWindow();
		WindowManager.LayoutParams wlp = window.getAttributes();
		wlp.gravity = Gravity.CENTER;

		txtname.setText(res.getName());
		merchant_nick_name.setText(res.getNickName());
		merchant_virtual_address.setText(res.getAddress());
		merchant_desc.setText(res.getDescription());
		bene_ifsc.setText(res.getIfsc());
		
		bene_accc_number.setText(res.getMaskedNumber());
		title_merchant.setText("Beneficiary Detail");
		
		if( res.getIfsc().equals("null") ){
			ll_acc.setVisibility(View.GONE);
			ll_ifsc.setVisibility(View.GONE);
		}
		else {
			ll_virtual_address.setVisibility(View.GONE);
		}
		cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		pay.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(BeneficiaryGetActivity.this, PaymentActivity.class);
				intent.putExtra("call_from", "beneficiary");
				intent.putExtra("name", res.getName());
				intent.putExtra("nick", res.getNickName());
				startActivity(intent);
				finish();
			}
		});
		dialog.show();
	}
	
}