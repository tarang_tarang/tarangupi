package com.tarang.upi.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tarang.upi.R;
import com.tarang.upi.utils.CommonUtilsUpi;
import com.tarang.upi.utils.ServiceCallback;
import com.tarang.upi.utils.WebServiceCall;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.npci.upi.security.pinactivitycomponent.PinActivityComponent;

import java.util.HashMap;

public class PaymentActivity extends Activity implements OnClickListener {

	TextView title,payer_vir_add,payee_nick_name,payee_virtual_address,name_type;
	String location;
	CommonUtilsUpi comUtils;
	WebServiceCall services;
	EditText amount,transaction_pin,note;
	Button pay;
	Bundle extra;
	String  thirdPartyVirtualAddr="";
	String server_type="",server_nickName="";
	String payCollectUrl="";
    String dataFromPinActivity="";
    String txnId="",notificationId="";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_payment);
		title = (TextView) findViewById(R.id.title);
		pay = (Button) findViewById(R.id.pay);
		amount = (EditText) findViewById(R.id.amount);
		note = (EditText) findViewById(R.id.note);

		transaction_pin=(EditText) findViewById(R.id.transaction_pin);
		payer_vir_add=(TextView) findViewById(R.id.payer_vir_add);
		payee_nick_name=(TextView) findViewById(R.id.payee_nick_name);
		payee_virtual_address=(TextView) findViewById(R.id.payee_virtual_address);
		name_type=(TextView) findViewById(R.id.name_type);
		pay.setOnClickListener(this);
		title.setText("Payment");
		payCollectUrl=UPIMacros.PAYMENT;
        transaction_pin.setVisibility(View.GONE);

        txnId=constructMessageId();
        fetchingDataFromIntent();
	}

	@Override
	protected void onStart() {
		super.onStart();
		location = getCommonUtils().getAddress(PaymentActivity.this);
	}

    public void fetchingDataFromIntent() {

        try {
            extra = getIntent().getExtras();
            if (extra.getString("call_from").equalsIgnoreCase("merchant")){

                payer_vir_add.setText("Payer virtual add : "+UPIMacros.PAYER_VIRTUAL_ADDRESS);
                payee_nick_name.setVisibility(View.VISIBLE);
                name_type.setVisibility(View.GONE);
                payee_virtual_address.setText("Payee virtual add : "+extra.getString("payee_virtual_add"));
                payee_nick_name.setText("Nick name : "+extra.getString("nick"));
                thirdPartyVirtualAddr=extra.getString("virtual_address");
                server_type="MERCHANT";
                return;
            } else if(extra.getString("call_from").equalsIgnoreCase("AccountNoteMobile")) {
                payer_vir_add.setText("Payer virtual add : "+UPIMacros.PAYER_VIRTUAL_ADDRESS);
                payee_nick_name.setText("Nick name : "+extra.getString("nick"));
                name_type.setVisibility(View.VISIBLE);
                name_type.setText("Mobile Number : "+UPIMacros.MOBILE_ADHAAR);
                server_type="MOBILE";
                server_nickName=extra.getString("nick");
                return;
            } else if(extra.getString("call_from").equalsIgnoreCase("AccountNoteVirtual")) {
                payer_vir_add.setText("Payer virtual add : "+UPIMacros.PAYER_VIRTUAL_ADDRESS);
                payee_nick_name.setText("Nick name : "+extra.getString("nick"));
                payee_virtual_address.setVisibility(View.VISIBLE);
                payee_virtual_address.setText("Payee virtual add : "+extra.getString("payee_virtual_add"));
                thirdPartyVirtualAddr=extra.getString("payee_virtual_add");
                server_type="VIRTUAL_ADDRESS";
                return;

            } else if(extra.getString("call_from").equalsIgnoreCase("CollectPayToAccountNote")) {
                payer_vir_add.setText("Payer virtual add : "+extra.getString("payee_virtual_add"));
                payee_nick_name.setText("Nick name : "+extra.getString("nick"));
                payee_virtual_address.setVisibility(View.VISIBLE);
                payee_virtual_address.setText("Payee virtual add : "+UPIMacros.PAYER_VIRTUAL_ADDRESS);
                thirdPartyVirtualAddr=extra.getString("payee_virtual_add");
                server_type="VIRTUAL_ADDRESS";
                title.setText("Collect Payment");
                pay.setText("Collect");
                payCollectUrl=UPIMacros.COLLECT_PAYMRNT;
                transaction_pin.setHint("Duration in days");
                return;

            } else if(extra.getString("call_from").equalsIgnoreCase("AccountNoteAdhaar")) {
                payer_vir_add.setText("Payer virtual add : "+UPIMacros.PAYER_VIRTUAL_ADDRESS);
                payee_nick_name.setText("Nick name : "+extra.getString("nick"));
                server_type="AADHAAR";
                name_type.setVisibility(View.VISIBLE);
                name_type.setText("Adhaar Number : "+UPIMacros.MOBILE_ADHAAR);
                server_nickName=extra.getString("nick");
                return;
            } else if (extra.getString("call_from").equalsIgnoreCase("beneficiary")) {
                payer_vir_add.setText("Payer virtual add : "+UPIMacros.PAYER_VIRTUAL_ADDRESS);
                payee_nick_name.setVisibility(View.VISIBLE);
                name_type.setVisibility(View.VISIBLE);
                payee_virtual_address.setText("Payee virtual add : "+extra.getString("payee_virtual_add"));
                payee_nick_name.setText("Nick name : "+extra.getString("nick"));
                name_type.setText("Name : "+extra.getString("name"));
                server_type="BENEFICIARY";
                server_nickName=extra.getString("nick");
                return;
            } else if (extra.getString("call_from").equalsIgnoreCase("CollectPaymentFromBeneficiaryList")) {
                payer_vir_add.setText("Payer virtual add : "+UPIMacros.PAYER_VIRTUAL_ADDRESS);
                payee_nick_name.setVisibility(View.VISIBLE);
                name_type.setVisibility(View.VISIBLE);
                payee_virtual_address.setText("Payee virtual add : "+extra.getString("payee_virtual_add"));
                payee_nick_name.setText("Nick name : "+extra.getString("nick"));
                name_type.setText("Name : "+extra.getString("name"));
                server_type="BENEFICIARY";
                server_nickName=extra.getString("nick");
                title.setText("Collect Payment");
                pay.setText("Collect");
                return;
            } else if (extra.getString("call_from").equalsIgnoreCase("Notitficatoin_Collect")) {

                payer_vir_add.setText("Payer virtual add : "+extra.getString("payeerrrrr_virtual_add"));
                payee_nick_name.setText("Payee name : "+extra.getString("nick"));
                payee_virtual_address.setVisibility(View.VISIBLE);
                payee_virtual_address.setText("Payee virtual add : "+extra.getString("payee_virtual_add"));
                thirdPartyVirtualAddr=extra.getString("payee_virtual_add");
                server_type="VIRTUAL_ADDRESS";
                notificationId=extra.getString("notificationId");
                amount.setText("Amount : "+extra.getString("noti_amount"));
                amount.setEnabled(false);
                payCollectUrl=UPIMacros.COLLECT_PAYMENT_CONFIRM_NOTIFICATION;
                transaction_pin.setVisibility(View.GONE);
                return;
            }
        } catch (Exception e) {

        }
    }
	private CommonUtilsUpi getCommonUtils() {
		if (comUtils == null) {
			comUtils = new CommonUtilsUpi();
		}
		return comUtils;
	}

	private WebServiceCall getWebServiceCall() {
		if (services == null) {
			services = new WebServiceCall(this);
		}
		return services;
	}

	public void paymentServerHit(String url) {

		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		String moblNum = prefs.getString(UPIMacros.PREF_MOBILE_NUM, null);
		String userName = prefs.getString(UPIMacros.PREF_USER_NAME, null);

		JSONObject jobj = new JSONObject();

		try {
			jobj.put("userName", userName);
			jobj.put("type", server_type);
			jobj.put("nickName", server_nickName);
			jobj.put("amount", amount.getText().toString()+".0");
			jobj.put("selfVirtualAddr", UPIMacros.PAYER_VIRTUAL_ADDRESS);
			jobj.put("thirdPartyVirtualAddr", thirdPartyVirtualAddr);
			jobj.put("txnNote", note.getText().toString());

			JSONObject DObj = CommonUtilsUpi.DeviceInfo(moblNum, 
					location, CommonUtilsUpi.getDeviceIPAddress(true),
					UPIMacros.TYPE,
					CommonUtilsUpi.getDeviceID(PaymentActivity.this),
					CommonUtilsUpi.getOS(), UPIMacros.APP_NAME,
					UPIMacros.CAPABILITY);

			JSONObject mobileCredentials = new JSONObject();
			mobileCredentials.put("dataValue", dataFromPinActivity);

			jobj.put("mobileCredentials", mobileCredentials);
			jobj.put("device", DObj);
            jobj.put("txnId", txnId);
			
			if(url.equalsIgnoreCase(UPIMacros.COLLECT_PAYMRNT)) {
				jobj.put("durationInDays", transaction_pin.getText().toString().trim());
			}

			System.out.println("Request ---"+jobj.toString());
			System.out.println("URL ---"+url);
			getWebServiceCall().triggerWebServiceCall(url,
					jobj.toString(), new ServiceCallback() {

						@Override
						public void SuccessCallbak(String resp) {
							System.out.println("Response ---"+resp);
							try {
								JSONObject jobj=new JSONObject(resp);
								if(jobj.getInt("statusCode")==50126 || jobj.getInt("statusCode")==50134) {
									Toast.makeText(PaymentActivity.this, "Payment Success initiated.",
											Toast.LENGTH_LONG).show();
									Intent intent=new Intent(PaymentActivity.this,HomeActivity.class);
									startActivity(intent);
									finish();
									
								}else if(jobj.getInt("statusCode")==50114) {
                                    Toast.makeText(PaymentActivity.this, "Session expired, please login again.", Toast.LENGTH_LONG).show();
                                    Intent intent=new Intent(PaymentActivity.this,LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                                else {
									Toast.makeText(PaymentActivity.this, "Internal server error", Toast.LENGTH_LONG).show();
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
							
						}

						@Override
						public void ErrorCallbak(String resp) {

						}
					}, "");
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

    public void paymentServerHitCollectNotification(String url) {

        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        String moblNum = prefs.getString(UPIMacros.PREF_MOBILE_NUM, null);
        String userName = prefs.getString(UPIMacros.PREF_USER_NAME, null);

        JSONObject jobj = new JSONObject();
        try {
            jobj.put("userName", userName);

            JSONObject DObj = CommonUtilsUpi.DeviceInfo(moblNum,
                    location, CommonUtilsUpi.getDeviceIPAddress(true),
                    UPIMacros.TYPE,
                    CommonUtilsUpi.getDeviceID(PaymentActivity.this),
                    CommonUtilsUpi.getOS(), UPIMacros.APP_NAME,
                    UPIMacros.CAPABILITY);

            JSONObject mobileCredentials = new JSONObject();
            mobileCredentials.put("dataValue", dataFromPinActivity);

            jobj.put("mobileCredentials", mobileCredentials);
            jobj.put("device", DObj);
            jobj.put("notificationId", notificationId);

            if(url.equalsIgnoreCase(UPIMacros.COLLECT_PAYMRNT)) {
                jobj.put("durationInDays", transaction_pin.getText().toString().trim());
            }

            System.out.println("Request ---"+jobj.toString());
            System.out.println("URL ---"+url);
            getWebServiceCall().triggerWebServiceCall(url,
                    jobj.toString(), new ServiceCallback() {

                        @Override
                        public void SuccessCallbak(String resp) {
                            System.out.println("Response ---"+resp);
                            try {
                                JSONObject jobj=new JSONObject(resp);
                                if(jobj.getInt("statusCode")==200) {
                                    Toast.makeText(PaymentActivity.this, "Payment Success.",
                                            Toast.LENGTH_LONG).show();
                                    Intent intent=new Intent(PaymentActivity.this,HomeActivity.class);
                                    startActivity(intent);
                                    finish();

                                }else if(jobj.getInt("statusCode")==50114) {
                                    Toast.makeText(PaymentActivity.this, "Session expired, please login again.", Toast.LENGTH_LONG).show();
                                    Intent intent=new Intent(PaymentActivity.this,LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                                else {
                                    Toast.makeText(PaymentActivity.this, "Internal server error", Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void ErrorCallbak(String resp) {

                        }
                    }, "");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.pay:

			if (amount.getText().toString().isEmpty()) {
				amount.requestFocus();
				Toast.makeText(PaymentActivity.this, "Please enter amount",
						Toast.LENGTH_LONG).show();
			} /*else if (transaction_pin.getText().toString().isEmpty()) {
				transaction_pin.requestFocus();
				if(transaction_pin.getHint().toString().equalsIgnoreCase("Duration in days")) {
					
				}else {
				Toast.makeText(PaymentActivity.this, "Please enter pin",
						Toast.LENGTH_LONG).show();
				}
				
			}*/ else if(note.getText().toString().isEmpty()) {
				Toast.makeText(PaymentActivity.this, "Please enter some note",
						Toast.LENGTH_LONG).show();
			} else {
				PaymentDetailPopUp();
			}
			break;

		default:
			break;
		}
	}

	public void PaymentDetailPopUp() {

		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setContentView(R.layout.payment_popup);
		
		Button cancel=(Button) dialog.findViewById(R.id.merchant_cancel);
		Button pay=(Button) dialog.findViewById(R.id.merchant_Pay);
		Window window = dialog.getWindow();
		WindowManager.LayoutParams wlp = window.getAttributes();
		wlp.gravity = Gravity.CENTER;

		TextView payer_vir_add1,payee_nick_name1,amount_pay1,not_pay1;
		payer_vir_add1=(TextView) dialog.findViewById(R.id.payer_vir_add1);
		payee_nick_name1=(TextView) dialog.findViewById(R.id.payee_nick_name1);
		amount_pay1=(TextView) dialog.findViewById(R.id.amount_pay1);
		not_pay1=(TextView) dialog.findViewById(R.id.not_pay1);
		
		payer_vir_add1.setText(payer_vir_add.getText().toString());
		payee_nick_name1.setText(payee_nick_name.getText().toString());
		amount_pay1.setText("Amount : "+amount.getText().toString());
		not_pay1.setText("Note : "+note.getText().toString());
		
		try {
			if(extra.getString("call_from").equalsIgnoreCase("CollectPayToAccountNote") ||
					extra.getString("call_from").equalsIgnoreCase("CollectPaymentFromBeneficiaryList")) {
				pay.setText("Collect");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		pay.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//paymentServerHit(payCollectUrl);
                if(extra.getString("call_from").equalsIgnoreCase("CollectPayToAccountNote") ||
                        extra.getString("call_from").equalsIgnoreCase("CollectPaymentFromBeneficiaryList")) {
                   // pay.setText("Collect");
                   paymentServerHit(payCollectUrl);
                } else if (extra.getString("call_from").equalsIgnoreCase("Notitficatoin_Collect")) {
                    txnId=extra.getString("txnId");
                    getCollectPaymentCredential(extra.getString("txnId"));
                    dialog.dismiss();
                }
                else {
                    getPinCredential();
                    dialog.dismiss();
                }
			}
		});
		cancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.show();
	
	}

    public void getPinCredential() {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        String xmlPayloadString = prefs.getString(UPIMacros.PUBLIC_KEY, null);

        String credAllowedString = "{\n" + "\t\"CredAllowed\": [{\n"
                + "\t\t\"type\": \"PIN\",\n" + "\t\t\"subtype\": \"MPIN\",\n"
                + "\t\t\"dType\": \"ALPH | NUM\",\n" + "\t\t\"dLength\": 6\n" +
                "\t}]\n" + "}";

        Intent intent = new Intent(PaymentActivity.this,
                PinActivityComponent.class);

        // Create Keycode
        intent.putExtra("keyCode", "NPCI");

        // Create xml payload
        if (xmlPayloadString.isEmpty()) {
            Toast.makeText(PaymentActivity.this,
                    "XML List Key API is not loaded.",
                    Toast.LENGTH_LONG).show();
            return;
        }
        intent.putExtra("keyXmlPayload", xmlPayloadString);

        // Create Controls
        if (credAllowedString.isEmpty()) {
            Toast.makeText(PaymentActivity.this,
                    "Required Credentials could not be loaded.",
                    Toast.LENGTH_LONG).show();
            return;
        }
        intent.putExtra("controls", credAllowedString);

        // Create Configuration
        try {
            JSONObject configuration = new JSONObject();
            configuration.put("payerBankName", "Tarang Software Pvt Ltd.");
            configuration.put("backgroundColor", "#FFFFFF");
            Log.i("configuration", configuration.toString());
            intent.putExtra("configuration", configuration.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Create Salt
        try {
            JSONObject salt = new JSONObject();
            salt.put("txnId", txnId);
            salt.put("txnAmount", amount.getText().toString()+"0");
            salt.put("deviceId", CommonUtilsUpi.getDeviceID(PaymentActivity.this));
            salt.put("appId", UPIMacros.APP_ID);
            Log.i("salt", salt.toString());
            intent.putExtra("salt", salt.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Create Pay Info
        JSONArray payInfoArray = new JSONArray();
        try {
			JSONObject jsonPayeeName = new JSONObject();
			jsonPayeeName.put("name", "payeeName");
			jsonPayeeName.put("value", extra.getString("payee_virtual_add"));
			payInfoArray.put(jsonPayeeName);

			JSONObject jsonNote = new JSONObject();
			jsonNote.put("name", "note");
			jsonNote.put("value", "Pay restaurant bill");
			payInfoArray.put(jsonNote);

			/*JSONObject jsonRefId = new JSONObject();
			jsonRefId.put("name", "refId");
			jsonRefId.put("value", "589430957393");
			payInfoArray.put(jsonRefId);

			JSONObject jsonRefUrl = new JSONObject();
			jsonRefUrl.put("name", "refUrl");
			jsonRefUrl.put("value", "https://indianbank.com");
			payInfoArray.put(jsonRefUrl);

			JSONObject jsonAccount = new JSONObject();
			jsonAccount.put("name", "account");
			jsonAccount.put("value", "122XXX423");
			payInfoArray.put(jsonAccount);*/

            Log.i("payInfo", payInfoArray.toString());
            intent.putExtra("payInfo", payInfoArray.toString());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        // Create Language Pref
        intent.putExtra("languagePref", "en_US");
        startActivityForResult(intent, 1);
    }

    public void getCollectPaymentCredential(String coll_txnId) {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        String xmlPayloadString = prefs.getString(UPIMacros.PUBLIC_KEY, null);

        String credAllowedString = "{\n" + "\t\"CredAllowed\": [{\n"
                + "\t\t\"type\": \"OTP\",\n" + "\t\t\"subtype\": \"OTP\",\n"
                + "\t\t\"dType\": \"ALPH | NUM\",\n" + "\t\t\"dLength\": 6\n" +
                "\t}]\n" + "}";

        Intent intent = new Intent(PaymentActivity.this,
                PinActivityComponent.class);

        // Create Keycode
        intent.putExtra("keyCode", "NPCI");

        // Create xml payload
        if (xmlPayloadString.isEmpty()) {
            Toast.makeText(PaymentActivity.this,
                    "XML List Key API is not loaded.",
                    Toast.LENGTH_LONG).show();
            return;
        }
        intent.putExtra("keyXmlPayload", xmlPayloadString); // It will

        // Create Controls
        if (credAllowedString.isEmpty()) {
            Toast.makeText(PaymentActivity.this,
                    "Required Credentials could not be loaded.",
                    Toast.LENGTH_LONG).show();
            return;
        }
        intent.putExtra("controls", credAllowedString);

        // Create Configuration
        try {
            JSONObject configuration = new JSONObject();
            configuration.put("payerBankName", "Tarang Software Pvt Ltd.");
            configuration.put("backgroundColor", "#FFFFFF");
            Log.i("configuration", configuration.toString());
            intent.putExtra("configuration", configuration.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Create Salt
        try {
            JSONObject salt = new JSONObject();
            salt.put("txnId", coll_txnId);
            salt.put("txnAmount", amount.getText().toString()+"0");
            salt.put("deviceId", CommonUtilsUpi.getDeviceID(PaymentActivity.this));
            salt.put("appId", UPIMacros.APP_ID);
            Log.i("salt", salt.toString());
            intent.putExtra("salt", salt.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Create Pay Info
        JSONArray payInfoArray = new JSONArray();
        try {
            JSONObject jsonPayeeName = new JSONObject();
            jsonPayeeName.put("name", "payeeName");
            jsonPayeeName.put("value", extra.getString("payee_virtual_add"));
            payInfoArray.put(jsonPayeeName);

            JSONObject jsonNote = new JSONObject();
            jsonNote.put("name", "note");
            jsonNote.put("value", "Pay restaurant bill");
            payInfoArray.put(jsonNote);

			/*JSONObject jsonRefId = new JSONObject();
			jsonRefId.put("name", "refId");
			jsonRefId.put("value", "589430957393");
			payInfoArray.put(jsonRefId);

			JSONObject jsonRefUrl = new JSONObject();
			jsonRefUrl.put("name", "refUrl");
			jsonRefUrl.put("value", "https://indianbank.com");
			payInfoArray.put(jsonRefUrl);

			JSONObject jsonAccount = new JSONObject();
			jsonAccount.put("name", "account");
			jsonAccount.put("value", "122XXX423");
			payInfoArray.put(jsonAccount);*/

            Log.i("payInfo", payInfoArray.toString());
            intent.putExtra("payInfo", payInfoArray.toString());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        // Create Language Pref
        intent.putExtra("languagePref", "en_US");
        startActivityForResult(intent, 3);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("Result Code:", String.valueOf(resultCode));
        if (requestCode == 1 && resultCode == Activity.RESULT_OK
                && data != null) {
            String errorMsgStr = data.getStringExtra("error");
            if (errorMsgStr != null && !errorMsgStr.isEmpty()) {
                Log.d("Error:", errorMsgStr);
                try {
                    JSONObject error = new JSONObject(errorMsgStr);
                    String errorCode = error.getString("errorCode");
                    String errorText = error.getString("errorText");
                    Toast.makeText(PaymentActivity.this,
                            errorCode + ":" + errorText, Toast.LENGTH_LONG)
                            .show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return;
            }
            HashMap<String, String> credListHashMap = (HashMap<String, String>) data
                    .getSerializableExtra("credBlocks");
            for (String cred : credListHashMap.keySet()) { // This will return
                try {
                    JSONObject credBlock = new JSONObject(
                            credListHashMap.get(cred));
                    dataFromPinActivity=credBlock.getJSONObject("data").getString("encryptedBase64String");
                    Log.i("enc_msg", credBlock.toString());
                    Log.i("enc_msg",dataFromPinActivity);
                    // Log.i("enc_msg", credBlock.getString("message"));

                    paymentServerHit(payCollectUrl);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        } else  if (requestCode == 3 && resultCode == Activity.RESULT_OK
                && data != null) {
            String errorMsgStr = data.getStringExtra("error");
            if (errorMsgStr != null && !errorMsgStr.isEmpty()) {
                Log.d("Error:", errorMsgStr);
                try {
                    JSONObject error = new JSONObject(errorMsgStr);
                    String errorCode = error.getString("errorCode");
                    String errorText = error.getString("errorText");
                    Toast.makeText(PaymentActivity.this,
                            errorCode + ":" + errorText, Toast.LENGTH_LONG)
                            .show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return;
            }
            HashMap<String, String> credListHashMap = (HashMap<String, String>) data
                    .getSerializableExtra("credBlocks");
            for (String cred : credListHashMap.keySet()) { // This will return
                try {
                    JSONObject credBlock = new JSONObject(
                            credListHashMap.get(cred));
                    dataFromPinActivity=credBlock.getJSONObject("data").getString("encryptedBase64String");
                    Log.i("enc_msg", credBlock.toString());
                    Log.i("enc_msg",dataFromPinActivity);

                    dataFromPinActivity=credBlock.getJSONObject("data").getString("encryptedBase64String");
                    Log.i("enc_msg", credBlock.toString());
                    Log.i("enc_msg",dataFromPinActivity);
                    // Log.i("enc_msg", credBlock.getString("message"));
                    paymentServerHitCollectNotification(UPIMacros.COLLECT_PAYMENT_CONFIRM_NOTIFICATION);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public static String constructMessageId(){
        return "TARANG" + String.valueOf(System.currentTimeMillis());
    }
}
