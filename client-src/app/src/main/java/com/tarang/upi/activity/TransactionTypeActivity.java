package com.tarang.upi.activity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.tarang.upi.R.id;
import com.tarang.upi.R.layout;

public class TransactionTypeActivity extends Activity implements OnClickListener{

	TextView title,merchant,mobile,adhaar;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(layout.activity_transaction_type);
		
		title = (TextView) findViewById(id.title);
		merchant = (TextView) findViewById(id.merchant);
		mobile = (TextView) findViewById(id.mobile);
		adhaar = (TextView) findViewById(id.adhaar);
		merchant.setOnClickListener(this);
		mobile.setOnClickListener(this);
		adhaar.setOnClickListener(this);
		findViewById(id.virtual_address).setOnClickListener(this);
		findViewById(id.entity).setOnClickListener(this);
		title.setText("Transaction Type");
		
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case id.mobile:
			Bundle extra=getIntent().getExtras();
			/*Intent lIntent = new Intent(this, MobileDetailForAccountActivity.class);
			lIntent.putExtra("acc_detail", extra.getString("acc_detail"));
			startActivity(lIntent);*/
            Toast.makeText(TransactionTypeActivity.this,"Under construction, please go with virtual address.",Toast.LENGTH_LONG).show();
			break;
			
		case id.merchant:
			/*Intent lIntent2 = new Intent(this, MerchantListActivity.class);
			startActivity(lIntent2);*/
            Toast.makeText(TransactionTypeActivity.this,"Under construction, please go with virtual address.",Toast.LENGTH_LONG).show();
			break;

		case id.entity:
			/*Intent lIntent3 = new Intent(this, BeneficiaryGetActivity.class);
			startActivity(lIntent3);*/
            Toast.makeText(TransactionTypeActivity.this,"Under construction, please go with virtual address.",Toast.LENGTH_LONG).show();
			break;

		case id.adhaar:
			/*Intent lIntent1 = new Intent(this, AdhaarDetailForAccNoteActivity.class);
			startActivity(lIntent1);*/
            Toast.makeText(TransactionTypeActivity.this,"Under construction, please go with virtual address.",Toast.LENGTH_LONG).show();
			break;
			
		case id.virtual_address:
			Intent lIntent4 = new Intent(this, AccountNoteActivity.class);
			lIntent4.putExtra("call_from","virtual_address");
			startActivity(lIntent4);
			break;
		default:
			break;
		}
	}
	
}
