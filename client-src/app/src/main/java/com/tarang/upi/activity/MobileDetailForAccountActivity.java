package com.tarang.upi.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.tarang.upi.R;
import com.tarang.upi.pojo.AccountDetail;
import com.tarang.upi.utils.CommonUtilsUpi;
import com.tarang.upi.utils.ServiceCallback;
import com.tarang.upi.utils.WebServiceCall;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MobileDetailForAccountActivity extends Activity implements OnClickListener {

	EditText mobile, mmid;
	TextView title;
	CommonUtilsUpi comUtils;
	WebServiceCall services;
	String location;
	LinearLayout ll_mobile_mmid;
	
	ArrayList<AccountDetail> bankAccountList;
	ListView bankAccountListView;
	private BankAccountsAdapter bankAccAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mobile_mmid_details);

		findViewById(R.id.submit).setOnClickListener(this);
		mmid = (EditText) findViewById(R.id.mmid);
		title = (TextView) findViewById(R.id.title);
		mobile = (EditText) findViewById(R.id.mobile_number_acc);
		title.setText("Mobile Details");
		ll_mobile_mmid=(LinearLayout) findViewById(R.id.ll_mobile_mmid);
		bankAccountListView = (ListView) findViewById(R.id.bank_accounts_list);
		
		int maxLength = 10;    
		mobile.setFilters(new InputFilter[] {new InputFilter.LengthFilter(maxLength)});
		
		bankAccountList = new ArrayList<AccountDetail>();
		
		bankAccountListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent lIntent4 = new Intent(MobileDetailForAccountActivity.this, AccountNoteActivity.class);
				lIntent4.putExtra("call_from","MobileDetailForAccount");
				lIntent4.putExtra("id",bankAccountList.get(position).getId());
				lIntent4.putExtra("name",bankAccountList.get(position).getName());
				lIntent4.putExtra("acc_no",bankAccountList.get(position).getAccountNo());
				lIntent4.putExtra("ifsc",bankAccountList.get(position).getIFSCCode());
				startActivity(lIntent4);
				finish();
			}
		});
	}

	private WebServiceCall getWebServiceCall() {
		if (services == null) {
			services = new WebServiceCall(this);
		}
		return services;
	}

	@Override
	protected void onStart() {
		super.onStart();
		location = getCommonUtils().getAddress(MobileDetailForAccountActivity.this);
	}

	private CommonUtilsUpi getCommonUtils() {
		if (comUtils == null) {
			comUtils = new CommonUtilsUpi();
		}
		return comUtils;
	}
	
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.submit:

			if (mobile.getText().toString().isEmpty()) {
				mobile.requestFocus();
				Toast.makeText(this, "Please enter mobile number.",
						Toast.LENGTH_LONG).show();
			} else if (mobile.getText().toString().length() < 10) {
				mobile.requestFocus();
				Toast.makeText(this, "Please enter valid mobile number.",
						Toast.LENGTH_LONG).show();
			}  else {
				UPIMacros.MOBILE_ADHAAR=mobile.getText().toString();
				getAccList();
			}
			break;

		default:
			break;
		}
	}
	
	public void getAccList() {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		String moblNum = prefs.getString(UPIMacros.PREF_MOBILE_NUM, null);
		String userName = prefs.getString(UPIMacros.PREF_USER_NAME, null);
		
		JSONObject jobj=new JSONObject();
		
		try {
			jobj.put("isBenAccDetails", "true");
			jobj.put("userName", userName);
			
			JSONObject accJson=new JSONObject();
			accJson.put("mobnum", mobile.getText().toString().trim());
			accJson.put("mmid", mmid.getText().toString().trim());
			
			jobj.put("acDetails", accJson);
			
			JSONObject DObj = CommonUtilsUpi.DeviceInfo(moblNum, 
					location, CommonUtilsUpi.getDeviceIPAddress(true),
					UPIMacros.TYPE,
					CommonUtilsUpi.getDeviceID(MobileDetailForAccountActivity.this),
					CommonUtilsUpi.getOS(), UPIMacros.APP_NAME,
					UPIMacros.CAPABILITY);

			jobj.put("device", DObj);
			
			System.out.println("Url--- "+UPIMacros.ACC_LIST);
			System.out.println("Request--- "+jobj.toString());
			getWebServiceCall().triggerWebServiceCall(UPIMacros.ACC_LIST, jobj.toString(), new ServiceCallback() {
				
				@Override
				public void SuccessCallbak(String resp) {
					System.out.println("Response--- "+resp.toString());
					JSONObject respJson;
					try {
						respJson = new JSONObject(resp);
						if(respJson.getInt("statusCode")==200) {
							ll_mobile_mmid.setVisibility(View.GONE);
							bankAccountListView.setVisibility(View.VISIBLE);
							bankAccountList = parseAcctOtpResp(resp);
							populateAccountLists(bankAccountList);
							}else if(respJson.getInt("statusCode")==50114) {
                            Toast.makeText(MobileDetailForAccountActivity.this, "Session expired, please login again.", Toast.LENGTH_LONG).show();
                            Intent intent=new Intent(MobileDetailForAccountActivity.this,LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }
					} catch (JSONException e) {
						e.printStackTrace();
					}
					
				}
				
				@Override
				public void ErrorCallbak(String resp) {
					
				}
			}, "");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
	
	public ArrayList<AccountDetail> parseAcctOtpResp(String resp) {
		ArrayList<AccountDetail> accList = new ArrayList<AccountDetail>();
		try {
			JSONObject jobj = new JSONObject(resp);
			JSONArray jArray = jobj.getJSONArray("accountSummaryList");
			for (int i = 0; i < jArray.length(); i++) {

				AccountDetail accDetail = new AccountDetail();
				JSONObject arrObj = jArray.getJSONObject(i);
				accDetail.setAccountNo(arrObj.getString("maskedAccnumber"));
				accDetail.setAeba(arrObj.getString("aeba"));
				accDetail.setBankType("BankType");
				accDetail.setId(arrObj.getInt("id"));
				accDetail.setIFSCCode(arrObj.getString("ifsc"));
				accDetail.setName(arrObj.getString("name"));
				accList.add(accDetail);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return accList;
	}
	
	private void populateAccountLists(ArrayList<AccountDetail> accountList) {
		bankAccAdapter = new BankAccountsAdapter(this,
				R.layout.bank_accounts_adapter, accountList);
		bankAccountListView.setAdapter(bankAccAdapter);
		bankAccAdapter.notifyDataSetChanged();
	}
	
	public class BankAccountsAdapter extends BaseAdapter {

		public BankAccountsAdapter(Context context, int resource,
				List<AccountDetail> list) {

		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) MobileDetailForAccountActivity.this
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (convertView == null)
				convertView = inflater.inflate(R.layout.bank_accounts_adapter,
						parent, false);

			AccountDetail accountDetail = bankAccountList.get(position);
			TextView accountType = (TextView) convertView
					.findViewById(R.id.account_type);
			TextView accountNum = (TextView) convertView
					.findViewById(R.id.account_no);
			TextView IFSCCode = (TextView) convertView
					.findViewById(R.id.ifsc_code);
			TextView account_holder_name=(TextView) convertView.findViewById(R.id.account_holder_name);
			ImageView imageSelect = (ImageView) convertView
					.findViewById(R.id.icon_selected);
			ImageView imageDefault = (ImageView) convertView
					.findViewById(R.id.icon_default);
			account_holder_name.setText(accountDetail.getName());
			accountType.setText(accountDetail.getBankType());
			accountNum.setText("A/C No. " + accountDetail.getAccountNo());
			IFSCCode.setText("IFSC Code: " + accountDetail.getIFSCCode());
			return convertView;
		}

		@Override
		public int getCount() {

			return bankAccountList.size();
		}

		@Override
		public Object getItem(int position) {

			return null;
		}

		@Override
		public long getItemId(int position) {

			return position;
		}
	}
}
