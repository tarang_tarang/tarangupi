package com.tarang.upi.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONException;

public class WebServiceCall {

	Context ctx;
	
	public WebServiceCall(Context ctx) {
		this.ctx = ctx;
	}
	
	public void triggerWebServiceCall(String url, String inputParam,ServiceCallback serviceCallBack,String progressDialogText) {
		if(CommonUtilsUpi.hasInternetConnectivity(ctx))
		{
			CallPostWebServices callPostWebServices = new CallPostWebServices(url,serviceCallBack,progressDialogText,ctx);
			callPostWebServices.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, inputParam);
		}else{
			CommonUtilsUpi.showToastMessage(ctx, "No internet connection", "bottom");
		}
		
	}
	
	public void triggerWebServiceCall(String url, String inputParam,ServiceCallback serviceCallBack) {
		if(CommonUtilsUpi.hasInternetConnectivity(ctx))
		{
			CallPostWebServices1 callPostWebServices = new CallPostWebServices1(url,serviceCallBack,ctx);
			callPostWebServices.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, inputParam);
		}else{
			CommonUtilsUpi.showToastMessage(ctx, "No internet connection", "bottom");
		}
		
	}
	
	private class CallPostWebServices extends AsyncTask<String, Void, String> {
		private ProgressDialog dialog; 
		private String url;
		private Boolean isCallSuccess = true;
		private ServiceCallback svccallback;
		private String progressDialogString = null;
		Context context;
	
		public CallPostWebServices(String url, ServiceCallback serviceCallBack, String progressDialogText,Context context) {
			this.url = url;
			this.svccallback = serviceCallBack;	
			this.progressDialogString = progressDialogText;
			this.context=context;
		}

	
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if(progressDialogString!=null)
			{
				dialog= new ProgressDialog(ctx);
	            dialog.setCancelable(false);
	            dialog.setMessage("Please wait...");
	            dialog.show();
				
			}
			
		}

		@Override
		protected String doInBackground(String... params) {
			String serviceResponse = null;
			try {
				
				serviceResponse = CommonUtilsUpi.createHttpsJsonRequest(url, params[0],context);
				//serviceResponse = CommonUtilsUpi.createHttpJsonRequest(url, params[0]);
				
			} catch (Exception e) {	
				isCallSuccess = false;
				e.printStackTrace();
			}
			return serviceResponse;
		}

		protected void onPostExecute(String serviceResponse) {
			
			try {
				if(progressDialogString!=null)
				{
					if(dialog!=null)
					{
							if(dialog.isShowing())
							{
								dialog.dismiss();
							}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
	
			if (serviceResponse != null) {
				
				if (this.isCallSuccess) {
                    try {
                        this.svccallback.SuccessCallbak(serviceResponse);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return;
				} else {
					this.svccallback.ErrorCallbak(serviceResponse);
					return;
				}
			}
				
			this.svccallback.ErrorCallbak(serviceResponse);
		}
	}
	
	private class CallPostWebServices1 extends AsyncTask<String, Void, String> {
		private String url;
		private Boolean isCallSuccess = true;
		private ServiceCallback svccallback;
		private String progressDialogString = null;
		Context context;
	
		public CallPostWebServices1(String url, ServiceCallback serviceCallBack,Context context) {
			this.url = url;
			this.svccallback = serviceCallBack;	
			this.context=context;
		}


		@Override
		protected String doInBackground(String... params) {
			String serviceResponse = null;
			try {
				
				serviceResponse = CommonUtilsUpi.createHttpsJsonRequest(url, params[0],context);
				
			} catch (Exception e) {	
				isCallSuccess = false;
				e.printStackTrace();
			}
			return serviceResponse;
		}

		protected void onPostExecute(String serviceResponse) {
			
			if (serviceResponse != null) {
				
				if (this.isCallSuccess) {
                    try {
                        this.svccallback.SuccessCallbak(serviceResponse);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return;
				} else {
					this.svccallback.ErrorCallbak(serviceResponse);
					return;
				}
			}
				
			this.svccallback.ErrorCallbak(serviceResponse);
		}
	}
}
