package com.tarang.upi.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.tarang.upi.R;
import com.tarang.upi.pojo.TransactionDetail;
import com.tarang.upi.utils.CommonUtilsUpi;
import com.tarang.upi.utils.ServiceCallback;
import com.tarang.upi.utils.WebServiceCall;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class TransactinHistoryActivity extends Activity implements OnItemClickListener{

	ListView transactionListView;
	String location;
	CommonUtilsUpi comUtils;
	WebServiceCall services;
	ArrayList<TransactionDetail> transactionList;
	TransactionListAdapter adapter;
	TextView txn_no_items;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_history);
		transactionListView = (ListView) findViewById(R.id.listorderhistory);
		TextView textTitle = (TextView) findViewById(R.id.title);
		textTitle.setText("Transaction History");
		txn_no_items=(TextView) findViewById(R.id.txn_no_items);
		
		transactionList = new ArrayList<TransactionDetail>();
		
		transactionListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				transactionDetaisPopUp(position);
			}
		});
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		location = getCommonUtils().getAddress(TransactinHistoryActivity.this);
		getTxtHistory();
	}

	private CommonUtilsUpi getCommonUtils() {
		if (comUtils == null) {
			comUtils = new CommonUtilsUpi();
		}
		return comUtils;
	}

	private WebServiceCall getWebServiceCall() {
		if (services == null) {
			services = new WebServiceCall(this);
		}
		return services;
	}
	
	public class TransactionListAdapter extends BaseAdapter {

		ArrayList<TransactionDetail> list;
		public TransactionListAdapter(Context context, ArrayList<TransactionDetail> list) {

			this.list=list;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) TransactinHistoryActivity.this
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if(convertView == null) 
				convertView = inflater.inflate(R.layout.transaction_adapter, parent, false);
			TextView date = (TextView) convertView.findViewById(R.id.transcation_date);
			TextView transcation_amount = (TextView) convertView.findViewById(R.id.transcation_amount);
			TextView transcation_status = (TextView) convertView.findViewById(R.id.transcation_status);
			TextView transcation_msg = (TextView) convertView.findViewById(R.id.transcation_msg);
			
			date.setText("Date : "+list.get(position).getTransactionDate());
			String amt = Double.toString(list.get(position).getAmount());
			transcation_amount.setText("Amount : "+amt);
			transcation_status.setText("Status : "+list.get(position).getStatus());
			transcation_msg.setText("Message : "+list.get(position).getTxnMessage());
			
			return convertView;
		}

		@Override
		public int getCount() {

			return transactionList.size();
		}

		@Override
		public Object getItem(int position) {

			return null;
		}

		@Override
		public long getItemId(int position) {

			return position;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
	}
	
	public void getTxtHistory() {
		
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		String moblNum = prefs.getString(UPIMacros.PREF_MOBILE_NUM, null);
		String userName = prefs.getString(UPIMacros.PREF_USER_NAME, null);
		
		JSONObject jobj=new JSONObject();
		JSONObject DObj = CommonUtilsUpi.DeviceInfo(moblNum,
				location, CommonUtilsUpi.getDeviceIPAddress(true),
				UPIMacros.TYPE,
				CommonUtilsUpi.getDeviceID(TransactinHistoryActivity.this),
				CommonUtilsUpi.getOS(), UPIMacros.APP_NAME,
				UPIMacros.CAPABILITY);
		
		try {
			jobj.put("device",DObj);
			jobj.put("userName", userName);
			
			System.out.println("Request --"+jobj.toString());
			getWebServiceCall().triggerWebServiceCall(UPIMacros.GET_TXT_DETAILS,jobj.toString(), new ServiceCallback() {
				
				@Override
				public void SuccessCallbak(String resp) {
					System.out.println("Responce -- "+resp);
					try {
						JSONObject jobj=new JSONObject(resp);
						if(jobj.getInt("statusCode")==200){
							
							JSONArray jArray=jobj.getJSONArray("txnSummaryList");
							
							for (int i = 0; i < jArray.length(); i++) {
								TransactionDetail pojo =new TransactionDetail();
								pojo.setStatus(jArray.getJSONObject(i).getString("status"));
								pojo.setPaymentFrom(jArray.getJSONObject(i).getString("paymentFrom"));
								pojo.setPaymentTo(jArray.getJSONObject(i).getString("paymentTo"));
								pojo.setTxnMessage(jArray.getJSONObject(i).getString("txnMessage"));
								pojo.setAmount(jArray.getJSONObject(i).getDouble("amount"));
								
								Date date=new Date(jArray.getJSONObject(i).getLong("transactionDate"));
						        SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yy");
						        String dateText = df2.format(date);
						        
						        pojo.setTransactionDate(dateText);
						        System.out.println(dateText);
								
								transactionList.add(pojo);
							}
							adapter=new TransactionListAdapter(TransactinHistoryActivity.this, transactionList);
							transactionListView.setAdapter(adapter);
							adapter.notifyDataSetChanged();
							if(transactionList.isEmpty()|| transactionList.size()==0) {
								txn_no_items.setVisibility(View.VISIBLE);
								transactionListView.setVisibility(View.GONE);
							}
						}else if(jobj.getInt("statusCode")==50114) {
                            Toast.makeText(TransactinHistoryActivity.this, "Session expired, please login again.", Toast.LENGTH_LONG).show();
                            Intent intent=new Intent(TransactinHistoryActivity.this,LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				
				@Override
				public void ErrorCallbak(String resp) {
					
				}
			}, "");
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public void transactionDetaisPopUp(int index) {
		
		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setContentView(R.layout.transaction_details);
		Window window = dialog.getWindow();
		WindowManager.LayoutParams wlp = window.getAttributes();
		wlp.gravity = Gravity.CENTER;

		TextView paymentFrom,paymentTo,txn_date,txn_detail_amount,txn_detail_status,txn_detail_msg;
		paymentFrom=(TextView) dialog.findViewById(R.id.paymentFrom);
		paymentTo=(TextView) dialog.findViewById(R.id.paymentTo);
		txn_date=(TextView) dialog.findViewById(R.id.txn_date);
		txn_detail_amount=(TextView) dialog.findViewById(R.id.txn_detail_amount);
		txn_detail_status=(TextView) dialog.findViewById(R.id.txn_detail_status);
		txn_detail_msg=(TextView) dialog.findViewById(R.id.txn_detail_msg);
		
		paymentFrom.setText("Payment from : "+transactionList.get(index).getPaymentFrom());
		paymentTo.setText("Payment to : "+transactionList.get(index).getPaymentTo());
		txn_date.setText("Date : "+transactionList.get(index).getTransactionDate());
		String amt = Double.toString(transactionList.get(index).getAmount());
		txn_detail_amount.setText("Amount : "+amt);
		txn_detail_status.setText("Status :"+transactionList.get(index).getStatus());
		txn_detail_msg.setText("Message : "+transactionList.get(index).getTxnMessage());
		
		Button done=(Button) dialog.findViewById(R.id.txt_done);
		done.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.show();
	
	}
}
