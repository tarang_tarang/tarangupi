package com.tarang.upi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tarang.upi.R;
import com.tarang.upi.pojo.MerchantListPojo;

import java.util.ArrayList;

public class MerchantListAdapter extends BaseAdapter {

	Context context;
	ArrayList<MerchantListPojo> list = new ArrayList();
	LayoutInflater inflater;

	public MerchantListAdapter(Context context, ArrayList<MerchantListPojo> list) {
		this.context = context;
		this.list = list;
		inflater = LayoutInflater.from(this.context);
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		MyViewHolder mViewHolder;
		 if (convertView == null) {
	            convertView = inflater.inflate(R.layout.merchant_list_view, parent, false);
	            mViewHolder = new MyViewHolder(convertView);
	            convertView.setTag(mViewHolder);
	        } else {
	            mViewHolder = (MyViewHolder) convertView.getTag();
	        }
		 
		 mViewHolder.nickName.setText(list.get(position).getNickName());
		return convertView;
	}

	private class MyViewHolder {
        TextView nickName;

        public MyViewHolder(View item) {
        	nickName = (TextView) item.findViewById(R.id.merchant_nickName);
            
        }
    }
}
