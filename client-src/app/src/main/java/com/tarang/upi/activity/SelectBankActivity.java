package com.tarang.upi.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.tarang.upi.R;
import com.tarang.upi.pojo.AccountDetail;
import com.tarang.upi.pojo.AccountProviders;
import com.tarang.upi.utils.CommonUtilsUpi;
import com.tarang.upi.utils.ServiceCallback;
import com.tarang.upi.utils.WebServiceCall;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.npci.upi.security.pinactivitycomponent.PinActivityComponent;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;

public class SelectBankActivity extends Activity implements
		OnItemSelectedListener, OnItemClickListener, OnClickListener {

	Spinner banksSpinner;
	boolean firstTime = true;
	ArrayList<AccountDetail> bankAccountList;
	private BankAccountsAdapter bankAccAdapter;
	ListView bankAccountListView;
	Button addAccountBtn;
	int selectedPosition = -1;
	ImageView menu;
	CommonUtilsUpi comUtils;
	WebServiceCall services;
	String location;
	ArrayList<AccountProviders> acountProviderList;
	CustomAdapter spinnerAdapter;
	Bundle extra;
    String dataFromPinActivity="";
    LinearLayout noti_bar;
    String txnId="";
    boolean isfirst=true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_bank);
		banksSpinner = (Spinner) findViewById(R.id.bank_spinner);
		addAccountBtn = (Button) findViewById(R.id.add_account);
		findViewById(R.id.menu).setOnClickListener(this);

        noti_bar=(LinearLayout)findViewById(R.id.ll_notification);
        noti_bar.setVisibility(View.GONE);
		addAccountBtn.setOnClickListener(this);
		banksSpinner.setOnItemSelectedListener(this);

		getAccountProviders();
		bankAccountList = new ArrayList<AccountDetail>();

		bankAccountListView = (ListView) findViewById(R.id.bank_accounts_list);
		bankAccountListView.setVisibility(View.GONE);
		addAccountBtn.setVisibility(View.GONE);
		bankAccountListView.setOnItemClickListener(this);
        findViewById(R.id.menu).setVisibility(View.GONE);

        txnId=constructMessageId();
        try {
            //coming back from the AccountDeatail, mobile details, adhaar number details activity and calling otp
            try {
                extra= getIntent().getExtras();
                if (extra.getString("call_otp").equals("call_otp")) {
                    //Otp_Popup();
                    getOtpCredential();
                }
            }catch(Exception e){
                //  return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
	}

	public void setAccountProvidersData() {
		spinnerAdapter = new CustomAdapter(this, R.layout.custom_spinner_item,
				acountProviderList);
		spinnerAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		banksSpinner.setAdapter(spinnerAdapter);
		spinnerAdapter.notifyDataSetChanged();
	}

	private WebServiceCall getWebServiceCall() {
		if (services == null) {
			services = new WebServiceCall(this);
		}
		return services;
	}

	@Override
	protected void onStart() {
		super.onStart();
        /*if(isfirst) {
            isfirst=false;
            location = getCommonUtils().getAddress(SelectBankActivity.this);
        }*/
        backGroundFetchLocation();
	}

    public void backGroundFetchLocation() {
        class locationFetch extends AsyncTask<Void,Void,Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                location = getCommonUtils().getAddress(SelectBankActivity.this);
                return null;
            }
        }locationFetch task=new locationFetch();
        task.execute();
    }
	private CommonUtilsUpi getCommonUtils() {
		if (comUtils == null) {
			comUtils = new CommonUtilsUpi();
		}
		return comUtils;
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		switch (parent.getId()) {
		case R.id.bank_spinner:

			if (position != 0) {
				firstTime = false;
				askOptionPopUp();
			} else {
				bankAccountListView.setVisibility(View.GONE);
				addAccountBtn.setVisibility(View.GONE);
				findViewById(R.id.no_account_one).setVisibility(View.VISIBLE);
				findViewById(R.id.no_account_two).setVisibility(View.VISIBLE);
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {

	}

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("Result Code:", String.valueOf(resultCode));
        if (requestCode == 1 && resultCode == Activity.RESULT_OK
                && data != null) {
            String errorMsgStr = data.getStringExtra("error");
            if (errorMsgStr != null && !errorMsgStr.isEmpty()) {
                Log.d("Error:", errorMsgStr);
                try {
                    JSONObject error = new JSONObject(errorMsgStr);
                    String errorCode = error.getString("errorCode");
                    String errorText = error.getString("errorText");
                    Toast.makeText(SelectBankActivity.this,
                            errorCode + ":" + errorText, Toast.LENGTH_LONG)
                            .show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return;
            }
            HashMap<String, String> credListHashMap = (HashMap<String, String>) data
                    .getSerializableExtra("credBlocks");
            for (String cred : credListHashMap.keySet()) { // This will return
                try {
                    JSONObject credBlock = new JSONObject(
                            credListHashMap.get(cred));
                    dataFromPinActivity=credBlock.getJSONObject("data").getString("encryptedBase64String");
                    Log.i("enc_msg", credBlock.toString());
                    Log.i("enc_msg",dataFromPinActivity);
                    // Log.i("enc_msg", credBlock.getString("message"));
                    getAccountList(dataFromPinActivity);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        } else if(requestCode == 2 && resultCode == Activity.RESULT_OK
                && data != null) {
            {
                String errorMsgStr = data.getStringExtra("error");
                if (errorMsgStr != null && !errorMsgStr.isEmpty()) {
                    Log.d("Error:", errorMsgStr);
                    try {
                        JSONObject error = new JSONObject(errorMsgStr);
                        String errorCode = error.getString("errorCode");
                        String errorText = error.getString("errorText");
                        Toast.makeText(SelectBankActivity.this,
                                errorCode + ":" + errorText, Toast.LENGTH_LONG)
                                .show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return;
                }
                HashMap<String, String> credListHashMap = (HashMap<String, String>) data
                        .getSerializableExtra("credBlocks");
                for (String cred : credListHashMap.keySet()) { // This will return
                    try {
                        JSONObject credBlock = new JSONObject(
                                credListHashMap.get(cred));
                        dataFromPinActivity=credBlock.getJSONObject("data").getString("encryptedBase64String");
                        Log.i("enc_msg", credBlock.toString());
                        Log.i("enc_msg",dataFromPinActivity);
                        Intent lIntent = new Intent(SelectBankActivity.this,
                                GeneratePinActivity.class);
                        lIntent.putExtra("dataFromPinActivity",dataFromPinActivity);
                        lIntent.putExtra("txnId", txnId);
                        lIntent.putExtra("account", bankAccountList.get(selectedPosition).getAccountNo());
                        startActivity(lIntent);
                       // finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

        }
    }
	public class BankAccountsAdapter extends BaseAdapter {

		public BankAccountsAdapter(Context context, int resource,
				List<AccountDetail> list) {
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) SelectBankActivity.this
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (convertView == null)
				convertView = inflater.inflate(R.layout.bank_accounts_adapter,
						parent, false);

			AccountDetail accountDetail = bankAccountList.get(position);
			TextView accountType = (TextView) convertView
					.findViewById(R.id.account_type);
			TextView accountNum = (TextView) convertView
					.findViewById(R.id.account_no);
			TextView IFSCCode = (TextView) convertView
					.findViewById(R.id.ifsc_code);
			TextView account_holder_name=(TextView) convertView.findViewById(R.id.account_holder_name);
			ImageView imageSelect = (ImageView) convertView
					.findViewById(R.id.icon_selected);
			ImageView imageDefault = (ImageView) convertView
					.findViewById(R.id.icon_default);
			if (selectedPosition != -1 && selectedPosition == position) {
				imageDefault.setVisibility(View.GONE);
				imageSelect.setVisibility(View.VISIBLE);
			} else {
				imageDefault.setVisibility(View.VISIBLE);
				imageSelect.setVisibility(View.GONE);
			}
			account_holder_name.setText(accountDetail.getName());
			accountType.setText(accountDetail.getBankType());
			accountNum.setText("A/C No. " + accountDetail.getAccountNo());
			IFSCCode.setText("IFSC Code: " + accountDetail.getIFSCCode());
			return convertView;
		}

		@Override
		public int getCount() {

			return bankAccountList.size();
		}

		@Override
		public Object getItem(int position) {

			return null;
		}

		@Override
		public long getItemId(int position) {

			return position;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		selectedPosition = position;
		bankAccAdapter.notifyDataSetChanged();
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.add_account:
			if (selectedPosition != -1) {
				//pinPopup();
			addBankAccount(bankAccountList.get(selectedPosition)
						.getId());

			} else {
				Toast.makeText(SelectBankActivity.this,
						"Please select one account from the list",
						Toast.LENGTH_LONG).show();
			}
			break;

		case R.id.menu:
			//menuPopup();
			break;

		case R.id.txtAdd:
			Toast.makeText(SelectBankActivity.this, "Hi", Toast.LENGTH_LONG)
					.show();
			break;
		default:
			break;
		}
	}

	private void getAccountProviders() {
		String ip = null;
		try {
			for (Enumeration en = NetworkInterface.getNetworkInterfaces(); en
					.hasMoreElements();) {
				NetworkInterface intf = (NetworkInterface) en.nextElement();
				for (Enumeration<InetAddress> enumIpAddr = intf
						.getInetAddresses(); enumIpAddr.hasMoreElements();) {
					InetAddress inetAddress = enumIpAddr.nextElement();
					if (!inetAddress.isLoopbackAddress()) {
						ip = inetAddress.getHostAddress();
					}
				}
			}
		} catch (Exception e) {
			Log.e("------------", e.toString());
		}

		String type = "Mobile";
		String os = "Android " + Build.VERSION.RELEASE;
		String appName = "UPI solution v1.0";

		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		String moblNum = prefs.getString(UPIMacros.PREF_MOBILE_NUM, null);
		String userName = prefs.getString(UPIMacros.PREF_USER_NAME, null);
		JSONObject jobj = new JSONObject();
		try {
			jobj.put("userName", userName);

			JSONObject Djobj = new JSONObject();
			Djobj.put("mobile", moblNum);
			Djobj.put("geoCode", "560017");
			Djobj.put("location", "");
			Djobj.put("ip", ip);
			Djobj.put("type", type);
			Djobj.put("terminalId",
					CommonUtilsUpi.getDeviceID(SelectBankActivity.this));
			Djobj.put("os", os);
			Djobj.put("app", appName);
			Djobj.put("capability", UPIMacros.CAPABILITY);

			jobj.put("device", Djobj);

			System.out.println("Request---" + jobj.toString());
			System.out.println("URL --- " + UPIMacros.ACC_PROVIDER);
			getWebServiceCall().triggerWebServiceCall(UPIMacros.ACC_PROVIDER,
					jobj.toString(), new ServiceCallback() {

						@Override
						public void SuccessCallbak(String resp) {
							System.out.println("Result---" + resp);
							if (resp != null) {

								try {
									JSONObject JResp = new JSONObject(resp);
									int statusCode = JResp.getInt("statusCode");
									if (statusCode == 200) {
										acountProviderList = parseAccountProviderList(resp);
										setAccountProvidersData();
									}else if(JResp.getInt("statusCode")==50114) {
                                        Toast.makeText(SelectBankActivity.this, "Session expired, please login again.", Toast.LENGTH_LONG).show();
                                        Intent intent=new Intent(SelectBankActivity.this,LoginActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
								} catch (JSONException e) {
									e.printStackTrace();
								}

							}
						}

						@Override
						public void ErrorCallbak(String resp) {

						}
					}, "Loading");

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void getAccountList(String otp) {
		JSONObject jobj = new JSONObject();
		try {
			SharedPreferences prefs = PreferenceManager
					.getDefaultSharedPreferences(getApplicationContext());
			String moblNum = prefs.getString(UPIMacros.PREF_MOBILE_NUM, null);
			String userName = prefs.getString(UPIMacros.PREF_USER_NAME, null);
			String challenge = prefs.getString(UPIMacros.PREF_UPI_CHLL, null);

			jobj.put("isBenAccDetails", "false");
			jobj.put("mmid", UPIMacros.MMID);
			jobj.put("userName", userName);
            jobj.put("txnId", txnId);

			JSONObject DObj = CommonUtilsUpi.DeviceInfo(moblNum,
					location, CommonUtilsUpi.getDeviceIPAddress(true),
					UPIMacros.TYPE,
					CommonUtilsUpi.getDeviceID(SelectBankActivity.this),
					CommonUtilsUpi.getOS(), UPIMacros.APP_NAME,
					UPIMacros.CAPABILITY);

			jobj.put("device", DObj);
			
			JSONObject mobileCredentials = new JSONObject();
			mobileCredentials.put("dataValue", otp);
			
			jobj.put("mobileCredentials", mobileCredentials);
			
			JSONObject acDetails=new JSONObject(extra.getString("acDetails"));
			
			jobj.put("acDetails", acDetails);
			
			System.out.println("Request---" + jobj.toString());
			System.out.println("URL --- " + UPIMacros.ACC_LIST);
			getWebServiceCall().triggerWebServiceCall(UPIMacros.ACC_LIST,
					jobj.toString(), new ServiceCallback() {

						@Override
						public void SuccessCallbak(String resp) {

                            try {
                                JSONObject jobj=new JSONObject(resp);
                                    if(resp.contains("200")) {
                                        System.out.println("Result---" + resp);
                                        bankAccountList = parseAcctOtpResp(resp);
                                        populateAccountLists(bankAccountList);
                                    }else if(resp.contains("50112")){
                                        Toast.makeText(SelectBankActivity.this, "Get Accounts service failed", Toast.LENGTH_LONG).show();
                                    } else if(resp.contains("500")) {
                                        Toast.makeText(SelectBankActivity.this, "Connection time out, please try again.", Toast.LENGTH_LONG).show();
                                    }else if(jobj.getInt("statusCode")==50114) {
                                        Toast.makeText(SelectBankActivity.this, "Session expired, please login again.", Toast.LENGTH_LONG).show();
                                        Intent intent=new Intent(SelectBankActivity.this,LoginActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
						}

						@Override
						public void ErrorCallbak(String resp) {
                            Toast.makeText(SelectBankActivity.this, "Connection time out, please try again.", Toast.LENGTH_LONG).show();
						}
					}, "");
		} catch (JSONException e) {
			e.printStackTrace();
            Toast.makeText(SelectBankActivity.this, "Connection time out, please try again.", Toast.LENGTH_LONG).show();
		}
	}

	/***** Adapter class extends with ArrayAdapter ******/
	public class CustomAdapter extends ArrayAdapter<String> {

		private Activity activity;
		private ArrayList data;
		AccountProviders tempValues = null;
		LayoutInflater inflater;

		public CustomAdapter(SelectBankActivity context,
				int textViewResourceId, ArrayList objects) {
			super(context, textViewResourceId, objects);

			activity = context;
			data = objects;

			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {
			return getCustomView(position, convertView, parent);
		}

		@Override
		public int getCount() {
			return acountProviderList.size();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			return getCustomView(position, convertView, parent);
		}
		public View getCustomView(int position, View convertView,
				ViewGroup parent) {
			View row = inflater.inflate(R.layout.custom_spinner_item, parent,
					false);
			tempValues = null;
			tempValues = (AccountProviders) data.get(position);

			TextView label = (TextView) row.findViewById(R.id.label);
			label.setText(tempValues.getBankName());
			return row;
		}
	}

	public ArrayList<AccountProviders> parseAccountProviderList(String resp) {

		ArrayList<AccountProviders> accList = new ArrayList<AccountProviders>();
		try {
			JSONObject jobj = new JSONObject(resp);
			JSONArray jArray = jobj.getJSONArray("listOfBanks");

			AccountProviders accPro0 = new AccountProviders();
			accPro0.setId(00);
			accPro0.setBankName("Please select Bank");
			accList.add(accPro0);
			
			for (int i = 0; i < jArray.length(); i++) {

				AccountProviders accPro = new AccountProviders();
				JSONObject arrayObj = jArray.getJSONObject(i);

				accPro.setId(arrayObj.getInt("id"));
				accPro.setBankName(arrayObj.getString("bankName"));

				accList.add(accPro);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return accList;
	}

	public ArrayList<AccountDetail> parseAcctOtpResp(String resp) {
		ArrayList<AccountDetail> accList = new ArrayList<AccountDetail>();
		try {
			JSONObject jobj = new JSONObject(resp);
			JSONArray jArray = jobj.getJSONArray("accountSummaryList");
			for (int i = 0; i < jArray.length(); i++) {

				AccountDetail accDetail = new AccountDetail();
				JSONObject arrObj = jArray.getJSONObject(i);
				accDetail.setAccountNo(arrObj.getString("maskedAccnumber"));
				accDetail.setAeba(arrObj.getString("aeba"));
				accDetail.setBankType("BankType");
				accDetail.setId(arrObj.getInt("id"));
				accDetail.setIFSCCode(arrObj.getString("ifsc"));
				accDetail.setName(arrObj.getString("name"));
				accList.add(accDetail);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return accList;
	}

	private void populateAccountLists(ArrayList<AccountDetail> accountList) {
		bankAccountListView.setVisibility(View.VISIBLE);
		addAccountBtn.setVisibility(View.VISIBLE);
		findViewById(R.id.no_account_one).setVisibility(View.GONE);
		findViewById(R.id.no_account_two).setVisibility(View.GONE);
		bankAccAdapter = new BankAccountsAdapter(this,
				R.layout.bank_accounts_adapter, accountList);
		bankAccountListView.setAdapter(bankAccAdapter);
		bankAccAdapter.notifyDataSetChanged();
	}

	private void addBankAccount(int id) {
		JSONObject jobj = new JSONObject();
		try {
			SharedPreferences prefs = PreferenceManager
					.getDefaultSharedPreferences(getApplicationContext());
			String moblNum = prefs.getString(UPIMacros.PREF_MOBILE_NUM, null);
			String userName = prefs.getString(UPIMacros.PREF_USER_NAME, null);

			jobj.put("id", id);
			jobj.put("userName", userName);
			JSONObject DObj = CommonUtilsUpi.DeviceInfo(moblNum,
					location, CommonUtilsUpi.getDeviceIPAddress(true),
					UPIMacros.TYPE,
					CommonUtilsUpi.getDeviceID(SelectBankActivity.this),
					CommonUtilsUpi.getOS(), UPIMacros.APP_NAME,
					UPIMacros.CAPABILITY);

			jobj.put("device", DObj);

			System.out.println("Request---" + jobj.toString());
			System.out.println("URL --- " + UPIMacros.ADD_BANK_ACC);
			getWebServiceCall().triggerWebServiceCall(UPIMacros.ADD_BANK_ACC,
					jobj.toString(), new ServiceCallback() {

						@Override
						public void SuccessCallbak(String resp) {
							System.out.println("Result---" + resp);
							if (resp != null) {

								try {
									JSONObject jobj = new JSONObject(resp);

									if (jobj.getInt("statusCode") == 200) {

										UPIMacros.bankAccountListGlobal
												.add(bankAccountList
														.get(selectedPosition));
                                        generatePinPopUp();
									}else if(jobj.getInt("statusCode")==50114) {
                                        Toast.makeText(SelectBankActivity.this, "Session expired, please login again.", Toast.LENGTH_LONG).show();
                                        Intent intent=new Intent(SelectBankActivity.this,LoginActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
								} catch (JSONException e) {
									e.printStackTrace();
									Toast.makeText(SelectBankActivity.this, "Connection time out, please try again.",  Toast.LENGTH_LONG).show();
								}

							}
						}

						@Override
						public void ErrorCallbak(String resp) {

						}
					}, "");
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

    public void generatePinPopUp() {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.generate_pin_popup);

        dialog.setCancelable(false);
        Button generate_btn=(Button) dialog.findViewById(R.id.generate_btn);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;

        generate_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(SelectBankActivity.this,GeneratePinActivity.class);
                intent.putExtra("txnId",txnId);
                startActivity(intent);
                finish();
            }
        });
        dialog.show();
    }

    public void getOtpCredential() {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        String xmlPayloadString = prefs.getString(UPIMacros.PUBLIC_KEY, null);

        String credAllowedString = "{\n" + "\t\"CredAllowed\": [{\n"
                + "\t\t\"type\": \"OTP\",\n" + "\t\t\"subtype\": \"OTP\",\n"
                + "\t\t\"dType\": \"ALPH | NUM\",\n" + "\t\t\"dLength\": 6\n" +
                "\t}]\n" + "}";

        Intent intent = new Intent(SelectBankActivity.this,
                PinActivityComponent.class);

        // Create Keycode
        intent.putExtra("keyCode", "NPCI");

        // Create xml payload
        if (xmlPayloadString.isEmpty()) {
            Toast.makeText(SelectBankActivity.this,
                    "XML List Key API is not loaded.",
                    Toast.LENGTH_LONG).show();
            return;
        }
        intent.putExtra("keyXmlPayload", xmlPayloadString); // It will

        // Create Controls
        if (credAllowedString.isEmpty()) {
            Toast.makeText(SelectBankActivity.this,
                    "Required Credentials could not be loaded.",
                    Toast.LENGTH_LONG).show();
            return;
        }
        intent.putExtra("controls", credAllowedString);
        // Create Configuration
        try {
            JSONObject configuration = new JSONObject();
            configuration.put("payerBankName", "Tarang Software Pvt Ltd.");
            configuration.put("backgroundColor", "#FFFFFF");
            Log.i("configuration", configuration.toString());
            intent.putExtra("configuration", configuration.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Create Salt
        try {
            JSONObject salt = new JSONObject();
            salt.put("txnId", txnId);
            //salt.put("txnAmount", "200");
            salt.put("deviceId", CommonUtilsUpi.getDeviceID(SelectBankActivity.this));
            salt.put("appId", UPIMacros.APP_ID);
            Log.i("salt", salt.toString());
            intent.putExtra("salt", salt.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Create Pay Info
        JSONArray payInfoArray = new JSONArray();
        try {
			/*JSONObject jsonPayeeName = new JSONObject();
			jsonPayeeName.put("name", "payeeName");
			jsonPayeeName.put("value", "John Smith");
			payInfoArray.put(jsonPayeeName);

			JSONObject jsonNote = new JSONObject();
			jsonNote.put("name", "note");
			jsonNote.put("value", "Pay restaurant bill");
			payInfoArray.put(jsonNote);

			JSONObject jsonRefId = new JSONObject();
			jsonRefId.put("name", "refId");
			jsonRefId.put("value", "589430957393");
			payInfoArray.put(jsonRefId);

			JSONObject jsonRefUrl = new JSONObject();
			jsonRefUrl.put("name", "refUrl");
			jsonRefUrl.put("value", "https://indianbank.com");
			payInfoArray.put(jsonRefUrl);

			JSONObject jsonAccount = new JSONObject();
			jsonAccount.put("name", "account");
			jsonAccount.put("value", "122XXX423");
			payInfoArray.put(jsonAccount);*/

            Log.i("payInfo", payInfoArray.toString());
            intent.putExtra("payInfo", payInfoArray.toString());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        // Create Language Pref
        intent.putExtra("languagePref", "En_US");

        startActivityForResult(intent, 1);
    }

    public void getGeneratePinCredential() {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        String xmlPayloadString = prefs.getString(UPIMacros.PUBLIC_KEY, null);

        String credAllowedString = "{\n" + "\t\"CredAllowed\": [{\n"
                + "\t\t\"type\": \"PIN\",\n" + "\t\t\"subtype\": \"MPIN\",\n"
                + "\t\t\"dType\": \"ALPH | NUM\",\n" + "\t\t\"dLength\": 6\n" +
                "\t}]\n" + "}";

        Intent intent = new Intent(SelectBankActivity.this,
                PinActivityComponent.class);

        // Create Keycode
        intent.putExtra("keyCode", "NPCI");

        // Create xml payload
        if (xmlPayloadString.isEmpty()) {
            Toast.makeText(SelectBankActivity.this,
                    "XML List Key API is not loaded.",
                    Toast.LENGTH_LONG).show();
            return;
        }
        intent.putExtra("keyXmlPayload", xmlPayloadString); // It will

        // Create Controls
        if (credAllowedString.isEmpty()) {
            Toast.makeText(SelectBankActivity.this,
                    "Required Credentials could not be loaded.",
                    Toast.LENGTH_LONG).show();
            return;
        }
        intent.putExtra("controls", credAllowedString);

        // Create Configuration
        try {
            JSONObject configuration = new JSONObject();
            configuration.put("payerBankName", "Tarang Software Pvt Ltd.");
            configuration.put("backgroundColor", "#FFFFFF");
            Log.i("configuration", configuration.toString());
            intent.putExtra("configuration", configuration.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Create Salt
        try {
            JSONObject salt = new JSONObject();
            salt.put("txnId", txnId);
            //salt.put("txnAmount", "200");
            salt.put("deviceId", CommonUtilsUpi.getDeviceID(SelectBankActivity.this));
            salt.put("appId", UPIMacros.APP_ID);
            Log.i("salt", salt.toString());
            intent.putExtra("salt", salt.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Create Pay Info
        JSONArray payInfoArray = new JSONArray();
        try {
			/*JSONObject jsonPayeeName = new JSONObject();
			jsonPayeeName.put("name", "payeeName");
			jsonPayeeName.put("value", "John Smith");
			payInfoArray.put(jsonPayeeName);

			JSONObject jsonNote = new JSONObject();
			jsonNote.put("name", "note");
			jsonNote.put("value", "Pay restaurant bill");
			payInfoArray.put(jsonNote);

			JSONObject jsonRefId = new JSONObject();
			jsonRefId.put("name", "refId");
			jsonRefId.put("value", "589430957393");
			payInfoArray.put(jsonRefId);

			JSONObject jsonRefUrl = new JSONObject();
			jsonRefUrl.put("name", "refUrl");
			jsonRefUrl.put("value", "https://indianbank.com");
			payInfoArray.put(jsonRefUrl);

			JSONObject jsonAccount = new JSONObject();
			jsonAccount.put("name", "account");
			jsonAccount.put("value", "122XXX423");
			payInfoArray.put(jsonAccount);*/

            Log.i("payInfo", payInfoArray.toString());
            intent.putExtra("payInfo", payInfoArray.toString());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        // Create Language Pref
        intent.putExtra("languagePref", "En_US");

        startActivityForResult(intent, 2);
    }

	public void askOptionPopUp() {

		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setContentView(R.layout.ask_option);

		Window window = dialog.getWindow();
		WindowManager.LayoutParams wlp = window.getAttributes();
		wlp.gravity = Gravity.CENTER ;

		TextView mobile_mmid=(TextView) dialog.findViewById(R.id.mobile_ask);
		TextView adhaar=(TextView) dialog.findViewById(R.id.adhaar);
		TextView acc_detail=(TextView) dialog.findViewById(R.id.acc_detail);

		mobile_mmid.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				/*Intent intent=new Intent(SelectBankActivity.this,MobileMmidActivity.class);
				startActivity(intent);*/

                Toast.makeText(SelectBankActivity.this,"Please select Account details to proceed.",Toast.LENGTH_LONG).show();
			}
		});
		adhaar.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				/*Intent intent=new Intent(SelectBankActivity.this,AdhaarDetailActivity.class);
				startActivity(intent);*/
                Toast.makeText(SelectBankActivity.this,"Please select Account details to proceed.",Toast.LENGTH_LONG).show();
			}
		});
		acc_detail.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent=new Intent(SelectBankActivity.this,AccountDetailAskActivity.class);
				startActivity(intent);
			}
		});
		dialog.show();
	}

    public static String constructMessageId(){
        return "TARANG" + String.valueOf(System.currentTimeMillis());
    }

}
