package com.tarang.upi.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.tarang.upi.R;
import com.tarang.upi.utils.AppLocationService;
import com.tarang.upi.utils.CommonUtilsUpi;
import com.tarang.upi.utils.LocationAddress;
import com.tarang.upi.utils.ServiceCallback;
import com.tarang.upi.utils.WebServiceCall;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class CreatePasswordActivity extends Activity implements OnClickListener {

	private EditText emailId, password, confirmPassword, userName, ADHAARNum,
			firstName, lastName, middleName;
	AppLocationService appLocationService;
	WebServiceCall service;
	String locationAddress;

	// for NPCI utilities start
	private String xmlPayloadString = "";
	private String credAllowedString = "";
	private final String keyCode = "NPCI";
	String token = "";
	
	String location;
	CommonUtilsUpi comUtils;
	
	boolean clInitialized = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_password);
		emailId = (EditText) findViewById(R.id.edt_email);
		password = (EditText) findViewById(R.id.edt_createpassword);
		confirmPassword = (EditText) findViewById(R.id.edt_confirmpassword);
		userName = (EditText) findViewById(R.id.edt_username);
		firstName = (EditText) findViewById(R.id.edt_firstname);
		lastName = (EditText) findViewById(R.id.edt_lastname);
		middleName = (EditText) findViewById(R.id.edt_middlename);
		ADHAARNum = (EditText) findViewById(R.id.edt_adhaar);
		findViewById(R.id.signin).setOnClickListener(this);
		findViewById(R.id.skip).setOnClickListener(this);

		appLocationService = new AppLocationService(this);
		service = new WebServiceCall(this);

	}

	@Override
	protected void onStart() {
		super.onStart();
		location = getCommonUtils().getAddress(CreatePasswordActivity.this);
	}

	private WebServiceCall getWebServiceCall() {
		if (service == null) {
			service = new WebServiceCall(this);
		}
		return service;
	}

	private CommonUtilsUpi getCommonUtils() {
		if (comUtils == null) {
			comUtils = new CommonUtilsUpi();
		}
		return comUtils;
	}

	

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.signin:

			if (firstName.getText().toString().isEmpty()) {
				firstName.requestFocus();
				Toast.makeText(this, "Please enter first name.",
						Toast.LENGTH_LONG).show();
			} else if (firstName.getText().toString().length() < 3) {
				firstName.requestFocus();
				Toast.makeText(this,
						"first name should be minimun 3 characters.",
						Toast.LENGTH_LONG).show();
			} else if (lastName.getText().toString().isEmpty()) {
				lastName.requestFocus();
				Toast.makeText(this, "Please enter last name.",
						Toast.LENGTH_LONG).show();
			} else if (lastName.getText().toString().length() < 3) {
				lastName.requestFocus();
				Toast.makeText(this,
						"Last name should be minimun 3 characters.",
						Toast.LENGTH_LONG).show();
			} else if (userName.getText().toString().isEmpty()) {
				userName.requestFocus();
				Toast.makeText(this, "Please enter user name.",
						Toast.LENGTH_LONG).show();
			} else if (userName.getText().toString().length() < 5) {
				userName.requestFocus();
				Toast.makeText(this,
						"User name should be minimun 5 characters.",
						Toast.LENGTH_LONG).show();
			} else if (emailId.getText().toString().isEmpty()) {
				emailId.requestFocus();
				Toast.makeText(this, "Please enter email.", Toast.LENGTH_LONG)
						.show();
			} else if (emailId.getText().toString().length() < 7) {
				emailId.requestFocus();
				Toast.makeText(this,
						"Email Id should be minimum 7 characters.",
						Toast.LENGTH_LONG).show();
			} else if (ADHAARNum.getText().toString().isEmpty()) {
				ADHAARNum.requestFocus();
				Toast.makeText(this, "Please enter ADHAAR number.",
						Toast.LENGTH_LONG).show();
			} else if (ADHAARNum.getText().toString().length() < 12) {
				ADHAARNum.requestFocus();
				Toast.makeText(this,
						"Adhaa number should be minimun 12 characters.",
						Toast.LENGTH_LONG).show();
			} else if (password.getText().toString().isEmpty()) {
				password.requestFocus();
				Toast.makeText(this, "Please enter password.",
						Toast.LENGTH_LONG).show();
			} else if (password.getText().toString().length() < 7) {
				password.requestFocus();
				Toast.makeText(this,
						"Password should be minimum 7 characters.",
						Toast.LENGTH_LONG).show();
			} else if (!confirmPassword.getText().toString().equals(password.getText().toString())) {
				confirmPassword.requestFocus();
				Toast.makeText(this,
						"Confirm password should match with password.",
						Toast.LENGTH_LONG).show();
			}  else {
				RegisterHitJson();
			}

			break;
		default:
			break;
		}
	}

	public void RegisterHitJson() {

		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		String moblNum = prefs.getString(UPIMacros.PREF_MOBILE_NUM, null);
		String rnsMpaId = prefs.getString(UPIMacros.GCM_REG_ID, null);
        if(rnsMpaId.length()<10) {
            registerInBackground();
            Toast.makeText(CreatePasswordActivity.this,"Google play server failed, please try again.",Toast.LENGTH_LONG).show();
            return;
        }
		JSONObject jobj = new JSONObject();
		try {
			jobj.put("firstName", firstName.getText().toString());
			jobj.put("lastName", lastName.getText().toString());
			jobj.put("middleName", middleName.getText().toString());
			jobj.put("mobileNumber", moblNum);
			jobj.put("adharNumber", ADHAARNum.getText().toString());
			jobj.put("email", emailId.getText().toString());
			jobj.put("userName", userName.getText().toString());
			jobj.put("password", password.getText().toString());
			jobj.put("rnsMpaId", rnsMpaId);

			JSONObject DObj = CommonUtilsUpi.DeviceInfo(moblNum,
					location, CommonUtilsUpi.getDeviceIPAddress(true),
					UPIMacros.TYPE,
					CommonUtilsUpi.getDeviceID(CreatePasswordActivity.this),
					CommonUtilsUpi.getOS(), UPIMacros.APP_NAME,
					UPIMacros.CAPABILITY);
			
			jobj.put("device", DObj);

			System.out.println("Request---" + jobj.toString());
			System.out.println("URL --- " + UPIMacros.registration);
			service.triggerWebServiceCall(UPIMacros.registration,
					jobj.toString(), new ServiceCallback() {

						@Override
						public void SuccessCallbak(String resp) {
							System.out.println("Result---" + resp);
							if (resp != null) {

								try {
									JSONObject JResp = new JSONObject(resp);
									int statusCode = JResp.getInt("statusCode");
									if (statusCode == 200) {
										SharedPreferences prefs = PreferenceManager
												.getDefaultSharedPreferences(getApplicationContext());
										SharedPreferences.Editor prefEditor = prefs
												.edit();
										prefEditor.putString(
												UPIMacros.PREF_EMAIL_ID,
												emailId.getText().toString());
										prefEditor.putString(
												UPIMacros.PREF_USER_NAME,
												userName.getText().toString());
										prefEditor.putString(
												UPIMacros.PREF_PASSWORD,
												password.getText().toString());
										prefEditor.putString(
												UPIMacros.PREF_ADHAAR,
												ADHAARNum.getText().toString());
										prefEditor.putBoolean(
												UPIMacros.PREF_LOG_ON, true);
										prefEditor.putString(
												UPIMacros.PUBLIC_KEY, JResp.getString("xmlPayload"));
										prefEditor.commit();
										Intent lIntent = new Intent(
												CreatePasswordActivity.this,
												LoginActivity.class);
										startActivity(lIntent);
										finish();

									} else if (statusCode == 50018) {
										Toast.makeText(CreatePasswordActivity.this,
												"Internal Server Error",
												Toast.LENGTH_LONG).show();
									} else if (statusCode == 50101) {
										Toast.makeText(CreatePasswordActivity.this,
												checkErrorMsg(resp),
												Toast.LENGTH_LONG).show();
									} else if (statusCode == 50102) {
										Toast.makeText(CreatePasswordActivity.this,
												"Registration Service fail",
												Toast.LENGTH_LONG).show();
									} else if (statusCode == 50103) {
										Toast.makeText(
												CreatePasswordActivity.this,
												"Aadhaar number already exists",
												Toast.LENGTH_LONG).show();
									} else if (statusCode == 50104) {
										Toast.makeText(CreatePasswordActivity.this,
												"Mobile number already exists",
												Toast.LENGTH_LONG).show();
									} else if (statusCode == 50105) {
										Toast.makeText(CreatePasswordActivity.this,
												"User name already exists",
												Toast.LENGTH_LONG).show();
									} else if (statusCode == 50105) {
										Toast.makeText(CreatePasswordActivity.this,
												"User name already exists",
												Toast.LENGTH_LONG).show();
									} else if (statusCode == 50105) {

									}
								} catch (JSONException e) {
									e.printStackTrace();
								}

							}
						}

						@Override
						public void ErrorCallbak(String resp) {

						}
					}, "Loading");

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

    public void registerInBackground() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                GoogleCloudMessaging gcm = GoogleCloudMessaging
                        .getInstance(getApplicationContext());
                try {
                    String regId = "";
                    int i = 0;
                    do {
                        regId = gcm.register(UPIMacros.SENDER_ID);
                        i++;
                        if (regId.length() > 0) {
                            i = 3;
                        }
                    } while (i < 3);
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor prefEditor = prefs.edit();
                    prefEditor.putString(UPIMacros.GCM_REG_ID, regId);
                    prefEditor.commit();
                    Log.d("SplashActivity", "Got GCM Reg ID -----> " + regId);

                } catch (IOException ex) {
                    Log.d("Upi ", "Error: while GCM regn" + ex.getMessage());
                    /*if(countGCMRegister<5) {
                        countGCMRegister++;
                        System.out.println("countGCMRegister --- "+countGCMRegister);
                        registerInBackground();
                    }*/
                }
                return null;
            }
        }.execute(null, null, null);
    }
	public String checkErrorMsg(String resp) {

		try {
			JSONObject jobj = new JSONObject(resp);
			if (!jobj.getString("firstNameErrMsg").equals("null")) {
				return jobj.getString("firstNameErrMsg");
			} else if (!jobj.getString("lastNameErrMsg").equals("null")) {
				return jobj.getString("lastNameErrMsg");
			} else if (!jobj.getString("middleNameErrMsg").equals("null")) {
				return jobj.getString("middleNameErrMsg");
			} else if (!jobj.getString("mobileNumberErrMsg").equals("null")) {
				return jobj.getString("mobileNumberErrMsg");
			} else if (!jobj.getString("adharNumberErrMsg").equals("null")) {
				return jobj.getString("adharNumberErrMsg");
			} else if (!jobj.getString("emailErrMsg").equals("null")) {
				return jobj.getString("emailErrMsg");
			} else if (!jobj.getString("userNameErrMsg").equals("null")) {
				return jobj.getString("userNameErrMsg");
			} else if (!jobj.getString("passwordErrMsg").equals("null")) {
				return jobj.getString("passwordErrMsg");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;

	}

	public void LocationSer() {

		Location gpsLocation = appLocationService
				.getLocation(LocationManager.GPS_PROVIDER);
		if (gpsLocation != null) {
			double latitude = gpsLocation.getLatitude();
			double longitude = gpsLocation.getLongitude();
			String result = "Latitude: " + gpsLocation.getLatitude()
					+ " Longitude: " + gpsLocation.getLongitude();

		} else {
			showSettingsAlert();
		}
	}

	public void addressSer() {

		Location location = appLocationService
				.getLocation(LocationManager.GPS_PROVIDER);

		if (location != null) {
			double latitude = location.getLatitude();
			double longitude = location.getLongitude();
			LocationAddress.getAddressFromLocation(latitude, longitude,
					getApplicationContext(), new GeocoderHandler());
		} else {
			showSettingsAlert();
		}
	}

	public void showSettingsAlert() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
		alertDialog.setTitle("SETTINGS");
		alertDialog
				.setMessage("Enable Location Provider! Go to settings menu?");
		alertDialog.setPositiveButton("Settings",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						CreatePasswordActivity.this.startActivity(intent);
					}
				});
		alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		alertDialog.show();
	}

	@SuppressLint("HandlerLeak")
	private class GeocoderHandler extends Handler {
		@Override
		public void handleMessage(Message message) {
			switch (message.what) {
			case 1:
				Bundle bundle = message.getData();
				locationAddress = bundle.getString("address");
				break;
			default:
				locationAddress = null;
			}
		}
	}
}
