package com.tarang.upi.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tarang.upi.R;

/**
 * Created by tripatys on 18/3/16.
 */
public class ResetPinActivity extends Activity implements View.OnClickListener{

    TextView title;
    EditText new_pin,old_pin,con_pin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_pin);

        title= (TextView) findViewById(R.id.title);
        title.setText("Reset Pin");

        new_pin= (EditText) findViewById(R.id.new_pin);
        old_pin= (EditText) findViewById(R.id.old_pin);
        con_pin= (EditText) findViewById(R.id.new_con_pin);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.reset_submit:
                if(old_pin.getText().toString().trim().isEmpty() || new_pin.getText().toString().trim().length()<6) {
                    old_pin.requestFocus();
                    Toast.makeText(ResetPinActivity.this,"Please enter valid 6 digit.",Toast.LENGTH_LONG).show();
                } else if(new_pin.getText().toString().trim().isEmpty() || new_pin.getText().toString().trim().length()<6) {
                    new_pin.requestFocus();
                    Toast.makeText(ResetPinActivity.this,"Please enter valid 6 digit.",Toast.LENGTH_LONG).show();
                }
                break;
            default:
                break;
        }
    }
}
