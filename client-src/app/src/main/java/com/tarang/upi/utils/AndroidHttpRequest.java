package com.tarang.upi.utils;

import android.content.Context;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public final class AndroidHttpRequest {
    private static final String CERTIFICATE_ALIAS = "certificateTarangtech";
    private static final String CHAR_SET = "utf-8";

    private Map<String, String> params;
    private URL url;
    private Context ctx;

    public void setContext(final Context ctx) {
        this.ctx = ctx;
    }

    public AndroidHttpRequest(String url) {
        try {
            this.url = new URL(url);
        } catch (MalformedURLException e) {
            Log.d("AndroidHttpRequest", e.getMessage());
        }
    }

    private TrustManager tm = new X509TrustManager() {
        public void checkClientTrusted(X509Certificate[] chain,
                                       String authType) throws CertificateException {

        }

        public void checkServerTrusted(X509Certificate[] chain,
                                       String authType) throws CertificateException {

        }

        public X509Certificate[] getAcceptedIssuers() {

            return null;
        }
    };

    private HttpsURLConnection getUrlConnection(SSLContext context) {
        try {
            HttpsURLConnection urlConnection = null;

            // Tell the URLConnection to use a SocketFactory from our SSLContext
            urlConnection = (HttpsURLConnection) url.openConnection();

            HostnameVerifier hostnameVerifier = new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    /*HostnameVerifier hv = HttpsURLConnection.getDefaultHostnameVerifier();
                    return hv.verify("121.244.157.134/emailAddress=action@tarangtech.com", session);*/
                    return true;
                }
            };

            urlConnection.setSSLSocketFactory(context.getSocketFactory());
            urlConnection.setHostnameVerifier(hostnameVerifier);
            urlConnection.setRequestMethod("POST");
            urlConnection.setConnectTimeout(60000);
            urlConnection.setReadTimeout(60000);
            urlConnection.setUseCaches(false);
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("charset", CHAR_SET);
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);
            urlConnection.connect();
            return urlConnection;
        } catch (Exception e) {
            Log.d("tag", e.getMessage(), e);
        }
        return null;
    }

    public void put(final String name, final String value) {
        if (params == null) {
            params = new HashMap<>();
        }
        params.put(name, value);
    }

    public void put(Map<String, String> parameters) {
        for (Object o : parameters.entrySet()) {
            HashMap.Entry pair = (HashMap.Entry) o;
            put((String) pair.getKey(), (String) pair.getValue());
        }
    }

    public String postJSONRequest() {
        Certificate certificate = null;
        DataOutputStream os = null;
        BufferedReader br = null;
        HttpsURLConnection urlConnection = null;

        try {
            // Try to fetch Certificate from key store
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(null);
            certificate = keyStore.getCertificate(CERTIFICATE_ALIAS);

            // Save certificate if it's not stored already
            if (null == certificate) {
                CertificateFactory cf = CertificateFactory.getInstance("X.509");
                InputStream caInput = ctx.getAssets().open("Tarang-ssl.crt");
                try {
                    certificate = cf.generateCertificate(caInput);
                } finally {
                    caInput.close();
                }
                keyStore.setCertificateEntry(CERTIFICATE_ALIAS, certificate);
            }

            // Create a KeyManagerFactory that trusts the CAs in our KeyStore
            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            keyManagerFactory.init(keyStore, null);

            SSLContext context = SSLContext.getInstance("TLS");
            context.init(keyManagerFactory.getKeyManagers(), new TrustManager[]{tm}, null);

            // Tell the URLConnection to use a SocketFactory from our SSLContext
            urlConnection = getUrlConnection(context);

            // Write Request Body
            os = new DataOutputStream(urlConnection.getOutputStream());
            String writeData = getJSONString();
            if (writeData == null) {
                return "SC_NO_CONTENT";
            }
            os.writeBytes(writeData);
            os.flush();

            // Read Response
            StringBuilder sb = new StringBuilder();
            br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), CHAR_SET));
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();
        } catch (IOException e) {
            return "SC_GATEWAY_TIMEOUT";
        } catch (Exception e) {
            return "SC_NOT_FOUND";
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }

            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    Log.d("Exception :", e.getMessage(), e);
                }
            }

            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Log.d("Exception :", e.getMessage(), e);
                }
            }
        }
    }

    private String getJSONString() {
        if (params == null) {
            return null;
        }

        JSONObject json = new JSONObject();

        try {
            for (Object o : params.entrySet()) {
                HashMap.Entry pair = (HashMap.Entry) o;
                json.put((String) pair.getKey(), pair.getValue());
            }
        } catch (org.json.JSONException e) {
            Log.d("Exception :", e.getMessage(), e);
        }

        return json.toString();
    }

    public void wipe() {
        params = null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[POST] [").append(url.toString()).append("]");
        try {
            sb.append("\n  [").append(URLEncoder.encode(getJSONString(), "UTF-8"));
        } catch (IOException e) {
            Log.d("Exception :", e.getMessage(), e);
            return "";
        }
        return sb.toString();
    }

}
