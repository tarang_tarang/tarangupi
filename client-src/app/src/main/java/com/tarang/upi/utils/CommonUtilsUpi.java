package com.tarang.upi.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.util.InetAddressUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.URL;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.List;

//import org.apache.http.HttpEntity;

public class CommonUtilsUpi {

	AppLocationService appLocationService;
	String locationAddress;
	String addressLine = "";
	static String Lttde, Lngtde;
	static Certificate ca;
	static KeyStore keyStore;

	public static String createHttpJsonRequest(String WEB_SERVER_URL,
			String jsonString) {

		StringBuilder sb = new StringBuilder();
		HttpURLConnection urlConnection = null;

		try {
			URL url = new URL(WEB_SERVER_URL);
			urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setDoOutput(true);
			urlConnection.setRequestMethod("POST");
			urlConnection.setUseCaches(false);
			urlConnection.setConnectTimeout(100000);
			urlConnection.setReadTimeout(100000);
			urlConnection
					.setRequestProperty("Content-Type", "application/json");

			urlConnection.connect();

			// Send POST output.
			DataOutputStream printout = new DataOutputStream(
					urlConnection.getOutputStream());

			String str = jsonString.toString();
			byte[] data = str.getBytes("UTF-8");
			printout.write(data);
			printout.flush();
			printout.close();

			int HttpResult = urlConnection.getResponseCode();
			if (HttpResult == HttpURLConnection.HTTP_OK) {
				BufferedReader br = new BufferedReader(new InputStreamReader(
						urlConnection.getInputStream(), "utf-8"));
				String line = null;
				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");
				}
				br.close();

				System.out.println("" + sb.toString());

				return sb.toString();

			} else {
				System.out.println(urlConnection.getResponseMessage());
				return urlConnection.getResponseMessage();
			}
		} catch (MalformedURLException e) {
			Log.d("Error Malformed URL excep: ", "" + e.getMessage());
		} catch (IOException e) {
			Log.d("IO excep: ", "" + e.getMessage());
			return null;
		} catch (Exception e) {
			Log.d(" excep: ", "" + e.getMessage());
		} finally {
			if (urlConnection != null)
				urlConnection.disconnect();
		}

		return null;
	}

	public static String createHttpsJsonRequest(String webServerUrl,
			String jsonString, Context context) {
		String errorResponseResult = "{'MessageType':'500','ResponseCode':'500','ResponseText':'"
				+ "','ResponseMessage':'";
		String responseResult = null;

		HttpClient client = getNewHttpsClient(context);

		HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); // Timeout

		HttpResponse response;
		try {
			HttpPost post = new HttpPost(webServerUrl);
			post.setHeader(HTTP.CONTENT_TYPE, "application/json");
			StringEntity se = new StringEntity(jsonString.toString());
			se.setContentEncoding("UTF-8");
			se.setContentType("application/json");
			post.setEntity(se);
			response = client.execute(post);

			// Checking response
			if (response != null) {
				HttpEntity entity = response.getEntity();
				responseResult = EntityUtils.toString(entity);

			}
		} catch (ConnectTimeoutException ex) {
			Log.d("OmnyPay", ex.getMessage());
			return errorResponseResult;

		} catch (Exception ex) {
			Log.d("OmnyPay", ex.getMessage());
			return errorResponseResult;
		}
		return responseResult;
	}

	public static Certificate getCertificateFatry(Context context) {
		if (ca == null) {
			CertificateFactory cf;
			try {
				cf = CertificateFactory.getInstance("X.509");
				InputStream caInput = context.getAssets()
						.open("Tarang-ssl.crt");
				try {
					ca = cf.generateCertificate(caInput);
				} finally {
					caInput.close();
				}
				String keyStoreType = KeyStore.getDefaultType();
				keyStore = KeyStore.getInstance(keyStoreType);
				keyStore.load(null, null);
			} catch (CertificateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// From
			// https://www.washington.edu/itconnect/security/ca/load-der.crt
			catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (KeyStoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return ca;

	}

	public static HttpClient getNewHttpsClient(Context context) {
		try {

			CertificateFactory cf= CertificateFactory.getInstance("X.509");;
			Certificate ca=null;
			KeyStore keyStore=null;
			try {
                AssetManager assManager = context.getAssets();
				//cf = CertificateFactory.getInstance("X.509");
				//InputStream caInput = context.getAssets().open("Tarang-ssl.crt");
                InputStream caInput = assManager.open("Tarang-ssl.crt");//new BufferedInputStream(new FileInputStream("Tarang-ssl.crt"));
				try {
					ca = cf.generateCertificate(caInput);
				} finally {
					caInput.close();
				}
				String keyStoreType = KeyStore.getDefaultType();
				keyStore = KeyStore.getInstance(keyStoreType);
				keyStore.load(null, null);
			} catch (CertificateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// From
			// https://www.washington.edu/itconnect/security/ca/load-der.crt
			catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (KeyStoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			keyStore.setCertificateEntry("ca", ca);

			SSLSocketFactory sf = new MySSLSocketFactory(keyStore);
			sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

			HttpParams params = new BasicHttpParams();
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

			SchemeRegistry registry = new SchemeRegistry();
			registry.register(new Scheme("http", PlainSocketFactory
					.getSocketFactory(), 80));
			registry.register(new Scheme("https", sf, 443));

			ClientConnectionManager ccm = new ThreadSafeClientConnManager(
					params, registry);

			return new DefaultHttpClient(ccm, params);
		} catch (Exception e) {
			Log.d("Upi", e.getMessage());
			return new DefaultHttpClient();
		}
	}

	public static boolean hasInternetConnectivity(Context context) {
		boolean hasConnectedWifi = false;
		boolean hasConnectedMobile = false;
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("wifi"))
				if (ni.isConnected())
					hasConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("mobile"))
				if (ni.isConnected())
					hasConnectedMobile = true;
		}
		return hasConnectedWifi || hasConnectedMobile;
	}

	public static void showToastMessage(Context context, String message,
			String position) {
		Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
		int gravityPosition = Gravity.BOTTOM;

		if (position.equalsIgnoreCase("top")) {
			gravityPosition = Gravity.TOP;
		} else if (position.equalsIgnoreCase("left")) {
			gravityPosition = Gravity.LEFT;
		} else if (position.equalsIgnoreCase("right")) {
			gravityPosition = Gravity.RIGHT;
		}
		toast.setGravity(gravityPosition, 0, 0);

		toast.show();
	}

	public static String getDeviceID(Context context) {
		TelephonyManager telephonyManager = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		return telephonyManager.getDeviceId();
	}

	public static String getDeviceIPAddress(boolean useIPv4) {
		try {
			List<NetworkInterface> networkInterfaces = Collections
					.list(NetworkInterface.getNetworkInterfaces());
			for (NetworkInterface networkInterface : networkInterfaces) {
				List<InetAddress> inetAddresses = Collections
						.list(networkInterface.getInetAddresses());
				for (InetAddress inetAddress : inetAddresses) {
					if (!inetAddress.isLoopbackAddress()) {
						String sAddr = inetAddress.getHostAddress()
								.toUpperCase();
						boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
						if (useIPv4) {
							if (isIPv4)
								return sAddr;
						} else {
							if (!isIPv4) {
								// drop ip6 port suffix
								int delim = sAddr.indexOf('%');
								return delim < 0 ? sAddr : sAddr.substring(0,
										delim);
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "";
	}

	public static String getOS() {

		String os = "Android " + Build.VERSION.RELEASE;
		return os;

	}

	public static JSONObject DeviceInfo(String mobile, String location,
			String ip, String type, String terminalId, String os, String app,
			String capability) {

		JSONObject Djobj = new JSONObject();
		try {
			double lalitutue = Double.parseDouble(Lttde);
			lalitutue = Double.parseDouble(new DecimalFormat("##.####")
					.format(lalitutue));
			double longitude = Double.parseDouble(Lngtde);
			longitude = Double.parseDouble(new DecimalFormat("##.####")
					.format(longitude));

			String geoCode = lalitutue + "," + longitude;
			Djobj.put("mobile", "91" + mobile);
			Djobj.put("geoCode", geoCode);
			Djobj.put("location", location);
			Djobj.put("ip", ip);
			Djobj.put("type", type);
			Djobj.put("terminalId", terminalId);
			Djobj.put("os", os);
			Djobj.put("app", app);
			Djobj.put("capability", capability);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return Djobj;
	}

	public String getAddress(Context contx) {
		if (IsLocationSettingsEnabled(contx)) {
			GPSTracker gpsTracker = new GPSTracker(contx);
			if (gpsTracker.canGetLocation()) {
				Lttde = String.valueOf(gpsTracker.latitude);
				Lngtde = String.valueOf(gpsTracker.longitude);
				addressLine = gpsTracker.getAddressLine(contx);
			} else {
				gpsTracker.showSettingsAlert();
			}

		} else {
			showSettingsAlert(contx);
		}
		return addressLine;

	}

	public boolean IsLocationSettingsEnabled(Context contx) {
		LocationManager locManager = (LocationManager) contx
				.getSystemService(Context.LOCATION_SERVICE);
		if (locManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			// GPS enabled
			return true;
		} else if (locManager
				.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
			return true;
		} else {
			// GPS disabled
			return false;
		}
	}

	public void showSettingsAlert(final Context contx) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(contx);
		alertDialog.setTitle("SETTINGS");
		alertDialog
				.setMessage("Enable Location Provider! Go to settings menu?");
		alertDialog.setPositiveButton("Settings",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						contx.startActivity(intent);
					}
				});
		alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		alertDialog.show();
	}

    public static boolean validateNumeric(String param) {
        String regex = "^[0-9]*$";
        return param.matches(regex);
    }
}
