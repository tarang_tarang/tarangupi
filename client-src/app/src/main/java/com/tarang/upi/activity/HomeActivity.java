package com.tarang.upi.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.tarang.upi.R;
import com.tarang.upi.adapter.NotificationListAdapter;
import com.tarang.upi.pojo.AccountDetail;
import com.tarang.upi.pojo.NotificationPOjo;
import com.tarang.upi.utils.CommonUtilsUpi;
import com.tarang.upi.utils.ServiceCallback;
import com.tarang.upi.utils.WebServiceCall;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.npci.upi.security.pinactivitycomponent.PinActivityComponent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HomeActivity extends Activity implements OnItemClickListener,
		OnClickListener {

	ArrayList<AccountDetail> bankAccountList;
	private BankAccountsAdapter bankAccAdapter;
	ListView bankAccountListView,list_notification;
	CommonUtilsUpi comUtils;
	WebServiceCall services;
	String location;
	TextView notification;
	LinearLayout ll_notifications,ll_notificationView,noti_bar;
	boolean isNotificationListVisible=false;
    ArrayList<NotificationPOjo> notiList;
    String txnId,dataFromPinActivity;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		findViewById(R.id.menu).setOnClickListener(this);
		bankAccountListView = (ListView) findViewById(R.id.accounts_list);
		bankAccountListView.setOnItemClickListener(this);

        getExistingAccList();

		list_notification = (ListView) findViewById(R.id.list_notification);
		
		notification=(TextView) findViewById(R.id.notification_txt);
		notification.setOnClickListener(this);
		ll_notifications=(LinearLayout) findViewById(R.id.ll_notifications);
		ll_notificationView=(LinearLayout) findViewById(R.id.ll_notification);
		//ll_notificationView.setVisibility(View.GONE);
        noti_bar=(LinearLayout)findViewById(R.id.ll_notification);
        //noti_bar.setVisibility(View.GONE);
		list_notification.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				//notificationDetailsPopUp();
                getNotificationSummary(notiList.get(position).getNotificationId());
			}
		});

        findViewById(R.id.refresh).setOnClickListener(this);
        txnId=constructMessageId();
	}

	private WebServiceCall getWebServiceCall() {
		if (services == null) {
			services = new WebServiceCall(this);
		}
		return services;
	}

	@Override
	protected void onStart() {
		super.onStart();
		location = getCommonUtils().getAddress(HomeActivity.this);
        getNotification();
	}

	private CommonUtilsUpi getCommonUtils() {
		if (comUtils == null) {
			comUtils = new CommonUtilsUpi();
		}
		return comUtils;
	}

	public class BankAccountsAdapter extends BaseAdapter {

		public BankAccountsAdapter(Context context, int resource,
				List<AccountDetail> list) {

		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) HomeActivity.this
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (convertView == null)
				convertView = inflater.inflate(R.layout.account_adapter,
						parent, false);

			AccountDetail accountDetail = bankAccountList.get(position);
			TextView accountType = (TextView) convertView
					.findViewById(R.id.account_type);
			TextView accountNum = (TextView) convertView
					.findViewById(R.id.account_no);
			TextView account_nname = (TextView) convertView
					.findViewById(R.id.account_nname);
			TextView account_virtual_adde = (TextView) convertView
					.findViewById(R.id.account_virtual_adde);
			accountType.setText(accountDetail.getBankType());
			account_nname.setText("Name : " + accountDetail.getName());
			accountNum.setText("A/C No. " + accountDetail.getAccountNo());
			account_virtual_adde.setText("Address : "
					+ accountDetail.getVirtualAddress());
			return convertView;
		}

		@Override
		public int getCount() {

			return bankAccountList.size();
		}

		@Override
		public Object getItem(int position) {

			return null;
		}

		@Override
		public long getItemId(int position) {

			return position;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		paymentOptionPopUp(position);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.menu:
			menuPopup();
			break;
		case R.id.notification_txt:
			if(isNotificationListVisible){
				ll_notifications.setVisibility(View.GONE);
				isNotificationListVisible=false;
			} else {
				ll_notifications.setVisibility(View.VISIBLE);
                isNotificationListVisible=true;
			}
			break;
            case R.id.refresh:
                getNotification();
                break;
		default:
			break;
		}
	}

	public void menuPopup() {

		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setContentView(R.layout.menu_popup);

		dialog.findViewById(R.id.txthistory).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent lIntent = new Intent(HomeActivity.this,
								TransactinHistoryActivity.class);
						startActivity(lIntent);
						dialog.dismiss();
					}
				});
		dialog.findViewById(R.id.txtAdd).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent lIntent = new Intent(HomeActivity.this,
								SelectBankActivity.class);
						startActivity(lIntent);
						//finish();
						dialog.dismiss();
					}
				});
		dialog.findViewById(R.id.txtlogout).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						logOut();
						dialog.dismiss();
					}
				});

        dialog.findViewById(R.id.txtget_notification).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
               getNotification();
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.txtchange).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent lIntent = new Intent(HomeActivity.this,
                        ChangePasswordActivity.class);
                startActivity(lIntent);
                //finish();
            }
        });
		Window window = dialog.getWindow();
		WindowManager.LayoutParams wlp = window.getAttributes();
		wlp.gravity = Gravity.TOP | Gravity.RIGHT;
		wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		window.setAttributes(wlp);
		dialog.show();
	}

	public void getExistingAccList() {

		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		String moblNum = prefs.getString(UPIMacros.PREF_MOBILE_NUM, null);
		String userName = prefs.getString(UPIMacros.PREF_USER_NAME, null);
		JSONObject jobj = new JSONObject();

		try {
			jobj.put("userName", userName);

			JSONObject DObj = CommonUtilsUpi.DeviceInfo(moblNum, location,
					CommonUtilsUpi.getDeviceIPAddress(true), UPIMacros.TYPE,
					CommonUtilsUpi.getDeviceID(HomeActivity.this),
					CommonUtilsUpi.getOS(), UPIMacros.APP_NAME,
					UPIMacros.CAPABILITY);

			jobj.put("device", DObj);

			System.out.println("Request---" + jobj.toString());
			System.out.println("URL --- " + UPIMacros.EXIST_ACC_LIST);
			getWebServiceCall().triggerWebServiceCall(UPIMacros.EXIST_ACC_LIST,
					jobj.toString(), new ServiceCallback() {

						@Override
						public void SuccessCallbak(String resp) {
                            System.out.println("Result---" + resp);
                            JSONObject jobj= null;
                            try {
                                jobj = new JSONObject(resp);
                                if(jobj.getInt("statusCode")==50114) {
                                    Toast.makeText(HomeActivity.this, "Session expired, please login again.", Toast.LENGTH_LONG).show();
                                    Intent intent=new Intent(HomeActivity.this,LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                }else {
                                    System.out.println("Result---" + resp);
                                    bankAccountList = parseAcctOtpResp(resp);
                                    populateAccountLists(bankAccountList);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
						}

						@Override
						public void ErrorCallbak(String resp) {

						}
					}, "");
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<AccountDetail> parseAcctOtpResp(String resp) {
		ArrayList<AccountDetail> accList = new ArrayList<AccountDetail>();
		try {
			JSONObject jobj = new JSONObject(resp);

			AccountDetail accDetail = new AccountDetail();
			accDetail.setAccountNo(jobj.getJSONObject("accountSummary")
					.getString("maskedAccnumber"));
			accDetail.setAeba(jobj.getJSONObject("accountSummary").getString(
					"aeba"));
			accDetail.setBankType("BankType");
			accDetail.setVirtualAddress(jobj.getJSONObject("accountSummary")
					.getString("virtualAddress"));
            UPIMacros.PAYER_VIRTUAL_ADDRESS=jobj.getJSONObject("accountSummary")
                    .getString("virtualAddress");
			try {
				accDetail.setId(jobj.getJSONObject("accountSummary").getInt(
						"id"));
			} catch (Exception e) {
				accDetail.setId(1);
			}
			accDetail.setIFSCCode(jobj.getJSONObject("accountSummary")
					.getString("ifsc"));
			accDetail.setName(jobj.getJSONObject("accountSummary").getString(
					"name"));
			accList.add(accDetail);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return accList;
	}

	private void populateAccountLists(ArrayList<AccountDetail> accountList) {
		bankAccAdapter = new BankAccountsAdapter(this,
				R.layout.account_adapter, bankAccountList);
		bankAccountListView.setAdapter(bankAccAdapter);
		bankAccAdapter.notifyDataSetChanged();
	}

	public void logOut() {

		JSONObject jobj = new JSONObject();
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		String moblNum = prefs.getString(UPIMacros.PREF_MOBILE_NUM, null);
		String userName = prefs.getString(UPIMacros.PREF_USER_NAME, null);
		JSONObject DObj = CommonUtilsUpi.DeviceInfo(moblNum, location,
				CommonUtilsUpi.getDeviceIPAddress(true), UPIMacros.TYPE,
				CommonUtilsUpi.getDeviceID(HomeActivity.this),
				CommonUtilsUpi.getOS(), UPIMacros.APP_NAME,
				UPIMacros.CAPABILITY);

		try {
			jobj.put("userName", userName);
			jobj.put("device", DObj);
			System.out.println("Request---" + jobj.toString());

			getWebServiceCall().triggerWebServiceCall(UPIMacros.LOGOUT,
					jobj.toString(), new ServiceCallback() {

						@Override
						public void SuccessCallbak(String resp) {
							System.out.println("Response---" + resp);
							if (resp != null) {
								try {
									JSONObject jobj = new JSONObject(resp);
									if (jobj.getInt("statusCode") == 200) {
										Intent intent = new Intent(
												HomeActivity.this,
												LoginActivity.class);
										startActivity(intent);
										finish();
									} else if(jobj.getInt("statusCode")==50114) {
                                        Toast.makeText(HomeActivity.this, "Session expired, please login again.", Toast.LENGTH_LONG).show();
                                        Intent intent=new Intent(HomeActivity.this,LoginActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
						}

						@Override
						public void ErrorCallbak(String resp) {

						}
					}, "");
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public void paymentOptionPopUp(final int position) {
		
		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setContentView(R.layout.make_collect);
		Window window = dialog.getWindow();
		WindowManager.LayoutParams wlp = window.getAttributes();
		wlp.gravity = Gravity.CENTER;
		dialog.show();
		
		Button make_payment,collect_payment,make_cancel,balance;
		make_cancel=(Button) dialog.findViewById(R.id.make_cancel);
		make_payment=(Button) dialog.findViewById(R.id.make_payment);
        balance=(Button) dialog.findViewById(R.id.balance);
		collect_payment=(Button) dialog.findViewById(R.id.collect_payment);
		
		make_cancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		
		make_payment.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
                dialog.dismiss();
				Intent lIntent = new Intent(HomeActivity.this, TransactionTypeActivity.class);
				lIntent.putExtra("acc_detail", bankAccountList.get(position));
				UPIMacros.PAYER_VIRTUAL_ADDRESS = bankAccountList.get(position)
						.getVirtualAddress();
				startActivity(lIntent);
			}
		});
		
		collect_payment.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
                dialog.dismiss();
				Intent lIntent = new Intent(HomeActivity.this, CollectPaymentActivity.class);
				startActivity(lIntent);
			}
		});

        balance.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                getBalanceCredential();
            }
        });
	}
	
	public void PopulateNotificationList(ArrayList<NotificationPOjo> nlist) {

		NotificationListAdapter adapter=new NotificationListAdapter(HomeActivity.this, nlist);
		list_notification.setAdapter(adapter);
		adapter.notifyDataSetChanged();
	}
	
	public void notificationDetailsPopUp(final String name,final String noti_desc,final String noti_payeeeee_virtual_address,
                                         final String noti_amount,final String noti_payer_virtual_address,final String noti_txnId
                                           ,final String notificationId) {

			final Dialog dialog = new Dialog(this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawable(
					new ColorDrawable(android.graphics.Color.TRANSPARENT));
			dialog.setContentView(R.layout.notification_details_popup);
			Button cancel=(Button) dialog.findViewById(R.id.noti_cancel);
			Button pay=(Button) dialog.findViewById(R.id.noti_Pay);
			Window window = dialog.getWindow();
			WindowManager.LayoutParams wlp = window.getAttributes();
			wlp.gravity = Gravity.CENTER;

        TextView txt_noti_name,txt_noti_payeeeee_virtual_address,txt_noti_amount,txt_noti_payer_virtual_address
                ,txt_noti_desc;
        txt_noti_desc= (TextView) dialog.findViewById(R.id.noti_desc);
        txt_noti_name= (TextView) dialog.findViewById(R.id.noti_name);
        txt_noti_payeeeee_virtual_address= (TextView) dialog.findViewById(R.id.noti_payeeeee_virtual_address);
        txt_noti_amount= (TextView) dialog.findViewById(R.id.noti_amount);
        txt_noti_payer_virtual_address= (TextView) dialog.findViewById(R.id.noti_payer_virtual_address);

                txt_noti_desc.setText(noti_desc);
                txt_noti_amount.setText("Amount : "+noti_amount);
                txt_noti_name.setText("Payee Name : "+name);
                txt_noti_payeeeee_virtual_address.setText("Payee Address : "+noti_payeeeee_virtual_address);
                txt_noti_payer_virtual_address.setText("Payer Address : "+noti_payer_virtual_address);

        cancel.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					rejectNotification(notificationId);
					dialog.dismiss();
				}
			});
			pay.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
                    dialog.dismiss();
					Intent intent=new Intent(HomeActivity.this,PaymentActivity.class);
                    intent.putExtra("call_from","Notitficatoin_Collect");
                    intent.putExtra("nick",name);
                    intent.putExtra("txnId",noti_txnId);
                    intent.putExtra("noti_amount",noti_amount);
                    intent.putExtra("notificationId",notificationId);
                    intent.putExtra("payee_virtual_add",noti_payeeeee_virtual_address);
                    intent.putExtra("payeerrrrr_virtual_add",noti_payer_virtual_address);
					startActivity(intent);
					finish();
				}
			});
			dialog.show();
	}
	
	public void rejectNotification(final String notificationId) {

		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setContentView(R.layout.notification_reject);
		Button reject=(Button) dialog.findViewById(R.id.noti_rej_cancel);
		Button cancel=(Button) dialog.findViewById(R.id.noti_later);
		Window window = dialog.getWindow();
		WindowManager.LayoutParams wlp = window.getAttributes();
		wlp.gravity = Gravity.CENTER;

       final EditText noti_reject_desc= (EditText) dialog.findViewById(R.id.noti_reject_desc);
		reject.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

                if(noti_reject_desc.getText().toString().isEmpty()) {
                    Toast.makeText(HomeActivity.this,"Please enter some reason to reject.",Toast.LENGTH_LONG).show();
                } else {
                    dialog.dismiss();
                    getRejectServerHit(noti_reject_desc.getText().toString(),notificationId) ;
                }
			}
		});
		cancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.show();
	}

    public void getNotification() {

        notiList=new ArrayList<NotificationPOjo>();
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        String moblNum = prefs.getString(UPIMacros.PREF_MOBILE_NUM, null);
        String userName = prefs.getString(UPIMacros.PREF_USER_NAME, null);

        JSONObject jobj=new JSONObject();
        try {
            JSONObject DObj = CommonUtilsUpi.DeviceInfo(moblNum, location,
                    CommonUtilsUpi.getDeviceIPAddress(true), UPIMacros.TYPE,
                    CommonUtilsUpi.getDeviceID(HomeActivity.this),
                    CommonUtilsUpi.getOS(), UPIMacros.APP_NAME,
                    UPIMacros.CAPABILITY);

            jobj.put("userName",userName);
            jobj.put("device",DObj);

            System.out.println("Request -- "+jobj.toString());
            System.out.println("Request -- "+UPIMacros.GET_NOTIFICATION);
            getWebServiceCall().triggerWebServiceCall(UPIMacros.GET_NOTIFICATION,jobj.toString(),new ServiceCallback() {
                @Override
                public void SuccessCallbak(String resp) {
                    System.out.println("Response -- "+resp);
                    try {
                        JSONObject jobj=new JSONObject(resp);
                        if(jobj.getInt("statusCode")==200) {
                            if(jobj.getJSONArray("notifications").length()==0) {
                                ll_notificationView.setVisibility(View.GONE);
                            } else {
                                ll_notificationView.setVisibility(View.VISIBLE);
                                for(int i=0;i<jobj.getJSONArray("notifications").length();i++) {
                                    NotificationPOjo pojo =new NotificationPOjo();
                                    pojo.setNotificationId(jobj.getJSONArray("notifications").getJSONObject(i).getString("notificationId"));
                                    pojo.setNotificationMessage(jobj.getJSONArray("notifications").getJSONObject(i).getString("notificationMessage"));
                                    pojo.setNotificationtType(jobj.getJSONArray("notifications").getJSONObject(i).getString("notificationtType"));

                                    notiList.add(pojo);
                                }
                                notification.setText(notiList.size()+" notifications");
                                PopulateNotificationList(notiList);
                            }
                        }else if(jobj.getInt("statusCode")==50114) {
                            Toast.makeText(HomeActivity.this, "Session expired, please login again.", Toast.LENGTH_LONG).show();
                            Intent intent=new Intent(HomeActivity.this,LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void ErrorCallbak(String resp) {

                }
            },"");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public  void getNotificationSummary(String notificationId) {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        String moblNum = prefs.getString(UPIMacros.PREF_MOBILE_NUM, null);
        String userName = prefs.getString(UPIMacros.PREF_USER_NAME, null);

        JSONObject jobj=new JSONObject();

        JSONObject DObj = CommonUtilsUpi.DeviceInfo(moblNum, location,
                CommonUtilsUpi.getDeviceIPAddress(true), UPIMacros.TYPE,
                CommonUtilsUpi.getDeviceID(HomeActivity.this),
                CommonUtilsUpi.getOS(), UPIMacros.APP_NAME,
                UPIMacros.CAPABILITY);

        try {
            jobj.put("device",DObj);
            jobj.put("userName",userName);
            jobj.put("notificationId",notificationId);

            System.out.println("Request -- "+jobj.toString());
            getWebServiceCall().triggerWebServiceCall(UPIMacros.GET_NOTIFICATION_SUMMARY,jobj.toString(),new ServiceCallback() {
                @Override
                public void SuccessCallbak(String resp) {
                    System.out.println(resp);
                    System.out.println(resp);
                    String name="",payee_vritual_address="",noti_desc="",noti_amount="",noti_payer_virtual_address="",txnid=""
                            ,notificationId="";
                    try {
                        JSONObject jobj=new JSONObject(resp);
                        if(jobj.getInt("statusCode")==200) {

                            name=jobj.getJSONObject("notification").getString("requestorName");
                            payee_vritual_address=jobj.getJSONObject("notification").getString("requestorVirtualAddress");
                            noti_desc=jobj.getJSONObject("notification").getString("requestorNote");
                            noti_amount=jobj.getJSONObject("notification").getString("amount");
                            noti_payer_virtual_address=jobj.getJSONObject("notification").getString("selfVitrualAddress");
                            txnid=jobj.getJSONObject("notification").getString("txnId");
                            notificationId=jobj.getJSONObject("notification").getString("notificationId");
                        }else if(jobj.getInt("statusCode")==50114) {
                            Toast.makeText(HomeActivity.this, "Session expired, please login again.", Toast.LENGTH_LONG).show();
                            Intent intent=new Intent(HomeActivity.this,LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    notificationDetailsPopUp(name,noti_desc,payee_vritual_address,noti_amount,noti_payer_virtual_address,txnid,notificationId);
                }

                @Override
                public void ErrorCallbak(String resp) {

                }
            },"");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getBalanceCredential() {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        String xmlPayloadString = prefs.getString(UPIMacros.PUBLIC_KEY, null);

        String credAllowedString = "{\n" + "\t\"CredAllowed\": [{\n"
                + "\t\t\"type\": \"PIN\",\n" + "\t\t\"subtype\": \"MPIN\",\n"
                + "\t\t\"dType\": \"ALPH | NUM\",\n" + "\t\t\"dLength\": 6\n" +
                "\t}]\n" + "}";

        Intent intent = new Intent(HomeActivity.this,
                PinActivityComponent.class);

        // Create Keycode
        intent.putExtra("keyCode", "NPCI");

        // Create xml payload
        if (xmlPayloadString.isEmpty()) {
            Toast.makeText(HomeActivity.this,
                    "XML List Key API is not loaded.",
                    Toast.LENGTH_LONG).show();
            return;
        }
        intent.putExtra("keyXmlPayload", xmlPayloadString); // It will

        // Create Controls
        if (credAllowedString.isEmpty()) {
            Toast.makeText(HomeActivity.this,
                    "Required Credentials could not be loaded.",
                    Toast.LENGTH_LONG).show();
            return;
        }
        intent.putExtra("controls", credAllowedString);

        // Create Configuration
        try {
            JSONObject configuration = new JSONObject();
            configuration.put("payerBankName", "Tarang Software Pvt Ltd.");
            configuration.put("backgroundColor", "#FFFFFF");
            Log.i("configuration", configuration.toString());
            intent.putExtra("configuration", configuration.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Create Salt
        try {
            JSONObject salt = new JSONObject();
            salt.put("txnId", txnId);
            //salt.put("txnAmount", "200");
            salt.put("deviceId", CommonUtilsUpi.getDeviceID(HomeActivity.this));
            salt.put("appId", UPIMacros.APP_ID);
            Log.i("salt", salt.toString());
            intent.putExtra("salt", salt.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Create Pay Info
        JSONArray payInfoArray = new JSONArray();
        try {
			/*JSONObject jsonPayeeName = new JSONObject();
			jsonPayeeName.put("name", "payeeName");
			jsonPayeeName.put("value", "John Smith");
			payInfoArray.put(jsonPayeeName);

			JSONObject jsonNote = new JSONObject();
			jsonNote.put("name", "note");
			jsonNote.put("value", "Pay restaurant bill");
			payInfoArray.put(jsonNote);

			JSONObject jsonRefId = new JSONObject();
			jsonRefId.put("name", "refId");
			jsonRefId.put("value", "589430957393");
			payInfoArray.put(jsonRefId);

			JSONObject jsonRefUrl = new JSONObject();
			jsonRefUrl.put("name", "refUrl");
			jsonRefUrl.put("value", "https://indianbank.com");
			payInfoArray.put(jsonRefUrl);

			JSONObject jsonAccount = new JSONObject();
			jsonAccount.put("name", "account");
			jsonAccount.put("value", "122XXX423");
			payInfoArray.put(jsonAccount);*/

            Log.i("payInfo", payInfoArray.toString());
            intent.putExtra("payInfo", payInfoArray.toString());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        // Create Language Pref
        intent.putExtra("languagePref", "En_US");

        startActivityForResult(intent, 1);
    }

    public static String constructMessageId(){
        return "TARANG" + String.valueOf(System.currentTimeMillis());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("Result Code:", String.valueOf(resultCode));
        if (requestCode == 1 && resultCode == Activity.RESULT_OK
                && data != null) {
            String errorMsgStr = data.getStringExtra("error");
            if (errorMsgStr != null && !errorMsgStr.isEmpty()) {
                Log.d("Error:", errorMsgStr);
                try {
                    JSONObject error = new JSONObject(errorMsgStr);
                    String errorCode = error.getString("errorCode");
                    String errorText = error.getString("errorText");
                    Toast.makeText(HomeActivity.this,
                            errorCode + ":" + errorText, Toast.LENGTH_LONG)
                            .show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return;
            }
            HashMap<String, String> credListHashMap = (HashMap<String, String>) data
                    .getSerializableExtra("credBlocks");
            for (String cred : credListHashMap.keySet()) { // This will return
                // the list of field
                // name e.g mpin,otp
                // etc...
                try {
                    JSONObject credBlock = new JSONObject(
                            credListHashMap.get(cred));
                    dataFromPinActivity=credBlock.getJSONObject("data").getString("encryptedBase64String");
                    Log.i("enc_msg", credBlock.toString());
                    Log.i("enc_msg",dataFromPinActivity);
                    // Log.i("enc_msg", credBlock.getString("message"));
                    //Toast.makeText(HomeActivity.this,"Balance Npci",Toast.LENGTH_LONG).show();
                    getBalanceServerHit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public void getBalanceServerHit() {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        String moblNum = prefs.getString(UPIMacros.PREF_MOBILE_NUM, null);
        String userName = prefs.getString(UPIMacros.PREF_USER_NAME, null);

        JSONObject jobj=new JSONObject();

        JSONObject DObj = CommonUtilsUpi.DeviceInfo(moblNum, location,
                CommonUtilsUpi.getDeviceIPAddress(true), UPIMacros.TYPE,
                CommonUtilsUpi.getDeviceID(HomeActivity.this),
                CommonUtilsUpi.getOS(), UPIMacros.APP_NAME,
                UPIMacros.CAPABILITY);

        try {
            jobj.put("device",DObj);
            jobj.put("userName",userName);
            jobj.put("txnId",txnId);

            JSONObject mobileCredentials = new JSONObject();
            mobileCredentials.put("dataValue", dataFromPinActivity);

            jobj.put("mobileCredentials", mobileCredentials);

            JSONObject acDetails=new JSONObject();
            acDetails.put("addrType","ACCOUNT");
            acDetails.put("ifsc","AACA1234567");
            acDetails.put("actype","SAVINGS");
            acDetails.put("acnum",bankAccountList.get(0).getAccountNo());//bankAccountList.get(0).ge);psdsd

            jobj.put("acDetails", acDetails);

            System.out.println("Request -- "+jobj.toString());
            getWebServiceCall().triggerWebServiceCall(UPIMacros.BALANCE_ENQ,jobj.toString(),new ServiceCallback() {
                @Override
                public void SuccessCallbak(String resp) throws JSONException {
                    System.out.println(resp);
                    JSONObject jobj=new JSONObject(resp);
                    if(jobj.getInt("statusCode")==50124) {
                      balancePopUp(jobj.getString("balance"));
                    }else if(jobj.getInt("statusCode")==50114) {
                        Toast.makeText(HomeActivity.this, "Session expired, please login again.", Toast.LENGTH_LONG).show();
                        Intent intent=new Intent(HomeActivity.this,LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }

                @Override
                public void ErrorCallbak(String resp) {

                }
            },"");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void balancePopUp(String balance) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.generate_pin_popup);

        dialog.setCancelable(false);
        Button generate_btn=(Button) dialog.findViewById(R.id.generate_btn);
        TextView detail_txt= (TextView) dialog.findViewById(R.id.detail_txt);
        TextView title_merchant= (TextView) dialog.findViewById(R.id.title_merchant);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;

        title_merchant.setText("Current Balance");
        generate_btn.setText("Done");
        detail_txt.setText("Current Balance : "+balance);
        generate_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //getGeneratePinCredential();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

     public void  getRejectServerHit(String reason,String notificationId) {

             SharedPreferences prefs = PreferenceManager
                     .getDefaultSharedPreferences(getApplicationContext());
             String moblNum = prefs.getString(UPIMacros.PREF_MOBILE_NUM, null);
             String userName = prefs.getString(UPIMacros.PREF_USER_NAME, null);

             JSONObject jobj = new JSONObject();
             try {
                 jobj.put("userName", userName);
                 jobj.put("rejected", "true");
                 jobj.put("note", reason);

                 JSONObject DObj = CommonUtilsUpi.DeviceInfo(moblNum,
                         location, CommonUtilsUpi.getDeviceIPAddress(true),
                         UPIMacros.TYPE,
                         CommonUtilsUpi.getDeviceID(HomeActivity.this),
                         CommonUtilsUpi.getOS(), UPIMacros.APP_NAME,
                         UPIMacros.CAPABILITY);

                 jobj.put("device", DObj);
                 jobj.put("notificationId", notificationId);

                 System.out.println("Request ---"+jobj.toString());
                 System.out.println("URL ---"+UPIMacros.COLLECT_PAYMENT_CONFIRM_NOTIFICATION);
                 getWebServiceCall().triggerWebServiceCall(UPIMacros.COLLECT_PAYMENT_CONFIRM_NOTIFICATION,
                         jobj.toString(), new ServiceCallback() {

                             @Override
                             public void SuccessCallbak(String resp) {
                                 System.out.println("Response ---"+resp);
                                 try {
                                     JSONObject jobj=new JSONObject(resp);
                                     if(jobj.getInt("statusCode")==50137) {
                                         ll_notifications.setVisibility(View.GONE);
                                         getNotification();
                                         Toast.makeText(HomeActivity.this, "Success", Toast.LENGTH_LONG).show();

                                     }else if(jobj.getInt("statusCode")==50114) {
                                         Toast.makeText(HomeActivity.this, "Session expired, please login again.", Toast.LENGTH_LONG).show();
                                         Intent intent=new Intent(HomeActivity.this,LoginActivity.class);
                                         startActivity(intent);
                                         finish();
                                     }
                                     else {
                                         Toast.makeText(HomeActivity.this, "Internal server error", Toast.LENGTH_LONG).show();
                                     }
                                 } catch (JSONException e) {
                                     e.printStackTrace();
                                 }

                             }

                             @Override
                             public void ErrorCallbak(String resp) {

                             }
                         }, "");
             } catch (JSONException e) {
                 e.printStackTrace();
             }
    }
}
