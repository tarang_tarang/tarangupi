package com.tarang.upi.pojo;

public class TransactionDetail {

	/*"paymentFrom": "pawank1@tarang",
    "paymentTo": "",
    "amount": 200,
    "transactionDate": 1457447122744,
    "status": "COMPLETED",
    "txnMessage": "payment"*/
	private String paymentFrom;
	public String getPaymentFrom() {
		return paymentFrom;
	}
	public void setPaymentFrom(String paymentFrom) {
		this.paymentFrom = paymentFrom;
	}
	public String getPaymentTo() {
		return paymentTo;
	}
	public void setPaymentTo(String paymentTo) {
		this.paymentTo = paymentTo;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTxnMessage() {
		return txnMessage;
	}
	public void setTxnMessage(String txnMessage) {
		this.txnMessage = txnMessage;
	}
	private String paymentTo;
	private Double amount;
	private String status;
	private String transactionDate;
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	private String txnMessage;
	
}
