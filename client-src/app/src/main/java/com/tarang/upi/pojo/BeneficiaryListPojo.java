package com.tarang.upi.pojo;

public class BeneficiaryListPojo {
	String benId;
	public String getBenId() {
		return benId;
	}
	public void setBenId(String benId) {
		this.benId = benId;
	}
	public String getBenName() {
		return benName;
	}
	public void setBenName(String benName) {
		this.benName = benName;
	}
	public String getBenNickName() {
		return benNickName;
	}
	public void setBenNickName(String benNickName) {
		this.benNickName = benNickName;
	}
	public String getBenVirtualAddr() {
		return benVirtualAddr;
	}
	public void setBenVirtualAddr(String benVirtualAddr) {
		this.benVirtualAddr = benVirtualAddr;
	}
	String benName;
	String benNickName;
	String benVirtualAddr;
}
