package com.tarang.upi.activity;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tarang.upi.R;
import com.tarang.upi.pojo.AccountDetail;

public class TransactionActivity extends Activity implements OnClickListener {
	
	RelativeLayout collectPayLayout;
	RelativeLayout makePayLayout;
	TextView makePayTxt, collectPayTxt;
	AccountDetail account;
	String amount;
	boolean isCollect;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_transaction);
		findViewById(R.id.rl_collet_payment).setOnClickListener(this);
		findViewById(R.id.rr_make_payment).setOnClickListener(this);
		findViewById(R.id.submit).setOnClickListener(this);
		findViewById(R.id.submit_collect).setOnClickListener(this);
		makePayTxt = (TextView) findViewById(R.id.make_payment);
		collectPayTxt = (TextView) findViewById(R.id.collect_payment);
		account = (AccountDetail) getIntent().getSerializableExtra("acc_detail");
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.rl_collet_payment:
			findViewById(R.id.ll_makepayment_details).setVisibility(View.GONE);
			findViewById(R.id.ll_collect_payment).setVisibility(View.VISIBLE);
			findViewById(R.id.view_make_payment).setVisibility(View.INVISIBLE);
			findViewById(R.id.view_collect_payment).setVisibility(View.VISIBLE);
			collectPayTxt.setTextColor(getResources().getColor(R.color.white));
			makePayTxt.setTextColor(getResources().getColor(R.color.text_color));
			break;
		case R.id.rr_make_payment:
			findViewById(R.id.ll_makepayment_details).setVisibility(View.VISIBLE);
			findViewById(R.id.ll_collect_payment).setVisibility(View.GONE);
			findViewById(R.id.view_collect_payment).setVisibility(View.INVISIBLE);
			findViewById(R.id.view_make_payment).setVisibility(View.VISIBLE);
			collectPayTxt.setTextColor(getResources().getColor(R.color.text_color));
			makePayTxt.setTextColor(getResources().getColor(R.color.white));
			break;
			
		case R.id.submit:
			isCollect = false;
			EditText mobileText = (EditText) findViewById(R.id.edt_mobile_number);
			EditText adhaarText = (EditText) findViewById(R.id.edt_adhaar_number);
			EditText emailText = (EditText) findViewById(R.id.edt_email_id);
			EditText amountPay = (EditText) findViewById(R.id.edt_amount_pay);
			EditText pin = (EditText) findViewById(R.id.edt_pin);
			
			if(mobileText.getText().toString().isEmpty() && adhaarText.getText().toString().isEmpty() && 
					emailText.getText().toString().isEmpty()) {
				Toast.makeText(TransactionActivity.this, "Please enter mobile number or Adhaar number or email id",Toast.LENGTH_LONG).show();
			} else if(amountPay.getText().toString().isEmpty()) {
				Toast.makeText(TransactionActivity.this, "Please enter amount to pay",Toast.LENGTH_LONG).show();
			} else if(pin.getText().toString().isEmpty()) {
				Toast.makeText(TransactionActivity.this, "Please enter transaction PIN",Toast.LENGTH_LONG).show();
			} else {
				successPayment("Payment made successfully.");
			}
			break;
			
		case R.id.submit_collect:
			isCollect = true;
			EditText mobileTextCollect = (EditText) findViewById(R.id.edt_collect_mobile_number);
			EditText adhaarTextCollect = (EditText) findViewById(R.id.edt_collect_adhaar_number);
			EditText emailTextCollect = (EditText) findViewById(R.id.edt_collect_email_id);
			EditText amountCollect = (EditText) findViewById(R.id.edt_amount_collect);
			if(mobileTextCollect.getText().toString().isEmpty() && adhaarTextCollect.getText().toString().isEmpty() && 
					emailTextCollect.getText().toString().isEmpty()) {
				Toast.makeText(TransactionActivity.this, "Please enter mobile number or Adhaar number or email id",Toast.LENGTH_LONG).show();
			} else if(amountCollect.getText().toString().isEmpty()) {
				Toast.makeText(TransactionActivity.this, "Please enter amount to pay",Toast.LENGTH_LONG).show();
			} else {
				successPayment("Payment collected successfully.");
			}
			break;

		default:
			break;
		}
	}
	
	public void successPayment(String message) {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		final Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.otp_layout);
		final EditText otp = (EditText) dialog.findViewById(R.id.Opt);
		final TextView messageText = (TextView) dialog.findViewById(R.id.message);
		otp.setVisibility(View.GONE);
		messageText.setText(message);
		dialog.setTitle("Payment");

		Button dialogSubmit = (Button) dialog.findViewById(R.id.btnSubmit);
		dialogSubmit.setText("OK");
		dialogSubmit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
			}
		});
		dialog.show();
	  }
}
