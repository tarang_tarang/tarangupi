package com.tarang.upi.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tarang.upi.R;
import com.tarang.upi.utils.CommonUtilsUpi;
import com.tarang.upi.utils.ServiceCallback;
import com.tarang.upi.utils.WebServiceCall;

import org.json.JSONException;
import org.json.JSONObject;

public class ChangePasswordActivity extends Activity implements OnClickListener {

	TextView title;
	Button submit;
	EditText old_password, new_password;
	WebServiceCall services;
	String location;
	CommonUtilsUpi comUtils;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_change_password);

		title = (TextView) findViewById(R.id.title);
		new_password = (EditText) findViewById(R.id.new_password);
		old_password = (EditText) findViewById(R.id.old_password);
		submit=(Button) findViewById(R.id.submit);
		title.setText("Change Passward");
		submit.setOnClickListener(this);
	}

	@Override
	protected void onStart() {
		super.onStart();
		location = getCommonUtils().getAddress(ChangePasswordActivity.this);
	}

	private WebServiceCall getWebServiceCall() {
		if (services == null) {
			services = new WebServiceCall(this);
		}
		return services;
	}

	private CommonUtilsUpi getCommonUtils() {
		if (comUtils == null) {
			comUtils = new CommonUtilsUpi();
		}
		return comUtils;
	}

	public void changePassword() {

		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		String userName = prefs.getString(UPIMacros.PREF_USER_NAME, null);
		String moblNum = prefs.getString(UPIMacros.PREF_MOBILE_NUM, null);
		JSONObject jobj = new JSONObject();
		try {
			jobj.put("userName", userName);
			jobj.put("oldPassword", old_password.getText().toString());
			jobj.put("newPassword", new_password.getText().toString());

			JSONObject DObj = CommonUtilsUpi.DeviceInfo(moblNum,
					location, CommonUtilsUpi.getDeviceIPAddress(true),
					UPIMacros.TYPE,
					CommonUtilsUpi.getDeviceID(ChangePasswordActivity.this),
					CommonUtilsUpi.getOS(), UPIMacros.APP_NAME,
					UPIMacros.CAPABILITY);

			jobj.put("device", DObj);

			getWebServiceCall().triggerWebServiceCall(
					UPIMacros.CHANGE_PASSWORD, jobj.toString(),
					new ServiceCallback() {

						@Override
						public void SuccessCallbak(String resp) {
							if (resp != null) {
								try {
									JSONObject respJson = new JSONObject(resp);
									if (respJson.getInt("statusCode") == 200) {
                                        Intent intent=new Intent(ChangePasswordActivity.this,LoginActivity.class);
                                        startActivity(intent);
										finish();
										Toast.makeText(ChangePasswordActivity.this,
												"Password change successfuly",
												Toast.LENGTH_LONG).show();
									} else if (respJson.getInt("statusCode") == 50101) {
										Toast.makeText(ChangePasswordActivity.this,
												checkErrorMsg(resp),
												Toast.LENGTH_LONG).show();
									} else if(respJson.getInt("statusCode")==50114) {
                                        Toast.makeText(ChangePasswordActivity.this, "Session expired, please login again.", Toast.LENGTH_LONG).show();
                                        Intent intent=new Intent(ChangePasswordActivity.this,LoginActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
						}

						@Override
						public void ErrorCallbak(String resp) {

						}
					}, "");
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.submit:
			changePassword();
			break;

		default:
			break;
		}
	}
	
	public String checkErrorMsg(String resp) {

		try {
			JSONObject jobj = new JSONObject(resp);
			if (!jobj.getString("oldPasswordErrMsg").equals("null")) {
				return jobj.getString("oldPasswordErrMsg");
			} else if (!jobj.getString("newPasswordErrMsg").equals("null")) {
				return jobj.getString("newPasswordErrMsg");
			} 
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;

	}
}
