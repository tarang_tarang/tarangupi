package com.tarang.upi.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tarang.upi.R;
import com.tarang.upi.utils.CommonUtilsUpi;
import com.tarang.upi.utils.ServiceCallback;
import com.tarang.upi.utils.WebServiceCall;

import org.json.JSONException;
import org.json.JSONObject;

public class OtpActivity extends Activity {
	
	private String mobileNum;
	private TextView txtMobileNum;
	WebServiceCall services;
	String location;
	CommonUtilsUpi comUtils;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_otp);
		txtMobileNum = (TextView) findViewById(R.id.mobile_number);
		
		if (savedInstanceState == null && getIntent().getExtras() != null) {
			mobileNum = getIntent().getExtras().getString(UPIMacros.INTENT_MOBILE_NUM);
			txtMobileNum.setText("Mobile Number: +91 " + mobileNum);
		}
		findViewById(R.id.confirm).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String otpNo =  ((EditText)findViewById(R.id.edt_opt_no)).getText().toString();
				
				if (otpNo != null && otpNo.length() == 6) {
					SharedPreferences prefs = PreferenceManager
							.getDefaultSharedPreferences(getApplicationContext());
					SharedPreferences.Editor prefEditor = prefs.edit();
					prefEditor.putString(UPIMacros.PREF_MOBILE_NUM, mobileNum);
					prefEditor.commit();
					try {
						Thread.sleep(1500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					Intent lIntent = new Intent(OtpActivity.this, CreatePasswordActivity.class);
					startActivity(lIntent);
					finish();
				} else {
					Toast.makeText(OtpActivity.this, getString(R.string.validate_otp_no), Toast.LENGTH_LONG).show();
				}
			}
		});
		
		services = new WebServiceCall(this);
		comUtils = new CommonUtilsUpi();
	}

	@Override
	protected void onStart() {
		super.onStart();
		location = comUtils.getAddress(OtpActivity.this);
		otpRequest();
	}
	
	private void otpRequest() {
		JSONObject jobj = new JSONObject();
		try {
			SharedPreferences prefs = PreferenceManager
					.getDefaultSharedPreferences(getApplicationContext());
			String moblNum = prefs.getString(UPIMacros.PREF_MOBILE_NUM, null);
			String userName = prefs.getString(UPIMacros.PREF_USER_NAME, null);

			jobj.put("mmid", UPIMacros.MMID);
			jobj.put("userName", userName);
			JSONObject DObj = CommonUtilsUpi.DeviceInfo(moblNum,
					location, CommonUtilsUpi.getDeviceIPAddress(true),
					UPIMacros.TYPE, CommonUtilsUpi.getDeviceID(OtpActivity.this),
					CommonUtilsUpi.getOS(), UPIMacros.APP_NAME, "110110");

			jobj.put("device", DObj);

			services.triggerWebServiceCall(UPIMacros.LOGIN, jobj.toString(),
					new ServiceCallback() {

						@Override
						public void SuccessCallbak(String resp) {
							System.out.println(resp);
							Toast.makeText(OtpActivity.this, "success",
									Toast.LENGTH_LONG).show();
						}

						@Override
						public void ErrorCallbak(String resp) {

						}
					}, "");
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}
