package com.tarang.upi.pojo;

public class MerchantListPojo {

	/*"id": 1,
    "name": "Reliance",
    "nickName": "Rel",
    "virtualAddress": "Reliance@tarang",
    "description": "RelianceMart"*/
	int id;
	String name;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getVirtualAddress() {
		return virtualAddress;
	}
	public void setVirtualAddress(String virtualAddress) {
		this.virtualAddress = virtualAddress;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	String nickName;
	String virtualAddress;
	String description;
}
