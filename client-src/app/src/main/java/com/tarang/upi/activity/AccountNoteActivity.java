package com.tarang.upi.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tarang.upi.R;
import com.tarang.upi.utils.CommonUtilsUpi;
import com.tarang.upi.utils.ServiceCallback;
import com.tarang.upi.utils.WebServiceCall;

import org.json.JSONException;
import org.json.JSONObject;

public class AccountNoteActivity extends Activity implements OnClickListener {

	EditText nick_name, note, virtual_add;
	CommonUtilsUpi comUtils;
	WebServiceCall services;
	String location;
	TextView title, ben_name, ben_acc_num, ben_acc_ifsc;
	Bundle extra;
	String Gtype = "", Gvitual_address = "";
	int id_type;
	LinearLayout ll_beneficiary_detail;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_account_note);
		nick_name = (EditText) findViewById(R.id.nick_name);
		note = (EditText) findViewById(R.id.note);
		title = (TextView) findViewById(R.id.title);
		ben_name = (TextView) findViewById(R.id.ben_name);
		ben_acc_num = (TextView) findViewById(R.id.ben_acc_num);
		ben_acc_ifsc = (TextView) findViewById(R.id.ben_acc_ifsc);
		virtual_add = (EditText) findViewById(R.id.virtual_add);
		ll_beneficiary_detail = (LinearLayout) findViewById(R.id.ll_beneficiary_detail);

		findViewById(R.id.submit).setOnClickListener(this);
		title.setText("Beneficiary Detail");
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.submit:

			if (nick_name.getText().toString().isEmpty()) {
				nick_name.requestFocus();
				Toast.makeText(this, "Please enter Nick name.",
						Toast.LENGTH_LONG).show();
			} else if (!note.getText().toString().isEmpty() && note.getText().toString().length() <5) {
				Toast.makeText(this, "Note should be minimum of 5 letters.",
						Toast.LENGTH_LONG).show();
			}
			else {

				if (virtual_add.getVisibility() == View.VISIBLE
						&& virtual_add.getText().toString().trim().isEmpty()) {

					Toast.makeText(AccountNoteActivity.this,
							"Please enter vitrual address", Toast.LENGTH_LONG)
							.show();
					return;
				} else {
					Gvitual_address = virtual_add.getText().toString().trim();
				}
				addBeneficiary();
			}
			break;

		default:
			break;
		}
	}

	private WebServiceCall getWebServiceCall() {
		if (services == null) {
			services = new WebServiceCall(this);
		}
		return services;
	}

	@Override
	protected void onStart() {
		super.onStart();
		try {
			extra = getIntent().getExtras();

			if (extra.getString("call_from").equalsIgnoreCase(
					"MobileDetailForAccount")) {
				ben_name.setText("Name : " + extra.getString("name"));
				ben_acc_num.setText("Acc No. : " + extra.getString("acc_no"));
				ben_acc_ifsc.setText("IFSC : " + extra.getString("ifsc"));
				id_type = extra.getInt("id");
				Gtype = "MOBILE";
			} else if (extra.getString("call_from").equalsIgnoreCase(
					"virtual_address")) {
				ll_beneficiary_detail.setVisibility(View.GONE);
				virtual_add.setVisibility(View.VISIBLE);
				Gtype = "VIRTUAL_ADDRESS";
			} else if (extra.getString("call_from").equalsIgnoreCase(
					"AhaadharActivity")) {
				ben_name.setText("Name : " + extra.getString("name"));
				ben_acc_num.setText("Acc No. : " + extra.getString("acc_no"));
				ben_acc_ifsc.setText("IFSC : " + extra.getString("ifsc"));
				id_type = extra.getInt("id");
				Gtype = "AADHAAR";
			} else if (extra.getString("call_from").equalsIgnoreCase(
					"CollectPaymentActivity")) {
				ll_beneficiary_detail.setVisibility(View.GONE);
				virtual_add.setVisibility(View.VISIBLE);
				Gtype = "VIRTUAL_ADDRESS";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		location = getCommonUtils().getAddress(AccountNoteActivity.this);
	}

	private CommonUtilsUpi getCommonUtils() {
		if (comUtils == null) {
			comUtils = new CommonUtilsUpi();
		}
		return comUtils;
	}

	public void addBeneficiary() {

		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		String moblNum = prefs.getString(UPIMacros.PREF_MOBILE_NUM, null);
		String userName = prefs.getString(UPIMacros.PREF_USER_NAME, null);

		JSONObject jobj = new JSONObject();

		JSONObject DObj = CommonUtilsUpi.DeviceInfo(moblNum,
				location, CommonUtilsUpi.getDeviceIPAddress(true),
				UPIMacros.TYPE,
				CommonUtilsUpi.getDeviceID(AccountNoteActivity.this),
				CommonUtilsUpi.getOS(), UPIMacros.APP_NAME,
				UPIMacros.CAPABILITY);
		try {
			jobj.put("userName", userName);
			jobj.put("id", id_type);
			jobj.put("type", Gtype);
			jobj.put("address", Gvitual_address);
			jobj.put("nickName", nick_name.getText().toString());
			jobj.put("beneficiaryNote", note.getText().toString());
			jobj.put("device", DObj);

			System.out.println("Request -- " + jobj.toString());
			getWebServiceCall().triggerWebServiceCall(UPIMacros.ADDBENEFICIARY,
					jobj.toString(), new ServiceCallback() {

						@Override
						public void SuccessCallbak(String resp) {
							System.out.println("Response ---" + resp);
							try {
								JSONObject jobj=new JSONObject(resp);
								if(jobj.getInt("statusCode")==200) {
								Intent intent = new Intent(
										AccountNoteActivity.this,
										PaymentActivity.class);
								if (virtual_add.getVisibility() == View.GONE) {
									if (extra.getString("call_from")
											.equalsIgnoreCase("AhaadharActivity")) {
										intent.putExtra("call_from",
												"AccountNoteAdhaar");
										intent.putExtra("nick", nick_name.getText()
												.toString());
									} else if (extra.getString("call_from")
											.equalsIgnoreCase(
													"MobileDetailForAccount")) {
										intent.putExtra("call_from",
												"AccountNoteMobile");
										intent.putExtra("nick", nick_name.getText()
												.toString());
									}
								} else {
									
									if(extra.getString("call_from")
											.equalsIgnoreCase(
													"CollectPaymentActivity")) {
										intent.putExtra("call_from",
												"CollectPayToAccountNote");
										intent.putExtra("payee_virtual_add",
												Gvitual_address);
										intent.putExtra("nick", nick_name.getText()
												.toString());
									} else {
									intent.putExtra("call_from",
											"AccountNoteVirtual");
									intent.putExtra("payee_virtual_add",
											Gvitual_address);
									intent.putExtra("nick", nick_name.getText()
											.toString());
									}
								}
								startActivity(intent);
								finish();
								}  else if(jobj.getInt("statusCode")==50114) {
                                    Toast.makeText(AccountNoteActivity.this, "Session expired, please login again.", Toast.LENGTH_LONG).show();
                                    Intent intent=new Intent(AccountNoteActivity.this,LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                                else {
									Toast.makeText(AccountNoteActivity.this, "Try again",Toast.LENGTH_LONG).show();
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}

						@Override
						public void ErrorCallbak(String resp) {

						}
					}, "");
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}
