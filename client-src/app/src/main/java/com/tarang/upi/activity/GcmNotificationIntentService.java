package com.tarang.upi.activity;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.tarang.upi.R;

public class GcmNotificationIntentService extends IntentService {

	private NotificationManager mNotificationManager;
	NotificationCompat.Builder builder;
	Bundle extras;

	public GcmNotificationIntentService() {
		super("GcmIntentService");
	}

	Boolean push = true;
	String check;
	public static final String TAG = "GCMNotificationIntentService";

	@SuppressWarnings("deprecation")
	@Override
	protected void onHandleIntent(Intent intent) {
		extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

		String messageType = gcm.getMessageType(intent);
		System.out.println("Message type --->" + messageType);

		String[] strS;
		System.out.println("Message Received: " + extras.getString("message"));

		if (!extras.isEmpty()) {
			 if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
					.equals(messageType)) {

				try{
				if(!extras.getString("message").isEmpty()){
				  sendNotification(extras.getString("message"));
				  Log.i(TAG, "Received: " + extras.toString());
				  System.out.println("bundle text --->" + extras.toString());
				}
				}catch(Exception e){}
			}
		}
		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}

	private void sendNotification(String msg) {
		Log.d(TAG, "Preparing to send notification...: " + msg);
		
		Intent lIntent = new Intent(this, LoginActivity.class);
		lIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, lIntent, PendingIntent.FLAG_CANCEL_CURRENT);
		mNotificationManager = (NotificationManager) this
				.getSystemService(Context.NOTIFICATION_SERVICE);
		
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				this).setSmallIcon(R.drawable.ic_launcher)
				.setContentTitle(getString(R.string.app_name))
				.setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
				.setContentText(msg).setContentIntent(pendingIntent);

		AudioManager am = (AudioManager) this
				.getSystemService(Context.AUDIO_SERVICE);

		switch (am.getRingerMode()) {
		case AudioManager.RINGER_MODE_VIBRATE:
			mBuilder.setDefaults(Notification.DEFAULT_VIBRATE);

			break;
		case AudioManager.RINGER_MODE_NORMAL:
			mBuilder.setDefaults(Notification.DEFAULT_SOUND);
			break;
		default:
			mBuilder.setDefaults(Notification.DEFAULT_SOUND);
		}

		mBuilder.setContentIntent(pendingIntent);
		mBuilder.setAutoCancel(true);

//		mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
		mNotificationManager.notify(UPIMacros.NOTIFY_ID, mBuilder.build());
		PowerManager pm = (PowerManager) this
				.getSystemService(Context.POWER_SERVICE);
		WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
				| PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");
		wl.acquire(10000);
	}
}