package com.tarang.upi.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.tarang.upi.R;
import com.tarang.upi.utils.CommonUtilsUpi;
import com.tarang.upi.utils.ServiceCallback;
import com.tarang.upi.utils.WebServiceCall;

import org.json.JSONException;
import org.json.JSONObject;

public class AdhaarDetailActivity extends Activity implements OnClickListener {

	TextView title, adhaar_number, iin_number;
	WebServiceCall services;
	String location;
	CommonUtilsUpi comUtils;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mobile_mmid_details);

		title = (TextView) findViewById(R.id.title);
		title.setText("Adhaar Details");

		adhaar_number = (TextView) findViewById(R.id.mobile_number_acc);
		iin_number = (TextView) findViewById(R.id.mmid);

		adhaar_number.setHint("Enter Adhaar Number");
		iin_number.setHint("Enter IIN Number");
	}

	@Override
	protected void onStart() {
		super.onStart();
		location = getCommonUtils().getAddress(AdhaarDetailActivity.this);
	}

	private WebServiceCall getWebServiceCall() {
		if (services == null) {
			services = new WebServiceCall(this);
		}
		return services;
	}

	private CommonUtilsUpi getCommonUtils() {
		if (comUtils == null) {
			comUtils = new CommonUtilsUpi();
		}
		return comUtils;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.submit:

			if (adhaar_number.getText().toString().isEmpty()) {
				adhaar_number.requestFocus();
				Toast.makeText(this, "Please enter Adhaar number.",
						Toast.LENGTH_LONG).show();
			} else if (adhaar_number.getText().toString().length() < 12) {
				adhaar_number.requestFocus();
				Toast.makeText(this, "Please enter valid Adhaar number.",
						Toast.LENGTH_LONG).show();
			} else if (iin_number.getText().toString().isEmpty()) {
				iin_number.requestFocus();
				Toast.makeText(this, "Please enter MMID number.",
						Toast.LENGTH_LONG).show();
			} else if (iin_number.getText().toString().length() < 4) {
				iin_number.requestFocus();
				Toast.makeText(this, "Please enter valid MMID number.",
						Toast.LENGTH_LONG).show();
			} else {
				serverHit("AADHAAR", adhaar_number.getText().toString().trim(),
						iin_number.getText().toString().trim());
			}

			break;

		default:
			break;
		}
	}

	public void serverHit(String addressType, String uidnum, String iin) {

		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		String moblNum = prefs.getString(UPIMacros.PREF_MOBILE_NUM, null);
		JSONObject jobj = new JSONObject();
		try {
			jobj.put("addrType", addressType);
			jobj.put("uidnum", uidnum);
			jobj.put("iin", iin);

			JSONObject DObj = CommonUtilsUpi.DeviceInfo(moblNum, 
					location, CommonUtilsUpi.getDeviceIPAddress(true),
					UPIMacros.TYPE,
					CommonUtilsUpi.getDeviceID(AdhaarDetailActivity.this),
					CommonUtilsUpi.getOS(), UPIMacros.APP_NAME,
					UPIMacros.CAPABILITY);

			jobj.put("device", DObj);

			System.out.println("Request url --- " + UPIMacros.OTP);
			System.out.println("Request --- " + jobj.toString());
			getWebServiceCall().triggerWebServiceCall(UPIMacros.OTP,
					jobj.toString(), new ServiceCallback() {

						@Override
						public void SuccessCallbak(String resp) {
							System.out.println("Responce ---" + resp);
                            JSONObject jobj= null;
                            try {
                                jobj = new JSONObject(resp);
                                if (resp != null) {

                                } else if(jobj.getInt("statusCode")==50114) {
                                    Toast.makeText(AdhaarDetailActivity.this, "Session expired, please login again.", Toast.LENGTH_LONG).show();
                                    Intent intent=new Intent(AdhaarDetailActivity.this,LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
						}

						@Override
						public void ErrorCallbak(String resp) {

						}
					}, "");
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

}
