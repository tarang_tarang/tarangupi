package com.tarang.upi.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tarang.upi.R;
import com.tarang.upi.utils.CommonUtilsUpi;
import com.tarang.upi.utils.ServiceCallback;
import com.tarang.upi.utils.WebServiceCall;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.npci.upi.security.pinactivitycomponent.PinActivityComponent;

import java.util.HashMap;

public class GeneratePinActivity extends Activity {
	
	TextView title;
	WebServiceCall services;
	String location,dataFromPinActivity,txnId;
    Bundle extra;
    EditText expiry;
    EditText PIN;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.transactop_pin_popup);
		title = (TextView) findViewById(R.id.title);
		title.setText("Generate PIN");
		PIN = (EditText)findViewById(R.id.edt_card_num);
		expiry = (EditText)findViewById(R.id.edt_expy_date);
		
		findViewById(R.id.submit).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//pinPopup();
				if(PIN.getText().toString().trim().isEmpty() || PIN.getText().toString().trim().length()<14 ) {
					Toast.makeText(GeneratePinActivity.this, "Please enter valid card number minimum 14 digits.", Toast.LENGTH_SHORT).show();
				} else if (expiry.getText().toString().trim().isEmpty()) {
					Toast.makeText(GeneratePinActivity.this, "Please enter expiry date.", Toast.LENGTH_SHORT).show();
				}
				else {
                  //  generatePin();
                    getGeneratePinCredential();
				}
			}
		});

        TextWatcher expiryTextChang=new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

                if (expiry.getText().length() == 1) {
                    if (CommonUtilsUpi.validateNumeric(expiry.getText().toString())) {
                        if(Integer.parseInt(expiry.getText().toString()) > 1){
                            expiry.getText().insert(0, "0");
                        }
                    }
                    else{
                        expiry.getText().clear();
                    }

                } else if (expiry.getText().length() == 2) {
                    if(!CommonUtilsUpi.validateNumeric(expiry.getText().toString())){
                        expiry.getText().delete(1, 2);
                    }
                    else if (Integer.parseInt(expiry.getText().toString()) > 12) {
                        expiry.getText().delete(1, 2);
                    } else if (count != 0) {
                        expiry.append("/");
                    }

                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                //Do nothing
            }
        };

        expiry.addTextChangedListener(expiryTextChang);
        txnId=constructMessageId();
	}
	
	private WebServiceCall getWebServiceCall() {
		if (services == null) {
			services = new WebServiceCall(this);
		}
		return services;
	}
	
	private void setPinRequest(boolean isChangePIN, String oldPin, String newPin) {

		JSONObject jobj = new JSONObject();
		try {
			SharedPreferences prefs = PreferenceManager
					.getDefaultSharedPreferences(getApplicationContext());
			String moblNum = prefs.getString(UPIMacros.PREF_MOBILE_NUM, null);
			String userName = prefs.getString(UPIMacros.PREF_USER_NAME, null);

			jobj.put("userName", userName);
			
			JSONObject DObj = CommonUtilsUpi.DeviceInfo(moblNum,
					location, CommonUtilsUpi.getDeviceIPAddress(true),
					UPIMacros.TYPE,
					CommonUtilsUpi.getDeviceID(GeneratePinActivity.this),
					CommonUtilsUpi.getOS(), UPIMacros.APP_NAME,
					UPIMacros.CAPABILITY);

			JSONObject mobileCredentials = new JSONObject();
			mobileCredentials.put("dataValue", newPin);
			
			jobj.put("mobileCredentials", mobileCredentials);
			jobj.put("device", DObj);
			jobj.put("isChangePin", isChangePIN);
			jobj.put("userName", userName);

			System.out.println("Request---" + jobj.toString());
			System.out.println("URL --- " + UPIMacros.SET_PIN);
			getWebServiceCall().triggerWebServiceCall(UPIMacros.SET_PIN,
					jobj.toString(), new ServiceCallback() {

						@Override
						public void SuccessCallbak(String resp) {
							System.out.println("Result---" + resp);
							if (resp != null) {

								if(resp.contains("Set pin failed")) {

									Intent lIntent = new Intent(GeneratePinActivity.this, HomeActivity.class);
									startActivity(lIntent);
									finish();
								}
								try {
									JSONObject jobj = new JSONObject(resp);

										Intent lIntent = new Intent(GeneratePinActivity.this, HomeActivity.class);
										startActivity(lIntent);
										finish();
								} catch (JSONException e) {
									e.printStackTrace();
									Intent lIntent = new Intent(GeneratePinActivity.this, HomeActivity.class);
									startActivity(lIntent);
									finish();
								}

							}
						}

						@Override
						public void ErrorCallbak(String resp) {

						}
					}, "");
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

    public void generatePin(String dataFromPinActivity) {
        JSONObject jobj = new JSONObject();
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        String moblNum = prefs.getString(UPIMacros.PREF_MOBILE_NUM, null);
        String userName = prefs.getString(UPIMacros.PREF_USER_NAME, null);

        extra=getIntent().getExtras();
        try {
            jobj.put("userName", userName);
            jobj.put("cardDigits",PIN.getText().toString().trim());
            jobj.put("expiryDate",expiry.getText().toString().trim());
            jobj.put("txnId",txnId);

            JSONObject DObj = CommonUtilsUpi.DeviceInfo(moblNum,
                    location, CommonUtilsUpi.getDeviceIPAddress(true),
                    UPIMacros.TYPE,
                    CommonUtilsUpi.getDeviceID(GeneratePinActivity.this),
                    CommonUtilsUpi.getOS(), UPIMacros.APP_NAME,
                    UPIMacros.CAPABILITY);

            JSONObject mobileCredentials = new JSONObject();
            mobileCredentials.put("dataValue", dataFromPinActivity);

            jobj.put("mobileCredentials", mobileCredentials);
            jobj.put("device", DObj);

            getWebServiceCall().triggerWebServiceCall(UPIMacros.GENERATE_PIN,jobj.toString(),new ServiceCallback() {
                @Override
                public void SuccessCallbak(String resp) {
                    System.out.println(resp);
                    Intent lIntent = new Intent(GeneratePinActivity.this, HomeActivity.class);
                    startActivity(lIntent);
                    finish();
                }

                @Override
                public void ErrorCallbak(String resp) {

                }
            },"");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getGeneratePinCredential() {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        String xmlPayloadString = prefs.getString(UPIMacros.PUBLIC_KEY, null);

        String credAllowedString = "{\n" + "\t\"CredAllowed\": [{\n"
                + "\t\t\"type\": \"PIN\",\n" + "\t\t\"subtype\": \"MPIN\",\n"
                + "\t\t\"dType\": \"ALPH | NUM\",\n" + "\t\t\"dLength\": 6\n" +
                "\t}]\n" + "}";

        Intent intent = new Intent(GeneratePinActivity.this,
                PinActivityComponent.class);

        // Create Keycode
        intent.putExtra("keyCode", "NPCI");

        // Create xml payload
        if (xmlPayloadString.isEmpty()) {
            Toast.makeText(GeneratePinActivity.this,
                    "XML List Key API is not loaded.",
                    Toast.LENGTH_LONG).show();
            return;
        }
        intent.putExtra("keyXmlPayload", xmlPayloadString); // It will

        // Create Controls
        if (credAllowedString.isEmpty()) {
            Toast.makeText(GeneratePinActivity.this,
                    "Required Credentials could not be loaded.",
                    Toast.LENGTH_LONG).show();
            return;
        }
        intent.putExtra("controls", credAllowedString);

        // Create Configuration
        try {
            JSONObject configuration = new JSONObject();
            configuration.put("payerBankName", "Tarang Software Pvt Ltd.");
            configuration.put("backgroundColor", "#FFFFFF");
            Log.i("configuration", configuration.toString());
            intent.putExtra("configuration", configuration.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Create Salt
        try {
            extra=getIntent().getExtras();
            JSONObject salt = new JSONObject();
            salt.put("txnId", txnId);
            //salt.put("txnAmount", "200");
            salt.put("deviceId", CommonUtilsUpi.getDeviceID(GeneratePinActivity.this));
            salt.put("appId", UPIMacros.APP_ID);
            Log.i("salt", salt.toString());
            intent.putExtra("salt", salt.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Create Pay Info
        JSONArray payInfoArray = new JSONArray();
        try {
			/*JSONObject jsonPayeeName = new JSONObject();
			jsonPayeeName.put("name", "payeeName");
			jsonPayeeName.put("value", "John Smith");
			payInfoArray.put(jsonPayeeName);

			JSONObject jsonNote = new JSONObject();
			jsonNote.put("name", "note");
			jsonNote.put("value", "Pay restaurant bill");
			payInfoArray.put(jsonNote);

			JSONObject jsonRefId = new JSONObject();
			jsonRefId.put("name", "refId");
			jsonRefId.put("value", "589430957393");
			payInfoArray.put(jsonRefId);

			JSONObject jsonRefUrl = new JSONObject();
			jsonRefUrl.put("name", "refUrl");
			jsonRefUrl.put("value", "https://indianbank.com");
			payInfoArray.put(jsonRefUrl);*/

			JSONObject jsonAccount = new JSONObject();
			jsonAccount.put("name", "account");
			jsonAccount.put("value", extra.getString("account"));
			payInfoArray.put(jsonAccount);

            Log.i("payInfo", payInfoArray.toString());
            intent.putExtra("payInfo", payInfoArray.toString());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        // Create Language Pref
        intent.putExtra("languagePref", "En_US");

        startActivityForResult(intent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("Result Code:", String.valueOf(resultCode));
        if (requestCode == 1 && resultCode == Activity.RESULT_OK
                && data != null) {
            String errorMsgStr = data.getStringExtra("error");
            if (errorMsgStr != null && !errorMsgStr.isEmpty()) {
                Log.d("Error:", errorMsgStr);
                try {
                    JSONObject error = new JSONObject(errorMsgStr);
                    String errorCode = error.getString("errorCode");
                    String errorText = error.getString("errorText");
                    Toast.makeText(GeneratePinActivity.this,
                            errorCode + ":" + errorText, Toast.LENGTH_LONG)
                            .show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return;
            }
            HashMap<String, String> credListHashMap = (HashMap<String, String>) data
                    .getSerializableExtra("credBlocks");
            for (String cred : credListHashMap.keySet()) { // This will return
                // the list of field
                // name e.g mpin,otp
                // etc...
                try {
                    JSONObject credBlock = new JSONObject(
                            credListHashMap.get(cred));
                    dataFromPinActivity=credBlock.getJSONObject("data").getString("encryptedBase64String");
                    Log.i("enc_msg", credBlock.toString());
                    // Log.i("enc_msg", credBlock.getString("message"));

                    generatePin(dataFromPinActivity);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }
    public static String constructMessageId(){
        return "TARANG" + String.valueOf(System.currentTimeMillis());
    }
}
