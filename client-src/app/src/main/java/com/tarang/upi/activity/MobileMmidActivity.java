package com.tarang.upi.activity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tarang.upi.R;
import com.tarang.upi.utils.CommonUtilsUpi;
import com.tarang.upi.utils.ServiceCallback;
import com.tarang.upi.utils.WebServiceCall;

import org.json.JSONException;
import org.json.JSONObject;

public class MobileMmidActivity extends Activity implements OnClickListener{
	
	TextView title;
	WebServiceCall services;
	String location;
	CommonUtilsUpi comUtils;
	EditText mobile,mmid;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mobile_mmid_details);
		
		mmid=(EditText) findViewById(R.id.mmid);
		mobile=(EditText) findViewById(R.id.mobile_number_acc);
		title = (TextView) findViewById(R.id.title);
		title.setText("Mobile Details");
		
		findViewById(R.id.submit).setOnClickListener(this);
	}

	@Override
	protected void onStart() {
		super.onStart();
		location = getCommonUtils().getAddress(MobileMmidActivity.this);
	}
	
	private WebServiceCall getWebServiceCall() {
		if (services == null) {
			services = new WebServiceCall(this);
		}
		return services;
	}

	private CommonUtilsUpi getCommonUtils() {
		if (comUtils == null) {
			comUtils = new CommonUtilsUpi();
		}
		return comUtils;
	}
	
	public void serverHit(String addressType,String mobile,String mmid) {
		
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		String moblNum = prefs.getString(UPIMacros.PREF_MOBILE_NUM, null);
		JSONObject jobj=new JSONObject();
		try {
			jobj.put("addrType", addressType);
			jobj.put("mobnum", mobile);
			jobj.put("mmid", mmid);
			
			JSONObject DObj = CommonUtilsUpi.DeviceInfo(moblNum,
					location, CommonUtilsUpi.getDeviceIPAddress(true),
					UPIMacros.TYPE,
					CommonUtilsUpi.getDeviceID(MobileMmidActivity.this),
					CommonUtilsUpi.getOS(), UPIMacros.APP_NAME,
					UPIMacros.CAPABILITY);

			jobj.put("device", DObj);
			
			System.out.println("Request url --- "+UPIMacros.OTP);
			System.out.println("Request --- "+jobj.toString());
			getWebServiceCall().triggerWebServiceCall(UPIMacros.OTP,jobj.toString(),new ServiceCallback() {
				
				@Override
				public void SuccessCallbak(String resp) {
					System.out.println("Responce ---"+resp);
					if(resp!=null) {
						
					}
				}
				
				@Override
				public void ErrorCallbak(String resp) {
					
				}
			}, "");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.submit:
			
			if (mobile.getText().toString().isEmpty()) {
				mobile.requestFocus();
				Toast.makeText(this, "Please enter mobile number.",
						Toast.LENGTH_LONG).show();
			} else if (mobile.getText().toString().length() < 10) {
				mobile.requestFocus();
				Toast.makeText(this,
						"Please enter valid mobile number.",
						Toast.LENGTH_LONG).show();
			} else if (mmid.getText().toString().isEmpty()) {
				mmid.requestFocus();
				Toast.makeText(this, "Please enter MMID number.",
						Toast.LENGTH_LONG).show();
			} else if (mmid.getText().toString().length() < 7) {
				mmid.requestFocus();
				Toast.makeText(this,
						"Please enter valid MMID number.",
						Toast.LENGTH_LONG).show();
			} else {
				serverHit("MOBILE", mobile.getText().toString().trim(), mmid.getText().toString().trim());
			}
			break;

		default:
			break;
		}
	}
}
