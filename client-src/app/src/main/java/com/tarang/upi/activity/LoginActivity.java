package com.tarang.upi.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.tarang.upi.R;
import com.tarang.upi.utils.CommonUtilsUpi;
import com.tarang.upi.utils.ServiceCallback;
import com.tarang.upi.utils.WebServiceCall;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.npci.upi.security.pinactivitycomponent.PinActivityComponent;

import java.util.HashMap;

public class LoginActivity extends Activity implements OnClickListener {

	private Button login;
	private EditText username, password;
	WebServiceCall services;
	String location;
	CommonUtilsUpi comUtils;
	
	private String xmlPayloadString = "";
	private String credAllowedString = "";
	private final String keyCode = "NPCI";
	String token = "";
	boolean clInitialized = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		login = (Button) findViewById(R.id.signin);
		username = (EditText) findViewById(R.id.edt_username);
		password = (EditText) findViewById(R.id.edt_password);
		login.setOnClickListener(this);
    }

	public void getCredentials() {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		xmlPayloadString = prefs.getString(UPIMacros.PUBLIC_KEY, null);
		
		credAllowedString = "{\n" + "\t\"CredAllowed\": [{\n"
				+ "\t\t\"type\": \"OTP\",\n" + "\t\t\"subtype\": \"OTP\",\n"
				+ "\t\t\"dType\": \"ALPH | NUM\",\n" + "\t\t\"dLength\": 6\n" +
				"\t}]\n" + "}";
		
		Intent intent = new Intent(LoginActivity.this,
				PinActivityComponent.class);

		// Create Keycode
		intent.putExtra("keyCode", keyCode);

		// Create xml payload
		if (xmlPayloadString.isEmpty()) {
			Toast.makeText(LoginActivity.this,
					"XML List Key API is not loaded.",
					Toast.LENGTH_LONG).show();
			return;
		}
		intent.putExtra("keyXmlPayload", xmlPayloadString); // It will
															// get the
															// data from
															// list keys
															// API
															// response

		// Create Controls
		if (credAllowedString.isEmpty()) {
			Toast.makeText(LoginActivity.this,
					"Required Credentials could not be loaded.",
					Toast.LENGTH_LONG).show();
			return;
		}
		intent.putExtra("controls", credAllowedString);

		// Create Configuration
		try {
			JSONObject configuration = new JSONObject();
			configuration.put("payerBankName", "Tarang Software Pvt Ltd.");
			configuration.put("backgroundColor", "#FFFFFF");
			Log.i("configuration", configuration.toString());
			intent.putExtra("configuration", configuration.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}

		// Create Salt
		try {
			JSONObject salt = new JSONObject();
			//salt.put("txnId", "589430957393");
			//salt.put("txnAmount", "200");
			salt.put("deviceId", "74235ae00124fab8");
			salt.put("appId", UPIMacros.APP_ID);
			Log.i("salt", salt.toString());
			intent.putExtra("salt", salt.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}

		// Create Pay Info
		JSONArray payInfoArray = new JSONArray();
		try {
			/*JSONObject jsonPayeeName = new JSONObject();
			jsonPayeeName.put("name", "payeeName");
			jsonPayeeName.put("value", "John Smith");
			payInfoArray.put(jsonPayeeName);

			JSONObject jsonNote = new JSONObject();
			jsonNote.put("name", "note");
			jsonNote.put("value", "Pay restaurant bill");
			payInfoArray.put(jsonNote);

			JSONObject jsonRefId = new JSONObject();
			jsonRefId.put("name", "refId");
			jsonRefId.put("value", "589430957393");
			payInfoArray.put(jsonRefId);

			JSONObject jsonRefUrl = new JSONObject();
			jsonRefUrl.put("name", "refUrl");
			jsonRefUrl.put("value", "https://indianbank.com");
			payInfoArray.put(jsonRefUrl);

			JSONObject jsonAccount = new JSONObject();
			jsonAccount.put("name", "account");
			jsonAccount.put("value", "122XXX423");
			payInfoArray.put(jsonAccount);*/

			Log.i("payInfo", payInfoArray.toString());
			intent.putExtra("payInfo", payInfoArray.toString());
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		// Create Language Pref
		intent.putExtra("languagePref", "en_US");

		startActivityForResult(intent, 1);
	}
	
	
	
	private CommonUtilsUpi getCommonUtils() {
		if (comUtils == null) {
			comUtils = new CommonUtilsUpi();
		}
		return comUtils;
	}

	private WebServiceCall getWebServiceCall() {
		if (services == null) {
			services = new WebServiceCall(this);
		}
		return services;
	}

	@Override
	protected void onStart() {
		super.onStart();
		location = getCommonUtils().getAddress(LoginActivity.this);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.signin:
			String name = username.getText().toString().trim();
			String pass = password.getText().toString().trim();
			if (name.isEmpty()) {
				Toast.makeText(this,
						getString(R.string.validate_username_empty),
						Toast.LENGTH_LONG).show();
			} else if (pass.isEmpty()) {
				Toast.makeText(this,
						getString(R.string.validate_password_empty),
						Toast.LENGTH_LONG).show();
			} else {
				logInServerHit();
			}
			break;

		default:
			break;
		}
	}

	public void logInServerHit() {
		JSONObject jobj = new JSONObject();
		try {
			SharedPreferences prefs = PreferenceManager
					.getDefaultSharedPreferences(getApplicationContext());
			String moblNum = prefs.getString(UPIMacros.PREF_MOBILE_NUM, null);
			jobj.put("userName", username.getText().toString().trim());
			jobj.put("password", password.getText().toString().trim());

			JSONObject DObj = CommonUtilsUpi.DeviceInfo(moblNum,
					location, CommonUtilsUpi.getDeviceIPAddress(true),
					UPIMacros.TYPE,
					CommonUtilsUpi.getDeviceID(LoginActivity.this),
					CommonUtilsUpi.getOS(), UPIMacros.APP_NAME, "011001");

			jobj.put("device", DObj);
			System.out.println("Request---" + jobj.toString());
			System.out.println("URL --- " + UPIMacros.LOGIN);
			getWebServiceCall().triggerWebServiceCall(UPIMacros.LOGIN,
					jobj.toString(), new ServiceCallback() {

						@Override
						public void SuccessCallbak(String resp) {
							System.out.println("Result---" + resp);
							if (resp != null) {
								try {
									JSONObject jobj = new JSONObject(resp);

									if (jobj.getInt("statusCode") == 200) {
										SharedPreferences prefs = PreferenceManager
												.getDefaultSharedPreferences(getApplicationContext());
										SharedPreferences.Editor prefEditor = prefs
												.edit();
										prefEditor.putString(
												UPIMacros.PREF_USER_NAME,
												username.getText().toString()
														.trim());
										prefEditor.commit();
										if (jobj.isNull("accountSummary")) {
											Intent lIntent = new Intent(
													LoginActivity.this,
													SelectBankActivity.class);
											startActivity(lIntent);
											finish();
										} else {
											Intent lIntent = new Intent(
													LoginActivity.this,
													HomeActivity.class);
											startActivity(lIntent);
											finish();
										}
									} else {
										Toast.makeText(
												LoginActivity.this,
												"Incorrect username or password",
												Toast.LENGTH_LONG).show();
									}
								} catch (JSONException e) {
									e.printStackTrace();
									Toast.makeText(LoginActivity.this, "Connection refues, pleae try again. ",  Toast.LENGTH_LONG).show();
								}

							} else {
								Toast.makeText(LoginActivity.this,
										"Incorrect username or password",
										Toast.LENGTH_LONG).show();
							}
						}

						@Override
						public void ErrorCallbak(String resp) {
							Toast.makeText(LoginActivity.this,
									"Please try again.",
									Toast.LENGTH_LONG).show();
						}
					}, "");
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("Result Code:", String.valueOf(resultCode));
        if (requestCode == 1 && resultCode == Activity.RESULT_OK
                && data != null) {
            String errorMsgStr = data.getStringExtra("error");
            if (errorMsgStr != null && !errorMsgStr.isEmpty()) {
                Log.d("Error:", errorMsgStr);
                try {
                    JSONObject error = new JSONObject(errorMsgStr);
                    String errorCode = error.getString("errorCode");
                    String errorText = error.getString("errorText");
                    Toast.makeText(LoginActivity.this,
                            errorCode + ":" + errorText, Toast.LENGTH_LONG)
                            .show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return;
            }
            HashMap<String, String> credListHashMap = (HashMap<String, String>) data
                    .getSerializableExtra("credBlocks");
            for (String cred : credListHashMap.keySet()) { // This will return
                // the list of field
                // name e.g mpin,otp
                // etc...
                try {
                    JSONObject credBlock = new JSONObject(
                            credListHashMap.get(cred));
                    Log.i("enc_msg", credBlock.toString());
                    Log.i("enc_msg",
                            credBlock.getJSONObject("data").getString(
                                    "encryptedBase64String"));
                    // Log.i("enc_msg", credBlock.getString("message"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }
}
