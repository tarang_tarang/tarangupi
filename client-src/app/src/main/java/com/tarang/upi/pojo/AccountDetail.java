package com.tarang.upi.pojo;

import java.io.Serializable;

public class AccountDetail implements Serializable {
	
	private int id;
	private String accountNo;
	private String iFSCCode;
	private String bankType;
	private String name;
	private String aeba;
	private String virtualAddress;
	
	public String getVirtualAddress() {
		return virtualAddress;
	}
	public void setVirtualAddress(String virtualAddress) {
		this.virtualAddress = virtualAddress;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getIFSCCode() {
		return iFSCCode;
	}
	public void setIFSCCode(String iFSCCode) {
		this.iFSCCode = iFSCCode;
	}
	public String getBankType() {
		return bankType;
	}
	public void setBankType(String bankType) {
		this.bankType = bankType;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAeba() {
		return aeba;
	}
	public void setAeba(String aeba) {
		this.aeba = aeba;
	}
}
