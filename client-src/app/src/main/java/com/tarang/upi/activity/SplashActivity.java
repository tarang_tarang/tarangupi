package com.tarang.upi.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.tarang.upi.R;
import com.tarang.upi.utils.CommonUtilsUpi;

import java.io.IOException;

public class SplashActivity extends Activity {
	/** Called when the activity is first created. */

	public static final int SPLASH_TIMEOUT = 2000;
	boolean isError;
	/** set time to splash out in ms */
	final int welcomeScreenDisplay = 2000;
	int countGCMRegister=0;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_screen);

		/** create a thread to show splash up to splash time */
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		String gcmRegisterId = prefs.getString(UPIMacros.GCM_REG_ID, null);
		if (CommonUtilsUpi.hasInternetConnectivity(getApplicationContext())) {
			if (gcmRegisterId == null || gcmRegisterId.length() < 5) {
				registerInBackground();
			}
			StopSplash();
		} else {
			Toast.makeText(SplashActivity.this, "No internet connection, Please check you internet.", Toast.LENGTH_LONG).show();
			finish();
		}

	}

	public void StopSplash() {
		new Thread() {
			int wait = 0;

			@Override
			public void run() {
				try {

					super.run();
					sleep(welcomeScreenDisplay);
				} catch (Exception e) {
				} finally {
					init();
				}
			}
		}.start();
	}

	public void registerInBackground() {
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				GoogleCloudMessaging gcm = GoogleCloudMessaging
						.getInstance(getApplicationContext());
				try {
					String regId = "";
					int i = 0;
					do {
						regId = gcm.register(UPIMacros.SENDER_ID);
						i++;
						if (regId.length() > 0) {
							i = 3;
						}
					} while (i < 3);
					SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
					SharedPreferences.Editor prefEditor = prefs.edit();
					prefEditor.putString(UPIMacros.GCM_REG_ID, regId);
					prefEditor.commit();
					Log.d("SplashActivity", "Got GCM Reg ID -----> " + regId);

				} catch (IOException ex) {
					Log.d("Upi ", "Error: while GCM regn" + ex.getMessage());
					if(countGCMRegister<5) {
						countGCMRegister++;
						System.out.println("countGCMRegister --- "+countGCMRegister);
						registerInBackground();
					}
				}
				return null;
			}
		}.execute(null, null, null);
	}

	public void goLogin() {
		Intent intent = new Intent(this, LoginActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		this.startActivity(intent);
		finish();
	}

	public void goHome() {
		Intent intent = new Intent(this, HomeActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		this.startActivity(intent);
		finish();
	}

	private void goRegistration() {
		Intent intent = new Intent(this, RegistrationActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		this.startActivity(intent);
		finish();

	}

	private void init() {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		boolean passcode = prefs.getBoolean(UPIMacros.PREF_LOG_ON, false);

		if (passcode) {
			goLogin();
		} else {
			goRegistration();
		}
	}
}
