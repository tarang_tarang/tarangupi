package com.tarang.upi.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.tarang.upi.R;
import com.tarang.upi.pojo.AccountDetail;
import com.tarang.upi.utils.CommonUtilsUpi;
import com.tarang.upi.utils.ServiceCallback;
import com.tarang.upi.utils.WebServiceCall;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AccountDetailAskActivity extends Activity implements OnClickListener,OnItemSelectedListener{
	
	TextView title;
	Spinner accTypeSpinner;
	ArrayList<String> arrayTypeList;
	SpinnerAdapter adapter;
	EditText accNum,acc_ifsc;
	CommonUtilsUpi comUtils;
	WebServiceCall services;
	String location;
	ArrayList<AccountDetail> bankAccountList;
	ListView bankAccountListView;
	String spnnerText=null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ask_account_details);
		
		findViewById(R.id.submit).setOnClickListener(this);
		accTypeSpinner=(Spinner) findViewById(R.id.bank_type_spinner);
		accNum=(EditText) findViewById(R.id.acc_number);
		acc_ifsc=(EditText) findViewById(R.id.acc_ifsc);
		title = (TextView) findViewById(R.id.title);
		title.setText("Account Details");
		populateSpinner();
		accTypeSpinner.setOnItemSelectedListener(this);
		
	}

	private WebServiceCall getWebServiceCall() {
		if (services == null) {
			services = new WebServiceCall(this);
		}
		return services;
	}

	@Override
	protected void onStart() {
		super.onStart();
		location = getCommonUtils().getAddress(AccountDetailAskActivity.this);
	}

	private CommonUtilsUpi getCommonUtils() {
		if (comUtils == null) {
			comUtils = new CommonUtilsUpi();
		}
		return comUtils;
	}
	
	public void populateSpinner() {
		
		arrayTypeList=new ArrayList<String>();
		arrayTypeList.add("Select account type");
		arrayTypeList.add("SAVINGS");
		arrayTypeList.add("CURRENT");
		arrayTypeList.add("DEFAULT");
		
		adapter=new SpinnerAdapter(this, R.layout.custom_spinner_item, arrayTypeList);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		accTypeSpinner.setAdapter(adapter);
		adapter.notifyDataSetChanged();
	}
	
	class SpinnerAdapter extends ArrayAdapter<String> {

		private Activity activity;
		private ArrayList data;
		LayoutInflater inflater;
		
		public SpinnerAdapter(AccountDetailAskActivity context,
				int textViewResourceId, ArrayList<String> objects) {
			super(context, textViewResourceId, objects);
			
			activity = context;
			data = objects;
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}
		
		@Override
		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {
			return getCustomView(position, convertView, parent);
		}

		@Override
		public int getCount() {
			return arrayTypeList.size();
		}
		
		public View getCustomView(int position, View convertView,
				ViewGroup parent) {

			View row = inflater.inflate(R.layout.custom_spinner_item, parent,
					false);

			TextView label = (TextView) row.findViewById(R.id.label);
			label.setText(arrayTypeList.get(position));

			return row;
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.submit:
			
			if(accNum.getText().toString().isEmpty()) {
				Toast.makeText(AccountDetailAskActivity.this, "Please enter account number", Toast.LENGTH_LONG).show();
			} else if(acc_ifsc.getText().toString().isEmpty()||acc_ifsc.getText().toString().length()<7 ) {
				Toast.makeText(AccountDetailAskActivity.this, "Please enter 7 digit ifsc code ", Toast.LENGTH_LONG).show();
			} else if(spnnerText==null || spnnerText.equalsIgnoreCase("Select account type")) {
				
				Toast.makeText(AccountDetailAskActivity.this, "Please select account.", Toast.LENGTH_LONG).show();
			} else {
				serverHit("ACCOUNT", accNum.getText().toString().trim(), "AACA"+acc_ifsc.getText().toString().trim(), spnnerText);
			}
			break;

		default:
			break;
		}
	}
	
	private void getAccountList(String otp) {
		JSONObject jobj = new JSONObject();
		try {
			SharedPreferences prefs = PreferenceManager
					.getDefaultSharedPreferences(getApplicationContext());
			String moblNum = prefs.getString(UPIMacros.PREF_MOBILE_NUM, null);
			String userName = prefs.getString(UPIMacros.PREF_USER_NAME, null);

			jobj.put("mmid", UPIMacros.MMID);
			jobj.put("userName", userName);
			jobj.put("otp", otp);
			JSONObject DObj = CommonUtilsUpi.DeviceInfo(moblNum, 
					location, CommonUtilsUpi.getDeviceIPAddress(true),
					UPIMacros.TYPE,
					CommonUtilsUpi.getDeviceID(AccountDetailAskActivity.this),
					CommonUtilsUpi.getOS(), UPIMacros.APP_NAME,
					UPIMacros.CAPABILITY);

			jobj.put("device", DObj);

			System.out.println("Request---" + jobj.toString());
			System.out.println("URL --- " + UPIMacros.ACC_LIST);
			getWebServiceCall().triggerWebServiceCall(UPIMacros.ACC_LIST,
					jobj.toString(), new ServiceCallback() {

						@Override
						public void SuccessCallbak(String resp) {
							System.out.println("Result---" + resp);
							UPIMacros.bankAccountListGlobal = new ArrayList<AccountDetail>();
							UPIMacros.bankAccountListGlobal = parseAcctOtpResp(resp);
							Intent intent=new Intent(AccountDetailAskActivity.this,SelectBankActivity.class);
							startActivity(intent);
							finish();
						}

						@Override
						public void ErrorCallbak(String resp) {

						}
					}, "");
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<AccountDetail> parseAcctOtpResp(String resp) {
		ArrayList<AccountDetail> accList = new ArrayList<AccountDetail>();
		try {
			JSONObject jobj = new JSONObject(resp);
			JSONArray jArray = jobj.getJSONArray("accountSummaryList");
			for (int i = 0; i < jArray.length(); i++) {

				AccountDetail accDetail = new AccountDetail();
				JSONObject arrObj = jArray.getJSONObject(i);
				accDetail.setAccountNo(arrObj.getString("maskedAccnumber"));
				accDetail.setAeba(arrObj.getString("aeba"));
				accDetail.setBankType("BankType");
				accDetail.setId(arrObj.getInt("id"));
				accDetail.setIFSCCode(arrObj.getString("ifsc"));
				accDetail.setName(arrObj.getString("name"));
				accList.add(accDetail);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return accList;
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		spnnerText=accTypeSpinner.getSelectedItem().toString();
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		
	}

public void serverHit(String addressType,String acnum,String ifsc,String actype) {
		
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		String moblNum = prefs.getString(UPIMacros.PREF_MOBILE_NUM, null);
		String username=prefs.getString(UPIMacros.PREF_USER_NAME, null);
		JSONObject jobj=new JSONObject();
		try {
			jobj.put("userName", username);
			
			final JSONObject accJobj=new JSONObject();
			accJobj.put("addrType", addressType);
			accJobj.put("acnum", acnum);
			accJobj.put("ifsc", ifsc);
			accJobj.put("actype", actype);
			
			jobj.put("acDetails", accJobj);
			
			JSONObject DObj = CommonUtilsUpi.DeviceInfo(moblNum, 
					location, CommonUtilsUpi.getDeviceIPAddress(true),
					UPIMacros.TYPE,
					CommonUtilsUpi.getDeviceID(AccountDetailAskActivity.this),
					CommonUtilsUpi.getOS(), UPIMacros.APP_NAME,
					UPIMacros.CAPABILITY);

			jobj.put("device", DObj);
			
			System.out.println("Request url --- "+UPIMacros.OTP);
			System.out.println("Request --- "+jobj.toString());
			getWebServiceCall().triggerWebServiceCall(UPIMacros.OTP,jobj.toString(),new ServiceCallback() {
				
				@Override
				public void SuccessCallbak(String resp) {
					System.out.println("Responce ---"+resp);
					
					try {
						JSONObject jobj=new JSONObject(resp);
						if(jobj.getInt("statusCode")==50118) {
							Intent intent=new Intent(AccountDetailAskActivity.this,SelectBankActivity.class);
							intent.putExtra("call_otp", "call_otp");
							intent.putExtra("acDetails", accJobj.toString());
							startActivity(intent);
							finish();
						} else if(jobj.getInt("statusCode")==50114) {
                            Toast.makeText(AccountDetailAskActivity.this, "Session expired, please login again.", Toast.LENGTH_LONG).show();
                            Intent intent=new Intent(AccountDetailAskActivity.this,LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }
                        else {
							Toast.makeText(AccountDetailAskActivity.this, "Otp request failed, please try again.", Toast.LENGTH_LONG).show();
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
                        Toast.makeText(AccountDetailAskActivity.this, "Connection time out, please try again.", Toast.LENGTH_LONG).show();
					}
					
				}
				
				@Override
				public void ErrorCallbak(String resp) {
					/*Intent intent=new Intent(AccountDetailAskActivity.this,SelectBankActivity.class);
					intent.putExtra("call_otp", "call_otp");
					startActivity(intent);
					finish();*/
				}
			}, "");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
}
