/**
 * 
 */
package com.tarang.upi.activity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.tarang.upi.utils.CommonUtilsUpi;
import com.tarang.upi.utils.WebServiceCall;

import java.text.DecimalFormat;

import psp.mobile.model.request.MobileDevice;

/**
 * @author prasadj
 *
 */
public class UpiAbstractActivity extends Activity {

	private WebServiceCall services;

	@Override
	protected void onStart() {
		super.onStart();
	}

	protected String getStringFromSharedPreferences(String propName){
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		return prefs.getString(propName, null);
	}
	
	protected WebServiceCall getWebServiceCall() {
		if (services == null) {
			services = new WebServiceCall(this);
		}
		return services;
	}

	protected MobileDevice getMobileDevice() {

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		String moblNum = prefs.getString(UPIMacros.PREF_MOBILE_NUM, null);
		String lalitutue = "0.0";
		String longitude = "0.0";
		String location = "";
		String ip = CommonUtilsUpi.getDeviceIPAddress(true);
		String type = UPIMacros.TYPE;
		String terminalId = CommonUtilsUpi.getDeviceID(this);
		String os = CommonUtilsUpi.getOS();
		String app = UPIMacros.APP_NAME;
		String capability = UPIMacros.CAPABILITY;
		
		MobileDevice mobileDevice = new MobileDevice();
		mobileDevice.setMobile(moblNum);
		mobileDevice.setGeoCode(getDoubleString(lalitutue, "##.####") + ", " + getDoubleString(longitude, "##.####"));
		mobileDevice.setLocation(location);
		mobileDevice.setIp(ip);
		mobileDevice.setType(type);
		mobileDevice.setTerminalId(terminalId);
		mobileDevice.setOs(os);
		mobileDevice.setApp(app);
		mobileDevice.setCapability(capability);
		
		return mobileDevice;
	}
	
	public static String getDoubleString(String valueStr, String format){
		try {
			double value = Double.parseDouble(valueStr);
			return new DecimalFormat(format).format(value);
		}
		catch(Exception ex) {
			return "0.0";
		}
	}
	
}