package com.tarang.upi.utils;

/**
 * Created by NeeloyG on 02-02-2016.
 */


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import android.util.Base64;

public class CryptLib {

    public byte[] SHA256(String paramString)throws Exception{
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(paramString.getBytes("UTF-8"));
        byte[] digest = md.digest();
        return digest;
    }

    public byte[] encrypt(byte[] data, byte[] key)throws Exception
    {
        SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
        byte[] iv = new byte[16];
        IvParameterSpec ivSpec = new IvParameterSpec(iv);
        Cipher acipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        byte[] arrayOfByte1;
        acipher.init(Cipher.ENCRYPT_MODE, keySpec,ivSpec);
        arrayOfByte1 = acipher.doFinal(data);
        return arrayOfByte1;
    }

    public byte[] decrypt(byte[] data, byte[] key)throws Exception
    {
        SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
        byte[] iv = new byte[16];
        IvParameterSpec ivSpec = new IvParameterSpec(iv);
        Cipher acipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        byte[] arrayOfByte1;
        acipher.init(Cipher.DECRYPT_MODE, keySpec,ivSpec);
        arrayOfByte1 = acipher.doFinal(data);
        return arrayOfByte1;
    }


    public PrivateKey readPrivateKeyFromString(String privateKeyData) throws InvalidKeySpecException,
            NoSuchAlgorithmException, IOException {

        byte[] keyBytes = Base64.decode(privateKeyData, Base64.NO_WRAP);

        // Get private Key
        PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory fact = KeyFactory.getInstance("RSA");
        PrivateKey privateKey = fact.generatePrivate(pkcs8EncodedKeySpec);

        return privateKey;
    }

    public String decryptData(String encryptedData, PrivateKey privateKey){

        try {
             byte[] encryptedDataBytes = Base64.decode(encryptedData, Base64.NO_WRAP);

            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            byte[] decryptedData = cipher.doFinal(encryptedDataBytes);
            String palinTextDecryptedData = new String(decryptedData);

            return palinTextDecryptedData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public byte[] hexStringToByteArray(String s) {
        byte[] b = new byte[s.length() / 2];
        for (int i = 0; i < b.length; i++) {
            int index = i * 2;
            int v = Integer.parseInt(s.substring(index, index + 2), 16);
            b[i] = (byte) v;
        }
        return b;
    }

}