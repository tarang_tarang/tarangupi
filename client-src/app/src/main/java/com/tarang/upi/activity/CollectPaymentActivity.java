package com.tarang.upi.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.tarang.upi.R;
import com.tarang.upi.adapter.BeneficiaryListAdapter;
import com.tarang.upi.utils.ServiceCallback;

import java.util.List;

import psp.mobile.model.request.GetBeneficiaryListRequest;
import psp.mobile.model.request.GetBeneficiaryRequest;
import psp.mobile.model.response.BeneficiarySummary;
import psp.mobile.model.response.GetBeneficiaryListResponse;
import psp.mobile.model.response.GetBeneficiaryResponse;

public class CollectPaymentActivity extends UpiAbstractActivity implements OnClickListener {

	private TextView entity_id_list, new_entity_id, title;
	private ListView ben_list;
	private List<BeneficiarySummary> mlist;
	private BeneficiaryListAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_collect_payment);
		new_entity_id = (TextView) findViewById(R.id.new_entity_id);
		entity_id_list = (TextView) findViewById(R.id.entity_id_list);
		title = (TextView) findViewById(R.id.title);
		entity_id_list.setOnClickListener(this);
		new_entity_id.setOnClickListener(this);
		title.setText("Collect Payment");
		ben_list = (ListView) findViewById(R.id.collect_beneficiary_list);
		ben_list.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				getBeneficiarySummary(mlist.get(position).getBenNickName());
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.entity_id_list:
				getBeneficiaryList();
				break;
			case R.id.new_entity_id:
				Intent intent = new Intent(CollectPaymentActivity.this, AccountNoteActivity.class);
				intent.putExtra("call_from", "CollectPaymentActivity");
				startActivity(intent);
				break;
			default:
				break;
		}
	}
	
	public void getBeneficiaryList() {
		GetBeneficiaryListRequest gbflReq = new GetBeneficiaryListRequest();
		gbflReq.setUserName(getStringFromSharedPreferences(UPIMacros.PREF_USER_NAME));
		gbflReq.setDevice(getMobileDevice());
		gbflReq.setRequestType("VIRTUAL_ADDRESS");
		getWebServiceCall().triggerWebServiceCall(UPIMacros.GET_BENEFICIARY_LIST, gbflReq.toJsonString(), new ServiceCallback() {
			@Override
			public void SuccessCallbak(String resp) {
				GetBeneficiaryListResponse gbflResp = new GetBeneficiaryListResponse();
				if(gbflResp.getStatusCode().equals("200")) {
                    try {
                        mlist = gbflResp.getBenefSummaries();
                        adapter = new BeneficiaryListAdapter(CollectPaymentActivity.this, mlist);
                        ben_list.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        if (mlist == null || mlist.isEmpty()) {
                            ben_list.setVisibility(View.GONE);
                            Toast.makeText(CollectPaymentActivity.this, "Please add some beneficairy.", Toast.LENGTH_LONG).show();
                            entity_id_list.setVisibility(View.GONE);
                        } else {
                            ben_list.setVisibility(View.VISIBLE);
                            entity_id_list.setVisibility(View.GONE);
                            new_entity_id.setVisibility(View.GONE);
                        }
                     }catch(Exception e){
                        ben_list.setVisibility(View.GONE);
                        Toast.makeText(CollectPaymentActivity.this, "Please add some beneficairy.", Toast.LENGTH_LONG).show();
                        entity_id_list.setVisibility(View.GONE);
                    }
                    } else if(gbflResp.getStatusCode().equals("50114")) {
                    Toast.makeText(CollectPaymentActivity.this, "Session expired, please login again.", Toast.LENGTH_LONG).show();
                    Intent intent=new Intent(CollectPaymentActivity.this,LoginActivity.class);
                    startActivity(intent);
                    finish();
                }

			}
			@Override
			public void ErrorCallbak(String resp) {
				
			}
		}, "");
	}

	public void getBeneficiarySummary(String nickName) {
		GetBeneficiaryRequest gbfReq = new GetBeneficiaryRequest();
		gbfReq.setUserName(getStringFromSharedPreferences(UPIMacros.PREF_USER_NAME));
		gbfReq.setNickName(nickName);
		gbfReq.setDevice(getMobileDevice());
		getWebServiceCall().triggerWebServiceCall(UPIMacros.GET_BENEFICIARY, gbfReq.toJsonString(),
			new ServiceCallback() {
				@Override
				public void SuccessCallbak(String resp) {
					GetBeneficiaryResponse res = GetBeneficiaryResponse.constructGetBeneficiaryResponse(resp);
					if(res.getStatusCode().equals("200")) {
						beneficiaryDetailPopUp(res);
					} else if(res.getStatusCode().equals("50114")) {
                        Toast.makeText(CollectPaymentActivity.this, "Session expired, please login again.", Toast.LENGTH_LONG).show();
                        Intent intent=new Intent(CollectPaymentActivity.this,LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
				}
				@Override
				public void ErrorCallbak(String resp) {
				}
			}, "");
	}
	
	public void beneficiaryDetailPopUp(final GetBeneficiaryResponse res) {

		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setContentView(R.layout.merchant_details);

		TextView title_merchant = (TextView) dialog.findViewById(R.id.title_merchant);
		TextView txtname = (TextView) dialog.findViewById(R.id.merchant_name);
		TextView merchant_nick_name = (TextView) dialog.findViewById(R.id.merchant_nick_name);
		TextView merchant_virtual_address = (TextView) dialog.findViewById(R.id.merchant_virtual_address);
		TextView merchant_desc = (TextView) dialog.findViewById(R.id.merchant_desc);
		TextView bene_accc_number = (TextView) dialog.findViewById(R.id.bene_accc_number);
		TextView bene_ifsc = (TextView) dialog.findViewById(R.id.bene_ifsc);
		
		LinearLayout ll_acc = (LinearLayout) dialog.findViewById(R.id.ll_acc_number);
		LinearLayout ll_ifsc = (LinearLayout) dialog.findViewById(R.id.ll_ifsc);
		LinearLayout ll_virtual_address = (LinearLayout) dialog.findViewById(R.id.ll_virtual_address);
		ll_acc.setVisibility(View.VISIBLE);
		ll_ifsc.setVisibility(View.VISIBLE);
		
		Button cancel = (Button) dialog.findViewById(R.id.merchant_cancel);
		Button pay = (Button) dialog.findViewById(R.id.merchant_Pay);
		Window window = dialog.getWindow();
		WindowManager.LayoutParams wlp = window.getAttributes();
		wlp.gravity = Gravity.CENTER;

		txtname.setText(res.getName());
		merchant_nick_name.setText(res.getNickName());
		merchant_virtual_address.setText(res.getAddress());
		merchant_desc.setText(res.getDescription());
		bene_ifsc.setText(res.getIfsc());
		
		bene_accc_number.setText(res.getMaskedNumber());
		title_merchant.setText("Beneficiary Detail");
		pay.setText("Collect");
		
		if( res.getIfsc().equalsIgnoreCase("null") ) {
			ll_acc.setVisibility(View.GONE);
			ll_ifsc.setVisibility(View.GONE);
		} 
		else {
		    ll_virtual_address.setVisibility(View.GONE);
		}
		cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		
		pay.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(CollectPaymentActivity.this,PaymentActivity.class);
				intent.putExtra("call_from", "CollectPaymentFromBeneficiaryList");
				intent.putExtra("name", res.getName());
				intent.putExtra("nick", res.getNickName());
				startActivity(intent);
				finish();
			}
		});
		dialog.show();
	}
	
}