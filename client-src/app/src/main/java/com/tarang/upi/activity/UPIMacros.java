package com.tarang.upi.activity;

import com.tarang.upi.pojo.AccountDetail;

import java.util.ArrayList;

public class UPIMacros {
	
	//public static String BASE_URL = "http://121.244.157.143:9200/psp-webserver/psp/mobile/";//
	//public static String BASE_URL = "https://172.30.70.118:443/upi/mobile/";
	//public static String BASE_URL = "http://172.30.66.150:8090/upi/mobile/"; // manas system
	//public static String BASE_URL = "http://172.30.66.190:8002/upi/mobile/"; // hemanth system
	//public static String BASE_URL = "https://172.30.70.118:9200/upi/mobile/";
	public static String BASE_URL = "https://121.244.157.134/upi/mobile/";// main ip for out side
	//public static String BASE_URL = "http://121.244.157.134:9200/upi/mobile/";// main ip for out side
	public static String registration = BASE_URL+"registration";
	public static String ACC_PROVIDER = BASE_URL+ "getAccProviders";
	public static String OTP = BASE_URL+ "otpRequest";
	public static String LOGIN = BASE_URL+"login";
	public static String ACC_LIST = BASE_URL+ "getAccList";
	public static String EXIST_ACC_LIST = BASE_URL+ "getExistingAccounts";
	public static String ADD_BANK_ACC = BASE_URL+ "addBankAcc";
	public static String SET_PIN = BASE_URL+ "setPin";
	public static String IS_REGISTER = BASE_URL+ "isRegisterUser";
	public static String LOGOUT = BASE_URL+ "logout";
	public static String CHANGE_PASSWORD = BASE_URL+ "changePassword";
	public static String MAKE_PAYMENT = BASE_URL+ "makePayment";
	public static String GET_TOKEN = BASE_URL+ "getToken";
	public static String PAYMENT = BASE_URL+ "makePayment";
	public static String ADDBENEFICIARY = BASE_URL+ "addBeneficiary";
	public static String GET_MERCHANT_LIST = BASE_URL+ "getMerchantList";
	public static String GET_BENEFICIARY_LIST = BASE_URL+ "getBeneficiaryList";
	public static String GET_BENEFICIARY = BASE_URL+ "getBeneficiary";
	public static String GET_MERCHANT = BASE_URL+ "getMerchant";
	public static String BALANCE_ENQ = BASE_URL+ "balEnq";
	public static String GET_TXT_DETAILS = BASE_URL+ "getTxnDetails";
	public static String COLLECT_PAYMRNT = BASE_URL+ "collectPayment";
    public static String GET_NOTIFICATION = BASE_URL+ "getNotificationList";
    public static String GET_NOTIFICATION_SUMMARY = BASE_URL+ "getNotificationDetails";
    public static String GENERATE_PIN = BASE_URL+ "generatePin";
    public static String COLLECT_PAYMENT_CONFIRM_NOTIFICATION = BASE_URL+ "confirmCollectPay";
	
	public static String INTENT_MOBILE_NUM = "mobile_num";
	public static String PREF_MOBILE_NUM = "pref_mobile_num";
	public static String PREF_EMAIL_ID = "pref_email";
	public static String PREF_UPI_CHLL = "pref_chell";
	public static String PREF_ADHAAR = "pref_adhaar";
	public static String PUBLIC_KEY = "public_key";
	public static String PREF_PASSWORD = "pref_password";
	public static String PREF_USER_NAME = "pref_username";
	
	public static String PREF_LOG_ON = "is_log_on";
	public static String PREF_ADDED_BANK = "is_log_on";
	public static ArrayList<AccountDetail> bankAccountListGlobal = new ArrayList<AccountDetail>();
	
	public static String MMID = "234123234";
	public static String CAPABILITY = "011001";
	
	public static String APP_NAME = "UPI solution v1.0";
	public static String APP_ID = "com.tarang.upi";
	public static String TYPE = "Mobile";
	public static String PAYER_VIRTUAL_ADDRESS = "vaddress";
	public static String MOBILE_ADHAAR = "mob_adh";
	public static String GCM_REG_ID = "grID";
	
	//Google cloud messaging sender Id (project Id/num)
		public static final String SENDER_ID = "235808539517";
		public static final int NOTIFY_ID = 235808539;//713979214;
	
}
