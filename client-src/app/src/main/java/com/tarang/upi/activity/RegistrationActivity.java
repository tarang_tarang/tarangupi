package com.tarang.upi.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.tarang.upi.R;
import com.tarang.upi.utils.CommonUtilsUpi;


public class RegistrationActivity extends Activity implements OnClickListener {

	EditText edtMobileNumber;
	private CheckBox checkBoxAgree;
	String location;
	CommonUtilsUpi comUtils;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registration);
		edtMobileNumber = (EditText) findViewById(R.id.edt_mobile);
		checkBoxAgree = (CheckBox) findViewById(R.id.checkbox_agree);
		findViewById(R.id.btnSubmit).setOnClickListener(this);
		
		comUtils = new CommonUtilsUpi();
	}

	@Override
	protected void onStart() {
		super.onStart();
		location = comUtils.getAddress(RegistrationActivity.this);
	}
	
	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.btnSubmit:
			String moblNo = edtMobileNumber.getText().toString();
			if(checkBoxAgree.isChecked()) {
				if (moblNo != null && moblNo.length() == 10) {
					Intent lIntent = new Intent(RegistrationActivity.this, OtpActivity.class);
					lIntent.putExtra(UPIMacros.INTENT_MOBILE_NUM, moblNo);
					startActivity(lIntent);
					finish();
				} else {
					Toast.makeText(this, getString(R.string.validate_mobl_no), Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(this, "Please agree the terms and conditions", Toast.LENGTH_LONG).show();
			}
			break;
		default:
			break;
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
	}
	
}
