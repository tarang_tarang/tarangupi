package com.tarang.upi.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.tarang.upi.R;
import com.tarang.upi.adapter.MerchantListAdapter;
import com.tarang.upi.pojo.MerchantListPojo;
import com.tarang.upi.utils.CommonUtilsUpi;
import com.tarang.upi.utils.ServiceCallback;
import com.tarang.upi.utils.WebServiceCall;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MerchantListActivity  extends Activity{

	TextView title;
	CommonUtilsUpi comUtils;
	WebServiceCall services;
	String location;
	ListView merchant_list;
	ArrayList<MerchantListPojo> mlist;
	MerchantListAdapter adapter ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_get_merchant_list);
		title = (TextView) findViewById(R.id.title);
		title.setText("Merchant List");
		merchant_list=(ListView) findViewById(R.id.merchant_list);
		mlist=new ArrayList<MerchantListPojo>();
		getMerchantList();
		
		merchant_list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				MerchantListPojo pojo=mlist.get(position);
				getMerchant(pojo.getId());
			}
		});
	}
	
	private WebServiceCall getWebServiceCall() {
		if (services == null) {
			services = new WebServiceCall(this);
		}
		return services;
	}

	@Override
	protected void onStart() {
		super.onStart();
		location = getCommonUtils().getAddress(MerchantListActivity.this);
	}

	private CommonUtilsUpi getCommonUtils() {
		if (comUtils == null) {
			comUtils = new CommonUtilsUpi();
		}
		return comUtils;
	}
	
	public void getMerchantList() {
		
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		String moblNum = prefs.getString(UPIMacros.PREF_MOBILE_NUM, null);
		String userName = prefs.getString(UPIMacros.PREF_USER_NAME, null);
		
		 JSONObject jobj=new JSONObject();
		 try {
			jobj.put("userName", userName);
			
			JSONObject DObj = CommonUtilsUpi.DeviceInfo(moblNum,
					location, CommonUtilsUpi.getDeviceIPAddress(true),
					UPIMacros.TYPE,
					CommonUtilsUpi.getDeviceID(MerchantListActivity.this),
					CommonUtilsUpi.getOS(), UPIMacros.APP_NAME,
					UPIMacros.CAPABILITY);

			jobj.put("device", DObj);
			System.out.println("Request --"+jobj.toString());
			getWebServiceCall().triggerWebServiceCall(UPIMacros.GET_MERCHANT_LIST, jobj.toString(), new ServiceCallback() {
				
				@Override
				public void SuccessCallbak(String resp) {
					System.out.println(resp);
					if(resp!=null) {
						try {
							JSONObject jobj=new JSONObject(resp);
							if(jobj.getInt("statusCode")==200) {
								
								JSONArray jArray= jobj.getJSONArray("merchantList");
								for(int i=0;i<jArray.length();i++) {
									MerchantListPojo pojo=new MerchantListPojo();
									pojo.setDescription(jArray.getJSONObject(i).getString("description"));
									pojo.setName(jArray.getJSONObject(i).getString("name"));
									pojo.setNickName(jArray.getJSONObject(i).getString("nickName"));
									pojo.setVirtualAddress(jArray.getJSONObject(i).getString("virtualAddress"));
									pojo.setId(jArray.getJSONObject(i).getInt("id"));
									
									mlist.add(pojo);
								}
								
								adapter=new MerchantListAdapter(MerchantListActivity.this, mlist);
								merchant_list.setAdapter(adapter);
								adapter.notifyDataSetChanged();
								
							}else if(jobj.getInt("statusCode")==50114) {
                                Toast.makeText(MerchantListActivity.this, "Session expired, please login again.", Toast.LENGTH_LONG).show();
                                Intent intent=new Intent(MerchantListActivity.this,LoginActivity.class);
                                startActivity(intent);
                                finish();
                            }
						} catch (JSONException e) {
							e.printStackTrace();
							Toast.makeText(MerchantListActivity.this, "Server time out, please try later.", Toast.LENGTH_LONG).show();
							finish();
						}
						
					}
				}
				
				@Override
				public void ErrorCallbak(String resp) {
					
				}
			}, "");
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public void getMerchant(int id) {
		
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		String moblNum = prefs.getString(UPIMacros.PREF_MOBILE_NUM, null);
		String userName = prefs.getString(UPIMacros.PREF_USER_NAME, null);
		JSONObject jobj=new JSONObject();
		
		try {
			jobj.put("userName", userName);
			jobj.put("merchantId", id);
			
			JSONObject DObj = CommonUtilsUpi.DeviceInfo(moblNum,
					location, CommonUtilsUpi.getDeviceIPAddress(true),
					UPIMacros.TYPE,
					CommonUtilsUpi.getDeviceID(MerchantListActivity.this),
					CommonUtilsUpi.getOS(), UPIMacros.APP_NAME,
					UPIMacros.CAPABILITY);

			jobj.put("device", DObj);
			System.out.println("Request ---"+jobj.toString());
			getWebServiceCall().triggerWebServiceCall(UPIMacros.GET_MERCHANT, jobj.toString(), new ServiceCallback() {
				
				@Override
				public void SuccessCallbak(String resp) {
					System.out.println("Responce -- "+resp);
					if(resp!=null) {
						try {
							JSONObject respJson=new JSONObject(resp);
							if(respJson.getInt("statusCode")==200) {
								int id=respJson.getJSONObject("merchantDetails").getInt("id");
								String name=respJson.getJSONObject("merchantDetails").getString("name");
								String nick=respJson.getJSONObject("merchantDetails").getString("nickName");
								String virAddess=respJson.getJSONObject("merchantDetails").getString("virtualAddress");
								String description=respJson.getJSONObject("merchantDetails").getString("description");
								merchantDetailPopUp(id,name,nick,virAddess,description);
							}else if(respJson.getInt("statusCode")==50114) {
                                Toast.makeText(MerchantListActivity.this, "Session expired, please login again.", Toast.LENGTH_LONG).show();
                                Intent intent=new Intent(MerchantListActivity.this,LoginActivity.class);
                                startActivity(intent);
                                finish();
                            }
						} catch (JSONException e) {
							e.printStackTrace();
							Toast.makeText(MerchantListActivity.this, "Server time out, please try later.", Toast.LENGTH_LONG).show();
						}
					}
				}
				
				@Override
				public void ErrorCallbak(String resp) {
					
				}
			},"");
		} catch (JSONException e) {
			e.printStackTrace();
			Toast.makeText(MerchantListActivity.this, "Server time out, please try later.", Toast.LENGTH_LONG).show();
		}
	}
	
	public void merchantDetailPopUp(final int id,final String name,final String nick,final String virtual_address,String desc) {


		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setContentView(R.layout.merchant_details);

		TextView txtname=(TextView) dialog.findViewById(R.id.merchant_name);
		TextView merchant_nick_name=(TextView) dialog.findViewById(R.id.merchant_nick_name);
		TextView merchant_virtual_address=(TextView) dialog.findViewById(R.id.merchant_virtual_address);
		TextView merchant_desc=(TextView) dialog.findViewById(R.id.merchant_desc);
		
		Button cancel=(Button) dialog.findViewById(R.id.merchant_cancel);
		Button pay=(Button) dialog.findViewById(R.id.merchant_Pay);
		Window window = dialog.getWindow();
		WindowManager.LayoutParams wlp = window.getAttributes();
		wlp.gravity = Gravity.CENTER;

		txtname.setText(name);
		merchant_nick_name.setText(nick);
		merchant_virtual_address.setText(virtual_address);
		merchant_desc.setText(desc);
		
		cancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		pay.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(MerchantListActivity.this,PaymentActivity.class);
				intent.putExtra("call_from", "merchant");
				intent.putExtra("nick", nick);
				intent.putExtra("virtual_address", virtual_address);
				intent.putExtra("id", id);
				startActivity(intent);
				finish();
			}
		});
		dialog.show();
	
	}
}
