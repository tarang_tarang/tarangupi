package com.tarang.upi.activity;
/*package org.npci.upi.security.commonsapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.npci.upi.security.pinactivitycomponent.CLConstants;
import org.npci.upi.security.services.CLRemoteResultReceiver;
import org.npci.upi.security.services.CLServices;
import org.npci.upi.security.services.ServiceConnectionStatusNotifier;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

*//**
 * A placeholder fragment containing a simple view.
 *//*

public class MainActivityFragment extends Fragment {

	private final String keyCode = "NPCI";
	Button button;
    Button rotateButton;
	TextView textResult;
	EditText transactionID;
	EditText transactionAmount;
	Spinner spinner;
	// LIST OF ARRAY STRINGS WHICH WILL SERVE AS LIST ITEMS
	ArrayList<String> listItems = new ArrayList<String>();
	// DEFINING A STRING ADAPTER WHICH WILL HANDLE THE DATA OF THE LISTVIEW
	ArrayAdapter<String> adapter;
	private ListView responseListView;
	private String xmlPayloadString = "";
	private String credAllowedString = "";
	private boolean isValidated = false;
	// Token is to be obtained from list keys API
	String token = "";
	boolean clInitialized = false;

    Context mContext = null;
    private CLServices clServices;

	public MainActivityFragment() {
		// sendPostRequest(keyCode);
	}

    *//**
     * Integrate the library for Registration/Rotation.
     * @param type "initial" for registartion, "rotate" for rotation
     * @throws Exception
     *//*
	private void integrateLibrary(String type) throws Exception {

		String app_id = CommonUtils.getAppId(this.getActivity());
		String mobile = CommonUtils.getMobileNumber();
		String device_id = CommonUtils.getDeviceId(this.getActivity());
         call get challenge service to get the challenge for registration
        String challenge = getChallenge(device_id, type);
        if (challenge == null) {
            Log.d(this.getClass().getName(), "Error while getting challenge.");
            return;
        }

        token = CommonUtils.getToken(challenge, type); // receive token from psp calling reqListKeys service
		Log.d("Token Received", token);
		if (!token.isEmpty()) {
             Populate Hmac for app registration
			String hmac = populateHMAC(app_id, mobile, token, device_id);
             Call register app service for registering psp app 
            clInitialized = clServices.registerApp(app_id, mobile, device_id, hmac);
            if (challenge == null) {
                Log.d(this.getClass().getName(), "Error while registering app");
                return;
            }
		}
    }

    *//**
     * Call Common Library Service to get the Challenge for registration / rotation
     * @param device_id current device id
     * @param type "initial" for registration, "rotate" for rotation
     * @return
     *//*
	public String getChallenge(String device_id,
							   String type) {
		String request_challenge = null;
        request_challenge = clServices.getChallenge(type, device_id);
		Log.d("challenge", request_challenge);
		return request_challenge;

	}

    *//**
     * Populate HMac for app registration
     * @param app_id
     * @param mobile
     * @param token
     * @param deviceId
     * @return
     *//*
	private String populateHMAC(String app_id, String mobile, String token,
								String deviceId) {
		String hmac = null;
		try {
			CryptLib cryptLib = new CryptLib();
			String message = app_id + "|" + mobile + "|" + deviceId;
			Log.d("PSP Hmac Msg", message);
			//String hashMessage = cryptLib.SHA256(message);
            byte[] tokenBytes = cryptLib.hexStringToByteArray(token);
			byte[] hmacBytes = cryptLib.encrypt(
                    cryptLib.SHA256(message),
                    tokenBytes);
            hmac = Base64.encodeToString(hmacBytes, Base64.NO_WRAP);
            Log.d("PSP Hmac", hmac);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hmac;
	}

	View rootView = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.fragment_main, container, false);
		 Calling app_init() for declaring the event and initialization 
		// app_init(rootView);
        mContext = getActivity();

        *//**
         * Initiate Common Library Services before using. If the service is successfully bound, serviceConnected
         * method of the ServiceConnectionStatusNotifier will be called.
         *//*
        CLServices.initService(mContext, new ServiceConnectionStatusNotifier() {
            @Override
            public void serviceConnected(CLServices services) {
                clServices = services;
                Log.d("CL Service", "Service connected");
                try {
                    integrateLibrary("initial");
                    if (!clInitialized) {
                        Toast.makeText(getActivity(), "App not registered with CL", Toast.LENGTH_LONG);
                    } else {
                        app_init(rootView);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void serviceDisconnected() {
                Log.d("CL Service", "Service disconnected");
                clServices = null;
            }
        });
		return rootView;

	}

	public void app_init(View container) {

		// Creating API Responses
		xmlPayloadString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><ns2:RespListKeys xmlns:ns2=\"http://npci.org/upi/schema/\"><Head msgId=\"5skv5ptRj1uBlSYA8Ny\" orgId=\"NPCI\" ts=\"2015-12-31T17:42:26+05:30\" ver=\"1.0\"/><Resp reqMsgId=\"d0kSkoT7PaUSbv0u\" result=\"SUCCESS\"/><Txn id=\"d0kSkoT7PaUStXEf\" ts=\"2015-12-31T17:42:23+05:30\"/><keyList><key code=\"NPCI\" ki=\"20150822\" owner=\"NPCI\" type=\"PKI\"><keyValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\">MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4rIIEHkJ2TYgO/JUJQI/sxDgbDEAIuy9uTf4DItWeIMsG9AuilOj9R+dwAv8S6/9No/z0cwsw4UnsHQG1ALVIxFznLizMjaVJ7TJ+yTS9C9bYEFakRqH8b4jje7SC7rZ9/DtZGsaWaCaDTyuZ9dMHrgcmJjeklRKxl4YVmQJpzYLrK4zOpyY+lNPBqs+aiwJa53ZogcUGBhx/nIXfDDvVOtKzNb/08U7dZuXoiY0/McQ7xEiFcEtMpEJw5EB4o3RhE9j/IQOvc7l/BfD85+YQ5rJGk4HUb6GrQXHzfHvIOf53l1Yb0IX4v9q7HiAyOdggO+PVzXMSbrcFBrEjGZD7QIDAQAB</keyValue></key></keyList><Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\"><SignedInfo><CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\"/><SignatureMethod Algorithm=\"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256\"/><Reference URI=\"\"><Transforms><Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\"/><DigestValue>GYiC65ThmzF0ka0DrD7/lLJuKcgTweu/DGNgp+n0JQE=</DigestValue></Reference></SignedInfo><SignatureValue>GjGo2YkxpUAOj+RLU3RUCLDhsPzaqHDEiUEOELjj0XYu9AuQEBEAvr6phgulwIi7zkmsrK/In47Y\n"
				+ "QqsVhn9MVJZM/gr8Uym7EtgJzvx8M8TsecKoRf8NNkex8fYpBmX728kuQ73RPpbinVhn+zlqDl55\n"
				+ "adDBevsucJNFkr0/fWDUX21coYYqZs5O/0OdHXBG+a/K5ruZ4DIsVaxYfbOMmZzVBjKHX0I88gqw\n"
				+ "zHpiIBonax2YEhOaoDF9NaKcwKqt+xNdOJZbSj6gCE0+tKUfcpY15Xv40jheoEXiNl8xJvjX8wkd\n"
				+ "SrIpNZFETpCpxnLl1dcBZFUrF+ZCZAyohmdGBw==</SignatureValue><KeyInfo><KeyValue><RSAKeyValue><Modulus>01DqzBsJTyMHT2S9MK5AIyFXNU646kwiOK3uymXIy9EW0nRKNKRkeIRTlGwX4wEnymGtGgX5B/Ij\n"
				+ "1elkLN4VJ9GplDV+wf0Lp2i2q4E6uRiWIzsqq42MCQgv8Fq/IPqjqPbeP9yh/8YPmBiMehBmhQd3\n"
				+ "qzl77C03k6d0yBIO5q/zXneTK9uFBNEL5yNpukrLGBcf3b9VHsjXpEaQrxGSMHCgNWpQgXpEcBr5\n"
				+ "OJ0/XxWbgMCZMlkYe1d6gswjuCRZ/xxJwEfbSO5AsnPtyqxSIjyhgEi9REtYnzaWwOBN4JCqt0pM\n"
				+ "L0ja23lUwVJuNwkwNGKBXvkGoXUln8Sf7PIv7w==</Modulus><Exponent>AQAB</Exponent></RSAKeyValue></KeyValue></KeyInfo></Signature></ns2:RespListKeys>";

		 This will be obtained from the response of ListAccountResponse API 
		credAllowedString = "{\n" + "\t\"CredAllowed\": [{\n"
				+ "\t\t\"type\": \"PIN\",\n" + "\t\t\"subtype\": \"MPIN\",\n"
				+ "\t\t\"dType\": \"ALPH | NUM\",\n" + "\t\t\"dLength\": 6\n" +
				// "\t}, {\n" +
				// "\t\t\"type\": \"OTP\",\n" +
				// "\t\t\"subtype\": \"OTP\",\n" +
				// "\t\t\"dType\": \"NUM\",\n" +
				// "\t\t\"dLength\": 6\n" +
				"\t}]\n" + "}";

        credAllowedString = "{\n" + "\t\"CredAllowed\": [" +
                  "{\n" +
                  //"\t\t\"type\": \"PIN\",\n" +
                  //"\t\t\"subtype\": \"MPIN\",\n" +
                  //"\t\t\"dType\": \"ALPH | NUM\",\n" +
                  //"\t\t\"dLength\": 6\n" +
                 //"\t}, {\n" +
                 "\t\t\"type\": \"OTP\",\n" +
                 "\t\t\"subtype\": \"SMS\",\n" +
                 "\t\t\"dType\": \"NUM\",\n" +
                 "\t\t\"dLength\": 4\n" +
                "\t}]\n" + "}";

		transactionID = (EditText) container
				.findViewById(R.id.editTransactionID);
		// Set a dynamic Transaction ID
		transactionID.setText(String.valueOf(new Random().nextInt(99999999)));

		transactionAmount = (EditText) container
				.findViewById(R.id.editTransactionAmount);

		// For response view
		responseListView = (ListView) container.findViewById(R.id.listView);
		spinner = (Spinner) container.findViewById(R.id.controls_spinner);
		adapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1, listItems);
		responseListView.setAdapter(adapter);

		// Pay button
		button = (Button) container.findViewById(R.id.button);
		button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!clInitialized) {
                    Toast.makeText(getActivity(), "App not registered with CL", Toast.LENGTH_LONG);
                    return;
                }
                callGetCredential();
            }
        });

        //rotateButton
        // Pay button
        rotateButton = (Button) container.findViewById(R.id.buttonRotate);
        rotateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!clInitialized) {
                    Toast.makeText(getActivity(), "App not registered with CL", Toast.LENGTH_LONG);
                    return;
                }
                try {
                    integrateLibrary("rotate");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (!clInitialized) {
                    Toast.makeText(getActivity(), "App not registered with CL", Toast.LENGTH_LONG);
                }
            }
        });
	}

    private void callGetCredential() {

        //Intent intent = new Intent(getActivity(),
        //        GetCredential.class);

        // Create Keycode
        //intent.putExtra("keyCode", keyCode);

        // Create xml payload
        if (xmlPayloadString.isEmpty()) {
            Toast.makeText(getActivity(),
                    "XML List Key API is not loaded.",
                    Toast.LENGTH_LONG).show();
            return;
        }
        //intent.putExtra("keyXmlPayload", xmlPayloadString);

        // Create Controls
        if (credAllowedString.isEmpty()) {
            Toast.makeText(getActivity(),
                    "Required Credentials could not be loaded.",
                    Toast.LENGTH_LONG).show();
            return;
        }
        //intent.putExtra("controls", credAllowedString);

        // Create Configuration
        JSONObject configuration = null;
        try {
            configuration = new JSONObject();
            configuration.put("payerBankName", "Indian Bank Ltd.");
            configuration.put("backgroundColor", "#FFFFFF");
            Log.i("configuration", configuration.toString());
            //intent.putExtra("configuration", configuration.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Create Salt
        JSONObject salt = null;
        try {
            salt = new JSONObject();
            salt.put("txnId", transactionID.getText().toString());
            salt.put("txnAmount", transactionAmount.getText()
                    .toString());
            salt.put("deviceId", CommonUtils.getDeviceId(getActivity()));
            salt.put("appId", CommonUtils.getAppId(getActivity()));
            salt.put("mobileNumber", CommonUtils.getMobileNumber());
            salt.put("payerAddr", "zeeshan.khan@sbi");
            salt.put("payeeAddr", "rohit.patekar@hdfc");
            Log.i("salt", salt.toString());
            //intent.putExtra("salt", salt.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String trustStr = null;
        try {
            StringBuilder trustParamBuilder = new StringBuilder(100);
            trustParamBuilder.append(transactionAmount.getText()
                    .toString()).append(CLConstants.SALT_DELIMETER)
                    .append(transactionID.getText().toString()).append(CLConstants.SALT_DELIMETER)
                    .append("zeeshan.khan@sbi").append(CLConstants.SALT_DELIMETER)
                    .append("rohit.patekar@hdfc").append(CLConstants.SALT_DELIMETER)
                    .append(CommonUtils.getAppId(getActivity())).append(CLConstants.SALT_DELIMETER)
                    .append(CommonUtils.getMobileNumber()).append(CLConstants.SALT_DELIMETER)
                    .append(CommonUtils.getDeviceId(getActivity()));
            trustStr = TrustCreator.createTrust(trustParamBuilder.toString(), token);
            //intent.putExtra("trust", trustStr);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Create Pay Info
        JSONArray payInfoArray = new JSONArray();
        try {
            JSONObject jsonPayeeName = new JSONObject();
            jsonPayeeName.put("name", "payeeName");
            jsonPayeeName.put("value", "Rohit Patkar");
            payInfoArray.put(jsonPayeeName);

            JSONObject txnAmount = new JSONObject();
            txnAmount.put("name", "txnAmount");
            txnAmount.put("value", transactionAmount.getText()
                    .toString());
            payInfoArray.put(txnAmount);

            JSONObject jsonNote = new JSONObject();
            jsonNote.put("name", "note");
            jsonNote.put("value", "Pay restaurant bill");
            payInfoArray.put(jsonNote);

            JSONObject jsonRefId = new JSONObject();
            jsonRefId.put("name", "refId");
            jsonRefId.put("value", transactionID.getText().toString());
            payInfoArray.put(jsonRefId);

            JSONObject jsonRefUrl = new JSONObject();
            jsonRefUrl.put("name", "refUrl");
            jsonRefUrl.put("value", "https://indianbank.com");
            payInfoArray.put(jsonRefUrl);

            JSONObject jsonAccount = new JSONObject();
            jsonAccount.put("name", "account");
            jsonAccount.put("value", "122XXX423");
            payInfoArray.put(jsonAccount);

            Log.i("payInfo", payInfoArray.toString());
            //intent.putExtra("payInfo", payInfoArray.toString());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        // Create Language Pref
        //intent.putExtra("languagePref", "en_US");

        *//**
         * create a CLRemoteResultReceiver instance to receive the encrypted credential.
         *//*
        CLRemoteResultReceiver remoteResultReceiver = new CLRemoteResultReceiver(new ResultReceiver(new Handler()) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                super.onReceiveResult(resultCode, resultData);
                parseResult(resultData);
            }
        });

        *//**
         * Call get Credential
         *//*
        clServices.getCredential(keyCode, xmlPayloadString, credAllowedString,
                configuration.toString(), salt.toString(), payInfoArray.toString(), trustStr, "en_US", remoteResultReceiver);
        //startActivityForResult(intent, 1);

    }

    *//**
     * Parse the output from Get Credential Service of Common Library
     * @param data bundle containing the output
     *//*
    private void parseResult(Bundle data) {
        String errorMsgStr = data.getString("error");
        if (errorMsgStr != null && !errorMsgStr.isEmpty()) {
            Log.d("Error:", errorMsgStr);
            try {
                JSONObject error = new JSONObject(errorMsgStr);
                String errorCode = error.getString("errorCode");
                String errorText = error.getString("errorText");
                Toast.makeText(getActivity().getApplicationContext(),
                        errorCode + ":" + errorText, Toast.LENGTH_LONG)
                        .show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return;
        }
        HashMap<String, String> credListHashMap = (HashMap<String, String>) data
                .getSerializable("credBlocks");
        for (String cred : credListHashMap.keySet()) {
        // This will return the list of field name e.g mpin,otp etc...
            try {
                JSONObject credBlock = new JSONObject(
                        credListHashMap.get(cred));
                Log.i("enc_msg", credBlock.toString());
                adapter.add("Control name : " + cred);
                adapter.add("keyId : "
                        + credBlock.getJSONObject("data").getString("code"));
                adapter.add("key index : "
                        + credBlock.getJSONObject("data").getString("ki"));
                adapter.add("Encrypted Message from Common Library: "
                        + credBlock.getJSONObject("data").getString(
                        "encryptedBase64String"));

                Log.i("enc_msg",
                        credBlock.getJSONObject("data").getString(
                                "encryptedBase64String"));
                // Log.i("enc_msg", credBlock.getString("message"));

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.i("Result Code:", String.valueOf(resultCode));
		if (requestCode == 1 && resultCode == Activity.RESULT_OK
				&& data != null) {
			parseResult(data.getExtras());
		}
	}
}
*/