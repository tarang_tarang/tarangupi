package psp.mobile.client.impl;

import psp.mobile.client.PspUriService;

public class PspUriServiceImpl implements PspUriService {

	private String pspServerUrl;
	
	public PspUriServiceImpl(){
	}
	
	public PspUriServiceImpl(String pspServerUrl){
		this.pspServerUrl = pspServerUrl;
	}

	@Override
	public String getPspServerUrl(String key){
		return pspServerUrl + CONTROLLER + key;
	}
	
}