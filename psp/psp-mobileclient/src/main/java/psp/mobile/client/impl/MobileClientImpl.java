package psp.mobile.client.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import psp.mobile.client.MobileClient;
import psp.mobile.client.PspUriService;
import psp.mobile.model.request.AddBankAccountRequest;
import psp.mobile.model.request.BalanceEnquiryRequest;
import psp.mobile.model.request.ChangePasswordRequest;
import psp.mobile.model.request.CheckUserRequest;
import psp.mobile.model.request.ForgetPasswordRequest;
import psp.mobile.model.request.GetAccountListRequest;
import psp.mobile.model.request.GetAccountProvidersRequest;
import psp.mobile.model.request.GetTxnDetailsRequest;
import psp.mobile.model.request.LoginRequest;
import psp.mobile.model.request.OtpRequest;
import psp.mobile.model.request.PaymentRequest;
import psp.mobile.model.request.RegistrationRequest;
import psp.mobile.model.request.SetPinRequest;
import psp.mobile.model.response.MessageResponse;

@Component("mobileToPsp")
public class MobileClientImpl implements MobileClient {

	@Autowired
	private PspUriService pspUriService;
	
	@Autowired
	private RestTemplate restTemplate;
	
	public MobileClientImpl(){
	}
	
	@Override
	public MessageResponse registration(RegistrationRequest request) {
		return restTemplate.postForObject(pspUriService.getPspServerUrl(PspUriService.REGISTRATION), request, MessageResponse.class);
	}

	@Override
	public MessageResponse login(LoginRequest request) {
		return restTemplate.postForObject(pspUriService.getPspServerUrl(PspUriService.LOGIN), request, MessageResponse.class);
	}

	@Override
	public MessageResponse changePassword(ChangePasswordRequest request) {
		return restTemplate.postForObject(pspUriService.getPspServerUrl(PspUriService.CHANGE_PWD), request, MessageResponse.class);
	}

	@Override
	public MessageResponse forgetPassword(ForgetPasswordRequest request) {
		return restTemplate.postForObject(pspUriService.getPspServerUrl(PspUriService.FORGET_PWD), request, MessageResponse.class);
	}

	@Override
	public MessageResponse getAccProviders(GetAccountProvidersRequest request) {
		return restTemplate.postForObject(pspUriService.getPspServerUrl(PspUriService.GET_ACC_PVDS), request, MessageResponse.class);
	}

	@Override
	public MessageResponse otpRequest(OtpRequest request) {
		return restTemplate.postForObject(pspUriService.getPspServerUrl(PspUriService.OTP_REQ), request, MessageResponse.class);
	}

	@Override
	public MessageResponse getAccDetails(GetAccountListRequest request) {
		return restTemplate.postForObject(pspUriService.getPspServerUrl(PspUriService.GET_ACC_LIST), request, MessageResponse.class);
	}

	@Override
	public MessageResponse setPin(SetPinRequest request) {
		return restTemplate.postForObject(pspUriService.getPspServerUrl(PspUriService.SET_PIN), request, MessageResponse.class);
	}

	@Override
	public MessageResponse makePayment(PaymentRequest request) {
		return restTemplate.postForObject(pspUriService.getPspServerUrl(PspUriService.MAKE_PAYMENT), request, MessageResponse.class);
	}

	@Override
	public MessageResponse getTxnDetails(GetTxnDetailsRequest request) {
		return restTemplate.postForObject(pspUriService.getPspServerUrl(PspUriService.GET_TXN_DETAILS), request, MessageResponse.class);
	}

	@Override
	public MessageResponse balEnquiry(BalanceEnquiryRequest request) {
		return restTemplate.postForObject(pspUriService.getPspServerUrl(PspUriService.BAL_ENQ), request, MessageResponse.class);
	}

	@Override
	public MessageResponse addBankAcc(AddBankAccountRequest request) {
		return restTemplate.postForObject(pspUriService.getPspServerUrl(PspUriService.ADD_BANK_ACC), request, MessageResponse.class);
	}

	@Override
	public MessageResponse isRegisteredUser(CheckUserRequest request) {		
		return restTemplate.postForObject(pspUriService.getPspServerUrl(PspUriService.IS_REGISTER_USER), request, MessageResponse.class);
	}

}