package psp.mobile.client;

import psp.common.model.constants.ServiceUris;

public interface PspUriService extends ServiceUris {

	String CONTROLLER = "/mobile";
	
	String REGISTRATION = "/registration";
	
	String LOGIN = "/login";
	
	String CHANGE_PWD = "/changePassword";
	
	String FORGET_PWD = "/forgetPassword";
	
	String GET_ACC_PVDS = "/getAccProviders";
	
	String OTP_REQ = "/otpRequest";
	
	String GET_ACC_LIST = "/getAccList";
	
	String SET_PIN = "/setPin";
	
	String MAKE_PAYMENT = "/makePayment";
	
	String GET_TXN_DETAILS = "/getTxnDetails";
	
	String BAL_ENQ = "/balEnquiry";
	
	String ADD_BANK_ACC = "/addBankAcc";
	
	String IS_REGISTER_USER = "/isRegisterUser";
	
	String getPspServerUrl(String key);
	
}