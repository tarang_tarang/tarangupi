package psp.mobile.client;

import psp.mobile.model.request.AddBankAccountRequest;
import psp.mobile.model.request.BalanceEnquiryRequest;
import psp.mobile.model.request.ChangePasswordRequest;
import psp.mobile.model.request.CheckUserRequest;
import psp.mobile.model.request.ForgetPasswordRequest;
import psp.mobile.model.request.GetAccountListRequest;
import psp.mobile.model.request.GetAccountProvidersRequest;
import psp.mobile.model.request.GetTxnDetailsRequest;
import psp.mobile.model.request.LoginRequest;
import psp.mobile.model.request.OtpRequest;
import psp.mobile.model.request.PaymentRequest;
import psp.mobile.model.request.RegistrationRequest;
import psp.mobile.model.request.SetPinRequest;
import psp.mobile.model.response.MessageResponse;

public interface MobileClient {

	MessageResponse registration(RegistrationRequest request);
	
	MessageResponse login(LoginRequest request);
	
	MessageResponse changePassword(ChangePasswordRequest request);
	
	MessageResponse forgetPassword(ForgetPasswordRequest request);
	
	MessageResponse getAccProviders(GetAccountProvidersRequest request);
	
	MessageResponse otpRequest(OtpRequest request);
	
	MessageResponse getAccDetails(GetAccountListRequest request);
	
	MessageResponse setPin(SetPinRequest request);
	
	MessageResponse makePayment(PaymentRequest request);
	
	MessageResponse getTxnDetails(GetTxnDetailsRequest request);
	
	MessageResponse balEnquiry(BalanceEnquiryRequest request);
	
	MessageResponse addBankAcc(AddBankAccountRequest request);
	
	MessageResponse isRegisteredUser(CheckUserRequest request);
}