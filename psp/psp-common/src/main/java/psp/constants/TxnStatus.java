/**
 * 
 */
package psp.constants;

/**
 * @author debaa
 *
 */
public enum TxnStatus {

	INITIATED,
	FAILED,
	COMPLETED;
}
