/**
 * 
 */
package psp.constants;

/**
 * @author manasp
 *
 */
public enum NotificationStatus {

	INITIATED,
	NOTIFIED;
	
	public static NotificationStatus getNotificationStatus(String notificationStatus){
		if(notificationStatus == null){
			return null;
		}
		else if (INITIATED.name().equals(notificationStatus)){
			return INITIATED;
		}
		else if (NOTIFIED.name().equals(notificationStatus)){
			return NOTIFIED;
		}
		else {
			return null;
		}
	}
}
