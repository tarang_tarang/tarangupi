package psp.constants;

public enum ServiceStatus {

	ACKNOWLEDGED,
	RECEIVED;
}
