package psp.constants;

public interface ServiceNotes {

	String OTP_NOTE = "otp request";
	
	String SET_CRED_NOTE = "setting credentials";
	
	String ACC_PROV_LIST_NOTE = "Account provider Listing";
	
	String ACC_LIST_NOTE = "Account Listing";
	
	String HEART_BEAT = "Heart beat";
	
	String PAYMENT_NOTE = "payment";
	
	String REG_MOB = "Mobile registration";
	
	String BAL_ENQ = "Balance enquiry";
	
}
