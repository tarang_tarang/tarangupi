package psp.constants;


public interface CommonConstants {

	String applicationName = "psp";
	
	String PSP_VERSION = "1.0";
	
	String UPI_VERSION = "1.0";

	//TODO: can go in cofig
	String ORG_ID = "700025";
	
	//TODO: need to check
	String PAYER_CODE = "4234";
	
	String PAYER_SEQ_NUM = "1";
	
	String TXN_ID = "1";
	
	String HEADER = "Accept=application/json";
	
	String PRODUCER = "application/json";

	String SUCCESS = "SUCCESS";
	
	String UPI_XML_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
	
	String SPACE_STR = " ";
	
	//TODO: go in cofig
	String REF_URL = "https://121.244.157.134/";
	
	String REF_ID = "TARANG01";
	
	String PRODUCT_ID = "TARANG";
	
	String PAYMENT_SUCCESS_MSG = "Payment successfully done.";
	
	String PAYMENT_FAILED_MSG = "Payment failed.";
	
	String SELF_PSP_NAME = "@tarang";
	
}