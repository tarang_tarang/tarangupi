/**
 * 
 */
package psp.constants;

/**
 * @author prasadj
 *
 */
public enum UserStatus {

	REGISTERED,
	ADD_BANK,
	SET_PIN,
	ACTIVE,
	DELETE;
	
	public static UserStatus getUserStatus(String us){
		if(us == null){
			return null;
		}
		else if (REGISTERED.name().equals(us)){
			return REGISTERED;
		}
		else if (ADD_BANK.name().equals(us)){
			return ADD_BANK;
		}
		else if (SET_PIN.name().equals(us)){
			return SET_PIN;
		}
		else if (ACTIVE.name().equals(us)){
			return ACTIVE;
		}
		else if (DELETE.name().equals(us)){
			return DELETE;
		}
		else {
			return null;
		}
	}
	
}