/**
 * 
 */
package psp.constants;

/**
 * @author prasadj
 *
 */
public interface PropertyName {

	String BANK_PROVIDERS_NUMBER = "bank.providers.number";
	
	String LOGIN_EXPIRY_DURATION = "login.expiry.duration";
	
	String MOBILE_PROCESS_DELAY =	"mobile.process.delay";
	
	String ORG_ID = "psp.server.orgId";
	
	String SELF_REFERENCE_URL = "self.reference.url";
	
	String AUTH_KEY = "gcm.authKey";
	
	String TIME_TO_LIVE = "gcm.timetolive";
	
}