package psp.constants;

public enum ServiceName {

	LOGIN,
	SET_PIN,
	GET_ACCOUNT_LIST,
	ADD_BANK_ACCOUNT,
	GET_TXN_DETAILS;
	
	public static ServiceName getUserStatus(String serviceName){
		if(serviceName == null){
			return null;
		}
		else if (LOGIN.name().equals(serviceName)){
			return LOGIN;
		}
		else if (SET_PIN.name().equals(serviceName)){
			return SET_PIN;
		}
		else if (GET_ACCOUNT_LIST.name().equals(serviceName)){
			return GET_ACCOUNT_LIST;
		}
		else if (ADD_BANK_ACCOUNT.name().equals(serviceName)){
			return ADD_BANK_ACCOUNT;
		}
		else if (GET_TXN_DETAILS.name().equals(serviceName)){
			return GET_TXN_DETAILS;
		}
		else {
			return null;
		}
	}
}
