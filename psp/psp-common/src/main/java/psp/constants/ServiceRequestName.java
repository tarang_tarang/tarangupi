package psp.constants;

public interface ServiceRequestName {

	String REQ_PAY = "ReqPay";
	
	String RESP_AUTH_DETAIL = "RespAuthDetails";
	
	String RESP_TXN_CONF = "RespTxnConfirmation";
	
	String REQ_TXN_CONF = "ReqTxnConfirmation";
	
	String REQ_LIST_ACC = "ReqListAccount";
	
	String REQ_BAL_ENQ = "ReqBalEnq";
	
	String REQ_TXN_CHK = "ReqChkTxn";
	
	String REQ_HBT = "ReqHbt";
	
	String REQ_LIST_PSP = "ReqListPsp";
	
	String REQ_LIST_ACC_PVD = "ReqListAccPvd";
	
	String REQ_LIST_KEYS = "ReqListKeys";
	
	String REQ_LIST_VAE = "ReqListVae";
	
	String REQ_MNG_VAE = "ReqManageVae";
	
	String REQ_OTP = "ReqOtp";
	
	String REQ_PEND_MSG = "ReqPendingMsg";
	
	String REQ_VAL_ADD = "ReqValAdd";
	
	String REQ_SET_CRED = "ReqSetCre";
	
	String REG_MOB = "RegMob";
	
	String REQ_MOB_REG = "ReqRegMob";
	
	String RESP_PAY = "RespPay";
	
	String REQ_AUTH_DETAIL = "ReqAuthDetails";
	
	String SET_CRED = "SetCre";
	
	String LIST_ACCOUNT = "ListAccount";
	
	String LIST_ACCOUNT_PROVIDER = "ListAccPvd";
	
	String HBT = "Hbt";
	
	String RESP_HBT = "RespHbt";
	
	String RESP_LIST_ACC_PVD = "RespListAccPvd";
	
	String RESP_BAL_ENQ = "RespBalEnq";
	
	String RESP_CHK_TXN = "RespChkTxn";
	
	String RESP_LIST_PSP = "RespListPsp";
	
	String RESP_LIST_ACCOUNT = "RespListAccount";
	
	String RESP_LIST_OF_KEYS = "RespListKeys";
	
	String RESP_LIST_VAE = "RespListVae";
	
	String RESP_MANAGE_VAE = "RespManageVae";
	
	String RESP_OTP = "RespOtp";
	
	String RESP_PENDING_MSG = "RespPendingMsg";
	
	String RESP_VAL_ADD = "RespValAdd";
	
	String RESP_SET_CRE = "RespSetCre";
	
	String RESP_REG_MOB = "RespRegMob";
	
}