/**
 * 
 */
package psp.constants;

/**
 * @author manasp
 *
 */
public enum TxnType {

	MERCHANT,
	VIRTUAL_ADDRESS,
	MOBILE,
	AADHAAR,
	BENEFICIARY;
	public static TxnType getTxnType(String txnType){
		if(txnType == null){
			return null;
		}
		else if (MERCHANT.name().equals(txnType)){
			return MERCHANT;
		}
		else if (VIRTUAL_ADDRESS.name().equals(txnType)){
			return VIRTUAL_ADDRESS;
		}
		else if (MOBILE.name().equals(txnType)){
			return MOBILE;
		}
		else if (AADHAAR.name().equals(txnType)){
			return AADHAAR;
		}
		else if (BENEFICIARY.name().equals(txnType)){
			return BENEFICIARY;
		}
		else {
			return null;
		}
	}
}
