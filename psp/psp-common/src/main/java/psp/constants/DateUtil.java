/**
 * 
 */
package psp.constants;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.xml.bind.DatatypeConverter;

/**
 * @author prasadj
 *
 */
public class DateUtil {

	private DateUtil(){
	}
	
	public static String getUpiDateStrFormat(Date date){
		SimpleDateFormat sdf = new SimpleDateFormat(CommonConstants.UPI_XML_DATE_FORMAT);
		return sdf.format(date);
	}
	
	public static Date getUpiDateStrFormat(String dateString){
		Calendar cal = DatatypeConverter.parseDateTime(dateString);
		return cal.getTime();
	}
	
}