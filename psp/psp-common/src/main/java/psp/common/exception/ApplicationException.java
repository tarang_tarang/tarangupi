package psp.common.exception;

import psp.constants.StatusCode;

public class ApplicationException extends Exception {

	private static final long serialVersionUID = 1L;
	
	private final StatusCode statusCode;
	
	public ApplicationException(StatusCode statusCode) {
		super(statusCode.getMessage());	
		this.statusCode = statusCode;
	}

	public StatusCode getStatusCode() {
		return statusCode;
	}	
	
}
