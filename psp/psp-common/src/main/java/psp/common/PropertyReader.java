package psp.common;


/**
 * @author prasadj
 *
 */
public interface PropertyReader {

	void load(); 
	
	String getValue(String propKey);
	
	int getBankProvidersNumber();
	
	int getLoginExpiryDuration();
	
	int getMobileProcessDelay();
	
	String getOrgId();
	
	String getReferenceUrl();
	
	String getAuthKey();
	
	int getTimeToLive();
	
}