package psp.common.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import psp.common.PropertyReader;
import psp.constants.PropertyName;
/**
 * @author prasadj
 *
 */
public class PropertyReaderImpl implements PropertyReader {

	private Properties projectProp = new Properties();

	private String filePath;
	
	PropertyReaderImpl(String filePath){
		System.out.println("constructed PropertyReaderImpl");
		this.filePath = filePath;
		load();
	}
	
	@Override
	public void load() {
		try {
			System.out.println("filePath: " + filePath);
	        final InputStream is = new FileInputStream(new File(filePath));
	        try {
	        	projectProp.load(is);
	        } finally {
	        	if(is != null){
	        		is.close();
	        	}
	        }
	    } 
		catch (Exception asd) {
	    	throw new RuntimeException("Error while reading from properties file.");
	    }
	}
	
	@Override
	public String getValue(String propKey) {
		return projectProp.getProperty(propKey);
	}
	
	@Override
	public int getBankProvidersNumber(){
		return getIntValue(projectProp.getProperty(PropertyName.BANK_PROVIDERS_NUMBER));
	}
	
	@Override
	public int getLoginExpiryDuration(){
		return getIntValue(projectProp.getProperty(PropertyName.LOGIN_EXPIRY_DURATION));
	}
	
	@Override
	public int getMobileProcessDelay(){
		return getIntValue(projectProp.getProperty(PropertyName.MOBILE_PROCESS_DELAY)) * 1000;
	}
	
	@Override
	public String getOrgId() {
		return getValue(PropertyName.ORG_ID);
	}

	@Override
	public String getAuthKey() {
		return getValue(PropertyName.AUTH_KEY);
	}

	@Override
	public int getTimeToLive() {
		return getIntValue(projectProp.getProperty(PropertyName.TIME_TO_LIVE));
	}
	
	@Override
	public String getReferenceUrl() {
		return getValue(PropertyName.SELF_REFERENCE_URL);
	}

	private int getIntValue(String intValue){
		try{
			return Integer.parseInt(intValue);
		}
		catch(Exception ex){
			return 0;
		}
	}

}