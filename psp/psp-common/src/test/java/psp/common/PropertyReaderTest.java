package psp.common;
/**
 * 
 */


import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import psp.common.PropertyReader;

/**
 * @author prasadj
 *
 */
@RunWith(SpringJUnit4ClassRunner.class) 
@ContextConfiguration(locations = {"classpath*:/test-applicationContext.xml", "classpath*:/psp-ApplicationContext.xml"})
public class PropertyReaderTest  {

	@Autowired
	private PropertyReader propertyReader;
	
	@Test
	public void getProcessBeanNameTest(){
		Assert.assertEquals(10, propertyReader.getLoginExpiryDuration());
		Assert.assertEquals(1, propertyReader.getBankProvidersNumber());
	}

}