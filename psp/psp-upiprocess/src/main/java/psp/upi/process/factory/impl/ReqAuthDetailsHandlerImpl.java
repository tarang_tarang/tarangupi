/**
 * 
 */
package psp.upi.process.factory.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.upi.system_1_2.PayConstant;
import org.upi.system_1_2.PayTrans;
import org.upi.system_1_2.PayeeType;
import org.upi.system_1_2.PayeesType;
import org.upi.system_1_2.PayerType;
import org.upi.system_1_2.ReqAuthDetails;
import org.upi.system_1_2.RespAuthDetails;
import org.upi.system_1_2.RespType;

import psp.constants.NotificationStatus;
import psp.constants.ServiceRequestName;
import psp.constants.StatusCode;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.AccountDetails;
import psp.dbservice.model.NotificationDetails;
import psp.dbservice.model.UpiMessagePacket;
import psp.dbservice.model.UserDetails;
import psp.upi.process.factory.UpiCoreHandler;
import psp.upi.process.util.TransactionThread;
import psp.upi.process.util.UpiProcessUtility;
import psp.util.DtoObjectUtil;
import psp.util.PspClientTool;
import psp.util.dto.AcDto;
import psp.util.dto.HeadDto;
import psp.util.dto.TxnDto;
import psp.util.gcm.GcmNotification;
import psp.util.upi.client.UpiClientService;
import psp.util.upiclient.UpiDataPreparationUtility;

import com.google.android.gcm.server.Result;

/**
 * @author prasadj
 *
 */
@Component("reqAuthDetailsHandler")
public class ReqAuthDetailsHandlerImpl extends UpiCoreHandler {

	@Autowired
	private PspMgmtService pspMgmtService;
	
	@Autowired
	private UpiClientService upiClientService;
	
	@Autowired
	private GcmNotification gcmNotification;
	
	public ReqAuthDetailsHandlerImpl(){
	}
	
	@Override
	public String handleProcess(String upiData) {
		ReqAuthDetails reqAuthDetails = PspClientTool.convertUpiRequest(upiData, ReqAuthDetails.class);
		PayTrans payTrans = reqAuthDetails.getTxn();
		List<StatusCode> scList = new ArrayList<>();
		if (payTrans.getType() == PayConstant.PAY) {
			PayeeType payeeType = reqAuthDetails.getPayees().getPayee().get(0);
			AccountDetails accountDetails = pspMgmtService.getAccountDetailsByVirtualAddr(payeeType.getAddr());
			if (accountDetails == null) {
				scList.add(StatusCode.ADDR_RESL_FAILED);
				prepareFailedPayRespnse( reqAuthDetails , StatusCode.ADDR_RESL_FAILED);
			}
			else {
				preparePayrequest(payeeType, accountDetails, reqAuthDetails);
			}
		}
		else if (payTrans.getType() == PayConstant.COLLECT) {
			AccountDetails payerAccountDetails = pspMgmtService.getAccountDetailsByVirtualAddr(reqAuthDetails.getPayer().getAddr());
			if (payerAccountDetails == null) {
				scList.add(StatusCode.ADDR_RESL_FAILED);
				prepareFailedPayRespnse( reqAuthDetails , StatusCode.ADDR_RESL_FAILED);
			}
			else {
				UpiMessagePacket upiMessagePacket = UpiProcessUtility.prepareUpiMessagePacket(upiData, reqAuthDetails.getTxn().getId());
				pspMgmtService.saveUpiMessagePacket(upiMessagePacket);
				NotificationDetails notificationDetails = prapareNotificationDetails(payTrans.getNote(), payerAccountDetails, reqAuthDetails.getPayees().getPayee().get(0).getName());
				notificationDetails.setUpiMessagePacket(upiMessagePacket);
				pspMgmtService.saveNotificationDetails(notificationDetails);				
				Result result = gcmNotification.send(notificationDetails.getNotificationId(), notificationDetails.getNotificationtType(), notificationDetails.getNotificationMessage(), notificationDetails.getRegistrationId());
				if (result.getMessageId() == null) {
					throw new RuntimeException(""); 
				} 
			}
		}
		return PspClientTool.requestToString(prepareAckData(ServiceRequestName.REQ_AUTH_DETAIL, reqAuthDetails.getHead().getMsgId(), scList));
	}

	private NotificationDetails prapareNotificationDetails(String note, AccountDetails payerAccountDetails, String payeeName){
		UserDetails userDetails = pspMgmtService.getUserDetails(payerAccountDetails.getUserDetails().getId());
		String message = "Hi "+userDetails.getFirstName()+", Mr. " + payeeName  + " initiated to collect money. added note: " + note;		
		NotificationDetails notificationDetails = new NotificationDetails();
		notificationDetails.setGeneratedTime(new Date());
		notificationDetails.setNotificationId(UpiProcessUtility.constructNotificationId());
		notificationDetails.setNotificationMessage(message);
		notificationDetails.setRegistrationId(userDetails.getRnsMpaId());
		notificationDetails.setStatus(NotificationStatus.INITIATED.name());
		notificationDetails.setUserName(userDetails.getUserName());
		notificationDetails.setNotificationtType(PayConstant.COLLECT.name());
		return notificationDetails;
	}
	
	private void preparePayrequest(PayeeType payeeType, AccountDetails accountDetails, ReqAuthDetails reqAuthDetails){
		Date date = new Date();
		String messageId = DtoObjectUtil.constructMessageId();
		HeadDto head = DtoObjectUtil.constructHeadDto(messageId, date);
		PayTrans payTrans = reqAuthDetails.getTxn();
		TxnDto txn = DtoObjectUtil.constructTxnDto(payTrans.getId(), payTrans.getNote(), null, payTrans.getRefId(), payTrans.getRefUrl(), date, payTrans.getType().name());

		AcDto acDto = new AcDto();//TODO need to change
		acDto.setAddrType(accountDetails.getAddressType());
		acDto.setAcnum(accountDetails.getAccountNumber());
		acDto.setActype(accountDetails.getAccountType());
		acDto.setIfsc(accountDetails.getIfsc());
		payeeType.setAc(UpiDataPreparationUtility.convertAc(acDto));
		
		PayeesType payeesType = new PayeesType();
		List<PayeeType> payeeTypes = payeesType.getPayee();
		payeeTypes.add(payeeType);
		
		RespType respType = new RespType();
		respType.setReqMsgId(reqAuthDetails.getHead().getMsgId());
		respType.setResult("SUCCESS");
		
		PayerType payerType = reqAuthDetails.getPayer();
		payeeType.setAmount(payerType.getAmount());
		
		RespAuthDetails respAuthDetails = new RespAuthDetails();
		respAuthDetails.setHead(UpiDataPreparationUtility.convertHeader(head));
		respAuthDetails.setTxn(UpiDataPreparationUtility.convertTxn(txn));
		respAuthDetails.setPayees(payeesType);
		respAuthDetails.setPayer(payerType);
		respAuthDetails.setResp(respType);
		
		TransactionThread transactionThread = new TransactionThread(respAuthDetails, ServiceRequestName.RESP_AUTH_DETAIL, upiClientService, null, null);
		new Thread(transactionThread).start();
	}

	private void prepareFailedPayRespnse( ReqAuthDetails reqAuthDetails, StatusCode statusCode){
		Date date = new Date();
		String messageId = DtoObjectUtil.constructMessageId();
		HeadDto head = DtoObjectUtil.constructHeadDto(messageId, date);
		PayTrans payTrans = reqAuthDetails.getTxn();
		TxnDto txn = DtoObjectUtil.constructTxnDto(payTrans.getId(), payTrans.getNote(), null, payTrans.getRefId(), payTrans.getRefUrl(), date, payTrans.getType().name());

		RespType respType = new RespType();
		respType.setReqMsgId(reqAuthDetails.getHead().getMsgId());
		respType.setResult("FAILURE");
		respType.setErrCode(statusCode.getCode());
		
		RespAuthDetails respAuthDetails = new RespAuthDetails();
		respAuthDetails.setHead(UpiDataPreparationUtility.convertHeader(head));
		respAuthDetails.setTxn(UpiDataPreparationUtility.convertTxn(txn));
		respAuthDetails.setResp(respType);
		respAuthDetails.setPayees(reqAuthDetails.getPayees());
		respAuthDetails.setPayer(reqAuthDetails.getPayer());
		
		TransactionThread transactionThread = new TransactionThread(respAuthDetails, ServiceRequestName.RESP_AUTH_DETAIL, upiClientService, null, null);
		new Thread(transactionThread).start();
	}
}