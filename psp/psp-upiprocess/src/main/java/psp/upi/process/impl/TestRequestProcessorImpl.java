/**
 * 
 */
package psp.upi.process.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import psp.constants.ServiceRequestName;
import psp.mobile.model.response.MessageResponse;
import psp.resp.fromupi.AccountListResponse;
import psp.resp.fromupi.AccountProviderListResponse;
import psp.resp.fromupi.AckResponse;
import psp.resp.fromupi.BalanceResponse;
import psp.resp.fromupi.HeartBeatResponse;
import psp.resp.fromupi.KeysListResponse;
import psp.resp.fromupi.ManageVerifiedAddressResponse;
import psp.resp.fromupi.MobileRegistrationResponse;
import psp.resp.fromupi.OtpResponse;
import psp.resp.fromupi.PendingMsgResponse;
import psp.resp.fromupi.PspListResponse;
import psp.resp.fromupi.SetCredentialResponse;
import psp.resp.fromupi.TxnConfirmationResponse;
import psp.resp.fromupi.VerifiedAddressListResponse;
import psp.resp.fromupi.VerifiedAddressResponse;
import psp.upi.process.TestRequestProcessor;
import psp.upi.process.util.SampleData;
import psp.util.PspClientTool;
import upi.client.UpiClient;

/**
 * @author manasp
 *
 */
@Component("testRequestProcessor")
public class TestRequestProcessorImpl implements TestRequestProcessor, ServiceRequestName {

	@Autowired
	private UpiClient upiClient;
	
	@Override
	public Object requestByServiceName(String serviceName) {
		String request = prepareRequest(serviceName);
		if (request == null) {
			MessageResponse messageResponse = new MessageResponse();
			messageResponse.setStatusMessage("Service not allowed");
			messageResponse.setStatusCode("404");
			return messageResponse;
		}
		String response = upiClient.reqUpi(request, serviceName, "1.0", "12");
		return prepareResponse(response,serviceName);
	}
	
	private String prepareRequest(String serviceName) {
		switch (serviceName) {
			case REQ_BAL_ENQ:
				return SampleData.BALANCE_REQUEST;
			case REQ_LIST_ACC_PVD:
				return SampleData.ACCOUNT_PROVIDER_LIST_REQUEST;
			case REQ_LIST_KEYS:
				return SampleData.KEYS_LIST_REQUEST;
			case REQ_LIST_ACC:
				return SampleData.ACCOUNT_LIST_REQUEST;
			case REQ_LIST_VAE:
				return SampleData.VERIFIED_ADDRESS_LIST_REQUEST;
			case REQ_MNG_VAE:
				return SampleData.MANAGED_VERIFIED_ADDRESS_LIST_REQUEST;
			case REQ_VAL_ADD:
				return SampleData.VALIDATE_ADDRESS_REQUEST;
			case REQ_SET_CRED:
				return SampleData.SET_CREDENTIAL_REQUEST;
			case REQ_MOB_REG:
				return SampleData.MOBILE_REG_REQUEST;
			case REQ_TXN_CHK:
				return SampleData.CHK_TXN_REQUEST;
			case REQ_OTP:
				return SampleData.OTP_REQUEST;
			case REQ_HBT:
				return SampleData.HEART_BEAT_REQUEST;
			case REQ_PEND_MSG:
				return SampleData.PENDING_MESSAGE_REQUEST;
			case REQ_LIST_PSP:
				return SampleData.PSP_LIST_REQUEST;
			case REQ_PAY:
				return SampleData.PAY_REQUEST;
			/*case "RespAuthDetails":
				return SampleData.PAY_REQUEST;
			case "RespTxnConfirmation":
				return SampleData.RESP_TXN_CONFIRM;*/
			default:
				return null;
		}
	}
	
	private Object prepareResponse(String response, String serviceName) {
		switch (serviceName) {
			case REQ_BAL_ENQ:
				return PspClientTool.convertUpiRequest(response, BalanceResponse.class);
			case REQ_LIST_ACC_PVD:
				return PspClientTool.convertUpiRequest(response, AccountProviderListResponse.class);
			case REQ_LIST_KEYS:
				return PspClientTool.convertUpiRequest(response, KeysListResponse.class);
			case REQ_LIST_ACC:
				return PspClientTool.convertUpiRequest(response, AccountListResponse.class);
			case REQ_LIST_VAE:
				return PspClientTool.convertUpiRequest(response, VerifiedAddressListResponse.class);
			case REQ_MNG_VAE:
				return PspClientTool.convertUpiRequest(response, ManageVerifiedAddressResponse.class);
			case REQ_VAL_ADD:
				return PspClientTool.convertUpiRequest(response, VerifiedAddressResponse.class);
			case REQ_SET_CRED:
				return PspClientTool.convertUpiRequest(response, SetCredentialResponse.class);
			case REQ_MOB_REG:
				return PspClientTool.convertUpiRequest(response, MobileRegistrationResponse.class);
			case REQ_TXN_CHK:
				return PspClientTool.convertUpiRequest(response, TxnConfirmationResponse.class);
			case REQ_OTP:
				return PspClientTool.convertUpiRequest(response, OtpResponse.class);
			case REQ_HBT:
				return PspClientTool.convertUpiRequest(response, HeartBeatResponse.class);
			case REQ_PEND_MSG:
				return PspClientTool.convertUpiRequest(response, PendingMsgResponse.class);
			case REQ_LIST_PSP:
				return PspClientTool.convertUpiRequest(response, PspListResponse.class);
			default:
				return PspClientTool.convertUpiRequest(response, AckResponse.class);
		}
	}
}
