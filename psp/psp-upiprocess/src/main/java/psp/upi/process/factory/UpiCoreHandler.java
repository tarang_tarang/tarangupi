/**
 * 
 */
package psp.upi.process.factory;


import java.util.Date;
import java.util.List;

import psp.constants.DateUtil;
import psp.constants.StatusCode;

import org.upi.system_1_2.Ack;
import org.upi.system_1_2.ErrorMessage;


/**
 * @author prasadj
 *
 */
public abstract class UpiCoreHandler {

	public UpiCoreHandler(){
	}

	public abstract String handleProcess(String upiData);
	
	public Ack prepareAckData(String serviceName, String msgId, List<StatusCode> statusCode) {
		Ack ack = new Ack();
		ack.setApi(serviceName);
		ack.setReqMsgId(msgId);
		ack.setTs(DateUtil.getUpiDateStrFormat(new Date()));
		if (statusCode != null && statusCode.size() > 0) {
			List<ErrorMessage> errorMessages = ack.getErrorMessages();
			for (StatusCode sc : statusCode) {
				ErrorMessage errorMessage = new ErrorMessage();
				errorMessage.setErrorCd(sc.getCode());
				errorMessage.setErrorDtl(sc.getMessage());
				errorMessages.add(errorMessage);
			}
		}
		return ack;
	}

}