/**
 * 
 */
package psp.upi.process.factory;

/**
 * @author prasadj
 *
 */
public interface UpiHandlerFactory {

	UpiCoreHandler getHandler(String name);
	
}