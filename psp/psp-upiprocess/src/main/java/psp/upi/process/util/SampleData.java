/**
 * 
 */
package psp.upi.process.util;

/**
 * @author manasp
 *
 */
public class SampleData {

	public static String HEAD = "<Head ver='1.0' ts='2015-02-16T22:02:28+05:30' orgId='400001' msgId='HENSVVR4QOS7X1UGPY7JGZZ444PL9T2C3QM'/>";
	
	public static String RESP = "<Resp reqMsgId='HENZVVR4QOS7X1UGPY7JGZZ444PL9T2C3QM' result='SUCCESS' errCode=''/>";
	
	public static String PSP_LIST_REQUEST = "<upi:ReqListPsp xmlns:upi='http://npci.org/upi/schema/'>" + HEAD + prepareTxn("Listing PSP�s", "ListPSP") + "</upi:ReqListPsp>";
	
	public static String ACCOUNT_PROVIDER_LIST_REQUEST = "<upi:ReqListAccPvd xmlns:upi='http://npci.org/upi/schema/'>" + HEAD + prepareTxn("Account provider Listing", "ListAccPvd")+ "</upi:ReqListAccPvd>";
	
	public static String KEYS_LIST_REQUEST = "<upi:ReqListKeys xmlns:upi='http://npci.org/upi/schema/'>" + HEAD + prepareTxn("Account provider Listing", "ListKeys")+ "</upi:ReqListKeys>";
	
	public static String ACCOUNT_LIST_REQUEST = "<upi:ReqListAccount xmlns:upi='http://npci.org/upi/schema/'>" + HEAD + prepareTxn("Account Listing", "ListAccount")
			+ "<Payer addr='Zeeshan.khan@hdfc' name='Mohd Zeeshan Khan' seqNum='1' type='PERSON' code='4814'>"
			+ "<Link type='MOBILE' value='9383828283'/>"
			+ "<Ac addrType='ACCOUNT'><Detail name='IFSC' value='HDFC'/><Detail name='ACTYPE' value='SAVINGS'/><Detail name='ACNUM' value='5678892001828'/></Ac>"
			+ "<Creds><Cred type='OTP' subtype='SMS'><Data> base-64 encoded/encrypted authentication data</Data></Cred></Creds>"
			+ "</upi:ReqListAccount>";
	
	public static String VERIFIED_ADDRESS_LIST_REQUEST = "<upi:ReqListVae xmlns:upi='http://npci.org/upi/schema/'>" + HEAD + prepareTxn("Account provider Listing", "ListVae")+ "</upi:ReqListVae>";
	
	public static String MANAGED_VERIFIED_ADDRESS_LIST_REQUEST = "<upi:ReqManageVae xmlns:upi='http://npci.org/upi/schema/'>" + HEAD + prepareTxn("Account provider Listing", "ManageVae")
			+ "<VaeList><Vae op='UPDATE' name='LIC' addr='lic@hdfc' logo='image' url='www.lic.co.in'/></VaeList>"
			+ "</upi:ReqManageVae>";
	
	public static String OTP_REQUEST = "<upi:ReqOtp xmlns:upi='http://npci.org/upi/schema/'>" + HEAD + prepareTxn("Account provider Listing", "ReqOtp")
			+ "<Payer addr='Zeeshan.khan@hdfc' name='Mohd Zeeshan Khan' seqNum='1' type='PERSON' code='4814'>"
			+ "<Device><Tag name='MOBILE' value='+91.99999.00000'/></Device>"
			+ "<Ac addrType='ACCOUNT'><Detail name='IFSC' value='HDFC'/><Detail name='ACTYPE' value='SAVINGS'/><Detail name='ACNUM' value='5678892001828'/></Ac>"
			+ "</payer>"
			+ "</upi:ReqOtp>";
	
	public static String PENDING_MESSAGE_REQUEST = "<upi:ReqPendingMsg xmlns:upi='http://npci.org/upi/schema/'>" + HEAD + prepareTxn("Request pending Msg", "ReqPendingMsg") 
			+ "<ReqMsg type='MOBILE' value='9293837234' addr='zeeshan.khan@hdfc' />"
			+ "</upi:ReqPendingMsg>";
	
	public static String VALIDATE_ADDRESS_REQUEST = "<upi:ReqValAdd xmlns:upi='http://npci.org/upi/schema/'>" + HEAD + prepareTxn("validating Address", "ValAdd")
			+ "<Payer addr='Zeeshan.khan@hdfc' name='Mohd Zeeshan Khan' seqNum='1' type='PERSON' code='4814'>"
			+ "<Info><Identity type='ACCOUNT' verifiedName='Zeeshan Khan' id='7389923983430'/><Rating VerifiedAddress='TRUE'/></Info>"
			+ "</payer>"
			+ "<Payee addr='Zeeshan.khan@hdfc' name='Mohd Zeeshan Khan' seqNum='1'>"
			+ "</upi:ReqValAdd>";
	
	public static String SET_CREDENTIAL_REQUEST = "<upi:ReqSetCre xmlns:upi='http://npci.org/upi/schema/'>" + HEAD + prepareTxn("setting credentials", "SetCre")
			+ "<Payer addr='Zeeshan.khan@hdfc' name='Mohd Zeeshan Khan' seqNum='1' type='PERSON' code='4814'>"
			+ "<Ac addrType='ACCOUNT'><Detail name='IFSC' value='HDFC'/><Detail name='ACTYPE' value='SAVINGS'/><Detail name='ACNUM' value='5678892001828'/></Ac>"
			+ "<Creds><Cred type='OTP' subtype='SMS'><Data> base-64 encoded/encrypted authentication data</Data></Cred></Creds>"
			+ "<NewCred><Data> base-64 encoded/encrypted authentication data</Data></Cred></NewCred>"
			+ "</upi:ReqSetCre>";
	
	public static String MOBILE_REG_REQUEST = "<upi:ReqRegMob xmlns:upi='http://npci.org/upi/schema/'>" + HEAD + prepareTxn("Account provider Listing", "ReqOtp")
			+ "<Payer addr='Zeeshan.khan@hdfc' name='Mohd Zeeshan Khan' seqNum='1' type='PERSON' code='4814'>"
			+ "<Device><Tag name='MOBILE' value='+91.99999.00000'/></Device>"
			+ "<Ac addrType='ACCOUNT'><Detail name='IFSC' value='HDFC'/><Detail name='ACTYPE' value='SAVINGS'/><Detail name='ACNUM' value='5678892001828'/></Ac>"
			+ "</payer>"
			+ "<RegDetails><Detail name='MOBILE' value='+91.99999.00000'/>"
			+ "<Creds><Cred type='OTP' subtype='SMS'><Data> base-64 encoded/encrypted authentication data</Data></Cred></Creds>"
			+ "</RegDetails>"
			+ "</upi:ReqRegMob>";
	
	public static String TXN_CONFIRMATION_REQUEST = "<upi:ReqTxnConfirmation xmlns:upi='http://npci.org/upi/schema/'>" + HEAD + prepareTxn("Restaurant Bill", "txnConfirmation")
			+ "<TxnConfirmation note='Birthday Party contribution' orgStatus='PENDING' orgErrCode='debit unsuccessful#relevant error code for original txn' type='COLLECT'/>"
			+ "</upi:ReqTxnConfirmation>";
	
	public static String BALANCE_REQUEST = "<upi:ReqBalEnq xmlns:upi='http://npci.org/upi/schema/'>" + HEAD + prepareTxn("Balance enquiry", "BalEnq")
			+ "<Payer addr='Zeeshan.khan@hdfc' name='Mohd Zeeshan Khan' seqNum='1' type='PERSON' code='4814'><Bal><Data code='' ki=''> base-64 encoded/encrypted data </Data></Bal></Payer>"
			+ "<Info><Identity type='ACCOUNT' verifiedName='Zeeshan Khan' id='7389923983430'/><Rating VerifiedAddress='TRUE'/></Info>"
			+ "<Device><Tag name='MOBILE' value='+91.99999.00000'/></Device>"
			+ "<Ac addrType='ACCOUNT'><Detail name='IFSC' value='HDFC'/><Detail name='ACTYPE' value='SAVINGS'/><Detail name='ACNUM' value='5678892001828'/></Ac>"
			+ "<Creds><Cred type='OTP' subtype='SMS'><Data> base-64 encoded/encrypted authentication data</Data></Cred></Creds>"
			+ "</payer>"
			+ "</upi:ReqBalEnq>";
	
	public static String CHK_TXN_REQUEST = "<upi:ReqChkTxn xmlns:upi='http://npci.org/upi/schema/'>" + HEAD + prepareTxn("Restaurant Bill", "ChkTxn")
			+ "</upi:ReqChkTxn>";
	
	public static String HEART_BEAT_REQUEST = "<upi:ReqHbt xmlns:upi='http://npci.org/upi/schema/'>" + HEAD + prepareTxn("Heart beat", "Hbt")+
			"<HbtMsg type='ALIVE' value='NA'/>"
			+"</upi:ReqHbt>";
	
	public static String PAY_REQUEST = "<upi:ReqPay xmlns:upi='http://npci.org/upi/schema/'>" + HEAD + prepareTxn("Restaurant Bill", "PAY")
			+ "<Payer addr='Zeeshan.khan@hdfc' name='Mohd Zeeshan Khan' seqNum='1' type='PERSON' code='4814'>"
			+ "<Info><Identity type='ACCOUNT' verifiedName='Zeeshan Khan' id='7389923983430'/><Rating VerifiedAddress='TRUE'/></Info>"
			+ "<Device><Tag name='MOBILE' value='+91.99999.00000'/></Device>"
			+ "<Ac addrType='ACCOUNT'><Detail name='IFSC' value='HDFC'/><Detail name='ACTYPE' value='SAVINGS'/><Detail name='ACNUM' value='5678892001828'/></Ac>"
			+ "<Creds><Cred type='OTP' subtype='SMS'><Data> base-64 encoded/encrypted authentication data</Data></Cred></Creds>"
			+ "<Amount value='4000' curr='INR'></Amount>"
			+ "<Payees><Payee addr='Zeeshan.khan@hdfc' name='' seqNum='1' type='' code=''></Payees>"
			+ "</upi:ReqPay>";
	
	public static String RESP_AUTH_DETAILS = "<upi:RespAuthDetails xmlns:upi='http://npci.org/upi/schema/'>" + HEAD + prepareTxn("Restaurant Bill", "PAY")
			+ "<Payer addr='Zeeshan.khan@hdfc' name='Mohd Zeeshan Khan' seqNum='1' type='PERSON' code='4814'>"
			+ "<Info><Identity type='ACCOUNT' verifiedName='Zeeshan Khan' id='7389923983430'/><Rating VerifiedAddress='TRUE'/></Info>"
			+ "<Amount value='4000' curr='INR'></Amount>"
			+ "<Payees>"
			+ "<Payee addr='rohit.patekar@sbi' name='Rohit Patekar' seqNum='1' type='PERSON' code='4814'>"
			+ "<Info><Identity type='ACCOUNT' verifiedName='Rohit Patekar' id='8392389838'/><Rating VerifiedAddress='TRUE'/></Info>"
			+ "<Ac addrType='ACCOUNT'><Detail name='IFSC' value='UBIN0288100'/><Detail name='ACTYPE' value='SAVINGS'/><Detail name='ACNUM' value='5678892001828'/></Ac>"
			+ "<Amount value='4000' curr='INR'></Amount>"
			+ "</Payees>"
			+ "</upi:RespAuthDetails>";
	
	public static String RESP_TXN_CONFIRM = "<upi:RespTxnConfirmation xmlns:upi='http://npci.org/upi/schema/'>" + HEAD + prepareTxn("Restaurant Bill", "TxnConfirmation")
			+ "<Payer addr='Zeeshan.khan@hdfc' name='Mohd Zeeshan Khan' seqNum='1' type='PERSON' code='4814'>"
			+ "<Info><Identity type='ACCOUNT' verifiedName='Zeeshan Khan' id='7389923983430'/><Rating VerifiedAddress='TRUE'/></Info>"
			+ "<Amount value='4000' curr='INR'></Amount>"
			+ "<Payees>"
			+ "<Payee addr='rohit.patekar@sbi' name='Rohit Patekar' seqNum='1' type='PERSON' code='4814'>"
			+ "<Info><Identity type='ACCOUNT' verifiedName='Rohit Patekar' id='8392389838'/><Rating VerifiedAddress='TRUE'/></Info>"
			+ "<Ac addrType='ACCOUNT'><Detail name='IFSC' value='UBIN0288100'/><Detail name='ACTYPE' value='SAVINGS'/><Detail name='ACNUM' value='5678892001828'/></Ac>"
			+ "<Amount value='4000' curr='INR'></Amount>"
			+ "</Payees>"
			+ "</upi:RespTxnConfirmation>";
	
	private static String prepareTxn(String note, String type) {
		return "<Txn id='HENZVVR4QOS7X1UGPY7JGZZ444PL9T2C3QM' note='"+ note +"' refId='' refUrl='' ts='2015-02-16T22:02:25+05:30' type='"+ type +"'/>";
	}
}
