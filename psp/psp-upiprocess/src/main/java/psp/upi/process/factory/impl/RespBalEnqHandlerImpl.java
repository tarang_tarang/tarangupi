/**
 * 
 */
package psp.upi.process.factory.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.upi.system_1_2.RespBalEnq;

import psp.constants.ServiceRequestName;
import psp.constants.ServiceStatus;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.BalanceEnquiryUpiRequest;
import psp.upi.process.factory.UpiCoreHandler;
import psp.util.PspClientTool;

/**
 * @author prasadj
 *
 */
@Component("respBalEnqHandler")
public class RespBalEnqHandlerImpl extends UpiCoreHandler {

	@Autowired
	private PspMgmtService pspMgmtService;

	public RespBalEnqHandlerImpl(){
	}
	
	@Override
	public String handleProcess(String upiData) {
		RespBalEnq respBalEnq = PspClientTool.convertUpiRequest(upiData, RespBalEnq.class);
		BalanceEnquiryUpiRequest requestDetails = (BalanceEnquiryUpiRequest) pspMgmtService.getUpiRequestByTxnId(respBalEnq.getTxn().getId());
		if(null != requestDetails) {
			requestDetails.setStatus(ServiceStatus.RECEIVED.name());
			requestDetails.setResult(respBalEnq.getResp().getResult());
			requestDetails.setErrMsg(respBalEnq.getResp().getErrCode());
			requestDetails.setResponseTimeFromUpi(new Date());
			if (respBalEnq.getPayer() != null) {
				requestDetails.setBalance(respBalEnq.getPayer().getBal().getData().getValue());
			}
			pspMgmtService.updateUpiRequest(requestDetails);
		}
		return PspClientTool.requestToString(prepareAckData(ServiceRequestName.RESP_BAL_ENQ, respBalEnq.getHead().getMsgId(), null));
	}

}