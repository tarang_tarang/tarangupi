/**
 * 
 */
package psp.upi.process.util;

import java.util.Date;

import org.upi.system_1_2.Ack;
import org.upi.system_1_2.RespAuthDetails;
import org.upi.system_1_2.RespBalEnq;
import org.upi.system_1_2.RespListAccount;
import org.upi.system_1_2.RespOtp;
import org.upi.system_1_2.RespPay;
import org.upi.system_1_2.RespRegMob;
import org.upi.system_1_2.RespTxnConfirmation;

import psp.constants.DateUtil;
import psp.constants.ServiceRequestName;
import psp.dbservice.model.NotificationDetails;
import psp.util.PspClientTool;
import psp.util.gcm.GcmNotification;
import psp.util.upi.client.UpiClientService;

import com.google.android.gcm.server.Result;

/**
 * @author manasp
 *
 */
public class TransactionThread implements Runnable {

	private Object object;
	
	private String serviceName;
	
	private UpiClientService upiClientService;
	
	private GcmNotification gcmNotification;
	
	private NotificationDetails notificationDetails;
	
	public TransactionThread(){
	}
	
	public TransactionThread(Object object, String serviceName, UpiClientService upiClientService, GcmNotification gcmNotification, NotificationDetails notificationDetails){
		this.object = object;
		this.serviceName = serviceName;
		setUpiClientService(upiClientService);
		setGcmNotification(gcmNotification);
		setNotificationDetails(notificationDetails);
	}
	
	@Override
	public void run() {
		
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			throw new RuntimeException(e.getMessage());
		}
		
		if (serviceName == ServiceRequestName.RESP_AUTH_DETAIL) {
			processResponseAuthDetails();
		}
		else if (serviceName == ServiceRequestName.RESP_TXN_CONF) {
			processResponseTxnConf();
		}
		else if (serviceName == ServiceRequestName.RESP_OTP) {
			processResponseRespOtp();
		}
		else if (serviceName == ServiceRequestName.RESP_PAY) {
			processResponsePay();
		}
		else if (serviceName == ServiceRequestName.RESP_LIST_ACCOUNT) {
			processRespListAccount();
		}
		else if (serviceName == ServiceRequestName.RESP_REG_MOB) {
			processRespRegMobile();
		}
		else if (serviceName == ServiceRequestName.RESP_BAL_ENQ) {
			processRespBalEnq();
		}
	}

	private void processResponseAuthDetails() {
		RespAuthDetails respAuthDetails = (RespAuthDetails)object;
		String responseXml = upiClientService.respAuthDetails(respAuthDetails);
		Ack ack = PspClientTool.convertUpiRequest(responseXml, Ack.class);
		if(ack.getErr() == null) {
			
		}
	}

	private void processResponseTxnConf() {
		RespTxnConfirmation respTxnConfirmation = (RespTxnConfirmation)object;
		String responseXml = upiClientService.respTxnConfirmation(respTxnConfirmation);
		Ack ack = PspClientTool.convertUpiRequest(responseXml, Ack.class);
		if(ack.getErr() == null && notificationDetails != null) {
			Result result = gcmNotification.send(notificationDetails.getNotificationId(), notificationDetails.getNotificationtType(), notificationDetails.getNotificationMessage(), notificationDetails.getRegistrationId());
			if (result.getMessageId() == null) {
				throw new RuntimeException(""); 
			} 
		}
	}
	
	private void processResponseRespOtp() {
		RespOtp respOtp = (RespOtp)object;
		String responseXml = upiClientService.respOtp(respOtp);
		Ack ack = PspClientTool.convertUpiRequest(responseXml, Ack.class);
		if(ack.getErr() == null) {
			//TODO do opertion
		}
	}
	
	private void processResponsePay() {
		RespPay respPay = (RespPay)object;
		String responseXml = upiClientService.respPay(respPay);
		Ack ack = PspClientTool.convertUpiRequest(responseXml, Ack.class);
		if(ack.getErr() == null) {
			//TODO do opertion
		}
	}
	
	private void processRespListAccount() {
		RespListAccount respListAccount = (RespListAccount)object;
		String responseXml = upiClientService.respListAccount(respListAccount);
		Ack ack = PspClientTool.convertUpiRequest(responseXml, Ack.class);
		if(ack.getErr() == null) {
			//TODO do opertion
		}
	}
	
	private void processRespRegMobile() {
		RespRegMob respRegMob = (RespRegMob)object;
		String responseXml = upiClientService.respRegMob(respRegMob);
		Ack ack = PspClientTool.convertUpiRequest(responseXml, Ack.class);
		if(ack.getErr() == null) {
			//TODO do opertion
		}
	}
	
	private void processRespBalEnq() {
		RespBalEnq respBalEnq = (RespBalEnq)object;
		String responseXml = upiClientService.respBalEnq(respBalEnq);
		Ack ack = PspClientTool.convertUpiRequest(responseXml, Ack.class);
		if(ack.getErr() == null) {
			//TODO do opertion
		}
	}
	
	public Ack prepareAckData(String serviceName, String msgId) {
		Ack ack = new Ack();
		ack.setApi(serviceName);
		ack.setReqMsgId(msgId);
		ack.setTs(DateUtil.getUpiDateStrFormat(new Date()));
		return ack;
	}
	
	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public UpiClientService getUpiClientService() {
		return upiClientService;
	}

	public void setUpiClientService(UpiClientService upiClientService) {
		this.upiClientService = upiClientService;
	}

	public GcmNotification getGcmNotification() {
		return gcmNotification;
	}

	public void setGcmNotification(GcmNotification gcmNotification) {
		this.gcmNotification = gcmNotification;
	}

	public NotificationDetails getNotificationDetails() {
		return notificationDetails;
	}

	public void setNotificationDetails(NotificationDetails notificationDetails) {
		this.notificationDetails = notificationDetails;
	}

}
