/**
 * 
 */
package psp.upi.process.factory.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.upi.system_1_2.RespListAccPvd;
import org.upi.system_1_2.RespListAccPvd.AccPvdList.AccPvd;

import psp.constants.DateUtil;
import psp.constants.ServiceRequestName;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.AccountProviderDetails;
import psp.upi.process.factory.UpiCoreHandler;
import psp.util.PspClientTool;

/**
 * @author prasadj
 *
 */
@Component("respListAccPvdHandler")
public class RespListAccPvdHandlerImpl extends UpiCoreHandler {

	@Autowired
	private PspMgmtService pspMgmtService;

	public RespListAccPvdHandlerImpl(){
	}
	
	@Override
	public String handleProcess(String upiData) {
		RespListAccPvd hbr = PspClientTool.convertUpiRequest(upiData, RespListAccPvd.class);
		
		List<AccountProviderDetails> reqAccProvs = prepareAccountProvider(hbr.getAccPvdList().getAccPvd());
		List<AccountProviderDetails> dbAccProvs = pspMgmtService.getAccountProviderDetails();
		List<AccountProviderDetails> updateProviderDetails = getUpdateList(reqAccProvs, dbAccProvs);
		pspMgmtService.operateAccountProviderDetails(updateProviderDetails, dbAccProvs);
		
		return PspClientTool.requestToString(prepareAckData(ServiceRequestName.RESP_LIST_ACC_PVD, hbr.getHead().getMsgId(), null));
	}


	public static List<AccountProviderDetails> prepareAccountProvider(List<AccPvd> accPvds){
		List<AccountProviderDetails> accList = new ArrayList<AccountProviderDetails>();
		for(AccPvd accProvider : accPvds) {
			AccountProviderDetails accountProviderDetails = new AccountProviderDetails();
			accountProviderDetails.setActive(accProvider.getActive().name());
			accountProviderDetails.setBankName(accProvider.getName());
			accountProviderDetails.setIin(accProvider.getIin().toString());
			accountProviderDetails.setLastModifedTs(DateUtil.getUpiDateStrFormat(accProvider.getLastModifedTs()));
			accountProviderDetails.setProds(accProvider.getProds());
			accountProviderDetails.setSpocEmail(accProvider.getSpocEmail());
			accountProviderDetails.setSpocName(accProvider.getName());
			accountProviderDetails.setSpocPhone(accProvider.getSpocPhone());
			accountProviderDetails.setUrl(accProvider.getUrl());
			accList.add(accountProviderDetails);
		}
		return accList;
	}
	
	private List<AccountProviderDetails> getUpdateList(List<AccountProviderDetails> reqAccProviders, List<AccountProviderDetails> dbAccProviders) {
		Map<String, AccountProviderDetails> map = new HashMap<>();
		for (AccountProviderDetails accountProviderDetails2 : reqAccProviders) {
			map.put(accountProviderDetails2.getIin(), accountProviderDetails2);
		}
		List<AccountProviderDetails> finalAccProviderDetails = new ArrayList<AccountProviderDetails>();
		Iterator<AccountProviderDetails> iterator = dbAccProviders.iterator();
		while(iterator.hasNext()){
			AccountProviderDetails details = iterator.next();
			AccountProviderDetails reqAccProv = map.get(details.getIin());
			if (reqAccProv != null) {
				boolean isModified = false;
				if (!details.getBankName().equals(reqAccProv.getBankName())) {
					details.setBankName(reqAccProv.getBankName());
					isModified = true;
				}
				if (!details.getProds().equals(reqAccProv.getProds())) {
					details.setProds(reqAccProv.getProds());
					isModified = true;
				}
				if (!details.getActive().equals(reqAccProv.getActive())) {
					details.setActive(reqAccProv.getActive());
					isModified = true;
				}
				if (!details.getSpocEmail().equals(reqAccProv.getSpocEmail())) {
					details.setSpocEmail(reqAccProv.getSpocEmail());
					isModified = true;
				}
				if (!details.getSpocPhone().equals(reqAccProv.getSpocPhone())) {
					details.setSpocPhone(reqAccProv.getSpocPhone());
					isModified = true;
				}
				if (!details.getSpocName().equals(reqAccProv.getSpocName())) {
					details.setSpocName(reqAccProv.getSpocName());
					isModified = true;
				}
				if (!details.getUrl().equals(reqAccProv.getUrl())) {
					details.setUrl(reqAccProv.getUrl());
					isModified = true;
				}
				if (isModified) {
					details.setLastModifedTs(new Date());
					finalAccProviderDetails.add(details);
				}
				map.remove(details.getIin());
				iterator.remove();
			}
		}
		//Adding unmatched data with request account provider
		finalAccProviderDetails.addAll(map.values());
		return finalAccProviderDetails;
	}
	
}