package psp.upi.process.factory.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.upi.system_1_2.PayTrans;
import org.upi.system_1_2.PayerType;
import org.upi.system_1_2.ReqBalEnq;
import org.upi.system_1_2.RespBalEnq;
import org.upi.system_1_2.RespBalEnq.Payer;
import org.upi.system_1_2.RespBalEnq.Payer.Bal;
import org.upi.system_1_2.RespBalEnq.Payer.Bal.Data;
import org.upi.system_1_2.RespType;

import psp.constants.CommonConstants;
import psp.constants.ServiceRequestName;
import psp.dbservice.mgmt.PspMgmtService;
import psp.upi.process.factory.UpiCoreHandler;
import psp.upi.process.util.TransactionThread;
import psp.util.DtoObjectUtil;
import psp.util.PspClientTool;
import psp.util.dto.HeadDto;
import psp.util.dto.TxnDto;
import psp.util.upi.client.UpiClientService;
import psp.util.upiclient.UpiDataPreparationUtility;

@Component("reqBalEnqHandler")
public class ReqBalEnqHandlerImpl extends UpiCoreHandler {

	@Autowired
	private PspMgmtService pspMgmtService;

	@Autowired
	private UpiClientService upiClientService;
	
	@Override
	public String handleProcess(String upiData) {
		ReqBalEnq response = PspClientTool.convertUpiRequest(upiData, ReqBalEnq.class);
		Date date = new Date();
		String messageId = DtoObjectUtil.constructMessageId();
		HeadDto head = DtoObjectUtil.constructHeadDto(messageId, date);
		
		PayTrans payTrans = response.getTxn();
		TxnDto txn = DtoObjectUtil.constructTxnDto(payTrans.getId(), payTrans.getNote(), null, payTrans.getRefId(), payTrans.getRefUrl(), date, ServiceRequestName.REQ_BAL_ENQ);
				
		RespType respType = new RespType();
		respType.setResult(CommonConstants.SUCCESS);
		respType.setReqMsgId(response.getHead().getMsgId());
		
		Payer payer = new Payer();
		PayerType payerType = response.getPayer();
		payer.setAddr(payerType.getAddr());
		payer.setName(payerType.getName());
		payer.setSeqNum(payerType.getSeqNum());
		payer.setType(payerType.getType().name());
		
		Bal bal = new Bal();
		Data data = new Data();
		//data.setValue(payerType.getCreds().getCred().get(0).getData().getValue());
		data.setValue("5000");
		bal.setData(data);
		payer.setBal(bal);
		
		RespBalEnq respBal = new RespBalEnq();
		respBal.setHead(UpiDataPreparationUtility.convertHeader(head));
		respBal.setTxn(UpiDataPreparationUtility.convertTxn(txn));
		respBal.setResp(respType);
		respBal.setPayer(payer);
		 
		TransactionThread transactionThread = new TransactionThread(respBal, ServiceRequestName.RESP_BAL_ENQ, upiClientService, null, null);
		new Thread(transactionThread).start();
		return PspClientTool.requestToString(prepareAckData(ServiceRequestName.REQ_BAL_ENQ, response.getHead().getMsgId(), null));
	}

}
