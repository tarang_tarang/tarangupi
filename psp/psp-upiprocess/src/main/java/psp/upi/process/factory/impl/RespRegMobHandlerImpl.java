/**
 * 
 */
package psp.upi.process.factory.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.upi.system_1_2.RespRegMob;

import psp.constants.ServiceRequestName;
import psp.constants.ServiceStatus;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.GeneratePinUpiRequest;
import psp.upi.process.factory.UpiCoreHandler;
import psp.util.PspClientTool;

/**
 * @author prasadj
 *
 */
@Component("respRegMobHandler")
public class RespRegMobHandlerImpl extends UpiCoreHandler {

	@Autowired
	private PspMgmtService pspMgmtService;
	
	public RespRegMobHandlerImpl(){
	}
	
	@Override
	public String handleProcess(String upiData) {
		RespRegMob response = PspClientTool.convertUpiRequest(upiData, RespRegMob.class);
		GeneratePinUpiRequest reqdetails = (GeneratePinUpiRequest) pspMgmtService.getUpiRequestByTxnId(response.getTxn().getId());
		if(null != reqdetails) {
			reqdetails.setStatus(ServiceStatus.RECEIVED.name());
			reqdetails.setResult(response.getResp().getResult());
			reqdetails.setErrMsg(response.getResp().getErrCode());
			reqdetails.setResponseTimeFromUpi(new Date());
			pspMgmtService.updateUpiRequest(reqdetails);
		}
		return PspClientTool.requestToString(prepareAckData(ServiceRequestName.RESP_REG_MOB, response.getHead().getMsgId(), null));
	}

}