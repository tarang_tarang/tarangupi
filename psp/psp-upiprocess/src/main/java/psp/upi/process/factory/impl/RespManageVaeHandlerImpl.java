/**
 * 
 */
package psp.upi.process.factory.impl;

import org.springframework.stereotype.Component;
import org.upi.system_1_2.RespManageVae;

import psp.constants.ServiceRequestName;
import psp.upi.process.factory.UpiCoreHandler;
import psp.util.PspClientTool;

/**
 * @author prasadj
 *
 */
@Component("respManageVaeHandler")
public class RespManageVaeHandlerImpl extends UpiCoreHandler {

	public RespManageVaeHandlerImpl(){
	}
	
	@Override
	public String handleProcess(String upiData) {
		RespManageVae response = PspClientTool.convertUpiRequest(upiData, RespManageVae.class);
		return PspClientTool.requestToString(prepareAckData(ServiceRequestName.RESP_MANAGE_VAE, response.getHead().getMsgId(), null));
	}

}