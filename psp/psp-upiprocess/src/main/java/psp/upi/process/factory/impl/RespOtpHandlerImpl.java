/**
 * 
 */
package psp.upi.process.factory.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.upi.system_1_2.RespOtp;

import psp.constants.ServiceRequestName;
import psp.constants.ServiceStatus;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.OtpUpiRequest;
import psp.upi.process.factory.UpiCoreHandler;
import psp.util.PspClientTool;

/**
 * @author prasadj
 *
 */
@Component("respOtpHandler")
public class RespOtpHandlerImpl extends UpiCoreHandler {

	@Autowired
	private PspMgmtService pspMgmtService;

	public RespOtpHandlerImpl(){
	}
	
	@Override
	public String handleProcess(String upiData) {
		RespOtp response = PspClientTool.convertUpiRequest(upiData, RespOtp.class);
		OtpUpiRequest otpRequestDetails = (OtpUpiRequest) pspMgmtService.getUpiRequestByTxnId(response.getTxn().getId());
		if(null != otpRequestDetails) {
			otpRequestDetails.setStatus(ServiceStatus.RECEIVED.name());
			otpRequestDetails.setResult(response.getResp().getResult());
			otpRequestDetails.setErrMsg(response.getResp().getErrCode());
			otpRequestDetails.setResponseTimeFromUpi(new Date());
			pspMgmtService.updateUpiRequest(otpRequestDetails);
		}
		return PspClientTool.requestToString(prepareAckData(ServiceRequestName.RESP_OTP, response.getHead().getMsgId(), null));
	}

}