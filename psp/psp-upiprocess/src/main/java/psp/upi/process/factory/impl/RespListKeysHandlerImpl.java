/**
 * 
 */
package psp.upi.process.factory.impl;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.upi.system_1_2.PayConstant;
import org.upi.system_1_2.RespListKeys;
import org.upi.system_1_2.RespListKeys.KeyList.Key;

import psp.constants.ServiceRequestName;
import psp.constants.ServiceStatus;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.GetTokenUpiRequest;
import psp.dbservice.model.KeyInfo;
import psp.dbservice.model.KeyInfoReq;
import psp.upi.process.factory.UpiCoreHandler;
import psp.util.PspClientTool;

/**
 * @author prasadj
 *
 */
@Component("respListKeysHandler")
public class RespListKeysHandlerImpl extends UpiCoreHandler {

	@Autowired
	private PspMgmtService pspMgmtService;

	public RespListKeysHandlerImpl(){
	}
	
	@Override
	public String handleProcess(String upiData) {
		RespListKeys response = PspClientTool.convertUpiRequest(upiData, RespListKeys.class);
		if (response.getTxn().getType() == PayConstant.LIST_KEYS) {
			KeyInfo keyInfo = new KeyInfo();
			keyInfo.setXmlPayload(upiData);
			pspMgmtService.operateKeyInfo(keyInfo);
		}
		else if (response.getTxn().getType() == PayConstant.GET_TOKEN) {
			GetTokenUpiRequest tokenUpiRequest = (GetTokenUpiRequest) pspMgmtService.getUpiRequestByTxnId(response.getTxn().getId());
			if (tokenUpiRequest != null) {
				tokenUpiRequest.setStatus(ServiceStatus.RECEIVED.name());
				tokenUpiRequest.setResult(response.getResp().getResult());
				tokenUpiRequest.setErrMsg(response.getResp().getErrCode());
				if (response.getKeyList().getKey() != null) {
					Set<KeyInfoReq> keyInfoReqs = new HashSet<>();
					for (Key key : response.getKeyList().getKey()) {
						KeyInfoReq keyInfoReq = new KeyInfoReq();
						keyInfoReq.setCode(key.getCode());
						keyInfoReq.setType(key.getType());
						keyInfoReq.setKi(key.getKi());
						keyInfoReq.setKeyValue(key.getKeyValue().toString());
						keyInfoReqs.add(keyInfoReq);
					}
					tokenUpiRequest.setKeyInfoReqs(keyInfoReqs);
				}
				pspMgmtService.updateUpiRequest(tokenUpiRequest);
			}
		}	
		return PspClientTool.requestToString(prepareAckData(ServiceRequestName.RESP_LIST_OF_KEYS, response.getHead().getMsgId(), null));
	}

}