/**
 * 
 */
package psp.upi.process.util;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.upi.system_1_2.HeadType;
import org.upi.system_1_2.ReqAuthDetails;
import org.upi.system_1_2.ReqBalEnq;
import org.upi.system_1_2.ReqListAccount;
import org.upi.system_1_2.ReqOtp;
import org.upi.system_1_2.ReqPay;
import org.upi.system_1_2.ReqRegMob;
import org.upi.system_1_2.ReqTxnConfirmation;
import org.upi.system_1_2.RespBalEnq;
import org.upi.system_1_2.RespHbt;
import org.upi.system_1_2.RespListAccPvd;
import org.upi.system_1_2.RespListAccount;
import org.upi.system_1_2.RespListKeys;
import org.upi.system_1_2.RespListPsp;
import org.upi.system_1_2.RespListVae;
import org.upi.system_1_2.RespManageVae;
import org.upi.system_1_2.RespOtp;
import org.upi.system_1_2.RespPay;
import org.upi.system_1_2.RespPendingMsg;
import org.upi.system_1_2.RespRegMob;
import org.upi.system_1_2.RespSetCre;
import org.upi.system_1_2.RespTxnConfirmation;
import org.upi.system_1_2.RespValAdd;

import psp.constants.ServiceRequestName;
import psp.dbservice.model.NotificationDetails;
import psp.dbservice.model.UpiMessagePacket;
import psp.util.PspClientTool;

/**
 * @author prasadj
 *
 */
public class UpiProcessUtility {

	/**
	 * coreProcessBeanMap holds the request/service name as key and alias name as value
	 */
	private static final Map<String, String> upiHandlerBeanMap = new HashMap<String, String>();
	
	private static final  Map<String, Class<?>> upiRequestClassMap= new HashMap<>();
	
	static {
		upiHandlerBeanMap.put(ServiceRequestName.RESP_PAY, "RespPayHandler");
		upiHandlerBeanMap.put(ServiceRequestName.REQ_AUTH_DETAIL, "ReqAuthDetailsHandler");
		upiHandlerBeanMap.put(ServiceRequestName.REQ_TXN_CONF, "ReqTxnConfirmationHandler");
		upiHandlerBeanMap.put(ServiceRequestName.RESP_HBT, "RespHbtHandler");
		upiHandlerBeanMap.put(ServiceRequestName.RESP_LIST_ACC_PVD, "RespListAccPvdHandler");
		upiHandlerBeanMap.put(ServiceRequestName.RESP_BAL_ENQ, "RespBalEnqHandler");
		upiHandlerBeanMap.put(ServiceRequestName.RESP_CHK_TXN, "RespChkTxnHandler");
		upiHandlerBeanMap.put(ServiceRequestName.RESP_LIST_PSP, "RespListPspHandler");
		upiHandlerBeanMap.put(ServiceRequestName.RESP_LIST_ACCOUNT, "RespListAccountHandler");
		upiHandlerBeanMap.put(ServiceRequestName.RESP_LIST_OF_KEYS, "RespListKeysHandler");
		upiHandlerBeanMap.put(ServiceRequestName.RESP_LIST_VAE, "RespListVaeHandler");
		upiHandlerBeanMap.put(ServiceRequestName.RESP_MANAGE_VAE, "RespManageVaeHandler");
		upiHandlerBeanMap.put(ServiceRequestName.RESP_OTP, "RespOtpHandler");
		upiHandlerBeanMap.put(ServiceRequestName.RESP_PENDING_MSG, "RespPendingMsgHandler");
		upiHandlerBeanMap.put(ServiceRequestName.RESP_VAL_ADD, "RespValAddHandler");
		upiHandlerBeanMap.put(ServiceRequestName.RESP_SET_CRE, "RespSetCreHandler");
		upiHandlerBeanMap.put(ServiceRequestName.RESP_REG_MOB, "RespRegMobHandler");
		upiHandlerBeanMap.put(ServiceRequestName.REQ_OTP, "ReqOtpHandler");
		upiHandlerBeanMap.put(ServiceRequestName.REQ_PAY, "ReqPayHandler");
		upiHandlerBeanMap.put(ServiceRequestName.REQ_LIST_ACC, "ReqListAccountHandler");
		upiHandlerBeanMap.put(ServiceRequestName.REQ_MOB_REG, "ReqRegMobHandler");
		upiHandlerBeanMap.put(ServiceRequestName.REQ_BAL_ENQ, "ReqBalEnqHandler");
		
		upiRequestClassMap.put(ServiceRequestName.RESP_PAY, RespPay.class);
		upiRequestClassMap.put(ServiceRequestName.REQ_AUTH_DETAIL, ReqAuthDetails.class);
		upiRequestClassMap.put(ServiceRequestName.REQ_TXN_CONF, ReqTxnConfirmation.class);
		upiRequestClassMap.put(ServiceRequestName.RESP_HBT, RespHbt.class);
		upiRequestClassMap.put(ServiceRequestName.RESP_LIST_ACC_PVD, RespListAccPvd.class);
		upiRequestClassMap.put(ServiceRequestName.RESP_BAL_ENQ, RespBalEnq.class);
		upiRequestClassMap.put(ServiceRequestName.RESP_CHK_TXN, RespTxnConfirmation.class);
		upiRequestClassMap.put(ServiceRequestName.RESP_LIST_PSP, RespListPsp.class);
		upiRequestClassMap.put(ServiceRequestName.RESP_LIST_ACCOUNT, RespListAccount.class);
		upiRequestClassMap.put(ServiceRequestName.RESP_LIST_OF_KEYS, RespListKeys.class);
		upiRequestClassMap.put(ServiceRequestName.RESP_LIST_VAE, RespListVae.class);
		upiRequestClassMap.put(ServiceRequestName.RESP_MANAGE_VAE, RespManageVae.class);
		upiRequestClassMap.put(ServiceRequestName.RESP_OTP, RespOtp.class);
		upiRequestClassMap.put(ServiceRequestName.RESP_PENDING_MSG, RespPendingMsg.class);
		upiRequestClassMap.put(ServiceRequestName.RESP_VAL_ADD, RespValAdd.class);
		upiRequestClassMap.put(ServiceRequestName.RESP_SET_CRE, RespSetCre.class);
		upiRequestClassMap.put(ServiceRequestName.RESP_REG_MOB, RespRegMob.class);
		upiRequestClassMap.put(ServiceRequestName.REQ_OTP, ReqOtp.class);
		upiRequestClassMap.put(ServiceRequestName.REQ_PAY, ReqPay.class);
		upiRequestClassMap.put(ServiceRequestName.REQ_LIST_ACC, ReqListAccount.class);
		upiRequestClassMap.put(ServiceRequestName.REQ_MOB_REG, ReqRegMob.class);
		upiRequestClassMap.put(ServiceRequestName.REQ_BAL_ENQ, ReqBalEnq.class);
	}
	
	private UpiProcessUtility() {
	}
	
	public static String getProcessBeanName(String serviceName){
		return upiHandlerBeanMap.get(serviceName);
	}
	
	public static String getMsgId(String data, String serviceName){
		Class<?> upiRequestClass = upiRequestClassMap.get(serviceName);
		HeadType headType = null;
		try {
			Object obj = PspClientTool.convertUpiRequest(data, Class.forName(upiRequestClass.getName()));
			Field field = obj.getClass().getDeclaredField("head");
			Class<?> targetType = field.getType();
			headType = (HeadType)targetType.newInstance();
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
		return headType.getMsgId();
	}
	
	public static UpiMessagePacket prepareUpiMessagePacket(String messagePacket, String txnId) {
		UpiMessagePacket upiMessagePacket = new UpiMessagePacket();
		upiMessagePacket.setMessagePacket(messagePacket);
		upiMessagePacket.setTxnId(txnId);
		return upiMessagePacket;
	}
	public static String constructNotificationId(){
		return String.valueOf(System.currentTimeMillis());
	}
	
	public static NotificationDetails prepareNotificationDetails(String notificationId, String notificationMessage, String registrationId, String username, String notificationType) {
		NotificationDetails notificationDetails = new NotificationDetails();
		notificationDetails.setGeneratedTime(new Date());
		notificationDetails.setNotificationId(notificationId);
		notificationDetails.setNotificationMessage(notificationMessage);
		notificationDetails.setRegistrationId(registrationId);
		notificationDetails.setStatus("");
		notificationDetails.setUserName(username);
		notificationDetails.setNotificationtType(notificationType);
		return notificationDetails;
	}
	
	
}