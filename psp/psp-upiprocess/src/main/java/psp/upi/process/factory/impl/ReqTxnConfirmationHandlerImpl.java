/**
 * 
 */
package psp.upi.process.factory.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.upi.system_1_2.PayTrans;
import org.upi.system_1_2.Ref;
import org.upi.system_1_2.ReqTxnConfirmation;
import org.upi.system_1_2.RespTxnConfirmation;
import org.upi.system_1_2.RespType;

import psp.constants.CommonConstants;
import psp.constants.ServiceRequestName;
import psp.constants.TxnStatus;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.AccountDetails;
import psp.dbservice.model.NotificationDetails;
import psp.dbservice.model.TransactionDetails;
import psp.dbservice.model.UserDetails;
import psp.upi.process.factory.UpiCoreHandler;
import psp.upi.process.util.TransactionThread;
import psp.upi.process.util.UpiProcessUtility;
import psp.util.DtoObjectUtil;
import psp.util.PspClientTool;
import psp.util.dto.HeadDto;
import psp.util.dto.TxnDto;
import psp.util.gcm.GcmNotification;
import psp.util.upi.client.UpiClientService;
import psp.util.upiclient.UpiDataPreparationUtility;

/**
 * @author prasadj
 *
 */
@Component("reqTxnConfirmationHandler")
public class ReqTxnConfirmationHandlerImpl extends UpiCoreHandler {

	@Autowired
	private UpiClientService upiClientService;

	@Autowired
	private PspMgmtService pspMgmtService;
	
	@Autowired
	private GcmNotification gcmNotification;
	
	public ReqTxnConfirmationHandlerImpl(){
	}
	
	@Override
	public String handleProcess(String upiData) {
		ReqTxnConfirmation reqTxnConfirmation = PspClientTool.convertUpiRequest(upiData, ReqTxnConfirmation.class);
		pspMgmtService.saveTransactionDetails(prepareTransactionDetails(reqTxnConfirmation));
		
		NotificationDetails notificationDetails = prapareNotificationDetails(reqTxnConfirmation);
		
		if(notificationDetails != null){
			pspMgmtService.saveNotificationDetails(notificationDetails);
		}
		
		TransactionThread transactionThread = new TransactionThread(prepareRespTxnConfirmation(reqTxnConfirmation), ServiceRequestName.RESP_TXN_CONF, upiClientService, gcmNotification, notificationDetails);
		new Thread(transactionThread).start();
		
		return PspClientTool.requestToString(prepareAckData(ServiceRequestName.REQ_TXN_CONF, reqTxnConfirmation.getHead().getMsgId(), null)); 
	}

	private RespTxnConfirmation prepareRespTxnConfirmation(ReqTxnConfirmation reqTxnConfirmation) {
		Date date = new Date();
		HeadDto head = DtoObjectUtil.constructHeadDto(DtoObjectUtil.constructMessageId(), date);
		
		PayTrans payTrans = reqTxnConfirmation.getTxn();
		TxnDto txn = DtoObjectUtil.constructTxnDto(payTrans.getOrgTxnId(), payTrans.getNote(), payTrans.getOrgTxnId(), payTrans.getRefId(), payTrans.getRefUrl(), date, payTrans.getType().name());
		
		RespType respType = new RespType();
		respType.setReqMsgId(DtoObjectUtil.constructMessageId());
		respType.setResult(reqTxnConfirmation.getTxnConfirmation().getOrgStatus());
		
		RespTxnConfirmation respTxnConfirmation = new RespTxnConfirmation();
		respTxnConfirmation.setHead(UpiDataPreparationUtility.convertHeader(head));
		respTxnConfirmation.setTxn(UpiDataPreparationUtility.convertTxn(txn));
		respTxnConfirmation.setResp(respType);
		return respTxnConfirmation;
	}
	
	private TransactionDetails prepareTransactionDetails(ReqTxnConfirmation reqTxnConfirmation) {
		TransactionDetails txnDetails = new TransactionDetails();		
		txnDetails.setToAddr(reqTxnConfirmation.getTxnConfirmation().getRef().get(0).getAddr());
		txnDetails.setStatus(TxnStatus.COMPLETED.name());//TODO need to confirm
		txnDetails.setTxnType(reqTxnConfirmation.getTxn().getType().name());
		txnDetails.setUpiErrorCode(reqTxnConfirmation.getTxnConfirmation().getOrgErrCode());
		txnDetails.setUpiStatus(reqTxnConfirmation.getTxnConfirmation().getOrgStatus());
		txnDetails.setInitiator(false);
		txnDetails.setTxnInitiationTime(new Date());//TODO need to confirm
		txnDetails.setTxnConfirmationTime(new Date());
		txnDetails.setTxnId(reqTxnConfirmation.getTxn().getOrgTxnId());
		txnDetails.setTxnMessage(reqTxnConfirmation.getTxn().getNote());
		return txnDetails;
	}
	
	private NotificationDetails prapareNotificationDetails(ReqTxnConfirmation reqTxnConfirmation){
		List<Ref> refs = reqTxnConfirmation.getTxnConfirmation().getRef();
		AccountDetails accountDetails = pspMgmtService.getAccountDetailsByVirtualAddr(refs.get(0).getAddr());
		NotificationDetails notificationDetails = null;
		if (accountDetails != null) {
			UserDetails userDetails = pspMgmtService.getUserDetails(accountDetails.getUserDetails().getId());
			String notificationType = reqTxnConfirmation.getTxn().getType().name();
			notificationDetails = new NotificationDetails();
			notificationDetails.setGeneratedTime(new Date());
			notificationDetails.setNotificationId(UpiProcessUtility.constructNotificationId());
			String message = ( reqTxnConfirmation.getTxnConfirmation().getOrgStatus().equals("SUCCESS") ) ? CommonConstants.PAYMENT_SUCCESS_MSG: CommonConstants.PAYMENT_FAILED_MSG; 
			notificationDetails.setNotificationMessage(message);
			notificationDetails.setRegistrationId(userDetails.getRnsMpaId());
			notificationDetails.setStatus("");
			notificationDetails.setUserName(userDetails.getUserName());
			notificationDetails.setNotificationtType(notificationType);
		}
		return notificationDetails;
	}
	
}