/**
 * 
 */
package psp.upi.process.factory.impl;

import org.springframework.stereotype.Component;
import org.upi.system_1_2.RespPendingMsg;

import psp.constants.ServiceRequestName;
import psp.upi.process.factory.UpiCoreHandler;
import psp.util.PspClientTool;

/**
 * @author prasadj
 *
 */
@Component("respPendingMsgHandler")
public class RespPendingMsgHandlerImpl extends UpiCoreHandler {

	@Override
	public String handleProcess(String upiData) {
		RespPendingMsg response = PspClientTool.convertUpiRequest(upiData, RespPendingMsg.class);
		return PspClientTool.requestToString(prepareAckData(ServiceRequestName.RESP_PENDING_MSG, response.getHead().getMsgId(), null));
	}

}