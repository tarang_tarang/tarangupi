package psp.upi.process;


/**
 * @author manasp
 *
 */
public interface TestRequestProcessor {

	Object requestByServiceName(String serviceName);
	
}