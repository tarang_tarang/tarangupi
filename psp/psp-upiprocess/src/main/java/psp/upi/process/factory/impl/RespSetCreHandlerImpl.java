/**
 * 
 */
package psp.upi.process.factory.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.upi.system_1_2.RespSetCre;

import psp.constants.ServiceRequestName;
import psp.constants.ServiceStatus;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.SetCredentialUpiRequest;
import psp.upi.process.factory.UpiCoreHandler;
import psp.util.PspClientTool;

/**
 * @author prasadj
 *
 */
@Component("respSetCreHandler")
public class RespSetCreHandlerImpl extends UpiCoreHandler {

	@Autowired
	private PspMgmtService pspMgmtService;
	
	public RespSetCreHandlerImpl(){
	}
	
	@Override
	public String handleProcess(String upiData) {
		RespSetCre response = PspClientTool.convertUpiRequest(upiData, RespSetCre.class);
		SetCredentialUpiRequest req = (SetCredentialUpiRequest) pspMgmtService.getUpiRequestByTxnId(response.getTxn().getId());
		if(null != req) {
			req.setStatus(ServiceStatus.RECEIVED.name());
			req.setResult(response.getResp().getResult());
			req.setErrMsg(response.getResp().getErrCode());
			req.setResponseTimeFromUpi(new Date());
			pspMgmtService.updateUpiRequest(req);
		}
		return PspClientTool.requestToString(prepareAckData(ServiceRequestName.RESP_SET_CRE, response.getHead().getMsgId(), null));
	}

}