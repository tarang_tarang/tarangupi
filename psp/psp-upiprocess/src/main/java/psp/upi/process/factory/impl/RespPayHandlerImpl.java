/**
 * 
 */
package psp.upi.process.factory.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.upi.system_1_2.PayConstant;
import org.upi.system_1_2.PayTrans;
import org.upi.system_1_2.RespPay;
import org.upi.system_1_2.ResultType;

import psp.constants.CommonConstants;
import psp.constants.ServiceRequestName;
import psp.constants.TxnStatus;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.AccountDetails;
import psp.dbservice.model.NotificationDetails;
import psp.dbservice.model.TransactionDetails;
import psp.dbservice.model.UserDetails;
import psp.upi.process.factory.UpiCoreHandler;
import psp.upi.process.util.UpiProcessUtility;
import psp.util.PspClientTool;
import psp.util.gcm.GcmNotification;

import com.google.android.gcm.server.Result;
/**
 * @author prasadj
 *
 */
@Component("respPayHandler")
public class RespPayHandlerImpl extends UpiCoreHandler {

	@Autowired
	private PspMgmtService pspMgmtService;
	
	@Autowired
	private GcmNotification gcmNotification;
	
	public RespPayHandlerImpl(){
	}

	@Override
	public String handleProcess(String upiData) {
		RespPay rpr = PspClientTool.convertUpiRequest(upiData, RespPay.class);
		//TODO need to add type
		PayTrans payTrans = rpr.getTxn(); 
		TransactionDetails txnDetails = pspMgmtService.getTransactionDetailsByTxnId(payTrans.getId(), payTrans.getType().name());
		txnDetails.setUpiErrorCode(rpr.getResp().getErrCode());
		txnDetails.setUpiStatus(rpr.getResp().getResult().name());
		txnDetails.setTxnConfirmationTime(new Date());
		txnDetails.setStatus(TxnStatus.COMPLETED.name());
		pspMgmtService.updateTransactionDetails(txnDetails);
		
		String notificationId = UpiProcessUtility.constructNotificationId();
		AccountDetails accountDetails = pspMgmtService.getAccountDetailsByVirtualAddr(txnDetails.getFromAddr());
		UserDetails userDetails = pspMgmtService.getUserDetails(accountDetails.getUserDetails().getId());
		String message = ( rpr.getResp().getResult() == ResultType.SUCCESS ) ? CommonConstants.PAYMENT_SUCCESS_MSG: CommonConstants.PAYMENT_FAILED_MSG;
		String notificationType = PayConstant.COLLECT.name();
		NotificationDetails notificationDetails = UpiProcessUtility.prepareNotificationDetails(notificationId, message, userDetails.getRnsMpaId(), userDetails.getUserName(), notificationType);
		pspMgmtService.saveNotificationDetails(notificationDetails);
		
		Result result = gcmNotification.send(notificationId, notificationType, message, userDetails.getRnsMpaId());
		if (result.getMessageId() == null) {
			throw new RuntimeException(""); 
		} 
		return PspClientTool.requestToString(prepareAckData(ServiceRequestName.RESP_PAY, rpr.getHead().getMsgId(), null)); 
	}
	
}