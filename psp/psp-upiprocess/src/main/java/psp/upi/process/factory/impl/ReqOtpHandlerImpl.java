/**
 * 
 */
package psp.upi.process.factory.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.upi.system_1_2.PayTrans;
import org.upi.system_1_2.ReqOtp;
import org.upi.system_1_2.RespOtp;
import org.upi.system_1_2.RespType;

import psp.constants.ServiceRequestName;
import psp.dbservice.mgmt.PspMgmtService;
import psp.upi.process.factory.UpiCoreHandler;
import psp.upi.process.util.TransactionThread;
import psp.util.DtoObjectUtil;
import psp.util.PspClientTool;
import psp.util.dto.HeadDto;
import psp.util.dto.TxnDto;
import psp.util.upi.client.UpiClientService;
import psp.util.upiclient.UpiDataPreparationUtility;

/**
 * @author prasadj
 *
 */
@Component("ReqOtpHandler")
public class ReqOtpHandlerImpl extends UpiCoreHandler {

	@Autowired
	private PspMgmtService pspMgmtService;

	@Autowired
	private UpiClientService upiClientService;
	
	public ReqOtpHandlerImpl(){
	}
	
	@Override
	public String handleProcess(String upiData) {
		ReqOtp response = PspClientTool.convertUpiRequest(upiData, ReqOtp.class);
		
		Date date = new Date();
		String messageId = DtoObjectUtil.constructMessageId();
		HeadDto head = DtoObjectUtil.constructHeadDto(messageId, date);
		
		PayTrans payTrans = response.getTxn();
		TxnDto txn = DtoObjectUtil.constructTxnDto(payTrans.getId(), payTrans.getNote(), null, payTrans.getRefId(), payTrans.getRefUrl(), date, ServiceRequestName.REQ_OTP);
				
		RespType respType = new RespType();
		respType.setResult("SUCCESS");
		respType.setReqMsgId(response.getHead().getMsgId());
		
		RespOtp respOtp = new RespOtp();
		respOtp.setHead(UpiDataPreparationUtility.convertHeader(head));
		respOtp.setTxn(UpiDataPreparationUtility.convertTxn(txn));
		respOtp.setResp(respType);
		
		TransactionThread transactionThread = new TransactionThread(respOtp, ServiceRequestName.RESP_OTP, upiClientService, null, null);
		new Thread(transactionThread).start();
		return PspClientTool.requestToString(prepareAckData(ServiceRequestName.REQ_OTP, response.getHead().getMsgId(), null));
	}

}