/**
 * 
 */
package psp.upi.process.factory.impl;

import org.springframework.stereotype.Component;
import org.upi.system_1_2.RespListVae;

import psp.constants.ServiceRequestName;
import psp.upi.process.factory.UpiCoreHandler;
import psp.util.PspClientTool;

/**
 * @author prasadj
 *
 */
@Component("respListVaeHandler")
public class RespListVaeHandlerImpl extends UpiCoreHandler {

	public RespListVaeHandlerImpl(){
	}
	
	@Override
	public String handleProcess(String upiData) {
		RespListVae response = PspClientTool.convertUpiRequest(upiData, RespListVae.class);
		return PspClientTool.requestToString(prepareAckData(ServiceRequestName.RESP_LIST_VAE, response.getHead().getMsgId(), null));
	}

}