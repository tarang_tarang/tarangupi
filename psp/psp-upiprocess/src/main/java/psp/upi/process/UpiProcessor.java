/**
 * 
 */
package psp.upi.process;

/**
 * @author prasadj
 *
 */
public interface UpiProcessor {

	String doProcess(String serviceName, String data);
	
}