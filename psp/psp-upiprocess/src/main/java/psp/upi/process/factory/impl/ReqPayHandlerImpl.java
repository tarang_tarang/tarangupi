/**
 * 
 */
package psp.upi.process.factory.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.upi.system_1_2.PayConstant;
import org.upi.system_1_2.PayTrans;
import org.upi.system_1_2.PayeeType;
import org.upi.system_1_2.PayeesType;
import org.upi.system_1_2.PayerType;
import org.upi.system_1_2.Ref;
import org.upi.system_1_2.RefType;
import org.upi.system_1_2.ReqPay;
import org.upi.system_1_2.RespPay;
import org.upi.system_1_2.RespPay.Resp;
import org.upi.system_1_2.ResultType;

import psp.constants.ServiceRequestName;
import psp.dbservice.mgmt.PspMgmtService;
import psp.upi.process.factory.UpiCoreHandler;
import psp.upi.process.util.TransactionThread;
import psp.util.DtoObjectUtil;
import psp.util.PspClientTool;
import psp.util.dto.HeadDto;
import psp.util.dto.TxnDto;
import psp.util.upi.client.UpiClientService;
import psp.util.upiclient.UpiDataPreparationUtility;
/**
 * @author prasadj
 *
 */
@Component("reqPayHandler")
public class ReqPayHandlerImpl extends UpiCoreHandler {

	@Autowired
	private PspMgmtService pspMgmtService;
	
	@Autowired
	private UpiClientService upiClientService;
	
	public ReqPayHandlerImpl(){
	}

	@Override
	public String handleProcess( String upiData ) {
		ReqPay rpr = PspClientTool.convertUpiRequest(upiData, ReqPay.class);
		PayTrans payTrans = rpr.getTxn();
		if (payTrans.getType() == PayConstant.DEBIT) {
			peparePayResponse( rpr );
		}
		else if (payTrans.getType() == PayConstant.CREDIT) {
			peparePayResponse( rpr );
		}
		return PspClientTool.requestToString(prepareAckData(ServiceRequestName.REQ_PAY, rpr.getHead().getMsgId(), null)); 
	}
	
	private void peparePayResponse( ReqPay reqPay ){
		Date date = new Date();
		String messageId = DtoObjectUtil.constructMessageId();
		HeadDto head = DtoObjectUtil.constructHeadDto(messageId, date);
		PayTrans payTrans = reqPay.getTxn();
		TxnDto txn = DtoObjectUtil.constructTxnDto(payTrans.getId(), payTrans.getNote(), null, payTrans.getRefId(), payTrans.getRefUrl(), date, payTrans.getType().name());

		Resp resp = new Resp();
		resp.setReqMsgId(reqPay.getHead().getMsgId());
		resp.setResult(ResultType.SUCCESS);
		
		List<Ref> refs = resp.getRef();
		refs.add(preparePayerRefData(reqPay.getPayer()));
		refs.add(preparePayeeRefData(reqPay.getPayees()));
		
		RespPay respPay = new RespPay();
		respPay.setHead(UpiDataPreparationUtility.convertHeader(head));
		respPay.setTxn(UpiDataPreparationUtility.convertTxn(txn));
		respPay.setResp(resp);
		
		TransactionThread transactionThread = new TransactionThread(respPay, ServiceRequestName.RESP_PAY, upiClientService, null, null);
		new Thread(transactionThread).start();
	}
	
	private Ref preparePayeeRefData(PayeesType payeesType){
		PayeeType payeeType = payeesType.getPayee().get(0);
		Ref ref = new Ref();
		ref.setAddr(payeeType.getAddr());
		ref.setType(RefType.PAYEE);
		ref.setRespCode("00");
		ref.setSeqNum(payeeType.getSeqNum());
		ref.setSettAmount(payeeType.getAmount().getValue());
		ref.setSettCurrency(payeeType.getAmount().getCurr());
		ref.setRegName(payeeType.getName());
		ref.setApprovalNum("9399389827");//TODO Approval reference Number. need to check
		return ref;
		
	}
	
	private Ref preparePayerRefData(PayerType payerType){
		Ref ref = new Ref();
		ref.setAddr(payerType.getAddr());
		ref.setType(RefType.PAYER);
		ref.setRespCode("00");
		ref.setSeqNum(payerType.getSeqNum());
		ref.setSettAmount(payerType.getAmount().getValue());
		ref.setSettCurrency(payerType.getAmount().getCurr());
		ref.setRegName(payerType.getName());
		ref.setApprovalNum("9399389827");//TODO Approval reference Number. need to check
		return ref;
		
	}
}