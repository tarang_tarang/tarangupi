/**
 * 
 */
package psp.upi.process.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.upi.system_1_2.Ack;

import psp.constants.StatusCode;
import psp.upi.process.UpiProcessor;
import psp.upi.process.factory.UpiCoreHandler;
import psp.upi.process.factory.UpiHandlerFactory;
import psp.upi.process.util.UpiProcessUtility;
import psp.util.DtoObjectUtil;
import psp.util.PspClientTool;
import psp.util.SignatureGenUtil;

/**
 * @author prasadj
 *
 */
@Component("upiProcessor")
public class UpiProcessorImpl implements UpiProcessor {

	@Autowired
	private UpiHandlerFactory upiHandlerFactory;

	public UpiProcessorImpl(){
	}
	
	@Override
	public String doProcess(String serviceName, String data) {
		String beanName = UpiProcessUtility.getProcessBeanName(serviceName);
		UpiCoreHandler process = upiHandlerFactory.getHandler(beanName);
		boolean isSignatureValid = false;
		List<StatusCode> scList = new ArrayList<>();
		try {
			isSignatureValid = SignatureGenUtil.isXmlDigitalSignatureValid(data);
			if (!isSignatureValid) {//validate if signature is valid or not
				scList.add(StatusCode.SIGNATURE_ERROR);
				Ack ack = process.prepareAckData(serviceName, UpiProcessUtility.getMsgId(data, serviceName), scList);
				return  PspClientTool.requestToString(ack);
			}
		} catch (Exception e) {
			scList.add(StatusCode.INTERNAL_SERVER_ERROR_MSG);
			Ack ack = process.prepareAckData(serviceName, DtoObjectUtil.constructMessageId(), scList);
			return  PspClientTool.requestToString(ack);
		}
		
		return process.handleProcess(data);
	}

}