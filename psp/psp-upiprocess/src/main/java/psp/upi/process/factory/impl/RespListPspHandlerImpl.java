/**
 * 
 */
package psp.upi.process.factory.impl;

import org.springframework.stereotype.Component;
import org.upi.system_1_2.RespListPsp;

import psp.constants.ServiceRequestName;
import psp.upi.process.factory.UpiCoreHandler;
import psp.util.PspClientTool;

/**
 * @author prasadj
 *
 */
@Component("respListPspHandler")
public class RespListPspHandlerImpl extends UpiCoreHandler {

	public RespListPspHandlerImpl(){
	}
	
	@Override
	public String handleProcess(String upiData) {
		RespListPsp psp = PspClientTool.convertUpiRequest(upiData, RespListPsp.class);
		return PspClientTool.requestToString(prepareAckData(ServiceRequestName.RESP_LIST_PSP, psp.getHead().getMsgId(), null));
	}

}