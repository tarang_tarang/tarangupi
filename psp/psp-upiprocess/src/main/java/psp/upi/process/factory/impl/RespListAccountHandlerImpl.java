/**
 * 
 */
package psp.upi.process.factory.impl;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.upi.system_1_2.RespListAccount;
import org.upi.system_1_2.RespListAccount.AccountList.Account;

import psp.constants.ServiceRequestName;
import psp.constants.ServiceStatus;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.GetAccountsUpiRequest;
import psp.dbservice.model.ReqAccountReference;
import psp.upi.process.factory.UpiCoreHandler;
import psp.util.PspClientTool;

/**
 * @author prasadj
 *
 */
@Component("respListAccountHandler")
public class RespListAccountHandlerImpl extends UpiCoreHandler {

	@Autowired
	private PspMgmtService pspMgmtService;
	
	public RespListAccountHandlerImpl(){
	}
	
	@Override
	public String handleProcess(String upiData) {
		RespListAccount respListAccount = PspClientTool.convertUpiRequest(upiData, RespListAccount.class);		
		GetAccountsUpiRequest requestDetails = (GetAccountsUpiRequest) pspMgmtService.
				getUpiRequestByTxnId(respListAccount.getTxn().getId());
		if(null != requestDetails) {
			requestDetails.setStatus(ServiceStatus.RECEIVED.name());
			requestDetails.setResult(respListAccount.getResp().getResult());
			requestDetails.setErrMsg(respListAccount.getResp().getErrCode());
			requestDetails.setResponseTimeFromUpi(new Date());
			if (respListAccount.getAccountList() != null) {
				Set<ReqAccountReference> accounts = new HashSet<ReqAccountReference>();
				Iterator<Account> iter = respListAccount.getAccountList().getAccount().iterator();
				while(iter.hasNext()) {
					Account acc = iter.next();
					ReqAccountReference ref = new ReqAccountReference();
					ref.setAeba(acc.getAeba().name());
					ref.setIfsc(acc.getIfsc());
					ref.setMaskedNumber(acc.getMaskedAccnumber());
					ref.setMmid(acc.getMmid());
					ref.setName(acc.getName());
					ref.setReferenceNumber(acc.getAccRefNumber());
					ref.setCredentailType(acc.getCredsAllowed().get(0).getType().name());
					ref.setCredentailSubType(acc.getCredsAllowed().get(0).getSubType().name());
					ref.setCredentailDataType(acc.getCredsAllowed().get(0).getDType().name());
					ref.setCredentailDataLength((acc.getCredsAllowed().get(0).getDLength()));
					accounts.add(ref);
				}
				requestDetails.setAccounts(accounts);
			}
			pspMgmtService.updateUpiRequest(requestDetails);
		}
		return PspClientTool.requestToString(prepareAckData(ServiceRequestName.LIST_ACCOUNT, respListAccount.getHead().getMsgId(), null));
	}

}