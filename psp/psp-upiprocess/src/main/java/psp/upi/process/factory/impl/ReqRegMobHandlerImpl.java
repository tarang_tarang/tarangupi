package psp.upi.process.factory.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.upi.system_1_2.PayTrans;
import org.upi.system_1_2.ReqRegMob;
import org.upi.system_1_2.RespRegMob;
import org.upi.system_1_2.RespType;

import psp.constants.CommonConstants;
import psp.constants.ServiceRequestName;
import psp.dbservice.mgmt.PspMgmtService;
import psp.upi.process.factory.UpiCoreHandler;
import psp.upi.process.util.TransactionThread;
import psp.util.DtoObjectUtil;
import psp.util.PspClientTool;
import psp.util.dto.HeadDto;
import psp.util.dto.TxnDto;
import psp.util.upi.client.UpiClientService;
import psp.util.upiclient.UpiDataPreparationUtility;

@Component("reqRegMobHandler")
public class ReqRegMobHandlerImpl extends UpiCoreHandler {

	@Autowired
	private PspMgmtService pspMgmtService;

	@Autowired
	private UpiClientService upiClientService;
	
	@Override
	public String handleProcess(String upiData) {
		ReqRegMob response = PspClientTool.convertUpiRequest(upiData, ReqRegMob.class);
		Date date = new Date();
		String messageId = DtoObjectUtil.constructMessageId();
		HeadDto head = DtoObjectUtil.constructHeadDto(messageId, date);
		
		PayTrans payTrans = response.getTxn();
		TxnDto txn = DtoObjectUtil.constructTxnDto(payTrans.getId(), payTrans.getNote(), null, payTrans.getRefId(), payTrans.getRefUrl(), date, ServiceRequestName.REQ_MOB_REG);
				
		RespType respType = new RespType();
		respType.setResult(CommonConstants.SUCCESS);
		respType.setReqMsgId(response.getHead().getMsgId());
		
		RespRegMob respRegMob = new RespRegMob();
		respRegMob.setHead(UpiDataPreparationUtility.convertHeader(head));
		respRegMob.setTxn(UpiDataPreparationUtility.convertTxn(txn));
		respRegMob.setResp(respType);
		
		TransactionThread transactionThread = new TransactionThread(respRegMob, ServiceRequestName.RESP_REG_MOB, upiClientService, null, null);
		new Thread(transactionThread).start();
		return PspClientTool.requestToString(prepareAckData(ServiceRequestName.REQ_MOB_REG, response.getHead().getMsgId(), null));
	}

}
