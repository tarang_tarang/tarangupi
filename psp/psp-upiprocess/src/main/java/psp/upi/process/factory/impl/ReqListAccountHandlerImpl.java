/**
 * 
 */
package psp.upi.process.factory.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.upi.system_1_2.AebaType;
import org.upi.system_1_2.CredDataTypeConstant;
import org.upi.system_1_2.CredSubType;
import org.upi.system_1_2.CredType;
import org.upi.system_1_2.CredsAllowedType;
import org.upi.system_1_2.ListedAccountType;
import org.upi.system_1_2.PayTrans;
import org.upi.system_1_2.ReqListAccount;
import org.upi.system_1_2.RespListAccount;
import org.upi.system_1_2.RespListAccount.AccountList;
import org.upi.system_1_2.RespListAccount.AccountList.Account;
import org.upi.system_1_2.RespType;

import psp.constants.ServiceRequestName;
import psp.dbservice.mgmt.PspMgmtService;
import psp.upi.process.factory.UpiCoreHandler;
import psp.upi.process.util.TransactionThread;
import psp.util.DtoObjectUtil;
import psp.util.PspClientTool;
import psp.util.dto.HeadDto;
import psp.util.dto.TxnDto;
import psp.util.upi.client.UpiClientService;
import psp.util.upiclient.UpiDataPreparationUtility;
/**
 * @author prasadj
 *
 */
@Component("reqListAccountHandler")
public class ReqListAccountHandlerImpl extends UpiCoreHandler {

	@Autowired
	private PspMgmtService pspMgmtService;
	
	@Autowired
	private UpiClientService upiClientService;
	
	public ReqListAccountHandlerImpl(){
	}

	@Override
	public String handleProcess( String upiData ) {
		ReqListAccount rpr = PspClientTool.convertUpiRequest(upiData, ReqListAccount.class);
		
		pepareReqListAccountResponse( rpr );
		return PspClientTool.requestToString(prepareAckData(ServiceRequestName.REQ_LIST_ACC, rpr.getHead().getMsgId(), null)); 
	}
	
	private void pepareReqListAccountResponse( ReqListAccount reqPay ){
		Date date = new Date();
		String messageId = DtoObjectUtil.constructMessageId();
		HeadDto head = DtoObjectUtil.constructHeadDto(messageId, date);
		PayTrans payTrans = reqPay.getTxn();
		TxnDto txn = DtoObjectUtil.constructTxnDto(payTrans.getId(), payTrans.getNote(), null, payTrans.getRefId(), payTrans.getRefUrl(), date, payTrans.getType().name());

		RespType resp = new RespType();
		resp.setReqMsgId(reqPay.getHead().getMsgId());
		resp.setResult("SUCCESS");
		
		RespListAccount respListAccount = new RespListAccount();
		respListAccount.setHead(UpiDataPreparationUtility.convertHeader(head));
		respListAccount.setTxn(UpiDataPreparationUtility.convertTxn(txn));
		respListAccount.setResp(resp);
		respListAccount.setAccountList(prepareAccountList(reqPay.getPayer().getName()));
		
		TransactionThread transactionThread = new TransactionThread(respListAccount, ServiceRequestName.RESP_LIST_ACCOUNT, upiClientService, null, null);
		new Thread(transactionThread).start();
	}

	private AccountList prepareAccountList(String name) {
		AccountList accountList = new AccountList();
		List<Account> accounts = accountList.getAccount();
		
		Account account = new Account();
		account.setAccRefNumber("123654789");
		account.setAccType(ListedAccountType.SAVINGS);
		account.setAeba(AebaType.Y);
		account.setIfsc("AACA0000023");
		account.setMaskedAccnumber("************5012");
		account.setMbeba(AebaType.Y);
		account.setName(name);
		account.setMmid("TARA123");
		
		List<CredsAllowedType> allowedTypes = account.getCredsAllowed();
		CredsAllowedType credsAllowedType = new CredsAllowedType();
		credsAllowedType.setDLength(6);
		credsAllowedType.setDType(CredDataTypeConstant.ALPHANUMERIC);
		credsAllowedType.setType(CredType.PIN);
		credsAllowedType.setSubType(CredSubType.MPIN);
		allowedTypes.add(credsAllowedType);
		
		accounts.add(account);
		
		account = new Account();
		account.setAccRefNumber("987654321");
		account.setAccType(ListedAccountType.CURRENT);
		account.setAeba(AebaType.N);
		account.setIfsc("AACA0000024");
		account.setMaskedAccnumber("************8099");
		account.setMbeba(AebaType.Y);
		account.setName(name);
		account.setMmid("TARA654");
		
		allowedTypes = account.getCredsAllowed();
		credsAllowedType = new CredsAllowedType();
		credsAllowedType.setDLength(6);
		credsAllowedType.setDType(CredDataTypeConstant.ALPHANUMERIC);
		credsAllowedType.setType(CredType.PIN);
		credsAllowedType.setSubType(CredSubType.MPIN);
		allowedTypes.add(credsAllowedType);
		accounts.add(account);
		return accountList;
	}
	
	
}