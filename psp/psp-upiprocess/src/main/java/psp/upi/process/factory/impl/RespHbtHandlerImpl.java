/**
 * 
 */
package psp.upi.process.factory.impl;

import org.springframework.stereotype.Component;
import org.upi.system_1_2.RespHbt;

import psp.constants.ServiceRequestName;
import psp.upi.process.factory.UpiCoreHandler;
import psp.util.PspClientTool;

/**
 * @author prasadj
 *
 */
@Component("respHbtHandler")
public class RespHbtHandlerImpl extends UpiCoreHandler {

	public RespHbtHandlerImpl(){
	}
	
	@Override
	public String handleProcess(String upiData) {
		RespHbt hbr = PspClientTool.convertUpiRequest(upiData, RespHbt.class);
		return PspClientTool.requestToString(prepareAckData(ServiceRequestName.RESP_HBT, hbr.getResp().getReqMsgId(), null)); 
	}

}