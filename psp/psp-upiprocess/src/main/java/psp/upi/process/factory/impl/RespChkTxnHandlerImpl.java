/**
 * 
 */
package psp.upi.process.factory.impl;

import org.springframework.stereotype.Component;
import org.upi.system_1_2.RespChkTxn;

import psp.constants.ServiceRequestName;
import psp.upi.process.factory.UpiCoreHandler;
import psp.util.PspClientTool;

/**
 * @author prasadj
 *
 */
@Component("respChkTxnHandler")
public class RespChkTxnHandlerImpl extends UpiCoreHandler {

	public RespChkTxnHandlerImpl(){
	}
	
	@Override
	public String handleProcess(String upiData) {
		RespChkTxn respChkTxn = PspClientTool.convertUpiRequest(upiData, RespChkTxn.class);
		return PspClientTool.requestToString(prepareAckData(ServiceRequestName.RESP_CHK_TXN, respChkTxn.getHead().getMsgId(), null));
	}

}
