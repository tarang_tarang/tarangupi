/**
 * 
 */
package psp.upi.process.factory.impl;

import org.springframework.stereotype.Component;
import org.upi.system_1_2.RespValAdd;

import psp.constants.ServiceRequestName;
import psp.upi.process.factory.UpiCoreHandler;
import psp.util.PspClientTool;

/**
 * @author prasadj
 *
 */
@Component("respValAddHandler")
public class RespValAddHandlerImpl extends UpiCoreHandler {

	public RespValAddHandlerImpl(){
	}
	
	@Override
	public String handleProcess(String upiData) {
		RespValAdd response = PspClientTool.convertUpiRequest(upiData, RespValAdd.class);
		return PspClientTool.requestToString(prepareAckData(ServiceRequestName.RESP_VAL_ADD, response.getHead().getMsgId(), null));
	}

}