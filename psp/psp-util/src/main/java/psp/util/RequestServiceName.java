package psp.util;

public interface RequestServiceName {

	String REQ_PAY = "ReqPay";
	
	String RESP_AUTH_DETAIL = "RespAuthDetails";
	
	String RESP_TXN_CONF = "RespTxnConfirmation";
	
	String REQ_LIST_ACC = "ReqListAccount";
	
	String REQ_BAL_ENQ = "ReqBalEnq";
	
	String REQ_TXN_CHK = "ReqChkTxn";
	
	String REQ_HBT = "ReqHbt";
	
	String REQ_LIST_PSP = "ReqListPsp";
	
	String REQ_LIST_ACC_PVD = "ReqListAccPvd";
	
	String REQ_LIST_KEYS = "ReqListKeys";
	
	String REQ_LIST_VAE = "ReqListVae";
	
	String REQ_MNG_VAE = "ReqManageVae";
	
	String REQ_OTP = "ReqOtp";
	
	String REQ_PEND_MSG = "ReqPendingMsg";
	
	String REQ_VAL_ADD = "ReqValAdd";
	
	String REQ_SET_CRED = "ReqSetCre";
	
	String REQ_MOB = "RegMob";
	
}