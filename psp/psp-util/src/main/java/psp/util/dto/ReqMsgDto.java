/**
 * 
 */
package psp.util.dto;

/**
 * @author prasadj
 *
 */
public class ReqMsgDto {

	private String type;
	
	private String value;
	
	private String addr;
	
	public ReqMsgDto(){
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}	
}