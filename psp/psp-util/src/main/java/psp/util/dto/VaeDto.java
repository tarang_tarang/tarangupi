/**
 * 
 */
package psp.util.dto;

/**
 * @author prasadj
 *
 */
public class VaeDto {
	
	private String op;
	
	private String name;
	
	private String addr;
	
	private String logo;
	
	private String url;
	
	public VaeDto(){
	}

	public String getOp() {
		return op;
	}

	public void setOp(String op) {
		this.op = op;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}	
}