/**
 * 
 */
package psp.util.dto;

/**
 * @author manasp
 *
 */
public class LinkDto {

	private String linkType;
	
	private String value;

	public String getLinkType() {
		return linkType;
	}

	public void setLinkType(String linkType) {
		this.linkType = linkType;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
