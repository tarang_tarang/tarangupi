/**
 * 
 */
package psp.util.dto;

import java.util.List;

/**
 * @author prasadj
 *
 */
public class RegDetailsDto {

	private String mobile;
	
	private String cardNum;
	
	private String expDate;
	
	private List<CredDto> creds;
	
	public RegDetailsDto(){
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getCardNum() {
		return cardNum;
	}

	public void setCardNum(String cardNum) {
		this.cardNum = cardNum;
	}

	public String getExpDate() {
		return expDate;
	}

	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}

	public List<CredDto> getCreds() {
		return creds;
	}

	public void setCreds(List<CredDto> creds) {
		this.creds = creds;
	}	
}