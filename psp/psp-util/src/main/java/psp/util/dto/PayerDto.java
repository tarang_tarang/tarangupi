/**
 * 
 */
package psp.util.dto;

import java.util.List;

/**
 * @author prasadj
 *
 */

public class PayerDto {
	
	private String addr;

	private String name;
	
	private String type;
	
	private String seqNum;
	
	private String code;
	
	private IdentityInfoDto info;
	
	private DeviceDto device;
	
	private AcDto ac;
	
	private List<CredDto> creds;
	
	private List<CredDto> newCreds;
	
	private String amount;
	
	public PayerDto(){
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public IdentityInfoDto getInfo() {
		return info;
	}

	public void setInfo(IdentityInfoDto info) {
		this.info = info;
	}

	public DeviceDto getDevice() {
		return device;
	}

	public void setDevice(DeviceDto device) {
		this.device = device;
	}

	public AcDto getAc() {
		return ac;
	}

	public void setAc(AcDto ac) {
		this.ac = ac;
	}

	public List<CredDto> getCreds() {
		return creds;
	}

	public void setCreds(List<CredDto> creds) {
		this.creds = creds;
	}

	public List<CredDto> getNewCreds() {
		return newCreds;
	}

	public void setNewCreds(List<CredDto> newCreds) {
		this.newCreds = newCreds;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getSeqNum() {
		return seqNum;
	}

	public void setSeqNum(String seqNum) {
		this.seqNum = seqNum;
	}		
}