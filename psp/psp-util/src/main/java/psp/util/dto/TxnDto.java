/**
 * 
 */
package psp.util.dto;

import java.util.Date;

/**
 * @author prasadj
 *
 */
public class TxnDto {

	private String id;
	
	private String note;
	
	private String refId;
	
	private String refUrl;
	
	private Date startTime;
	
	private String type;
	
	private String orgTxnId;
	
	private boolean isRuleRequired;
	
	public TxnDto(){		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public String getRefUrl() {
		return refUrl;
	}

	public void setRefUrl(String refUrl) {
		this.refUrl = refUrl;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOrgTxnId() {
		return orgTxnId;
	}

	public void setOrgTxnId(String orgTxnId) {
		this.orgTxnId = orgTxnId;
	}

	public boolean isRuleRequired() {
		return isRuleRequired;
	}

	public void setRuleRequired(boolean isRuleRequired) {
		this.isRuleRequired = isRuleRequired;
	}	
}