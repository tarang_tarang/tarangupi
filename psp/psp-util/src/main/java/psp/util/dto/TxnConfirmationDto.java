/**
 * 
 */
package psp.util.dto;

/**
 * @author prasadj
 *
 */
public class TxnConfirmationDto {

	private String note;
	
	private String orgStatus;
	
	private String orgErrCode;
	
	private String type;
	
	public TxnConfirmationDto(){
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getOrgStatus() {
		return orgStatus;
	}

	public void setOrgStatus(String orgStatus) {
		this.orgStatus = orgStatus;
	}

	public String getOrgErrCode() {
		return orgErrCode;
	}

	public void setOrgErrCode(String orgErrCode) {
		this.orgErrCode = orgErrCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}	
}