/**
 * 
 */
package psp.util.dto;

import java.util.Date;

/**
 * @author prasadj
 *
 */
public class HeadDto {

	private String version;
	
	private Date startTime;
	
	private String orgId;
	
	private String msgId;
	
	public HeadDto(){
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	
}