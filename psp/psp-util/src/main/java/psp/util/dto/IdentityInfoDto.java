/**
 * 
 */
package psp.util.dto;

/**
 * @author prasadj
 *
 */
public class IdentityInfoDto {
	
	private String type;
	
	private String verifiedName;
	
	private Boolean verifiedAddress;

	public IdentityInfoDto(){
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getVerifiedName() {
		return verifiedName;
	}

	public void setVerifiedName(String verifiedName) {
		this.verifiedName = verifiedName;
	}

	public Boolean getVerifiedAddress() {
		return verifiedAddress;
	}

	public void setVerifiedAddress(Boolean verifiedAddress) {
		this.verifiedAddress = verifiedAddress;
	}
	
}