/**
 * 
 */
package psp.util.dto;

/**
 * @author prasadj
 *
 */
public class CredDto {

	private String type;
	
	private String subType;
	
	private String data;
		
	public CredDto(){
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
	
}