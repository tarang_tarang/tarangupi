package psp.util;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

/**
 * @author manasp
 *
 */
public class PspClientTool {

	private PspClientTool(){
	}
	
	public static String requestToString(Object object) {
		String response = null;
		DOMResult result;
		Document doc = null;
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(object.getClass());
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			result= new DOMResult();
			jaxbMarshaller.marshal(object, result);
			doc = (Document)result.getNode();
			doc = SignatureGenUtil.signdoc(doc);
			response = convertDocumentToString(doc);
		} catch ( Exception e) {
			throw new RuntimeException("Exception while preparing to xml.");
		}
		System.out.println("--------------------------- Request :  "+ object.getClass().getName() +"-----------------------------");
		System.out.println(response);
		return response;
	}
	
	public static <T> T convertUpiRequest(String response, Class<T> className){
		System.out.println("--------------------------- Response :  "+ className.getName() +"-----------------------------");
		System.out.println(response);
		return JAXB.unmarshal(new StringReader(response), className);
	}
	
	private static String convertDocumentToString(Document doc) {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = tf.newTransformer();
            // below code to remove XML declaration
            // transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
            String output = writer.getBuffer().toString();
            return output;
        } catch (TransformerException e) {
        	throw new RuntimeException(e.getMessage());
        }
    }
	
	 public static Document convertStringToDocument(String xmlStr) {
    	 DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    	 factory.setNamespaceAware(true); 
         DocumentBuilder builder;
         try  {
             builder = factory.newDocumentBuilder();
             return builder.parse( new InputSource( new StringReader( xmlStr ) ) );
         } catch (Exception e) {
        	 throw new RuntimeException(e.getMessage());
         }
    }
	
}