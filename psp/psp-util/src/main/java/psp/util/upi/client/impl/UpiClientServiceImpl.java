package psp.util.upi.client.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.upi.system_1_2.ReqBalEnq;
import org.upi.system_1_2.ReqChkTxn;
import org.upi.system_1_2.ReqHbt;
import org.upi.system_1_2.ReqListAccPvd;
import org.upi.system_1_2.ReqListAccount;
import org.upi.system_1_2.ReqListKeys;
import org.upi.system_1_2.ReqListPsp;
import org.upi.system_1_2.ReqListVae;
import org.upi.system_1_2.ReqManageVae;
import org.upi.system_1_2.ReqOtp;
import org.upi.system_1_2.ReqPay;
import org.upi.system_1_2.ReqPendingMsg;
import org.upi.system_1_2.ReqRegMob;
import org.upi.system_1_2.ReqSetCre;
import org.upi.system_1_2.ReqValAdd;
import org.upi.system_1_2.RespAuthDetails;
import org.upi.system_1_2.RespBalEnq;
import org.upi.system_1_2.RespListAccount;
import org.upi.system_1_2.RespOtp;
import org.upi.system_1_2.RespPay;
import org.upi.system_1_2.RespRegMob;
import org.upi.system_1_2.RespTxnConfirmation;

import psp.constants.CommonConstants;
import psp.constants.ServiceRequestName;
import psp.util.PspClientTool;
import psp.util.dto.CredDto;
import psp.util.dto.HeadDto;
import psp.util.dto.LinkDto;
import psp.util.dto.PayeeDto;
import psp.util.dto.PayerDto;
import psp.util.dto.RegDetailsDto;
import psp.util.dto.ReqMsgDto;
import psp.util.dto.TxnDto;
import psp.util.dto.VaeDto;
import psp.util.upi.client.UpiClientService;
import psp.util.upiclient.UpiDataPreparationUtility;
import upi.client.UpiClient;

@Component("upiServiceCall")
public class UpiClientServiceImpl implements UpiClientService, ServiceRequestName, CommonConstants {

	@Autowired
	private UpiClient upiClient;
	
	public UpiClientServiceImpl(){
	}
	
	private String callUpi(String data, String api, String version, String txnId) {
		return upiClient.reqUpi(data, api, version, txnId);
	}
	
	@Override
	public String reqBalEnq(HeadDto headDto, TxnDto txnDto, PayerDto payerDto) {
		ReqBalEnq balEnq = new ReqBalEnq();
		balEnq.setHead(UpiDataPreparationUtility.convertHeader(headDto));
		balEnq.setTxn(UpiDataPreparationUtility.convertTxn(txnDto));
		balEnq.setPayer(UpiDataPreparationUtility.convertPayer(payerDto));
		return callUpi(PspClientTool.requestToString(balEnq), REQ_BAL_ENQ, PSP_VERSION, txnDto.getId());
	}

	@Override
	public String reqChkTxn(HeadDto headDto, TxnDto txnDto) {
		ReqChkTxn chkTxn = new ReqChkTxn();
		chkTxn.setHead(UpiDataPreparationUtility.convertHeader(headDto));
		chkTxn.setTxn(UpiDataPreparationUtility.convertTxn(txnDto));
		return callUpi(PspClientTool.requestToString(chkTxn), REQ_TXN_CHK, PSP_VERSION, txnDto.getId());
	}

	@Override
	public String reqHbt(HeadDto headDto, TxnDto txnDto) {
		ReqHbt reqHbt = new ReqHbt();
		reqHbt.setHbtMsg(UpiDataPreparationUtility.convertHbtMsg());
		reqHbt.setHead(UpiDataPreparationUtility.convertHeader(headDto));
		reqHbt.setTxn(UpiDataPreparationUtility.convertTxn(txnDto));	
		return callUpi(PspClientTool.requestToString(reqHbt), REQ_HBT, PSP_VERSION, txnDto.getId());
	}
	
	@Override
	public String reqListPsp(HeadDto headDto, TxnDto txnDto) {
		ReqListPsp listPsp = new ReqListPsp();
		listPsp.setHead(UpiDataPreparationUtility.convertHeader(headDto));
		listPsp.setTxn(UpiDataPreparationUtility.convertTxn(txnDto));
		return callUpi(PspClientTool.requestToString(listPsp), REQ_TXN_CHK, PSP_VERSION, txnDto.getId());
	}

	@Override
	public String reqListAccount(HeadDto headDto, TxnDto txnDto,PayerDto payerDto, LinkDto linkDto) {
		ReqListAccount listAccount = new ReqListAccount();
		listAccount.setHead(UpiDataPreparationUtility.convertHeader(headDto));
		listAccount.setTxn(UpiDataPreparationUtility.convertTxn(txnDto));
		listAccount.setPayer(UpiDataPreparationUtility.convertPayer(payerDto));
		listAccount.setLink(UpiDataPreparationUtility.convertLink(linkDto));
		return callUpi(PspClientTool.requestToString(listAccount), REQ_LIST_ACC, PSP_VERSION, txnDto.getId());
	}

	@Override
	public String reqListAccPvd(HeadDto headDto, TxnDto txnDto) {
		ReqListAccPvd reqListAccPvd = new ReqListAccPvd();
		reqListAccPvd.setHead(UpiDataPreparationUtility.convertHeader(headDto));
		reqListAccPvd.setTxn(UpiDataPreparationUtility.convertTxn(txnDto));
		return callUpi(PspClientTool.requestToString(reqListAccPvd), REQ_LIST_ACC_PVD, PSP_VERSION, txnDto.getId());
	}

	@Override
	public String reqListKeys(HeadDto headDto, TxnDto txnDto) {
		ReqListKeys listKeys = new ReqListKeys();
		listKeys.setHead(UpiDataPreparationUtility.convertHeader(headDto));
		listKeys.setTxn(UpiDataPreparationUtility.convertTxn(txnDto));
		return callUpi(PspClientTool.requestToString(listKeys), REQ_LIST_KEYS, PSP_VERSION, txnDto.getId());
	}

	@Override
	public String reqListVae(HeadDto headDto, TxnDto txnDto) {
		ReqListVae reqListVae = new ReqListVae();
		reqListVae.setHead(UpiDataPreparationUtility.convertHeader(headDto));
		reqListVae.setTxn(UpiDataPreparationUtility.convertTxn(txnDto));
		return callUpi(PspClientTool.requestToString(reqListVae), REQ_LIST_VAE, PSP_VERSION, txnDto.getId());
	}

	@Override
	public String reqManageVae(HeadDto headDto, TxnDto txnDto, List<VaeDto> vaes) {
		ReqManageVae reqManageVae = new ReqManageVae();
		reqManageVae.setHead(UpiDataPreparationUtility.convertHeader(headDto));
		reqManageVae.setTxn(UpiDataPreparationUtility.convertTxn(txnDto));
		/*request.setVerifiedAddressEntry(UpiDataPreparationUtility.convertVae(vaes));*///TODO later
		return callUpi(PspClientTool.requestToString(reqManageVae), REQ_MNG_VAE, PSP_VERSION, txnDto.getId());
	}

	@Override
	public String reqOtp(HeadDto headDto, TxnDto txnDto, PayerDto payerDto) {
		ReqOtp reqOtp = new ReqOtp();
		reqOtp.setHead(UpiDataPreparationUtility.convertHeader(headDto));
		reqOtp.setTxn(UpiDataPreparationUtility.convertTxn(txnDto));
		reqOtp.setPayer(UpiDataPreparationUtility.convertPayer(payerDto));
		return callUpi(PspClientTool.requestToString(reqOtp), REQ_OTP, PSP_VERSION, txnDto.getId());
	}

	@Override
	public String reqPendingMsg(HeadDto headDto, TxnDto txnDto,	ReqMsgDto reqMsgDto) {
		ReqPendingMsg reqPendingMsg = new ReqPendingMsg();
		reqPendingMsg.setHead(UpiDataPreparationUtility.convertHeader(headDto));
		reqPendingMsg.setTxn(UpiDataPreparationUtility.convertTxn(txnDto));
		reqPendingMsg.setReqMsg(UpiDataPreparationUtility.convertReqPenMsg(reqMsgDto));
		return callUpi(PspClientTool.requestToString(reqPendingMsg), REQ_PEND_MSG, PSP_VERSION, txnDto.getId());
	}

	@Override
	public String reqValAdd(HeadDto headDto, TxnDto txnDto, PayerDto payerDto,PayeeDto payeeDto) {
		ReqValAdd reqValAdd = new ReqValAdd();
		reqValAdd.setHead(UpiDataPreparationUtility.convertHeader(headDto));
		reqValAdd.setTxn(UpiDataPreparationUtility.convertTxn(txnDto));
		reqValAdd.setPayer(UpiDataPreparationUtility.convertPayer(payerDto));
		reqValAdd.setPayee(UpiDataPreparationUtility.convertPayee(payeeDto));
		return callUpi(PspClientTool.requestToString(reqValAdd), REQ_VAL_ADD, PSP_VERSION, txnDto.getId());
	}

	@Override
	public String reqSetCre(HeadDto headDto, TxnDto txnDto, PayerDto payerDto) {
		ReqSetCre reqSetCre = new ReqSetCre();
		reqSetCre.setHead(UpiDataPreparationUtility.convertHeader(headDto));
		reqSetCre.setTxn(UpiDataPreparationUtility.convertTxn(txnDto));
		reqSetCre.setPayer(UpiDataPreparationUtility.convertPayer(payerDto));
		return callUpi(PspClientTool.requestToString(reqSetCre), REQ_SET_CRED, PSP_VERSION, txnDto.getId());
	}

	@Override
	public String regMob(HeadDto headDto, TxnDto txnDto, PayerDto payerDto,	RegDetailsDto regDetailsDto) {
		ReqRegMob reqRegMob = new ReqRegMob();
		reqRegMob.setHead(UpiDataPreparationUtility.convertHeader(headDto));
		reqRegMob.setTxn(UpiDataPreparationUtility.convertTxn(txnDto));
		reqRegMob.setPayer(UpiDataPreparationUtility.convertPayer(payerDto));
		reqRegMob.setRegDetails(UpiDataPreparationUtility.convertRegDetails(regDetailsDto));
		return callUpi(PspClientTool.requestToString(reqRegMob), REQ_MOB_REG, PSP_VERSION, txnDto.getId());
	}

	@Override
	public String reqGetToken(HeadDto headDto, TxnDto txnDto, List<CredDto> creds) {
		ReqListKeys listKeys = new ReqListKeys();
		listKeys.setHead(UpiDataPreparationUtility.convertHeader(headDto));
		listKeys.setTxn(UpiDataPreparationUtility.convertTxn(txnDto));
		listKeys.setCreds(UpiDataPreparationUtility.convertCredDtos(creds));
		return callUpi(PspClientTool.requestToString(listKeys), REQ_LIST_KEYS, PSP_VERSION, txnDto.getId());
	}

	@Override
	public String reqPay(HeadDto headDto, TxnDto txnDto, PayerDto payerDto, PayeeDto payeeDto) {
		ReqPay reqPay = new ReqPay();
		reqPay.setHead(UpiDataPreparationUtility.convertHeader(headDto));
		reqPay.setTxn(UpiDataPreparationUtility.convertTxn(txnDto));
		reqPay.setPayer(UpiDataPreparationUtility.convertPayer(payerDto));
		reqPay.setPayees(UpiDataPreparationUtility.convertPayees(payeeDto));
		reqPay.setMeta(UpiDataPreparationUtility.prepareMetaData());
		return callUpi(PspClientTool.requestToString(reqPay), REQ_PAY, PSP_VERSION, txnDto.getId());
	}

	@Override
	public String respAuthDetails(RespAuthDetails respAuthDetails) {
		return callUpi(PspClientTool.requestToString(respAuthDetails), RESP_AUTH_DETAIL, PSP_VERSION, respAuthDetails.getTxn().getId());
	}

	@Override
	public String respTxnConfirmation(RespTxnConfirmation respTxnConfirmation) {
		return callUpi(PspClientTool.requestToString(respTxnConfirmation), RESP_TXN_CONF, PSP_VERSION, respTxnConfirmation.getTxn().getId());
	}

	@Override
	public String respOtp(RespOtp respOtp) {
		return callUpi(PspClientTool.requestToString(respOtp), RESP_OTP, PSP_VERSION, respOtp.getTxn().getId());
	}
	
	@Override
	public String respPay(RespPay respPay) {
		return callUpi(PspClientTool.requestToString(respPay), RESP_PAY, PSP_VERSION, respPay.getTxn().getId());
	}

	@Override
	public String respListAccount(RespListAccount respListAccount) {
		return callUpi(PspClientTool.requestToString(respListAccount), RESP_LIST_ACCOUNT, PSP_VERSION, respListAccount.getTxn().getId());
	}

	@Override
	public String respRegMob(RespRegMob respRegMob) {
		return callUpi(PspClientTool.requestToString(respRegMob), RESP_REG_MOB, PSP_VERSION, respRegMob.getTxn().getId());
	}

	@Override
	public String respBalEnq(RespBalEnq respBalEnq) {
		return callUpi(PspClientTool.requestToString(respBalEnq), RESP_BAL_ENQ, PSP_VERSION, respBalEnq.getTxn().getId());

	}

}
