package psp.util.upi.client;

import java.util.List;

import org.upi.system_1_2.RespAuthDetails;
import org.upi.system_1_2.RespBalEnq;
import org.upi.system_1_2.RespListAccount;
import org.upi.system_1_2.RespOtp;
import org.upi.system_1_2.RespPay;
import org.upi.system_1_2.RespRegMob;
import org.upi.system_1_2.RespTxnConfirmation;

import psp.util.dto.CredDto;
import psp.util.dto.HeadDto;
import psp.util.dto.LinkDto;
import psp.util.dto.PayeeDto;
import psp.util.dto.PayerDto;
import psp.util.dto.RegDetailsDto;
import psp.util.dto.ReqMsgDto;
import psp.util.dto.TxnDto;
import psp.util.dto.VaeDto;

public interface UpiClientService {

	 String reqBalEnq(HeadDto headDto, TxnDto txnDto, PayerDto payerDto);
	 
	 String reqChkTxn(HeadDto headDto, TxnDto txnDto);
	 
	 String reqHbt(HeadDto headDto, TxnDto txnDto);
	 
	 String reqListPsp(HeadDto headDto, TxnDto txnDto);
	 
	 String reqListAccount(HeadDto headDto, TxnDto txnDto, PayerDto payerDto, LinkDto linkDto);
	 
	 String reqListAccPvd(HeadDto headDto, TxnDto txnDto);
	 
	 String reqListKeys(HeadDto headDto, TxnDto txnDto);
	 
	 String reqListVae(HeadDto headDto, TxnDto txnDto);
	 
	 String reqManageVae(HeadDto headDto, TxnDto txnDto, List<VaeDto> vaes);
	 
	 String reqOtp(HeadDto headDto, TxnDto txnDto, PayerDto payerDto);
	 
	 String reqPendingMsg(HeadDto headDto, TxnDto txnDto, ReqMsgDto reqMsgDto);
	 
	 String reqValAdd(HeadDto headDto, TxnDto txnDto, PayerDto payerDto, PayeeDto payeeDto);
	 
	 String reqSetCre(HeadDto headDto, TxnDto txnDto, PayerDto payerDto);
	 
	 String regMob(HeadDto headDto, TxnDto txnDto, PayerDto payerDto, RegDetailsDto regDetailsDto);
	 
	 String reqGetToken(HeadDto headDto, TxnDto txnDto, List<CredDto> creds);
	 
	 String reqPay(HeadDto headDto, TxnDto txnDto, PayerDto payerDto, PayeeDto payeeDto);
	 
	 String respAuthDetails(RespAuthDetails respAuthDetails);
	 
	 String respTxnConfirmation(RespTxnConfirmation respTxnConfirmation);
	 
	 String respOtp(RespOtp respOtp);
	 
	 String respPay(RespPay respPay);
	 
	 String respListAccount(RespListAccount respListAccount);
	 
	 String respRegMob(RespRegMob respRegMob);
	 
	 String respBalEnq(RespBalEnq respBalEnq);
	 
}
