/**
 * 
 */
package psp.util.upiclient;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.upi.system_1_2.AccountDetailType;
import org.upi.system_1_2.AccountType;
import org.upi.system_1_2.AmountType;
import org.upi.system_1_2.CredsType;
import org.upi.system_1_2.DeviceTagNameType;
import org.upi.system_1_2.DeviceType;
import org.upi.system_1_2.ExpireRuleConstant;
import org.upi.system_1_2.HbtMsgType;
import org.upi.system_1_2.HeadType;
import org.upi.system_1_2.IdentityType;
import org.upi.system_1_2.InfoType;
import org.upi.system_1_2.MetaTagNameType;
import org.upi.system_1_2.MobRegDetailsNameType;
import org.upi.system_1_2.PayTrans;
import org.upi.system_1_2.PayeeType;
import org.upi.system_1_2.PayeesType;
import org.upi.system_1_2.PayerType;
import org.upi.system_1_2.RatingType;
import org.upi.system_1_2.ReqHbt.HbtMsg;
import org.upi.system_1_2.ReqListAccount.Link;
import org.upi.system_1_2.ReqPay.Meta;
import org.upi.system_1_2.ReqPendingMsg;
import org.upi.system_1_2.ReqRegMob;
import org.upi.system_1_2.RulesType;
import org.upi.system_1_2.RulesType.Rule;

import psp.constants.CommonConstants;
import psp.constants.DateUtil;
import psp.upi.constants.SampleData;
import psp.upi.model.TxnConfirmation;
import psp.upi.model.VerifiedAddressEntry;
import psp.util.DtoObjectUtil;
import psp.util.dto.AcDto;
import psp.util.dto.CredDto;
import psp.util.dto.DeviceDto;
import psp.util.dto.HeadDto;
import psp.util.dto.IdentityInfoDto;
import psp.util.dto.LinkDto;
import psp.util.dto.PayeeDto;
import psp.util.dto.PayerDto;
import psp.util.dto.RegDetailsDto;
import psp.util.dto.ReqMsgDto;
import psp.util.dto.TxnConfirmationDto;
import psp.util.dto.TxnDto;
import psp.util.dto.VaeDto;

/**
 * @author gowthamb
 *
 */
public class UpiDataPreparationUtility implements CommonConstants, SampleData{

	public UpiDataPreparationUtility() {
	}
	
	public static HeadType convertHeader(HeadDto dto){
		HeadType headType = new HeadType();
		if (dto.getMsgId() != null && !dto.getMsgId().isEmpty()) {
			headType.setMsgId(dto.getMsgId());
		}
		if (dto.getOrgId() != null && !dto.getOrgId().isEmpty()) {
			headType.setOrgId(dto.getOrgId());
		}
		if (dto.getStartTime() != null) {
			headType.setTs(DateUtil.getUpiDateStrFormat(dto.getStartTime()));
		}
		else {
			headType.setTs(DateUtil.getUpiDateStrFormat(new Date()) );
		}
		if (dto.getVersion() != null && !dto.getVersion().isEmpty()) {
			headType.setVer(dto.getVersion());
		}
		else {
			headType.setVer(UPI_VERSION);
		}
		return headType;
	}
	
	public static PayTrans convertTxn(TxnDto dto){
		PayTrans txn = new PayTrans();
		//TODO: riskScores, rules are pending
		if (dto.isRuleRequired()) {//TODO need to change logic added for collect payment
			RulesType rulesType = new RulesType();
			List<Rule> rules = rulesType.getRule();
			Rule rule = new Rule();
			rule.setName(ExpireRuleConstant.EXPIREAFTER);
			rule.setValue("10080");
			rules.add(rule);
			txn.setRules(rulesType);
		}
		
		
		if(dto.getId() != null && !dto.getId().isEmpty()){
			txn.setId(dto.getId());
		}
		if (dto.getNote() != null && !dto.getNote().isEmpty()) {
			txn.setNote(dto.getNote());
		}
		if (dto.getRefId() != null && !dto.getRefId().isEmpty()) {
			txn.setRefId(dto.getRefId());
		}
		if (dto.getRefUrl() != null && !dto.getRefUrl().isEmpty()) {
			txn.setRefUrl(dto.getRefUrl());
		}
		if (dto.getStartTime() != null) {
			txn.setTs(DateUtil.getUpiDateStrFormat(dto.getStartTime()));
		}
		else {
			txn.setTs(DateUtil.getUpiDateStrFormat(new Date()) );
		}
		if (dto.getType() != null) {
			txn.setType(DtoObjectUtil.getPayConstant(dto.getType()));
		}
		if (dto.getOrgTxnId() != null && !dto.getOrgTxnId().isEmpty()) {
			txn.setOrgTxnId(dto.getOrgTxnId());
		}
		return txn;
	}
	
	public static PayerType convertPayer(PayerDto dto){
		
		PayerType payer = new PayerType();
		if (dto.getAddr() != null && !dto.getAddr().isEmpty()) {
			payer.setAddr(dto.getAddr());
		}
		if (dto.getName() != null && !dto.getName().isEmpty()) {
			payer.setName(dto.getName());
		}
		if (dto.getType() != null && !dto.getType().isEmpty()) {
			payer.setType(DtoObjectUtil.getPayerConstant(dto.getType()));
		}
		if (dto.getSeqNum() != null && !dto.getSeqNum().isEmpty()) {
			payer.setSeqNum(dto.getSeqNum());
		}
		if (dto.getCode() != null && !dto.getCode().isEmpty()) {
			payer.setCode(dto.getCode());
		}
		
		if (dto.getInfo() != null
				&& ((dto.getInfo().getType() != null && !dto.getInfo().getType().isEmpty())
						|| (dto.getInfo().getVerifiedName() != null && !dto.getInfo().getVerifiedName().isEmpty())
						|| dto.getInfo().getVerifiedAddress() != null ) ){
			payer.setInfo(covertInfo(dto.getInfo()));
		}
		if (dto.getDevice() != null) {
			payer.setDevice(convertDevice(dto.getDevice()));
		}
		if (dto.getAc() != null) {
			payer.setAc(convertAc(dto.getAc()));
		}
		List<CredDto> credDtos = dto.getCreds();
		if(credDtos != null && !credDtos.isEmpty()){
			CredsType credentials = convertCredDtos(credDtos);
			payer.setCreds(credentials);
		}
		List<CredDto> newCredDtos = dto.getNewCreds();
		if(newCredDtos != null && !newCredDtos.isEmpty()){
			CredsType credentials = convertCredDtos(credDtos);
			payer.setNewCred(credentials);
		}
		if(dto.getAmount() != null){
			AmountType amount = new AmountType();
			amount.setValue(dto.getAmount());
			amount.setCurr("INR");
			payer.setAmount(amount);
		}
		return payer;
	}
		
	public static AccountType convertAc(AcDto dto){
		AccountType ac = new AccountType();
		String addrType = dto.getAddrType();
		if(addrType != null && !addrType.isEmpty()){
			ac.setAddrType(DtoObjectUtil.getAddressType(dto.getAddrType()));
			List<AccountType.Detail> details = ac.getDetail();
			
			AccountType.Detail detail = new AccountType.Detail();
			if( "MOBILE".equals(addrType) ){
				detail.setName(AccountDetailType.MOBNUM);
				detail.setValue(dto.getMobnum());
				details.add(detail);
				detail = new AccountType.Detail();
				detail.setName(AccountDetailType.MMID);
				detail.setValue(dto.getMmid());
				details.add(detail);
			}
			else if( "ACCOUNT".equals(addrType) ){
				detail.setName(AccountDetailType.IFSC);
				detail.setValue(dto.getIfsc());
				details.add(detail);
				detail = new AccountType.Detail();
				detail.setName(AccountDetailType.ACTYPE);
				detail.setValue(dto.getActype());
				details.add(detail);
				detail = new AccountType.Detail();
				detail.setName(AccountDetailType.ACNUM);
				detail.setValue(dto.getAcnum());
				details.add(detail);
			}
			else if( "AADHAAR".equals(addrType) ){
				detail.setName(AccountDetailType.IIN);
				detail.setValue(dto.getIin());
				details.add(detail);
				detail = new AccountType.Detail();
				detail.setName(AccountDetailType.UIDNUM);
				detail.setValue(dto.getUidnum());
				details.add(detail);
			}
		}
		return ac;
	}
	
	public static DeviceType convertDevice(DeviceDto dto){//TODO need to check
		DeviceType device = new DeviceType();
		List<DeviceType.Tag> tags = device.getTag();
		DeviceType.Tag tag = null;
		if(dto.getMobile() != null && !dto.getMobile().isEmpty()){
			tag = new DeviceType.Tag();
			tag.setName(DeviceTagNameType.MOBILE);
			tag.setValue(dto.getMobile());
			tags.add(tag);
		}
		if (dto.getGeoCode() != null && !dto.getGeoCode().isEmpty()) {
			tag = new DeviceType.Tag();
			tag.setName(DeviceTagNameType.GEOCODE);
			tag.setValue(dto.getGeoCode());
			tags.add(tag);
		}
		if (dto.getLocation() != null && !dto.getLocation().isEmpty()) {
			tag = new DeviceType.Tag();
			tag.setName(DeviceTagNameType.LOCATION);
			tag.setValue(dto.getLocation());
			tags.add(tag);
		}
		if (dto.getIp() != null && !dto.getIp().isEmpty()) {
			tag = new DeviceType.Tag();
			tag.setName(DeviceTagNameType.IP);
			tag.setValue(dto.getIp());
			tags.add(tag);
		}
		if (dto.getType() != null && !dto.getType().isEmpty()) {
			tag = new DeviceType.Tag();
			tag.setName(DeviceTagNameType.TYPE);
			tag.setValue(dto.getType());
			tags.add(tag);
		}
		if (dto.getTerminalId() != null && !dto.getTerminalId().isEmpty()) {
			tag = new DeviceType.Tag();
			tag.setName(DeviceTagNameType.ID);
			tag.setValue(dto.getTerminalId());
			tags.add(tag);
		}
		if (dto.getOs() != null && !dto.getOs().isEmpty()) {
			tag = new DeviceType.Tag();
			tag.setName(DeviceTagNameType.OS);
			tag.setValue(dto.getOs());
			tags.add(tag);
		}
		if (dto.getApp() != null && !dto.getApp().isEmpty()) {
			tag = new DeviceType.Tag();
			tag.setName(DeviceTagNameType.APP);
			tag.setValue(dto.getApp());
			tags.add(tag);
		}
		if (dto.getCapability() != null && !dto.getCapability().isEmpty()) {
			tag = new DeviceType.Tag();
			tag.setName(DeviceTagNameType.CAPABILITY);
			tag.setValue(dto.getCapability());
			tags.add(tag);
		}
		return device;
	}
	
	public static InfoType covertInfo(IdentityInfoDto dto){
		InfoType info = new InfoType();
		IdentityType identity = new IdentityType();
		if ( (dto.getType() != null && !dto.getType().isEmpty())
				|| (dto.getVerifiedName() != null && !dto.getVerifiedName().isEmpty()) ){
			
			identity.setType(DtoObjectUtil.getIdentityConstant(dto.getType()));
			identity.setVerifiedName(dto.getVerifiedName());
			info.setIdentity(identity);
		}
		if(dto.getVerifiedAddress() != null) {
			RatingType rating = new RatingType();
			rating.setValue("");//TODO no data
			rating.setVerifiedAddress(DtoObjectUtil.getWhiteListedConstant(dto.getVerifiedAddress()));
			info.setRating(rating);
		}
		return info;	
	}
	     
	public static PayeeType convertPayee(PayeeDto dto){
		PayeeType payee = new PayeeType();

		if (dto.getAddr() != null && !dto.getAddr().isEmpty()) {
			payee.setAddr(dto.getAddr());
		}
		if (dto.getName() != null && !dto.getName().isEmpty()) {
			payee.setName(dto.getName());
		}
		if (dto.getType() != null && !dto.getType().isEmpty()) {
			payee.setType(DtoObjectUtil.getPayerConstant(dto.getType()));
		}
		if (dto.getSeqNum() != null && !dto.getSeqNum().isEmpty()) {
			payee.setSeqNum(dto.getSeqNum());
		}
		if (dto.getCode() != null && !dto.getCode().isEmpty()) {
			payee.setCode(dto.getCode());
		}
		if (dto.getInfo() != null
				&& ((dto.getInfo().getType() != null && !dto.getInfo().getType().isEmpty())
						|| (dto.getInfo().getVerifiedName() != null && !dto.getInfo().getVerifiedName().isEmpty())
						|| dto.getInfo().getVerifiedAddress() != null ) ){
			payee.setInfo(covertInfo(dto.getInfo()));
		}		
		if (dto.getDevice() != null) {
			payee.setDevice(convertDevice(dto.getDevice()));
		}
		if (dto.getAc() != null) {
			payee.setAc(convertAc(dto.getAc()));
		}
		if(dto.getAmount() != null){
			AmountType amount = new AmountType();
			amount.setValue(dto.getAmount());
			amount.setCurr("INR");
			payee.setAmount(amount);
		}
		return payee;
	}
	
	
	public static ReqPendingMsg.ReqMsg convertReqPenMsg(ReqMsgDto dto){
		ReqPendingMsg.ReqMsg reqPendingMsg = new ReqPendingMsg.ReqMsg();
		if (dto.getAddr() != null && !dto.getAddr().isEmpty()) {
			reqPendingMsg.setAddr(dto.getAddr());
		}
		if (dto.getType() != null && !dto.getType().isEmpty()) {
			reqPendingMsg.setType(DtoObjectUtil.getReqMsgType(dto.getType()));
		}
		if (dto.getValue() != null && !dto.getValue().isEmpty()) {
			reqPendingMsg.setValue(dto.getValue());
		}
		return reqPendingMsg;
	}
	
	public static CredsType.Cred convertCredDto(CredDto dto){
		CredsType.Cred cred = new CredsType.Cred();
	    if(dto.getType() != null && !dto.getType().isEmpty()){
	    	cred.setType(DtoObjectUtil.getCredType(dto.getType()));
	    }
	    if(dto.getSubType() != null && !dto.getSubType().isEmpty()){
	    	cred.setSubType(DtoObjectUtil.getCredSubType(dto.getSubType()));
	    }
	    CredsType.Cred.Data  dataObj = null;
	    if (dto.getData() != null && !dto.getData().isEmpty()) {
	    	dataObj = new CredsType.Cred.Data();
	    	dataObj.setValue(dto.getData());
			dataObj.setCode("NPCI");//TODO sample data
	    	dataObj.setKi("20150822");//TODO sample data
		}	    
	    cred.setData(dataObj);
		return cred;
	}
	
	public static CredsType convertCredDtos(List<CredDto> credDtos) {
		CredsType credsType = new CredsType();
		List<CredsType.Cred>  creds = credsType.getCred();
		for(CredDto credDto: credDtos){
			creds.add(convertCredDto(credDto));
		}
		return credsType;
	}
	
	public static List<VerifiedAddressEntry> convertVae(List<VaeDto> vaes){
		List<VerifiedAddressEntry> verifiedAddressEntry = new ArrayList<VerifiedAddressEntry>();		
		for(int i = 0; i < vaes.size(); i++){			
			VaeDto vae = vaes.get(i);			
			VerifiedAddressEntry verifyaddr = new VerifiedAddressEntry();			
			if(vae.getAddr() != null && !vae.getAddr().isEmpty()){
				verifyaddr.setAddr(vae.getAddr());
			}						
			if (vae.getOp() != null && !vae.getOp().isEmpty()) {
				verifyaddr.setOp(vae.getOp());
			}
			if (vae.getName() != null && !vae.getName().isEmpty()) {
				verifyaddr.setName(vae.getName());
			}			
			if (vae.getLogo() != null && !vae.getLogo().isEmpty()) {
				verifyaddr.setLogo(vae.getLogo());
			}			
			if (vae.getUrl() != null && !vae.getUrl().isEmpty()) {
				verifyaddr.setUrl(vae.getUrl());
			}			
			verifiedAddressEntry.set(i, verifyaddr);
		}
		return verifiedAddressEntry;
	}
	
	public static TxnConfirmation convertTxnConfirmation(TxnConfirmationDto dto) {		
		TxnConfirmation txnConfirm = new TxnConfirmation();		
		if (dto.getNote() != null && !dto.getNote().isEmpty()) {
			txnConfirm.setNote(dto.getNote());
		}	
		if (dto.getOrgStatus() != null && !dto.getOrgStatus().isEmpty()) {
			txnConfirm.setOrgStatus(dto.getOrgStatus());
		}				
		if (dto.getOrgErrCode() != null && !dto.getOrgErrCode().isEmpty()) {
			txnConfirm.setOrgErrCode(dto.getOrgErrCode());
		}		
		if (dto.getType() != null && !dto.getType().isEmpty()) {
			txnConfirm.setType(dto.getType());
		}
		return txnConfirm;
	}
	
	public static PayeesType convertPayees(PayeeDto payeeDto) {
		PayeesType payees = new PayeesType();
		
		if (payeeDto != null) {
			List<PayeeType> payeeTypes = payees.getPayee();
			payeeTypes.add(convertPayee(payeeDto));
		}
		return payees;
	}
		
	public static ReqRegMob.RegDetails convertRegDetails(RegDetailsDto dto){
		ReqRegMob.RegDetails regDetails = new ReqRegMob.RegDetails();
		List<ReqRegMob.RegDetails.Detail> details = regDetails.getDetail();
		ReqRegMob.RegDetails.Detail detail = null;
		if(dto.getMobile() != null && !dto.getMobile().isEmpty()) {
			detail = new ReqRegMob.RegDetails.Detail();
			detail.setName(MobRegDetailsNameType.MOBILE);
			detail.setValue(dto.getMobile());
			details.add(detail);
		}
		if(dto.getCardNum() != null && !dto.getCardNum().isEmpty()) {
			detail = new ReqRegMob.RegDetails.Detail();
			detail.setName(MobRegDetailsNameType.CARDDIGITS);
			detail.setValue(dto.getCardNum());
			details.add(detail);
		}
		if(dto.getExpDate() != null && !dto.getExpDate().isEmpty()) {
			detail = new ReqRegMob.RegDetails.Detail();
			detail.setName(MobRegDetailsNameType.EXPDATE);
			detail.setValue(dto.getExpDate());
			details.add(detail);
		}
		
		List<CredDto> credDtos = dto.getCreds();
		if(credDtos != null && !credDtos.isEmpty()) {
			CredsType credentials = new CredsType();
			List<CredsType.Cred> creds = credentials.getCred();
			for(CredDto credDto: credDtos){
				creds.add(convertCredDto(credDto));
			}
			regDetails.setCreds(credentials);
		}
		return regDetails;
	}

	public static HbtMsg convertHbtMsg(){
		HbtMsg heartbeatMessage = new HbtMsg();
		heartbeatMessage.setType(HbtMsgType.ALIVE);
		heartbeatMessage.setValue("NA");
		return heartbeatMessage;
	}
	
	public static Link convertLink(LinkDto dto){
		Link link = null;
		if(dto != null) {
			link = new Link();
			link.setType(DtoObjectUtil.getLinkType(dto.getLinkType()));
			link.setValue(dto.getValue());
		}
		return link;
	}
	
	public static Meta prepareMetaData() {
		Meta meta = new Meta();
		List<org.upi.system_1_2.ReqPay.Meta.Tag> tags = meta.getTag();
		org.upi.system_1_2.ReqPay.Meta.Tag tag = new org.upi.system_1_2.ReqPay.Meta.Tag();
		tag.setName(MetaTagNameType.PAYREQSTART);
		tag.setValue(DateUtil.getUpiDateStrFormat(new Date()));
		tags.add(tag);
		tag = new org.upi.system_1_2.ReqPay.Meta.Tag();
		tag.setName(MetaTagNameType.PAYREQEND);
		tag.setValue(DateUtil.getUpiDateStrFormat(new Date()));
		tags.add(tag);
		return meta;
	}
}