/**
 * 
 */
package psp.util;

import java.io.BufferedReader;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;


@SuppressWarnings({"rawtypes", "unchecked"})
public class JsonUtil {

	private static final Logger LOGGER = Logger.getLogger(JsonUtil.class.getName());
	
	private JsonUtil(){
	}
	
	public static Object getJsonObject(HttpServletRequest request, Class clas){
		String data = readJsonData(request);
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		try{
			return mapper.readValue(data, clas);
		}
		catch(Exception e){
			LOGGER.warn(e.getMessage());
			LOGGER.debug(e.getMessage(), e);
		}
		return null;
	}
	
	public static String readJsonData(HttpServletRequest request){
		try {
			StringBuilder sb = new StringBuilder();
		    BufferedReader reader = request.getReader();
		    try {
		        String line;
		        while ((line = reader.readLine()) != null) {
		            sb.append(line);
		        }
		    } finally {
		        reader.close();
		    }
		    return sb.toString();
		}
		catch(Exception e){
			LOGGER.warn(e.getMessage());
			LOGGER.debug(e.getMessage(), e);
		}
		return null;
	}
	
	public static String generateJSonString(Map<String, String> jsonKeyValues) {
		JSONObject jsonObject = new JSONObject();
		Iterator<String> keySetIterator = jsonKeyValues.keySet().iterator();
		while (keySetIterator.hasNext()) {
			String key = keySetIterator.next();
			try {
				jsonObject.put(key, jsonKeyValues.get(key));
			} catch (JSONException e) {
				return null;
			}
		}
		return jsonObject.toString();
	}
	
	public static Object getObject(String data, Class clas){
		ObjectMapper mapper = new ObjectMapper();
		try{
			return mapper.readValue(data, clas);
		}
		catch(Exception e){
			LOGGER.warn(e.getMessage());
			LOGGER.debug(e.getMessage(), e);
		}
		return null;
	}
	
}