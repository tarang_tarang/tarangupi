/**
 * 
 */
package psp.util.gcm;

import java.io.IOException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import psp.common.PropertyReader;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

/**
 * @author prasadj
 *
 */
@Component("gcmNotification")
public class GcmNotificationImpl implements GcmNotification {

	private static final Logger LOG = LoggerFactory.getLogger(GcmNotificationImpl.class);
	
	private final static int RETRIES = 30;

	@Autowired
	private PropertyReader propertyReader;
	
	public GcmNotificationImpl(){
	}
	
	static {//Added for ignoring ssl handshake error with google
		 TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
	            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
	                return null;
	            }
	            public void checkClientTrusted(X509Certificate[] certs, String authType) { }
	            public void checkServerTrusted(X509Certificate[] certs, String authType) { }
	        }
	     };
		SSLContext sc = null;
		try {
			sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
		} catch (Exception e) {
			e.printStackTrace();
		}
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };
	}
	
	@Override
	public Result send(String notificationId, String notificationType, String notificationMessage, String deviceId) {
		LOG.info("GCM notification request started from psp to gcm");
		Message message = new Message.Builder().timeToLive(propertyReader.getTimeToLive()).addData("ID", notificationId).addData("TYPE", notificationType).addData("MESSAGE", notificationMessage).build();
		Sender sender = new Sender(propertyReader.getAuthKey());
		Result result = null;
		try {
			result = sender.send(message, deviceId, RETRIES);
			LOG.debug("gcm notification input notificationId: " + notificationId + "\trnsId: " + deviceId);
			LOG.debug("gcm notification result messageId: " + result.getMessageId() + "\tregistrationId: " + result.getCanonicalRegistrationId());
		} 
		catch (IOException e) {
			LOG.error("Exception in GCMServerImpl" + e.getMessage());
		}
		LOG.info("GCM notification request completed from psp to gcm");
		return result;
	}

}