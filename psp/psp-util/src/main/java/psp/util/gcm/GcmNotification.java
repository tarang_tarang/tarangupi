/**
 * 
 */
package psp.util.gcm;

import com.google.android.gcm.server.Result;

/**
 * @author prasadj
 *
 */
public interface GcmNotification {

	Result send(String notificationId, String notificationType, String notificationMessage, String deviceId);
		
}