/**
 * 
 */
package psp.util;

import java.util.Date;
import java.util.List;

import org.upi.system_1_2.AddressType;
import org.upi.system_1_2.CredSubType;
import org.upi.system_1_2.CredType;
import org.upi.system_1_2.IdentityConstant;
import org.upi.system_1_2.LinkType;
import org.upi.system_1_2.PayConstant;
import org.upi.system_1_2.PayerConstant;
import org.upi.system_1_2.ReqMsgType;
import org.upi.system_1_2.WhiteListedConstant;

import psp.constants.CommonConstants;
import psp.constants.ServiceRequestName;
import psp.mobile.model.request.MobileDevice;
import psp.upi.constants.PayerType;
import psp.util.dto.AcDto;
import psp.util.dto.CredDto;
import psp.util.dto.DeviceDto;
import psp.util.dto.HeadDto;
import psp.util.dto.IdentityInfoDto;
import psp.util.dto.PayeeDto;
import psp.util.dto.PayerDto;
import psp.util.dto.RegDetailsDto;
import psp.util.dto.TxnDto;

/**
 * @author prasadj
 *
 */
public class DtoObjectUtil implements CommonConstants {

	private DtoObjectUtil(){
	}

	public static HeadDto constructHeadDto(String msgId, Date startTime) {
		HeadDto head = new HeadDto();
		head.setMsgId(msgId);
		head.setOrgId(ORG_ID);
		head.setStartTime(startTime);
		head.setVersion(UPI_VERSION);
		return head;
	}
	
	public static TxnDto constructTxnDto(String id, String note, String orgTxnId, String refId, String refUrl, Date startTime, String type) {
		TxnDto txn = new TxnDto();
		txn.setId(id);
		txn.setNote(note);
		txn.setOrgTxnId(orgTxnId);
		txn.setRefId(REF_ID);
		txn.setRefUrl(REF_URL);
		txn.setStartTime(startTime);
		txn.setType(type);
		return txn;
	}
	
	public static PayerDto constructPayerDto(String addr, String fullName, String type, 
			AcDto ac, String amount, List<CredDto> creds, List<CredDto> newCreds, MobileDevice md, IdentityInfoDto info) {
		
		PayerDto payerDto = new PayerDto();
		payerDto.setAddr(addr);
		payerDto.setName(fullName);
		payerDto.setSeqNum(PAYER_SEQ_NUM);
		payerDto.setCode("");
		payerDto.setType(type);

		payerDto.setAc(ac);
		payerDto.setAmount(amount);
		payerDto.setCreds(creds);
		payerDto.setNewCreds(newCreds);
		if(null != md) {
			payerDto.setDevice(constructDeviceDto(md));	
		}
		payerDto.setInfo(info);
		return payerDto;
	}
	
	public static DeviceDto constructDeviceDto(MobileDevice md){
		DeviceDto dd = new DeviceDto();
		dd.setApp(md.getApp());
		dd.setCapability(md.getCapability());
		dd.setGeoCode(md.getGeoCode());
		dd.setIp(md.getIp());
		dd.setLocation(md.getLocation());
		dd.setMobile(md.getMobile());
		dd.setOs(md.getMobile());
		dd.setTerminalId(md.getTerminalId());
		dd.setType(md.getType());
		return dd;
	}
	
	public static String constructMessageId(){
		return PRODUCT_ID + String.valueOf(System.currentTimeMillis());
	}
	
	public static String constructTxnId(){
		return PRODUCT_ID + String.valueOf(System.currentTimeMillis());
	}
	
	public static PayConstant getPayConstant(String type) {
		PayConstant constant = null;
		if (type.equals(ServiceRequestName.LIST_ACCOUNT_PROVIDER)) {
			constant = PayConstant.LIST_ACC_PVD;
		}
		else if (type.equals(ServiceRequestName.REQ_HBT)) {
			constant = PayConstant.HBT;
		}
		else if (type.equals(ServiceRequestName.REQ_OTP)) {
			constant = PayConstant.OTP;
		}
		else if (type.equals(ServiceRequestName.REQ_LIST_ACC)) {
			constant = PayConstant.LIST_ACCOUNT;
		}
		else if (type.equals(ServiceRequestName.SET_CRED)) {
			constant = PayConstant.SET_CRE;
		}
		else if (type.equals(PayConstant.LIST_KEYS.name())) {
			constant = PayConstant.LIST_KEYS;
		}
		else if (type.equals(PayConstant.GET_TOKEN.name())) {
			constant = PayConstant.GET_TOKEN;
		}
		else if (type.equals(PayConstant.PAY.name())) {
			constant = PayConstant.PAY;
		}
		else if (type.equals(PayConstant.COLLECT.name())) {
			constant = PayConstant.COLLECT;
		}
		else if (type.equals(PayConstant.TXN_CONFIRMATION.name())) {
			constant = PayConstant.TXN_CONFIRMATION;
		}
		else if (type.equals(ServiceRequestName.REQ_MOB_REG)) {//TODO As per spec type is RegMob
			constant = PayConstant.REQ_REG_MOB;
		}
		else if (type.equals(PayConstant.CREDIT.name())) {
			constant = PayConstant.CREDIT;
		}
		else if (type.equals(PayConstant.DEBIT.name())) {
			constant = PayConstant.DEBIT;
		}
		else if (type.equals(ServiceRequestName.LIST_ACCOUNT)) {
			constant = PayConstant.LIST_ACCOUNT;
		}
		else if (type.equals(ServiceRequestName.REQ_BAL_ENQ)) {
			constant = PayConstant.BAL;
		}
		
		return constant;
	}
	
	public static PayerConstant getPayerConstant(String type) {
		PayerConstant constant = null;
		if (type.equals(PayerType.PERSON.name())) {
			constant = PayerConstant.PERSON;
		}
		else {
			constant = PayerConstant.ENTITY;
		}
		return constant;
	}
	
	public static IdentityConstant getIdentityConstant(String type) {
		IdentityConstant constant = null;
		if (type.equals(IdentityConstant.BANK.name())) {//TODO need to check
			constant = IdentityConstant.BANK;
		}
		else if (type.equals(IdentityConstant.AADHAAR.name())) {//TODO need to check
			constant = IdentityConstant.AADHAAR;
		} 
		else if (type.equals(IdentityConstant.ACCOUNT.name())) {//TODO need to check
			constant = IdentityConstant.ACCOUNT;
		}
		else {
			constant = IdentityConstant.PAN;
		}
		return constant;
	}
	
	public static WhiteListedConstant getWhiteListedConstant(Boolean type){
		WhiteListedConstant constant = null;
		if (type.equals(WhiteListedConstant.TRUE.name())) {
			constant = WhiteListedConstant.TRUE;
		}
		else {
			constant = WhiteListedConstant.FALSE;
		}
		return constant;
	}
	
	public static AddressType getAddressType(String type){
		AddressType constant = null;
		if (type.equals(AddressType.AADHAAR.name())) {
			constant = AddressType.AADHAAR;
		}
		else if (type.equals(AddressType.ACCOUNT.name())) {
			constant = AddressType.ACCOUNT;
		}
		else if (type.equals(AddressType.CARD.name())) {
			constant = AddressType.CARD;
		}
		else if (type.equals(AddressType.MOBILE.name())) {
			constant = AddressType.MOBILE;
		}
		return constant;
	}
	
	public static CredType getCredType(String type){
		CredType constant = null;
		if (type.equals(CredType.AADHAAR.name())) {
			constant = CredType.AADHAAR;
		}
		else if (type.equals(CredType.CARD.name())) {
			constant = CredType.CARD;
		}
		else if (type.equals(CredType.CHALLENGE.name())) {
			constant = CredType.CHALLENGE;
		}
		else if (type.equals(CredType.OTP.name())) {
			constant = CredType.OTP;
		}
		else if (type.equals(CredType.PIN.name())) {
			constant = CredType.PIN;
		}
		else if (type.equals(CredType.PRE_APPROVED.name())) {
			constant = CredType.PRE_APPROVED;
		}
		return constant;
	}
	
	public static CredSubType getCredSubType(String type){
		CredSubType constant = null;
		if (type.equals(CredSubType.CVV_1.name())) {
			constant = CredSubType.CVV_1;
		}
		else if (type.equals(CredSubType.CVV_2.name())) {
			constant = CredSubType.CVV_2;
		}
		else if (type.equals(CredSubType.EMAIL.name())) {
			constant = CredSubType.EMAIL;
		}
		else if (type.equals(CredSubType.EMV.name())) {
			constant = CredSubType.EMV;
		}
		else if (type.equals(CredSubType.FIR.name())) {
			constant = CredSubType.FIR;
		}
		else if (type.equals(CredSubType.FMR.name())) {
			constant = CredSubType.FMR;
		}
		else if (type.equals(CredSubType.HOTP.name())) {
			constant = CredSubType.HOTP;
		}
		else if (type.equals(CredSubType.IIR.name())) {
			constant = CredSubType.IIR;
		}
		else if (type.equals(CredSubType.INITIAL.name())) {
			constant = CredSubType.INITIAL;
		}
		else if (type.equals(CredSubType.MPIN.name())) {
			constant = CredSubType.MPIN;
		}
		else if (type.equals(CredSubType.NA.name())) {
			constant = CredSubType.NA;
		}
		else if (type.equals(CredSubType.OTP.name())) {
			constant = CredSubType.OTP;
		}
		else if (type.equals(CredSubType.RESET.name())) {
			constant = CredSubType.RESET;
		}
		else if (type.equals(CredSubType.ROTATE.name())) {
			constant = CredSubType.ROTATE;
		}
		else if (type.equals(CredSubType.SMS.name())) {
			constant = CredSubType.SMS;
		}
		else if (type.equals(CredSubType.TOTP.name())) {
			constant = CredSubType.TOTP;
		}
		return constant;
	}
	
	public static ReqMsgType getReqMsgType(String type) {
		ReqMsgType msgType = null;
		if (type.equals(ReqMsgType.AADHAAR.name())) {
			msgType = ReqMsgType.AADHAAR;
		}
		else if (type.equals(ReqMsgType.MOBILE.name())) {
			msgType = ReqMsgType.MOBILE;
		}
		return msgType;
	}
	
	public static LinkType getLinkType(String type) {
		LinkType linkType = null;
		if (type.equals(LinkType.AADHAAR.name())) {
			linkType = LinkType.AADHAAR;
		}
		else if (type.equals(LinkType.MOBILE.name())) {
			linkType = LinkType.MOBILE;
		}
		return linkType;
	}
	
	public static PayeeDto constructPayeeDto(String addr, String fullName, String type, 
			AcDto ac, String amount, List<CredDto> creds, List<CredDto> newCreds, MobileDevice md, IdentityInfoDto info) {
		
		PayeeDto payeeDto = new PayeeDto();
		payeeDto.setAddr(addr);
		payeeDto.setName(fullName);
		payeeDto.setSeqNum(PAYER_SEQ_NUM);
		payeeDto.setCode("");
		payeeDto.setType(type);

		payeeDto.setAc(ac);
		payeeDto.setAmount(amount);
		if(null != md) {
			payeeDto.setDevice(constructDeviceDto(md));	
		}
		payeeDto.setInfo(info);
		return payeeDto;
	}
	
	public static RegDetailsDto constructRegDetailsDto(String mobile, String cardDigits, String expDate, List<CredDto> creds) {
		RegDetailsDto regDto = new RegDetailsDto();
		regDto.setCardNum(cardDigits);
		regDto.setExpDate(expDate);
		regDto.setMobile(mobile);
		regDto.setCreds(creds);
		return regDto;
	}
	
	public static IdentityInfoDto constructIdentityInfoDto(String address){
		IdentityInfoDto identityInfoDto = new IdentityInfoDto();
		identityInfoDto.setType(IdentityConstant.ACCOUNT.name());
		identityInfoDto.setVerifiedName(address);
		identityInfoDto.setVerifiedAddress(Boolean.TRUE);
		return identityInfoDto;
	}
	
	public static CredDto constructCredDto(String dataValue, CredType type, CredSubType subType){
		CredDto credDto = new CredDto();
		credDto.setData(dataValue);
		credDto.setType(type.name());
		credDto.setSubType(subType.name());
		return credDto;
	}
}