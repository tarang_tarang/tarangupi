/**
 * 
 */
package psp.server.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import psp.common.PropertyReader;
import psp.dbservice.mgmt.PspMgmtService;
import psp.mobile.process.util.GetBankProvidersTimerTask;
import psp.util.upi.client.UpiClientService;

/**
 * @author manasp
 *
 */
public class PspServerListener implements ServletContextListener {

	private static final Logger LOGGER = Logger.getLogger(PspServerListener.class.getName());
	
	private static final String GET_BANK_PROVIDERS_TIMER = "getBankAccountTimer";
	
	@Override
	public void contextInitialized(ServletContextEvent event) {
		try {
			WebApplicationContext servletContext =  WebApplicationContextUtils.getWebApplicationContext(event.getServletContext()); 
			PspMgmtService pspMgmtService = (PspMgmtService) servletContext.getBean("pspMgmtService");
			UpiClientService upiClientService = (UpiClientService) servletContext.getBean("upiServiceCall"); 
			PropertyReader propertyLoader = (PropertyReader) servletContext.getBean("propertyReader"); 
			GetBankProvidersTimerTask getBankAccountsTimerTask = GetBankProvidersTimerTask.getGetBankAccountsTimerTask(pspMgmtService, upiClientService, propertyLoader);
			getBankAccountsTimerTask.startTimer();
			event.getServletContext().setAttribute(GET_BANK_PROVIDERS_TIMER, getBankAccountsTimerTask);
		}
		catch(Exception e){
			LOGGER.warn(e.getMessage());
			LOGGER.debug(e.getMessage(), e);
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		GetBankProvidersTimerTask getBankAccountsTimerTask = (GetBankProvidersTimerTask)event.getServletContext().getAttribute(GET_BANK_PROVIDERS_TIMER);
		getBankAccountsTimerTask.stopTimer();
	}

}