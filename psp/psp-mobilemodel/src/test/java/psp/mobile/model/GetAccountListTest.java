package psp.mobile.model;

import java.util.List;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import psp.common.model.AccountSummary;
import psp.mobile.model.request.AcDetails;
import psp.mobile.model.request.GetAccountListRequest;
import psp.mobile.model.request.MobileCredentials;
import psp.mobile.model.response.GetAccountListResponse;
import psp.mobile.model.util.RequestTestUtility;

public class GetAccountListTest {

	@Test
	public void constructJsonStringGetAccountListRequest01Test(){
		GetAccountListRequest request = new GetAccountListRequest();
		request.setIsBenAccDetails(true);
		MobileCredentials credentials = RequestTestUtility.prepareCredentials();
		request.setMobileCredentials(credentials);		
		JSONObject jsonObj = new JSONObject(request.toJsonString());
		Assert.assertEquals(true, jsonObj.getBoolean("isBenAccDetails"));
		JSONObject jsonObj1 =  new JSONObject(jsonObj.get("mobileCredentials").toString());	
		Assert.assertEquals("123", jsonObj1.get("dataCode"));
		Assert.assertEquals("222", jsonObj1.get("dataki"));
		Assert.assertEquals("MIIBIjANBgkqhkiG9w0BB", jsonObj1.get("dataValue"));
		Assert.assertEquals("MPIN", jsonObj1.get("subType"));
		Assert.assertEquals("PIN", jsonObj1.get("type"));

		AcDetails acDetailsAccount = RequestTestUtility.prepareAcDetails("ACCOUNT");
		request.setAcDetails(acDetailsAccount);
		JSONObject jsonObjAccount = new JSONObject(request.toJsonString());
		JSONObject jsonObj1Account =  new JSONObject(jsonObjAccount.get("acDetails").toString());	
		Assert.assertEquals("111111111111", jsonObj1Account.get("acnum"));
		Assert.assertEquals("ACCOUNT", jsonObj1Account.get("addrType"));
		Assert.assertEquals("sbm123", jsonObj1Account.get("ifsc"));
		Assert.assertEquals("SAVINGS", jsonObj1Account.get("actype"));
		
		AcDetails acDetailsMobile = RequestTestUtility.prepareAcDetails("MOBILE");
		request.setAcDetails(acDetailsMobile);
		JSONObject jsonObjMobile = new JSONObject(request.toJsonString());
		JSONObject jsonObj1Mobile =  new JSONObject(jsonObjMobile.get("acDetails").toString());	
		Assert.assertEquals("MOBILE", jsonObj1Mobile.get("addrType"));
		Assert.assertEquals("1234567", jsonObj1Mobile.get("mmid"));
		Assert.assertEquals("1234567890", jsonObj1Mobile.get("mobnum"));

		AcDetails acDetailsAadhaar = RequestTestUtility.prepareAcDetails("AADHAAR");
		request.setAcDetails(acDetailsAadhaar );
		JSONObject jsonObjAadhaar  = new JSONObject(request.toJsonString());
		JSONObject jsonObj1Aadhaar  =  new JSONObject(jsonObjAadhaar .get("acDetails").toString());	
		Assert.assertEquals("AADHAAR", jsonObj1Aadhaar.get("addrType"));
		Assert.assertEquals("12345678", jsonObj1Aadhaar.get("iin"));
		Assert.assertEquals("12345678333", jsonObj1Aadhaar.get("uidnum"));
		
		AcDetails acDetailsCard = RequestTestUtility.prepareAcDetails("CARD");
		request.setAcDetails(acDetailsCard);
		JSONObject jsonObjCard  = new JSONObject(request.toJsonString());
		JSONObject jsonObj1Card  =  new JSONObject(jsonObjCard .get("acDetails").toString());	
		Assert.assertEquals("CARD", jsonObj1Card.get("addrType"));
		Assert.assertEquals("111111111111", jsonObj1Card.get("cardNum"));
		Assert.assertEquals("SAVINGS", jsonObj1Card.get("actype"));
	}
	
	@Test
	public void constructJsonStringGetAccountListRequest02Test(){
		GetAccountListRequest request = new GetAccountListRequest();
		request.setIsBenAccDetails(false);
		MobileCredentials credentials = RequestTestUtility.prepareCredentialsEmpty();
		request.setMobileCredentials(credentials);		
		AcDetails acDetails = RequestTestUtility.prepareAcDetailsEmpty();
		request.setAcDetails(acDetails);
		JSONObject jsonObj = new JSONObject(request.toJsonString());
		Assert.assertEquals(false, jsonObj.getBoolean("isBenAccDetails"));
		JSONObject jsonObj1 =  new JSONObject(jsonObj.get("mobileCredentials").toString());	
		Assert.assertEquals("", jsonObj1.get("dataCode"));
		Assert.assertEquals("", jsonObj1.get("dataki"));
		Assert.assertEquals("", jsonObj1.get("dataValue"));
		Assert.assertEquals("", jsonObj1.get("subType"));
		Assert.assertEquals("", jsonObj1.get("type"));
		JSONObject jsonObj2 =  new JSONObject(jsonObj.get("acDetails").toString());	
		Assert.assertEquals("", jsonObj2.get("acnum"));
		Assert.assertEquals("", jsonObj2.get("addrType"));
		Assert.assertEquals("", jsonObj2.get("ifsc"));
		Assert.assertEquals("", jsonObj2.get("actype"));
		Assert.assertEquals("", jsonObj2.get("iin"));
		Assert.assertEquals("", jsonObj2.get("mmid"));
		Assert.assertEquals("", jsonObj2.get("mobnum"));
		Assert.assertEquals("", jsonObj2.get("cardNum"));
	}

	@Test
	public void constructGetAccountListRequestFromStrTest(){
		String jsonStr = "{\n" +
                "    \"otpErrMsg\": \"No Error\",\n" +  
                "    \"mmidErrMsg\": \"MMID is more than seven char\",\n" +
                "    \"statusCode\": \"104\",\n" +
                "    \"statusMessage\": \"Failed\",\n" +
                "\"accountSummaryList\": [" + "{" +
				" \"id\": \"1\",\n" +
				"\"maskedAccnumber\": \"****1234567\",\n" + 
				"\"ifsc\": \"HDFC1470\",\n" + 
				"\"name\": \"DEBA\",\n" + 
				"\"aeba\": \"Y\",\n" + 
				"\"virtualAddress\": \"DEBAA@tarang\",\n"+
				"\"isPinSet\": \"TRUE\""
				 + "},\n" +
				 "{" +
					" \"id\": \"2\",\n" +
					"\"maskedAccnumber\": \"****7654321\",\n" + 
					"\"ifsc\": \"AXIS2468\",\n" + 
					"\"name\": \"KARAN\",\n" + 
					"\"aeba\": \"N\",\n" + 
					"\"virtualAddress\": \"KARAN@tarang\",\n"+
					"\"isPinSet\": \"FALSE\""
					 + "}"
				+ "]"
				 + "}";            
             
		GetAccountListResponse actList = GetAccountListResponse.constructGetAccountListResponse(jsonStr);
		Assert.assertNotNull(actList);
		Assert.assertEquals("No Error", actList.getOtpErrMsg());
		Assert.assertEquals("MMID is more than seven char", actList.getMmidErrMsg());
		Assert.assertEquals("Failed", actList.getStatusMessage());
		Assert.assertEquals("104", actList.getStatusCode());
		List<AccountSummary> accountSummaryList = actList.getAccountSummaryList();
		AccountSummary actSummary = accountSummaryList.get(0);
		Assert.assertEquals(Long.parseLong("1"), actSummary.getId().longValue());
		Assert.assertEquals("****1234567", actSummary.getMaskedAccnumber());				
		Assert.assertEquals("HDFC1470", actSummary.getIfsc());
		Assert.assertEquals("DEBA", actSummary.getName());				
		Assert.assertEquals("Y", actSummary.getAeba());
		Assert.assertEquals("DEBAA@tarang", actSummary.getVirtualAddress());				
		Assert.assertEquals(Boolean.parseBoolean("TRUE"), actSummary.getIsPinSet());
		AccountSummary actSummary2 = accountSummaryList.get(1);
		Assert.assertEquals(Long.parseLong("2"), actSummary2.getId().longValue());
		Assert.assertEquals("****7654321", actSummary2.getMaskedAccnumber());				
		Assert.assertEquals("AXIS2468", actSummary2.getIfsc());
		Assert.assertEquals("KARAN", actSummary2.getName());				
		Assert.assertEquals("N", actSummary2.getAeba());
		Assert.assertEquals("KARAN@tarang", actSummary2.getVirtualAddress());				
		Assert.assertEquals(Boolean.parseBoolean("FALSE"), actSummary2.getIsPinSet());
	}
	
	@Test
	public void constructAccountSummaryFromJsonObjTest() {		
		JSONObject obj = new JSONObject();
		obj.put("id", new Long(1));
		obj.put("maskedAccnumber", "*****12345");
		obj.put("ifsc", "5738921");
		obj.put("name", "Deba K");
		obj.put("aeba", "Y");
		obj.put("virtualAddress", "debaa@tarangtech");		
		obj.put("isPinSet", Boolean.TRUE);		
		AccountSummary actSummary = AccountSummary.constructAccountSummary(obj);
		Assert.assertEquals(Long.parseLong("1"), actSummary.getId().longValue());
		Assert.assertEquals("*****12345", actSummary.getMaskedAccnumber());				
		Assert.assertEquals("5738921", actSummary.getIfsc());
		Assert.assertEquals("Deba K", actSummary.getName());				
		Assert.assertEquals("Y", actSummary.getAeba());
		Assert.assertEquals("debaa@tarangtech", actSummary.getVirtualAddress());				
		Assert.assertEquals(Boolean.parseBoolean("TRUE"), actSummary.getIsPinSet());
	}
}