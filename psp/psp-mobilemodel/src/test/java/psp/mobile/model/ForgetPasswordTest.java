package psp.mobile.model;

import org.junit.Assert;
import org.junit.Test;

import psp.mobile.model.response.ForgetPasswordResponse;


public class ForgetPasswordTest {

	@Test
	public void constructForgetPasswordResFromJsonStrTest(){	
		String jsonStr = "{\n" +    
	            "    \"statusCode\": \"104\",\n" +
	            "    \"statusMessage\": \"Failed\"}";
		ForgetPasswordResponse forgetPasswordRes = ForgetPasswordResponse.constructForgetPasswordResponse(jsonStr);			
		Assert.assertEquals("104", forgetPasswordRes.getStatusCode());
		Assert.assertEquals("Failed", forgetPasswordRes.getStatusMessage());
	}
	
}