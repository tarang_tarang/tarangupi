package psp.mobile.model;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import psp.mobile.model.request.CheckUserRequest;
import psp.mobile.model.response.CheckUserResponse;

public class CheckUserTest {

	@Test
	public void constructJsonStringCheckUserRequest01Test() { 
		CheckUserRequest request = new CheckUserRequest();
		request.setMobileNum("1234567890");
		JSONObject jsonObj = new JSONObject(request.toJsonString());
		Assert.assertEquals("1234567890", jsonObj.get("mobileNum"));
	}
	
	@Test
	public void constructJsonStringCheckUserRequest02Test() { 
		CheckUserRequest request = new CheckUserRequest();
		request.setMobileNum("");
		JSONObject jsonObj = new JSONObject(request.toJsonString());
		Assert.assertEquals("", jsonObj.get("mobileNum"));
	}

	@Test
	public void constructCheckUserResFromJsonStrTest(){	
		String jsonStr = "{\n" +
                "    \"mobileNumErrMsg\": \"Mobile Num is more than 10digits\",\n" +               
                "    \"statusCode\": \"104\",\n" +
                "    \"statusMessage\": \"Failed\"}";
		CheckUserResponse chkUserRes = CheckUserResponse.constructCheckUserResponse(jsonStr);
		Assert.assertEquals("Mobile Num is more than 10digits", chkUserRes.getMobileNumErrMsg());			
		Assert.assertEquals("104", chkUserRes.getStatusCode());
		Assert.assertEquals("Failed", chkUserRes.getStatusMessage());
	}
	
}