package psp.mobile.model;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import psp.mobile.model.request.CollectPaymentRequest;

public class CollectPaymentTest {

	@Test
	public void constructJsonStringCollectPaymentRequest01Test() { 
		CollectPaymentRequest request = new CollectPaymentRequest();
		request.setAmount(25000.0);
		request.setSelfVirtualAddr("Gauti@tarang");
		request.setThirdPartyVirtualAddr("pawan@tarang");
		request.setTxnNote("Your payment was successfull");
		request.setDurationInDays(5);
		JSONObject jsonObj = new JSONObject(request.toJsonString());
		Assert.assertEquals(25000.0, jsonObj.getDouble("amount"), 0.0);
		Assert.assertEquals("Gauti@tarang", jsonObj.get("selfVirtualAddr"));
		Assert.assertEquals("pawan@tarang", jsonObj.get("thirdPartyVirtualAddr"));
		Assert.assertEquals("Your payment was successfull", jsonObj.get("txnNote"));
		Assert.assertEquals(5, jsonObj.getInt("durationInDays"));
	}
	
	@Test
	public void constructJsonStringCollectPaymentRequest02Test() { 
		CollectPaymentRequest request = new CollectPaymentRequest();
		request.setAmount(0D);
		request.setSelfVirtualAddr("");
		request.setThirdPartyVirtualAddr("");
		request.setTxnNote("");
		request.setDurationInDays(0);
		JSONObject jsonObj = new JSONObject(request.toJsonString());
		Assert.assertEquals(0D, jsonObj.getDouble("amount"), 0D);
		Assert.assertEquals("", jsonObj.get("selfVirtualAddr"));
		Assert.assertEquals("", jsonObj.get("thirdPartyVirtualAddr"));
		Assert.assertEquals("", jsonObj.get("txnNote"));
		Assert.assertEquals(0, jsonObj.getInt("durationInDays"));
	}
	
}