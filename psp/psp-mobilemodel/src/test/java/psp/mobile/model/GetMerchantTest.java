package psp.mobile.model;

import java.util.List;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import psp.mobile.model.request.GetMerchantRequest;
import psp.mobile.model.response.GetMerchantListResponse;
import psp.mobile.model.response.GetMerchantResponse;
import psp.mobile.model.response.MerchantDetails;

public class GetMerchantTest {

	@Test
	public void constructJsonStringGetMerchantRequest01Test() { 
		GetMerchantRequest request = new GetMerchantRequest();
		request.setMerchantId(1L);
		JSONObject jsonObj = new JSONObject(request.toJsonString());
		Assert.assertEquals(1L, jsonObj.getLong("merchantId"));
	}
	
	@Test
	public void constructJsonStringGetMerchantRequest02Test() { 
		GetMerchantRequest request = new GetMerchantRequest();
		request.setMerchantId(1L);
		JSONObject jsonObj = new JSONObject(request.toJsonString());
		Assert.assertEquals(1L, jsonObj.getLong("merchantId"));
	}
	
	@Test
	public void constructMerchantDetailsFromJsonObjTest(){
		JSONObject obj = new JSONObject();
		obj.put("id", new Long(1));
		obj.put("name", "DebaK");	
		obj.put("nickName", "Debu");	
		obj.put("virtualAddress", "DebaK@tarang");	
		obj.put("description", "Bonjour");	
		MerchantDetails merchantDtls = MerchantDetails.construcMerchantDetails(obj);
		Assert.assertEquals(Long.parseLong("1"), merchantDtls.getId().longValue());
		Assert.assertEquals("DebaK",merchantDtls.getName());
		Assert.assertEquals("Debu",merchantDtls.getNickName());
		Assert.assertEquals("DebaK@tarang",merchantDtls.getVirtualAddress());
		Assert.assertEquals("Bonjour",merchantDtls.getDescription());	
	}
	
	@Test
	public void constructGetMerchantListResponseFromStrTest(){
		String jsonStr = "{\n" +               
                "    \"statusCode\": \"108\",\n" +
                "    \"statusMessage\": \"Success\",\n" +
                "\"merchantList\": [" + "{" +
				" \"id\": \"1\",\n" +
				"\"name\": \"Deba\",\n" + 
				"\"nickName\": \"Debu\",\n" + 
				"\"virtualAddress\": \"Deba@tarang\",\n" + 
				"\"description\": \"Deba,GudenBerg\",\n" + 
				 "},\n" +
				 "{" +
				 " \"id\": \"2\",\n" +
				 "\"name\": \"Deba1\",\n" + 
					"\"nickName\": \"Debu1\",\n" + 
					"\"virtualAddress\": \"Deba1@tarang\",\n" + 
					"\"description\": \"Deba,Bonjour\",\n" + 
					  "}"
				+ "]"
				 + "}";      
		GetMerchantListResponse merchantRes = GetMerchantListResponse.constructGetMerchantListResponse(jsonStr);
		Assert.assertNotNull(merchantRes);		
		Assert.assertEquals("Success", merchantRes.getStatusMessage());
		Assert.assertEquals("108", merchantRes.getStatusCode());
		List<MerchantDetails> merchantList = merchantRes.getMerchantList();
		MerchantDetails merchant = merchantList.get(0);
		Assert.assertEquals(Long.parseLong("1"), merchant.getId().longValue());				
		Assert.assertEquals("Deba", merchant.getName());
		Assert.assertEquals("Debu", merchant.getNickName());
		Assert.assertEquals("Deba@tarang", merchant.getVirtualAddress());
		Assert.assertEquals("Deba,GudenBerg", merchant.getDescription());
		MerchantDetails merchant1 = merchantList.get(1);
		Assert.assertEquals(Long.parseLong("2"), merchant1.getId().longValue());				
		Assert.assertEquals("Deba1", merchant1.getName());
		Assert.assertEquals("Debu1", merchant1.getNickName());
		Assert.assertEquals("Deba1@tarang", merchant1.getVirtualAddress());
		Assert.assertEquals("Deba,Bonjour", merchant1.getDescription());
	}
	
	@Test
	public void construcGetMerchantResponseFromStrTest(){	
		String jsonStr = "{\n" +
				"	\"merchantDetails\":	{"+
				 "    \"id\": \"11\",\n" +
				 "\"name\": \"Deba11\",\n" + 
					"\"nickName\": \"Debu11\",\n" + 
					"\"virtualAddress\": \"Deba11@tarang\",\n" + 
					"\"description\": \"Deba11,GudenBerg\",\n" +
				"},"+
                "    \"statusCode\": \"100\",\n" +
                "    \"statusMessage\": \"Success\"}";
		GetMerchantResponse merchantRes = GetMerchantResponse.constructGetMerchantResponse(jsonStr);
		Assert.assertEquals("100", merchantRes.getStatusCode());
		Assert.assertEquals("Success", merchantRes.getStatusMessage());
		MerchantDetails merchantDtls = merchantRes.getMerchantDetails();
		Assert.assertEquals("Deba11", merchantDtls.getName());
		Assert.assertEquals("Debu11", merchantDtls.getNickName());
		Assert.assertEquals("Deba11@tarang", merchantDtls.getVirtualAddress());
		Assert.assertEquals("Deba11,GudenBerg", merchantDtls.getDescription());
	}
	
}