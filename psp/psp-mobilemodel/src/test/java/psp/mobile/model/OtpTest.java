package psp.mobile.model;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import psp.mobile.model.request.AcDetails;
import psp.mobile.model.request.OtpRequest;
import psp.mobile.model.util.RequestTestUtility;

public class OtpTest {

	@Test
	public void constructJsonStringOtpRequestTestAccount() {
		OtpRequest request = new OtpRequest();
		String addrType = "ACCOUNT";
		AcDetails acDetails = RequestTestUtility.prepareAcDetails(addrType);
		request.setAcDetails(acDetails);
		JSONObject jsonObj = new JSONObject(request.toJsonString());
		JSONObject jsonObj1 =  new JSONObject(jsonObj.get("acDetails").toString());	
		Assert.assertEquals("111111111111", jsonObj1.get("acnum"));
		Assert.assertEquals("ACCOUNT", jsonObj1.get("addrType"));
		Assert.assertEquals("sbm123", jsonObj1.get("ifsc"));
		Assert.assertEquals("SAVINGS", jsonObj1.get("actype"));
		
		AcDetails acDetailsMobile = RequestTestUtility.prepareAcDetails("MOBILE");
		request.setAcDetails(acDetailsMobile);
		JSONObject jsonObjMobile = new JSONObject(request.toJsonString());
		JSONObject jsonObj1Mobile =  new JSONObject(jsonObjMobile.get("acDetails").toString());	
		Assert.assertEquals("MOBILE", jsonObj1Mobile.get("addrType"));
		Assert.assertEquals("1234567", jsonObj1Mobile.get("mmid"));
		Assert.assertEquals("1234567890", jsonObj1Mobile.get("mobnum"));

		AcDetails acDetailsAadhaar = RequestTestUtility.prepareAcDetails("AADHAAR");
		request.setAcDetails(acDetailsAadhaar );
		JSONObject jsonObjAadhaar  = new JSONObject(request.toJsonString());
		JSONObject jsonObj1Aadhaar  =  new JSONObject(jsonObjAadhaar .get("acDetails").toString());	
		Assert.assertEquals("AADHAAR", jsonObj1Aadhaar.get("addrType"));
		Assert.assertEquals("12345678", jsonObj1Aadhaar.get("iin"));
		Assert.assertEquals("12345678333", jsonObj1Aadhaar.get("uidnum"));
		
		AcDetails acDetailsCard = RequestTestUtility.prepareAcDetails("CARD");
		request.setAcDetails(acDetailsCard);
		JSONObject jsonObjCard  = new JSONObject(request.toJsonString());
		JSONObject jsonObj1Card  =  new JSONObject(jsonObjCard .get("acDetails").toString());	
		Assert.assertEquals("CARD", jsonObj1Card.get("addrType"));
		Assert.assertEquals("111111111111", jsonObj1Card.get("cardNum"));
		Assert.assertEquals("SAVINGS", jsonObj1Card.get("actype"));
	}

	@Test
	public void constructJsonStringOtpRequest02Test(){
		OtpRequest request = new OtpRequest();
		AcDetails acDetails = RequestTestUtility.prepareAcDetailsEmpty();
		request.setAcDetails(acDetails);
		JSONObject json = new JSONObject(request.toJsonString());
		JSONObject jsonObj1 =  new JSONObject(json.get("acDetails").toString());	
		Assert.assertEquals("", jsonObj1.get("acnum"));
		Assert.assertEquals("", jsonObj1.get("addrType"));
		Assert.assertEquals("", jsonObj1.get("ifsc"));
		Assert.assertEquals("", jsonObj1.get("actype"));
		Assert.assertEquals("", jsonObj1.get("iin"));
		Assert.assertEquals("", jsonObj1.get("mmid"));
		Assert.assertEquals("", jsonObj1.get("mobnum"));
		Assert.assertEquals("", jsonObj1.get("cardNum"));
	}
	
}