/**
 * 
 */
package psp.mobile.model;

import org.junit.Assert;
import org.junit.Test;

import psp.common.model.constants.ValidatorConstants;
import psp.common.model.util.MobileValidator;

/**
 * @author gowthamb
 *
 */
public class MobileExpressionsTest {
	
	@Test
	public void alphabetExpCheckTest(){
		Assert.assertTrue(MobileValidator.expCheck("abc", ValidatorConstants.ALPHABET_EXP));
		Assert.assertTrue(MobileValidator.expCheck("ABC", ValidatorConstants.ALPHABET_EXP));
		Assert.assertTrue(MobileValidator.expCheck("", ValidatorConstants.ALPHABET_EXP));
		Assert.assertFalse(MobileValidator.expCheck("abc1", ValidatorConstants.ALPHABET_EXP));
		Assert.assertFalse(MobileValidator.expCheck("211", ValidatorConstants.ALPHABET_EXP));
		Assert.assertFalse(MobileValidator.expCheck(" ", ValidatorConstants.ALPHABET_EXP));
		Assert.assertFalse(MobileValidator.expCheck("abc#", ValidatorConstants.ALPHABET_EXP));
		Assert.assertFalse(MobileValidator.expCheck("abc1#", ValidatorConstants.ALPHABET_EXP));
	}
	
	@Test
	public void alphabetSpaceExpCheckTest(){
		Assert.assertTrue(MobileValidator.expCheck("Gowtham Boj", ValidatorConstants.ALPHABET_SPACE_EXP));
		Assert.assertTrue(MobileValidator.expCheck("ABC XYZ", ValidatorConstants.ALPHABET_SPACE_EXP));
		Assert.assertTrue(MobileValidator.expCheck("  ", ValidatorConstants.ALPHABET_SPACE_EXP));
		Assert.assertFalse(MobileValidator.expCheck("abc1", ValidatorConstants.ALPHABET_SPACE_EXP));
		Assert.assertFalse(MobileValidator.expCheck("211", ValidatorConstants.ALPHABET_SPACE_EXP));
		Assert.assertFalse(MobileValidator.expCheck(" @123", ValidatorConstants.ALPHABET_SPACE_EXP));
		Assert.assertFalse(MobileValidator.expCheck("abc#", ValidatorConstants.ALPHABET_SPACE_EXP));
		Assert.assertFalse(MobileValidator.expCheck("abc1#", ValidatorConstants.ALPHABET_SPACE_EXP));
	}
	
	@Test
	public void alphaNumaricExpCheckTest(){
		Assert.assertTrue(MobileValidator.expCheck("abc", ValidatorConstants.ALPHANUMARIC_EXP));
		Assert.assertTrue(MobileValidator.expCheck("ABC", ValidatorConstants.ALPHANUMARIC_EXP));
		Assert.assertTrue(MobileValidator.expCheck("abc1", ValidatorConstants.ALPHANUMARIC_EXP));
		Assert.assertTrue(MobileValidator.expCheck("211", ValidatorConstants.ALPHANUMARIC_EXP));
		Assert.assertTrue(MobileValidator.expCheck("", ValidatorConstants.ALPHANUMARIC_EXP));
		Assert.assertFalse(MobileValidator.expCheck(" ", ValidatorConstants.ALPHANUMARIC_EXP));
		Assert.assertFalse(MobileValidator.expCheck("abc#", ValidatorConstants.ALPHANUMARIC_EXP));
		Assert.assertFalse(MobileValidator.expCheck("abc1#", ValidatorConstants.ALPHANUMARIC_EXP));
	}
	
	@Test
	public void numaricExpCheckTest(){
		Assert.assertTrue(MobileValidator.expCheck("123", ValidatorConstants.NUMARIC_EXP));
		Assert.assertTrue(MobileValidator.expCheck("344", ValidatorConstants.NUMARIC_EXP));
		Assert.assertTrue(MobileValidator.expCheck("6456", ValidatorConstants.NUMARIC_EXP));
		Assert.assertTrue(MobileValidator.expCheck("456", ValidatorConstants.NUMARIC_EXP));
		Assert.assertFalse(MobileValidator.expCheck("AaBbCc", ValidatorConstants.NUMARIC_EXP));
		Assert.assertFalse(MobileValidator.expCheck(" ", ValidatorConstants.NUMARIC_EXP));
		Assert.assertFalse(MobileValidator.expCheck("abc#", ValidatorConstants.NUMARIC_EXP));
		Assert.assertFalse(MobileValidator.expCheck("DCc1#", ValidatorConstants.NUMARIC_EXP));
	}
	
	@Test
	public void decimalNumberExpCheckTest(){
		Assert.assertTrue(MobileValidator.expCheck("123.45", ValidatorConstants.DECIMAL_NUMBER_EXP));
		Assert.assertTrue(MobileValidator.expCheck("234.1", ValidatorConstants.DECIMAL_NUMBER_EXP));
		Assert.assertTrue(MobileValidator.expCheck("1233.21", ValidatorConstants.DECIMAL_NUMBER_EXP));
		Assert.assertFalse(MobileValidator.expCheck("1234.22.33", ValidatorConstants.DECIMAL_NUMBER_EXP));
		Assert.assertFalse(MobileValidator.expCheck("12345.1234534", ValidatorConstants.DECIMAL_NUMBER_EXP));
		Assert.assertFalse(MobileValidator.expCheck("213@)(*&", ValidatorConstants.DECIMAL_NUMBER_EXP));
		Assert.assertFalse(MobileValidator.expCheck("12121212121212121212121212.123", ValidatorConstants.DECIMAL_NUMBER_EXP));
		Assert.assertFalse(MobileValidator.expCheck("DCc1#", ValidatorConstants.DECIMAL_NUMBER_EXP));
	}
	
	//@Test
	public void passwordExpCheckTest(){
		Assert.assertTrue(MobileValidator.expCheck("Gautam@123", ValidatorConstants.PASSWORD_EXP));
		Assert.assertTrue(MobileValidator.expCheck("Gow@t123", ValidatorConstants.PASSWORD_EXP));
		Assert.assertTrue(MobileValidator.expCheck("gowthaM@t123", ValidatorConstants.PASSWORD_EXP));
		Assert.assertFalse(MobileValidator.expCheck("123", ValidatorConstants.PASSWORD_EXP));
		Assert.assertFalse(MobileValidator.expCheck("gowtham tham", ValidatorConstants.PASSWORD_EXP));
		Assert.assertFalse(MobileValidator.expCheck("@123#", ValidatorConstants.PASSWORD_EXP));
		Assert.assertFalse(MobileValidator.expCheck("asd", ValidatorConstants.PASSWORD_EXP));
		Assert.assertFalse(MobileValidator.expCheck("Asdfff*999999", ValidatorConstants.PASSWORD_EXP));
	}
	
	@Test
	public void emailExpCheckTest(){
		Assert.assertTrue(MobileValidator.expCheck("Gautam@gmail.com", ValidatorConstants.EMAIL_EXP));
		Assert.assertTrue(MobileValidator.expCheck("Gautam@yahoo.com", ValidatorConstants.EMAIL_EXP));
		Assert.assertTrue(MobileValidator.expCheck("Gautamk29@gmail.com", ValidatorConstants.EMAIL_EXP));
		Assert.assertFalse(MobileValidator.expCheck("xyz@gmail", ValidatorConstants.EMAIL_EXP));
		Assert.assertFalse(MobileValidator.expCheck("gowthamtham.com", ValidatorConstants.EMAIL_EXP));
		Assert.assertFalse(MobileValidator.expCheck("123gmail.com", ValidatorConstants.EMAIL_EXP));
		Assert.assertFalse(MobileValidator.expCheck("gowtham@123", ValidatorConstants.EMAIL_EXP));
		Assert.assertFalse(MobileValidator.expCheck("xyz@gmail.c", ValidatorConstants.EMAIL_EXP));
	}

	@Test
	public void dateExpCheckTest(){
		Assert.assertTrue(MobileValidator.expCheck("11-12-2014", ValidatorConstants.DATE_EXP));
		Assert.assertTrue(MobileValidator.expCheck("03-12-2014", ValidatorConstants.DATE_EXP));
		Assert.assertTrue(MobileValidator.expCheck("01-12-2014", ValidatorConstants.DATE_EXP));
		Assert.assertFalse(MobileValidator.expCheck("12/12/2014", ValidatorConstants.DATE_EXP));
		Assert.assertFalse(MobileValidator.expCheck("13-13-2014", ValidatorConstants.DATE_EXP));
		Assert.assertFalse(MobileValidator.expCheck("2014/02/02", ValidatorConstants.DATE_EXP));
		Assert.assertFalse(MobileValidator.expCheck("2014-02-02", ValidatorConstants.DATE_EXP));
		Assert.assertFalse(MobileValidator.expCheck("15-13-2014", ValidatorConstants.DATE_EXP));
	}	
	
	@Test
	public void alphaNumericSpaceSplCharExpCheckTest(){
		Assert.assertTrue(MobileValidator.expCheck("qweZ-", ValidatorConstants.ALPHANUMARIC_SPACE_SPECIALCHAR_EXP));
		Assert.assertTrue(MobileValidator.expCheck("sdfsdf0'_3-z0-", ValidatorConstants.ALPHANUMARIC_SPACE_SPECIALCHAR_EXP));
		Assert.assertTrue(MobileValidator.expCheck("sdfsdf03-z0-9.", ValidatorConstants.ALPHANUMARIC_SPACE_SPECIALCHAR_EXP));
		Assert.assertFalse(MobileValidator.expCheck("12/12/2014", ValidatorConstants.ALPHANUMARIC_SPACE_SPECIALCHAR_EXP));
		Assert.assertFalse(MobileValidator.expCheck("13/13-2014", ValidatorConstants.ALPHANUMARIC_SPACE_SPECIALCHAR_EXP));
		Assert.assertFalse(MobileValidator.expCheck("2014/02/02", ValidatorConstants.ALPHANUMARIC_SPACE_SPECIALCHAR_EXP));
		Assert.assertFalse(MobileValidator.expCheck("2014/02-02", ValidatorConstants.ALPHANUMARIC_SPACE_SPECIALCHAR_EXP));
		Assert.assertFalse(MobileValidator.expCheck("15-13/2014", ValidatorConstants.ALPHANUMARIC_SPACE_SPECIALCHAR_EXP));
	}	
	
}