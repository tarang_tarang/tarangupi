package psp.mobile.model;

import java.util.List;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import psp.common.model.BankSummary;
import psp.mobile.model.response.GetAccountProvidersResponse;

public class GetAccountProvidersTest {

	@Test
	public void constructGetAccountProvidersFromStrTest(){
		String jsonStr = "{\n" +               
                "    \"statusCode\": \"108\",\n" +
                "    \"statusMessage\": \"Success\",\n" +
                "\"listOfBanks\": [" + "{" +
				" \"id\": \"1\",\n" +
				"\"bankName\": \"AXISBank\",\n" + 				
				 "},\n" +
				 "{" +
				 " \"id\": \"2\",\n" +
					"\"bankName\": \"HDFCBank\",\n"
					 + "}"
				+ "]"
				 + "}";      
		GetAccountProvidersResponse actPvd = GetAccountProvidersResponse.constructGetAccountProvidersResponse(jsonStr);
		Assert.assertNotNull(actPvd);		
		Assert.assertEquals("Success", actPvd.getStatusMessage());
		Assert.assertEquals("108", actPvd.getStatusCode());
		List<BankSummary> bankSummaryList = actPvd.getListOfBanks();
		BankSummary bankSummary = bankSummaryList.get(0);
		Assert.assertEquals(Long.parseLong("1"), bankSummary.getId().longValue());				
		Assert.assertEquals("AXISBank", bankSummary.getBankName());
		BankSummary bankSummary1 = bankSummaryList.get(1);
		Assert.assertEquals(Long.parseLong("2"), bankSummary1.getId().longValue());				
		Assert.assertEquals("HDFCBank", bankSummary1.getBankName());
	}
	
	@Test
	public void constructBankSummaryFromJsonObjTest(){		
		JSONObject obj = new JSONObject();
		obj.put("id", new Long(1));
		obj.put("bankName", "HDFC");		
		BankSummary bankSummary = BankSummary.constructBankSummary(obj);
		Assert.assertEquals(Long.parseLong("1"), bankSummary.getId().longValue());
		Assert.assertEquals("HDFC", bankSummary.getBankName());				
	}
	
}