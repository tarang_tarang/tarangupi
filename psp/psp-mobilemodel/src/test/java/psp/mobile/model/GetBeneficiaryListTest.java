package psp.mobile.model;

import java.util.List;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import psp.mobile.model.response.AddBeneficiaryResponse;
import psp.mobile.model.response.BeneficiarySummary;
import psp.mobile.model.response.GetBeneficiaryListResponse;
import psp.mobile.model.response.GetBeneficiaryResponse;

public class GetBeneficiaryListTest {

	@Test
	public void constructBnfSummaryFromJsonStrTest(){		
		String jsonStr = "{\n" +
                "    \"benId\": \"1000\",\n" +
                "    \"benName\": \"Gautam\",\n" +
                "    \"benNickName\": \"Gauti\",\n" +
                "    \"benVirtualAddr\": \"gautam@tarang\"}";
		BeneficiarySummary bnfSummary = BeneficiarySummary.constructBeneficiarySummary(jsonStr);
		Assert.assertEquals(Long.parseLong("1000"), bnfSummary.getBenId().longValue());
		Assert.assertEquals("Gautam", bnfSummary.getBenName());
		Assert.assertEquals("Gauti", bnfSummary.getBenNickName());
		Assert.assertEquals("gautam@tarang", bnfSummary.getBenVirtualAddr());		
	}
	
	@Test
	public void constructBnfSummaryFromJsonObjTest(){		
		JSONObject obj = new JSONObject();
		obj.put("benId", new Long(10));
		obj.put("benName", "Xyz");	
		obj.put("benNickName", "Debu");
		obj.put("benVirtualAddr", "debu@tarang");	
		BeneficiarySummary bnfSummary = BeneficiarySummary.constructBeneficiarySummary(obj);
		Assert.assertEquals(Long.parseLong("10"), bnfSummary.getBenId().longValue());
		Assert.assertEquals("Xyz", bnfSummary.getBenName());
		Assert.assertEquals("Debu", bnfSummary.getBenNickName());
		Assert.assertEquals("debu@tarang", bnfSummary.getBenVirtualAddr());					
	}
	
	@Test
	public void constructGetBeneficiaryListResponseFromStrTest(){
		String jsonStr = "{\n" +               
                "    \"statusCode\": \"118\",\n" +
                "    \"statusMessage\": \"Success\",\n" +
                "\"benefSummaries\": [" + "{" +
				" \"benId\": \"1\",\n" +
				"\"benName\": \"Gautam\",\n" + 
				"\"benNickName\": \"Gauti\",\n" + 	
				"\"benVirtualAddr\": \"gautam@tarang\",\n" + 	
				 "},\n" +
				 "{" +
				 " \"benId\": \"2\",\n" +
					"\"benName\": \"Gautam1\",\n" + 
					"\"benNickName\": \"Gauti123\",\n" + 	
					"\"benVirtualAddr\": \"xyz@tarang\",\n" 	
					 + "}"
				+ "]"
				 + "}";  
		GetBeneficiaryListResponse bnfs = GetBeneficiaryListResponse.constructGetBeneficiaryListResponse(jsonStr);
		Assert.assertNotNull(bnfs);		
		Assert.assertEquals("Success", bnfs.getStatusMessage());
		Assert.assertEquals("118", bnfs.getStatusCode());
		List<BeneficiarySummary> bnfSummaryList = bnfs.getBenefSummaries();
		BeneficiarySummary bnfSummary = bnfSummaryList.get(0);
		Assert.assertEquals(Long.parseLong("1"), bnfSummary.getBenId().longValue());				
		Assert.assertEquals("Gautam", bnfSummary.getBenName());
		Assert.assertEquals("Gauti", bnfSummary.getBenNickName());
		Assert.assertEquals("gautam@tarang", bnfSummary.getBenVirtualAddr());
		BeneficiarySummary bnfSummary1 = bnfSummaryList.get(1);
		Assert.assertEquals(Long.parseLong("2"), bnfSummary1.getBenId().longValue());				
		Assert.assertEquals("Gautam1", bnfSummary1.getBenName());
		Assert.assertEquals("Gauti123", bnfSummary1.getBenNickName());
		Assert.assertEquals("xyz@tarang", bnfSummary1.getBenVirtualAddr());
	}
	
	@Test
	public void constructGetBeneficiaryResponseFromStrTest(){
		String jsonStr = "{\n" +
				 "    \"statusCode\": \"118\",\n" +
	            "    \"statusMessage\": \"Success\",\n" +
                "    \"id\": \"1000\",\n" +
                "    \"address\": \"Bangalore\",\n" +
                "    \"addressType\": \"Mobile\",\n" +
                "    \"description\": \"Transferring 100000\",\n" +
                "    \"referenceNumber\": \"5\",\n" +
                "    \"maskedNumber\": \"****1234\",\n" +
                "    \"ifsc\": \"HDFC1234\",\n" +
                "    \"mmid\": \"1267890\",\n" +
                "    \"name\": \"DebaK\",\n" +
                "    \"aeba\": \"Y\",\n" +
                "    \"nickName\": \"Debu\"}";
		GetBeneficiaryResponse bnfResponse = GetBeneficiaryResponse.constructGetBeneficiaryResponse(jsonStr);
		Assert.assertEquals("118", bnfResponse.getStatusCode());
		Assert.assertEquals("Success", bnfResponse.getStatusMessage());
		Assert.assertEquals(Long.parseLong("1000"), bnfResponse.getId().longValue());
		Assert.assertEquals("Bangalore", bnfResponse.getAddress());
		Assert.assertEquals("Mobile", bnfResponse.getAddressType());
		Assert.assertEquals("Transferring 100000", bnfResponse.getDescription());
		Assert.assertEquals("5", bnfResponse.getReferenceNumber());
		Assert.assertEquals("****1234", bnfResponse.getMaskedNumber());
		Assert.assertEquals("HDFC1234", bnfResponse.getIfsc());
		Assert.assertEquals("1267890", bnfResponse.getMmid());
		Assert.assertEquals("DebaK", bnfResponse.getName());
		Assert.assertEquals("Y", bnfResponse.getAeba());
		Assert.assertEquals("Debu", bnfResponse.getNickName());
	}
	
	@Test
	public void constructAddBnfResFromJsonStrTest(){			
		String jsonStr = "{\n" +
                "    \"nickNameErrMsg\": \"length validation failed\",\n" +
                "    \"beneficiaryNoteErrMsg\": \"No error\",\n" +
                "    \"statusCode\": \"100\",\n" +
                "    \"statusMessage\": \"Failed\"}";
		AddBeneficiaryResponse addBnfResponse = AddBeneficiaryResponse.constructAddBeneficiaryResponse(jsonStr);
		Assert.assertEquals("length validation failed", addBnfResponse.getNickNameErrMsg());
		Assert.assertEquals("No error", addBnfResponse.getBeneficiaryNoteErrMsg());
		Assert.assertEquals("100", addBnfResponse.getStatusCode());;
		Assert.assertEquals("Failed", addBnfResponse.getStatusMessage());;
	}
}
