package psp.mobile.model;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import psp.mobile.model.request.MobileDevice;
import psp.mobile.model.request.MobileRequest;
import psp.mobile.model.util.RequestTestUtility;

public class MobileTest {

	@Test
	public void constructJsonStringMobileRequest01Test(){
		MobileRequest request = new MobileRequest();
		request.setUserName("Gauti1234");
		MobileDevice device = RequestTestUtility.prepareDevice();
		request.setDevice(device);
        Assert.assertEquals("Gauti1234", request.getJsonString().get("userName"));
        JSONObject jsonObj =  new JSONObject(request.getJsonString().get("device").toString());	
        Assert.assertEquals("CC 1.0", jsonObj.get("app"));
        Assert.assertEquals("011001", jsonObj.get("capability"));
        Assert.assertEquals("560017", jsonObj.get("geoCode"));
        Assert.assertEquals("123.456.123.123", jsonObj.get("ip"));
        Assert.assertEquals("Sarjapur Road, Bangalore, KA, IN", jsonObj.get("location"));
        Assert.assertEquals("Android 4.4", jsonObj.get("os"));
        Assert.assertEquals("123456789", jsonObj.get("terminalId"));
        Assert.assertEquals("Mobile", jsonObj.get("type"));
	}
	
	@Test
	public void constructJsonStringMobileRequest02Test(){
		MobileRequest request = new MobileRequest();
		request.setUserName("");
		MobileDevice device = RequestTestUtility.prepareDeviceEmpty();
		request.setDevice(device);
        Assert.assertEquals("", request.getJsonString().get("userName"));
        JSONObject jsonObj =  new JSONObject(request.getJsonString().get("device").toString());	
        Assert.assertEquals("", jsonObj.get("app"));
        Assert.assertEquals("", jsonObj.get("capability"));
        Assert.assertEquals("", jsonObj.get("geoCode"));
        Assert.assertEquals("", jsonObj.get("ip"));
        Assert.assertEquals("", jsonObj.get("location"));
        Assert.assertEquals("", jsonObj.get("os"));
        Assert.assertEquals("", jsonObj.get("terminalId"));
        Assert.assertEquals("", jsonObj.get("type"));
	}
	
}