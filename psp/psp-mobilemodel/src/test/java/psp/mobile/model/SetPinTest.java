package psp.mobile.model;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import psp.mobile.model.request.MobileCredentials;
import psp.mobile.model.request.SetPinRequest;
import psp.mobile.model.util.RequestTestUtility;

public class SetPinTest {

	@Test
	public void constructJsonStringSetPinRequest01Test() {
		SetPinRequest request = new SetPinRequest();
		request.setIsChangePin(true);
		MobileCredentials credentials = RequestTestUtility.prepareCredentials();
		request.setMobileCredentials(credentials);	
		JSONObject jsonObj = new JSONObject(request.toJsonString());
		Assert.assertEquals(true, jsonObj.getBoolean("isChangePin"));
		JSONObject jsonObj1 =  new JSONObject(jsonObj.get("mobileCredentials").toString());	
		Assert.assertEquals("123", jsonObj1.get("dataCode"));
		Assert.assertEquals("222", jsonObj1.get("dataki"));
		Assert.assertEquals("MIIBIjANBgkqhkiG9w0BB", jsonObj1.get("dataValue"));
		Assert.assertEquals("MPIN", jsonObj1.get("subType"));
		Assert.assertEquals("PIN", jsonObj1.get("type"));
	}
	
	@Test
	public void constructJsonStringSetPinRequest02Test() {
		SetPinRequest request = new SetPinRequest();
		request.setIsChangePin(false);
		MobileCredentials credentials = RequestTestUtility.prepareCredentialsEmpty();
		request.setMobileCredentials(credentials);	
		JSONObject jsonObj = new JSONObject(request.toJsonString());
		Assert.assertEquals(false, jsonObj.getBoolean("isChangePin"));
		JSONObject jsonObj1 =  new JSONObject(jsonObj.get("mobileCredentials").toString());	
		Assert.assertEquals("", jsonObj1.get("dataCode"));
		Assert.assertEquals("", jsonObj1.get("dataki"));
		Assert.assertEquals("", jsonObj1.get("dataValue"));
		Assert.assertEquals("", jsonObj1.get("subType"));
		Assert.assertEquals("", jsonObj1.get("type"));
	}
	
}