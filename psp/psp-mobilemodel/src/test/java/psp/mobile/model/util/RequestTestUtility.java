package psp.mobile.model.util;

import psp.mobile.model.request.AcDetails;
import psp.mobile.model.request.MobileCredentials;
import psp.mobile.model.request.MobileDevice;

public class RequestTestUtility {

	public static MobileDevice prepareDevice() {
		MobileDevice mobiledevice = new MobileDevice();
		mobiledevice.setApp("CC 1.0");
		mobiledevice.setCapability("011001");
		mobiledevice.setGeoCode("560017");
		mobiledevice.setIp("123.456.123.123");
		mobiledevice.setLocation("Sarjapur Road, Bangalore, KA, IN");
		mobiledevice.setMobile("9741500833");
		mobiledevice.setOs("Android 4.4");
		mobiledevice.setTerminalId("123456789");
		mobiledevice.setType("Mobile");
		return mobiledevice;
	}
	
	public static MobileDevice prepareDeviceEmpty() {
		MobileDevice mobiledevice = new MobileDevice();
		mobiledevice.setApp("");
		mobiledevice.setCapability("");
		mobiledevice.setGeoCode("");
		mobiledevice.setIp("");
		mobiledevice.setLocation("");
		mobiledevice.setMobile("");
		mobiledevice.setOs("");
		mobiledevice.setTerminalId("");
		mobiledevice.setType("");
		return mobiledevice;
	}
	
	public static MobileCredentials prepareCredentials() {
		MobileCredentials credentials = new MobileCredentials();
		credentials.setDataCode("123");
		credentials.setDataki("222");
		credentials.setDataValue("MIIBIjANBgkqhkiG9w0BB");
		credentials.setSubType("MPIN");
		credentials.setType("PIN");
		return credentials;
	}
	
	public static MobileCredentials prepareCredentialsEmpty() {
		MobileCredentials credentials = new MobileCredentials();
		credentials.setDataCode("");
		credentials.setDataki("");
		credentials.setDataValue("");
		credentials.setSubType("");
		credentials.setType("");
		return credentials;
	}
	
	public static AcDetails prepareAcDetails(String addrType) {
		AcDetails acDetails = new AcDetails();
		acDetails.setAddrType(addrType);
		if(addrType == "ACCOUNT") {
			acDetails.setAcnum("111111111111");
			acDetails.setActype("SAVINGS");
			acDetails.setIfsc("sbm123");
		}
		if(addrType == "CARD") {
			acDetails.setCardNum("111111111111");
			acDetails.setActype("SAVINGS");
		}
		if(addrType == "MOBILE") {
			acDetails.setMmid("1234567");
			acDetails.setMobnum("1234567890");
		}
		if(addrType == "AADHAAR") {
			acDetails.setIin("12345678");
			acDetails.setUidnum("12345678333");
		}
		return acDetails;
	}
	
	public static AcDetails prepareAcDetailsEmpty() {
		AcDetails acDetails = new AcDetails();
		acDetails.setAcnum("");
		acDetails.setActype("");
		acDetails.setAddrType("");
		acDetails.setCardNum("");
		acDetails.setIfsc("");
		acDetails.setIin("");
		acDetails.setMmid("");
		acDetails.setMobnum("");
		acDetails.setUidnum("");
		return acDetails;
	}
	
}