/**
 * 
 */
package psp.mobile.model;


import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import psp.mobile.model.request.GeneratePinRequest;
import psp.mobile.model.request.MobileCredentials;
import psp.mobile.model.response.GeneratePinResponse;
import psp.mobile.model.util.RequestTestUtility;


/**
 * @author prasadj
 *
 */
public class GeneratePinTest {
	
	@Test
	public void constructJsonStringGeneratePinRequest01Test(){
		GeneratePinRequest request = new GeneratePinRequest();
		request.setUserName("Gauti1234");
		request.setCardDigits("12345678912345");
		request.setExpiryDate("09/14");		
		MobileCredentials credentials = RequestTestUtility.prepareCredentials();
		request.setMobileCredentials(credentials);
		JSONObject jsonObj = new JSONObject(request.toJsonString());
		JSONObject jsonObj1 =  new JSONObject(jsonObj.get("mobileCredentials").toString());	
		Assert.assertEquals("Gauti1234", jsonObj.get("userName"));
		Assert.assertEquals("12345678912345", jsonObj.get("cardDigits"));
		Assert.assertEquals("09/14", jsonObj.get("expiryDate"));
		Assert.assertEquals("123", jsonObj1.get("dataCode"));
		Assert.assertEquals("222", jsonObj1.get("dataki"));
		Assert.assertEquals("MIIBIjANBgkqhkiG9w0BB", jsonObj1.get("dataValue"));
		Assert.assertEquals("MPIN", jsonObj1.get("subType"));
		Assert.assertEquals("PIN", jsonObj1.get("type"));
	}
	
	@Test
	public void constructGeneratePinResFromJsonStrTest(){	
		String jsonStr = "{\n" +    
	            "    \"statusCode\": \"104\",\n" +
	            "    \"statusMessage\": \"Failed\"}";
		GeneratePinResponse generatePin = GeneratePinResponse.constructGeneratePinResponse(jsonStr);			
		Assert.assertEquals("104", generatePin.getStatusCode());
		Assert.assertEquals("Failed", generatePin.getStatusMessage());
	}

	@Test
	public void constructJsonStringGeneratePinRequest02Test(){
		GeneratePinRequest request = new GeneratePinRequest();
		request.setUserName("");
		request.setCardDigits("");
		request.setExpiryDate("");		
		MobileCredentials credentials = RequestTestUtility.prepareCredentialsEmpty();
		request.setMobileCredentials(credentials);
		JSONObject jsonObj = new JSONObject(request.toJsonString());
		JSONObject jsonObj1 =  new JSONObject(jsonObj.get("mobileCredentials").toString());	
		Assert.assertEquals("", jsonObj.get("userName"));
		Assert.assertEquals("", jsonObj.get("cardDigits"));
		Assert.assertEquals("", jsonObj.get("expiryDate"));
		Assert.assertEquals("", jsonObj1.get("dataCode"));
		Assert.assertEquals("", jsonObj1.get("dataki"));
		Assert.assertEquals("", jsonObj1.get("dataValue"));
		Assert.assertEquals("", jsonObj1.get("subType"));
		Assert.assertEquals("", jsonObj1.get("type"));
	}

}