package psp.mobile.model;

import org.junit.Assert;
import org.junit.Test;

import psp.common.model.AccountSummary;
import psp.mobile.model.response.GetExistingAccountsResponse;

public class GetExistingAccountsTest {

	@Test
	public void construcExistingAccountsResponseFromJsonObjTest(){	
		String jsonStr = "{\n" +
				"	\"accountSummary\":	{"+
				 "    \"id\": \"11\",\n" +
				 "    \"maskedAccnumber\": \"****1579\",\n" +
				 "    \"ifsc\": \"HDFC12345\",\n" +
				 "    \"name\": \"Deba\",\n" +
				 "    \"aeba\": \"Y\",\n" +
				 "    \"virtualAddress\": \"debaa@tarangtech\",\n" +
				 "    \"isPinSet\": \"TRUE\",\n" +
				"},"+
                "    \"statusCode\": \"100\",\n" +
                "    \"statusMessage\": \"Success\"}";
		GetExistingAccountsResponse getExtAct = GetExistingAccountsResponse.constructGetExistingAccountsResponse(jsonStr);
		Assert.assertEquals("100", getExtAct.getStatusCode());
		Assert.assertEquals("Success", getExtAct.getStatusMessage());
		AccountSummary actSummary = getExtAct.getAccountSummary();
		Assert.assertNotNull(actSummary);
		Assert.assertEquals(Long.parseLong("11"), actSummary.getId().longValue());
		Assert.assertEquals("****1579", actSummary.getMaskedAccnumber());
		Assert.assertEquals("HDFC12345", actSummary.getIfsc());
		Assert.assertEquals("Deba", actSummary.getName());
		Assert.assertEquals("Y", actSummary.getAeba());
		Assert.assertEquals("debaa@tarangtech", actSummary.getVirtualAddress());
		Assert.assertEquals(Boolean.parseBoolean("TRUE"), actSummary.getIsPinSet());
	}
	
}