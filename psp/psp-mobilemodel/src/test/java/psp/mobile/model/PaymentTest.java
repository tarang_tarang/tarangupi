package psp.mobile.model;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import psp.mobile.model.request.MobileCredentials;
import psp.mobile.model.request.PaymentRequest;
import psp.mobile.model.util.RequestTestUtility;

public class PaymentTest {

	@Test
	public void constructJsonStringPaymentRequest01Test() { 
		PaymentRequest request = new PaymentRequest();
		request.setAmount(2000D);
		request.setNickName("Gauti");
		request.setSelfVirtualAddr("Gauti@tarang");
		request.setThirdPartyVirtualAddr("pawan@tarang");
		request.setType("self");
		MobileCredentials credentials = RequestTestUtility.prepareCredentials();
		request.setMobileCredentials(credentials);			
		JSONObject jsonObj = new JSONObject(request.toJsonString());
		JSONObject jsonObj1 =  new JSONObject(jsonObj.get("mobileCredentials").toString());	
		Assert.assertEquals(2000D, jsonObj.getDouble("amount"), 0.0);
		Assert.assertEquals("Gauti", jsonObj.get("nickName"));
		Assert.assertEquals("Gauti@tarang", jsonObj.get("selfVirtualAddr"));
		Assert.assertEquals("pawan@tarang", jsonObj.get("thirdPartyVirtualAddr"));
		Assert.assertEquals("self", jsonObj.get("type"));	
		Assert.assertEquals("123", jsonObj1.get("dataCode"));
		Assert.assertEquals("222", jsonObj1.get("dataki"));
		Assert.assertEquals("MIIBIjANBgkqhkiG9w0BB", jsonObj1.get("dataValue"));
		Assert.assertEquals("MPIN", jsonObj1.get("subType"));
		Assert.assertEquals("PIN", jsonObj1.get("type"));
	}
	
	@Test
	public void constructJsonStringPaymentRequest02Test() { 
		PaymentRequest request = new PaymentRequest();
		request.setAmount(0D);
		request.setNickName("");
		request.setSelfVirtualAddr("");
		request.setThirdPartyVirtualAddr("");
		request.setType("");
		MobileCredentials credentials = RequestTestUtility.prepareCredentialsEmpty();
		request.setMobileCredentials(credentials);			
		JSONObject jsonObj = new JSONObject(request.toJsonString());
		JSONObject jsonObj1 =  new JSONObject(jsonObj.get("mobileCredentials").toString());	
		Assert.assertEquals(0.0, jsonObj.getDouble("amount"), 0.0);
		Assert.assertEquals("", jsonObj.get("nickName"));
		Assert.assertEquals("", jsonObj.get("selfVirtualAddr"));
		Assert.assertEquals("", jsonObj.get("thirdPartyVirtualAddr"));
		Assert.assertEquals("", jsonObj.get("type"));	
		Assert.assertEquals("", jsonObj1.get("dataCode"));
		Assert.assertEquals("", jsonObj1.get("dataki"));
		Assert.assertEquals("", jsonObj1.get("dataValue"));
		Assert.assertEquals("", jsonObj1.get("subType"));
		Assert.assertEquals("", jsonObj1.get("type"));
	}
	
}