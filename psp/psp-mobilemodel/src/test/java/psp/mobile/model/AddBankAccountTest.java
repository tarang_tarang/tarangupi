package psp.mobile.model;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import psp.mobile.model.request.AddBankAccountRequest;
import psp.mobile.model.response.AddBankAccountResponse;

public class AddBankAccountTest {

	@Test
	public void constructJsonStringAddBankAccountRequest01Test() { 
		AddBankAccountRequest request = new AddBankAccountRequest();
		request.setId(1L);
		JSONObject jsonObj = new JSONObject(request.toJsonString());
		Assert.assertEquals(1L, jsonObj.getLong("id"));
	}
	
	@Test
	public void constructJsonStringAddBankAccountRequest02Test() { 
		AddBankAccountRequest request = new AddBankAccountRequest();
		request.setId(0L);
		JSONObject jsonObj = new JSONObject(request.toJsonString());
		Assert.assertEquals(0L, jsonObj.getLong("id"));
	}

	@Test
	public void constructAddBankActFromJsonStrTest(){		
		
		String jsonStr = "{\n" +
                "    \"virtualAddress\": \"deba@tarang\",\n" +
                "    \"statusCode\": \"100\",\n" +
                "    \"statusMessage\": \"Success\"}";
		AddBankAccountResponse addBankAccountResponse = AddBankAccountResponse.constructAddBankAccountResponse(jsonStr);
		Assert.assertEquals("deba@tarang", addBankAccountResponse.getVirtualAddress());
		Assert.assertEquals("100", addBankAccountResponse.getStatusCode());
		Assert.assertEquals("Success", addBankAccountResponse.getStatusMessage());
	}
	
}