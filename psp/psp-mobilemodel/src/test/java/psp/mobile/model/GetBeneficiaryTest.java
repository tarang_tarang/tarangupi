package psp.mobile.model;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import psp.mobile.model.request.GetBeneficiaryRequest;

public class GetBeneficiaryTest {

	@Test
	public void constructJsonStringGetBeneficiaryRequest01Test() { 
		GetBeneficiaryRequest request = new GetBeneficiaryRequest();
		request.setNickName("Gauti");
		JSONObject jsonObj = new JSONObject(request.toJsonString());
		Assert.assertEquals("Gauti", jsonObj.get("nickName"));
	}
	
	@Test
	public void constructJsonStringGetBeneficiaryRequest02Test() { 
		GetBeneficiaryRequest request = new GetBeneficiaryRequest();
		request.setNickName("");
		JSONObject jsonObj = new JSONObject(request.toJsonString());
		Assert.assertEquals("", jsonObj.get("nickName"));
	}
	
}