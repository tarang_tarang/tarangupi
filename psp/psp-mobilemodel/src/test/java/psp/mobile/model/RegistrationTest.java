package psp.mobile.model;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import psp.mobile.model.request.RegistrationRequest;

public class RegistrationTest {

	@Test
	public void constructJsonStringRegistrationRequest01Test() { 
		RegistrationRequest request = new RegistrationRequest();
		request.setUserName("Gauti1234");
		request.setEmail("xyz@gmail.com");
		request.setFirstName("Gauti");
		request.setLastName("Bojan");
		request.setMiddleName("gambhir");
		request.setPassword("Gauti@1234");
		request.setMobileNumber("1234567890");
		JSONObject jsonObj = new JSONObject(request.toJsonString());
		Assert.assertEquals("Gauti1234", jsonObj.get("userName"));
		Assert.assertEquals("xyz@gmail.com", jsonObj.get("email"));
		Assert.assertEquals("Gauti", jsonObj.get("firstName"));
		Assert.assertEquals("Bojan", jsonObj.get("lastName"));
		Assert.assertEquals("gambhir", jsonObj.get("middleName"));
		Assert.assertEquals("Gauti@1234", jsonObj.get("password"));
		Assert.assertEquals("1234567890", jsonObj.get("mobileNumber"));
	}
	
	@Test
	public void constructJsonStringRegistrationRequest02Test() { 
		RegistrationRequest request = new RegistrationRequest();
		request.setUserName("");
		request.setEmail("");
		request.setFirstName("");
		request.setLastName("");
		request.setMiddleName("");
		request.setPassword("");
		request.setMobileNumber("");
		JSONObject jsonObj = new JSONObject(request.toJsonString());
		Assert.assertEquals("", jsonObj.get("userName"));
		Assert.assertEquals("", jsonObj.get("email"));
		Assert.assertEquals("", jsonObj.get("firstName"));
		Assert.assertEquals("", jsonObj.get("lastName"));
		Assert.assertEquals("", jsonObj.get("middleName"));
		Assert.assertEquals("", jsonObj.get("password"));
		Assert.assertEquals("", jsonObj.get("mobileNumber"));
	}
	
}