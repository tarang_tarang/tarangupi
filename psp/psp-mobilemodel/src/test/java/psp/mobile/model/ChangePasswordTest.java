package psp.mobile.model;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import psp.mobile.model.request.ChangePasswordRequest;
import psp.mobile.model.response.ChangePasswordResponse;

public class ChangePasswordTest {

	@Test
	public void constructJsonStringChangePasswordRequest01Test() { 
		ChangePasswordRequest request = new ChangePasswordRequest();
		request.setUserName("Gauti1234");
		request.setOldPassword("Gauti@1234");
		request.setNewPassword("Gauti@4321");
		JSONObject jsonObj = new JSONObject(request.toJsonString());
		Assert.assertEquals("Gauti1234", jsonObj.get("userName"));
		Assert.assertEquals("Gauti@1234", jsonObj.get("oldPassword"));
		Assert.assertEquals("Gauti@4321", jsonObj.get("newPassword"));
	}
	
	@Test
	public void constructJsonStringChangePasswordRequest02Test() { 
		ChangePasswordRequest request = new ChangePasswordRequest();
		request.setUserName("");
		request.setOldPassword("");
		request.setNewPassword("");
		JSONObject jsonObj = new JSONObject(request.toJsonString());
		Assert.assertEquals("", jsonObj.get("userName"));
		Assert.assertEquals("", jsonObj.get("oldPassword"));
		Assert.assertEquals("", jsonObj.get("newPassword"));
	}
	
	@Test
	public void constructChangePasswdFromJsonStrTest(){	
		String jsonStr = "{\n" +
                "    \"userNameErrMsg\": \"User Name is incorrect\",\n" +
                "    \"oldPasswordErrMsg\": \"Old password is incorrect\",\n" +
                "    \"newPasswordErrMsg\": \"No Error in new password\",\n" +
                "    \"statusCode\": \"100\",\n" +
                "    \"statusMessage\": \"Failed\"}";
		ChangePasswordResponse chngPasswd = ChangePasswordResponse.constructChangePasswordResponse(jsonStr);
		Assert.assertEquals("User Name is incorrect", chngPasswd.getUserNameErrMsg());
		Assert.assertEquals("Old password is incorrect", chngPasswd.getOldPasswordErrMsg());
		Assert.assertEquals("No Error in new password", chngPasswd.getNewPasswordErrMsg());		
		Assert.assertEquals("100", chngPasswd.getStatusCode());
		Assert.assertEquals("Failed", chngPasswd.getStatusMessage());
	}
	
}