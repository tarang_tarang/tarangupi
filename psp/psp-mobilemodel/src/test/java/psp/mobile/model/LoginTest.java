package psp.mobile.model;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import psp.mobile.model.request.LoginRequest;

public class LoginTest {

	@Test
	public void constructJsonStringLoginRequest01Test() { 
		LoginRequest request = new LoginRequest();
		request.setPassword("Gauti@1234");
		JSONObject jsonObj = new JSONObject(request.toJsonString());
		Assert.assertEquals("Gauti@1234", jsonObj.get("password"));
	}
	
	@Test
	public void constructJsonStringLoginRequest02Test() { 
		LoginRequest request = new LoginRequest();
		request.setPassword("");
		JSONObject jsonObj = new JSONObject(request.toJsonString());
		Assert.assertEquals("", jsonObj.get("password"));
	}
	
}