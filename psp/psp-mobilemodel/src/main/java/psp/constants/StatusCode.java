/**
 * 
 */
package psp.constants;

import java.util.HashMap;
import java.util.Map;


public enum StatusCode {

	// 501 - Mobile Messages
	
	SUCCESS("200", "Process success"), 	
	INTERNAL_SERVER_ERROR_MSG("50018", "Internal Server Error"),	
	FIELD_VALIDATION_FAILED("50101", "Some of the field validation is failed. Please check for appropriate error message"),
	REGISTRATION_SERVICE_FAIL("50102", "Registration Service fail"),
	DUPLICATE_AADHAAR_NUMBER("50103", "Aadhaar number already exists"),
	DUPLICATE_MOBILE_NUMBER("50104", "Mobile number already exists"),
	DUPLICATE_USER_NAME("50105", "User name already exists"),
	USERNAME_OR_PASSWORD_INCORRECT("50106", "Incorrect username or password"),
	GET_ACC_PVD_SERVICE_FAIL("50107", "Get Account Providers service fail"),
	SET_CREDENTIAL_FAILED("50108", "Set pin failed"),
	OTP_REQUEST_FAILED("50109", "Otp request failed"),
	USER_NOT_REGISTER("50110","User is not register"),
	DEVICE_VALIDATION_FAILED("50111", "Device Vidation Failed"),
	GET_ACCOUNT_SERVICE_FAIL("50112", "Get Accounts service failed"),
	ADD_BANK_ACCOUNT_SERVICE_FAIL("50113", "Add bank account service failed"),
	USER_NOT_LOGGED_IN("50114", "User is not logged in to do the service"),
	ACCOUNT_DETAILS_NOT_FOUND("50115", "User Account details not found"),
	USER_IS_NOT_REGISTERED("50116", "User is not registered"),
	INVALID_OLD_PASSWORD("50117", "Invalid old password"),
	OTP_SERVICE_SUCCESS("50118", "Otp request initiated succesfully"),
	GET_ACC_SERVICE_SUCCESS("50119", "Get Accounts Service Success"),
	SET_CRED_SERVICE_SUCCESS("50120", "Set Credential Service Success"),
	GET_TOKEN_SERVICE_FAIL("50121", "Get Token Service Failed at UPI"),
	GET_TOKEN_SERVICE_SUCCESS("50122", "Get Token Service Success"),
	GET_KEY_SERVICE_FAIL("50123", "Get key service failed at UPI"),
	BAL_ENQ_SERVICE_SUCCESS("50124", "Balance enquiry service success"),
	BAL_ENQ_SERVICE_FAIL("50125", "Balance enquiry service failed at UPI"),
	PAYMENT_INITIATION_SUCCESS("50126", "Payment initiated successfully"),
	PAYMENT_INITIATION_FAIL("50127", "Payment initiation failed at UPI"),
	GET_BENF_SERVICE_FAIL("50128", "Get Beneficiary service failed"),
	ACC_DETAILS_NOT_FOUND("50129", "Account details not found for this virtual address"),
	GEN_PIN_REQUEST_FAILED("50130", "Generate pin service failed at UPI"),
	GEN_PIN_SERVICE_SUCCESS("50131", "Generate pin service success"),
	GET_TXN_DETAILS_SERVICE_FAIL("50132", "Get Transaction details service failed"),
	INVALID_PAYEE_ADDRESS("50133", "Invalid payee Address"),
	COLLECT_INITIATION_SUCCESS("50134", "Collect payment initiated successfully"),
	COLLECT_INITIATION_FAIL("50135", "Collect payment initiation failed at UPI"),
	INVALID_NOTIFICATION_ID("50136", "Invalid notification id"),
	COLLECT_PAY_REJECTED_SUCCESS("50137", "Collect Pay rejected successfully"),
	
	
	ADDR_RESL_FAILED("U29", "Address resolution is failed"),
	SIGNATURE_ERROR("PSP03", "Signature error.");
	
	private final String code;
	
	private final String message;
	
	private static final Map<String, StatusCode> STATUS_CODES = new HashMap<String, StatusCode>();
	
	static {
			STATUS_CODES.put(SUCCESS.code, SUCCESS);
			STATUS_CODES.put(INTERNAL_SERVER_ERROR_MSG.code, INTERNAL_SERVER_ERROR_MSG);
			STATUS_CODES.put(REGISTRATION_SERVICE_FAIL.code, REGISTRATION_SERVICE_FAIL);
			STATUS_CODES.put(DUPLICATE_AADHAAR_NUMBER.code, DUPLICATE_AADHAAR_NUMBER);
			STATUS_CODES.put(DUPLICATE_MOBILE_NUMBER.code, DUPLICATE_MOBILE_NUMBER);
			STATUS_CODES.put(DUPLICATE_USER_NAME.code, DUPLICATE_USER_NAME);
			STATUS_CODES.put(USERNAME_OR_PASSWORD_INCORRECT.code, USERNAME_OR_PASSWORD_INCORRECT);
			STATUS_CODES.put(GET_ACC_PVD_SERVICE_FAIL.code, GET_ACC_PVD_SERVICE_FAIL);
			STATUS_CODES.put(SET_CREDENTIAL_FAILED.code, SET_CREDENTIAL_FAILED);
			STATUS_CODES.put(OTP_REQUEST_FAILED.code, OTP_REQUEST_FAILED);
			STATUS_CODES.put(USER_NOT_REGISTER.code, USER_NOT_REGISTER);
			STATUS_CODES.put(DEVICE_VALIDATION_FAILED.code, DEVICE_VALIDATION_FAILED);
			STATUS_CODES.put(GET_ACCOUNT_SERVICE_FAIL.code, GET_ACCOUNT_SERVICE_FAIL);
			STATUS_CODES.put(ADD_BANK_ACCOUNT_SERVICE_FAIL.code, ADD_BANK_ACCOUNT_SERVICE_FAIL);
			STATUS_CODES.put(FIELD_VALIDATION_FAILED.code, FIELD_VALIDATION_FAILED);
			STATUS_CODES.put(USER_NOT_LOGGED_IN.code, USER_NOT_LOGGED_IN);
			STATUS_CODES.put(ACCOUNT_DETAILS_NOT_FOUND.code, ACCOUNT_DETAILS_NOT_FOUND);
			STATUS_CODES.put(USER_IS_NOT_REGISTERED.code, USER_IS_NOT_REGISTERED);
			STATUS_CODES.put(INVALID_OLD_PASSWORD.code, INVALID_OLD_PASSWORD);
			STATUS_CODES.put(OTP_SERVICE_SUCCESS.code, OTP_SERVICE_SUCCESS);
			STATUS_CODES.put(GET_ACC_SERVICE_SUCCESS.code, GET_ACC_SERVICE_SUCCESS);
			STATUS_CODES.put(SET_CRED_SERVICE_SUCCESS.code, SET_CRED_SERVICE_SUCCESS);
			STATUS_CODES.put(GET_TOKEN_SERVICE_FAIL.code, GET_TOKEN_SERVICE_FAIL);
			STATUS_CODES.put(GET_TOKEN_SERVICE_SUCCESS.code, GET_TOKEN_SERVICE_SUCCESS);
			STATUS_CODES.put(GET_KEY_SERVICE_FAIL.code, GET_KEY_SERVICE_FAIL);
			STATUS_CODES.put(BAL_ENQ_SERVICE_FAIL.code, BAL_ENQ_SERVICE_FAIL);
			STATUS_CODES.put(PAYMENT_INITIATION_SUCCESS.code, PAYMENT_INITIATION_SUCCESS);
			STATUS_CODES.put(PAYMENT_INITIATION_FAIL.code, PAYMENT_INITIATION_FAIL);
			STATUS_CODES.put(GET_BENF_SERVICE_FAIL.code, GET_BENF_SERVICE_FAIL);
			STATUS_CODES.put(ACC_DETAILS_NOT_FOUND.code, ACC_DETAILS_NOT_FOUND);
			STATUS_CODES.put(GEN_PIN_REQUEST_FAILED.code, GEN_PIN_REQUEST_FAILED);
			STATUS_CODES.put(GEN_PIN_SERVICE_SUCCESS.code, GEN_PIN_SERVICE_SUCCESS);
			STATUS_CODES.put(GET_TXN_DETAILS_SERVICE_FAIL.code, GET_TXN_DETAILS_SERVICE_FAIL);
			STATUS_CODES.put(INVALID_PAYEE_ADDRESS.code, INVALID_PAYEE_ADDRESS);
			STATUS_CODES.put(COLLECT_INITIATION_FAIL.code, COLLECT_INITIATION_FAIL);
			STATUS_CODES.put(COLLECT_INITIATION_SUCCESS.code, COLLECT_INITIATION_SUCCESS);
			STATUS_CODES.put(INVALID_NOTIFICATION_ID.code, INVALID_NOTIFICATION_ID);
			STATUS_CODES.put(COLLECT_PAY_REJECTED_SUCCESS.code, COLLECT_PAY_REJECTED_SUCCESS);
			
			STATUS_CODES.put(ADDR_RESL_FAILED.code, ADDR_RESL_FAILED);
			STATUS_CODES.put(SIGNATURE_ERROR.code, SIGNATURE_ERROR);
		}
	
	private StatusCode(String code, String message){
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
	
	public static StatusCode getStatusCodeByCode(String code){
		return STATUS_CODES.get(code);
	}

}