package psp.common.model;

import java.io.Serializable;

import org.json.JSONObject;

public class AccountSummary implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private String maskedAccnumber; 

	private String ifsc;

	private String name;

	private String aeba;
	
	private String virtualAddress;
	
	private Boolean isPinSet;
	
	private String mmid;
	
	private String credentailType;
	
	private String credentailSubType;
	
	private String credentailDataType;
	
	private Integer credentailDataLength;
	
	public AccountSummary(){
	}
	
	public AccountSummary(String maskedAccnumber, String ifsc, String name, String aeba, String virtualAddress, 
			Boolean isPinSet, String mmid, String credentailType, String credentailSubType, String credentailDataType, Integer credentailDataLength){
		this.maskedAccnumber = maskedAccnumber;
		this.ifsc = ifsc;
		this.name = name;
		this.aeba = aeba;
		this.virtualAddress = virtualAddress;
		this.isPinSet = isPinSet;
		this.mmid = mmid;
		this.credentailType = credentailType;
		this.credentailSubType = credentailSubType;
		this.credentailDataType = credentailDataType;
		this.credentailDataLength = credentailDataLength;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMaskedAccnumber() {
		return maskedAccnumber;
	}

	public void setMaskedAccnumber(String maskedAccnumber) {
		this.maskedAccnumber = maskedAccnumber;
	}

	public String getIfsc() {
		return ifsc;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAeba() {
		return aeba;
	}

	public void setAeba(String aeba) {
		this.aeba = aeba;
	}

	public String getVirtualAddress() {
		return virtualAddress;
	}

	public void setVirtualAddress(String virtualAddress) {
		this.virtualAddress = virtualAddress;
	}

	public Boolean getIsPinSet() {
		return isPinSet;
	}

	public void setIsPinSet(Boolean isPinSet) {
		this.isPinSet = isPinSet;
	}

	public String getMmid() {
		return mmid;
	}

	public void setMmid(String mmid) {
		this.mmid = mmid;
	}
	
	public String getCredentailType() {
		return credentailType;
	}

	public void setCredentailType(String credentailType) {
		this.credentailType = credentailType;
	}

	public String getCredentailSubType() {
		return credentailSubType;
	}

	public void setCredentailSubType(String credentailSubType) {
		this.credentailSubType = credentailSubType;
	}

	public String getCredentailDataType() {
		return credentailDataType;
	}

	public void setCredentailDataType(String credentailDataType) {
		this.credentailDataType = credentailDataType;
	}

	public Integer getCredentailDataLength() {
		return credentailDataLength;
	}

	public void setCredentailDataLength(Integer credentailDataLength) {
		this.credentailDataLength = credentailDataLength;
	}

	public static AccountSummary constructAccountSummary(JSONObject jsonObj){		
		AccountSummary response = null;
		if(jsonObj != null ){				
			response = new AccountSummary();			
			response.id = jsonObj.optLong("id");
			response.maskedAccnumber = jsonObj.optString("maskedAccnumber", null);
			response.ifsc = jsonObj.optString("ifsc", null);
			response.name = jsonObj.optString("name", null);
			response.aeba = jsonObj.optString("aeba", null);
			response.virtualAddress = jsonObj.optString("virtualAddress", null);
			response.isPinSet = jsonObj.optBoolean("isPinSet");	
			response.credentailType = jsonObj.optString("credentailType");
			response.credentailSubType = jsonObj.optString("credentailSubType");
			response.credentailDataType = jsonObj.optString("credentailDataType");
			response.credentailDataLength = jsonObj.optInt("credentailDataLength");
		}
		return response;
	}
	
	public static AccountSummary constructAccountSummary(String jsonStr){		
		AccountSummary response = null;
		if(jsonStr != null && !"".equals(jsonStr)){				
			response = new AccountSummary();	
			JSONObject jsonObj = new JSONObject(jsonStr);
			response.id = jsonObj.optLong("id");
			response.maskedAccnumber = jsonObj.optString("maskedAccnumber", null);
			response.ifsc = jsonObj.optString("ifsc", null);
			response.name = jsonObj.optString("name", null);
			response.aeba = jsonObj.optString("aeba", null);
			response.virtualAddress = jsonObj.optString("virtualAddress", null);
			response.isPinSet = jsonObj.optBoolean("isPinSet");	
			response.credentailType = jsonObj.optString("credentailType");
			response.credentailSubType = jsonObj.optString("credentailSubType");
			response.credentailDataType = jsonObj.optString("credentailDataType");
			response.credentailDataLength = jsonObj.optInt("credentailDataLength");
		}
		return response;
	}
}