package psp.common.model.constants;

public interface ServiceUris {

	String REGISTRATION = "/registration";
	
	String LOGIN = "/login";
	
	String CHANGE_PWD = "/changePassword";
	
	String FORGET_PWD = "/forgetPassword";
	
	String GET_ACC_PVDS = "/getAccProviders";
	
	String OTP_REQ = "/otpRequest";
	
	String GET_ACC_LIST = "/getAccList";
	
	String SET_PIN = "/setPin";
	
	String MAKE_PAYMENT = "/makePayment";
	
	String GET_TXN_DETAILS = "/getTxnDetails";
	
	String BAL_ENQ = "/balEnq";
	
	String ADD_BNK_ACC = "/addBankAcc";
	
	String IS_REGISTER_USER = "/isRegisterUser";
	
	String LOGOUT = "/logout";
	
	String GET_MER_LIST = "/getMerchantList";
	
	String GET_BEN_LIST = "/getBeneficiaryList";
	
	String ADD_BEN = "/addBeneficiary";
	
	String GET_EXISTING_ACCOUNTS = "/getExistingAccounts";
	
	String GET_BEN = "/getBeneficiary";
	
	String GET_KEY = "/getKey";
	
	String GET_TOKEN = "/getToken";
	
	String GET_MER = "/getMerchant";
	
	String GEN_PIN = "/generatePin";
	
	String COLLECT_PAYMENT = "/collectPayment";
	
	String GET_NOTIFICATION_LIST = "/getNotificationList";
	
	String GET_NOTIFICATION_DETAILS = "/getNotificationDetails";
	
	String CONFIRM_COLLECT_PAY = "/confirmCollectPay";
	
}