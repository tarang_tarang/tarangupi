/**
 * 
 */
package psp.common.model.util;

import psp.common.model.constants.FieldErrorMessages;
import psp.common.model.constants.ValidatorConstants;
import psp.constants.StatusCode;
import psp.mobile.model.response.MessageResponse;

/**
 * @author gowthamb
 *
 */
public class MobileValidator implements FieldErrorMessages, ValidatorConstants {

	private MobileValidator(){
	}
	
	public static String validate(String data, boolean isMandatory, int min, int max, String exp, 
			String mandatoryErrMsg, String lengthErrMsg, String charValErrMsg){
		if( isMandatory ){
			if(data == null || data.isEmpty()){
				return mandatoryErrMsg;
			}
		}
		else if(data == null || data.isEmpty()){
			return null;
		}

		if(data.length() < min || data.length() > max){
			return lengthErrMsg;
		}
	    if(!expCheck(data, exp)) {
	    	return charValErrMsg;	
	    }
	    return null;	
	}
	
	public static String validateAmount(String data, boolean isMandatory, int min, int max, String exp, 
			String mandatoryErrMsg, String lengthErrMsg, String charValErrMsg){
		if( isMandatory ){
			if(data == null || data.isEmpty()){
				return mandatoryErrMsg;
			}
		}
		else if(data == null || data.isEmpty()){
			return null;
		}

		if(Double.parseDouble(data) < min || Double.parseDouble(data) > max){
			return lengthErrMsg;
		}
	    if(!expCheck(data, exp)) {
	    	return charValErrMsg;	
	    }
	    return null;	
	}
	
	public static boolean expCheck(String data, String exp) {
	    return data.matches(exp);
	}
	
	public static void setFailureResponse(StatusCode code, MessageResponse response) {
		response.setStatusCode(code.getCode());
		response.setStatusMessage(code.getMessage());
	}

	public static boolean checkEmpty(String str){
		boolean isEmpty= true;
		if (null == str || str.isEmpty()) {
			isEmpty = false;
		}
		return isEmpty;
	}
	
}