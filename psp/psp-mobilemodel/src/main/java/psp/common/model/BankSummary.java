package psp.common.model;

import java.io.Serializable;

import org.json.JSONObject;

public class BankSummary implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	private String bankName;	
	
	public BankSummary() {
	}
	
	public BankSummary(Long id, String bankName) {
		this.id = id;
		this.bankName = bankName;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public static BankSummary constructBankSummary(JSONObject jsonObj){		
		BankSummary response = null;
		if(jsonObj != null ){				
			response = new BankSummary();			
			response.id = jsonObj.optLong("id");
			response.bankName = jsonObj.optString("bankName", null);				
		}
		return response;
	}
}