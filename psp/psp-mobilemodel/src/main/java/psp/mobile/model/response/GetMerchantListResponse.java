package psp.mobile.model.response;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import psp.constants.StatusCode;

@SuppressWarnings("serial")
public class GetMerchantListResponse extends MessageResponse {

	private List<MerchantDetails> merchantList;
	
	public GetMerchantListResponse() {
	}

	public List<MerchantDetails> getMerchantList() {
		return merchantList;
	}

	public void setMerchantList(List<MerchantDetails> merchantList) {
		this.merchantList = merchantList;
	}

	public boolean validate() {
		boolean isSuccess = true;
		
		if (!isSuccess) {
			 setStatusCode(StatusCode.FIELD_VALIDATION_FAILED.getCode());
			 setStatusMessage(StatusCode.FIELD_VALIDATION_FAILED.getMessage());
		}
		return isSuccess;
	}
	
	public static GetMerchantListResponse constructGetMerchantListResponse(String jsonStr){		
		GetMerchantListResponse response = null;
		if(jsonStr != null && !"".equals(jsonStr)){	
			JSONObject jsonObj = new JSONObject(jsonStr);
			response = new GetMerchantListResponse();					
			JSONArray merchantList = jsonObj.getJSONArray("merchantList");
			List<MerchantDetails> merchantDtlsList = new ArrayList<>();			
			 for(int i =0; i < merchantList.length(); i++){
				 JSONObject merchantDtls = merchantList.getJSONObject(i);
				 MerchantDetails merchant = MerchantDetails.construcMerchantDetails(merchantDtls);
				 merchantDtlsList.add(i, merchant);
			 }			
			response.merchantList = merchantDtlsList;
			 response.constructMessageRespnse(jsonStr);
		}
		return response;
	}
}