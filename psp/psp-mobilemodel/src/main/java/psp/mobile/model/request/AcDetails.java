/**
 * 
 */
package psp.mobile.model.request;

import org.json.JSONObject;

/**
 * @author prasadj
 *
 */
public class AcDetails {

	private String addrType;
	
	private String ifsc;
	
	private String actype;
	
	private String acnum;
	
	private String iin;
	
	private String uidnum;
	
	private String mmid;
	
	private String mobnum;
	
	private String cardNum;
	
	public AcDetails(){
	}

	public String getCardNum() {
		return cardNum;
	}

	public void setCardNum(String cardNum) {
		this.cardNum = cardNum;
	}

	public String getAddrType() {
		return addrType;
	}

	public void setAddrType(String addrType) {
		this.addrType = addrType;
	}

	public String getIfsc() {
		return ifsc;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	public String getActype() {
		return actype;
	}

	public void setActype(String actype) {
		this.actype = actype;
	}

	public String getAcnum() {
		return acnum;
	}

	public void setAcnum(String acnum) {
		this.acnum = acnum;
	}

	public String getIin() {
		return iin;
	}

	public void setIin(String iin) {
		this.iin = iin;
	}

	public String getUidnum() {
		return uidnum;
	}

	public void setUidnum(String uidnum) {
		this.uidnum = uidnum;
	}

	public String getMmid() {
		return mmid;
	}

	public void setMmid(String mmid) {
		this.mmid = mmid;
	}

	public String getMobnum() {
		return mobnum;
	}

	public void setMobnum(String mobnum) {
		this.mobnum = mobnum;
	}
	
	public JSONObject toJsonString() {
		 JSONObject jobj = new JSONObject();
		 jobj.put("addrType", addrType);
		 jobj.put("ifsc", ifsc);
		 jobj.put("actype", actype);
		 jobj.put("acnum", acnum);
		 jobj.put("iin", iin);
		 jobj.put("uidnum", uidnum);
		 jobj.put("mmid", mmid);
		 jobj.put("mobnum", mobnum);
		 jobj.put("cardNum", cardNum);
		 return jobj;
	}
	
}