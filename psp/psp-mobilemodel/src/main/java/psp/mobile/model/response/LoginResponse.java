package psp.mobile.model.response;

import psp.common.model.AccountSummary;
import psp.common.model.constants.ValidatorConstants;
import psp.common.model.util.MobileValidator;
import psp.constants.StatusCode;
import psp.mobile.model.request.LoginRequest;

@SuppressWarnings("serial")
public class LoginResponse extends MessageResponse implements ValidatorConstants{

	private String userNameErrMsg;
	
	private String passwordErrMsg;
	
	private AccountSummary accountSummary;
	
	public LoginResponse() {
	}

	public String getUserNameErrMsg() {
		return userNameErrMsg;
	}

	public void setUserNameErrMsg(String userNameErrMsg) {
		this.userNameErrMsg = userNameErrMsg;
	}

	public String getPasswordErrMsg() {
		return passwordErrMsg;
	}

	public void setPasswordErrMsg(String passwordErrMsg) {
		this.passwordErrMsg = passwordErrMsg;
	}
 
	public AccountSummary getAccountSummary() {
		return accountSummary;
	}

	public void setAccountSummary(AccountSummary accountSummary) {
		this.accountSummary = accountSummary;
	}
	
	public boolean validate(LoginRequest req) {
		boolean isSuccess = true;

		String errMsg = MobileValidator.validate(req.getUserName(), USER_NAME_IS_MANDATORY, USER_NAME_MIN_LENGTH, USER_NAME_MAX_LENGTH,
				USER_NAME_EXPRESSION, USER_NAME_MANDATORY_ERR_MSG, USER_NAME_LENGTH_ERR_MSG, USER_NAME_EXPRESSION_ERR_MSG);
		if( errMsg != null){
			setUserNameErrMsg(errMsg);
			isSuccess = false;
		}
		
		errMsg = MobileValidator.validate(req.getPassword(), PASSWORD_IS_MANDATORY, PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH,
				PASSWORD_EXPRESSION, PASSWORD_MANDATORY_ERR_MSG, PASSWORD_LENGTH_ERR_MSG, PASSWORD_EXPRESSION_ERR_MSG);
		if( errMsg != null){
			setPasswordErrMsg(errMsg);
			isSuccess = false;
		}
		if (!isSuccess) {
			setStatusCode(StatusCode.FIELD_VALIDATION_FAILED.getCode());
			setStatusMessage(StatusCode.FIELD_VALIDATION_FAILED.getMessage()); 
		}
		return isSuccess;
	}
}