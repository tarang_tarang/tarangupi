package psp.mobile.model.request;

import org.json.JSONObject;


@SuppressWarnings("serial")
public class LogoutRequest extends MobileRequest {

	public LogoutRequest() {
	}

	public String toJsonString(){
		JSONObject jobj = getJsonString();
		return jobj.toString();
	}
	
}