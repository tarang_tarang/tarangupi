package psp.mobile.model.response;

import org.json.JSONObject;

import psp.constants.StatusCode;


@SuppressWarnings("serial")
public class GetMerchantResponse extends MessageResponse {

	private MerchantDetails merchantDetails;
	
	public GetMerchantResponse() {
	}

	public MerchantDetails getMerchantDetails() {
		return merchantDetails;
	}

	public void setMerchantDetails(MerchantDetails merchantDetails) {
		this.merchantDetails = merchantDetails;
	}

	public boolean validate() {
		boolean isSuccess = true;
		
		if (!isSuccess) {
			 setStatusCode(StatusCode.FIELD_VALIDATION_FAILED.getCode());
			 setStatusMessage(StatusCode.FIELD_VALIDATION_FAILED.getMessage());
		}
		return isSuccess;
	}
	
	public static GetMerchantResponse constructGetMerchantResponse(String jsonStr){	
		GetMerchantResponse response = null;
		if(jsonStr != null && !"".equals(jsonStr)){	
			JSONObject jsonObj = new JSONObject(jsonStr);
			response = new GetMerchantResponse();
			response.constructMessageRespnse(jsonStr);			
			response.merchantDetails =MerchantDetails.construcMerchantDetails(jsonObj.getJSONObject("merchantDetails"));			
		}
		return response;
	}
}