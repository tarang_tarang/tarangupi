package psp.mobile.model.request;

import org.json.JSONObject;


@SuppressWarnings("serial")
public class GetMerchantRequest extends MobileRequest {

	private Long merchantId;
	
	public GetMerchantRequest() {
	}

	public Long getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(Long merchantId) {
		this.merchantId = merchantId;
	}

	public String toJsonString() {
		 JSONObject jobj = getJsonString();
		 jobj.put("merchantId", merchantId);
		 return jobj.toString();
	}
	
}