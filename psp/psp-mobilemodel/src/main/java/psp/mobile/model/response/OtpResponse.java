package psp.mobile.model.response;

import psp.common.model.constants.ValidatorConstants;
import psp.common.model.util.MobileValidator;
import psp.constants.StatusCode;
import psp.mobile.model.request.AcDetails;


@SuppressWarnings("serial")
public class OtpResponse extends MessageResponse implements ValidatorConstants{

	private String  mmidErrMsg;
	
	private String acnumErrMsg;
	
	private String iinErrMsg;
	
	private String uidnumErrMsg;
	
	private String mobnumErrMsg;
	
	private String cardNumErrMsg;
	
	private String generalErrMsg;
	
	private String ifscErrMsg;
	
	private String actypeErrMsg;
	
	public OtpResponse() {
	}

	public String getMmidErrMsg() {
		return mmidErrMsg;
	}

	public void setMmidErrMsg(String mmidErrMsg) {
		this.mmidErrMsg = mmidErrMsg;
	}

	public String getAcnumErrMsg() {
		return acnumErrMsg;
	}

	public void setAcnumErrMsg(String acnumErrMsg) {
		this.acnumErrMsg = acnumErrMsg;
	}

	public String getIinErrMsg() {
		return iinErrMsg;
	}

	public void setIinErrMsg(String iinErrMsg) {
		this.iinErrMsg = iinErrMsg;
	}

	public String getUidnumErrMsg() {
		return uidnumErrMsg;
	}

	public void setUidnumErrMsg(String uidnumErrMsg) {
		this.uidnumErrMsg = uidnumErrMsg;
	}

	public String getMobnumErrMsg() {
		return mobnumErrMsg;
	}

	public void setMobnumErrMsg(String mobnumErrMsg) {
		this.mobnumErrMsg = mobnumErrMsg;
	}

	public String getCardNumErrMsg() {
		return cardNumErrMsg;
	}

	public void setCardNumErrMsg(String cardNumErrMsg) {
		this.cardNumErrMsg = cardNumErrMsg;
	}

	public String getGeneralErrMsg() {
		return generalErrMsg;
	}

	public void setGeneralErrMsg(String generalErrMsg) {
		this.generalErrMsg = generalErrMsg;
	}

	public String getIfscErrMsg() {
		return ifscErrMsg;
	}

	public void setIfscErrMsg(String ifscErrMsg) {
		this.ifscErrMsg = ifscErrMsg;
	}

	public String getActypeErrMsg() {
		return actypeErrMsg;
	}

	public void setActypeErrMsg(String actypeErrMsg) {
		this.actypeErrMsg = actypeErrMsg;
	}
	
	public boolean validate(AcDetails acDetails) {
		boolean isSuccess = true;
		
		String errMsg = null;
		
		String AccDetail = acDetails.getAddrType();
		if(AccDetail == null || AccDetail.isEmpty()) {
			setGeneralErrMsg("Please select Account or Mobile number or Aadhaar number or Card Number any one is mandatory");
			isSuccess = false; 
		}
		else {	
		switch (AccDetail) {
		case "MOBILE":
			errMsg = MobileValidator.validate(acDetails.getMobnum(), MOBILE_NUMBER_IS_MANDATORY, MOBILE_NUMBER_MIN_LENGTH, MOBILE_NUMBER_MAX_LENGTH,
				MOBILE_NUMBER_EXPRESSION, MOBILE_NUMBER_MANDATORY_ERR_MSG, MOBILE_NUMBER_LENGTH_ERR_MSG, MOBILE_NUMBER_EXPRESSION_ERR_MSG);
			if( errMsg != null){
				setMobnumErrMsg(errMsg);
				isSuccess = false;
			}
		
			errMsg = MobileValidator.validate(acDetails.getMmid(), MMID_IS_MANDATORY, MMID_MIN_LENGTH, MMID_MAX_LENGTH,
				MMID_EXPRESSION, MMID_MANDATORY_ERR_MSG, MMID_LENGTH_ERR_MSG, MMID_EXPRESSION_ERR_MSG);
			if( errMsg != null){
			setMmidErrMsg(errMsg);
			isSuccess = false;
			}
			break;
        
			case "AADHAAR":
			errMsg = MobileValidator.validate(acDetails.getUidnum(), AADHAR_NUMBER_IS_MANDATORY, AADHAR_NUMBER_MIN_LENGTH, AADHAR_NUMBER_MAX_LENGTH,
					AADHAR_NUMBER_EXPRESSION, AADHAR_NUMBER_MANDATORY_ERR_MSG, AADHAR_NUMBER_LENGTH_ERR_MSG, AADHAR_NUMBER_EXPRESSION_ERR_MSG);
			if( errMsg != null){
				setUidnumErrMsg(errMsg);
				isSuccess = false;
			}
		
			errMsg = MobileValidator.validate(acDetails.getIin(), IIN_IS_MANDATORY, IIN_MIN_LENGTH, IIN_MAX_LENGTH,
					IIN_EXPRESSION, IIN_MANDATORY_ERR_MSG, IIN_LENGTH_ERR_MSG, IIN_EXPRESSION_ERR_MSG);
			if( errMsg != null){
				setIinErrMsg(errMsg);
				isSuccess = false;
			}
			break;
			
			case "ACCOUNT":
			errMsg = MobileValidator.validate(acDetails.getIfsc(), IFSC_IS_MANDATORY, IFSC_MIN_LENGTH, IFSC_MAX_LENGTH,
					IFSC_EXPRESSION, IFSC_MANDATORY_ERR_MSG, IFSC_LENGTH_ERR_MSG, IFSC_EXPRESSION_ERR_MSG);
			if( errMsg != null){
				setIfscErrMsg(errMsg);
				isSuccess = false;
			}
			
			errMsg = MobileValidator.validate(acDetails.getActype(), ACC_TYPE_IS_MANDATORY, ACC_TYPE_MIN_LENGTH, ACC_TYPE_MAX_LENGTH,
					ACC_TYPE_EXPRESSION, ACC_TYPE_MANDATORY_ERR_MSG, ACC_TYPE_LENGTH_ERR_MSG, ACC_TYPE_EXPRESSION_ERR_MSG);
			if( errMsg != null){
				setActypeErrMsg(errMsg);
				isSuccess = false;
			}
			
			errMsg = MobileValidator.validate(acDetails.getAcnum(), ACC_NUM_IS_MANDATORY, ACC_NUM_MIN_LENGTH, ACC_NUM_MAX_LENGTH,
					ACC_NUM_EXPRESSION, ACC_NUM_MANDATORY_ERR_MSG, ACC_NUM_LENGTH_ERR_MSG, ACC_NUM_EXPRESSION_ERR_MSG);
			if( errMsg != null){
				setAcnumErrMsg(errMsg);
				isSuccess = false;
			}
			break;
			
			case "CARD":
			errMsg = MobileValidator.validate(acDetails.getActype(), ACC_TYPE_IS_MANDATORY, ACC_TYPE_MIN_LENGTH, ACC_TYPE_MAX_LENGTH,
					ACC_TYPE_EXPRESSION, ACC_TYPE_MANDATORY_ERR_MSG, ACC_TYPE_LENGTH_ERR_MSG, ACC_TYPE_EXPRESSION_ERR_MSG);
			if( errMsg != null){
				setActypeErrMsg(errMsg);
				isSuccess = false;
			}
			
			errMsg = MobileValidator.validate(acDetails.getCardNum(), CARD_NUM_IS_MANDATORY, CARD_NUM_MIN_LENGTH, CARD_NUM_MAX_LENGTH,
					CARD_NUM_EXPRESSION, CARD_NUM_MANDATORY_ERR_MSG, CARD_NUM_LENGTH_ERR_MSG, CARD_NUM_EXPRESSION_ERR_MSG);
			if( errMsg != null){
				setCardNumErrMsg(errMsg);
				isSuccess = false;
			}
			break;
		}
	}
		
		if (!isSuccess) {
			setStatusCode(StatusCode.FIELD_VALIDATION_FAILED.getCode());
			setStatusMessage(StatusCode.FIELD_VALIDATION_FAILED.getMessage()); 
		}
		return isSuccess;
	}
}