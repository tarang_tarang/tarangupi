package psp.mobile.model.request;

import org.json.JSONObject;


@SuppressWarnings("serial")
public class GetTxnDetailsRequest extends MobileRequest {

	private String fromDate;
	
	private String toDate;
	
	public GetTxnDetailsRequest() {
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String toJsonString() {
		 JSONObject jobj = getJsonString();
		 jobj.put("fromDate", fromDate);
		 jobj.put("toDate", toDate);
		 return jobj.toString();
	}
	
}