package psp.mobile.model.response;

import org.json.JSONObject;

import psp.common.model.constants.ValidatorConstants;
import psp.common.model.util.MobileValidator;
import psp.constants.StatusCode;
import psp.mobile.model.request.CheckUserRequest;



@SuppressWarnings("serial")
public class CheckUserResponse extends MessageResponse implements ValidatorConstants{
	
	private String mobileNumErrMsg;
	
	public CheckUserResponse(){		
	}

	public String getMobileNumErrMsg() {
		return mobileNumErrMsg;
	}

	public void setMobileNumErrMsg(String mobileNumErrMsg) {
		this.mobileNumErrMsg = mobileNumErrMsg;
	}
	
	public boolean validate(CheckUserRequest req) {
		boolean isSuccess = true;
		
		String errMsg = MobileValidator.validate(req.getMobileNum(), MOBILE_NUMBER_IS_MANDATORY, MOBILE_NUMBER_MIN_LENGTH, MOBILE_NUMBER_MAX_LENGTH,
				MOBILE_NUMBER_EXPRESSION, MOBILE_NUMBER_MANDATORY_ERR_MSG, MOBILE_NUMBER_LENGTH_ERR_MSG, MOBILE_NUMBER_EXPRESSION_ERR_MSG);
		if( errMsg != null){
			setMobileNumErrMsg(errMsg);
			isSuccess = false;
		}
		
		if (!isSuccess) {
			 setStatusCode(StatusCode.FIELD_VALIDATION_FAILED.getCode());
			 setStatusMessage(StatusCode.FIELD_VALIDATION_FAILED.getMessage());
		}
		return isSuccess;
		
	}
	
	public static CheckUserResponse constructCheckUserResponse(String jsonStr){		
		CheckUserResponse response = null;
		if(jsonStr != null && !"".equals(jsonStr)){	
			JSONObject jsonObj = new JSONObject(jsonStr);
			response = new CheckUserResponse();
			response.mobileNumErrMsg = jsonObj.optString("mobileNumErrMsg", null);			
			response.constructMessageRespnse(jsonStr);
		}
		return response;
	}
}