package psp.mobile.model.request;

import org.json.JSONObject;


@SuppressWarnings("serial")
public class GetBeneficiaryRequest extends MobileRequest {

	private String nickName;
	
	public GetBeneficiaryRequest(){		
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	
	public String toJsonString() {
		 JSONObject obj = getJsonString();
		 obj.put("nickName", nickName);
		 return obj.toString();
	}
	
}