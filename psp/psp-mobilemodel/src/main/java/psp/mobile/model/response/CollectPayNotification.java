package psp.mobile.model.response;

import org.json.JSONObject;

import psp.mobile.model.request.MobileNotification;

public class CollectPayNotification extends MobileNotification {
	
	private String requestorMobileNumber;
	
	private String requestorName;
	
	private String requestorVirtualAddress;
	
	private String requestorNote;
	
	private String amount;
	
	private String selfVitrualAddress;
	
	private String txnId;
	
	public CollectPayNotification() {
	}
	
	public String getRequestorMobileNumber() {
		return requestorMobileNumber;
	}

	public void setRequestorMobileNumber(String requestorMobileNumber) {
		this.requestorMobileNumber = requestorMobileNumber;
	}

	public String getRequestorName() {
		return requestorName;
	}

	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}

	public String getRequestorVirtualAddress() {
		return requestorVirtualAddress;
	}
	
	public void setRequestorVirtualAddress(String requestorVirtualAddress) {
		this.requestorVirtualAddress = requestorVirtualAddress;
	}
	
	public String getRequestorNote() {
		return requestorNote;
	}
	
	public void setRequestorNote(String requestorNote) {
		this.requestorNote = requestorNote;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	public String getSelfVitrualAddress() {
		return selfVitrualAddress;
	}
	
	public void setSelfVitrualAddress(String selfVitrualAddress) {
		this.selfVitrualAddress = selfVitrualAddress;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public static CollectPayNotification constructNotification(JSONObject jsonObj){		
		CollectPayNotification response = null;
		if(jsonObj != null ) {	
			response = new CollectPayNotification();
			response.constructMobileNotificationDetails(jsonObj);
			response.requestorMobileNumber = jsonObj.optString("requestorMobileNumber");
			response.requestorName = jsonObj.optString("requestorName", null);
			response.requestorVirtualAddress = jsonObj.optString("requestorVirtualAddress", null);
			response.requestorNote = jsonObj.optString("requestorNote", null);
			response.amount = jsonObj.optString("amount", null);
			response.selfVitrualAddress = jsonObj.optString("selfVitrualAddress", null);
			response.txnId = jsonObj.optString("txnId", null);
		}
		return response;
	}
}
