package psp.mobile.model.request;

import org.json.JSONObject;

@SuppressWarnings("serial")
public class LoginRequest extends MobileRequest {

	private String password;
	
	public LoginRequest() {
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String toJsonString() {
		 JSONObject jobj = getJsonString();
		 jobj.put("password", password);
		 return jobj.toString();
	}
	
}