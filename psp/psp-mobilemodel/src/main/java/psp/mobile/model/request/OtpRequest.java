package psp.mobile.model.request;

import org.json.JSONObject;


@SuppressWarnings("serial")
public class OtpRequest extends MobileRequest {

	private AcDetails  acDetails;
	
	public OtpRequest() {
	}

	public AcDetails getAcDetails() {
		return acDetails;
	}

	public void setAcDetails(AcDetails acDetails) {
		this.acDetails = acDetails;
	}

	public String toJsonString() {
		 JSONObject jobj = getJsonString();
		 if(acDetails != null) {
			 jobj.put("acDetails", acDetails.toJsonString());
		 }
		 return jobj.toString();
	}
	
}