/**
 * 
 */
package psp.mobile.model.request;

import java.io.Serializable;

import org.json.JSONObject;

/**
 * @author gowthamb
 *
 */
public class MobileRequest implements Serializable {

	private static final long serialVersionUID = 1L;
		
	private String userName;
	
	private MobileDevice device;
	
	private String txnId;
	
	public MobileRequest() {	
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public MobileDevice getDevice() {
		return device;
	}

	public void setDevice(MobileDevice device) {
		this.device = device;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public JSONObject getJsonString(){
		JSONObject jobj = new JSONObject();
		jobj.put("userName", userName);
		jobj.put("txnId", txnId);
		if(device != null){
			jobj.put("device", device.toJsonString());
		}
		return jobj;
	}
	
}