package psp.mobile.model.response;

import psp.constants.StatusCode;


@SuppressWarnings("serial")
public class GetTokenResponse extends MessageResponse {

	private String code;
	
	private String type;
	
	private String keyIndexDate;
	
	private String keyValue;
	
	public GetTokenResponse() {
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getKeyIndexDate() {
		return keyIndexDate;
	}

	public void setKeyIndexDate(String keyIndexDate) {
		this.keyIndexDate = keyIndexDate;
	}

	public String getKeyValue() {
		return keyValue;
	}

	public void setKeyValue(String keyValue) {
		this.keyValue = keyValue;
	}

	public boolean validate() {
		boolean isSuccess = true;
		
		if (!isSuccess) {
			 setStatusCode(StatusCode.FIELD_VALIDATION_FAILED.getCode());
			 setStatusMessage(StatusCode.FIELD_VALIDATION_FAILED.getMessage());
		}
		return isSuccess;
	}
}