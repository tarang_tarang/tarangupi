package psp.mobile.model.response;

import psp.constants.StatusCode;

@SuppressWarnings("serial")
public class CollectPayConfirmResponse extends MessageResponse {

	public CollectPayConfirmResponse() {
	}
	
	public boolean validate() {
		boolean isSuccess = true;
		if (!isSuccess) {
			setStatusCode(StatusCode.FIELD_VALIDATION_FAILED.getCode());
			setStatusMessage(StatusCode.FIELD_VALIDATION_FAILED.getMessage());
		}
		return false;
	}
	
	public static CollectPayConfirmResponse constructCollectPayConfirmResponse(String jsonStr){		
		CollectPayConfirmResponse response = null;
		if(jsonStr != null && !"".equals(jsonStr)){	
			response = new CollectPayConfirmResponse();
			response.constructMessageRespnse(jsonStr);
		}
		return response;
	}
}
