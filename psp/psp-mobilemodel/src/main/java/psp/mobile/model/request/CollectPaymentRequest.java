package psp.mobile.model.request;

import org.json.JSONObject;

@SuppressWarnings("serial")
public class CollectPaymentRequest extends MobileRequest {

	private Double amount;
	
	private String selfVirtualAddr;
	
	private String thirdPartyVirtualAddr;
	
	private String txnNote;
	
	private Integer durationInDays;
	
	public CollectPaymentRequest() {
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	public String getSelfVirtualAddr() {
		return selfVirtualAddr;
	}

	public void setSelfVirtualAddr(String selfVirtualAddr) {
		this.selfVirtualAddr = selfVirtualAddr;
	}

	public String getThirdPartyVirtualAddr() {
		return thirdPartyVirtualAddr;
	}

	public void setThirdPartyVirtualAddr(String thirdPartyVirtualAddr) {
		this.thirdPartyVirtualAddr = thirdPartyVirtualAddr;
	}

	public String getTxnNote() {
		return txnNote;
	}

	public void setTxnNote(String txnNote) {
		this.txnNote = txnNote;
	}

	public Integer getDurationInDays() {
		return durationInDays;
	}

	public void setDurationInDays(Integer durationInDays) {
		this.durationInDays = durationInDays;
	}
	
	public String toJsonString() {
		 JSONObject jobj = getJsonString();
		 jobj.put("amount", amount);
		 jobj.put("selfVirtualAddr", selfVirtualAddr);
		 jobj.put("thirdPartyVirtualAddr", thirdPartyVirtualAddr);
		 jobj.put("txnNote", txnNote);
		 jobj.put("durationInDays", durationInDays);
		 return jobj.toString();
	}
}