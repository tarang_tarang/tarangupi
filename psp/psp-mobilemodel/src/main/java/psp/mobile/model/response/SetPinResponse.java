package psp.mobile.model.response;

import psp.constants.StatusCode;


@SuppressWarnings("serial")
public class SetPinResponse extends MessageResponse {

	private String oldPinErrMsg;
	
	private String newPinErrMsg;
	
	public SetPinResponse() {
	}

	public String getOldPinErrMsg() {
		return oldPinErrMsg;
	}

	public void setOldPinErrMsg(String oldPinErrMsg) {
		this.oldPinErrMsg = oldPinErrMsg;
	}

	public String getNewPinErrMsg() {
		return newPinErrMsg;
	}

	public void setNewPinErrMsg(String newPinErrMsg) {
		this.newPinErrMsg = newPinErrMsg;
	}
	
	public boolean validate() {
		boolean isSuccess = true;

		if (!isSuccess) {
			setStatusCode(StatusCode.FIELD_VALIDATION_FAILED.getCode());
			setStatusMessage(StatusCode.FIELD_VALIDATION_FAILED.getMessage()); 
		}
		return isSuccess;
	}
}