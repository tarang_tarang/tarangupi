package psp.mobile.model.request;

import org.json.JSONObject;

@SuppressWarnings("serial")
public class GetNotificationListRequest extends MobileRequest {

	public GetNotificationListRequest() {
	}
	
	public String toJsonString(){
		JSONObject jobj = getJsonString();
		return jobj.toString();
	}
}
