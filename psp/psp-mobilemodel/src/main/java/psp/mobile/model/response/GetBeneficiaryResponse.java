package psp.mobile.model.response;

import org.json.JSONObject;

import psp.common.model.constants.ValidatorConstants;
import psp.common.model.util.MobileValidator;
import psp.constants.StatusCode;
import psp.mobile.model.request.GetBeneficiaryRequest;


@SuppressWarnings("serial")
public class GetBeneficiaryResponse extends MessageResponse implements ValidatorConstants{

	private Long id;
	
	private String address;

	private String addressType;
	
	private String nickName;
	
	private String description;

	private String referenceNumber;
	
	private String maskedNumber;
	
	private String ifsc;
	
	private String mmid;
	
	private String name;
	
	private String aeba;

	public GetBeneficiaryResponse(){		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getMaskedNumber() {
		return maskedNumber;
	}

	public void setMaskedNumber(String maskedNumber) {
		this.maskedNumber = maskedNumber;
	}

	public String getIfsc() {
		return ifsc;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	public String getMmid() {
		return mmid;
	}

	public void setMmid(String mmid) {
		this.mmid = mmid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAeba() {
		return aeba;
	}

	public void setAeba(String aeba) {
		this.aeba = aeba;
	}

	public boolean validate(GetBeneficiaryRequest req) {
		boolean isSuccess = true;

		String errMsg = MobileValidator.validate(req.getNickName(), NICK_NAME_IS_MANDATORY, NICK_NAME_MIN_LENGTH, NICK_NAME_MAX_LENGTH, 
				NICK_NAME_EXPRESSION, NICK_NAME_MANDATORY_ERR_MSG, NICK_NAME_LENGTH_ERR_MSG, NICK_NAME_EXPRESSION_ERR_MSG);
		if( errMsg != null){	
			setNickName(errMsg);
			isSuccess = false;
		}
		if (!isSuccess) {
				 setStatusCode(StatusCode.FIELD_VALIDATION_FAILED.getCode());
				 setStatusMessage(StatusCode.FIELD_VALIDATION_FAILED.getMessage());
		}
		return isSuccess;
	}	

	public static GetBeneficiaryResponse constructGetBeneficiaryResponse(String jsonStr){		
		GetBeneficiaryResponse response = null;
		if(jsonStr != null && !"".equals(jsonStr)){	
			JSONObject jsonObj = new JSONObject(jsonStr);
			response = new GetBeneficiaryResponse();
			response.constructMessageRespnse(jsonStr);
			response.id = jsonObj.optLong("id");
			response.address = jsonObj.optString("address", null);
			response.addressType = jsonObj.optString("addressType", null);
			response.nickName = jsonObj.optString("nickName", null);
			response.description = jsonObj.optString("description", null);
			response.referenceNumber = jsonObj.optString("referenceNumber", null);
			response.maskedNumber = jsonObj.optString("maskedNumber", null);
			response.ifsc = jsonObj.optString("ifsc", null);
			response.mmid = jsonObj.optString("mmid", null);
			response.name = jsonObj.optString("name", null);
			response.aeba = jsonObj.optString("aeba", null);
		}
		return response;
	}
	
}