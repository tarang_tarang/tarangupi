package psp.mobile.model.request;

import org.json.JSONObject;


@SuppressWarnings("serial")
public class GetTokenRequest extends MobileRequest {

	private MobileCredentials mobileCredentials;
	
	public GetTokenRequest() {
	}

	public MobileCredentials getMobileCredentials() {
		return mobileCredentials;
	}

	public void setMobileCredentials(MobileCredentials mobileCredentials) {
		this.mobileCredentials = mobileCredentials;
	}

	public String toJsonString() {
		 JSONObject jobj = getJsonString();
		 if(mobileCredentials != null) {
			 jobj.put("mobileCredentials", mobileCredentials.toJsonString());
		 }
		 return jobj.toString();
	}
	
}