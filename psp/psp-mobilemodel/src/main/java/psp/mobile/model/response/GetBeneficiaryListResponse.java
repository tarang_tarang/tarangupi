package psp.mobile.model.response;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import psp.constants.StatusCode;

@SuppressWarnings("serial")
public class GetBeneficiaryListResponse extends MessageResponse {
	
	private List<BeneficiarySummary> benefSummaries;
	
	public GetBeneficiaryListResponse() {
	}

	public List<BeneficiarySummary> getBenefSummaries() {
		return benefSummaries;
	}

	public void setBenefSummaries(List<BeneficiarySummary> benefSummaries) {
		this.benefSummaries = benefSummaries;
	}
	
	public boolean validate() {
		boolean isSuccess = true;

		if (!isSuccess) {
			 setStatusCode(StatusCode.FIELD_VALIDATION_FAILED.getCode());
			 setStatusMessage(StatusCode.FIELD_VALIDATION_FAILED.getMessage());
		}
		return isSuccess;
	}
	
	public static GetBeneficiaryListResponse constructGetBeneficiaryListResponse(String jsonStr){		
		GetBeneficiaryListResponse response = null;
		if(jsonStr != null && !"".equals(jsonStr)){	
			JSONObject jsonObj = new JSONObject(jsonStr);
			response = new GetBeneficiaryListResponse();			
			JSONArray bnfSummaryList = jsonObj.getJSONArray("benefSummaries");
			List<BeneficiarySummary> benfSummaryList = new ArrayList<>();			
			 for(int i =0; i < bnfSummaryList.length(); i++){
				 JSONObject bnfSummary = bnfSummaryList.getJSONObject(i);
				 BeneficiarySummary benfSummary = BeneficiarySummary.constructBeneficiarySummary(bnfSummary);
				 benfSummaryList.add(i, benfSummary);
			 }			
			response.benefSummaries = benfSummaryList;
			 response.constructMessageRespnse(jsonStr);
		}
		return response;
	}
}