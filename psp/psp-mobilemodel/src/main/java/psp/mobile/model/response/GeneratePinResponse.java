package psp.mobile.model.response;

import psp.constants.StatusCode;


@SuppressWarnings("serial")
public class GeneratePinResponse extends MessageResponse {
	
	public GeneratePinResponse() {
	}

	public boolean vaildate() {
		boolean isSuccess = true;
		
		if (!isSuccess) {
			 setStatusCode(StatusCode.FIELD_VALIDATION_FAILED.getCode());
			 setStatusMessage(StatusCode.FIELD_VALIDATION_FAILED.getMessage());
		}
		return isSuccess;
	}
	
	public static GeneratePinResponse constructGeneratePinResponse(String jsonStr){
		GeneratePinResponse response = null;
		if(jsonStr != null && !"".equals(jsonStr)){
			response = new GeneratePinResponse();
			response.constructMessageRespnse(jsonStr);
		}
		return response;
	}
	
}