package psp.mobile.model.response;

import org.json.JSONObject;

public class MerchantDetails {
	
	private Long id;
	
	private String name;
	
	private String nickName;
	
	private String virtualAddress;
	
	private String description;
	
	public MerchantDetails() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getVirtualAddress() {
		return virtualAddress;
	}

	public void setVirtualAddress(String virtualAddress) {
		this.virtualAddress = virtualAddress;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public static MerchantDetails construcMerchantDetails(JSONObject jsonObj){		
		MerchantDetails response = null;
		if(jsonObj != null ){				
			response = new MerchantDetails();			
			response.id = jsonObj.optLong("id");
			response.name = jsonObj.optString("name", null);	
			response.nickName = jsonObj.optString("nickName", null);	
			response.virtualAddress = jsonObj.optString("virtualAddress", null);	
			response.description = jsonObj.optString("description", null);	
		}
		return response;
	}
}