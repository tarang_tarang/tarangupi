package psp.mobile.model.response;

import org.json.JSONObject;

import psp.common.model.constants.ValidatorConstants;
import psp.common.model.util.MobileValidator;
import psp.constants.StatusCode;
import psp.mobile.model.request.AddBeneficiaryRequest;


@SuppressWarnings("serial")
public class AddBeneficiaryResponse extends MessageResponse implements ValidatorConstants {

	private String nickNameErrMsg;
	
	private String beneficiaryNoteErrMsg;
	
	public AddBeneficiaryResponse() {
	}

	public String getNickNameErrMsg() {
		return nickNameErrMsg;
	}

	public void setNickNameErrMsg(String nickNameErrMsg) {
		this.nickNameErrMsg = nickNameErrMsg;
	}

	public String getBeneficiaryNoteErrMsg() {
		return beneficiaryNoteErrMsg;
	}

	public void setBeneficiaryNoteErrMsg(String beneficiaryNoteErrMsg) {
		this.beneficiaryNoteErrMsg = beneficiaryNoteErrMsg;
	}

	public boolean validate(AddBeneficiaryRequest req){
		boolean isSuccess = true;
		
		String errMsg = MobileValidator.validate(req.getNickName(), NICK_NAME_IS_MANDATORY, NICK_NAME_MIN_LENGTH, NICK_NAME_MAX_LENGTH,
				NICK_NAME_EXPRESSION, NICK_NAME_MANDATORY_ERR_MSG, NICK_NAME_LENGTH_ERR_MSG, NICK_NAME_EXPRESSION_ERR_MSG);
		if( errMsg != null){
			setNickNameErrMsg(errMsg);
			isSuccess = false;
		}

		errMsg = MobileValidator.validate(req.getBeneficiaryNote(), BENEFICIARY_NOTE_IS_MANDATORY, BENEFICIARY_NOTE_MIN_LENGTH, BENEFICIARY_NOTE_MAX_LENGTH,
				BENEFICIARY_NOTE_EXPRESSION, BENEFICIARY_NOTE_MANDATORY_ERR_MSG, BENEFICIARY_NOTE_LENGTH_ERR_MSG, BENEFICIARY_NOTE_EXPRESSION_ERR_MSG);
		if( errMsg != null){
			setBeneficiaryNoteErrMsg(errMsg);
			isSuccess = false;
		}
		
		if (!isSuccess) {
			 setStatusCode(StatusCode.FIELD_VALIDATION_FAILED.getCode());
			 setStatusMessage(StatusCode.FIELD_VALIDATION_FAILED.getMessage());
		}
		return isSuccess;
	}
	

	public static AddBeneficiaryResponse constructAddBeneficiaryResponse(String jsonStr){		
		AddBeneficiaryResponse response = null;
		if(jsonStr != null && !"".equals(jsonStr)){	
			JSONObject jsonObj = new JSONObject(jsonStr);
			response = new AddBeneficiaryResponse();
			response.constructMessageRespnse(jsonStr);
			response.nickNameErrMsg = jsonObj.optString("nickNameErrMsg", null);
			response.beneficiaryNoteErrMsg = jsonObj.optString("beneficiaryNoteErrMsg", null);
		}
		return response;
	}
}