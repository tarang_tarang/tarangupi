package psp.mobile.model.request;

import org.json.JSONObject;


@SuppressWarnings("serial")
public class ForgetPasswordRequest extends MobileRequest {
	
	public ForgetPasswordRequest() {
	}

	public String toJsonString(){
		JSONObject jobj = getJsonString();
		return jobj.toString();
	}
	
}