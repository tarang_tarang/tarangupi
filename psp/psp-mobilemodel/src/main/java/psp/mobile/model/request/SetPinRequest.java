package psp.mobile.model.request;

import org.json.JSONObject;


@SuppressWarnings("serial")
public class SetPinRequest extends MobileRequest {

	private Boolean isChangePin;
	
	private MobileCredentials mobileCredentials;
	
	public SetPinRequest() {
	}

	public Boolean getIsChangePin() {
		return isChangePin;
	}

	public void setIsChangePin(Boolean isChangePin) {
		this.isChangePin = isChangePin;
	}

	public MobileCredentials getMobileCredentials() {
		return mobileCredentials;
	}

	public void setMobileCredentials(MobileCredentials mobileCredentials) {
		this.mobileCredentials = mobileCredentials;
	}
	
	public String toJsonString() {
		 JSONObject jobj = getJsonString();
		 jobj.put("isChangePin", isChangePin);
		 if(mobileCredentials != null) {
			 jobj.put("mobileCredentials", mobileCredentials.toJsonString());
		 }
		 return jobj.toString();
	}

}