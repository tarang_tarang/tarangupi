package psp.mobile.model.response;

import psp.constants.StatusCode;


@SuppressWarnings("serial")
public class LogoutResponse extends MessageResponse {

	public LogoutResponse() {
	}

	public boolean validate() {
		boolean isSuccess = true;
		
		if (!isSuccess) {
		setStatusCode(StatusCode.FIELD_VALIDATION_FAILED.getCode());
		setStatusMessage(StatusCode.FIELD_VALIDATION_FAILED.getMessage()); 
	}
		return isSuccess;
	}
}