package psp.mobile.model.response;

import psp.constants.StatusCode;


@SuppressWarnings("serial")
public class ForgetPasswordResponse extends MessageResponse {

	public ForgetPasswordResponse() {
	}

	public boolean validate() {
		boolean isSuccess = true;
		
		if (!isSuccess) {
			 setStatusCode(StatusCode.FIELD_VALIDATION_FAILED.getCode());
			 setStatusMessage(StatusCode.FIELD_VALIDATION_FAILED.getMessage());
		}
		return isSuccess;
	}
	public static ForgetPasswordResponse constructForgetPasswordResponse(String jsonStr){
		ForgetPasswordResponse response = null;
		if(jsonStr != null && !"".equals(jsonStr)){
			response = new ForgetPasswordResponse();
			response.constructMessageRespnse(jsonStr);
		}
		return response;
	}
}