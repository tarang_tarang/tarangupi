package psp.mobile.model.response;

import org.json.JSONObject;

import psp.mobile.model.request.MobileNotification;

public class TxnNotification extends MobileNotification {
	
	public TxnNotification() {
	}
	
	public static TxnNotification constructNotification(JSONObject jsonObj){		
		TxnNotification response = null;
		if(jsonObj != null ) {	
			response = new TxnNotification();
			response.constructMobileNotificationDetails(jsonObj);
		}
		return response;
	}

}
