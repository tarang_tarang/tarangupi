package psp.mobile.model.response;

import psp.common.model.constants.ValidatorConstants;
import psp.common.model.util.MobileValidator;
import psp.constants.StatusCode;
import psp.mobile.model.request.CollectPaymentRequest;

@SuppressWarnings("serial")
public class CollectPaymentResponse extends MessageResponse implements ValidatorConstants {

	private String amountErrMsg;
	
	private String selfVirtualAddErrMsg;
	
	private String thirdPartyVirtualAddrErrMsg;
	
	private String durationInDaysErrMsg;
	
	public CollectPaymentResponse() {
	}
	
	public String getAmountErrMsg() {
		return amountErrMsg;
	}

	public void setAmountErrMsg(String amountErrMsg) {
		this.amountErrMsg = amountErrMsg;
	}

	public String getSelfVirtualAddErrMsg() {
		return selfVirtualAddErrMsg;
	}

	public void setSelfVirtualAddErrMsg(String selfVirtualAddErrMsg) {
		this.selfVirtualAddErrMsg = selfVirtualAddErrMsg;
	}

	public String getThirdPartyVirtualAddrErrMsg() {
		return thirdPartyVirtualAddrErrMsg;
	}

	public void setThirdPartyVirtualAddrErrMsg(String thirdPartyVirtualAddrErrMsg) {
		this.thirdPartyVirtualAddrErrMsg = thirdPartyVirtualAddrErrMsg;
	}

	public String getDurationInDaysErrMsg() {
		return durationInDaysErrMsg;
	}

	public void setDurationInDaysErrMsg(String durationInDaysErrMsg) {
		this.durationInDaysErrMsg = durationInDaysErrMsg;
	}
	
	public boolean validate(CollectPaymentRequest req) {
		boolean isSuccess = true;
		String errMsg = null;
		
		 errMsg = MobileValidator.validate(req.getThirdPartyVirtualAddr(), true, VIRTUAL_ADDR_MIN_LENGTH, VIRTUAL_ADDR_MAX_LENGTH,
				 VIRTUAL_ADDR_EXPRESSION, THIRD_PARTY_VIRTUAL_ADDR_MANDATORY_ERR_MSG, THIRD_PARTY_VIRTUAL_ADDR_LENGTH_ERR_MSG, THIRD_PARTY_VIRTUAL_ADDR_EXPRESSION_ERR_MSG);
		if( errMsg != null){
			setThirdPartyVirtualAddrErrMsg(errMsg);
			isSuccess = false;	
		}
		
		 errMsg = MobileValidator.validate(req.getSelfVirtualAddr(), VIRTUAL_ADDR_IS_MANDATORY, VIRTUAL_ADDR_MIN_LENGTH, VIRTUAL_ADDR_MAX_LENGTH,
				 VIRTUAL_ADDR_EXPRESSION, SELF_VIRTUAL_ADDR_MANDATORY_ERR_MSG, SELF_VIRTUAL_ADDR_LENGTH_ERR_MSG, SELF_VIRTUAL_ADDR_EXPRESSION_ERR_MSG);
		if( errMsg != null){
			setSelfVirtualAddErrMsg(errMsg);
			isSuccess = false;	
		}
		
		 errMsg = MobileValidator.validateAmount(String.valueOf(req.getAmount()), AMOUNT_IS_MANDATORY, AMOUNT_MIN_LENGTH, AMOUNT_MAX_LENGTH,
				 AMOUNT_EXPRESSION, AMOUNT_MANDATORY_ERR_MSG, AMOUNT_LENGTH_ERR_MSG, AMOUNT_EXPRESSION_ERR_MSG);
		if( errMsg != null){
			setAmountErrMsg(errMsg);
			isSuccess = false;
		}
		if (!isSuccess) {
			setStatusCode(StatusCode.FIELD_VALIDATION_FAILED.getCode());
			setStatusMessage(StatusCode.FIELD_VALIDATION_FAILED.getMessage());
		}
		return false;
	}
}
