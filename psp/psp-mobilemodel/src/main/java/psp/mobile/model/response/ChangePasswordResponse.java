package psp.mobile.model.response;

import org.json.JSONObject;

import psp.common.model.constants.ValidatorConstants;
import psp.common.model.util.MobileValidator;
import psp.constants.StatusCode;
import psp.mobile.model.request.ChangePasswordRequest;


@SuppressWarnings("serial")
public class ChangePasswordResponse extends MessageResponse implements ValidatorConstants{

	private String userNameErrMsg;
	
	private String oldPasswordErrMsg;
	
	private String newPasswordErrMsg;
	
	public ChangePasswordResponse() {
	}

	public String getUserNameErrMsg() {
		return userNameErrMsg;
	}

	public void setUserNameErrMsg(String userNameErrMsg) {
		this.userNameErrMsg = userNameErrMsg;
	}

	public String getOldPasswordErrMsg() {
		return oldPasswordErrMsg;
	}

	public void setOldPasswordErrMsg(String oldPasswordErrMsg) {
		this.oldPasswordErrMsg = oldPasswordErrMsg;
	}

	public String getNewPasswordErrMsg() {
		return newPasswordErrMsg;
	}

	public void setNewPasswordErrMsg(String newPasswordErrMsg) {
		this.newPasswordErrMsg = newPasswordErrMsg;
	}
    
	public boolean validate(ChangePasswordRequest req) {
		boolean isSuccess = true;
		
		String errMsg = MobileValidator.validate(req.getUserName(), USER_NAME_IS_MANDATORY, USER_NAME_MIN_LENGTH, USER_NAME_MAX_LENGTH,
				USER_NAME_EXPRESSION, USER_NAME_MANDATORY_ERR_MSG, USER_NAME_LENGTH_ERR_MSG, USER_NAME_EXPRESSION_ERR_MSG);
		if( errMsg != null){
			setUserNameErrMsg(errMsg);
			isSuccess = false;
		}
		
		errMsg = MobileValidator.validate(req.getOldPassword(), PASSWORD_IS_MANDATORY, PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH,
				PASSWORD_EXPRESSION, OLD_PASSWORD_MANDATORY_ERR_MSG, OLD_PASSWORD_LENGTH_ERR_MSG, OLD_PASSWORD_EXPRESSION_ERR_MSG);
		if( errMsg != null){
			setOldPasswordErrMsg(errMsg);
			isSuccess = false;
		}	
		
		errMsg = MobileValidator.validate(req.getNewPassword(), PASSWORD_IS_MANDATORY, PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH,
				PASSWORD_EXPRESSION, NEW_PASSWORD_MANDATORY_ERR_MSG, NEW_PASSWORD_LENGTH_ERR_MSG, NEW_PASSWORD_EXPRESSION_ERR_MSG);
		if( errMsg != null){
			setNewPasswordErrMsg(errMsg);
			isSuccess = false;
		}	
		if (!isSuccess) {
			 setStatusCode(StatusCode.FIELD_VALIDATION_FAILED.getCode());
			 setStatusMessage(StatusCode.FIELD_VALIDATION_FAILED.getMessage());
		}
		return isSuccess;
	}
	
	public static ChangePasswordResponse constructChangePasswordResponse(String jsonStr){		
		ChangePasswordResponse response = null;
		if(jsonStr != null && !"".equals(jsonStr)){	
			JSONObject jsonObj = new JSONObject(jsonStr);
			response = new ChangePasswordResponse();
			response.userNameErrMsg = jsonObj.optString("userNameErrMsg", null);
			response.oldPasswordErrMsg = jsonObj.optString("oldPasswordErrMsg", null);
			response.newPasswordErrMsg = jsonObj.optString("newPasswordErrMsg", null);
			response.constructMessageRespnse(jsonStr);
		}
		return response;
	}
}