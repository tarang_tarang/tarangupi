package psp.mobile.model.request;

import org.json.JSONObject;


@SuppressWarnings("serial")
public class RegistrationRequest extends MobileRequest {

	private String firstName;
	
	private String lastName;
	
	private String middleName;
	
	private String mobileNumber;
	
	private String adharNumber;
	
	private String email;

	private String password;
	
	private String rnsMpaId;
	
	public RegistrationRequest() {
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAdharNumber() {
		return adharNumber;
	}

	public void setAdharNumber(String adharNumber) {
		this.adharNumber = adharNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRnsMpaId() {
		return rnsMpaId;
	}

	public void setRnsMpaId(String rnsMpaId) {
		this.rnsMpaId = rnsMpaId;
	}

	public String toJsonString() {
		 JSONObject jobj = getJsonString();
		 jobj.put("firstName", firstName);
		 jobj.put("lastName", lastName);
		 jobj.put("middleName", middleName);
		 jobj.put("mobileNumber", mobileNumber);
		 jobj.put("adharNumber", adharNumber);
		 jobj.put("email", email);
		 jobj.put("password", password);
		 jobj.put("rnsMpaId", rnsMpaId);
		 return jobj.toString();
	}
}