package psp.mobile.model.request;

import org.json.JSONObject;


@SuppressWarnings("serial")
public class GetExistingAccountsRequest  extends MobileRequest{

	public GetExistingAccountsRequest() {
	}
	
	public String toJsonString(){
		JSONObject jobj = getJsonString();
		return jobj.toString();
	}
	
}