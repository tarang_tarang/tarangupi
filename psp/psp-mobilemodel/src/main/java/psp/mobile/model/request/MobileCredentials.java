package psp.mobile.model.request;

import org.json.JSONObject;


public class MobileCredentials {

	private String type;
	
	private String subType;
	
	private String dataCode;
	
	private String dataki;
	
	private String dataValue;

	public MobileCredentials() {
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public String getDataCode() {
		return dataCode;
	}

	public void setDataCode(String dataCode) {
		this.dataCode = dataCode;
	}

	public String getDataki() {
		return dataki;
	}

	public void setDataki(String dataki) {
		this.dataki = dataki;
	}

	public String getDataValue() {
		return dataValue;
	}

	public void setDataValue(String dataValue) {
		this.dataValue = dataValue;
	}

	public JSONObject toJsonString(){
		JSONObject jobj = new JSONObject();
		jobj.put("type", type);
		jobj.put("subType", subType);
		jobj.put("dataCode", dataCode);
		jobj.put("dataki", dataki);
		jobj.put("dataValue", dataValue);
		return jobj;
	}
	
}