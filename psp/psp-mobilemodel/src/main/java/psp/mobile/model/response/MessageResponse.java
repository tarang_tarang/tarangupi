/**
 * 
 */
package psp.mobile.model.response;

import java.io.Serializable;

import org.json.JSONObject;

/**
 * @author prasadj
 *
 */
public class MessageResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String statusCode;
	
	private String statusMessage;
	
	public MessageResponse(){
		this.statusCode = "200";
		this.statusMessage = "Process success";
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	
	protected void constructMessageRespnse(String jsonStr){
		final JSONObject jsonObj = new JSONObject(jsonStr);
		statusCode = jsonObj.optString("statusCode");
		statusMessage = jsonObj.optString("statusMessage", null);
	}
	
}