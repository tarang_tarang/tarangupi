package psp.mobile.model.request;

import org.json.JSONObject;


@SuppressWarnings("serial")
public class GeneratePinRequest extends MobileRequest {
	
	private MobileCredentials mobileCredentials;
	
	private String cardDigits;
	
	private String expiryDate; 
	
	public GeneratePinRequest() {
	}

	public MobileCredentials getMobileCredentials() {
		return mobileCredentials;
	}

	public void setMobileCredentials(MobileCredentials mobileCredentials) {
		this.mobileCredentials = mobileCredentials;
	}

	public String getCardDigits() {
		return cardDigits;
	}

	public void setCardDigits(String cardDigits) {
		this.cardDigits = cardDigits;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	public String toJsonString() {
		JSONObject jobj = getJsonString();
		if(mobileCredentials != null){
			jobj.put("mobileCredentials", mobileCredentials.toJsonString());
		}
		jobj.put("cardDigits", cardDigits);
		jobj.put("expiryDate", expiryDate);
		return jobj.toString();
	}

}