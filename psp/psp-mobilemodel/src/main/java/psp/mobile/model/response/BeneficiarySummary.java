package psp.mobile.model.response;

import org.json.JSONObject;

public class BeneficiarySummary {

	private Long benId;
	
	private String benName;
	
	private String benNickName;
	
	private String benVirtualAddr;
	
	public BeneficiarySummary(){		
	}

	public Long getBenId() {
		return benId;
	}

	public void setBenId(Long benId) {
		this.benId = benId;
	}

	public String getBenName() {
		return benName;
	}

	public void setBenName(String benName) {
		this.benName = benName;
	}

	public String getBenNickName() {
		return benNickName;
	}

	public void setBenNickName(String benNickName) {
		this.benNickName = benNickName;
	}

	public String getBenVirtualAddr() {
		return benVirtualAddr;
	}

	public void setBenVirtualAddr(String benVirtualAddr) {
		this.benVirtualAddr = benVirtualAddr;
	}	
	
	public static BeneficiarySummary constructBeneficiarySummary(String jsonStr){		
		BeneficiarySummary response = null;
		if(jsonStr != null && !"".equals(jsonStr)){	
			JSONObject jsonObj = new JSONObject(jsonStr);
			response = new BeneficiarySummary();
			response.benId = jsonObj.optLong("benId");
			response.benName = jsonObj.optString("benName", null);
			response.benNickName = jsonObj.optString("benNickName", null);
			response.benVirtualAddr = jsonObj.optString("benVirtualAddr", null);
		}
		return response;
	}	

	public static BeneficiarySummary constructBeneficiarySummary(JSONObject jsonObj){		
		BeneficiarySummary response = null;
		if(jsonObj != null ){				
			response = new BeneficiarySummary();			
			response.benId = jsonObj.optLong("benId");
			response.benName = jsonObj.optString("benName", null);
			response.benNickName = jsonObj.optString("benNickName", null);
			response.benVirtualAddr = jsonObj.optString("benVirtualAddr", null);			
		}
		return response;
	}
}