package psp.mobile.model.request;

import org.json.JSONObject;


@SuppressWarnings("serial")
public class ChangePasswordRequest extends MobileRequest {
	
	private String userName;
	
	private String oldPassword;
	
	private String newPassword;
	
	public ChangePasswordRequest() {
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	
	public String toJsonString() {
		 JSONObject jobj = getJsonString();
		 jobj.put("userName", userName);
		 jobj.put("oldPassword", oldPassword);
		 jobj.put("newPassword", newPassword);
		 return jobj.toString();
	}
	
}