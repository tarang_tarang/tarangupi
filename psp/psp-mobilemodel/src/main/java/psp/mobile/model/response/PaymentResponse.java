package psp.mobile.model.response;

import psp.common.model.constants.ValidatorConstants;
import psp.common.model.util.MobileValidator;
import psp.constants.StatusCode;
import psp.mobile.model.request.PaymentRequest;


@SuppressWarnings("serial")
public class PaymentResponse extends MessageResponse implements ValidatorConstants{

	private String mPinErrMsg;
	
	private String amountErrMsg;
	
	private String selfVirtualAddErrMsg;
	
	private String thirdPartyVirtualAddrErrMsg;
	
	public PaymentResponse() {
	}

	public String getmPinErrMsg() {
		return mPinErrMsg;
	}

	public void setmPinErrMsg(String mPinErrMsg) {
		this.mPinErrMsg = mPinErrMsg;
	}

	public String getAmountErrMsg() {
		return amountErrMsg;
	}

	public void setAmountErrMsg(String amountErrMsg) {
		this.amountErrMsg = amountErrMsg;
	}

	public String getSelfVirtualAddErrMsg() {
		return selfVirtualAddErrMsg;
	}

	public void setSelfVirtualAddErrMsg(String selfVirtualAddErrMsg) {
		this.selfVirtualAddErrMsg = selfVirtualAddErrMsg;
	}

	public String getThirdPartyVirtualAddrErrMsg() {
		return thirdPartyVirtualAddrErrMsg;
	}

	public void setThirdPartyVirtualAddrErrMsg(String thirdPartyVirtualAddrErrMsg) {
		this.thirdPartyVirtualAddrErrMsg = thirdPartyVirtualAddrErrMsg;
	}
	
	public boolean validate(PaymentRequest req) {
		boolean isSuccess = true;
		
		String errMsg = null;
		//TODO update validation ex. "Manas@tarang"
		 errMsg = MobileValidator.validate(req.getThirdPartyVirtualAddr(), false, VIRTUAL_ADDR_MIN_LENGTH, VIRTUAL_ADDR_MAX_LENGTH,
				 VIRTUAL_ADDR_EXPRESSION, THIRD_PARTY_VIRTUAL_ADDR_MANDATORY_ERR_MSG, THIRD_PARTY_VIRTUAL_ADDR_LENGTH_ERR_MSG, THIRD_PARTY_VIRTUAL_ADDR_EXPRESSION_ERR_MSG);
		if( errMsg != null){
			setThirdPartyVirtualAddrErrMsg(errMsg);
			isSuccess = false;	
		}
		
		 errMsg = MobileValidator.validate(req.getSelfVirtualAddr(), VIRTUAL_ADDR_IS_MANDATORY, VIRTUAL_ADDR_MIN_LENGTH, VIRTUAL_ADDR_MAX_LENGTH,
				 VIRTUAL_ADDR_EXPRESSION, SELF_VIRTUAL_ADDR_MANDATORY_ERR_MSG, SELF_VIRTUAL_ADDR_LENGTH_ERR_MSG, SELF_VIRTUAL_ADDR_EXPRESSION_ERR_MSG);
		if( errMsg != null){
			setSelfVirtualAddErrMsg(errMsg);
			isSuccess = false;	
		}
		
		 errMsg = MobileValidator.validateAmount(String.valueOf(req.getAmount()), AMOUNT_IS_MANDATORY, AMOUNT_MIN_LENGTH, AMOUNT_MAX_LENGTH,
				 AMOUNT_EXPRESSION, AMOUNT_MANDATORY_ERR_MSG, AMOUNT_LENGTH_ERR_MSG, AMOUNT_EXPRESSION_ERR_MSG);
		if( errMsg != null){
			setAmountErrMsg(errMsg);
			isSuccess = false;
		}
		
		if (!isSuccess) {
			setStatusCode(StatusCode.FIELD_VALIDATION_FAILED.getCode());
			setStatusMessage(StatusCode.FIELD_VALIDATION_FAILED.getMessage()); 
		}
		return isSuccess;
	}
}