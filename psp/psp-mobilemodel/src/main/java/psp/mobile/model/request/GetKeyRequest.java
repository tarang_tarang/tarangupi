package psp.mobile.model.request;

import org.json.JSONObject;


@SuppressWarnings("serial")
public class GetKeyRequest extends MobileRequest {

	public GetKeyRequest(){		
	}
	
	public String toJsonString(){
		JSONObject jobj = getJsonString();
		return jobj.toString();
	}
	
}