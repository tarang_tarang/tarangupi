package psp.mobile.model.request;

import org.json.JSONObject;


@SuppressWarnings("serial")
public class CheckUserRequest extends MobileRequest {
	
	private String mobileNum;
	
	public CheckUserRequest(){		
	}

	public String getMobileNum() {
		return mobileNum;
	}

	public void setMobileNum(String mobileNum) {
		this.mobileNum = mobileNum;
	}

	public String toJsonString() {
		 JSONObject jobj = getJsonString();
		 jobj.put("mobileNum", mobileNum);
		 return jobj.toString();
	}
	
}