package psp.mobile.model.request;

import org.json.JSONObject;


@SuppressWarnings("serial")
public class GetAccountProvidersRequest extends MobileRequest {

	public GetAccountProvidersRequest() {
	}

	public String toJsonString(){
		JSONObject jobj = getJsonString();
		return jobj.toString();
	}
	
}