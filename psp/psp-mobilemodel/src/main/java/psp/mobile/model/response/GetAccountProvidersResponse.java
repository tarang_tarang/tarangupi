package psp.mobile.model.response;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import psp.common.model.BankSummary;
import psp.constants.StatusCode;

@SuppressWarnings("serial")
public class GetAccountProvidersResponse extends MessageResponse {

	private List<BankSummary> listOfBanks;
	 
	public GetAccountProvidersResponse() {
	}

	public List<BankSummary> getListOfBanks() {
		return listOfBanks;
	}

	public void setListOfBanks(List<BankSummary> listOfBanks) {
		this.listOfBanks = listOfBanks;
	}
	
	public boolean validate() { 
		boolean isSuccess = true;
		
		if (!isSuccess) {
			 setStatusCode(StatusCode.FIELD_VALIDATION_FAILED.getCode());
			 setStatusMessage(StatusCode.FIELD_VALIDATION_FAILED.getMessage());
		}
		return isSuccess;
	}
	public static GetAccountProvidersResponse constructGetAccountProvidersResponse(String jsonStr){		
		GetAccountProvidersResponse response = null;
		if(jsonStr != null && !"".equals(jsonStr)){	
			JSONObject jsonObj = new JSONObject(jsonStr);
			response = new GetAccountProvidersResponse();
			JSONArray listOfBanks = jsonObj.getJSONArray("listOfBanks");
			if(listOfBanks != null){
				List<BankSummary> bankSummaryList = new ArrayList<>();			
				for(int i = 0; i < listOfBanks.length(); i++){
					JSONObject bankSummary = listOfBanks.getJSONObject(i);
					BankSummary bank = BankSummary.constructBankSummary(bankSummary);
					bankSummaryList.add(i, bank);
				}			
				response.listOfBanks = bankSummaryList;
			}
			response.constructMessageRespnse(jsonStr);
		}
		return response;
	}
	
}