/**
 * 
 */
package psp.mobile.model.request;

import org.json.JSONObject;

/**
 * @author prasadj
 *
 */
public class MobileDevice {

	private String mobile;
	
	private String geoCode;
	
	private String location;
	
	private String ip;
	
	private String type;
	
	private String terminalId;
	
	private String os;
	
	private String app;
	
	private String capability;
	
	public MobileDevice(){
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getGeoCode() {
		return geoCode;
	}

	public void setGeoCode(String geoCode) {
		this.geoCode = geoCode;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getApp() {
		return app;
	}

	public void setApp(String app) {
		this.app = app;
	}

	public String getCapability() {
		return capability;
	}

	public void setCapability(String capability) {
		this.capability = capability;
	}

	public JSONObject toJsonString(){
		JSONObject jobj = new JSONObject();
		jobj.put("mobile", mobile);
		jobj.put("geoCode", geoCode);
		jobj.put("location", location);
		jobj.put("ip", ip);
		jobj.put("type", type);
		jobj.put("terminalId", terminalId);
		jobj.put("os", os);
		jobj.put("app", app);
		jobj.put("capability", capability);
		return jobj;
	}
}