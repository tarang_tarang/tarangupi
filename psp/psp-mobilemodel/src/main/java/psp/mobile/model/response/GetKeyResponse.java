package psp.mobile.model.response;

import psp.constants.StatusCode;


@SuppressWarnings("serial")
public class GetKeyResponse extends MessageResponse {

	private String code;
	
	private String ki;
	
	private String owner;
	
	private String type;
	
	private String publicKey;
	
	public GetKeyResponse(){		
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getKi() {
		return ki;
	}

	public void setKi(String ki) {
		this.ki = ki;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}	

	public boolean validate() {
		boolean isSuccess = true;
		
		if (!isSuccess) {
			 setStatusCode(StatusCode.FIELD_VALIDATION_FAILED.getCode());
			 setStatusMessage(StatusCode.FIELD_VALIDATION_FAILED.getMessage());
		}
		return isSuccess;
	}
	/*
	public static GetKeyResponse constructGetKeyResponse(String jsonStr){		
		GetKeyResponse response = null;
		if(jsonStr != null && !"".equals(jsonStr)){	
			JSONObject jsonObj = new JSONObject(jsonStr);
			response = new GetKeyResponse();
			response.constructMessageRespnse(jsonStr);
			response.code = jsonObj.optString("code", null);
			response.ki = jsonObj.optString("ki", null);
			response.owner = jsonObj.optString("owner", null);
			response.type = jsonObj.optString("type", null);
			response.publicKey = jsonObj.optString("publicKey", null);
		}
		return response;
	}*/
}