package psp.mobile.model.request;

import org.json.JSONObject;


@SuppressWarnings("serial")
public class AddBeneficiaryRequest extends MobileRequest {
	
	private Long id;
	
	private String type;
	
	private String address;
	
	private String nickName;
	
	private String beneficiaryNote;
	
	public AddBeneficiaryRequest() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getBeneficiaryNote() {
		return beneficiaryNote;
	}

	public void setBeneficiaryNote(String beneficiaryNote) {
		this.beneficiaryNote = beneficiaryNote;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public String toJsonString() {
		 JSONObject jobj = getJsonString();
		 jobj.put("id", id);
		 jobj.put("type", type);
		 jobj.put("address", address);
		 jobj.put("nickName", nickName);
		 jobj.put("beneficiaryNote", beneficiaryNote);
		 return jobj.toString();
	}
	
}