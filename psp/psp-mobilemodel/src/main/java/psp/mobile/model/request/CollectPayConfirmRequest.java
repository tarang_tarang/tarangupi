package psp.mobile.model.request;

import org.json.JSONObject;

@SuppressWarnings("serial")
public class CollectPayConfirmRequest extends MobileRequest {

	private MobileCredentials mobileCredentials;
	
	private String note;
	
	private boolean rejected;
	
	private String notificationId;
		
	public CollectPayConfirmRequest() {
	}

	public MobileCredentials getMobileCredentials() {
		return mobileCredentials;
	}

	public void setMobileCredentials(MobileCredentials mobileCredentials) {
		this.mobileCredentials = mobileCredentials;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public boolean getRejected() {
		return rejected;
	}

	public void setRejected(boolean rejected) {
		this.rejected = rejected;
	}

	public String getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(String notificationId) {
		this.notificationId = notificationId;
	}

	public String toJsonString(){
		JSONObject jobj = getJsonString();
		jobj.put("mobileCredentials", mobileCredentials.toJsonString());
		jobj.put("note", note);
		jobj.put("rejected", rejected);
		jobj.put("notificationId", notificationId);
		return jobj.toString();
	}
}
