/**
 * 
 */
package psp.mobile.model.response;

import java.util.Date;

/**
 * @author manasp
 *
 */
public class TxnSummary {

	private String paymentFrom;
	
	private String paymentTo;
	
	private Double amount;
	
	private Date transactionDate;
	
	private String status;
	
	private String txnMessage;

	public String getPaymentFrom() {
		return paymentFrom;
	}

	public void setPaymentFrom(String paymentFrom) {
		this.paymentFrom = paymentFrom;
	}

	public String getPaymentTo() {
		return paymentTo;
	}

	public void setPaymentTo(String paymentTo) {
		this.paymentTo = paymentTo;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTxnMessage() {
		return txnMessage;
	}

	public void setTxnMessage(String txnMessage) {
		this.txnMessage = txnMessage;
	}
	
}
