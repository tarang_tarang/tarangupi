package psp.mobile.model.response;

import org.json.JSONObject;

import psp.constants.StatusCode;


@SuppressWarnings("serial")
public class BalanceEnquiryResponse extends MessageResponse {

	private String balance;
	
	private String mPinErrMsg;
	
	public BalanceEnquiryResponse() {
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getmPinErrMsg() {
		return mPinErrMsg;
	}

	public void setmPinErrMsg(String mPinErrMsg) {
		this.mPinErrMsg = mPinErrMsg;
	}
	
	public boolean validate() {
		boolean isSuccess = true;
		
		if (!isSuccess) {
			 setStatusCode(StatusCode.FIELD_VALIDATION_FAILED.getCode());
			 setStatusMessage(StatusCode.FIELD_VALIDATION_FAILED.getMessage());
		}
		return isSuccess;
	}
	
	public static BalanceEnquiryResponse constructBalanceEnquiryResponse(String jsonStr){		
		BalanceEnquiryResponse response = null;
		if(jsonStr != null && !"".equals(jsonStr)){	
			JSONObject jsonObj = new JSONObject(jsonStr);
			response = new BalanceEnquiryResponse();
			response.constructMessageRespnse(jsonStr);
			response.balance = jsonObj.optString("balance", null);
			response.mPinErrMsg = jsonObj.optString("mPinErrMsg", null);
		}
		return response;
	}
}