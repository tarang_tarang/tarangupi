package psp.mobile.model.response;

import psp.constants.StatusCode;

import org.json.JSONObject;

@SuppressWarnings("serial")
public class AddBankAccountResponse extends MessageResponse {

	private String virtualAddress;
	
	public AddBankAccountResponse() {
	}

	public String getVirtualAddress() {
		return virtualAddress;
	}

	public void setVirtualAddress(String virtualAddress) {
		this.virtualAddress = virtualAddress;
	}

	public boolean validate() {
		boolean isSuccess = true;
		
		if (!isSuccess) {
			setStatusCode(StatusCode.FIELD_VALIDATION_FAILED.getCode());
			setStatusMessage(StatusCode.FIELD_VALIDATION_FAILED.getMessage());
		}
		
		return false;
	}
	
	public static AddBankAccountResponse constructAddBankAccountResponse(String jsonStr){		
		AddBankAccountResponse response = null;
		if(jsonStr != null && !"".equals(jsonStr)){	
			JSONObject jsonObj = new JSONObject(jsonStr);
			response = new AddBankAccountResponse();
			response.constructMessageRespnse(jsonStr);
			response.virtualAddress = jsonObj.optString("virtualAddress", null);
		}
		return response;
	}
}