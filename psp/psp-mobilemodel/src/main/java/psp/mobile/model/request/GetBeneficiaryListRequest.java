package psp.mobile.model.request;

import org.json.JSONObject;


@SuppressWarnings("serial")
public class GetBeneficiaryListRequest extends MobileRequest {

	private String requestType;
	
	public GetBeneficiaryListRequest() {
	}
	
	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String toJsonString(){
		JSONObject jobj = getJsonString();
		return jobj.toString();
	}

}