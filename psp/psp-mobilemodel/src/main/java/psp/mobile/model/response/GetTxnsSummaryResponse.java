package psp.mobile.model.response;

import java.util.List;

import psp.common.model.constants.ValidatorConstants;
import psp.common.model.util.MobileValidator;
import psp.constants.StatusCode;
import psp.mobile.model.request.GetTxnDetailsRequest;

@SuppressWarnings("serial")
public class GetTxnsSummaryResponse extends MessageResponse implements ValidatorConstants{

	private List<TxnSummary> txnSummaryList;
	
	private String fromDateErrMsg;
	
	private String toDateErrMsg;

	public GetTxnsSummaryResponse() {
	}

	public String getFromDateErrMsg() {
		return fromDateErrMsg;
	}

	public void setFromDateErrMsg(String fromDateErrMsg) {
		this.fromDateErrMsg = fromDateErrMsg;
	}

	public String getToDateErrMsg() {
		return toDateErrMsg;
	}

	
	public void setToDateErrMsg(String toDateErrMsg) {
		this.toDateErrMsg = toDateErrMsg;
	}

	public List<TxnSummary> getTxnSummaryList() {
		return txnSummaryList;
	}

	public void setTxnSummaryList(List<TxnSummary> txnSummaryList) {
		this.txnSummaryList = txnSummaryList;
	}

	public boolean validate(GetTxnDetailsRequest req) {
		boolean isSuccess = true;
		
		String errMsg = MobileValidator.validate(req.getFromDate(), DATE_IS_MANDATORY, DATE_MIN_LENGTH, DATE_MAX_LENGTH,
				DATE_EXPRESSION, FROM_DATE_MANDATORY_ERR_MSG, FROM_DATE_LENGTH_ERR_MSG, FROM_DATE_EXPRESSION_ERR_MSG);
		if( errMsg != null){
			setFromDateErrMsg(errMsg);
			isSuccess = false;
		}
		
		errMsg = MobileValidator.validate(req.getFromDate(), DATE_IS_MANDATORY, DATE_MIN_LENGTH, DATE_MAX_LENGTH,
				DATE_EXPRESSION, TO_DATE_MANDATORY_ERR_MSG, TO_DATE_LENGTH_ERR_MSG, TO_DATE_EXPRESSION_ERR_MSG);
		if( errMsg != null){
			setToDateErrMsg(errMsg);
			isSuccess = false;
		}
		
		if (!isSuccess) {
			setStatusCode(StatusCode.FIELD_VALIDATION_FAILED.getCode());
			setStatusMessage(StatusCode.FIELD_VALIDATION_FAILED.getMessage());
		}
		return isSuccess;
	}
}