package psp.mobile.model.response;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import psp.constants.StatusCode;
import psp.mobile.model.request.MobileNotification;

@SuppressWarnings("serial")
public class GetNotificationListResponse extends MessageResponse {

	private List<MobileNotification> notifications;
	
	public GetNotificationListResponse() {
	}
	
	public List<MobileNotification> getNotifications() {
		return notifications;
	}
	
	public void setNotifications(List<MobileNotification> notifications) {
		this.notifications = notifications;
	}
	
	public boolean validate() {
		boolean isSuccess = true;
		if (!isSuccess) {
			setStatusCode(StatusCode.FIELD_VALIDATION_FAILED.getCode());
			setStatusMessage(StatusCode.FIELD_VALIDATION_FAILED.getMessage());
		}
		return false;
	}
	
	public static GetNotificationListResponse constructGetNotificationListResponse(String jsonStr){		
		GetNotificationListResponse response = null;
		if(jsonStr != null && !"".equals(jsonStr)){	
			JSONObject jsonObj = new JSONObject(jsonStr);
			response = new GetNotificationListResponse();
			response.constructMessageRespnse(jsonStr);
			JSONArray notifications = jsonObj.getJSONArray("notifications");
			List<MobileNotification> notificationList = new ArrayList<>();		
			MobileNotification mobileNotification = null;
			 for(int i =0; i < notifications.length(); i++) {
				 JSONObject notification = notifications.getJSONObject(i);
				 mobileNotification = new MobileNotification();
				 mobileNotification.constructMobileNotificationDetails(notification);
				 notificationList.add(i, mobileNotification);
			 }			
			response.notifications = notificationList;
		}
		return response;
	}
}
