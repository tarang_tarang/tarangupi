package psp.mobile.model.request;

import org.json.JSONObject;

@SuppressWarnings("serial")
public class GetNotificationDetailsRequest extends MobileRequest {

	private String notificationId;
	
	public GetNotificationDetailsRequest() {
	}

	public String getNotificationId() {
		return notificationId;
	}
	
	public void setNotificationId(String notificationId) {
		this.notificationId = notificationId;
	}


	public String toJsonString(){
		JSONObject jobj = getJsonString();
		jobj.put("notificationId", notificationId);
		return jobj.toString();
	}
	
}
