package psp.mobile.model.request;

import org.json.JSONObject;

public class MobileNotification {

	private String notificationId;
	
	private String notificationtType;
	
	private String notificationMessage;
	
	public MobileNotification() {
	}

	public String getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(String notificationId) {
		this.notificationId = notificationId;
	}

	public String getNotificationtType() {
		return notificationtType;
	}

	public void setNotificationtType(String notificationtType) {
		this.notificationtType = notificationtType;
	}

	public String getNotificationMessage() {
		return notificationMessage;
	}

	public void setNotificationMessage(String notificationMessage) {
		this.notificationMessage = notificationMessage;
	}
	
	public void constructMobileNotificationDetails(JSONObject jsonObj){		
		this.notificationId = jsonObj.optString("notificationId");
		this.notificationtType = jsonObj.optString("notificationtType", null);
		this.notificationMessage = jsonObj.optString("notificationMessage", null);
	}

}
