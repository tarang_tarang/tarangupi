package psp.mobile.model.response;

import psp.common.model.constants.ValidatorConstants;
import psp.common.model.util.MobileValidator;
import psp.constants.StatusCode;
import psp.mobile.model.request.RegistrationRequest;

@SuppressWarnings("serial")
public class RegistrationResponse extends MessageResponse implements ValidatorConstants{

	private String firstNameErrMsg;
	
	private String lastNameErrMsg;
	
	private String middleNameErrMsg;
	
	private String mobileNumberErrMsg;
	
	private String adharNumberErrMsg;
	
	private String emailErrMsg;
	
	private String userNameErrMsg;
	
	private String passwordErrMsg;
	
	private String xmlPayload;
	
	public RegistrationResponse() {
	}

	public String getFirstNameErrMsg() {
		return firstNameErrMsg;
	}

	public void setFirstNameErrMsg(String firstNameErrMsg) {
		this.firstNameErrMsg = firstNameErrMsg;
	}

	public String getLastNameErrMsg() {
		return lastNameErrMsg;
	}

	public void setLastNameErrMsg(String lastNameErrMsg) {
		this.lastNameErrMsg = lastNameErrMsg;
	}

	public String getMiddleNameErrMsg() {
		return middleNameErrMsg;
	}

	public void setMiddleNameErrMsg(String middleNameErrMsg) {
		this.middleNameErrMsg = middleNameErrMsg;
	}

	public String getMobileNumberErrMsg() {
		return mobileNumberErrMsg;
	}

	public void setMobileNumberErrMsg(String mobileNumberErrMsg) {
		this.mobileNumberErrMsg = mobileNumberErrMsg;
	}

	public String getAdharNumberErrMsg() {
		return adharNumberErrMsg;
	}

	public void setAdharNumberErrMsg(String adharNumberErrMsg) {
		this.adharNumberErrMsg = adharNumberErrMsg;
	}

	public String getEmailErrMsg() {
		return emailErrMsg;
	}

	public void setEmailErrMsg(String emailErrMsg) {
		this.emailErrMsg = emailErrMsg;
	}

	public String getUserNameErrMsg() {
		return userNameErrMsg;
	}

	public void setUserNameErrMsg(String userNameErrMsg) {
		this.userNameErrMsg = userNameErrMsg;
	}

	public String getPasswordErrMsg() {
		return passwordErrMsg;
	}

	public void setPasswordErrMsg(String passwordErrMsg) {
		this.passwordErrMsg = passwordErrMsg;
	}
	
	public String getXmlPayload() {
		return xmlPayload;
	}

	public void setXmlPayload(String xmlPayload) {
		this.xmlPayload = xmlPayload;
	}

	public boolean validate(RegistrationRequest req) {
		boolean isSuccess = true;

		String errMsg = MobileValidator.validate(req.getFirstName(), FIRST_NAME_IS_MANDATORY, FIRST_NAME_MIN_LENGTH, FIRST_NAME_MAX_LENGTH,
				FIRST_NAME_EXPRESSION, FIRST_NAME_MANDATORY_ERR_MSG, FIRST_NAME_LENGTH_ERR_MSG, FIRST_NAME_EXPRESSION_ERR_MSG);
		if( errMsg != null){
			setFirstNameErrMsg(errMsg);
			isSuccess = false;
		}
		
		errMsg = MobileValidator.validate(req.getMiddleName(), MIDDLE_NAME_IS_MANDATORY, MIDDLE_NAME_MIN_LENGTH, MIDDLE_NAME_MAX_LENGTH,
				MIDDLE_NAME_EXPRESSION, MIDDLE_NAME_MANDATORY_ERR_MSG, MIDDLE_NAME_LENGTH_ERR_MSG, MIDDLE_NAME_EXPRESSION_ERR_MSG);
		if( errMsg != null){
			setMiddleNameErrMsg(errMsg);
			isSuccess = false;
		}
		
		errMsg = MobileValidator.validate(req.getLastName(), LAST_NAME_IS_MANDATORY, LAST_NAME_MIN_LENGTH, LAST_NAME_MAX_LENGTH,
				LAST_NAME_EXPRESSION, LAST_NAME_MANDATORY_ERR_MSG, LAST_NAME_LENGTH_ERR_MSG, LAST_NAME_EXPRESSION_ERR_MSG);
		if( errMsg != null){
			setLastNameErrMsg(errMsg);
			isSuccess = false;
		}
		
		errMsg = MobileValidator.validate(req.getMobileNumber(), MOBILE_NUMBER_IS_MANDATORY, MOBILE_NUMBER_MIN_LENGTH, MOBILE_NUMBER_MAX_LENGTH,
				MOBILE_NUMBER_EXPRESSION, MOBILE_NUMBER_MANDATORY_ERR_MSG, MOBILE_NUMBER_LENGTH_ERR_MSG, MOBILE_NUMBER_EXPRESSION_ERR_MSG);
		if( errMsg != null){
			setMobileNumberErrMsg(errMsg);
			isSuccess = false;
		}
		
		errMsg = MobileValidator.validate(req.getAdharNumber(), AADHAR_NUMBER_IS_MANDATORY, AADHAR_NUMBER_MIN_LENGTH, AADHAR_NUMBER_MAX_LENGTH,
				AADHAR_NUMBER_EXPRESSION, AADHAR_NUMBER_MANDATORY_ERR_MSG, AADHAR_NUMBER_LENGTH_ERR_MSG, AADHAR_NUMBER_EXPRESSION_ERR_MSG);
		if( errMsg != null){
			setAdharNumberErrMsg(errMsg);
			isSuccess = false;
		}
		
		errMsg = MobileValidator.validate(req.getEmail(), EMAIL_IS_MANDATORY, EMAIL_MIN_LENGTH, EMAIL_MAX_LENGTH,
				EMAIL_EXPRESSION, EMAIL_MANDATORY_ERR_MSG, EMAIL_LENGTH_ERR_MSG, EMAIL_EXPRESSION_ERR_MSG);
		if( errMsg != null){
			setEmailErrMsg(errMsg);
			isSuccess = false;
		}
		
		errMsg = MobileValidator.validate(req.getUserName(), USER_NAME_IS_MANDATORY, USER_NAME_MIN_LENGTH, USER_NAME_MAX_LENGTH,
				USER_NAME_EXPRESSION, USER_NAME_MANDATORY_ERR_MSG, USER_NAME_LENGTH_ERR_MSG, USER_NAME_EXPRESSION_ERR_MSG);
		if( errMsg != null){
			setUserNameErrMsg(errMsg);
			isSuccess = false;
		}
		
		errMsg = MobileValidator.validate(req.getPassword(), PASSWORD_IS_MANDATORY, PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH,
				PASSWORD_EXPRESSION, PASSWORD_MANDATORY_ERR_MSG, PASSWORD_LENGTH_ERR_MSG, PASSWORD_EXPRESSION_ERR_MSG);
		if( errMsg != null){
			setPasswordErrMsg(errMsg);
			isSuccess = false;
		}
		
		if (!isSuccess) {
			setStatusCode(StatusCode.FIELD_VALIDATION_FAILED.getCode());
			setStatusMessage(StatusCode.FIELD_VALIDATION_FAILED.getMessage()); 
		}
		return isSuccess;
	}
}