package psp.mobile.model.request;

import org.json.JSONObject;


@SuppressWarnings("serial")
public class AddBankAccountRequest extends MobileRequest {

	private Long id;
	
	public AddBankAccountRequest(){
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String toJsonString() {
		 JSONObject jobj = getJsonString();
		 jobj.put("id", id);
		 return jobj.toString();
	}
	
}