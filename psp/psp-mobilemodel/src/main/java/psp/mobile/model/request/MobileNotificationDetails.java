package psp.mobile.model.request;

import org.json.JSONObject;

public class MobileNotificationDetails {

	private String notificationId;
	
	private String notificationtType;
	
	private String notificationMessage;
	
	public MobileNotificationDetails() {
	}

	public String getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(String notificationId) {
		this.notificationId = notificationId;
	}

	public String getNotificationtType() {
		return notificationtType;
	}

	public void setNotificationtType(String notificationtType) {
		this.notificationtType = notificationtType;
	}

	public String getNotificationMessage() {
		return notificationMessage;
	}

	public void setNotificationMessage(String notificationMessage) {
		this.notificationMessage = notificationMessage;
	}
	
	public static MobileNotificationDetails constructMobileNotificationDetails(JSONObject jsonObj){		
		MobileNotificationDetails response = null;
		if(jsonObj != null ) {	
			response = new MobileNotificationDetails();
			response.notificationId = jsonObj.optString("notificationId");
			response.notificationtType = jsonObj.optString("notificationtType", null);
			response.notificationMessage = jsonObj.optString("notificationMessage", null);
		}
		return response;
	}

}
