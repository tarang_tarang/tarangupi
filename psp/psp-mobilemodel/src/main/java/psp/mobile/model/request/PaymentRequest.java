package psp.mobile.model.request;

import org.json.JSONObject;


@SuppressWarnings("serial")
public class PaymentRequest extends MobileRequest {

	private Double amount;
	
	private String selfVirtualAddr;
	
	private String thirdPartyVirtualAddr;
	
	private MobileCredentials mobileCredentials;
	
	private String nickName;
	
	private String type;
	
	private String txnNote;
	
	public PaymentRequest() {
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getSelfVirtualAddr() {
		return selfVirtualAddr;
	}

	public void setSelfVirtualAddr(String selfVirtualAddr) {
		this.selfVirtualAddr = selfVirtualAddr;
	}

	public String getThirdPartyVirtualAddr() {
		return thirdPartyVirtualAddr;
	}

	public void setThirdPartyVirtualAddr(String thirdPartyVirtualAddr) {
		this.thirdPartyVirtualAddr = thirdPartyVirtualAddr;
	}

	public MobileCredentials getMobileCredentials() {
		return mobileCredentials;
	}

	public void setMobileCredentials(MobileCredentials mobileCredentials) {
		this.mobileCredentials = mobileCredentials;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String getTxnNote() {
		return txnNote;
	}

	public void setTxnNote(String txnNote) {
		this.txnNote = txnNote;
	}

	public String toJsonString() {
		 JSONObject jobj = getJsonString();
		 jobj.put("amount", amount);
		 jobj.put("selfVirtualAddr", selfVirtualAddr);
		 jobj.put("thirdPartyVirtualAddr", thirdPartyVirtualAddr);
		 if(mobileCredentials != null) {
			 jobj.put("mobileCredentials", mobileCredentials.toJsonString());
		 }
		 jobj.put("nickName", nickName);
		 jobj.put("type", type);
		 jobj.put("txnNote", txnNote);
		 return jobj.toString();
	}
	
}