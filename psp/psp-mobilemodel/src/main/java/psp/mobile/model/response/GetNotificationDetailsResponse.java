package psp.mobile.model.response;

import org.json.JSONObject;

import psp.constants.NotificationType;
import psp.constants.StatusCode;
import psp.mobile.model.request.MobileNotification;

@SuppressWarnings("serial")
public class GetNotificationDetailsResponse extends MessageResponse {
	
	private MobileNotification notification;
	
	public GetNotificationDetailsResponse() {
	}
	
	public MobileNotification getNotification() {
		return notification;
	}

	public void setNotification(MobileNotification notification) {
		this.notification = notification;
	}

	public boolean validate() {
		boolean isSuccess = true;
		if (!isSuccess) {
			setStatusCode(StatusCode.FIELD_VALIDATION_FAILED.getCode());
			setStatusMessage(StatusCode.FIELD_VALIDATION_FAILED.getMessage());
		}
		return false;
	}
	
	public static GetNotificationDetailsResponse constructGetNotificationDetailsResponse(String jsonStr){		
		GetNotificationDetailsResponse response = null;
		if(jsonStr != null && !"".equals(jsonStr)){	
			JSONObject jsonObj = new JSONObject(jsonStr);
			response = new GetNotificationDetailsResponse();
			response.constructMessageRespnse(jsonStr);
			JSONObject notificationJson = new JSONObject(jsonObj.optString("notification"));
			if(NotificationType.COLLECT.name().equals(notificationJson.get("notificationtType"))) {
				response.notification = CollectPayNotification.constructNotification(jsonObj);
			}
			else if(NotificationType.TXN.name().equals(notificationJson.get("notificationtType"))) {
				response.notification = TxnNotification.constructNotification(jsonObj);
			}
		}
		return response;
	}
}
