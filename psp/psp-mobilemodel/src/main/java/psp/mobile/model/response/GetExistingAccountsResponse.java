package psp.mobile.model.response;

import org.json.JSONObject;

import psp.common.model.AccountSummary;
import psp.constants.StatusCode;

@SuppressWarnings("serial")
public class GetExistingAccountsResponse extends MessageResponse {

	private AccountSummary accountSummary;

	public GetExistingAccountsResponse() {
	}

	public AccountSummary getAccountSummary() {
		return accountSummary;
	}

	public void setAccountSummary(AccountSummary accountSummary) {
		this.accountSummary = accountSummary;
	}
	
	public boolean validate() {
		boolean isSuccess = true;

		if (!isSuccess) {
			setStatusCode(StatusCode.FIELD_VALIDATION_FAILED.getCode());
			setStatusMessage(StatusCode.FIELD_VALIDATION_FAILED.getMessage());
		}
		return isSuccess;
	}
	
	public static GetExistingAccountsResponse constructGetExistingAccountsResponse(String jsonStr){	
		GetExistingAccountsResponse response = null;
		if(jsonStr != null && !"".equals(jsonStr)){	
			JSONObject jsonObj = new JSONObject(jsonStr);
			response = new GetExistingAccountsResponse();
			response.constructMessageRespnse(jsonStr);			
			response.accountSummary =AccountSummary.constructAccountSummary(jsonObj.getJSONObject("accountSummary"));
			
		}
		return response;
	}
}