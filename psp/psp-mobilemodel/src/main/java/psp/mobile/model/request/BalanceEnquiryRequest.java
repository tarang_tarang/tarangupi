package psp.mobile.model.request;

import org.json.JSONObject;


@SuppressWarnings("serial")
public class BalanceEnquiryRequest extends MobileRequest {

	private AcDetails  acDetails;
	
	private MobileCredentials mobileCredentials;
	
	public BalanceEnquiryRequest() {
	}

	public AcDetails getAcDetails() {
		return acDetails;
	}

	public void setAcDetails(AcDetails acDetails) {
		this.acDetails = acDetails;
	}

	public MobileCredentials getMobileCredentials() {
		return mobileCredentials;
	}

	public void setMobileCredentials(MobileCredentials mobileCredentials) {
		this.mobileCredentials = mobileCredentials;
	}

	public String toJsonString() {
		 JSONObject jobj = getJsonString();
		 if(acDetails != null) {
			 jobj.put("acDetails", acDetails.toJsonString());
		 }
		 if(mobileCredentials != null) {
			 jobj.put("mobileCredentials", mobileCredentials.toJsonString());
		 }
		 return jobj.toString();
	}
	
}