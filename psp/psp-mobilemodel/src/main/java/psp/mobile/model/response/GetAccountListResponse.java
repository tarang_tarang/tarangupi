package psp.mobile.model.response;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.json.JSONArray;

import psp.common.model.AccountSummary;
import psp.constants.StatusCode;

@SuppressWarnings("serial")
public class GetAccountListResponse extends MessageResponse {

	private String otpErrMsg;
	
	private String mmidErrMsg;
	
	private List<AccountSummary> accountSummaryList;
	
	public GetAccountListResponse() {
	}

	public List<AccountSummary> getAccountSummaryList() {
		return accountSummaryList;
	}

	public void setAccountSummaryList(List<AccountSummary> accountSummaryList) {
		this.accountSummaryList = accountSummaryList;
	}

	public String getOtpErrMsg() {
		return otpErrMsg;
	}

	public void setOtpErrMsg(String otpErrMsg) {
		this.otpErrMsg = otpErrMsg;
	}

	public String getMmidErrMsg() {
		return mmidErrMsg;
	}

	public void setMmidErrMsg(String mmidErrMsg) {
		this.mmidErrMsg = mmidErrMsg;
	}
	
	public boolean validate() {
		boolean isSuccess = true;
		
		if (!isSuccess) {
			 setStatusCode(StatusCode.FIELD_VALIDATION_FAILED.getCode());
			 setStatusMessage(StatusCode.FIELD_VALIDATION_FAILED.getMessage()); 
		}
		return isSuccess;
		
	}
	
	public static GetAccountListResponse constructGetAccountListResponse(String jsonStr){		
		GetAccountListResponse response = null;
		if(jsonStr != null && !"".equals(jsonStr)){	
			JSONObject jsonObj = new JSONObject(jsonStr);
			response = new GetAccountListResponse();			
			response.otpErrMsg = jsonObj.optString("otpErrMsg", null);
			response.mmidErrMsg = jsonObj.optString("mmidErrMsg", null);
			JSONArray actSummaryList = jsonObj.getJSONArray("accountSummaryList");
			List<AccountSummary> accountSummaryList = new ArrayList<>();			
			 for(int i =0; i < actSummaryList.length(); i++){
				 JSONObject accountSummary = actSummaryList.getJSONObject(i);
				 AccountSummary actSummary = AccountSummary.constructAccountSummary(accountSummary);
				 accountSummaryList.add(i, actSummary);
			 }			
			response.accountSummaryList = accountSummaryList;
			 response.constructMessageRespnse(jsonStr);
		}
		return response;
	}
}