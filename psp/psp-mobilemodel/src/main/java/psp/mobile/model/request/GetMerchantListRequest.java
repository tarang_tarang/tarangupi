package psp.mobile.model.request;

import org.json.JSONObject;


@SuppressWarnings("serial")
public class GetMerchantListRequest extends MobileRequest {

	public GetMerchantListRequest() {
	}
	
	public String toJsonString(){
		JSONObject jobj = getJsonString();
		return jobj.toString();
	}
	
}