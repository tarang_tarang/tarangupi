package psp.upi.constants;

public enum IdentityType {

	PAN,
	AADHAAR,
	ACCOUNT;
	
	public static IdentityType getIdentityType(String idenType){
		if(idenType == null){
			return null;
		}
		else if (PAN.name().equals(idenType)){
			return PAN;
		}
		else if (AADHAAR.name().equals(idenType)){
			return AADHAAR;
		}
		else if (ACCOUNT.name().equals(idenType)){
			return ACCOUNT;
		}		
		else {
			return null;
		}
	}
}