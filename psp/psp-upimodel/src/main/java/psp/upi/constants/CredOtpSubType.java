package psp.upi.constants;

public enum CredOtpSubType {

	SMS,
	EMAIL,
	HOTP,
	TOTP;
	
	public static CredOtpSubType getCredOtpSubType(String otpSubType){
		if(otpSubType == null){
			return null;
		}
		else if (SMS.name().equals(otpSubType)){
			return SMS;
		}
		else if (EMAIL.name().equals(otpSubType)){
			return EMAIL;
		}
		else if (HOTP.name().equals(otpSubType)){
			return HOTP;
		}	
		else if (TOTP.name().equals(otpSubType)){
			return TOTP;
		}
		else {
			return null;
		}
	}
	
}