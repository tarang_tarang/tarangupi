package psp.upi.constants;

public enum HbtMsgType {

	EOD,
	ALIVE;
	
	public static HbtMsgType getHbtMsgType(String hbtMessage){
		if(hbtMessage == null){
			return null;
		}
		else if (EOD.name().equals(hbtMessage)){
			return EOD;
		}
		else if (ALIVE.name().equals(hbtMessage)){
			return ALIVE;
		}		
		else {
			return null;
		}
	}
}