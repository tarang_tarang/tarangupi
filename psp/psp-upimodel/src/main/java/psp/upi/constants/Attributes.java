package psp.upi.constants;

public interface Attributes {

	String NAME = "name";
	
	String VER = "ver";
	
	String TS = "ts";
	
	String ORG_ID = "orgId";
	
	String MSG_ID = "msgId";
	
	String VALUE = "value";
	
	String ID = "id";
	
	String NOTE = "note";
	
	String REF_ID = "refId";
	
	String REF_URL = "refUrl";
	
	String TYPE = "type";
	
	String ORG_TXN_ID = "orgTxnId";
	
	String PROVIDER = "provider";
	
	String ADDR = "addr";
	
	String SEQ_NUM = "seqNum";
	
	String CODE = "code";
	
	String VERIFIED_NAME = "verifiedName";
	
	String WHITE_LISTED = "whiteListed";
	
	String ADDR_TYPE = "addrType";
	
	String SUB_TYPE = "subtype";
	
	String CURR = "curr";
	
	String RESP_CODE = "respCode";
	
	String APPROVAL_REF = "approvalRef";
	
	String REQ_MSG_ID = "reqMsgID";
	
	String RESULT = "result";
	
	String ERROR_CODE = "errCode";
	
	String SETT_AMOUNT = "settAmount";
	
	String SETT_CURRENCY = "settCurrency";
	
	String APPROVAL_NUM = "approvalNum";
	
	String CODES = "codes";
	
	String ACTIVE = "active";
	
	String URL = "url";
	
	String SPOC_NAME = "spocName";
	
	String SPOC_EMAIL = "spocEmail";
	
	String SPOC_PHONE = "spocPhone";
	
	String LAST_MODIFIED_TS = "lastModifiedTs";
	
	String IIN = "iin";
	
	String PRODS = "prods";
	
	String OWNER = "owner";
	
	String KI = "ki";
	
	String NUMBER = "number";
	
	String IFSC = "ifsc";
	
	String MMID = "mmid";
	
	String AEBA = "aeba";
	
	String LOGO = "logo";
	
	String OP = "op";
	
	String MASK_NAME = "maskName";	
	
	String VERIFIED_ADDR = "verifiedAddress";
	
	String ACCT_NUM = "acNum";
	
	String TIME_STAMP = "timeStamp";
	
	String MASK_NUMBER = "maskedAccNumber";
	
}