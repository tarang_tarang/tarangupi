package psp.upi.constants;

public enum CredAadharSubType {
	
	IIR,
	FMR,
	FIR,
	OTP;
	
	public static CredAadharSubType getCredAadharSubType(String aadharSubType){
		if(aadharSubType == null){
			return null;
		}
		else if (IIR.name().equals(aadharSubType)){
			return IIR;
		}
		else if (FMR.name().equals(aadharSubType)){
			return FMR;
		}
		else if (FIR.name().equals(aadharSubType)){
			return FIR;
		}	
		else if (OTP.name().equals(aadharSubType)){
			return OTP;
		}
		else {
			return null;
		}
	}
	
}