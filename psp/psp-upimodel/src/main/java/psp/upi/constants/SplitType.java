package psp.upi.constants;

public enum SplitType {

	PURCHASE,
	CASHBACK;
	
	public static SplitType getSplitType(String split){
		if(split == null){
			return null;
		}
		else if (PURCHASE.name().equals(split)){
			return PURCHASE;
		}
		else if (CASHBACK.name().equals(split)){
			return CASHBACK;
		}		
		else {
			return null;
		}
	}
}