package psp.upi.constants;

public enum CredCardSubType {

	CVV1,
	CVV2,
	EMV;
	
	public static CredCardSubType getCredCardSubType(String cardsubType){
		if(cardsubType == null){
			return null;
		}
		else if (CVV1.name().equals(cardsubType)){
			return CVV1;
		}
		else if (CVV2.name().equals(cardsubType)){
			return CVV2;
		}
		else if (EMV.name().equals(cardsubType)){
			return EMV;
		}		
		else {
			return null;
		}
	}
	
}