package psp.upi.constants;

public enum AccountType {

	SAVING,
	CURRENT,
	DEFAULT;	
	
	public static AccountType getAccountType(String actType){
		if(actType == null){
			return null;
		}
		else if (SAVING.name().equals(actType)){
			return SAVING;
		}
		else if (CURRENT.name().equals(actType)){
			return CURRENT;
		}
		else if (DEFAULT.name().equals(actType)){
			return DEFAULT;
		}		
		else {
			return null;
		}
	}
	
}