package psp.upi.constants;

public enum PayerType {

	PERSON,
	ENTITY;
	
	public static PayerType getPayerType(String payerType){
		if(payerType == null){
			return null;
		}
		else if (PERSON.name().equals(payerType)){
			return PERSON;
		}
		else if (ENTITY.name().equals(payerType)){
			return ENTITY;
		}		
		else {
			return null;
		}
	}
}