package psp.upi.constants;

public enum AccPvdProdType {

	AEPS,
	IMPS,
	CARD,
	NFS;
	
	public static AccPvdProdType getAccPvdProdType(String actPvtProdType){
		if(actPvtProdType == null){
			return null;
		}
		else if (AEPS.name().equals(actPvtProdType)){
			return AEPS;
		}
		else if (IMPS.name().equals(actPvtProdType)){
			return IMPS;
		}
		else if (CARD.name().equals(actPvtProdType)){
			return CARD;
		}	
		else if (NFS.name().equals(actPvtProdType)){
			return NFS;
		}
		else {
			return null;
		}
	}
}