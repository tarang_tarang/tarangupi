package psp.upi.constants;

public enum OwnerType {

	NPCI,
	UIDAI;
	
	public static OwnerType getOwnerType(String owner){
		if(owner == null){
			return null;
		}
		else if (NPCI.name().equals(owner)){
			return NPCI;
		}
		else if (UIDAI.name().equals(owner)){
			return UIDAI;
		}		
		else {
			return null;
		}
	}
}