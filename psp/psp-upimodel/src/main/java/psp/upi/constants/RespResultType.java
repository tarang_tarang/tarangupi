package psp.upi.constants;

public enum RespResultType {

	SUCCESS,
	FAILURE,
	PARTIAL,
	DEEMED,
	PENDING;
	
	public static RespResultType getRespResultType(String resp){
		if(resp == null){
			return null;
		}
		else if (SUCCESS.name().equals(resp)){
			return SUCCESS;
		}
		else if (FAILURE.name().equals(resp)){
			return FAILURE;
		}	
		else if (PARTIAL.name().equals(resp)){
			return PARTIAL;
		}
		else if (DEEMED.name().equals(resp)){
			return DEEMED;
		}
		else if (DEEMED.name().equals(resp)){
			return DEEMED;
		}
		else if (PENDING.name().equals(resp)){
			return PENDING;
		}
		else {
			return null;
		}
	}
}