package psp.upi.constants;

public enum TxnType {
	
	PAY, 	
	COLLECT,
	DEBIT,
	CREDIT,
	REVERSAL,
	REFUND;
	
	public static TxnType getTxnType(String txn){
		if(txn == null){
			return null;
		}
		else if (PAY.name().equals(txn)){
			return PAY;
		}
		else if (COLLECT.name().equals(txn)){
			return COLLECT;
		}	
		else if (DEBIT.name().equals(txn)){
			return DEBIT;
		}
		else if (CREDIT.name().equals(txn)){
			return CREDIT;
		}
		else if (REVERSAL.name().equals(txn)){
			return REVERSAL;
		}
		else if (REFUND.name().equals(txn)){
			return REFUND;
		}
		else {
			return null;
		}
	}
}