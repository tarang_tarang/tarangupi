package psp.upi.constants;

public interface Elements {

	String HEAD = "Head";
	
	String META = "Meta";
	
	String TAG = "Tag";
	
	String TXN = "Txn";
	
	String RISK_SCORES = "RiskScores";
	
	String SCORE = "Score";
	
	String RULES = "Rules";
	
	String RULE = "Rule";
	
	String PAYER = "Payer";
	
	String INFO = "Info";
	
	String IDENTITY = "Identity";
	
	String RATING = "Rating";
	
	String DEVICE = "Device";
	
	String AC = "Ac";
	
	String DETAIL = "Detail";
	
	String CREDS = "Creds";
	
	String CRED = "Cred";
	
	String DATA = "Data";
	
	String AMOUNT = "Amount";
	
	String SPLIT = "Split";
	
	String PRE_APPROVED = "PreApproved";
	
	String PAYEES = "Payees";
	
	String PAYEE = "Payee";
	
	String RESP = "Resp";
	
	String REF = "Ref";
	
	String PSP_LIST = "PspList";
	
	String PSP = "Psp";
	
	String ACC_PVD_LIST = "AccPvdList";
	
	String ACC_PVD = "AccPvd";
	
	String KEY_LIST = "KeyList";
	
	String KEY = "Key";
	
	String KEY_VALUE = "KeyValue";
	
	String ACCOUNT_LIST = "AccountList";
	
	String ACCOUNT = "Account";
	
	String VAE_LIST = "VaeList";
	
	String VAE = "Vae";
	
	String NEW_CRED = "NewCred";
	
	String BAL = "Bal";
	
	String HBT_MSG = "HbtMsg";
	
	String REQ_MSG = "ReqMsg";
	
	String TXN_CONFIRMATION = "TxnConfirmation";
	
	String REG_DETAILS = "RegDetails";
	
	String LINK = "Link";
	
	String PEN_TXN = "PenTxn";
	
	String RESP_MSG = "RespMsg";
	
}