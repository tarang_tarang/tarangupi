/**
 * 
 */
package psp.upi.constants;

/**
 * @author prasadj
 *
 */
public enum CredType {

	AADHAAR,
	OTP,
	PIN,
	CARD;
	
	public static CredType getCredType(String ct){
		if(ct == null){
			return null;
		}
		else if (AADHAAR.name().equals(ct)){
			return AADHAAR;
		}		
		else if (OTP.name().equals(ct)){
			return OTP;
		}		
		else if (PIN.name().equals(ct)){
			return PIN;
		}		
		else if (CARD.name().equals(ct)){
			return CARD;
		}		
		else {
			return null;
		}
	}
	
}