package psp.upi.constants;

public interface SampleData {

	String SCHEMA_NAME = "http://npci.org/upi/schema/";

	String MOBILE = "MOBILE";

	String GEOCODE = "GEOCODE";

	String LOCATION = "LOCATION";

	String DEVICE_TYPE = "TYPE";

	String OS = "OS";

	String APP = "APP";

	String CAPABILITY = "CAPABILITY";

	String CURR = "INR";

	String RISKSCORES_TYPE = "TXNRISK";

	String AEBA = "Y";
	
	String IFSC = "AACA0010012";
	
	String ACC_NUM = "XXXXXXXXXXXX1234";
	
	String MMID = "AACA012";
	
	String BEN_IFSC = "AACA0010013";
	
	String BEN_ACC_NUM = "XXXXXXXXXXXX4321";
	
	String BEN_MMID = "AACA013";
	
	String CRED_TYPE = "PIN";
	
	String CRED_SUB_TYPE = "MPIN";
	
	String CRED_DATA_TYPE = "NUM";
	
	Integer CRED_DATA_LENGTH = 6;
	
}