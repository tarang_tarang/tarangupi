package psp.upi.constants;

public interface PropertyOrder {

	String ACCT_REF_NUM = "accRefNumber";
	
	String MASKED_ACCT_NUM = "maskedAccnumber";
	
	String CURRENCY = "currency";
	
	String SPLIT = "split";
	
	String VERSION = "version";
	
	String TIMESTAMP = "timeStamp";
	
	String ORG_ID = "organizationId";
	
	String DATA = "data";
	
	String ADDRESS = "address";
	
	String INFO = "info";
	
	String DEVICE = "device";
	
	String AC = "ac";
	
	String AMOUNT = "amount";
	
	String CREDENTIALS = "credentials";
	
	String PRE_APPROVED = "preApproved";
	
	String BALANCE = "balance";
	
	String LINK = "link";
	
	String REQ_MSG_ID = "reqMsgId";
	
	String REF = "reference";
	
	String VAE_LIST = "verifiedAddressEntryList";
	
	String RISK_SCORES = "riskScores";
	
	String RULES = "rules";
}
