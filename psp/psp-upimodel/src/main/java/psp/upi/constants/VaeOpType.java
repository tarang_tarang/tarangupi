package psp.upi.constants;

public enum VaeOpType {

	UPDATE,
	REMOVE;
	
	public static VaeOpType getVaeOpType(String vae){
		if(vae == null){
			return null;
		}
		else if (UPDATE.name().equals(vae)){
			return UPDATE;
		}
		else if (REMOVE.name().equals(vae)){
			return REMOVE;
		}		
		else {
			return null;
		}
	}
}