package psp.upi.constants;

public enum CredPinSubType {

	MPIN;
	
	public static CredPinSubType getCredPinSubType(String pinSubType){
		if(pinSubType == null){
			return null;
		}
		else if (MPIN.name().equals(pinSubType)){
			return MPIN;
		}		
		else {
			return null;
		}
	}
	
}