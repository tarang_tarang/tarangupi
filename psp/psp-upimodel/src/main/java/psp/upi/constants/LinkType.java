package psp.upi.constants;

public enum LinkType {

	MOBILE,
	AADHAAR;
	
	public static LinkType getLinkType(String lnkType){
		if(lnkType == null){
			return null;
		}
		else if (MOBILE.name().equals(lnkType)){
			return MOBILE;
		}
		else if (AADHAAR.name().equals(lnkType)){
			return AADHAAR;
		}		
		else {
			return null;
		}
	}
}