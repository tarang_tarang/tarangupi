package psp.upi.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

import psp.upi.constants.Attributes;
import psp.upi.constants.PropertyOrder;

@XmlType(propOrder = { PropertyOrder.ACCT_REF_NUM, Attributes.MMID,
		Attributes.MASK_NUMBER, Attributes.IFSC,
		Attributes.NAME, Attributes.AEBA })
public class Account implements Attributes {

	private String accRefNumber;

	private Integer mmid;

	private String maskedAccNumber; // TODO Need to get the attribute name from
									// spec

	private String ifsc;

	private String name;

	private String aeba;

	public Account() {
	}

	public String getAccRefNumber() {
		return accRefNumber;
	}

	@XmlAttribute(name = NUMBER)
	public void setAccRefNumber(String accRefNumber) {
		this.accRefNumber = accRefNumber;
	}

	public Integer getMmid() {
		return mmid;
	}

	@XmlAttribute(name = MMID)
	public void setMmid(Integer mmid) {
		this.mmid = mmid;
	}
	
	public String getIfsc() {
		return ifsc;
	}

	@XmlAttribute (name = MASK_NUMBER)
	public String getMaskedAccNumber() {
		return maskedAccNumber;
	}

	public void setMaskedAccNumber(String maskedAccNumber) {
		this.maskedAccNumber = maskedAccNumber;
	}

	@XmlAttribute(name = IFSC)
	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	public String getName() {
		return name;
	}

	@XmlAttribute(name = NAME)
	public void setName(String name) {
		this.name = name;
	}

	public String getAeba() {
		return aeba;
	}

	@XmlAttribute(name = AEBA)
	public void setAeba(String aeba) {
		this.aeba = aeba;
	}

}
