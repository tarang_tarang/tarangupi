package psp.upi.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import psp.upi.constants.Elements;

public class AccountList {

	private List<Account> accountList;

	public AccountList() {
	}

	public List<Account> getAccountList() {
		return accountList;
	}

	@XmlElement(name = Elements.ACCOUNT)
	public void setAccountList(List<Account> accountList) {
		this.accountList = accountList;
	}

}
