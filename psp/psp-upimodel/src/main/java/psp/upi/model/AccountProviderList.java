package psp.upi.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import psp.upi.constants.Elements;

public class AccountProviderList {

	private List<AccountProvider> accountProvider;//TODO but in document 1..1

	public AccountProviderList() {
	}

	public List<AccountProvider> getAccountProvider() {
		return accountProvider;
	}

	@XmlElement(name = Elements.ACC_PVD)
	public void setAccountProvider(List<AccountProvider> accountProvider) {
		this.accountProvider = accountProvider;
	}

}
