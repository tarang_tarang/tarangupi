/**
 * 
 */
package psp.upi.model;

import javax.xml.bind.annotation.XmlAttribute;

import psp.upi.constants.Attributes;

/**
 * @author manasp
 * 
 */
public class PreApproved {

	private String respCode;

	private String approvalRef;

	public PreApproved() {
	}

	public String getRespCode() {
		return respCode;
	}

	@XmlAttribute(name = Attributes.RESP_CODE)
	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}

	public String getApprovalRef() {
		return approvalRef;
	}

	@XmlAttribute(name = Attributes.APPROVAL_REF)
	public void setApprovalRef(String approvalRef) {
		this.approvalRef = approvalRef;
	}

}
