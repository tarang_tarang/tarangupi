package psp.upi.model;

import javax.xml.bind.annotation.XmlAttribute;

import psp.upi.constants.Attributes;

public class HeartbeatMessage {

	private String type;

	private String value;

	public HeartbeatMessage() {
	}

	public String getType() {
		return type;
	}

	@XmlAttribute(name = Attributes.TYPE)
	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	@XmlAttribute(name = Attributes.VALUE)
	public void setValue(String value) {
		this.value = value;
	}

}
