/**
 * 
 */
package psp.upi.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import psp.upi.constants.Attributes;
import psp.upi.constants.Elements;
import psp.upi.constants.PropertyOrder;

/**
 * @author manasp
 * 
 */
@XmlType(propOrder = { PropertyOrder.ADDRESS, Attributes.NAME,
		Attributes.SEQ_NUM, Attributes.TYPE,
		Attributes.CODE, PropertyOrder.INFO,
		PropertyOrder.DEVICE, PropertyOrder.AC, PropertyOrder.CREDENTIALS,
		PropertyOrder.AMOUNT, PropertyOrder.PRE_APPROVED, 
		PropertyOrder.BALANCE, PropertyOrder.LINK })
public class Payer implements Attributes, Elements {
	
	private String address;

	private String name;

	private String seqNum;

	private String type;

	private String code;

	private Info info;

	private Device device;

	private Ac ac;

	private Credentials credentials;

	private Amount amount;

	private PreApproved preApproved;

	private Balance balance;

	private Link link;

	public Payer() {
	}

	public String getAddress() {
		return address;
	}

	@XmlAttribute(name = ADDR)
	public void setAddress(String address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	@XmlAttribute(name = NAME)
	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	@XmlAttribute(name = TYPE)
	public void setType(String type) {
		this.type = type;
	}

	public Info getInfo() {
		return info;
	}

	@XmlElement(name = INFO)
	public void setInfo(Info info) {
		this.info = info;
	}

	public Device getDevice() {
		return device;
	}

	@XmlElement(name = DEVICE)
	public void setDevice(Device device) {
		this.device = device;
	}

	public Ac getAc() {
		return ac;
	}

	@XmlElement(name = AC)
	public void setAc(Ac ac) {
		this.ac = ac;
	}

	public Credentials getCredentials() {
		return credentials;
	}

	@XmlElement(name = CREDS)
	public void setCredentials(Credentials credentials) {
		this.credentials = credentials;
	}

	public PreApproved getPreApproved() {
		return preApproved;
	}

	@XmlElement(name = PRE_APPROVED)
	public void setPreApproved(PreApproved preApproved) {
		this.preApproved = preApproved;
	}

	public Balance getBalance() {
		return balance;
	}

	@XmlElement(name = BAL)
	public void setBalance(Balance balance) {
		this.balance = balance;
	}

	public Link getLink() {
		return link;
	}

	@XmlElement(name = LINK)
	public void setLink(Link link) {
		this.link = link;
	}

	public String getSeqNum() {
		return seqNum;
	}

	@XmlAttribute(name = SEQ_NUM)
	public void setSeqNum(String seqNum) {
		this.seqNum = seqNum;
	}

	public String getCode() {
		return code;
	}

	@XmlAttribute(name = CODE)
	public void setCode(String code) {
		this.code = code;
	}

	public Amount getAmount() {
		return amount;
	}
	
	@XmlElement(name = AMOUNT)
	public void setAmount(Amount amount) {
		this.amount = amount;
	}

}