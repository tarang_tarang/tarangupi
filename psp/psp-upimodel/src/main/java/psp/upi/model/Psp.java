/**
 * 
 */
package psp.upi.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;

import psp.upi.constants.Attributes;

/**
 * @author manasp
 * 
 */
/*@XmlType(propOrder = { Attributes.NAME, Attributes.CODES,
		Attributes.ACTIVE, Attributes.URL,
		Attributes.SPOC_NAME, Attributes.SPOC_EMAIL,
		Attributes.SPOC_PHONE,
		Attributes.LAST_MODIFIED_TS })*/
public class Psp implements Attributes {

	private String name;

	private String codes;

	private String active;

	private String url;

	private String spocName;

	private String spocEmail;

	private String spocPhone;

	private Date lastModifedTs;

	public Psp() {
	}

	public String getName() {
		return name;
	}

	@XmlAttribute(name = NAME)
	public void setName(String name) {
		this.name = name;
	}

	public String getCodes() {
		return codes;
	}

	@XmlAttribute(name = CODES)
	public void setCodes(String codes) {
		this.codes = codes;
	}

	public String getActive() {
		return active;
	}

	@XmlAttribute(name = ACTIVE)
	public void setActive(String active) {
		this.active = active;
	}

	public String getUrl() {
		return url;
	}

	@XmlAttribute(name = URL)
	public void setUrl(String url) {
		this.url = url;
	}

	public String getSpocName() {
		return spocName;
	}

	@XmlAttribute(name = SPOC_NAME)
	public void setSpocName(String spocName) {
		this.spocName = spocName;
	}

	public String getSpocEmail() {
		return spocEmail;
	}

	@XmlAttribute(name = SPOC_EMAIL)
	public void setSpocEmail(String spocEmail) {
		this.spocEmail = spocEmail;
	}

	public String getSpocPhone() {
		return spocPhone;
	}

	@XmlAttribute(name = SPOC_PHONE)
	public void setSpocPhone(String spocPhone) {
		this.spocPhone = spocPhone;
	}

	public Date getLastModifedTs() {
		return lastModifedTs;
	}

	@XmlAttribute(name = LAST_MODIFIED_TS)
	public void setLastModifedTs(Date lastModifedTs) {
		this.lastModifedTs = lastModifedTs;
	}

}
