package psp.upi.model;

import javax.xml.bind.annotation.XmlAttribute;

import psp.upi.constants.Attributes;

/*@XmlType(propOrder = { Attributes.NAME, Attributes.ADDR,
		"urls", Attributes.OP, "logos", Attributes.MASK_NAME })*/
public class VerifiedAddressEntry implements Attributes {

	private String name;

	private String addr;

	private String url;

	private String op;

	private String logo; 

	private String maskName;

	public VerifiedAddressEntry() {
	}

	public String getName() {
		return name;
	}

	@XmlAttribute(name = NAME)
	public void setName(String name) {
		this.name = name;
	}

	public String getAddr() {
		return addr;
	}

	@XmlAttribute(name = ADDR)
	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getUrls() {
		return url;
	}

	@XmlAttribute(name = URL)
	public void setUrl(String url) {
		this.url = url;
	}

	public String getOp() {
		return op;
	}

	@XmlAttribute(name = OP)
	public void setOp(String op) {
		this.op = op;
	}

	public String getLogo() {
		return logo;
	}

	@XmlAttribute(name = LOGO)
	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getMaskName() {
		return maskName;
	}

	@XmlAttribute(name = MASK_NAME)
	public void setMaskName(String maskName) {
		this.maskName = maskName;
	}

}