/**
 * 
 */
package psp.upi.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import psp.upi.constants.Elements;

/**
 * @author manasp
 * 
 */
public class PspList {

	private List<Psp> psps;//TODO but in document 1..1

	public PspList() {

	}

	public List<Psp> getPsps() {
		return psps;
	}

	@XmlElement(name = Elements.PSP)
	public void setPsps(List<Psp> paymentSystemPlayerInfos) {
		this.psps = paymentSystemPlayerInfos;
	}

}
