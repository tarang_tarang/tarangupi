/**
 * 
 */
package psp.upi.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import psp.upi.constants.Attributes;
import psp.upi.constants.Elements;

/**
 * @author manasp
 * 
 */
public class Ac {

	private String addrType;

	private List<Detail> details;

	public Ac() {
	}

	public String getAddrType() {
		return addrType;
	}

	@XmlAttribute(name = Attributes.ADDR_TYPE)
	public void setAddrType(String addrType) {
		this.addrType = addrType;
	}

	public List<Detail> getDetails() {
		return details;
	}

	@XmlElement(name = Elements.DETAIL)
	public void setDetails(List<Detail> details) {
		this.details = details;
	}

}
