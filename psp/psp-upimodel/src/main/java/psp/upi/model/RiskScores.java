/**
 * 
 */
package psp.upi.model;

import javax.xml.bind.annotation.XmlElement;

import psp.upi.constants.Elements;

/**
 * @author manasp
 * 
 */
public class RiskScores {

	private Score score;

	public RiskScores() {

	}

	public Score getScore() {
		return score;
	}

	@XmlElement(name = Elements.SCORE)
	public void setScore(Score score) {
		this.score = score;
	}

}