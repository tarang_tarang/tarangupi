/**
 * 
 */
package psp.upi.model;

import javax.xml.bind.annotation.XmlAttribute;

import psp.upi.constants.Attributes;

/**
 * @author manasp
 * 
 */
public class Rule {

	private String name;

	private String value;

	public Rule() {
	}

	public String getName() {
		return name;
	}

	@XmlAttribute(name = Attributes.NAME)
	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	@XmlAttribute(name = Attributes.VALUE)
	public void setValue(String value) {
		this.value = value;
	}

}
