/**
 * 
 */
package psp.upi.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

import psp.upi.constants.Attributes;
import psp.upi.constants.PropertyOrder;

/**
 * @author manasp
 * 
 */
@XmlType(propOrder = { Attributes.TYPE,
		Attributes.SEQ_NUM, PropertyOrder.ADDRESS,
		Attributes.SETT_AMOUNT, Attributes.SETT_CURRENCY,
		Attributes.APPROVAL_NUM, Attributes.RESP_CODE,
		Attributes.ACCT_NUM })
public class Reference implements Attributes {

	private String type;

	private Integer seqNum;

	private String address;

	private String settAmount;

	private String settCurrency;

	private String approvalNum;

	private String respCode;

	private String acNum;// TODO

	public Reference() {
	}

	public String getType() {
		return type;
	}

	@XmlAttribute(name = TYPE)
	public void setType(String type) {
		this.type = type;
	}

	public Integer getSeqNum() {
		return seqNum;
	}

	@XmlAttribute(name = SEQ_NUM)
	public void setSeqNum(Integer seqNum) {
		this.seqNum = seqNum;
	}

	public String getAddress() {
		return address;
	}

	@XmlAttribute(name = ADDR)
	public void setAddress(String address) {
		this.address = address;
	}

	public String getSettAmount() {
		return settAmount;
	}

	@XmlAttribute(name = SETT_AMOUNT)
	public void setSettAmount(String settAmount) {
		this.settAmount = settAmount;
	}

	public String getSettCurrency() {
		return settCurrency;
	}

	@XmlAttribute(name = SETT_CURRENCY)
	public void setSettCurrency(String settCurrency) {
		this.settCurrency = settCurrency;
	}

	public String getApprovalNum() {
		return approvalNum;
	}

	@XmlAttribute(name = APPROVAL_NUM)
	public void setApprovalNum(String approvalNum) {
		this.approvalNum = approvalNum;
	}

	public String getRespCode() {
		return respCode;
	}

	@XmlAttribute(name = RESP_CODE)
	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}

	public String getAcNum() {
		return acNum;
	}

	@XmlAttribute(name = ACCT_NUM)
	public void setAcNum(String acNum) {
		this.acNum = acNum;
	}

}
