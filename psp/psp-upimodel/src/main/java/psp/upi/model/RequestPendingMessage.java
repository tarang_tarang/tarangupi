package psp.upi.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

import psp.upi.constants.Attributes;

@XmlType(propOrder = { Attributes.TYPE, Attributes.VALUE,
		Attributes.ADDR })
public class RequestPendingMessage {

	private String type;

	private String value; // TODO No attribute specified in specification

	private String addr;

	public RequestPendingMessage() {
	}

	public String getType() {
		return type;
	}

	@XmlAttribute(name = Attributes.TYPE)
	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	@XmlAttribute(name = Attributes.VALUE)
	public void setValue(String value) {
		this.value = value;
	}

	public String getAddr() {
		return addr;
	}

	@XmlAttribute(name = Attributes.ADDR)
	public void setAddr(String addr) {
		this.addr = addr;
	}

}
