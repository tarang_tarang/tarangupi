package psp.upi.model;

import javax.xml.bind.annotation.XmlElement;

import psp.upi.constants.Elements;

public class Info {

	private Identity identity;

	private Rating rating;

	public Info() {
	}

	public Identity getIdentity() {
		return identity;
	}

	@XmlElement(name = Elements.IDENTITY)
	public void setIdentity(Identity identity) {
		this.identity = identity;
	}

	public Rating getRating() {
		return rating;
	}

	@XmlElement(name = Elements.RATING)
	public void setRating(Rating rating) {
		this.rating = rating;
	}

}
