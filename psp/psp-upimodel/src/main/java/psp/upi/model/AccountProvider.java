package psp.upi.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;

import psp.upi.constants.Attributes;

/*@XmlType(propOrder = { Attributes.NAME, Attributes.IIN,
		Attributes.ACTIVE, Attributes.SPOC_NAME, Attributes.URL,
		Attributes.SPOC_EMAIL, Attributes.SPOC_PHONE,
		Attributes.PRODS, Attributes.LAST_MODIFIED_TS })*/
public class AccountProvider implements Attributes {

	private String name;

	private Integer iin;

	private String active;

	private String url;

	private String spocName;

	private String spocEmail;

	private String spocPhone;

	private String prods;

	private Date lastModifedTs;

	public AccountProvider() {
	}

	public String getName() {
		return name;
	}

	@XmlAttribute(name = NAME)
	public void setName(String name) {
		this.name = name;
	}

	public Integer getIin() {
		return iin;
	}

	@XmlAttribute(name = IIN)
	public void setIin(Integer iin) {
		this.iin = iin;
	}

	public String getActive() {
		return active;
	}

	@XmlAttribute(name = ACTIVE)
	public void setActive(String active) {
		this.active = active;
	}

	public String getUrl() {
		return url;
	}

	@XmlAttribute(name = URL)
	public void setUrl(String url) {
		this.url = url;
	}

	public String getSpocName() {
		return spocName;
	}

	@XmlAttribute(name = SPOC_NAME)
	public void setSpocName(String spocName) {
		this.spocName = spocName;
	}

	public String getSpocEmail() {
		return spocEmail;
	}

	@XmlAttribute(name = SPOC_EMAIL)
	public void setSpocEmail(String spocEmail) {
		this.spocEmail = spocEmail;
	}

	public String getSpocPhone() {
		return spocPhone;
	}

	@XmlAttribute(name = SPOC_PHONE)
	public void setSpocPhone(String spocPhone) {
		this.spocPhone = spocPhone;
	}

	public String getProds() {
		return prods;
	}

	@XmlAttribute(name = PRODS)
	public void setProds(String prods) {
		this.prods = prods;
	}

	public Date getLastModifedTs() {
		return lastModifedTs;
	}

	@XmlAttribute(name = LAST_MODIFIED_TS)
	public void setLastModifedTs(Date lastModifedTs) {
		this.lastModifedTs = lastModifedTs;
	}

}
