/**
 * 
 */
package psp.upi.model;

import javax.xml.bind.annotation.XmlAttribute;

import psp.upi.constants.Attributes;

/**
 * @author manasp
 * 
 */
public class Tag implements Attributes {

	private String name;

	private String value;

	public Tag() {
	}

	public String getName() {
		return name;
	}

	@XmlAttribute(name = NAME)
	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	@XmlAttribute(name = VALUE)
	public void setValue(String value) {
		this.value = value;
	}

}
