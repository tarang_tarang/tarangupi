package psp.upi.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

import psp.upi.constants.Attributes;
import psp.upi.constants.PropertyOrder;

@XmlType(propOrder = { PropertyOrder.VERSION, PropertyOrder.TIMESTAMP,
		PropertyOrder.ORG_ID,Attributes.MSG_ID })
public class Head implements Attributes {

	String version;

	String timeStamp;

	String organizationId;

	String msgId;

	public Head() {
	}

	public String getVersion() {
		return version;
	}

	@XmlAttribute(name = VER)
	public void setVersion(String version) {
		this.version = version;
	}

	public String getMsgId() {
		return msgId;
	}

	@XmlAttribute(name = MSG_ID)
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	@XmlAttribute(name = TS)
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getOrganizationId() {
		return organizationId;
	}

	@XmlAttribute(name = ORG_ID)
	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

}