package psp.upi.model;

import javax.xml.bind.annotation.XmlElement;

import psp.upi.constants.Elements;

public class RegDetails {

	private Detail detail;

	private Credentials credential;

	public RegDetails() {

	}

	public Detail getDetail() {
		return detail;
	}

	@XmlElement(name = Elements.DETAIL)
	public void setDetail(Detail detail) {
		this.detail = detail;
	}

	public Credentials getCredential() {
		return credential;
	}

	@XmlElement(name = Elements.CREDS)
	public void setCredential(Credentials credential) {
		this.credential = credential;
	}

}
