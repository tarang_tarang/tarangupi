package psp.upi.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import psp.upi.constants.Attributes;
import psp.upi.constants.Elements;
import psp.upi.constants.PropertyOrder;

@XmlType(propOrder = { Attributes.TYPE,
		Attributes.SUB_TYPE, PropertyOrder.DATA })
public class NewCredential {

	private String type;

	private String subtype;

	private Data data;

	public NewCredential() {
	}

	public String getType() {
		return type;
	}

	@XmlAttribute(name = Attributes.TYPE)
	public void setType(String type) {
		this.type = type;
	}

	public String getSubtype() {
		return subtype;
	}

	@XmlAttribute(name = Attributes.SUB_TYPE)
	public void setSubtype(String subtype) {
		this.subtype = subtype;
	}

	public Data getData() {
		return data;
	}

	@XmlElement(name = Elements.DATA)
	public void setData(Data data) {
		this.data = data;
	}

}
