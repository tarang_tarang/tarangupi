package psp.upi.model;

import javax.xml.bind.annotation.XmlAttribute;

public class TxnConfirmation {

	private String note;

	private String orgStatus;

	private String orgErrCode;

	private String type;

	private String orgTxnId;

	public TxnConfirmation() {
	}

	public String getNote() {
		return note;
	}

	@XmlAttribute
	public void setNote(String note) {
		this.note = note;
	}

	public String getOrgStatus() {
		return orgStatus;
	}

	@XmlAttribute
	public void setOrgStatus(String orgStatus) {
		this.orgStatus = orgStatus;
	}

	public String getOrgErrCode() {
		return orgErrCode;
	}

	@XmlAttribute
	public void setOrgErrCode(String orgErrCode) {
		this.orgErrCode = orgErrCode;
	}

	public String getType() {
		return type;
	}

	@XmlAttribute
	public void setType(String type) {
		this.type = type;
	}

	public String getOrgTxnId() {
		return orgTxnId;
	}

	@XmlAttribute
	public void setOrgTxnId(String orgTxnId) {
		this.orgTxnId = orgTxnId;
	}

}
