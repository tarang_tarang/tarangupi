/**
 * 
 */
package psp.upi.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import psp.upi.constants.Elements;

/**
 * @author manasp
 * 
 */
public class Rules {

	private List<Rule> rules;

	public Rules() {
	}

	public List<Rule> getRules() {
		return rules;
	}

	@XmlElement(name = Elements.RULE)
	public void setRules(List<Rule> rules) {
		this.rules = rules;
	}

}
