/**
 * 
 */
package psp.upi.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import psp.upi.constants.Attributes;
import psp.upi.constants.Elements;
import psp.upi.constants.PropertyOrder;

/**
 * @author manasp
 * 
 */
@XmlType(propOrder = { PropertyOrder.REQ_MSG_ID, Attributes.RESULT,
		Attributes.ERROR_CODE, Attributes.MASK_NAME,
		PropertyOrder.REF, PropertyOrder.VAE_LIST })
public class Response implements Attributes {

	private String reqMsgId;

	private String result;

	private String errCode;

	private String maskName;

	private Reference reference;

	private VerifiedAddressEntryList verifiedAddressEntryList;
	
	public Response() {
	}

	public String getReqMsgId() {
		return reqMsgId;
	}

	@XmlAttribute(name = REQ_MSG_ID)
	public void setReqMsgId(String reqMsgId) {
		this.reqMsgId = reqMsgId;
	}

	public String getResult() {
		return result;
	}

	@XmlAttribute(name = RESULT)
	public void setResult(String result) {
		this.result = result;
	}

	public String getErrCode() {
		return errCode;
	}

	@XmlAttribute(name = ERROR_CODE)
	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	public String getMaskName() {
		return maskName;
	}

	@XmlAttribute(name = MASK_NAME)
	public void setMaskName(String maskName) {
		this.maskName = maskName;
	}

	public Reference getReference() {
		return reference;
	}

	@XmlElement(name = Elements.REF)
	public void setReference(Reference reference) {
		this.reference = reference;
	}

	public VerifiedAddressEntryList getVerifiedAddressEntryList() {
		return verifiedAddressEntryList;
	}

	@XmlElement(name = Elements.VAE_LIST)
	public void setVerifiedAddressEntryList(VerifiedAddressEntryList verifiedAddressEntryList) {
		this.verifiedAddressEntryList = verifiedAddressEntryList;
	}

}
