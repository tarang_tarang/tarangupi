/**
 * 
 */
package psp.upi.model;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * @author manasp
 * 
 */
public class Data {

	private String code;

	private String ki;

	private String value;
	
	public Data() {
	}

	public String getCode() {
		return code;
	}

	@XmlAttribute
	public void setCode(String code) {
		this.code = code;
	}

	public String getKi() {
		return ki;
	}

	@XmlAttribute
	public void setKi(String ki) {
		this.ki = ki;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}