package psp.upi.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import psp.upi.constants.Elements;

public class PublicKeyList {

	private List<PublicKey> publicKeys;//TODO but in document 1..1

	public PublicKeyList() {
	}

	public List<PublicKey> getPublicKeys() {
		return publicKeys;
	}

	@XmlElement(name = Elements.KEY)
	public void setPublicKeys(List<PublicKey> publicKeys) {
		this.publicKeys = publicKeys;
	}

}
