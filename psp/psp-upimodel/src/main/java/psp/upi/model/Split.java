/**
 * 
 */
package psp.upi.model;

import javax.xml.bind.annotation.XmlAttribute;

import psp.upi.constants.Attributes;

/**
 * @author manasp
 * 
 */
public class Split {

	private String name;

	private String value;

	public Split() {
	}

	public String getName() {
		return name;
	}

	@XmlAttribute(name = Attributes.NAME)
	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	@XmlAttribute(name = Attributes.VALUE)
	public void setValue(String value) {
		this.value = value;
	}

}
