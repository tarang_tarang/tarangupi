/**
 * 
 */
package psp.upi.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import psp.upi.constants.Attributes;
import psp.upi.constants.Elements;
import psp.upi.constants.PropertyOrder;

/**
 * @author manasp
 * 
 */
@XmlType(propOrder = { Attributes.VALUE, PropertyOrder.CURRENCY,
		PropertyOrder.SPLIT })
public class Amount {

	private String value;

	private String currency;

	private Split split;

	public Amount() {
	}

	public String getValue() {
		return value;
	}

	@XmlAttribute(name = Attributes.VALUE)
	public void setValue(String value) {
		this.value = value;
	}

	public String getCurrency() {
		return currency;
	}

	@XmlAttribute(name = Attributes.CURR)
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Split getSplit() {
		return split;
	}

	@XmlElement(name = Elements.SPLIT)
	public void setSplit(Split split) {
		this.split = split;
	}

}
