package psp.upi.model;

import javax.xml.bind.annotation.XmlElement;

import psp.upi.constants.Elements;

public class Balance {

	private Data data;

	public Balance() {
	}

	public Data getData() {
		return data;
	}

	@XmlElement(name = Elements.DATA)
	public void setData(Data data) {
		this.data = data;
	}

}
