/**
 * 
 */
package psp.upi.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

import psp.upi.constants.Attributes;

/**
 * @author manasp
 * 
 */
@XmlType(propOrder = { Attributes.PROVIDER,
		Attributes.TYPE, Attributes.VALUE })
public class Score implements Attributes {

	private String provider;

	private String type;

	private int value;

	public Score() {
	}

	public String getProvider() {
		return provider;
	}

	@XmlAttribute(name = PROVIDER)
	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getType() {
		return type;
	}

	@XmlAttribute(name = TYPE)
	public void setType(String type) {
		this.type = type;
	}

	public int getValue() {
		return value;
	}

	@XmlAttribute(name = VALUE)
	public void setValue(int value) {
		this.value = value;
	}

}
