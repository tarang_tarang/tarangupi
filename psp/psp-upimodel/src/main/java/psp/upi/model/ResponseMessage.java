/**
 * 
 */
package psp.upi.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import psp.upi.constants.Elements;

/**
 * @author manasp
 *
 */
public class ResponseMessage implements Elements{

	private List<PendingTxn> pendingTxns;//TODO need confirmation

	public List<PendingTxn> getPendingTxns() {
		return pendingTxns;
	}

	@XmlElement(name = PEN_TXN)
	public void setPendingTxns(List<PendingTxn> pendingTxns) {
		this.pendingTxns = pendingTxns;
	}

}
