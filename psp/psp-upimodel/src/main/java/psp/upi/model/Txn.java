/**
 * 
 */
package psp.upi.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import psp.upi.constants.Attributes;
import psp.upi.constants.Elements;
import psp.upi.constants.PropertyOrder;

/**
 * @author manasp
 * 
 */
@XmlType(propOrder = { Attributes.ID, Attributes.NOTE,
		Attributes.REF_ID, Attributes.REF_URL,
		Attributes.TIME_STAMP, Attributes.TYPE,
		Attributes.ORG_TXN_ID, PropertyOrder.RISK_SCORES, 
		PropertyOrder.RULES })
public class Txn implements Attributes, Elements {

	private String id;

	private String note;

	private String refId;

	private String refUrl;

	private String timeStamp;

	private String type;

	private String orgTxnId;

	private RiskScores riskScores;

	private Rules rules;

	public Txn() {
	}

	public String getId() {
		return id;
	}

	@XmlAttribute(name = ID)
	public void setId(String id) {
		this.id = id;
	}

	public String getNote() {
		return note;
	}

	@XmlAttribute(name = NOTE)
	public void setNote(String note) {
		this.note = note;
	}

	public String getRefId() {
		return refId;
	}

	@XmlAttribute(name = REF_ID)
	public void setRefId(String refId) {
		this.refId = refId;
	}

	public String getRefUrl() {
		return refUrl;
	}

	@XmlAttribute(name = REF_URL)
	public void setRefUrl(String refUrl) {
		this.refUrl = refUrl;
	}

	public String getType() {
		return type;
	}

	@XmlAttribute(name = TYPE)
	public void setType(String type) {
		this.type = type;
	}

	public String getOrgTxnId() {
		return orgTxnId;
	}

	@XmlAttribute(name = ORG_TXN_ID)
	public void setOrgTxnId(String orgTxnId) {
		this.orgTxnId = orgTxnId;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	@XmlAttribute(name = TS)
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public RiskScores getRiskScores() {
		return riskScores;
	}

	@XmlElement(name = RISK_SCORES)
	public void setRiskScores(RiskScores riskScores) {
		this.riskScores = riskScores;
	}

	public Rules getRules() {
		return rules;
	}

	@XmlElement(name = RULES)
	public void setRules(Rules rules) {
		this.rules = rules;
	}

}
