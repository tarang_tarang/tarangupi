/**
 * 
 */
package psp.upi.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import psp.upi.constants.Elements;

/**
 * @author manasp
 * 
 */
public class Credentials {

	private List<Credential> credentials;

	public Credentials() {
	}

	public List<Credential> getCredentials() {
		return credentials;
	}

	@XmlElement(name = Elements.CRED)
	public void setCredentials(List<Credential> credentials) {
		this.credentials = credentials;
	}

}
