/**
 * 
 */
package psp.upi.model;

import javax.xml.bind.annotation.XmlAttribute;

import psp.upi.constants.Attributes;

/**
 * @author manasp
 * 
 */
public class Rating {

	private boolean verifiedAddress; // TODO need to check

	public Rating() {
	}

	public boolean isVerifiedAddress() {
		return verifiedAddress;
	}

	@XmlAttribute(name = Attributes.VERIFIED_ADDR)
	public void setVerifiedAddress(boolean verifiedAddress) {
		this.verifiedAddress = verifiedAddress;
	}
}
