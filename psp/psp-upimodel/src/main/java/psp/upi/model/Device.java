/**
 * 
 */
package psp.upi.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import psp.upi.constants.Elements;

/**
 * @author manasp
 * 
 */
public class Device {

	private List<Tag> tags;

	public Device() {
	}

	public List<Tag> getTags() {
		return tags;
	}

	@XmlElement(name = Elements.TAG)
	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

}
