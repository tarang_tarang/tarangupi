/**
 * 
 */
package psp.upi.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;

import psp.upi.constants.Attributes;
import psp.upi.constants.Elements;

/**
 * @author manasp
 *
 */
public class PendingTxn implements Attributes, Elements{

	private String id;

	private String note;

	private String refId;

	private String refUrl;

	private Date timeStamp;

	private String type;

	private String orgTxnId;

	public PendingTxn() {
	}

	public String getId() {
		return id;
	}

	@XmlAttribute(name = ID)
	public void setId(String id) {
		this.id = id;
	}

	public String getNote() {
		return note;
	}

	@XmlAttribute(name = NOTE)
	public void setNote(String note) {
		this.note = note;
	}

	public String getRefId() {
		return refId;
	}

	@XmlAttribute(name = REF_ID)
	public void setRefId(String refId) {
		this.refId = refId;
	}

	public String getRefUrl() {
		return refUrl;
	}

	@XmlAttribute(name = REF_URL)
	public void setRefUrl(String refUrl) {
		this.refUrl = refUrl;
	}

	public String getType() {
		return type;
	}

	@XmlAttribute(name = TYPE)
	public void setType(String type) {
		this.type = type;
	}

	public String getOrgTxnId() {
		return orgTxnId;
	}

	@XmlAttribute(name = ORG_TXN_ID)
	public void setOrgTxnId(String orgTxnId) {
		this.orgTxnId = orgTxnId;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	@XmlAttribute(name = TS)
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

}
