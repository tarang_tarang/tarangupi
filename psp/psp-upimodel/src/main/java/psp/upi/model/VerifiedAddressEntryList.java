package psp.upi.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import psp.upi.constants.Elements;

public class VerifiedAddressEntryList {

	private List<VerifiedAddressEntry> verifiedAddressEntries;//TODO in document 1..1

	public VerifiedAddressEntryList() {
	}

	public List<VerifiedAddressEntry> getVerifiedAddressEntries() {
		return verifiedAddressEntries;
	}
	
	@XmlElement(name = Elements.VAE)
	public void setVerifiedAddressEntries(List<VerifiedAddressEntry> verifiedAddressEntries) {
		this.verifiedAddressEntries = verifiedAddressEntries;
	}

}
