/**
 * 
 */
package psp.upi.model;

import javax.xml.bind.annotation.XmlElement;

import psp.upi.constants.Elements;

/**
 * @author manasp
 * 
 */
public class Payees {

	private Payee payee;

	public Payees() {
	}

	public Payee getPayee() {
		return payee;
	}

	@XmlElement(name = Elements.PAYEE)
	public void setPayee(Payee payee) {
		this.payee = payee;
	}

}
