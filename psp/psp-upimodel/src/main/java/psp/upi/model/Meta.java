package psp.upi.model;

import javax.xml.bind.annotation.XmlElement;

import psp.upi.constants.Elements;

public class Meta {

	private Tag tag;

	public Meta() {
	}

	public Tag getTag() {
		return tag;
	}

	@XmlElement(name = Elements.TAG)
	public void setTag(Tag tag) {
		this.tag = tag;
	}

}
