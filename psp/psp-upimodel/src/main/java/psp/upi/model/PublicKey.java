package psp.upi.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import psp.upi.constants.Attributes;
import psp.upi.constants.Elements;

/*@XmlType(propOrder = { Attributes.CODE, Attributes.OWNER,
		Attributes.TYPE, Attributes.KI, "keyValue" })*/
public class PublicKey implements Attributes {

	private String code;

	private Integer ki;

	private String owner;

	private String type;

	private KeyValue keyValue;

	public PublicKey() {
	}

	public String getCode() {
		return code;
	}

	@XmlAttribute(name = CODE)
	public void setCode(String code) {
		this.code = code;
	}

	public Integer getKi() {
		return ki;
	}

	@XmlAttribute(name = KI)
	public void setKi(Integer ki) {
		this.ki = ki;
	}

	public String getOwner() {
		return owner;
	}

	@XmlAttribute(name = OWNER)
	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getType() {
		return type;
	}

	@XmlAttribute(name = TYPE)
	public void setType(String type) {
		this.type = type;
	}

	public KeyValue getKeyValue() {
		return keyValue;
	}

	@XmlElement(name = Elements.KEY_VALUE)
	public void setKeyValue(KeyValue keyValue) {
		this.keyValue = keyValue;
	}

}
