/**
 * 
 */
package psp.upi.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import psp.upi.constants.Attributes;
import psp.upi.constants.Elements;

/**
 * @author manasp
 * 
 */
/*@XmlType(propOrder = { Attributes.TYPE, "subType", "data" })*/
public class Credential {

	private String type;

	private String subType;

	private Data data;

	public Credential() {
	}

	public String getType() {
		return type;
	}

	@XmlAttribute(name = Attributes.TYPE)
	public void setType(String type) {
		this.type = type;
	}

	public String getSubType() {
		return subType;
	}

	@XmlAttribute(name = Attributes.SUB_TYPE)
	public void setSubType(String subType) {
		this.subType = subType;
	}

	@XmlElement(name = Elements.DATA)
	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

}
