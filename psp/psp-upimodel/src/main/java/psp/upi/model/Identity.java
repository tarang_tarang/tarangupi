/**
 * 
 */
package psp.upi.model;

import javax.xml.bind.annotation.XmlAttribute;

import psp.upi.constants.Attributes;

/**
 * @author manasp
 * 
 */
public class Identity {

	private String type;

	private String verifiedName;

	public Identity() {
	}

	public String getType() {
		return type;
	}

	@XmlAttribute(name = Attributes.TYPE)
	public void setType(String type) {
		this.type = type;
	}

	public String getVerifiedName() {
		return verifiedName;
	}

	@XmlAttribute(name = Attributes.VERIFIED_NAME)
	public void setVerifiedName(String verifiedName) {
		this.verifiedName = verifiedName;
	}

}
