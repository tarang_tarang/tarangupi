/**
 * 
 */
package psp.resp.fromupi;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import psp.upi.constants.Elements;
import psp.upi.model.Head;
import psp.upi.model.Payer;
import psp.upi.model.Response;
import psp.upi.model.Txn;


/**
 * @author manasp
 *
 */
@XmlRootElement(name="upi:RespBalEnq")
public class BalanceResponse  implements Elements{

	private Head head;

	private Txn transaction;
	
	private Payer payer;
	
	private Response resp;
	
	public Head getHead() {
		return head;
	}
	
	@XmlElement(name = HEAD)
	public void setHead(Head head) {
		this.head = head;
	}

	public Txn getTransaction() {
		return transaction;
	}

	@XmlElement(name = TXN)
	public void setTransaction(Txn transaction) {
		this.transaction = transaction;
	}

	public Payer getPayer() {
		return payer;
	}

	@XmlElement(name = PAYER)
	public void setPayer(Payer payer) {
		this.payer = payer;
	}

	public Response getResp() {
		return resp;
	}

	@XmlElement(name = RESP)
	public void setResp(Response resp) {
		this.resp = resp;
	}
}
