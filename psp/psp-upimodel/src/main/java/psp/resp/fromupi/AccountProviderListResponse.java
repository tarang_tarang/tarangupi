package psp.resp.fromupi;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import psp.upi.constants.Elements;
import psp.upi.model.AccountProviderList;
import psp.upi.model.Head;
import psp.upi.model.Response;
import psp.upi.model.Txn;

/**
 * 
 * @author manasp
 *
 */
@XmlRootElement(name="upi:RespListAccPvd")
public class AccountProviderListResponse implements Elements{

	private Head head;

	private Txn transaction;
	
	private Response resp;
	
	private AccountProviderList accountProviderList;
	
	public Head getHead() {
		return head;
	}
	
	@XmlElement(name = HEAD)
	public void setHead(Head head) {
		this.head = head;
	}

	public Txn getTransaction() {
		return transaction;
	}

	@XmlElement(name = TXN)
	public void setTransaction(Txn transaction) {
		this.transaction = transaction;
	}

	public Response getResp() {
		return resp;
	}

	@XmlElement(name = RESP)
	public void setResp(Response resp) {
		this.resp = resp;
	}

	public AccountProviderList getAccountProviderList() {
		return accountProviderList;
	}

	@XmlElement(name = ACC_PVD_LIST)
	public void setAccountProviderList(AccountProviderList accountProviderList) {
		this.accountProviderList = accountProviderList;
	}

}
