package psp.resp.fromupi;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author manasp
 *
 */
@XmlRootElement(name="upi:Ack")
public class AckResponse implements Serializable{

	private static final long serialVersionUID = 1L;

	private String api;
	
	private String reqMsgid;
	
	private String error;
	
	private String transaction;
	
	public AckResponse() {}
	
	public String getApi() {
		return api;
	}

	@XmlAttribute(name="api")
	public void setApi(String api) {
		this.api = api;
	}

	public String getReqMsgid() {
		return reqMsgid;
	}

	@XmlAttribute(name="reqMsgId")
	public void setReqMsgid(String reqMsgid) {
		this.reqMsgid = reqMsgid;
	}

	public String getError() {
		return error;
	}

	@XmlAttribute(name="err")
	public void setError(String error) {
		this.error = error;
	}

	public String getTransaction() {
		return transaction;
	}

	@XmlAttribute(name="ts")
	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}
}
