package psp.resp.fromupi;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import psp.upi.constants.Elements;
import psp.upi.model.Head;
import psp.upi.model.PublicKeyList;
import psp.upi.model.Response;
import psp.upi.model.Txn;

/**
 * 
 * @author manasp
 *
 */
@XmlRootElement(name="upi:RespListKeys")
public class KeysListResponse implements Elements{

	private Head head;

	private Txn transaction;
	
	private Response resp;
	
	private PublicKeyList publicKeyList;
	
	public Head getHead() {
		return head;
	}
	
	@XmlElement(name = HEAD)
	public void setHead(Head head) {
		this.head = head;
	}

	public Txn getTransaction() {
		return transaction;
	}

	@XmlElement(name = TXN)
	public void setTransaction(Txn transaction) {
		this.transaction = transaction;
	}

	

	public Response getResp() {
		return resp;
	}

	@XmlElement(name = RESP)
	public void setResp(Response resp) {
		this.resp = resp;
	}

	public PublicKeyList getPublicKeyList() {
		return publicKeyList;
	}

	@XmlElement(name = KEY_LIST)
	public void setPublicKeyList(PublicKeyList publicKeyList) {
		this.publicKeyList = publicKeyList;
	}


}
