/**
 * 
 */
package psp.req.fromupi;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import psp.upi.constants.Elements;
import psp.upi.model.Head;
import psp.upi.model.Transaction;
import psp.upi.model.TxnConfirmation;

/**
 * @author manasp
 *
 */
@XmlRootElement(name="upi:ReqTxnConfirmation")
public class TxnConfirmationRequest implements Elements {

	private Head head;
	
	private Transaction transaction;
	
	private TxnConfirmation txnConfirmation;

	public TxnConfirmationRequest() {
	}

	public Head getHead() {
		return head;
	}

	@XmlElement(name = HEAD)
	public void setHead(Head head) {
		this.head = head;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	@XmlElement(name = TXN)
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public TxnConfirmation getTxnConfirmation() {
		return txnConfirmation;
	}

	@XmlElement(name = TXN_CONFIRMATION)
	public void setTxnConfirmation(TxnConfirmation txnConfirmation) {
		this.txnConfirmation = txnConfirmation;
	}
	
	
	
}
