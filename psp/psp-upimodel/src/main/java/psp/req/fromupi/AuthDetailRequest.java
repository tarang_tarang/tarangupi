/**
 * 
 */
package psp.req.fromupi;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import psp.upi.constants.Elements;
import psp.upi.model.Head;
import psp.upi.model.Payees;
import psp.upi.model.Payer;
import psp.upi.model.Transaction;

/**
 * @author manasp
 *
 */
@XmlRootElement(name="upi:ReqAuthDetails")
public class AuthDetailRequest implements Elements{

	private Head head;
	
	private Transaction transaction;
	
	private Payer payer;
	
	private Payees payees;
	
	public AuthDetailRequest(){		
	}
	
	public Head getHead() {
		return head;
	}
	
	@XmlElement(name = HEAD)
	public void setHead(Head head) {
		this.head = head;
	}
	
	public Transaction getTransaction() {
		return transaction;
	}
	
	@XmlElement(name = TXN)
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}
	
	public Payer getPayer() {
		return payer;
	}
	
	@XmlElement(name = PAYER)
	public void setPayer(Payer payer) {
		this.payer = payer;
	}
	
	public Payees getPayees() {
		return payees;
	}
	
	@XmlElement(name = PAYEES)
	public void setPayees(Payees payees) {
		this.payees = payees;
	}
}
