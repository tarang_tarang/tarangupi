/**
 * 
 */
package psp.req.fromupi;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import psp.upi.constants.Elements;
import psp.upi.model.Head;
import psp.upi.model.Response;
import psp.upi.model.Transaction;

/**
 * @author manasp
 * 
 */
@XmlRootElement(name="upi:RespPay")
public class ResponsePayRequest  implements Elements{

	private Head head;

	private Transaction transaction;
	
	private Response response;
	
	public ResponsePayRequest(){		
	}

	public Head getHead() {
		return head;
	}

	@XmlElement(name = HEAD)
	public void setHead(Head head) {
		this.head = head;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	@XmlElement(name = TXN)
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public Response getResponse() {
		return response;
	}

	@XmlElement(name = RESP)
	public void setResponse(Response response) {
		this.response = response;
	}
}
