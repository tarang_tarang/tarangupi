/**
 * 
 */
package psp.req.fromupi;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import psp.upi.constants.Elements;
import psp.upi.model.Head;
import psp.upi.model.HeartbeatMessage;
import psp.upi.model.Transaction;

/**
 * @author manasp
 * 
 */
@XmlRootElement(name = "upi:ReqHbt")
public class HeartBeatRequest implements Elements{

	private Head head;
	
	private Transaction transaction;
	
	private HeartbeatMessage hbtMsg;;
	
	public HeartBeatRequest(){		
	}

	public Head getHead() {
		return head;
	}

	@XmlElement(name = HEAD)
	public void setHead(Head head) {
		this.head = head;
	}

	public Transaction getTransaction() {
		return transaction;
	}
	
	@XmlElement(name = TXN)
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public HeartbeatMessage getHbtMsg() {
		return hbtMsg;
	}

	@XmlElement(name = HBT_MSG)
	public void setHbtMsg(HeartbeatMessage hbtMsg) {
		this.hbtMsg = hbtMsg;
	}
}
