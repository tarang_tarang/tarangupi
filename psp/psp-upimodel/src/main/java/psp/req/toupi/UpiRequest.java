/**
 * 
 */
package psp.req.toupi;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import psp.upi.constants.Elements;
import psp.upi.model.Head;
import psp.upi.model.HeartbeatMessage;
import psp.upi.model.Meta;
import psp.upi.model.NewCredential;
import psp.upi.model.Payee;
import psp.upi.model.Payees;
import psp.upi.model.Payer;
import psp.upi.model.RegDetails;
import psp.upi.model.RequestPendingMessage;
import psp.upi.model.Response;
import psp.upi.model.Txn;
import psp.upi.model.TxnConfirmation;
import psp.upi.model.VerifiedAddressEntry;

/**
 * @author manasp
 * 
 */
@XmlRootElement(name = "common")
// @XmlType (propOrder={"head","meta","transaction", "payer", "payees",})
public class UpiRequest implements Elements {

	private String schemaName;

	private String api;

	private String reqMsgid;

	private String error;

	private String ts;

	private Head head;

	private Meta meta;

	private Txn transaction;

	private Payer payer;

	private Payees payees;

	private Response resp;

	private TxnConfirmation txnConfirmation;

	private HeartbeatMessage heartbeatMessage;

	private RequestPendingMessage requestPendingMessage;

	private Payee payee;

	private NewCredential newCredential;

	private RegDetails regDetails;

	private List<VerifiedAddressEntry> verifiedAddressEntry;
	
	public String getSchemaName() {
		return schemaName;
	}

	public String getApi() {
		return api;
	}

	@XmlAttribute(name = "xmlns:upi")
	public void setSchemaName(String schemaName) {
		this.schemaName = schemaName;
	}

	@XmlAttribute(name = "api")
	public void setApi(String api) {
		this.api = api;
	}

	public String getReqMsgid() {
		return reqMsgid;
	}

	@XmlAttribute(name = "reqMsgId")
	public void setReqMsgid(String reqMsgid) {
		this.reqMsgid = reqMsgid;
	}

	public String getError() {
		return error;
	}

	@XmlAttribute(name = "err")
	public void setError(String error) {
		this.error = error;
	}

	public String getTs() {
		return ts;
	}

	@XmlAttribute(name = "ts")
	public void setTs(String ts) {
		this.ts = ts;
	}

	public Head getHead() {
		return head;
	}

	@XmlElement(name = HEAD)
	public void setHead(Head head) {
		this.head = head;
	}

	public Meta getMeta() {
		return meta;
	}

	@XmlElement(name = META)
	public void setMeta(Meta meta) {
		this.meta = meta;
	}

	public Txn getTransaction() {
		return transaction;
	}

	@XmlElement(name = TXN)
	public void setTransaction(Txn transaction) {
		this.transaction = transaction;
	}

	public Payer getPayer() {
		return payer;
	}

	@XmlElement(name = PAYER)
	public void setPayer(Payer payer) {
		this.payer = payer;
	}

	public Payees getPayees() {
		return payees;
	}

	@XmlElement(name = PAYEES)
	public void setPayees(Payees payees) {
		this.payees = payees;
	}

	public Response getResp() {
		return resp;
	}

	@XmlElement(name = RESP)
	public void setResp(Response resp) {
		this.resp = resp;
	}

	public TxnConfirmation getTxnConfirmation() {
		return txnConfirmation;
	}

	@XmlElement(name = TXN_CONFIRMATION)
	public void setTxnConfirmation(TxnConfirmation txnConfirmation) {
		this.txnConfirmation = txnConfirmation;
	}

	public HeartbeatMessage getHeartbeatMessage() {
		return heartbeatMessage;
	}

	@XmlElement(name = HBT_MSG)
	public void setHeartbeatMessage(HeartbeatMessage heartbeatMessage) {
		this.heartbeatMessage = heartbeatMessage;
	}

	public RequestPendingMessage getRequestPendingMessage() {
		return requestPendingMessage;
	}

	@XmlElement(name = REQ_MSG)
	public void setRequestPendingMessage(
			RequestPendingMessage requestPendingMessage) {
		this.requestPendingMessage = requestPendingMessage;
	}

	public Payee getPayee() {
		return payee;
	}

	@XmlElement(name = PAYEE)
	public void setPayee(Payee payee) {
		this.payee = payee;
	}

	public NewCredential getNewCredential() {
		return newCredential;
	}

	@XmlElement(name = NEW_CRED)
	public void setNewCredential(NewCredential newCredential) {
		this.newCredential = newCredential;
	}

	public RegDetails getRegDetails() {
		return regDetails;
	}

	@XmlElement(name = REG_DETAILS)
	public void setRegDetails(RegDetails regDetails) {
		this.regDetails = regDetails;
	}


	public List<VerifiedAddressEntry> getVerifiedAddressEntry() {
		return verifiedAddressEntry;
	}

	@XmlElement(name = VAE_LIST)
	public void setVerifiedAddressEntry(List<VerifiedAddressEntry> verifiedAddressEntry) {
		this.verifiedAddressEntry = verifiedAddressEntry;
	}

	

}
