/**
 * 
 */
package psp.mobile.domain;

import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import psp.common.model.constants.ServiceUris;
import psp.constants.CommonConstants;
import psp.mobile.model.request.AddBankAccountRequest;
import psp.mobile.model.request.AddBeneficiaryRequest;
import psp.mobile.model.request.BalanceEnquiryRequest;
import psp.mobile.model.request.ChangePasswordRequest;
import psp.mobile.model.request.CheckUserRequest;
import psp.mobile.model.request.CollectPayConfirmRequest;
import psp.mobile.model.request.CollectPaymentRequest;
import psp.mobile.model.request.ForgetPasswordRequest;
import psp.mobile.model.request.GeneratePinRequest;
import psp.mobile.model.request.GetAccountListRequest;
import psp.mobile.model.request.GetAccountProvidersRequest;
import psp.mobile.model.request.GetBeneficiaryListRequest;
import psp.mobile.model.request.GetBeneficiaryRequest;
import psp.mobile.model.request.GetExistingAccountsRequest;
import psp.mobile.model.request.GetKeyRequest;
import psp.mobile.model.request.GetMerchantListRequest;
import psp.mobile.model.request.GetMerchantRequest;
import psp.mobile.model.request.GetNotificationDetailsRequest;
import psp.mobile.model.request.GetNotificationListRequest;
import psp.mobile.model.request.GetTokenRequest;
import psp.mobile.model.request.GetTxnDetailsRequest;
import psp.mobile.model.request.LoginRequest;
import psp.mobile.model.request.LogoutRequest;
import psp.mobile.model.request.OtpRequest;
import psp.mobile.model.request.PaymentRequest;
import psp.mobile.model.request.RegistrationRequest;
import psp.mobile.model.request.SetPinRequest;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.process.MobileRequestProcessor;
import psp.util.JsonUtil;

/**
 * @author manasp
 *
 */
@RestController
@RequestMapping("/mobile")
public class MobileRequestController {

	private static final Logger LOGGER = Logger.getLogger(MobileRequestController.class.getName());
	
	@Autowired
	private MobileRequestProcessor mobileRequestProcessor;
	
	public MobileRequestController(){
	}
	
	@RequestMapping(value = ServiceUris.REGISTRATION, method = RequestMethod.POST, headers = CommonConstants.HEADER, produces = CommonConstants.PRODUCER)
	@ResponseBody 
	public MessageResponse registration(HttpServletRequest request) {
		LOGGER.info("registration request started from mobile to psp");
		RegistrationRequest req = (RegistrationRequest)JsonUtil.getJsonObject(request, RegistrationRequest.class);
		MessageResponse response = mobileRequestProcessor.processNonAuthorisedRequest(req);
		LOGGER.info("registration request completed from mobile to psp");
		return response;
	}
	
	@RequestMapping(value = ServiceUris.LOGIN, method = RequestMethod.POST, headers = CommonConstants.HEADER, produces = CommonConstants.PRODUCER)
	@ResponseBody 
	public MessageResponse login(HttpServletRequest request) {
		LOGGER.info("login request started from mobile to psp");
		LoginRequest req = (LoginRequest)JsonUtil.getJsonObject(request, LoginRequest.class);
		MessageResponse response = mobileRequestProcessor.processNonAuthorisedRequest(req);
		LOGGER.info("login request completed from mobile to psp");
		return response;
	}	
	
	@RequestMapping(value = ServiceUris.CHANGE_PWD, method = RequestMethod.POST, headers = CommonConstants.HEADER, produces = CommonConstants.PRODUCER)
	@ResponseBody 
	public MessageResponse changePassword(HttpServletRequest request) {
		LOGGER.info("changePassword request started from mobile to psp");
		ChangePasswordRequest req = (ChangePasswordRequest)JsonUtil.getJsonObject(request, ChangePasswordRequest.class);
		MessageResponse response = mobileRequestProcessor.processAuthorisedRequest(req);
		LOGGER.info("changePassword request completed from mobile to psp");
		return response;
	}
	
	@RequestMapping(value = ServiceUris.FORGET_PWD, method = RequestMethod.POST, headers = CommonConstants.HEADER, produces = CommonConstants.PRODUCER)
	@ResponseBody 
	public MessageResponse forgetPassword(HttpServletRequest request) {
		LOGGER.info("forgetPassword request started from mobile to psp");
		ForgetPasswordRequest req = (ForgetPasswordRequest)JsonUtil.getJsonObject(request, ForgetPasswordRequest.class);
		MessageResponse response = mobileRequestProcessor.forgetPassword(req);
		LOGGER.info("forgetPassword request completed from mobile to psp");
		return response;
	}
	
	@RequestMapping(value = ServiceUris.GET_ACC_PVDS, method = RequestMethod.POST, headers = CommonConstants.HEADER, produces = CommonConstants.PRODUCER)
	@ResponseBody 
	public MessageResponse getAccProviders(HttpServletRequest request) {
		LOGGER.info("getAccProviders request started from mobile to psp");
		GetAccountProvidersRequest req = (GetAccountProvidersRequest)JsonUtil.getJsonObject(request, GetAccountProvidersRequest.class);
		MessageResponse response = mobileRequestProcessor.processAuthorisedRequest(req);
		LOGGER.info("getAccProviders request completed from mobile to psp");
		return response;
	}
	
	@RequestMapping(value = ServiceUris.OTP_REQ, method = RequestMethod.POST, headers = CommonConstants.HEADER, produces = CommonConstants.PRODUCER)
	@ResponseBody 
	public MessageResponse otpRequest(HttpServletRequest request) {
		LOGGER.info("otpRequest request started from mobile to psp");
		OtpRequest req = (OtpRequest)JsonUtil.getJsonObject(request, OtpRequest.class);
		MessageResponse response = mobileRequestProcessor.processAuthorisedRequest(req);
		LOGGER.info("otpRequest request completed from mobile to psp");
		return response;
	}
	
	@RequestMapping(value = ServiceUris.GET_ACC_LIST, method = RequestMethod.POST, headers = CommonConstants.HEADER, produces = CommonConstants.PRODUCER)
	@ResponseBody 
	public MessageResponse getAccList(HttpServletRequest request) {
		LOGGER.info("getAccList request started from mobile to psp");
		GetAccountListRequest req = (GetAccountListRequest)JsonUtil.getJsonObject(request, GetAccountListRequest.class);
		MessageResponse response = mobileRequestProcessor.processAuthorisedRequest(req);
		LOGGER.info("getAccList request completed from mobile to psp");
		return response;
	}
	
	@RequestMapping(value = ServiceUris.SET_PIN, method = RequestMethod.POST, headers = CommonConstants.HEADER, produces = CommonConstants.PRODUCER)
	@ResponseBody 
	public MessageResponse setPin(HttpServletRequest request) {
		LOGGER.info("setPin request started from mobile to psp");
		SetPinRequest req = (SetPinRequest)JsonUtil.getJsonObject(request, SetPinRequest.class);
		MessageResponse response = mobileRequestProcessor.processAuthorisedRequest(req);
		LOGGER.info("setPin request completed from mobile to psp");
		return response;
	}
	
	@RequestMapping(value = ServiceUris.MAKE_PAYMENT, method = RequestMethod.POST, headers = CommonConstants.HEADER, produces = CommonConstants.PRODUCER)
	@ResponseBody 
	public MessageResponse makePayment(HttpServletRequest request) {
		LOGGER.info("makePayment request started from mobile to psp");
		PaymentRequest req = (PaymentRequest)JsonUtil.getJsonObject(request, PaymentRequest.class);
		MessageResponse response = mobileRequestProcessor.processAuthorisedRequest(req);
		LOGGER.info("makePayment request completed from mobile to psp");
		return response;
	}
	
	@RequestMapping(value = ServiceUris.GET_TXN_DETAILS, method = RequestMethod.POST, headers = CommonConstants.HEADER, produces = CommonConstants.PRODUCER)
	@ResponseBody 
	public MessageResponse getTxnDetails(HttpServletRequest request) {
		LOGGER.info("getTxnDetails request started from mobile to psp");
		GetTxnDetailsRequest req = (GetTxnDetailsRequest)JsonUtil.getJsonObject(request, GetTxnDetailsRequest.class);
		MessageResponse response = mobileRequestProcessor.processAuthorisedRequest(req);
		LOGGER.info("getTxnDetails request completed from mobile to psp");
		return response;
	}
	
	@RequestMapping(value = ServiceUris.BAL_ENQ, method = RequestMethod.POST, headers = CommonConstants.HEADER, produces = CommonConstants.PRODUCER)
	@ResponseBody 
	public MessageResponse balEnquiry(HttpServletRequest request) {
		LOGGER.info("balEnquiry request started from mobile to psp");
		BalanceEnquiryRequest req = (BalanceEnquiryRequest)JsonUtil.getJsonObject(request, BalanceEnquiryRequest.class);
		MessageResponse response = mobileRequestProcessor.processAuthorisedRequest(req);
		LOGGER.info("balEnquiry request completed from mobile to psp");
		return response;
	}
	
	@RequestMapping(value = ServiceUris.ADD_BNK_ACC, method = RequestMethod.POST, headers = CommonConstants.HEADER, produces = CommonConstants.PRODUCER)
	@ResponseBody 
	public MessageResponse addBankAcc(HttpServletRequest request) {
		LOGGER.info("addBankAcc request started from mobile to psp");
		AddBankAccountRequest req = (AddBankAccountRequest)JsonUtil.getJsonObject(request, AddBankAccountRequest.class);
		MessageResponse response = mobileRequestProcessor.processAuthorisedRequest(req);
		LOGGER.info("addBankAcc request completed from mobile to psp");
		return response;
	}
	
	@RequestMapping(value = ServiceUris.IS_REGISTER_USER, method = RequestMethod.POST, headers = CommonConstants.HEADER, produces = CommonConstants.PRODUCER)
	@ResponseBody 
	public MessageResponse isUserRegistred(HttpServletRequest request) {
		LOGGER.info("Whether user is register or not request started");
		CheckUserRequest req = (CheckUserRequest)JsonUtil.getJsonObject(request, CheckUserRequest.class);
		MessageResponse response = mobileRequestProcessor.processNonAuthorisedRequest(req);
		LOGGER.info("registerUser request is completed");
		return response;
	}
	
	@RequestMapping(value = ServiceUris.LOGOUT, method = RequestMethod.POST, headers = CommonConstants.HEADER, produces = CommonConstants.PRODUCER)
	@ResponseBody 
	public MessageResponse logout(HttpServletRequest request) {
		LOGGER.info("logout request started from mobile to psp");
		LogoutRequest req = (LogoutRequest)JsonUtil.getJsonObject(request, LogoutRequest.class);
		MessageResponse response = mobileRequestProcessor.processAuthorisedRequest(req);
		LOGGER.info("logout request completed from mobile to psp");
		return response;
	}

	@RequestMapping(value = ServiceUris.GET_MER_LIST, method = RequestMethod.POST, headers = CommonConstants.HEADER, produces = CommonConstants.PRODUCER)
	@ResponseBody 
	public MessageResponse getMerchantList(HttpServletRequest request) {
		LOGGER.info("getMerchantList request started from mobile to psp");
		GetMerchantListRequest req = (GetMerchantListRequest)JsonUtil.getJsonObject(request, GetMerchantListRequest.class);
		MessageResponse response = mobileRequestProcessor.processAuthorisedRequest(req);
		LOGGER.info("getMerchantList request completed from mobile to psp");
		return response;
	}
	
	@RequestMapping(value = ServiceUris.GET_BEN_LIST, method = RequestMethod.POST, headers = CommonConstants.HEADER, produces = CommonConstants.PRODUCER)
	@ResponseBody 
	public MessageResponse getBeneficiaryList(HttpServletRequest request) {
		LOGGER.info("getBeneficiaryList request started from mobile to psp");
		GetBeneficiaryListRequest req = (GetBeneficiaryListRequest)JsonUtil.getJsonObject(request, GetBeneficiaryListRequest.class);
		MessageResponse response = mobileRequestProcessor.processAuthorisedRequest(req);
		LOGGER.info("getBeneficiaryList request completed from mobile to psp");
		return response;
	}
	
	@RequestMapping(value = ServiceUris.ADD_BEN, method = RequestMethod.POST, headers = CommonConstants.HEADER, produces = CommonConstants.PRODUCER)
	@ResponseBody 
	public MessageResponse addBeneficiary(HttpServletRequest request) {
		LOGGER.info("addBeneficiary request started from mobile to psp");
		AddBeneficiaryRequest req = (AddBeneficiaryRequest)JsonUtil.getJsonObject(request, AddBeneficiaryRequest.class);
		MessageResponse response = mobileRequestProcessor.processAuthorisedRequest(req);
		LOGGER.info("addBeneficiary request completed from mobile to psp");
		return response;
	}
	
	@RequestMapping(value = ServiceUris.GET_EXISTING_ACCOUNTS, method = RequestMethod.POST, headers = CommonConstants.HEADER, produces = CommonConstants.PRODUCER)
	@ResponseBody 
	public MessageResponse getExistingAccountsList(HttpServletRequest request) {
		LOGGER.info("Get existing accounts list request started from mobile to psp");
		GetExistingAccountsRequest req = (GetExistingAccountsRequest)JsonUtil.getJsonObject(request, GetExistingAccountsRequest.class);
		MessageResponse response = mobileRequestProcessor.processAuthorisedRequest(req);
		LOGGER.info("Get existing accounts list request completed from mobile to psp");
		return response;
	}
	
	@RequestMapping(value = ServiceUris.GET_BEN, method = RequestMethod.POST, headers = CommonConstants.HEADER, produces = CommonConstants.PRODUCER)
	@ResponseBody 
	public MessageResponse getBeneficiary(HttpServletRequest request) {
		LOGGER.info("GetBeneficiary request started from mobile to psp");
		GetBeneficiaryRequest req = (GetBeneficiaryRequest)JsonUtil.getJsonObject(request, GetBeneficiaryRequest.class);
		MessageResponse response = mobileRequestProcessor.processAuthorisedRequest(req);
		LOGGER.info("GetBeneficiary request completed from mobile to psp");
		return response;
	}
	
	@RequestMapping(value = ServiceUris.GET_KEY, method = RequestMethod.POST, headers = CommonConstants.HEADER, produces = CommonConstants.PRODUCER)
	@ResponseBody 
	public MessageResponse getKey(HttpServletRequest request) {
		LOGGER.info("GetKey request started from mobile to psp");
		GetKeyRequest req = (GetKeyRequest)JsonUtil.getJsonObject(request, GetKeyRequest.class);
		MessageResponse response = mobileRequestProcessor.processAuthorisedRequest(req);
		LOGGER.info("GetKey request completed from mobile to psp");
		return response;
	}
	
	@RequestMapping(value = ServiceUris.GET_TOKEN, method = RequestMethod.POST, headers = CommonConstants.HEADER, produces = CommonConstants.PRODUCER)
	@ResponseBody 
	public MessageResponse getToken(HttpServletRequest request) {
		LOGGER.info("getToken request started from mobile to psp");
		GetTokenRequest req = (GetTokenRequest)JsonUtil.getJsonObject(request, GetTokenRequest.class);
		MessageResponse response = mobileRequestProcessor.processNonAuthorisedRequest(req);
		LOGGER.info("getToken request completed from mobile to psp");
		return response;
	}
	
	@RequestMapping(value = ServiceUris.GET_MER, method = RequestMethod.POST, headers = CommonConstants.HEADER, produces = CommonConstants.PRODUCER)
	@ResponseBody 
	public MessageResponse getMerchant(HttpServletRequest request) {
		LOGGER.info("getMerchant request started from mobile to psp");
		GetMerchantRequest req = (GetMerchantRequest)JsonUtil.getJsonObject(request, GetMerchantRequest.class);
		MessageResponse response = mobileRequestProcessor.processAuthorisedRequest(req);
		LOGGER.info("getMerchant request completed from mobile to psp");
		return response;
	}
	
	@RequestMapping(value = ServiceUris.GEN_PIN, method = RequestMethod.POST, headers = CommonConstants.HEADER, produces = CommonConstants.PRODUCER)
	@ResponseBody 
	public MessageResponse generatePin(HttpServletRequest request) {
		LOGGER.info("generatePin request started from mobile to psp");
		GeneratePinRequest req = (GeneratePinRequest)JsonUtil.getJsonObject(request, GeneratePinRequest.class);
		MessageResponse response = mobileRequestProcessor.processAuthorisedRequest(req);
		LOGGER.info("generatePin request completed from mobile to psp");
		return response;
	}
	
	@RequestMapping(value = ServiceUris.COLLECT_PAYMENT, method = RequestMethod.POST, headers = CommonConstants.HEADER, produces = CommonConstants.PRODUCER)
	@ResponseBody 
	public MessageResponse collectPayment(HttpServletRequest request) {
		LOGGER.info("collectPayment request started from mobile to psp");
		CollectPaymentRequest req = (CollectPaymentRequest)JsonUtil.getJsonObject(request, CollectPaymentRequest.class);
		MessageResponse response = mobileRequestProcessor.processAuthorisedRequest(req);
		LOGGER.info("collectPayment request completed from mobile to psp");
		return response;
	}
	
	@RequestMapping(value = ServiceUris.GET_NOTIFICATION_LIST, method = RequestMethod.POST, headers = CommonConstants.HEADER, produces = CommonConstants.PRODUCER)
	@ResponseBody 
	public MessageResponse getNotificationList(HttpServletRequest request) {
		LOGGER.info("getNotificationList request started from mobile to psp");
		GetNotificationListRequest req = (GetNotificationListRequest)JsonUtil.getJsonObject(request, GetNotificationListRequest.class);
		MessageResponse response = mobileRequestProcessor.processAuthorisedRequest(req);
		LOGGER.info("getNotificationList request completed from mobile to psp");
		return response;
	}
	
	@RequestMapping(value = ServiceUris.GET_NOTIFICATION_DETAILS, method = RequestMethod.POST, headers = CommonConstants.HEADER, produces = CommonConstants.PRODUCER)
	@ResponseBody 
	public MessageResponse getNotificationDetails(HttpServletRequest request) {
		LOGGER.info("getNotificationDetails request started from mobile to psp");
		GetNotificationDetailsRequest req = (GetNotificationDetailsRequest)JsonUtil.getJsonObject(request, GetNotificationDetailsRequest.class);
		MessageResponse response = mobileRequestProcessor.processAuthorisedRequest(req);
		LOGGER.info("getNotificationDetails request completed from mobile to psp");
		return response;
	}
	
	@RequestMapping(value = ServiceUris.CONFIRM_COLLECT_PAY, method = RequestMethod.POST, headers = CommonConstants.HEADER, produces = CommonConstants.PRODUCER)
	@ResponseBody 
	public MessageResponse confirmCollectPay(HttpServletRequest request) {
		LOGGER.info("confirmCollectPay request started from mobile to psp");
		CollectPayConfirmRequest req = (CollectPayConfirmRequest)JsonUtil.getJsonObject(request, CollectPayConfirmRequest.class);
		MessageResponse response = mobileRequestProcessor.processAuthorisedRequest(req);
		LOGGER.info("confirmCollectPay request completed from mobile to psp");
		return response;
	}
	
}