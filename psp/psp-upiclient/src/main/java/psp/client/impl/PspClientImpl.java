/**
 * 
 */
package psp.client.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import psp.client.PspClient;
import psp.client.PspUriService;

/**
 * @author manasp
 *
 */
@Component("pspClient")
public class PspClientImpl implements PspClient {

	@Autowired
	private PspUriService pspUriService;
	
	@Autowired
	private RestTemplate restTemplate;
	
	public PspClientImpl(){
	}
	
	@Override
	public String reqPsp(String req, String api, String ver) {
		return restTemplate.postForObject(pspUriService.getPspServerUrl(PspUriService.DELIMETER + api + PspUriService.DELIMETER + ver), req, String.class);
	}

}