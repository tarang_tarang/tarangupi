/**
 * 
 */
package psp.client.impl;

import psp.client.PspUriService;

/**
 * @author manasp
 *
 */
public class PspUriServiceImpl implements PspUriService {

	private String pspServerUrl;
	
	public PspUriServiceImpl(String pspServerUrl){
		this.pspServerUrl = pspServerUrl;
	}

	@Override
	public String getPspServerUrl(String key){
		return pspServerUrl + key;
	}

}