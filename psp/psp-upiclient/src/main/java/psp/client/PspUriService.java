package psp.client;

public interface PspUriService {
	
	String DELIMETER = "/";
	
	String getPspServerUrl(String key);
	
}