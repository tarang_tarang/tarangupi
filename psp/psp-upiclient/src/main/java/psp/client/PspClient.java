/**
 * 
 */
package psp.client;

/**
 * @author manasp
 *
 */
public interface PspClient {

	String reqPsp(String req, String api, String ver);
	
}