package psp.mobile.process.factory.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.upi.system_1_2.Ack;
import org.upi.system_1_2.CredSubType;

import psp.common.PropertyReader;
import psp.common.exception.ApplicationException;
import psp.constants.CommonConstants;
import psp.constants.ServiceNotes;
import psp.constants.ServiceRequestName;
import psp.constants.ServiceStatus;
import psp.constants.StatusCode;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.AccountDetails;
import psp.dbservice.model.GeneratePinUpiRequest;
import psp.mobile.model.request.GeneratePinRequest;
import psp.mobile.model.response.GeneratePinResponse;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.process.factory.MobileCoreProcess;
import psp.mobile.process.util.MobileProcessUtility;
import psp.upi.constants.CredType;
import psp.upi.constants.PayerType;
import psp.util.DtoObjectUtil;
import psp.util.PspClientTool;
import psp.util.dto.AcDto;
import psp.util.dto.CredDto;
import psp.util.dto.HeadDto;
import psp.util.dto.PayerDto;
import psp.util.dto.RegDetailsDto;
import psp.util.dto.TxnDto;
import psp.util.upi.client.UpiClientService;

@Component("generatePinMpfb")
public class GeneratePinProcessImpl extends MobileCoreProcess {

	private static final Logger LOGGER = Logger.getLogger(GeneratePinProcessImpl.class.getName());
	
	@Autowired
	private PspMgmtService pspMgmtService;
	
	@Autowired
	private UpiClientService upiClient;
	
	@Autowired
	private PropertyReader propertyReader;
	
	public GeneratePinProcessImpl() {
	}
	
	@Override
	public MessageResponse validateRequest() {
		GeneratePinResponse response = new GeneratePinResponse();
		response.vaildate();
		return response;
	}

	@Override
	public void doProcess(MessageResponse response) throws ApplicationException {
		LOGGER.info("doProcess of GeneratePinProcessImpl started ");
		GeneratePinRequest generatePinRequest = (GeneratePinRequest) request;
		Date date = new Date();
		String messageId = DtoObjectUtil.constructMessageId();
		HeadDto head = DtoObjectUtil.constructHeadDto(messageId, date);
		String id = generatePinRequest.getTxnId();
		TxnDto txn = DtoObjectUtil.constructTxnDto(id, ServiceNotes.REG_MOB, null, null, null, date, ServiceRequestName.REQ_MOB_REG);	
		String fullName = user.getFirstName() + CommonConstants.SPACE_STR + user.getMiddleName() + CommonConstants.SPACE_STR + user.getLastName();
		String payerType = PayerType.PERSON.name();
		AccountDetails acDetails = pspMgmtService.getAccountDetailsByUserDetailsId(user.getId());
		AcDto acDto = MobileProcessUtility.getAcDto(acDetails);
		PayerDto payerDto = DtoObjectUtil.constructPayerDto(acDetails.getVirtualAddress(), fullName, payerType, acDto, null, null, null, request.getDevice(), null);
		List<CredDto> creds = new ArrayList<CredDto>();
		CredDto credDto = new CredDto();
		credDto.setData(generatePinRequest.getMobileCredentials().getDataValue());
		credDto.setType(CredType.PIN.name());
		credDto.setSubType(CredSubType.MPIN.name());
		creds.add(credDto);
		RegDetailsDto regDetailsDto = DtoObjectUtil.constructRegDetailsDto(request.getDevice().getMobile(), generatePinRequest.getCardDigits(), generatePinRequest.getExpiryDate(), creds);
		String ackXml = upiClient.regMob(head, txn, payerDto, regDetailsDto);
		Ack ackRes = PspClientTool.convertUpiRequest(ackXml, Ack.class);
		boolean respReceived = false;
		if(ackRes.getErr() == null) {
			pspMgmtService.saveUpiRequest(MobileProcessUtility.prepareUpiRequestDetails(messageId, id, GeneratePinUpiRequest.class));
			int maxDelay = 270; //seconds
			int count = 0;
			do {
				GeneratePinUpiRequest genPin =  (GeneratePinUpiRequest) pspMgmtService.getUpiRequestByTxnId(id);
				if (ServiceStatus.RECEIVED.name().equals(genPin.getStatus())) {
					respReceived = true;
					if(CommonConstants.SUCCESS.equals(genPin.getResult())) {
						prepareResponse(genPin, response);
					}
					else{
						prepareResponse(genPin, response);
						//throw new ApplicationException(StatusCode.GEN_PIN_REQUEST_FAILED);
					}
				}	
				try{
					count++;
					Thread.sleep(propertyReader.getMobileProcessDelay());
				}
				catch(Exception ex){
				}
				if(count > maxDelay){
					LOGGER.info("UPI Request time out");
					prepareResponse(genPin, response);
					//throw new ApplicationException(StatusCode.GEN_PIN_REQUEST_FAILED);
				}
			}while (!respReceived);
		}
		else {
			throw new ApplicationException(StatusCode.GEN_PIN_REQUEST_FAILED);
		}
		LOGGER.info("doProcess of GeneratePinProcessImpl completed ");
		
	}

	@Override
	public MessageResponse createResponseOnStatusCode(StatusCode code) {
		GeneratePinResponse response = new GeneratePinResponse();
		response.setStatusCode(code.getCode());
		response.setStatusMessage(code.getMessage());
		return response;
	}

	private void prepareResponse(GeneratePinUpiRequest genPin, MessageResponse response) {
		pspMgmtService.deleteUpiRequest(genPin);
		response.setStatusCode(StatusCode.SUCCESS.getCode());
		response.setStatusMessage(StatusCode.GEN_PIN_SERVICE_SUCCESS.getMessage());
	}
	
}
