/**
 * 
 */
package psp.mobile.process.factory.impl;

import java.util.Date;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import psp.common.exception.ApplicationException;
import psp.constants.StatusCode;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.DeviceDetails;
import psp.dbservice.model.LoginDetails;
import psp.dbservice.model.UserDetails;
import psp.mobile.model.request.LoginRequest;
import psp.mobile.model.response.LoginResponse;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.process.factory.MobileCoreProcess;
import psp.mobile.process.util.MobileProcessUtility;

/**
 * @author prasadj
 *
 */
@Component("loginMpfb")
public class LoginProcessImpl extends MobileCoreProcess {

	private static final Logger LOGGER = Logger.getLogger(LoginProcessImpl.class.getName());
	
	@Autowired
	private PspMgmtService pspMgmtService;	
	
	public LoginProcessImpl(){		
	}
	
	@Override
	public MessageResponse validateRequest() {
		LoginResponse response = new LoginResponse();
		response.validate((LoginRequest) request);
		return response;
	}

	@Override
	public void doProcess(MessageResponse response) throws ApplicationException {
		LOGGER.info("doProcess of LoginProcessImpl started ");
		LoginResponse res = (LoginResponse) response;
		LoginRequest req = (LoginRequest) request;
		UserDetails user = pspMgmtService.verifyUser(request.getUserName(), req.getPassword());
		if (null != user) {
			DeviceDetails deviceFromDb = pspMgmtService.getDeviceDetailsByUserId(user.getId());
			StatusCode statusCode = MobileProcessUtility.validateMobileDeviceDetails(deviceFromDb, request.getDevice());
			if(StatusCode.SUCCESS == statusCode) {
				 LoginDetails loginDetails = pspMgmtService.getLoginDetailsByUserName(req.getUserName());
				 if (null != loginDetails) {
					loginDetails.setLoginTime(new Date());
					pspMgmtService.updateLoginDetails(loginDetails);
				} else {
					pspMgmtService.saveLoginDetails(MobileProcessUtility.prepareLoginDetails(user.getUserName(), deviceFromDb.getTerminalId()));					
				}
				res.setAccountSummary(pspMgmtService.getAccountSummaryByUserDetailsId(user.getId()));
			}
			else {
				throw new ApplicationException(StatusCode.DEVICE_VALIDATION_FAILED) ;
			}
		} else {
			throw new ApplicationException(StatusCode.USERNAME_OR_PASSWORD_INCORRECT) ;
		}
		LOGGER.info("doProcess of LoginProcessImpl completed ");
	}

	@Override
	public MessageResponse createResponseOnStatusCode(StatusCode code) {
		LoginResponse response = new LoginResponse();
		response.setStatusCode(code.getCode());
		response.setStatusMessage(code.getMessage());
		return response;
	}

}