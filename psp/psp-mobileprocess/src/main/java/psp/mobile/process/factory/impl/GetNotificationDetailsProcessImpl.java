package psp.mobile.process.factory.impl;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.upi.system_1_2.ReqAuthDetails;

import psp.common.exception.ApplicationException;
import psp.constants.NotificationType;
import psp.constants.StatusCode;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.NotificationDetails;
import psp.dbservice.model.UpiMessagePacket;
import psp.mobile.model.request.GetNotificationDetailsRequest;
import psp.mobile.model.response.CollectPayNotification;
import psp.mobile.model.response.GetNotificationDetailsResponse;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.process.factory.MobileCoreProcess;
import psp.util.PspClientTool;

@Component("getNotificationDetailsMpfb")
public class GetNotificationDetailsProcessImpl extends MobileCoreProcess {

	private static final Logger LOGGER = Logger.getLogger(GetNotificationDetailsProcessImpl.class.getName());
	
	@Autowired
	private PspMgmtService pspMgmtService;
	
	public GetNotificationDetailsProcessImpl() {
	}
	
	@Override
	public MessageResponse validateRequest() {
		GetNotificationDetailsResponse response = new GetNotificationDetailsResponse();
		response.validate();
		return response;
	}

	@Override
	public void doProcess(MessageResponse response) throws ApplicationException {
		LOGGER.info("doProcess of GetNotificationDetailsProcessImpl started ");
		GetNotificationDetailsRequest req = (GetNotificationDetailsRequest) request;
		GetNotificationDetailsResponse res = (GetNotificationDetailsResponse) response;
		NotificationDetails notification = pspMgmtService.getNotificationDetailsByNotificationId(req.getNotificationId());
		if(null != notification) {
			if(NotificationType.COLLECT.name().equals(notification.getNotificationtType())) {
				CollectPayNotification collectPayNotification = new CollectPayNotification();
				ReqAuthDetails reqAuthDetails = PspClientTool.convertUpiRequest(notification.getUpiMessagePacket().getMessagePacket(), ReqAuthDetails.class);
				if(null != reqAuthDetails) {
					collectPayNotification.setAmount(reqAuthDetails.getPayer().getAmount().getValue());
					collectPayNotification.setNotificationId(notification.getNotificationId());
					//res.setPayeeMobileNumber();
					collectPayNotification.setRequestorName(reqAuthDetails.getPayees().getPayee().get(0).getName());
					collectPayNotification.setRequestorNote(reqAuthDetails.getTxn().getNote());
					collectPayNotification.setRequestorVirtualAddress(reqAuthDetails.getPayees().getPayee().get(0).getAddr());
					collectPayNotification.setSelfVitrualAddress(reqAuthDetails.getPayer().getAddr());
					collectPayNotification.setNotificationMessage(notification.getNotificationMessage());
					collectPayNotification.setNotificationtType(notification.getNotificationtType());
					collectPayNotification.setTxnId(reqAuthDetails.getTxn().getId());
					res.setNotification(collectPayNotification);
				}
			}
			else {
				pspMgmtService.updateNotificationDetailsByNotificationId(req.getNotificationId());	
			}
		}
		else {
			throw new ApplicationException(StatusCode.INVALID_NOTIFICATION_ID);
		}
		LOGGER.info("doProcess of GetNotificationDetailsProcessImpl completed ");
	}

	@Override
	public MessageResponse createResponseOnStatusCode(StatusCode code) {
		GetNotificationDetailsResponse response = new GetNotificationDetailsResponse();
		response.setStatusCode(code.getCode());
		response.setStatusMessage(code.getMessage());
		return response;
	}

}
