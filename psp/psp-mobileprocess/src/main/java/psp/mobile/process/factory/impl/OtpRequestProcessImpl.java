/**
 * 
 */
package psp.mobile.process.factory.impl;

import java.util.Date;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.upi.system_1_2.Ack;

import psp.common.PropertyReader;
import psp.common.exception.ApplicationException;
import psp.constants.CommonConstants;
import psp.constants.ServiceNotes;
import psp.constants.ServiceRequestName;
import psp.constants.ServiceStatus;
import psp.constants.StatusCode;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.AccountDetails;
import psp.dbservice.model.UpiRequest;
import psp.mobile.model.request.AcDetails;
import psp.mobile.model.request.OtpRequest;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.model.response.OtpResponse;
import psp.mobile.process.factory.MobileCoreProcess;
import psp.mobile.process.util.MobileProcessUtility;
import psp.upi.constants.PayerType;
import psp.util.DtoObjectUtil;
import psp.util.PspClientTool;
import psp.util.dto.AcDto;
import psp.util.dto.HeadDto;
import psp.util.dto.PayerDto;
import psp.util.dto.TxnDto;
import psp.util.upi.client.UpiClientService;

/**
 * @author prasadj
 *
 */
@Component("otpRequestMpfb")
public class OtpRequestProcessImpl extends MobileCoreProcess {

	private static final Logger LOGGER = Logger.getLogger(OtpRequestProcessImpl.class.getName());
	
	@Autowired
	private PspMgmtService pspMgmtService;
	
	@Autowired
	private UpiClientService upiClient;
	
	@Autowired
	private PropertyReader propertyReader;
	
	public OtpRequestProcessImpl(){
	}
	
	
	@Override
	public MessageResponse validateRequest() {
		OtpResponse response = new OtpResponse();
		response.validate(((OtpRequest)request).getAcDetails());
		return response;
	}

	@Override
	public void doProcess(MessageResponse response) throws ApplicationException {
		LOGGER.info("doProcess of OtpRequestProcessImpl started ");
		Date date = new Date();
		String messageId = DtoObjectUtil.constructMessageId();
		HeadDto head = DtoObjectUtil.constructHeadDto(messageId, date);
		String id = DtoObjectUtil.constructTxnId();
		String note = ServiceNotes.OTP_NOTE;
		String orgTxnId = null;
		String refId = messageId + 1;
		String refUrl = "";
		String type = ServiceRequestName.REQ_OTP;
		TxnDto txn = DtoObjectUtil.constructTxnDto(id, note, orgTxnId, refId, refUrl, date, type);	
		String fullName = user.getFirstName() + CommonConstants.SPACE_STR + user.getMiddleName() + CommonConstants.SPACE_STR + user.getLastName();
		String payerType = PayerType.PERSON.name();
		AcDetails acDetails = ((OtpRequest)request).getAcDetails();
		AcDto acDto = MobileProcessUtility.getAcDto(acDetails);

		String addr = acDetails.getAcnum() + "@" + acDetails.getIfsc() + ".ifsc.npci" ;//TODO need to change
		
		PayerDto payerDto = DtoObjectUtil.constructPayerDto(addr, fullName, payerType, acDto, null, null, null, request.getDevice(), null);
		String ackXml = upiClient.reqOtp(head, txn, payerDto);
		Ack ackRes = PspClientTool.convertUpiRequest(ackXml, Ack.class);
		boolean respReceived = false;
		if(ackRes.getErr() == null) {
			pspMgmtService.saveAccountDetails(prepareOnAddressType(acDetails));
			pspMgmtService.saveUpiRequest(MobileProcessUtility.prepareOtpReqDetails((OtpRequest)request, messageId, id));
			int maxDelay = 270; //seconds
			int count = 0;
			do {
				UpiRequest otpRequestDetails =  pspMgmtService.getUpiRequestByTxnId(id);
				if (ServiceStatus.RECEIVED.name().equals(otpRequestDetails.getStatus())) {
					respReceived = true;
					if(CommonConstants.SUCCESS.equals(otpRequestDetails.getResult())) {
						prepareResponse(otpRequestDetails, response);
					}
					else{
						prepareResponse(otpRequestDetails, response);
						//throw new ApplicationException(StatusCode.OTP_REQUEST_FAILED);
					}
				}	
				try{
					count++;
					Thread.sleep(propertyReader.getMobileProcessDelay());
				}
				catch(Exception ex){
				}
				if(count > maxDelay){
					LOGGER.info("UPI Request time out ");
					throw new ApplicationException(StatusCode.OTP_REQUEST_FAILED);
				}
			}while (!respReceived);
		}
		else {
			throw new ApplicationException(StatusCode.OTP_REQUEST_FAILED);
		}
		LOGGER.info("doProcess of OtpRequestProcessImpl completed ");
	}

	@Override
	public MessageResponse createResponseOnStatusCode(StatusCode code) {
		OtpResponse response = new OtpResponse();
		response.setStatusCode(code.getCode());
		response.setStatusMessage(code.getMessage());
		return response;
	}
	
	private void prepareResponse(UpiRequest otpRequestDetails, MessageResponse response) {
		pspMgmtService.deleteUpiRequest(otpRequestDetails);
		response.setStatusCode(StatusCode.OTP_SERVICE_SUCCESS.getCode());
		response.setStatusMessage(StatusCode.OTP_SERVICE_SUCCESS.getMessage());
	}

	private AccountDetails prepareOnAddressType(AcDetails acDetails) {
		AccountDetails accDetails = new AccountDetails();
		String addresstype = acDetails.getAddrType();
		switch(addresstype) {
		case ("ACCOUNT"):
			accDetails.setAccountNumber(acDetails.getAcnum());
			accDetails.setAccountType(acDetails.getActype());
			accDetails.setIfsc(acDetails.getIfsc());
		    break;
		case "MOBILE":
			accDetails.setMmid(accDetails.getMmid());
			accDetails.setMobileNumber(acDetails.getMobnum());
			break;
		//TODO Card and Aadhaar	
		}	
		accDetails.setUserDetails(user);
		accDetails.setAddressType(acDetails.getAddrType());
		return accDetails;
	}
}