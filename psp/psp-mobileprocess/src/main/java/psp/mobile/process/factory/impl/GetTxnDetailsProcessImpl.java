/**
 * 
 */
package psp.mobile.process.factory.impl;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import psp.common.exception.ApplicationException;
import psp.constants.StatusCode;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.AccountDetails;
import psp.dbservice.model.TransactionDetails;
import psp.mobile.model.request.GetTxnDetailsRequest;
import psp.mobile.model.response.GetTxnsSummaryResponse;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.process.factory.MobileCoreProcess;
import psp.mobile.process.util.MobileProcessUtility;

/**
 * @author prasadj
 *
 */
@Component("txnDetailsMpfb")
public class GetTxnDetailsProcessImpl extends MobileCoreProcess {

	private static final Logger LOGGER = Logger.getLogger(GetTxnDetailsProcessImpl.class.getName());
	
	@Autowired
	private PspMgmtService pspMgmtService;
	
	public GetTxnDetailsProcessImpl() {
	}
	
	@Override
	public MessageResponse validateRequest() {
		GetTxnsSummaryResponse response = new GetTxnsSummaryResponse();
		response.validate((GetTxnDetailsRequest)request);
		return response;
	}

	
	@Override
	public void doProcess(MessageResponse response) throws ApplicationException {
		LOGGER.info("doProcess of GetTxnDetailsProcessImpl started ");
		GetTxnDetailsRequest req = (GetTxnDetailsRequest) request;
		GetTxnsSummaryResponse res = (GetTxnsSummaryResponse) response;
		AccountDetails accountDetails = pspMgmtService.getAccountDetailsByUserDetailsId(user.getId());
		if(null != accountDetails) {
			List<TransactionDetails> transactionDetails = pspMgmtService.getTransactionDetailsByDateAndVirtualAddr(req.getFromDate(), req.getToDate(), accountDetails.getVirtualAddress());
			res.setTxnSummaryList(MobileProcessUtility.prepareTxnsSummaryResponse(transactionDetails));
		}
		else {
			throw new ApplicationException(StatusCode.GET_TXN_DETAILS_SERVICE_FAIL);
		}
		
		LOGGER.info("doProcess of GetTxnDetailsProcessImpl completed ");
	}

	@Override
	public MessageResponse createResponseOnStatusCode(StatusCode code) {
		GetTxnsSummaryResponse response = new GetTxnsSummaryResponse();
		response.setStatusCode(code.getCode());
		response.setStatusMessage(code.getMessage());
		return response;
	}

}