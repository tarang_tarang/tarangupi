package psp.mobile.process.factory.impl;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import psp.common.exception.ApplicationException;
import psp.constants.StatusCode;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.KeyInfo;
import psp.mobile.model.response.GetKeyResponse;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.process.factory.MobileCoreProcess;

@Component("getKeyMpfb")
public class GetKeyProcessImpl extends MobileCoreProcess {

	private static final Logger LOGGER = Logger.getLogger(GetKeyProcessImpl.class.getName());
	
	@Autowired
	private PspMgmtService pspMgmtService;
	
	public GetKeyProcessImpl() {
	}
	
	@Override
	public MessageResponse validateRequest() {
		GetKeyResponse response = new GetKeyResponse();
        response.validate();
		return response;
	}

	@Override
	public void doProcess(MessageResponse response) throws ApplicationException {
		LOGGER.info("doProcess of GetKeyProcessImpl started ");
		GetKeyResponse mobRes = (GetKeyResponse) response;
		KeyInfo keyInfo = pspMgmtService.getKeyInfo();
		mobRes.setPublicKey(keyInfo.getXmlPayload());
		LOGGER.info("doProcess of GetKeyProcessImpl completed ");
		
	}

	@Override
	public MessageResponse createResponseOnStatusCode(StatusCode code) {
		GetKeyResponse response = new GetKeyResponse();
		response.setStatusCode(code.getCode());
		response.setStatusMessage(code.getMessage());
		return response;
	}

}
