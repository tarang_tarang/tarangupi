/**
 * 
 */
package psp.mobile.process.factory.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.upi.system_1_2.Ack;
import org.upi.system_1_2.CredSubType;

import psp.common.PropertyReader;
import psp.common.exception.ApplicationException;
import psp.constants.CommonConstants;
import psp.constants.ServiceNotes;
import psp.constants.ServiceRequestName;
import psp.constants.ServiceStatus;
import psp.constants.StatusCode;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.AccountDetails;
import psp.dbservice.model.BalanceEnquiryUpiRequest;
import psp.mobile.model.request.BalanceEnquiryRequest;
import psp.mobile.model.response.BalanceEnquiryResponse;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.process.factory.MobileCoreProcess;
import psp.mobile.process.util.MobileProcessUtility;
import psp.upi.constants.CredType;
import psp.upi.constants.PayerType;
import psp.util.DtoObjectUtil;
import psp.util.PspClientTool;
import psp.util.dto.AcDto;
import psp.util.dto.CredDto;
import psp.util.dto.HeadDto;
import psp.util.dto.PayerDto;
import psp.util.dto.TxnDto;
import psp.util.upi.client.UpiClientService;

/**
 * @author prasadj
 *
 */
@Component("balanceEnquiryMpfb")
public class GetBalanceEnquiryProcessImpl extends MobileCoreProcess{

	private static final Logger LOGGER = Logger.getLogger(GetBalanceEnquiryProcessImpl.class.getName());
	
	@Autowired
	private PspMgmtService pspMgmtService;
	
	@Autowired
	private UpiClientService upiClient;
	
	@Autowired
	private PropertyReader propertyReader;
	
	public GetBalanceEnquiryProcessImpl(){
	}
	
	@Override
	public MessageResponse validateRequest() {
		BalanceEnquiryResponse response = new BalanceEnquiryResponse();
		response.validate();
		return response;
	}

	@Override
	public void doProcess(MessageResponse response) throws ApplicationException {
		LOGGER.info("doProcess of GetBalanceEnquiryProcessImpl started ");
		BalanceEnquiryRequest req = (BalanceEnquiryRequest)request;
		Date date = new Date();
		String messageId = DtoObjectUtil.constructMessageId();
		HeadDto head = DtoObjectUtil.constructHeadDto(messageId, date);
		String id = req.getTxnId();
		String note = ServiceNotes.BAL_ENQ;
		
		String type = ServiceRequestName.REQ_BAL_ENQ;
		TxnDto txn = DtoObjectUtil.constructTxnDto(id, note, null, "", "", date, type);
		
		String fullName = user.getFirstName() + CommonConstants.SPACE_STR + user.getMiddleName() + CommonConstants.SPACE_STR + user.getLastName();
		String payerType = PayerType.PERSON.name();
		
		AccountDetails acDetails = pspMgmtService.getAccountDetailsByUserDetailsId(user.getId());
		AcDto acDto = MobileProcessUtility.getAcDto(acDetails);
		
		//String addr = acDetails.getAcnum() + "@" + acDetails.getIfsc() + ".ifsc.npci" ;//TODO need to change
		
		PayerDto payerDto = DtoObjectUtil.constructPayerDto(acDetails.getVirtualAddress(), fullName, payerType, acDto, null, null, null, request.getDevice(), null);
		
		List<CredDto> creds = new ArrayList<CredDto>();
		CredDto credDto = new CredDto();
		credDto.setData(req.getMobileCredentials().getDataValue());
		credDto.setType(CredType.PIN.name());
		credDto.setSubType(CredSubType.MPIN.name());
		creds.add(credDto);
		payerDto.setCreds(creds);
		
		String responseXml = upiClient.reqBalEnq(head, txn, payerDto);
		Ack ack = PspClientTool.convertUpiRequest(responseXml, Ack.class);
		boolean respReceived = false;
		BalanceEnquiryResponse mobileRes = (BalanceEnquiryResponse) response;
		if(ack.getErr() == null) {
			pspMgmtService.saveUpiRequest(MobileProcessUtility.prepareUpiRequestDetails(messageId, id, BalanceEnquiryUpiRequest.class));
			do {
				BalanceEnquiryUpiRequest balResp = (BalanceEnquiryUpiRequest) pspMgmtService.getUpiRequestByTxnId(id);
				if (ServiceStatus.RECEIVED.name().equals(balResp.getStatus())) {
					respReceived = true;
					if(CommonConstants.SUCCESS.equals(balResp.getResult())) {
						prepareResponse(balResp.getBalance(), mobileRes, response, balResp);
					}
					else {
						prepareResponse("20000", mobileRes, response, balResp);
						//throw new ApplicationException(StatusCode.BAL_ENQ_SERVICE_FAIL);
					}
				}		
				try{
					Thread.sleep(propertyReader.getMobileProcessDelay());
				}
				catch(Exception ex){
				}
			}while (!respReceived);
		}
		else {
			throw new ApplicationException(StatusCode.BAL_ENQ_SERVICE_FAIL);
		}
		LOGGER.info("doProcess of GetBalanceEnquiryProcessImpl completed ");
	}

	@Override
	public MessageResponse createResponseOnStatusCode(StatusCode code) {
		BalanceEnquiryResponse response = new BalanceEnquiryResponse();
		response.setStatusCode(code.getCode());
		response.setStatusMessage(code.getMessage());
		return response;
	}
	
	private void prepareResponse(String bal, BalanceEnquiryResponse mobileRes, MessageResponse response, BalanceEnquiryUpiRequest balResp) {
		mobileRes.setBalance(bal);
		pspMgmtService.deleteUpiRequest(balResp);
		response.setStatusCode(StatusCode.BAL_ENQ_SERVICE_SUCCESS.getCode());
		response.setStatusMessage(StatusCode.BAL_ENQ_SERVICE_SUCCESS.getMessage());
	}

}