package psp.mobile.process.factory.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import psp.common.exception.ApplicationException;
import psp.constants.StatusCode;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.NotificationDetails;
import psp.mobile.model.request.MobileNotification;
import psp.mobile.model.response.GetNotificationListResponse;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.process.factory.MobileCoreProcess;
import psp.mobile.process.util.MobileProcessUtility;

@Component("getNotificationListMpfb")
public class GetNotificationListProcessImpl extends MobileCoreProcess {

	private static final Logger LOGGER = Logger.getLogger(GetNotificationListProcessImpl.class.getName());
	
	@Autowired
	private PspMgmtService pspMgmtService;
	
	public GetNotificationListProcessImpl() {
	}
	
	@Override
	public MessageResponse validateRequest() {
		GetNotificationListResponse response = new GetNotificationListResponse();
		response.validate();
		return response;
	}

	@Override
	public void doProcess(MessageResponse response) throws ApplicationException {
		LOGGER.info("doProcess of GetNotificationListProcessImpl started ");
		GetNotificationListResponse getNotificationListResponse = (GetNotificationListResponse) response;
		List<NotificationDetails> notifications = pspMgmtService.getInitiatedNotificationsByUserName(request.getUserName());
		List<MobileNotification> mobnotifications = new ArrayList<MobileNotification>();
		if(null != notifications) {
			for(NotificationDetails notification: notifications) {
				mobnotifications.add(MobileProcessUtility.prepareMobileNotifications(notification));
			}
		}
		getNotificationListResponse.setNotifications(mobnotifications);
		LOGGER.info("doProcess of GetNotificationListProcessImpl completed ");
		
	}

	@Override
	public MessageResponse createResponseOnStatusCode(StatusCode code) {
		GetNotificationListResponse response = new GetNotificationListResponse();
		response.setStatusCode(code.getCode());
		response.setStatusMessage(code.getMessage());
		return response;
	}

}
