package psp.mobile.process.factory.impl;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import psp.common.exception.ApplicationException;
import psp.constants.StatusCode;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.BeneficiaryDetails;
import psp.mobile.model.request.GetBeneficiaryListRequest;
import psp.mobile.model.response.GetBeneficiaryListResponse;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.process.factory.MobileCoreProcess;
import psp.mobile.process.util.MobileProcessUtility;

@Component("getBeneficiaryListMpfb")
public class GetBeneficiaryListProcessImpl extends MobileCoreProcess {

	private static final Logger LOGGER = Logger.getLogger(GetBeneficiaryListProcessImpl.class.getName());
	
	@Autowired
	private PspMgmtService pspMgmtService;
	
	public GetBeneficiaryListProcessImpl() {
	}
	
	@Override
	public MessageResponse validateRequest() {
		GetBeneficiaryListResponse response = new GetBeneficiaryListResponse();
		response.validate();
		return response;
	}
	
	@Override
	public void doProcess(MessageResponse response) throws ApplicationException {
		LOGGER.info("doProcess of GetBeneficiaryListProcessImpl started ");
		GetBeneficiaryListResponse res = (GetBeneficiaryListResponse) response;
		GetBeneficiaryListRequest benReq = (GetBeneficiaryListRequest) request;
		List<BeneficiaryDetails> benList =  pspMgmtService.getBeneficiaryDetailsByUserName(user.getUserName());
		res.setBenefSummaries(MobileProcessUtility.convertBeneficiaryDetailsToBeneficiarySummary(benList, benReq.getRequestType()));
		LOGGER.info("doProcess of GetBeneficiaryListProcessImpl completed ");
	}

	@Override
	public MessageResponse createResponseOnStatusCode(StatusCode code) {
		GetBeneficiaryListResponse response = new GetBeneficiaryListResponse();
		response.setStatusCode(code.getCode());
		response.setStatusMessage(code.getMessage());
		return response;
	}

}
