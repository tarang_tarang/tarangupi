package psp.mobile.process.account.service;

import java.util.List;

import psp.dbservice.model.AccountReference;

public interface AccountListCacheService {

	void addUserAccounts(String userName, List<AccountReference> accSummary);
	
	List<AccountReference> getUserAccounts(String userName);
	
	void deleteUserAccounts(String userName);

}