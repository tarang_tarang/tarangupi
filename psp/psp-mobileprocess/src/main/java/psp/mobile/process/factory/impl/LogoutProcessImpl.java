package psp.mobile.process.factory.impl;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import psp.constants.StatusCode;
import psp.dbservice.mgmt.PspMgmtService;
import psp.mobile.model.response.LogoutResponse;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.process.factory.MobileCoreProcess;

@Component("logoutMpfb")
public class LogoutProcessImpl extends MobileCoreProcess {

	private static final Logger LOGGER = Logger.getLogger(LogoutProcessImpl.class.getName());
	
	@Autowired
	private PspMgmtService pspMgmtService;
	
	public LogoutProcessImpl() {
	}
	
	@Override
	public MessageResponse validateRequest() {
		LogoutResponse response = new LogoutResponse();
		response.validate();
		return response;
	}

	@Override
	public void doProcess(MessageResponse response) {
		LOGGER.info("doProcess of LogoutProcessImpl started ");
		pspMgmtService.deleteLoginDetails(loginDetails);
		LOGGER.info("doProcess of LogoutProcessImpl completed ");
	}

	@Override
	public MessageResponse createResponseOnStatusCode(StatusCode code) {
		LogoutResponse response = new LogoutResponse();
		response.setStatusCode(code.getCode());
		response.setStatusMessage(code.getMessage());
		return response;
	}

}
