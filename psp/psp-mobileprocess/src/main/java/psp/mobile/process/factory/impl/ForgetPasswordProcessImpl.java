package psp.mobile.process.factory.impl;

import java.util.logging.Logger;

import org.springframework.stereotype.Component;

import psp.constants.StatusCode;
import psp.mobile.model.response.ForgetPasswordResponse;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.process.factory.MobileCoreProcess;

@Component("forgetPasswordMpfb")
public class ForgetPasswordProcessImpl extends MobileCoreProcess {

	private static final Logger LOGGER = Logger.getLogger(ForgetPasswordProcessImpl.class.getName());
	
	public ForgetPasswordProcessImpl(){
	}
	
	@Override
	public MessageResponse validateRequest() {
		ForgetPasswordResponse response = new ForgetPasswordResponse();
		response.validate();
		return response;
	}

	@Override
	public void doProcess(MessageResponse response) {
		// TODO Auto-generated method stub
		LOGGER.info("doProcess of ForgetPasswordProcessImpl started ");
		LOGGER.info("doProcess of ForgetPasswordProcessImpl completed ");

	}

	@Override
	public MessageResponse createResponseOnStatusCode(StatusCode code) {
		ForgetPasswordResponse response = new ForgetPasswordResponse();
		response.setStatusCode(code.getCode());
		response.setStatusMessage(code.getMessage());
		return response;
	}

}