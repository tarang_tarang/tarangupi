package psp.mobile.process.factory.impl;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import psp.common.exception.ApplicationException;
import psp.constants.StatusCode;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.AccountReference;
import psp.dbservice.model.BeneficiaryAccountDetails;
import psp.dbservice.model.BeneficiaryDetails;
import psp.mobile.model.request.AddBeneficiaryRequest;
import psp.mobile.model.response.AddBeneficiaryResponse;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.process.account.service.AccountListCacheService;
import psp.mobile.process.factory.MobileCoreProcess;
import psp.mobile.process.util.MobileProcessUtility;

@Component("addBeneficiaryMpfb")
public class AddBeneficiaryProcessImpl extends MobileCoreProcess {

	private static final Logger LOGGER = Logger.getLogger(AddBeneficiaryProcessImpl.class.getName());
	
	@Autowired
	private PspMgmtService pspMgmtService;

	@Autowired
	private AccountListCacheService accountListCacheService;
	
	public AddBeneficiaryProcessImpl() {	
	}
	
	@Override
	public MessageResponse validateRequest() {
		AddBeneficiaryResponse response = new AddBeneficiaryResponse();
		response.validate((AddBeneficiaryRequest) request);
		return response;
	}

	@Override
	public void doProcess(MessageResponse response) throws ApplicationException {
		LOGGER.info("doProcess of AddBeneficiaryProcessImpl started ");
		AddBeneficiaryRequest req = (AddBeneficiaryRequest) request;		
		BeneficiaryDetails bnf = new BeneficiaryDetails();
		if("MOBILE".equals(req.getType()) || "AADHAAR".equals(req.getType()) ){
			List<AccountReference> cacheList = accountListCacheService.getUserAccounts(request.getUserName());
			for(AccountReference actRef: cacheList) {
				if(actRef.getId().equals(req.getId())) {				
					BeneficiaryAccountDetails bnfAcc = new BeneficiaryAccountDetails();
					MobileProcessUtility.updateBenificiaryAccountDetails(actRef, bnfAcc);
					bnf.setAccountDetails(bnfAcc);
					accountListCacheService.deleteUserAccounts(req.getUserName());
				}
			}
		}
		MobileProcessUtility.updateBenificiary(bnf, req.getUserName(), req.getNickName(), req.getAddress(), req.getType(), req.getBeneficiaryNote());
		pspMgmtService.saveBeneficiaryDetails(bnf);	
		LOGGER.info("doProcess of AddBeneficiaryProcessImpl completed ");
	}

	@Override
	public MessageResponse createResponseOnStatusCode(StatusCode code) {
		AddBeneficiaryResponse response = new AddBeneficiaryResponse();
		response.setStatusCode(code.getCode());
		response.setStatusMessage(code.getMessage());
		return response;
	}

}