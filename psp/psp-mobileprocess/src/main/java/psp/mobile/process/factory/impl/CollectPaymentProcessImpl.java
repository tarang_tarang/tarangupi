package psp.mobile.process.factory.impl;

import java.util.Date;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.upi.system_1_2.Ack;
import org.upi.system_1_2.PayConstant;

import psp.common.exception.ApplicationException;
import psp.constants.CommonConstants;
import psp.constants.StatusCode;
import psp.constants.TxnStatus;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.AccountDetails;
import psp.dbservice.model.TransactionDetails;
import psp.mobile.model.request.CollectPaymentRequest;
import psp.mobile.model.response.CollectPaymentResponse;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.process.factory.MobileCoreProcess;
import psp.mobile.process.util.MobileProcessUtility;
import psp.upi.constants.PayerType;
import psp.util.DtoObjectUtil;
import psp.util.PspClientTool;
import psp.util.dto.AcDto;
import psp.util.dto.HeadDto;
import psp.util.dto.IdentityInfoDto;
import psp.util.dto.PayeeDto;
import psp.util.dto.PayerDto;
import psp.util.dto.TxnDto;
import psp.util.upi.client.UpiClientService;

@Component("collectPaymentMpfb")
public class CollectPaymentProcessImpl extends MobileCoreProcess {

	private static final Logger LOGGER = Logger.getLogger(CollectPaymentProcessImpl.class.getName());

	@Autowired
	private PspMgmtService pspMgmtService;
	
	@Autowired
	private UpiClientService upiClientService;
	
	public CollectPaymentProcessImpl() {
	}
	
	@Override
	public MessageResponse validateRequest() {
		CollectPaymentResponse response = new CollectPaymentResponse();
		response.validate((CollectPaymentRequest)request);
		return response;
	}

	@Override
	public void doProcess(MessageResponse response) throws ApplicationException {
		LOGGER.info("doProcess of CollectPaymentProcessImpl started ");
		CollectPaymentRequest req = (CollectPaymentRequest)request;
		String messageId = DtoObjectUtil.constructMessageId();
		String id = DtoObjectUtil.constructTxnId();
		Date date = new Date();
		HeadDto head = DtoObjectUtil.constructHeadDto(messageId, date);
		TxnDto txn = DtoObjectUtil.constructTxnDto(id, req.getTxnNote(), "", "", "", date, PayConstant.COLLECT.name());
		txn.setRuleRequired(true);
		
		String fullName = user.getFirstName() + CommonConstants.SPACE_STR + user.getMiddleName() + CommonConstants.SPACE_STR + user.getLastName();
		String payerType = PayerType.PERSON.name();
		AccountDetails accDetails = pspMgmtService.getAccountDetailsByVirtualAddr(req.getSelfVirtualAddr()); 
		AcDto acDto = null;
		if(null != accDetails) {
			acDto  = MobileProcessUtility.getAcDto(accDetails);
		}
		else {
			throw new ApplicationException(StatusCode.ACC_DETAILS_NOT_FOUND);
		}		
		
		IdentityInfoDto identityInfoDto = DtoObjectUtil.constructIdentityInfoDto(req.getSelfVirtualAddr());
		PayeeDto payeeDto = DtoObjectUtil.constructPayeeDto(req.getSelfVirtualAddr(), fullName, payerType, acDto, req.getAmount().toString(), null, null, request.getDevice(), identityInfoDto);
		
		PayerDto payerDto = DtoObjectUtil.constructPayerDto(req.getThirdPartyVirtualAddr(), null, payerType, null, req.getAmount().toString(), null, null, null, null);
		pspMgmtService.saveTransactionDetails(prepareTransactionDetails(req, id));
		
		String responseXml = upiClientService.reqPay(head, txn, payerDto, payeeDto);
		Ack ack = PspClientTool.convertUpiRequest(responseXml, Ack.class);
		if(ack.getErr() == null) {
			CollectPaymentResponse collectPaymentResponse = (CollectPaymentResponse)response;
			collectPaymentResponse.setStatusCode(StatusCode.COLLECT_INITIATION_SUCCESS.getCode());
			collectPaymentResponse.setStatusMessage(StatusCode.COLLECT_INITIATION_SUCCESS.getMessage());
		}
		else {
			throw new ApplicationException(StatusCode.COLLECT_INITIATION_FAIL);
		}
		LOGGER.info("doProcess of CollectPaymentProcessImpl completed ");
	}

	@Override
	public MessageResponse createResponseOnStatusCode(StatusCode code) {
		CollectPaymentResponse response = new CollectPaymentResponse();
		response.setStatusCode(code.getCode());
		response.setStatusMessage(code.getMessage());
		return response;
	}

	private TransactionDetails prepareTransactionDetails(CollectPaymentRequest req, String txnId) {
		TransactionDetails txnDetails = new TransactionDetails();
		txnDetails.setFromAddr(req.getSelfVirtualAddr());
		txnDetails.setToAddr(req.getThirdPartyVirtualAddr());
		txnDetails.setStatus(TxnStatus.INITIATED.name());
		txnDetails.setTxnType(PayConstant.COLLECT.name());
		txnDetails.setInitiator(true);
		txnDetails.setTxnInitiationTime(new Date());
		txnDetails.setAmount(String.valueOf(req.getAmount()));
		txnDetails.setTxnMessage(req.getTxnNote());
		txnDetails.setTxnId(txnId);
		return txnDetails;
	}
	
}
