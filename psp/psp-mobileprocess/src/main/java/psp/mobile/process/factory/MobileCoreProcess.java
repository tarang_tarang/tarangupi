/**
 * 
 */
package psp.mobile.process.factory;

import psp.common.exception.ApplicationException;
import psp.constants.StatusCode;
import psp.dbservice.model.LoginDetails;
import psp.dbservice.model.UserDetails;
import psp.mobile.model.request.MobileRequest;
import psp.mobile.model.response.MessageResponse;

/**
 * @author prasadj
 *
 */
public abstract class MobileCoreProcess {

	protected MobileRequest request;
	
	protected UserDetails user;
	
	protected LoginDetails loginDetails;
	
	public MobileCoreProcess(){
	}
	
	public abstract MessageResponse validateRequest();
	
	public abstract void doProcess(MessageResponse response) throws ApplicationException;
	
	public abstract MessageResponse createResponseOnStatusCode(StatusCode code);
	
	public void setUser(UserDetails user){
		this.user = user;
	}
	
	public void setRequest(MobileRequest request){
		this.request = request;
	}
	
	public void setLoginDetails(LoginDetails loginDetails){
		this.loginDetails = loginDetails;
	}
	
}