package psp.mobile.process.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.upi.system_1_2.PayConstant;

import psp.common.model.AccountSummary;
import psp.constants.ServiceStatus;
import psp.constants.StatusCode;
import psp.constants.UserStatus;
import psp.dbservice.model.AccountDetails;
import psp.dbservice.model.AccountProviderDetails;
import psp.dbservice.model.AccountReference;
import psp.dbservice.model.BeneficiaryAccountDetails;
import psp.dbservice.model.BeneficiaryDetails;
import psp.dbservice.model.DeviceDetails;
import psp.dbservice.model.GetAccountsUpiRequest;
import psp.dbservice.model.GetTokenUpiRequest;
import psp.dbservice.model.LoginDetails;
import psp.dbservice.model.Merchant;
import psp.dbservice.model.MobileAuditTrail;
import psp.dbservice.model.NotificationDetails;
import psp.dbservice.model.OtpUpiRequest;
import psp.dbservice.model.ReqAccountReference;
import psp.dbservice.model.TransactionDetails;
import psp.dbservice.model.UpiRequest;
import psp.dbservice.model.UserDetails;
import psp.mobile.model.request.AcDetails;
import psp.mobile.model.request.AddBankAccountRequest;
import psp.mobile.model.request.AddBeneficiaryRequest;
import psp.mobile.model.request.BalanceEnquiryRequest;
import psp.mobile.model.request.ChangePasswordRequest;
import psp.mobile.model.request.CheckUserRequest;
import psp.mobile.model.request.CollectPayConfirmRequest;
import psp.mobile.model.request.CollectPaymentRequest;
import psp.mobile.model.request.ForgetPasswordRequest;
import psp.mobile.model.request.GeneratePinRequest;
import psp.mobile.model.request.GetAccountListRequest;
import psp.mobile.model.request.GetAccountProvidersRequest;
import psp.mobile.model.request.GetBeneficiaryListRequest;
import psp.mobile.model.request.GetBeneficiaryRequest;
import psp.mobile.model.request.GetExistingAccountsRequest;
import psp.mobile.model.request.GetKeyRequest;
import psp.mobile.model.request.GetMerchantListRequest;
import psp.mobile.model.request.GetMerchantRequest;
import psp.mobile.model.request.GetNotificationDetailsRequest;
import psp.mobile.model.request.GetNotificationListRequest;
import psp.mobile.model.request.GetTokenRequest;
import psp.mobile.model.request.GetTxnDetailsRequest;
import psp.mobile.model.request.LoginRequest;
import psp.mobile.model.request.LogoutRequest;
import psp.mobile.model.request.MobileDevice;
import psp.mobile.model.request.MobileNotification;
import psp.mobile.model.request.OtpRequest;
import psp.mobile.model.request.PaymentRequest;
import psp.mobile.model.request.RegistrationRequest;
import psp.mobile.model.request.SetPinRequest;
import psp.mobile.model.response.BeneficiarySummary;
import psp.mobile.model.response.MerchantDetails;
import psp.mobile.model.response.TxnSummary;
import psp.resp.fromupi.AccountProviderListResponse;
import psp.upi.constants.SampleData;
import psp.upi.model.AccountProvider;
import psp.util.dto.AcDto;

public class MobileProcessUtility implements SampleData {

	/**
	 * coreProcessBeanMap holds the request class name as key and alias name as value
	 */
	private static final Map<String, String> coreProcessBeanMap = new HashMap<String, String>();
	
	static {
		coreProcessBeanMap.put(GetAccountProvidersRequest.class.getName(), "AccountProvidersMpfb");
		coreProcessBeanMap.put(OtpRequest.class.getName(), "OtpRequestMpfb");
		coreProcessBeanMap.put(GetAccountListRequest.class.getName(), "AccountListMpfb");
		coreProcessBeanMap.put(SetPinRequest.class.getName(), "SetPinMpfb");
		coreProcessBeanMap.put(PaymentRequest.class.getName(), "MakePaymentMpfb");
		coreProcessBeanMap.put(GetTxnDetailsRequest.class.getName(), "TxnDetailsMpfb");
		coreProcessBeanMap.put(AddBankAccountRequest.class.getName(), "AddBankAccountMpfb");
		coreProcessBeanMap.put(ChangePasswordRequest.class.getName(), "ChangePasswordMpfb");
		coreProcessBeanMap.put(ForgetPasswordRequest.class.getName(), "ForgetPasswordMpfb");
		coreProcessBeanMap.put(LoginRequest.class.getName(), "LoginMpfb");
		coreProcessBeanMap.put(RegistrationRequest.class.getName(), "RegistrationMpfb");
		coreProcessBeanMap.put(CheckUserRequest.class.getName(), "CheckUserMpfb");
		coreProcessBeanMap.put(LogoutRequest.class.getName(), "LogoutMpfb");
		coreProcessBeanMap.put(GetMerchantListRequest.class.getName(), "GetMerchantListMpfb");
		coreProcessBeanMap.put(GetBeneficiaryListRequest.class.getName(), "GetBeneficiaryListMpfb");
		coreProcessBeanMap.put(AddBeneficiaryRequest.class.getName(), "AddBeneficiaryMpfb");
		coreProcessBeanMap.put(GetExistingAccountsRequest.class.getName(), "GetExistingAccsMpfb");
		coreProcessBeanMap.put(GetKeyRequest.class.getName(), "GetKeyMpfb"); 
		coreProcessBeanMap.put(GetTokenRequest.class.getName(), "GetTokenMpfb"); 
		coreProcessBeanMap.put(GetBeneficiaryRequest.class.getName(), "GetBeneficiaryMpfb");
		coreProcessBeanMap.put(BalanceEnquiryRequest.class.getName(), "BalanceEnquiryMpfb");
		coreProcessBeanMap.put(GetMerchantRequest.class.getName(), "GetMerchantMpfb");
		coreProcessBeanMap.put(GeneratePinRequest.class.getName(), "GeneratePinMpfb");
		coreProcessBeanMap.put(CollectPaymentRequest.class.getName(), "CollectPaymentMpfb");
		coreProcessBeanMap.put(CollectPayConfirmRequest.class.getName(), "CollectPayConfirmationMpfb");
		coreProcessBeanMap.put(GetNotificationDetailsRequest.class.getName(), "GetNotificationDetailsMpfb");
		coreProcessBeanMap.put(GetNotificationListRequest.class.getName(), "GetNotificationListMpfb");
	}
	
	private MobileProcessUtility() {
	}
	
	public static String getProcessBeanName(Object object){
		return coreProcessBeanMap.get(object.getClass().getName());
	}
	
	public static UserDetails prepareUserDetails(RegistrationRequest request) {
		UserDetails userDetails = new UserDetails();
		userDetails.setAadhaarNumber(request.getAdharNumber());
		userDetails.setCreatedDate(new Date());
		userDetails.setEmail(request.getEmail());
		userDetails.setFirstName(request.getFirstName());
		userDetails.setLastName(request.getLastName());
		userDetails.setMiddleName(request.getMiddleName());
		userDetails.setMobileNumber(request.getMobileNumber());
		userDetails.setPassword(request.getPassword());
		userDetails.setStatus(UserStatus.REGISTERED.name());
		userDetails.setUserName(request.getUserName());
		userDetails.setRnsMpaId(request.getRnsMpaId());
		return userDetails;
	}
	
	public static List<AccountProviderDetails> prepareAccountProvider(AccountProviderListResponse response){
		List<AccountProviderDetails> accList = new ArrayList<AccountProviderDetails>();
		for(AccountProvider accProvider : response.getAccountProviderList().getAccountProvider()) {
			AccountProviderDetails accountProviderDetails = new AccountProviderDetails();
			accountProviderDetails.setActive(accProvider.getActive());
			accountProviderDetails.setBankName(accProvider.getName());
			accountProviderDetails.setIin(accProvider.getIin().toString());
			accountProviderDetails.setLastModifedTs(accProvider.getLastModifedTs());
			accountProviderDetails.setProds(accProvider.getProds());
			accountProviderDetails.setSpocEmail(accProvider.getSpocEmail());
			accountProviderDetails.setSpocName(accProvider.getName());
			accountProviderDetails.setSpocPhone(accProvider.getSpocPhone());
			accountProviderDetails.setUrl(accProvider.getUrl());
			accList.add(accountProviderDetails);
		}
		return accList;
	}
	
	public static List<AccountReference> prepareAccountReferenceList(GetAccountsUpiRequest response){
		List<AccountReference> list = new ArrayList<AccountReference>();
		Long id = 0l; 
		Iterator<ReqAccountReference> iterator = response.getAccounts().iterator();
		ReqAccountReference ref = null;
		while (iterator.hasNext()) {
			ref =  (ReqAccountReference) iterator.next();
			AccountReference ar = new AccountReference();
			ar.setAeba(ref.getAeba());
			ar.setIfsc(ref.getIfsc());
			ar.setMaskedNumber(ref.getMaskedNumber());
			ar.setName(ref.getName());
			ar.setCredentailType(ref.getCredentailDataType());
			ar.setCredentailSubType(ref.getCredentailSubType());
			ar.setCredentailDataType(ref.getCredentailDataType());
			ar.setCredentailDataLength(ref.getCredentailDataLength());
			ar.setId(++id);
			list.add(ar);
		}
		return list;
	}
	
	public static DeviceDetails prepareMobileDeviceDetails(RegistrationRequest request, UserDetails userDetails) {
		DeviceDetails deviceDetails = new DeviceDetails();
		MobileDevice mobileDevice = request.getDevice();
		deviceDetails.setApp(mobileDevice.getApp());
		deviceDetails.setCapability(mobileDevice.getCapability());
		deviceDetails.setGeoCode(mobileDevice.getGeoCode());
		deviceDetails.setIp(mobileDevice.getIp());
		deviceDetails.setLocation(mobileDevice.getLocation());
		deviceDetails.setOs(mobileDevice.getOs());
		deviceDetails.setType(mobileDevice.getType());
		deviceDetails.setTerminalId(mobileDevice.getTerminalId());
		deviceDetails.setMobile(mobileDevice.getMobile());
		deviceDetails.setUserDetails(userDetails);
		return deviceDetails;
	}
	
	public static LoginDetails prepareLoginDetails(String userName, String terminalId) {
		LoginDetails loginDetails = new LoginDetails();
		loginDetails.setLoginTime(new Date());					
		loginDetails.setUserName(userName);
		loginDetails.setTerminalId(terminalId);			
		return loginDetails;
	}
	
	public static MobileAuditTrail prepareMobileAuditTrail(DeviceDetails deviceDetails, String userName, String service) {
		MobileAuditTrail mobileAuditTrail = new MobileAuditTrail();
		mobileAuditTrail.setApp(deviceDetails.getApp());
		mobileAuditTrail.setCapability(deviceDetails.getCapability());
		mobileAuditTrail.setGeoCode(deviceDetails.getGeoCode());
		mobileAuditTrail.setIp(deviceDetails.getIp());
		mobileAuditTrail.setLocation(deviceDetails.getLocation());
		mobileAuditTrail.setMobile(deviceDetails.getMobile());
		mobileAuditTrail.setOs(deviceDetails.getOs());
		mobileAuditTrail.setTerminalId(deviceDetails.getTerminalId());
		mobileAuditTrail.setType(deviceDetails.getType());
		mobileAuditTrail.setRequestTime(new Date());
		mobileAuditTrail.setServiceName(service);
		mobileAuditTrail.setUserName(userName);
		return mobileAuditTrail;
	}

	public static List<TxnSummary> prepareTxnsSummaryResponse(List<TransactionDetails> transactionDetails) {
		List<TxnSummary> detailsResponses = new ArrayList<>();
		for (TransactionDetails txn : transactionDetails) {
			TxnSummary response = new TxnSummary();
			response.setAmount(Double.parseDouble(txn.getAmount()));
			response.setPaymentFrom(txn.getFromAddr());
			response.setPaymentTo(txn.getToAddr());
			response.setTransactionDate(txn.getTxnInitiationTime());
			response.setStatus(txn.getStatus());
			response.setTxnMessage(txn.getTxnMessage());
			detailsResponses.add(response);
		}
		return detailsResponses;
	}
	
	public static AcDto getAcDto(AcDetails acDetails){
		AcDto dto = new AcDto();
		dto.setAcnum(acDetails.getAcnum());
		dto.setActype(acDetails.getActype());
		dto.setAddrType(acDetails.getAddrType());
		dto.setCardNum(acDetails.getCardNum());
		dto.setIfsc(acDetails.getIfsc());
		dto.setIin(acDetails.getIin());
		dto.setMmid(acDetails.getMmid());
		dto.setMobnum(acDetails.getMobnum());
		dto.setUidnum(acDetails.getUidnum());
		return dto;
	}
	
	public static AcDto getAcDto(AccountDetails acDetails){
		AcDto dto = new AcDto();
		dto.setAcnum(acDetails.getAccountNumber());
		dto.setActype(acDetails.getAccountType());
		dto.setAddrType(acDetails.getAddressType());
		//dto.setCardNum(acDetails.get);
		dto.setIfsc(acDetails.getIfsc());
		//dto.setIin(acDetails.get);
		dto.setMmid(acDetails.getMmid());
		dto.setMobnum(acDetails.getMobileNumber());
		//dto.setUidnum(acDetails.get);
		return dto;
	}
	
	public static OtpUpiRequest prepareOtpReqDetails(OtpRequest otpRequest, String msgId, String txnId) {
		OtpUpiRequest otpRequestDetails = new OtpUpiRequest();
		otpRequestDetails.setAcNum(otpRequest.getAcDetails().getAcnum());
		otpRequestDetails.setAcType(otpRequest.getAcDetails().getActype());
		otpRequestDetails.setIfsc(otpRequest.getAcDetails().getIfsc());
		otpRequestDetails.setIin(otpRequest.getAcDetails().getIin());
		otpRequestDetails.setMessageId(msgId);
		otpRequestDetails.setMmid(otpRequest.getAcDetails().getMmid());
		otpRequestDetails.setMobNum(otpRequest.getAcDetails().getMobnum());
		otpRequestDetails.setRequestedTime(new Date());
		otpRequestDetails.setStatus(ServiceStatus.ACKNOWLEDGED.name());
		otpRequestDetails.setTransactionId(txnId);
		otpRequestDetails.setUidNum(otpRequest.getAcDetails().getUidnum());
		return otpRequestDetails;
	}
	
	public static DeviceDetails prepareDeviceDetails(MobileDevice mobileDevice) {
		DeviceDetails deviceDetails = new DeviceDetails();
		deviceDetails.setApp(mobileDevice.getApp());
		deviceDetails.setCapability(mobileDevice.getCapability());
		deviceDetails.setGeoCode(mobileDevice.getGeoCode());
		deviceDetails.setIp(mobileDevice.getIp());
		deviceDetails.setLocation(mobileDevice.getLocation());
		deviceDetails.setOs(mobileDevice.getOs());
		deviceDetails.setType(mobileDevice.getType());
		deviceDetails.setTerminalId(mobileDevice.getTerminalId());
		deviceDetails.setMobile(mobileDevice.getMobile());
		return deviceDetails;
	}

	public static GetTokenUpiRequest prepareGetTokenUpiRequest(String msgId, String txnId) {
		GetTokenUpiRequest upiRequest = new GetTokenUpiRequest();
		upiRequest.setMessageId(msgId);
		upiRequest.setRequestedTime(new Date());
		upiRequest.setStatus(ServiceStatus.ACKNOWLEDGED.name());
		upiRequest.setTransactionId(txnId);
		upiRequest.setKeyType(PayConstant.GET_TOKEN.name());
		return upiRequest;
	}
	public static  UpiRequest prepareUpiRequestDetails(String msgId, String txnId, Class<? extends UpiRequest> upiObject) {
		UpiRequest upiRequest = null;
		try {
			upiRequest = upiObject.newInstance();
			upiRequest.setMessageId(msgId);
			upiRequest.setRequestedTime(new Date());
			upiRequest.setStatus(ServiceStatus.ACKNOWLEDGED.name());
			upiRequest.setTransactionId(txnId);
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException("");
		}
		return upiRequest;
	}	

	public static List<BeneficiarySummary> convertBeneficiaryDetailsToBeneficiarySummary(List<BeneficiaryDetails> bnfsDtls, String reqType){
		List<BeneficiarySummary>  bnfsSummary = new ArrayList<BeneficiarySummary>();
		for(int i = 0; i < bnfsDtls.size(); i++) {			
			BeneficiaryDetails bnfDtls = bnfsDtls.get(i);	
			BeneficiarySummary  bnfSummary = new BeneficiarySummary();	
			/*if(RequestType.COLLECT.name().equals(reqType)) {
				bnfSummary.setBenVirtualAddr(bnfDtls.getAddress());
			}
			else {*/
				bnfSummary.setBenName(bnfDtls.getUserName());
				bnfSummary.setBenNickName(bnfDtls.getNickName());
				bnfSummary.setBenVirtualAddr(bnfDtls.getAddress()); 		
			//}	
			bnfsSummary.add(bnfSummary);
		}
		return bnfsSummary;
	}
	
	public static MerchantDetails prepareMerchantDetails(Merchant merchant) {
		MerchantDetails merchantDetails = new MerchantDetails();		
		merchantDetails.setId(merchant.getId());
		merchantDetails.setDescription(merchant.getDescription());
		merchantDetails.setName(merchant.getName());
		merchantDetails.setNickName(merchant.getNickName());
		merchantDetails.setVirtualAddress(merchant.getVirtualAddress());
		return merchantDetails;
	}	
	
	public static void updateBenificiary(BeneficiaryDetails bnfDetails, String name, String nickName, String address, String type, String note){
		bnfDetails.setAddress(address);
		bnfDetails.setAddressType(type);
		bnfDetails.setDescription(note);
		bnfDetails.setNickName(nickName);
		bnfDetails.setUserName(name);
	}

	public static void updateBenificiaryAccountDetails(AccountReference actRef, BeneficiaryAccountDetails bnfAcc){
		bnfAcc.setMaskedNumber(actRef.getMaskedNumber());
		bnfAcc.setIfsc(actRef.getIfsc());
		bnfAcc.setName(actRef.getName());
		bnfAcc.setAeba(actRef.getAeba());
		bnfAcc.setMmid(actRef.getMmid());
		bnfAcc.setReferenceNumber(actRef.getReferenceNumber());
	}
	
	public static BeneficiaryDetails prepareBenficiaryDetails(AccountReference actRef, String name, String nickName, String address, String type, String note){
		BeneficiaryDetails bnfDtls = new BeneficiaryDetails();
		BeneficiaryAccountDetails beneficiaryActDetails = new BeneficiaryAccountDetails();		
		beneficiaryActDetails.setMaskedNumber(actRef.getMaskedNumber());
		beneficiaryActDetails.setIfsc(actRef.getIfsc());
		beneficiaryActDetails.setName(actRef.getName());
		beneficiaryActDetails.setAeba(actRef.getAeba());
		beneficiaryActDetails.setMmid(actRef.getMmid());
		beneficiaryActDetails.setReferenceNumber(actRef.getReferenceNumber());
		bnfDtls.setAccountDetails(beneficiaryActDetails);
		bnfDtls.setAddress(address);
		bnfDtls.setAddressType(type);
		bnfDtls.setDescription(note);
		bnfDtls.setNickName(nickName);
		bnfDtls.setUserName(name);
		return bnfDtls;
	}
	
	public static List<AccountReference> prepareSampleDataForAccountSummaryList(String userName) {
		List<AccountReference> list = new ArrayList<AccountReference>(); 
		AccountReference ar = new AccountReference();
		ar.setAeba(AEBA);
		ar.setIfsc(IFSC);
		ar.setMaskedNumber(ACC_NUM);
		ar.setMmid(MMID);
		ar.setName(userName);
		ar.setCredentailType(CRED_TYPE);
		ar.setCredentailSubType(CRED_SUB_TYPE);
		ar.setCredentailDataType(CRED_DATA_TYPE);
		ar.setCredentailDataLength(CRED_DATA_LENGTH);
		ar.setId(1l);
		list.add(ar);		
		return list;
	}
	
	public static List<AccountReference> prepareSampleDataForBenAccountSummaryList(String userName) {
		List<AccountReference> list = new ArrayList<AccountReference>(); 
		AccountReference ar = new AccountReference();
		ar.setAeba(AEBA);
		ar.setIfsc(BEN_IFSC);
		ar.setMaskedNumber(BEN_ACC_NUM);
		ar.setMmid(BEN_MMID);
		ar.setId(1l);
		list.add(ar);		
		return list;
	}
	
	public static List<AccountSummary> prepareAccountSummaryList(List<AccountReference> accRefs) {
		List<AccountSummary> list = new ArrayList<AccountSummary>();
		Long id = 0l; 
		Iterator<AccountReference> iterator = accRefs.iterator();
		AccountReference ref = null;
		while (iterator.hasNext()) {
			ref =  (AccountReference) iterator.next();
			AccountSummary ar = new AccountSummary();
			ar.setAeba(ref.getAeba());
			ar.setIfsc(ref.getIfsc());
			ar.setMaskedAccnumber(ref.getMaskedNumber());
			ar.setName(ref.getName());
			ar.setCredentailType(ref.getCredentailType());
			ar.setCredentailSubType(ref.getCredentailSubType());
			ar.setCredentailDataType(ref.getCredentailDataType());
			ar.setCredentailDataLength(ref.getCredentailDataLength());
			ar.setId(++id);
			list.add(ar);
		}
		return list;
	}	

	public static StatusCode validateMobileDeviceDetails(DeviceDetails deviceDetails, MobileDevice mobileDevice) {
		StatusCode statusCode = null;
		if (!(deviceDetails.getOs().equals(mobileDevice.getOs())
				&& deviceDetails.getTerminalId().equals(mobileDevice.getTerminalId())
					&& deviceDetails.getCapability().equals(mobileDevice.getCapability()))) {
			statusCode = StatusCode.DEVICE_VALIDATION_FAILED;
		}
		else {
			statusCode = StatusCode.SUCCESS;
		}
		return statusCode;
	}	

	public static String getHexString(byte[] buffer) {
		  if(buffer == null) {
		   return null;
		  }
		  else {
		   return getHexString(buffer, 0, buffer.length);
		  }
	}
	
	public static String getHexString(byte[] buffer, int offset, int length) {
		if(buffer == null || offset < 0 || length < 0 || offset + length > buffer.length){
		   return null;
		}
	    StringBuilder sb = new StringBuilder();
		for (int i = offset; i < (offset + length); i++) {
		  sb.append(String.format("%02X", buffer[i]));
		}
		return sb.toString();
	}
	
	public static MobileNotification prepareMobileNotifications(NotificationDetails notificationDetails) {
		MobileNotification mobileNotificationDetails = new MobileNotification();
		mobileNotificationDetails.setNotificationId(notificationDetails.getNotificationId());
		mobileNotificationDetails.setNotificationMessage(notificationDetails.getNotificationMessage());
		mobileNotificationDetails.setNotificationtType(notificationDetails.getNotificationtType());
		return mobileNotificationDetails;
	}
}