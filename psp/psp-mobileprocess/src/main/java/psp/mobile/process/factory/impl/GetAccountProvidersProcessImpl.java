/**
 * 
 */
package psp.mobile.process.factory.impl;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import psp.constants.StatusCode;
import psp.dbservice.mgmt.PspMgmtService;
import psp.mobile.model.response.GetAccountProvidersResponse;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.process.factory.MobileCoreProcess;

/**
 * @author prasadj
 *
 */
@Component("accountProvidersMpfb")
public class GetAccountProvidersProcessImpl extends MobileCoreProcess {

	private static final Logger LOGGER = Logger.getLogger(GetAccountProvidersProcessImpl.class.getName());
	
	@Autowired
	private PspMgmtService pspMgmtService;
	
	public GetAccountProvidersProcessImpl(){
	}

	@Override
	public MessageResponse validateRequest() {
		GetAccountProvidersResponse response = new GetAccountProvidersResponse();
		response.validate();
		return response;
	}

	@Override
	public void doProcess(MessageResponse response) {
		LOGGER.info("doProcess of GetAccountProvidersProcessImpl started ");
		((GetAccountProvidersResponse)response).setListOfBanks(pspMgmtService.getBankSummary());	
		LOGGER.info("doProcess of GetAccountProvidersProcessImpl completed ");
	}

	@Override
	public MessageResponse createResponseOnStatusCode(StatusCode code) {
		GetAccountProvidersResponse response = new GetAccountProvidersResponse();
		response.setStatusCode(code.getCode());
		response.setStatusMessage(code.getMessage());
		return response;
	}

}