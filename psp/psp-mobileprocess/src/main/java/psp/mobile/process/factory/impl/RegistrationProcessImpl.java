/**
 * 
 */
package psp.mobile.process.factory.impl;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import psp.common.exception.ApplicationException;
import psp.constants.StatusCode;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.DeviceDetails;
import psp.dbservice.model.KeyInfo;
import psp.dbservice.model.UserDetails;
import psp.mobile.model.request.RegistrationRequest;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.model.response.RegistrationResponse;
import psp.mobile.process.factory.MobileCoreProcess;
import psp.mobile.process.util.MobileProcessUtility;

/**
 * @author prasadj
 *
 */
@Component("registrationMpfb")
public class RegistrationProcessImpl extends MobileCoreProcess {

	private static final Logger LOGGER = Logger.getLogger(RegistrationProcessImpl.class.getName());
	
	@Autowired
	private PspMgmtService pspMgmtService;
	
	public RegistrationProcessImpl(){
	}
	
	@Override
	public MessageResponse validateRequest() {
		RegistrationResponse response = new RegistrationResponse();
		response.validate((RegistrationRequest)request);
		return response;
	}

	@Override
	public void doProcess(MessageResponse response) throws ApplicationException {
		LOGGER.info("doProcess of RegistrationProcessImpl started ");
		RegistrationRequest req = (RegistrationRequest) request;
		RegistrationResponse registrationResponse = (RegistrationResponse)response;
		UserDetails user = pspMgmtService.getUser(request.getUserName(), req.getMobileNumber(), req.getAdharNumber());
		if(null == user) {
			UserDetails userDetails = MobileProcessUtility.prepareUserDetails(req);
			DeviceDetails deviceDetails = MobileProcessUtility.prepareMobileDeviceDetails(req, userDetails);
			pspMgmtService.saveUserAndDeviceDetails(userDetails, deviceDetails);
			KeyInfo keyInfo = pspMgmtService.getKeyInfo();
			registrationResponse.setXmlPayload(keyInfo.getXmlPayload());
		}
		else {
			if(req.getAdharNumber().equals(user.getAadhaarNumber())){
				throw new ApplicationException(StatusCode.DUPLICATE_AADHAAR_NUMBER);
			}
			else if(req.getMobileNumber().equals(user.getMobileNumber())) {
				throw new ApplicationException(StatusCode.DUPLICATE_MOBILE_NUMBER);
			}
			else if(req.getUserName().equals(user.getUserName())) {
				throw new ApplicationException(StatusCode.DUPLICATE_USER_NAME);
			}
		}
		LOGGER.info("doProcess of RegistrationProcessImpl completed ");
	}

	@Override
	public MessageResponse createResponseOnStatusCode(StatusCode code) {
		RegistrationResponse response = new RegistrationResponse();
		response.setStatusCode(code.getCode());
		response.setStatusMessage(code.getMessage());
		return response;
	}

}