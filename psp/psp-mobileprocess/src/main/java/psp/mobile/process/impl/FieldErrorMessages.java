/**
 * 
 */
package psp.mobile.process.impl;

/**
 * @author gowthamb
 *
 */
public interface FieldErrorMessages {

	String FIRSTNAME_ERR = "First name can not be empty";
	
	String FIRSTNAME_APHA = "First name should contain only alphabets and space";
	
	String FIRSTNAME_LEN = "First name minimum length is 3 maximum length is 30";
	
	String MIDDLENAME_APHA = "Middle name should contain only alphabets and space";
	
	String MIDDLENAME_LEN = "Middle name minimum length is 3 maximum length is 20";
	
	String LASTNAME_ERR = "Last name can not be empty";
	
	String LASTNAME_APHA = "Last name should contain only alphabets and space";
	
	String LASTNAME_LEN = "Last name minimum length is 3 maximum length is 30";
	
	String EMAILID_ERR = "Email can not be empty";
	
	String EMAILID_LEN = "Email minimum length is 7 maximum length is 40";
	
	String EMAILID_VALIDATE_ERR = "Email is not in valid format, valid format for e.g(XYZ@gmail.com)";
	
	String USERNAME_ERR = "User name can not be empty";
	
	String USERNAME_LEN = "User name minimum length is 5 maximum length is 30";
	
	String USERNAME_VALIDATE_ERR = "User name should contain only alphabets and digits";
	
	String PASSWORD_ERR = "Password can not be empty";
	
	String PASSWORD_VALIDATE_ERR = "Password must contain a digit least once ,a lower case letter at least once ,an upper case letter at least once ,a special character at least once and no whitespace allowed in the entire string";
	
	String PASSWORD_LEN = "Password minimum length is 7 maximum length is 20";
	
	String MOBILE_NUM_ERR = "Mobile number can not be empty";
	
	String MOBILE_NUM_LEN = "Mobile number minimum length is 10 maximum length is 10";
	
	String MOBILE_VALIDATE_ERR = "Mobile number should contain only digits";
	
	String AADHAAR_NUM_ERR = "Aadhaar number can not be empty";
	
	String AADHAAR_NUM_LEN = "Aadhaar number minimum length is 12  maximum length is 12";
	
	String AADHAAR_VALIDATE_ERR = "Aadhaar number must be only digits";
	
	String OLD_PASSWORD_ERR = "Old password can not be empty";
	
	String OLD_PASSWORD_LEN = "Old password minimum length is 7 maximum length is 20";
	
	String OLD_PASSWORD_VALIDATE_ERR = "Old password must contain a digit least once ,a lower case letter at least once ,an upper case letter at least once ,a special character at least once and no whitespace allowed in the entire string";

	
	String NEW_PASSWORD_ERR = "New password can not be empty";
	
	String NEW_PASSWORD_LEN = "New password minimum length is 7 maximum length is 20";
	
	String NEW_PASSWORD_VALIDATE_ERR = "New password must contain a digit least once ,a lower case letter at least once ,an upper case letter at least once ,a special character at least once and no whitespace allowed in the entire string";
	
	String OLD_PIN_ERR = "Old pin can not be empty";
	
	String OLD_PIN_LEN = "Old pin minimum length is 4 maximum length is 20";
	
	String OLD_PIN_VALIDATE_ERR = "Old pin should contain only digits";
	
	String PIN_VALIDATE_ERR = "Pin must be only digits";
	
	String NEW_PIN_ERR = "New pin can not be empty";
	
	String NEW_PIN_LEN = "New pin minimum length is 4 maximum length is 20";
	
	String NEW_PIN_VALIDATE_ERR = "New pin should contain only digits";
	
	String PAYEE_VIRTUAL_ADDR_ERR = "Payee virtual address can not be empty";
	
	String PAYEE_VIRTUAL_ADDR_LEN = "Payee virtual address minimum length is 5 maximum length is 40";
	
	String PAYEE_VIRTUAL_ADDR_VALIDATE_ERR = "Payee virtual address is not in valid format";
	
	String AMOUNT_ERR = "Amount can not be empty";
	
	String AMOUNT_LEN = "Amount minimum length is 1 maximum length is 18";
	
	String AMOUNT_VALIDATE = "Amount should contain only digits and one full stop and 5 decimal point";
	
	String FROM_DATE_ERR = "From date can not be empty";
	
	String TO_DATE_ERR = "To date can not be empty";
	
	String MMID_ERR = "MMID can not be empty";
	
	String MMID_LEN = "MMID minimum length is 7 maximum length is 10";
	
	String MMID_VALIDATE_ERR = "MMID should contain only digits";
	
	String OTP_ERR = "OTP can not be empty";
	
	String OTP_VALIDATE_ERR = "OTP must be only digits";
	
	String MPIN_ERR = "MPIN can not be empty";
	
	String MPIN_LEN = "MPIN minimum length is 4 maximum length is 20";
	
	String MPIN_VALIDATE_ERR = "MPIN should contain only digits";

}