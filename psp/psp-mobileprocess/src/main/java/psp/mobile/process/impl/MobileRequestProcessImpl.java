package psp.mobile.process.impl;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import psp.common.exception.ApplicationException;
import psp.constants.StatusCode;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.DeviceDetails;
import psp.dbservice.model.LoginDetails;
import psp.dbservice.model.UserDetails;
import psp.mobile.model.request.ForgetPasswordRequest;
import psp.mobile.model.request.MobileRequest;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.process.MobileRequestProcessor;
import psp.mobile.process.account.service.AccountListCacheService;
import psp.mobile.process.factory.CoreProcessFactory;
import psp.mobile.process.factory.MobileCoreProcess;
import psp.mobile.process.util.MobileProcessUtility;
import psp.util.upi.client.UpiClientService;

@Component("mobileRequestProcessor")
public class MobileRequestProcessImpl implements MobileRequestProcessor {

	private static final Logger LOGGER = Logger.getLogger(MobileRequestProcessImpl.class.getName());
	
	@Autowired
	private PspMgmtService pspMgmtService;
	
	@Autowired
	private UpiClientService upiClient;

	@Autowired
	private AccountListCacheService accountSummaryService;
	
	@Autowired
	private CoreProcessFactory coreProcessFactory;
	
	@Override
	public MessageResponse processAuthorisedRequest(MobileRequest request) {
		String beanName = MobileProcessUtility.getProcessBeanName(request);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(beanName);
		StatusCode statusCode = StatusCode.SUCCESS;
		process.setRequest(request);
		MessageResponse response = process.validateRequest();
		if( statusCode.getCode() == response.getStatusCode() ){
			try {
				UserDetails user = pspMgmtService.getNonDeletedUser(request.getUserName());
				if ( user != null) {
					DeviceDetails deviceDetails = pspMgmtService.getDeviceDetailsByUserId(user.getId());
					statusCode = MobileProcessUtility.validateMobileDeviceDetails(deviceDetails, request.getDevice());
					if (statusCode == StatusCode.SUCCESS) { 
						LoginDetails loginDetails = pspMgmtService.getLoginDetailsByUserName(request.getUserName());
						if(loginDetails != null && loginDetails.getExpiryTime().compareTo(new Date()) == 1) {	
							pspMgmtService.saveMobileAuditTrail(MobileProcessUtility.prepareMobileAuditTrail(deviceDetails, request.getUserName(), beanName));
							process.setUser(user);
							process.setLoginDetails(loginDetails);
							process.doProcess(response);
							return response;
						}
						else {
							if(loginDetails != null) {
								pspMgmtService.deleteLoginDetails(loginDetails);
							}
							statusCode = StatusCode.USER_NOT_LOGGED_IN;	
						}
					}
				}
				else {
					statusCode = StatusCode.USER_NOT_REGISTER;
				}
			}
			catch(ApplicationException ae) {
				LOGGER.error(ae.getMessage());
				statusCode = ae.getStatusCode();
			}
			catch(Exception e) {
				LOGGER.error(e.getMessage());
				statusCode = StatusCode.INTERNAL_SERVER_ERROR_MSG;
			}
			response = process.createResponseOnStatusCode(statusCode);
		}
		return response;	
	}

	//TODO This service need to discuss and implement
	@Override
	public MessageResponse forgetPassword(ForgetPasswordRequest request) {
		String beanName = MobileProcessUtility.getProcessBeanName(request);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(beanName);
		process.setRequest(request);
		MessageResponse response = process.validateRequest();
		if(StatusCode.SUCCESS == StatusCode.getStatusCodeByCode((response.getStatusCode()) )) {
			
		}
		return response;
	}

	@Override
	public MessageResponse processNonAuthorisedRequest(MobileRequest request) {
		String beanName = MobileProcessUtility.getProcessBeanName(request);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(beanName);
		StatusCode statusCode = StatusCode.SUCCESS;
		process.setRequest(request);
		MessageResponse response = process.validateRequest();
		try{
			if(StatusCode.SUCCESS == StatusCode.getStatusCodeByCode((response.getStatusCode()) )) {
				process.doProcess(response);
				if (statusCode == StatusCode.getStatusCodeByCode(response.getStatusCode())) { 
					DeviceDetails deviceDetails = MobileProcessUtility.prepareDeviceDetails(request.getDevice());
					pspMgmtService.saveMobileAuditTrail(MobileProcessUtility.prepareMobileAuditTrail(deviceDetails, request.getUserName(), beanName));				
				}
			}
			return response;			
		}
		catch(ApplicationException ae) {
			LOGGER.error(ae.getMessage());
			statusCode = ae.getStatusCode();
		}
		catch(Exception e) {
			LOGGER.error(e.getMessage());
			statusCode = StatusCode.INTERNAL_SERVER_ERROR_MSG;
		}
		response = process.createResponseOnStatusCode(statusCode);
		return response;
	}

}