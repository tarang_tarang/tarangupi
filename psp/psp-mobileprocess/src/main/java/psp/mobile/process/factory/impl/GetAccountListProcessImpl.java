/**
 * 
 */
package psp.mobile.process.factory.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.upi.system_1_2.Ack;
import org.upi.system_1_2.CredSubType;

import psp.common.PropertyReader;
import psp.common.exception.ApplicationException;
import psp.constants.CommonConstants;
import psp.constants.ServiceNotes;
import psp.constants.ServiceRequestName;
import psp.constants.ServiceStatus;
import psp.constants.StatusCode;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.AccountDetails;
import psp.dbservice.model.AccountReference;
import psp.dbservice.model.GetAccountsUpiRequest;
import psp.mobile.model.request.GetAccountListRequest;
import psp.mobile.model.response.GetAccountListResponse;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.process.account.service.AccountListCacheService;
import psp.mobile.process.factory.MobileCoreProcess;
import psp.mobile.process.util.MobileProcessUtility;
import psp.upi.constants.CredType;
import psp.upi.constants.LinkType;
import psp.upi.constants.PayerType;
import psp.util.DtoObjectUtil;
import psp.util.PspClientTool;
import psp.util.dto.AcDto;
import psp.util.dto.CredDto;
import psp.util.dto.HeadDto;
import psp.util.dto.LinkDto;
import psp.util.dto.PayerDto;
import psp.util.dto.TxnDto;
import psp.util.upi.client.UpiClientService;

/**
 * @author prasadj
 *
 */
@Component("accountListMpfb")
public class GetAccountListProcessImpl extends MobileCoreProcess {
	
	private static final Logger LOGGER = Logger.getLogger(GetAccountListProcessImpl.class.getName());
	
	@Autowired
	private PspMgmtService pspMgmtService;
	
	@Autowired
	private UpiClientService upiClient;

	@Autowired
	private AccountListCacheService accountSummaryService;
	
	@Autowired
	private PropertyReader propertyReader;
	
	public GetAccountListProcessImpl(){
	}

	@Override
	public MessageResponse validateRequest() {
		GetAccountListResponse response = new GetAccountListResponse();		
		response.validate();
		return response;
	}

	@Override
	public void doProcess(MessageResponse response) throws ApplicationException {
		LOGGER.info("doProcess of GetAccountListProcessImpl started ");
		GetAccountListRequest req = (GetAccountListRequest) request;
		GetAccountListResponse res = (GetAccountListResponse) response;
		Date date = new Date();
		String messageId = DtoObjectUtil.constructMessageId();
		HeadDto head = DtoObjectUtil.constructHeadDto(messageId, date);
		String id = req.getTxnId();
		String note = ServiceNotes.ACC_LIST_NOTE;
		String orgTxnId = null;
		String refId = "";
		String refUrl = "";
		String type = ServiceRequestName.LIST_ACCOUNT;
		TxnDto txn = DtoObjectUtil.constructTxnDto(id, note, orgTxnId, refId, refUrl, date, type);
		
		String fullName = user.getFirstName() + CommonConstants.SPACE_STR + user.getMiddleName() + CommonConstants.SPACE_STR + user.getLastName();
		String payerType = PayerType.PERSON.name();
		
		AccountDetails acDetails = pspMgmtService.getAccountDetailsByUserDetailsId(user.getId());
		AcDto acDto =  MobileProcessUtility.getAcDto(acDetails);		
		//TODO virtual address is mandatory
		PayerDto payerDto = DtoObjectUtil.constructPayerDto(user.getUserName() + CommonConstants.SELF_PSP_NAME, fullName, payerType, acDto, null, null, null, request.getDevice(), null);
		LinkDto linkDto = new LinkDto();
		linkDto.setLinkType(LinkType.MOBILE.name());
		if(Boolean.FALSE == req.getIsBenAccDetails()) {
			List<CredDto> creds = new ArrayList<CredDto>();
			CredDto credDto = new CredDto();
			credDto.setData(req.getMobileCredentials().getDataValue());
			credDto.setType(CredType.OTP.name());
			credDto.setSubType(CredSubType.SMS.name());
			creds.add(credDto);
			payerDto.setCreds(creds);
			linkDto.setValue(user.getMobileNumber());
		}
		else {
			linkDto.setValue(req.getAcDetails().getMobnum());		
		}		
		
		String responseXml = upiClient.reqListAccount(head, txn, payerDto, linkDto);	
		Ack ack = PspClientTool.convertUpiRequest(responseXml, Ack.class);
		boolean respReceived = false;
		if(ack.getErr() == null) {
			pspMgmtService.saveUpiRequest(MobileProcessUtility.prepareUpiRequestDetails(messageId, id, GetAccountsUpiRequest.class));
			int maxDelay = 270; //seconds
			int count = 0;
			do {
				GetAccountsUpiRequest accListResp = (GetAccountsUpiRequest) pspMgmtService.getUpiRequestByTxnId(id);
				if (ServiceStatus.RECEIVED.name().equals(accListResp.getStatus())) {
					respReceived = true;
					if(CommonConstants.SUCCESS.equals(accListResp.getResult())) {
						acDetails.setIsAccDetailsVerified(true);
						pspMgmtService.updateAccountDetails(acDetails);
						List<AccountReference> accountDetails = MobileProcessUtility.prepareAccountReferenceList(accListResp);
						prepareResponse(accountDetails, res, accListResp, response);
					}
					else{
						List<AccountReference> accountDetails = null;
						if(Boolean.TRUE == req.getIsBenAccDetails()) {
							accountDetails = MobileProcessUtility.prepareSampleDataForBenAccountSummaryList(user.getUserName());	
						}
						else {
							accountDetails = MobileProcessUtility.prepareSampleDataForAccountSummaryList(user.getUserName());
						}
						prepareResponse(accountDetails, res, accListResp, response);
						//throw new ApplicationException(StatusCode.GET_ACCOUNT_SERVICE_FAIL);
					}
				}
				try{
					count++;
					Thread.sleep(propertyReader.getMobileProcessDelay());
				}
				catch(Exception ex){
				}
				if(count > maxDelay){
					LOGGER.info("UPI Request time out");
					throw new ApplicationException(StatusCode.GET_ACCOUNT_SERVICE_FAIL);
				}
			}while (!respReceived);
		}
		else {
			throw new ApplicationException(StatusCode.GET_ACCOUNT_SERVICE_FAIL);
		}
		LOGGER.info("doProcess of GetAccountListProcessImpl completed ");
	}

	@Override
	public MessageResponse createResponseOnStatusCode(StatusCode code) {
		GetAccountListResponse response = new GetAccountListResponse();
		response.setStatusCode(code.getCode());
		response.setStatusMessage(code.getMessage());
		return response;
		
	}
	
	private void prepareResponse(List<AccountReference> accountDetails, GetAccountListResponse res, GetAccountsUpiRequest accListResp, MessageResponse response) {
		accountSummaryService.addUserAccounts(request.getUserName(), accountDetails);
		res.setAccountSummaryList(MobileProcessUtility.prepareAccountSummaryList(accountDetails));
		pspMgmtService.deleteUpiRequest(accListResp);
		response.setStatusCode(StatusCode.SUCCESS.getCode());
		response.setStatusMessage(StatusCode.GET_ACC_SERVICE_SUCCESS.getMessage());
	}

}