package psp.mobile.process.factory.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.upi.system_1_2.Ack;
import org.upi.system_1_2.PayConstant;

import psp.common.PropertyReader;
import psp.common.exception.ApplicationException;
import psp.constants.CommonConstants;
import psp.constants.ServiceNotes;
import psp.constants.ServiceStatus;
import psp.constants.StatusCode;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.DeviceDetails;
import psp.dbservice.model.GetTokenUpiRequest;
import psp.dbservice.model.KeyInfoReq;
import psp.dbservice.model.UserDetails;
import psp.mobile.model.request.GetTokenRequest;
import psp.mobile.model.response.GetTokenResponse;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.process.factory.MobileCoreProcess;
import psp.mobile.process.util.MobileProcessUtility;
import psp.util.DtoObjectUtil;
import psp.util.PspClientTool;
import psp.util.dto.CredDto;
import psp.util.dto.HeadDto;
import psp.util.dto.TxnDto;
import psp.util.upi.client.UpiClientService;

@Component("getTokenMpfb")
public class GetTokenProcessImpl extends MobileCoreProcess {

	private static final Logger LOGGER = Logger.getLogger(GetTokenProcessImpl.class.getName());
	
	@Autowired
	private PspMgmtService pspMgmtService;
	
	@Autowired
	private UpiClientService upiClient;
	
	@Autowired
	private PropertyReader propertyReader;
	
	public GetTokenProcessImpl() {
	}
	
	@Override
	public MessageResponse validateRequest() {
		GetTokenResponse response = new GetTokenResponse();
		response.validate();
		return response;
	}

	@Override
	public void doProcess(MessageResponse response) throws ApplicationException {
		LOGGER.info("doProcess of GetTokenProcessImpl started ");
		UserDetails user = pspMgmtService.getNonDeletedUser(request.getUserName());
		if ( user != null) {
			DeviceDetails deviceDetails = pspMgmtService.getDeviceDetailsByUserId(user.getId());
			StatusCode statusCode = MobileProcessUtility.validateMobileDeviceDetails(deviceDetails, request.getDevice());
			if (statusCode == StatusCode.SUCCESS) { 
				GetTokenRequest req = (GetTokenRequest)request;
				Date date = new Date();
				String messageId = DtoObjectUtil.constructMessageId();
				HeadDto head = DtoObjectUtil.constructHeadDto(messageId, date);
				String id = DtoObjectUtil.constructTxnId();
				String note = ServiceNotes.ACC_PROV_LIST_NOTE;
				
				String type = PayConstant.GET_TOKEN.name();
				TxnDto txn = DtoObjectUtil.constructTxnDto(id, note, null, "", "", date, type);
				
				List<CredDto> credDtos = new ArrayList<>();
				CredDto credDto = new CredDto();
				credDto.setData(req.getMobileCredentials().getDataValue());
				credDto.setSubType(req.getMobileCredentials().getSubType());
				credDto.setType(req.getMobileCredentials().getType());
				credDtos.add(credDto);
				
				String responseXml = upiClient.reqGetToken(head, txn, credDtos);
				Ack ack = PspClientTool.convertUpiRequest(responseXml, Ack.class);
				boolean respReceived = false;
				GetTokenResponse mobileRes = (GetTokenResponse) response;
				if(ack.getErr() == null) {
					pspMgmtService.saveUpiRequest(MobileProcessUtility.prepareUpiRequestDetails(messageId, id, GetTokenUpiRequest.class));
					int maxDelay = 270; //seconds
					int count = 0;
					do {
						GetTokenUpiRequest tokenResp = (GetTokenUpiRequest) pspMgmtService.getUpiRequestByTxnId(id);
						if (ServiceStatus.RECEIVED.name().equals(tokenResp.getStatus())) {
							respReceived = true;
							if(CommonConstants.SUCCESS.equals(tokenResp.getResult())) {
								if(!tokenResp.getKeyInfoReqs().isEmpty()){
									KeyInfoReq kiReq = tokenResp.getKeyInfoReqs().iterator().next();
									mobileRes.setCode(kiReq.getCode());
									mobileRes.setKeyIndexDate(kiReq.getKi());
									mobileRes.setKeyValue(kiReq.getKeyValue());
									mobileRes.setType(kiReq.getType());
								}
								pspMgmtService.deleteUpiRequest(tokenResp);
								response.setStatusCode(StatusCode.GET_TOKEN_SERVICE_SUCCESS.getCode());
								response.setStatusMessage(StatusCode.GET_TOKEN_SERVICE_SUCCESS.getMessage());
							}
							else{
								mobileRes.setCode("NPCI");
								DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
								mobileRes.setKeyIndexDate(dateFormat.format(new Date()));
								mobileRes.setKeyValue(MobileProcessUtility.getHexString("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4rIIEHkJ2TYgO/JUJQI/sxDgbDEAIuy9uTf4DItWeIMsG9AuilOj9R+dwAv8S6/9No/z0cwsw4UnsHQG1ALVIxFznLizMjaVJ7TJ+yTS9C9bYEFakRqH8b4jje7SC7rZ9/DtZGsaWaCaDTyuZ9dMHrgcmJjeklRKxl4YVmQJpzYLrK4zOpyY+lNPBqs+aiwJa53ZogcUGBhx/nIXfDDvVOtKzNb/08U7dZuXoiY0/McQ7xEiFcEtMpEJw5EB4o3RhE9j/IQOvc7l/BfD85+YQ5rJGk4HUb6GrQXHzfHvIOf53l1Yb0IX4v9q7HiAyOdggO+PVzXMSbrcFBrEjGZD7QIDAQAB".getBytes()));
								mobileRes.setType("PKI");
								pspMgmtService.deleteUpiRequest(tokenResp);
								response.setStatusCode(StatusCode.GET_TOKEN_SERVICE_SUCCESS.getCode());
								response.setStatusMessage(StatusCode.GET_TOKEN_SERVICE_SUCCESS.getMessage());
								//throw new ApplicationException(StatusCode.GET_TOKEN_SERVICE_FAIL);
							}
						}		
						try{
							count++;
							Thread.sleep(propertyReader.getMobileProcessDelay());
						}
						catch(Exception ex){
						}
						if(count > maxDelay){
							LOGGER.info("UPI Request time out ");
							throw new ApplicationException(StatusCode.GET_TOKEN_SERVICE_FAIL);
						}
					}while (!respReceived);
				}
				else {
					throw new ApplicationException(StatusCode.GET_TOKEN_SERVICE_FAIL);
				}
			}
			else {
				throw new ApplicationException(StatusCode.DEVICE_VALIDATION_FAILED) ;
			}
		}
		else {
			throw new ApplicationException(StatusCode.USER_NOT_REGISTER) ;
		}
		LOGGER.info("doProcess of GetTokenProcessImpl completed ");
		
	}

	@Override
	public MessageResponse createResponseOnStatusCode(StatusCode code) {
		GetTokenResponse response = new GetTokenResponse();
		response.setStatusCode(code.getCode());
		response.setStatusMessage(code.getMessage());
		return response;
	}

}
