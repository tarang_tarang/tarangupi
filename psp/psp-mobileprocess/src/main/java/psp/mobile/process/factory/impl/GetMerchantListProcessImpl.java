package psp.mobile.process.factory.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import psp.common.exception.ApplicationException;
import psp.constants.StatusCode;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.Merchant;
import psp.mobile.model.response.GetMerchantListResponse;
import psp.mobile.model.response.MerchantDetails;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.process.factory.MobileCoreProcess;
import psp.mobile.process.util.MobileProcessUtility;

@Component("getMerchantListMpfb")
public class GetMerchantListProcessImpl extends MobileCoreProcess {

	private static final Logger LOGGER = Logger.getLogger(GetMerchantListProcessImpl.class.getName());
	
	@Autowired
	private PspMgmtService pspMgmtService;
	
	public GetMerchantListProcessImpl() {
	}
	
	@Override
	public MessageResponse validateRequest() {
		GetMerchantListResponse response = new GetMerchantListResponse();
		response.validate();
		return response;
	}

	@Override
	public void doProcess(MessageResponse response) throws ApplicationException {
		LOGGER.info("doProcess of GetMerchantListProcessImpl started ");
		GetMerchantListResponse res = (GetMerchantListResponse) response;
		List<Merchant> merchantDetailsList = pspMgmtService.getMerchantList();
		List<MerchantDetails> merchantDetails = new ArrayList<>();
	    for (Merchant merchantDetail : merchantDetailsList) {
	    	merchantDetails.add(MobileProcessUtility.prepareMerchantDetails(merchantDetail));
		 }
		res.setMerchantList(merchantDetails);
		LOGGER.info("doProcess of GetMerchantListProcessImpl completed ");
	}

	@Override
	public MessageResponse createResponseOnStatusCode(StatusCode code) {
		GetMerchantListResponse response = new GetMerchantListResponse();
		response.setStatusCode(code.getCode());
		response.setStatusMessage(code.getMessage());
		return response;
	}

}
