/**
 * 
 */
package psp.mobile.process.factory.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.upi.system_1_2.Ack;
import org.upi.system_1_2.CredSubType;

import psp.common.PropertyReader;
import psp.common.exception.ApplicationException;
import psp.constants.CommonConstants;
import psp.constants.ServiceNotes;
import psp.constants.ServiceRequestName;
import psp.constants.ServiceStatus;
import psp.constants.StatusCode;
import psp.constants.UserStatus;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.AccountDetails;
import psp.dbservice.model.SetCredentialUpiRequest;
import psp.mobile.model.request.SetPinRequest;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.model.response.SetPinResponse;
import psp.mobile.process.factory.MobileCoreProcess;
import psp.mobile.process.util.MobileProcessUtility;
import psp.upi.constants.CredType;
import psp.upi.constants.PayerType;
import psp.util.DtoObjectUtil;
import psp.util.PspClientTool;
import psp.util.dto.AcDto;
import psp.util.dto.CredDto;
import psp.util.dto.HeadDto;
import psp.util.dto.PayerDto;
import psp.util.dto.TxnDto;
import psp.util.upi.client.UpiClientService;

/**
 * @author prasadj
 *
 */
@Component("setPinMpfb")
public class SetPinProcessImpl extends MobileCoreProcess {

	private static final Logger LOGGER = Logger.getLogger(SetPinProcessImpl.class.getName());
	
	@Autowired
	private PspMgmtService pspMgmtService;
	
	@Autowired
	private UpiClientService upiClient;
	
	@Autowired
	private PropertyReader propertyReader;
	
	public SetPinProcessImpl() {
	}
	
	@Override
	public MessageResponse validateRequest() {
		SetPinResponse response = new SetPinResponse();
		response.validate();
		return response;
	}

	 /*1. login expire time has to increase
	   2. If credential type is MPIN then only we need to allow the service
	   3. Need to discuss about the user status update*/
	@Override
	public void doProcess(MessageResponse response) throws ApplicationException {
		LOGGER.info("doProcess of SetPinProcessImpl started ");
		SetPinRequest req = (SetPinRequest) request;
		AccountDetails accDetails = pspMgmtService.getAccountDetailsByUserDetailsId(user.getId());
		if(null != accDetails){
			String msgId = DtoObjectUtil.constructMessageId();
			String id = DtoObjectUtil.constructTxnId();
			HeadDto head = DtoObjectUtil.constructHeadDto(msgId, new Date());
			TxnDto txn = DtoObjectUtil.constructTxnDto(id, ServiceNotes.SET_CRED_NOTE, null, "", "", new Date(), ServiceRequestName.SET_CRED);
			String fullName = user.getFirstName() + CommonConstants.SPACE_STR + user.getMiddleName() + CommonConstants.SPACE_STR + user.getLastName();
			AcDto acDto = MobileProcessUtility.getAcDto(accDetails);
			List<CredDto> creds = new ArrayList<CredDto>();
			CredDto credDto = new CredDto();
			credDto.setData(req.getMobileCredentials().getDataValue());
			credDto.setType(CredType.OTP.name());
			credDto.setSubType(CredSubType.SMS.name());
			creds.add(credDto);
			List<CredDto> newCreds = new ArrayList<CredDto>();
			credDto = new CredDto();
			credDto.setData(req.getMobileCredentials().getDataValue());
			credDto.setType(CredType.OTP.name());
			credDto.setSubType(CredSubType.SMS.name());
			newCreds.add(credDto);
			PayerDto payerDto = DtoObjectUtil.constructPayerDto(accDetails.getVirtualAddress(), fullName, PayerType.PERSON.name(), acDto, null, creds, newCreds, null, null);
			
			String responseXml = upiClient.reqSetCre(head, txn, payerDto);
			Ack ack = PspClientTool.convertUpiRequest(responseXml, Ack.class);
			boolean respReceived = false;
			if(ack.getErr() == null) {
				pspMgmtService.saveUpiRequest(MobileProcessUtility.prepareUpiRequestDetails(msgId, id, SetCredentialUpiRequest.class));
				int maxDelay = 270; //seconds
				int count = 0;
				do {
					SetCredentialUpiRequest credsRes = (SetCredentialUpiRequest) pspMgmtService.getUpiRequestByTxnId(id);
					if (ServiceStatus.RECEIVED.name().equals(credsRes.getStatus())) {
						respReceived = true;
						if(CommonConstants.SUCCESS.equals(credsRes.getResult())) {
							prepareResponse(accDetails, credsRes, response);
						}
						else{
							prepareResponse(accDetails, credsRes, response);
							//throw new ApplicationException(StatusCode.SET_CREDENTIAL_FAILED);
						}
					}	
					try{
						count++;
						Thread.sleep(propertyReader.getMobileProcessDelay());
					}
					catch(Exception ex){
					}
					if(count > maxDelay){
						LOGGER.info("UPI Request time out ");
						prepareResponse(accDetails, credsRes, response);
						//throw new ApplicationException(StatusCode.SET_CREDENTIAL_FAILED);
					}
				}while (!respReceived);
			}
			else {
				throw new ApplicationException(StatusCode.SET_CREDENTIAL_FAILED);
			}
		}
		else {
			throw new ApplicationException(StatusCode.ACCOUNT_DETAILS_NOT_FOUND);
		}
		LOGGER.info("doProcess of SetPinProcessImpl completed ");
	}

	@Override
	public MessageResponse createResponseOnStatusCode(StatusCode code) {
		SetPinResponse response = new SetPinResponse();
		response.setStatusCode(code.getCode());
		response.setStatusMessage(code.getMessage());
		return response;
	}

	private void prepareResponse(AccountDetails accDetails, SetCredentialUpiRequest credsRes, MessageResponse response) {
		user.setStatus(UserStatus.ACTIVE.name());
		accDetails.setIsPinSet(true);
		pspMgmtService.updateAccountDetails(accDetails);
		pspMgmtService.updateUserDetails(user);
		pspMgmtService.deleteUpiRequest(credsRes);
		response.setStatusCode(StatusCode.SET_CRED_SERVICE_SUCCESS.getCode());
		response.setStatusMessage(StatusCode.SET_CRED_SERVICE_SUCCESS.getMessage());
	}
}