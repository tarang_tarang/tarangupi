package psp.mobile.process.account.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import psp.dbservice.model.AccountReference;
import psp.mobile.process.account.service.AccountListCacheService;

@Component("accountListCacheService")
public class AccountListCacheImpl implements AccountListCacheService {

	private Map<String, List<AccountReference>> userTempararyAccounts = new HashMap<String, List<AccountReference>>();

	public AccountListCacheImpl(){
	}
	
	@Override
	public List<AccountReference> getUserAccounts(String userName) {
		return userTempararyAccounts.get(userName);
	}

	@Override
	public void addUserAccounts(String userName, List<AccountReference> accSummary) {
		userTempararyAccounts.put(userName, accSummary);
	}

	@Override
	public void deleteUserAccounts(String userName) {
		userTempararyAccounts.remove(getUserAccounts(userName));
	}
	
}