package psp.mobile.process;

import psp.mobile.model.request.ForgetPasswordRequest;
import psp.mobile.model.request.MobileRequest;
import psp.mobile.model.response.MessageResponse;

public interface MobileRequestProcessor {

	MessageResponse processAuthorisedRequest(MobileRequest request);
	
	MessageResponse forgetPassword(ForgetPasswordRequest request);
	
	MessageResponse processNonAuthorisedRequest(MobileRequest request);
	
}