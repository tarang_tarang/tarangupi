package psp.mobile.process.factory.impl;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import psp.constants.StatusCode;
import psp.dbservice.mgmt.PspMgmtService;
import psp.mobile.model.response.GetExistingAccountsResponse;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.process.factory.MobileCoreProcess;

@Component("getExistingAccsMpfb")
public class GetExistingAccountsProcessImpl extends MobileCoreProcess {

	private static final Logger LOGGER = Logger.getLogger(GetExistingAccountsProcessImpl.class.getName());
	
	@Autowired
	private PspMgmtService pspMgmtService;

	public GetExistingAccountsProcessImpl() {
	}

	@Override
	public MessageResponse validateRequest() {
		GetExistingAccountsResponse response = new GetExistingAccountsResponse();
		response.validate();
		return response;
	}

	@Override
	public void doProcess(MessageResponse response) {
		LOGGER.info("doProcess of GetExistingAccountsProcessImpl started ");
		GetExistingAccountsResponse res = (GetExistingAccountsResponse) response;
		res.setAccountSummary(pspMgmtService.getAccountSummaryByUserDetailsId(user.getId()));
		LOGGER.info("doProcess of GetExistingAccountsProcessImpl completed ");
	}

	@Override
	public MessageResponse createResponseOnStatusCode(StatusCode code) {
		GetExistingAccountsResponse response = new GetExistingAccountsResponse();
		response.setStatusCode(code.getCode());
		response.setStatusMessage(code.getMessage());
		return response;
	}

}
