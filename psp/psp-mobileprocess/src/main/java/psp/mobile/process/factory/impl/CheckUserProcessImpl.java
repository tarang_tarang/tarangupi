/**
 * 
 */
package psp.mobile.process.factory.impl;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import psp.common.exception.ApplicationException;
import psp.constants.StatusCode;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.DeviceDetails;
import psp.mobile.model.request.CheckUserRequest;
import psp.mobile.model.response.CheckUserResponse;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.process.factory.MobileCoreProcess;

/**
 * @author prasadj
 *
 */
@Component("checkUserMpfb")
public class CheckUserProcessImpl extends MobileCoreProcess {

	private static final Logger LOGGER = Logger.getLogger(CheckUserProcessImpl.class.getName());
	
	@Autowired
	private PspMgmtService pspMgmtService;
	
	public CheckUserProcessImpl(){
	}
	
	@Override
	public MessageResponse validateRequest() {
		CheckUserResponse response = new CheckUserResponse();
		response.validate((CheckUserRequest)request);
		return response;
	}

	@Override
	public void doProcess(MessageResponse response) throws ApplicationException {
		LOGGER.info("doProcess of CheckUserProcessImpl started ");
		DeviceDetails device = pspMgmtService.getDeviceDetailsByTerminalId(request.getDevice().getTerminalId());
		if (null == device ) {
			throw new ApplicationException(StatusCode.USER_IS_NOT_REGISTERED);
		}
		LOGGER.info("doProcess of CheckUserProcessImpl completed ");
	}

	@Override
	public MessageResponse createResponseOnStatusCode(StatusCode code) {
		CheckUserResponse response = new CheckUserResponse();
		response.setStatusCode(code.getCode());
		response.setStatusMessage(code.getMessage());
		return response;
	}

}