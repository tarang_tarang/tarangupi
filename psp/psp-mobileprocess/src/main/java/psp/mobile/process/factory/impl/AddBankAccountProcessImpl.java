/**
 * 
 */
package psp.mobile.process.factory.impl;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import psp.constants.StatusCode;
import psp.constants.UserStatus;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.AccountDetails;
import psp.dbservice.model.AccountReference;
import psp.mobile.model.request.AddBankAccountRequest;
import psp.mobile.model.response.AddBankAccountResponse;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.process.account.service.AccountListCacheService;
import psp.mobile.process.factory.MobileCoreProcess;

/**
 * @author prasadj
 *
 */
@Component("addBankAccountMpfb")
public class AddBankAccountProcessImpl extends MobileCoreProcess {

	private static final Logger LOGGER = Logger.getLogger(AddBankAccountProcessImpl.class.getName());
	
	@Autowired
	private PspMgmtService pspMgmtService;

	@Autowired
	private AccountListCacheService accountSummaryService;
	
	public AddBankAccountProcessImpl() {
	}
	
	@Override
	public MessageResponse validateRequest() {
		AddBankAccountResponse response = new AddBankAccountResponse();
		response.validate();
		return response;
	}

	@Override
	public void doProcess(MessageResponse response) {
		LOGGER.info("doProcess of AddBankAccountProcessImpl started ");
		AddBankAccountRequest req = (AddBankAccountRequest) request;
		AddBankAccountResponse res = (AddBankAccountResponse) response;
		List<AccountReference> cacheList = accountSummaryService.getUserAccounts(request.getUserName());
		for(AccountReference acc: cacheList) {
			if(acc.getId().equals(req.getId())) {
				user.setStatus(UserStatus.ADD_BANK.name());
				pspMgmtService.updateUserDetails(user);
				AccountDetails accDetails = pspMgmtService.getAccountDetailsByUserDetailsId(user.getId());
				accDetails.setVirtualAddress(user.getUserName() + "@tarang"); //TODO need to get it from properties file
				acc.setAccountDetails(accDetails);
				pspMgmtService.updateAccountDetails(accDetails);
				pspMgmtService.saveAccountReference(acc);
				accountSummaryService.deleteUserAccounts(request.getUserName());
				res.setVirtualAddress(accDetails.getVirtualAddress());
				break;
			}
		}
		LOGGER.info("doProcess of AddBankAccountProcessImpl completed");
	}

	@Override
	public MessageResponse createResponseOnStatusCode(StatusCode code) {
		AddBankAccountResponse response = new AddBankAccountResponse();
		response.setStatusCode(code.getCode());
		response.setStatusMessage(code.getMessage());
		return response;
	}

}