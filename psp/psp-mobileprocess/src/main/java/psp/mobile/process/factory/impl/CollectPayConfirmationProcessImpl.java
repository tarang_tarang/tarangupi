package psp.mobile.process.factory.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.upi.system_1_2.CredSubType;
import org.upi.system_1_2.CredType;
import org.upi.system_1_2.PayTrans;
import org.upi.system_1_2.PayerConstant;
import org.upi.system_1_2.PayerType;
import org.upi.system_1_2.ReqAuthDetails;
import org.upi.system_1_2.RespAuthDetails;
import org.upi.system_1_2.RespType;

import psp.common.exception.ApplicationException;
import psp.constants.CommonConstants;
import psp.constants.NotificationStatus;
import psp.constants.StatusCode;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.AccountDetails;
import psp.dbservice.model.NotificationDetails;
import psp.mobile.model.request.CollectPayConfirmRequest;
import psp.mobile.model.response.CollectPayConfirmResponse;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.process.factory.MobileCoreProcess;
import psp.util.DtoObjectUtil;
import psp.util.PspClientTool;
import psp.util.dto.AcDto;
import psp.util.dto.CredDto;
import psp.util.dto.IdentityInfoDto;
import psp.util.dto.PayerDto;
import psp.util.dto.TxnDto;
import psp.util.upi.client.UpiClientService;
import psp.util.upiclient.UpiDataPreparationUtility;

@Component("collectPayConfirmationMpfb")
public class CollectPayConfirmationProcessImpl extends MobileCoreProcess {

	private static final Logger LOGGER = Logger.getLogger(CollectPayConfirmationProcessImpl.class.getName());
	
	@Autowired
	private PspMgmtService pspMgmtService;
	
	@Autowired
	private UpiClientService upiClientService;
	
	public CollectPayConfirmationProcessImpl() {
	}
	
	@Override
	public MessageResponse validateRequest() {
		CollectPayConfirmResponse response = new CollectPayConfirmResponse();
		response.validate();
		return response;
	}

	@Override
	public void doProcess(MessageResponse response) throws ApplicationException {
		LOGGER.info("doProcess of CollectPayConfirmationProcessImpl started ");
		CollectPayConfirmRequest req = (CollectPayConfirmRequest) request;
		NotificationDetails notification = pspMgmtService.getNotificationDetailsByNotificationId(req.getNotificationId());
		if(null != notification) {
			ReqAuthDetails reqAuthDetails = PspClientTool.convertUpiRequest(notification.getUpiMessagePacket().getMessagePacket(), ReqAuthDetails.class);
			upiClientService.respAuthDetails(prepareCollectrequest(reqAuthDetails, req));
			notification.setStatus(NotificationStatus.NOTIFIED.name());
			pspMgmtService.updateNotificationDetails(notification);	
			if(req.getRejected()) {
				response.setStatusCode(StatusCode.COLLECT_PAY_REJECTED_SUCCESS.getCode());
				response.setStatusMessage(StatusCode.COLLECT_PAY_REJECTED_SUCCESS.getMessage());
			}
		}
		LOGGER.info("doProcess of CollectPayConfirmationProcessImpl completed ");
	}

	@Override
	public MessageResponse createResponseOnStatusCode(StatusCode code) {
		CollectPayConfirmResponse response = new CollectPayConfirmResponse();
		response.setStatusCode(code.getCode());
		response.setStatusMessage(code.getMessage());
		return response;
	}

	private RespAuthDetails prepareCollectrequest(ReqAuthDetails reqAuthDetails, CollectPayConfirmRequest req){
		Date date = new Date();
		String messageId = DtoObjectUtil.constructMessageId();
		PayTrans payTrans = reqAuthDetails.getTxn();
		TxnDto txn = DtoObjectUtil.constructTxnDto(payTrans.getId(), payTrans.getNote(), null, payTrans.getRefId(), payTrans.getRefUrl(), date, payTrans.getType().name());
		
		PayerType payerType = reqAuthDetails.getPayer();
		AccountDetails accountDetails = pspMgmtService.getAccountDetailsByVirtualAddr(reqAuthDetails.getPayer().getAddr());
		AcDto acDto = new AcDto();//TODO need to change
		acDto.setAddrType(accountDetails.getAddressType());
		acDto.setAcnum(accountDetails.getAccountNumber());
		acDto.setActype(accountDetails.getAccountType());
		acDto.setIfsc(accountDetails.getIfsc());
		
		List<CredDto> creds =null;
		RespType respType = new RespType();
		respType.setReqMsgId(reqAuthDetails.getHead().getMsgId());
		if (!req.getRejected()) {
			creds = new ArrayList<CredDto>();
			creds.add(DtoObjectUtil.constructCredDto(req.getMobileCredentials().getDataValue(), CredType.OTP, CredSubType.SMS));
			respType.setResult(CommonConstants.SUCCESS);
		}
		else {
			respType.setResult("FAILURE");
		}
		IdentityInfoDto identityInfoDto = DtoObjectUtil.constructIdentityInfoDto(payerType.getAddr());
		PayerDto payerDto = DtoObjectUtil.constructPayerDto(payerType.getAddr(), null, PayerConstant.PERSON.name(), acDto, payerType.getAmount().getValue(), creds, null, req.getDevice(), identityInfoDto);
	
		RespAuthDetails respAuthDetails = new RespAuthDetails();
		respAuthDetails.setHead(UpiDataPreparationUtility.convertHeader(DtoObjectUtil.constructHeadDto(messageId, date)));
		respAuthDetails.setTxn(UpiDataPreparationUtility.convertTxn(txn));
		respAuthDetails.setPayees(reqAuthDetails.getPayees());
		respAuthDetails.setPayer(UpiDataPreparationUtility.convertPayer(payerDto));
		respAuthDetails.setResp(respType);
		return respAuthDetails;
	}
}
