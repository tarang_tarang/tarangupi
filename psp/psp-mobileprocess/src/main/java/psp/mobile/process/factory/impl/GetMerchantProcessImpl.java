package psp.mobile.process.factory.impl;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import psp.common.exception.ApplicationException;
import psp.constants.StatusCode;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.Merchant;
import psp.mobile.model.request.GetMerchantRequest;
import psp.mobile.model.response.GetMerchantResponse;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.process.factory.MobileCoreProcess;
import psp.mobile.process.util.MobileProcessUtility;

@Component("getMerchantMpfb")
public class GetMerchantProcessImpl extends MobileCoreProcess {

	private static final Logger LOGGER = Logger.getLogger(GetMerchantProcessImpl.class.getName());
	
	@Autowired
	private PspMgmtService pspMgmtService;
	
	public GetMerchantProcessImpl() {	
	}
	
	@Override
	public MessageResponse validateRequest() {
		GetMerchantResponse response = new GetMerchantResponse();
		response.validate();
		return response;
	}

	@Override
	public void doProcess(MessageResponse response) throws ApplicationException {
		LOGGER.info("doProcess of GetMerchantProcessImpl started ");
		GetMerchantResponse resp = (GetMerchantResponse) response;
		GetMerchantRequest req = (GetMerchantRequest) request;
		Merchant merchantDetails = pspMgmtService.getMerchantById(req.getMerchantId());	
			resp.setMerchantDetails(MobileProcessUtility.prepareMerchantDetails(merchantDetails));
		LOGGER.info("doProcess of GetMerchantProcessImpl completed ");
	}

	@Override
	public MessageResponse createResponseOnStatusCode(StatusCode code) {
		GetMerchantResponse response = new GetMerchantResponse();
		response.setStatusCode(code.getCode());
		response.setStatusMessage(code.getMessage());
		return response;
	}

}
