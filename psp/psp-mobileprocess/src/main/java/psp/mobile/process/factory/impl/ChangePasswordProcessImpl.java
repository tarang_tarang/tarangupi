/**
 * 
 */
package psp.mobile.process.factory.impl;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import psp.common.exception.ApplicationException;
import psp.constants.StatusCode;
import psp.dbservice.mgmt.PspMgmtService;
import psp.mobile.model.request.ChangePasswordRequest;
import psp.mobile.model.response.ChangePasswordResponse;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.process.factory.MobileCoreProcess;

/**
 * @author prasadj
 *
 */
@Component("changePasswordMpfb")
public class ChangePasswordProcessImpl extends MobileCoreProcess {

	private static final Logger LOGGER = Logger.getLogger(ChangePasswordProcessImpl.class.getName());
	
	@Autowired
	private PspMgmtService pspMgmtService;
	
	public ChangePasswordProcessImpl(){
	}
	
	@Override
	public MessageResponse validateRequest() {
		ChangePasswordResponse response = new ChangePasswordResponse();
		response.validate((ChangePasswordRequest)request);
		return response;
	}

	@Override
	public void doProcess(MessageResponse response) throws ApplicationException {
		LOGGER.info("doProcess of ChangePasswordProcessImpl started ");
		ChangePasswordRequest chgPwdreq = (ChangePasswordRequest) request;
		if(user.getPassword().equals(chgPwdreq.getOldPassword())) {
			user.setPassword(chgPwdreq.getNewPassword());
			pspMgmtService.updateUserDetails(user);
		}
		else {
			throw new ApplicationException(StatusCode.INVALID_OLD_PASSWORD);
		}	
		LOGGER.info("doProcess of ChangePasswordProcessImpl completed ");
	}

	@Override
	public MessageResponse createResponseOnStatusCode(StatusCode code) {
		ChangePasswordResponse response = new ChangePasswordResponse();
		response.setStatusCode(code.getCode());
		response.setStatusMessage(code.getMessage());
		return response;
	}

}