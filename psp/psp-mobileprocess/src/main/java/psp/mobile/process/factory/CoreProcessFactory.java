/**
 * 
 */
package psp.mobile.process.factory;

/**
 * @author prasadj
 *
 */
public interface CoreProcessFactory {

	MobileCoreProcess getCoreProcess(String name);
	
}