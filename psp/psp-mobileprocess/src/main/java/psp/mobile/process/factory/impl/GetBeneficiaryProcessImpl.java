package psp.mobile.process.factory.impl;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import psp.common.exception.ApplicationException;
import psp.constants.StatusCode;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.BeneficiaryDetails;
import psp.mobile.model.request.GetBeneficiaryRequest;
import psp.mobile.model.response.GetBeneficiaryResponse;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.process.factory.MobileCoreProcess;

@Component("getBeneficiaryMpfb")
public class GetBeneficiaryProcessImpl extends MobileCoreProcess {

	private static final Logger LOGGER = Logger.getLogger(GetBeneficiaryProcessImpl.class.getName());
	
	@Autowired
	private PspMgmtService pspMgmtService;
	
	public GetBeneficiaryProcessImpl() {
	}
	
	@Override
	public MessageResponse validateRequest() {
		GetBeneficiaryResponse response = new GetBeneficiaryResponse();
		response.validate((GetBeneficiaryRequest)request);
		return response;
	}

	@Override
	public void doProcess(MessageResponse response) throws ApplicationException {
		LOGGER.info("doProcess of GetBeneficiaryProcessImpl started ");
		GetBeneficiaryRequest req = (GetBeneficiaryRequest) request;
		GetBeneficiaryResponse res = (GetBeneficiaryResponse) response;
		BeneficiaryDetails bnfDtls = pspMgmtService.getBeneficiaryDetails(user.getUserName(), req.getNickName());
		if ( null != bnfDtls ){
			if (null != bnfDtls.getAccountDetails()) {
				res.setReferenceNumber(bnfDtls.getAccountDetails().getReferenceNumber());
				res.setMaskedNumber(bnfDtls.getAccountDetails().getMaskedNumber());
				res.setIfsc(bnfDtls.getAccountDetails().getIfsc());		
				res.setMmid(bnfDtls.getAccountDetails().getMmid());
				res.setAeba(bnfDtls.getAccountDetails().getAeba());
			}
			res.setAddress(bnfDtls.getAddress());
			res.setAddressType(bnfDtls.getAddressType());
			res.setNickName(bnfDtls.getNickName());
			res.setName(bnfDtls.getUserName());
			res.setDescription(bnfDtls.getDescription());
		}
		else {
			throw new ApplicationException(StatusCode.GET_BENF_SERVICE_FAIL) ;
		}		
		LOGGER.info("doProcess of GetBeneficiaryProcessImpl completed ");
	}

	@Override
	public MessageResponse createResponseOnStatusCode(StatusCode code) {
		GetBeneficiaryResponse response = new GetBeneficiaryResponse();
		response.setStatusCode(code.getCode());
		response.setStatusMessage(code.getMessage());
		return response;
	}

}