/**
 * 
 */
package psp.mobile.process.util;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.apache.log4j.Logger;
import org.upi.system_1_2.Ack;
import org.upi.system_1_2.PayConstant;

import psp.common.PropertyReader;
import psp.constants.ServiceNotes;
import psp.constants.ServiceRequestName;
import psp.dbservice.mgmt.PspMgmtService;
import psp.util.DtoObjectUtil;
import psp.util.PspClientTool;
import psp.util.dto.HeadDto;
import psp.util.dto.TxnDto;
import psp.util.upi.client.UpiClientService;

/**
 * @author manasp
 *
 */
public class GetBankProvidersTimerTask extends TimerTask {

	private static final Logger LOGGER = Logger.getLogger(GetBankProvidersTimerTask.class.getName());
	
	private static GetBankProvidersTimerTask getBankAccountsTimerTask;
	
	private PspMgmtService pspMgmtService;
	
	private UpiClientService upiClientService;
	
	private PropertyReader propertyReader;
	
	private Timer timer;
	
	private boolean isStarted = false;
	
	private static final long START_UP_DELAY = 1*60*1000;
	
	public static final long ONE_HOUR = 4*60*1000;
	
	private GetBankProvidersTimerTask(PspMgmtService pspMgmtService, UpiClientService upiClientService, PropertyReader propertyReader) {
		this.pspMgmtService = pspMgmtService;
		this.upiClientService = upiClientService;
		this.propertyReader = propertyReader;
	}
	
	static {
	    HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier()
	        {
	            public boolean verify(String hostname, SSLSession session)
	            {
	                // ip address of the service URL(like.23.28.244.244)
	                if (hostname.equals("103.14.161.149"))//TODO need to get from properties.
	                    return true;
	                return false;
	            }
	        });
	}
	
	@Override
	public void run() {
		LOGGER.info("GetBankAccounts Timer task started at:" + new Date());
		try {
			Date date = new Date();
			String messageId = DtoObjectUtil.constructMessageId();
			HeadDto head = DtoObjectUtil.constructHeadDto(messageId, date);
			String id = DtoObjectUtil.constructTxnId();
			String note = ServiceNotes.ACC_PROV_LIST_NOTE;
			String orgTxnId = null;
			String refId = "";
			String refUrl = "";
			String type = ServiceRequestName.LIST_ACCOUNT_PROVIDER;
			TxnDto txn = DtoObjectUtil.constructTxnDto(id, note, orgTxnId, refId, refUrl, date, type);
			String responseXml = upiClientService.reqListAccPvd(head, txn);	
			Ack ack = PspClientTool.convertUpiRequest(responseXml, Ack.class);
			
			//TODO handle the error
			
			id = DtoObjectUtil.constructTxnId();
			note = ServiceNotes.HEART_BEAT;
			type = ServiceRequestName.REQ_HBT;
			txn = DtoObjectUtil.constructTxnDto(id, note, orgTxnId, refId, refUrl, date, type);
			responseXml = upiClientService.reqHbt(head, txn);	
			ack = PspClientTool.convertUpiRequest(responseXml, Ack.class);
			
			id = DtoObjectUtil.constructTxnId();
		    note = ServiceNotes.ACC_PROV_LIST_NOTE;
		    type = PayConstant.LIST_KEYS.name();
		    txn = DtoObjectUtil.constructTxnDto(id, note, orgTxnId, refId, refUrl, date, type);   
		    responseXml = upiClientService.reqListKeys(head, txn);
		    ack = PspClientTool.convertUpiRequest(responseXml, Ack.class);
			   
		}
		catch (Exception e) {
			LOGGER.warn(e.getMessage());
			LOGGER.debug(e.getMessage(), e);
		}
		LOGGER.info("GetBankAccounts Timer task finished at:" + new Date());
	}

	public static GetBankProvidersTimerTask getGetBankAccountsTimerTask(PspMgmtService pspMgmtService, UpiClientService upiClientService, PropertyReader propertyReader){
		if(getBankAccountsTimerTask == null){
			getBankAccountsTimerTask = new GetBankProvidersTimerTask(pspMgmtService, upiClientService, propertyReader);
		}
		return getBankAccountsTimerTask;
	}
	
	public void startTimer(){
		if(!isStarted){
	        timer = new Timer(true);
	        timer.scheduleAtFixedRate(this, START_UP_DELAY, propertyReader.getBankProvidersNumber() * ONE_HOUR); 
	        //timer.scheduleAtFixedRate(this, START_UP_DELAY, START_UP_DELAY); 
	        LOGGER.info("GetBankAccounts Timer stared...........");
	        isStarted = true;
		}
		else {
			LOGGER.info("GetBankAccounts Timer already started.....");
		}
	}
	
	public void stopTimer(){
		if(isStarted){
			timer.cancel();
			LOGGER.info("GetBankAccounts Timer stoped...........");
			isStarted = false;
		}
		else {
			LOGGER.info("GetBankAccounts Timer already stoped.....");
		}
	}
	
}
