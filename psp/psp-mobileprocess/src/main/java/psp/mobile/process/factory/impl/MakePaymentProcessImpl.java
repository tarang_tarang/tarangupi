/**
 * 
 */
package psp.mobile.process.factory.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.upi.system_1_2.Ack;
import org.upi.system_1_2.AddressType;
import org.upi.system_1_2.CredSubType;
import org.upi.system_1_2.CredType;
import org.upi.system_1_2.PayConstant;

import psp.common.PropertyReader;
import psp.common.exception.ApplicationException;
import psp.constants.CommonConstants;
import psp.constants.StatusCode;
import psp.constants.TxnStatus;
import psp.constants.TxnType;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.AccountDetails;
import psp.dbservice.model.BeneficiaryDetails;
import psp.dbservice.model.TransactionDetails;
import psp.mobile.model.request.PaymentRequest;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.model.response.PaymentResponse;
import psp.mobile.process.factory.MobileCoreProcess;
import psp.mobile.process.util.MobileProcessUtility;
import psp.upi.constants.PayerType;
import psp.util.DtoObjectUtil;
import psp.util.PspClientTool;
import psp.util.dto.AcDto;
import psp.util.dto.CredDto;
import psp.util.dto.HeadDto;
import psp.util.dto.IdentityInfoDto;
import psp.util.dto.PayeeDto;
import psp.util.dto.PayerDto;
import psp.util.dto.TxnDto;
import psp.util.upi.client.UpiClientService;
/**
 * @author prasadj
 *
 */
@Component("makePaymentMpfb")
public class MakePaymentProcessImpl extends MobileCoreProcess {

	private static final Logger LOGGER = Logger.getLogger(MakePaymentProcessImpl.class.getName());
	
	@Autowired
	private PspMgmtService pspMgmtService;
	
	@Autowired
	private UpiClientService upiClient;
	
	@Autowired
	private PropertyReader propertyReader;
	
	public MakePaymentProcessImpl(){
	}
	
	@Override
	public MessageResponse validateRequest() {
		PaymentResponse response = new PaymentResponse();
		response.validate((PaymentRequest)request);
		return response;
	}

	@Override
	public void doProcess(MessageResponse response) throws ApplicationException {
		LOGGER.info("doProcess of MakePaymentProcessImpl started ");
		PaymentRequest req = (PaymentRequest)request;
		String messageId = DtoObjectUtil.constructMessageId();
		String id = req.getTxnId();
		Date date = new Date();
		HeadDto head = DtoObjectUtil.constructHeadDto(messageId, date);
		TxnDto txn = DtoObjectUtil.constructTxnDto(id, req.getTxnNote(), "", "", "", date, PayConstant.PAY.name());
		
		String fullName = user.getFirstName() + CommonConstants.SPACE_STR + user.getMiddleName() + CommonConstants.SPACE_STR + user.getLastName();
		String payerType = PayerType.PERSON.name();
		AccountDetails accDetails = pspMgmtService.getAccountDetailsByVirtualAddr(req.getSelfVirtualAddr()); 
		AcDto acDto = null;
		if(null != accDetails) {
			acDto  = MobileProcessUtility.getAcDto(accDetails);
		}
		else {
			throw new ApplicationException(StatusCode.ACC_DETAILS_NOT_FOUND);
		}		
		List<CredDto> creds = new ArrayList<CredDto>();
		creds.add(DtoObjectUtil.constructCredDto(req.getMobileCredentials().getDataValue(), CredType.OTP, CredSubType.SMS));
		
		IdentityInfoDto identityInfoDto = DtoObjectUtil.constructIdentityInfoDto(req.getSelfVirtualAddr());
		PayerDto payerDto = DtoObjectUtil.constructPayerDto(req.getSelfVirtualAddr(), fullName, payerType, acDto, req.getAmount().toString(), creds, null, request.getDevice(), identityInfoDto);
		
		PayeeDto payeeDto = null;
		if (TxnType.VIRTUAL_ADDRESS == TxnType.getTxnType(req.getType()) || TxnType.MERCHANT == TxnType.getTxnType(req.getType())) {
			payeeDto = DtoObjectUtil.constructPayeeDto(req.getThirdPartyVirtualAddr(), null, payerType, null, req.getAmount().toString(), null, null, null, null);
		}
		else if (TxnType.MOBILE == TxnType.getTxnType(req.getType()) || TxnType.AADHAAR == TxnType.getTxnType(req.getType())) {
			BeneficiaryDetails beneficiaryDetails = pspMgmtService.getBeneficiaryDetailsByNickName(req.getNickName());
			if (beneficiaryDetails != null) {
				AcDto acoountDto = prepareAcDto(beneficiaryDetails);
				String payeeAddr = acoountDto.getAcnum() + "@" + beneficiaryDetails.getAccountDetails().getIfsc() +".ifsc.npci";
				payeeDto = DtoObjectUtil.constructPayeeDto(payeeAddr, req.getNickName(), payerType, acoountDto, req.getAmount().toString(), null, null, null, null);
			}
			else {
				throw new ApplicationException(StatusCode.INVALID_PAYEE_ADDRESS);
			}
		}
		else if (TxnType.BENEFICIARY == TxnType.getTxnType(req.getType())){
			BeneficiaryDetails beneficiaryDetails = pspMgmtService.getBeneficiaryDetailsByNickName(req.getNickName());
			if (beneficiaryDetails != null) {
				if (TxnType.VIRTUAL_ADDRESS == TxnType.getTxnType(beneficiaryDetails.getAddressType())) {
					payeeDto = DtoObjectUtil.constructPayeeDto(beneficiaryDetails.getAddress(), beneficiaryDetails.getUserName(), payerType, null, req.getAmount().toString(), null, null, null, null);
				}
				else {
					AcDto acoountDto = prepareAcDto(beneficiaryDetails);
					String payeeAddr = acoountDto.getAcnum() + "@" + beneficiaryDetails.getAccountDetails().getIfsc() +".ifsc.npci";
					payeeDto = DtoObjectUtil.constructPayeeDto(payeeAddr, req.getNickName(), payerType, acoountDto, req.getAmount().toString(), null, null, null, null);
				}
			}
			else {
				throw new ApplicationException(StatusCode.INVALID_PAYEE_ADDRESS);
			}
		}
		
		pspMgmtService.saveTransactionDetails(prepareTransactionDetails(req, id));
		
		String responseXml = upiClient.reqPay(head, txn, payerDto, payeeDto);
		Ack ack = PspClientTool.convertUpiRequest(responseXml, Ack.class);
		
		if(ack.getErr() == null) {
			PaymentResponse paymentResponse = (PaymentResponse)response;
			paymentResponse.setStatusCode(StatusCode.PAYMENT_INITIATION_SUCCESS.getCode());
			paymentResponse.setStatusMessage(StatusCode.PAYMENT_INITIATION_SUCCESS.getMessage());
		}
		else {
			throw new ApplicationException(StatusCode.PAYMENT_INITIATION_FAIL);
		}
		LOGGER.info("doProcess of MakePaymentProcessImpl completed ");
	}

	@Override
	public MessageResponse createResponseOnStatusCode(StatusCode code) {
		PaymentResponse response = new PaymentResponse();
		response.setStatusCode(code.getCode());
		response.setStatusMessage(code.getMessage());
		return response;
	}
	
	private AcDto prepareAcDto(BeneficiaryDetails beneficiaryDetails) {
		AcDto acoountDto = new AcDto();
		acoountDto.setAddrType(AddressType.ACCOUNT.name());//TODO need to fetch from db
		acoountDto.setActype("SAVINGS");//TODO need to fetch from db
		acoountDto.setAcnum("1288000011118");//TODO need to fetch from db
		acoountDto.setIfsc(beneficiaryDetails.getAccountDetails().getIfsc());
		return acoountDto;
	}
	
	private TransactionDetails prepareTransactionDetails(PaymentRequest req, String txnId) {
		TransactionDetails txnDetails = new TransactionDetails();
		txnDetails.setFromAddr(req.getSelfVirtualAddr());
		txnDetails.setToAddr(req.getThirdPartyVirtualAddr());
		txnDetails.setStatus(TxnStatus.INITIATED.name());
		txnDetails.setTxnType(PayConstant.PAY.name());
		txnDetails.setInitiator(true);
		txnDetails.setTxnInitiationTime(new Date());
		txnDetails.setAmount(String.valueOf(req.getAmount()));
		txnDetails.setTxnMessage(req.getTxnNote());
		txnDetails.setTxnId(txnId);
		return txnDetails;
	}
}