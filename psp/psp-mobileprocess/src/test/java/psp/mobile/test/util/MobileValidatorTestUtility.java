package psp.mobile.test.util;

import psp.mobile.model.request.AcDetails;
import psp.mobile.model.request.AddBeneficiaryRequest;
import psp.mobile.model.request.BalanceEnquiryRequest;
import psp.mobile.model.request.ChangePasswordRequest;
import psp.mobile.model.request.CheckUserRequest;
import psp.mobile.model.request.GetAccountListRequest;
import psp.mobile.model.request.GetTxnDetailsRequest;
import psp.mobile.model.request.LoginRequest;
import psp.mobile.model.request.MobileCredentials;
import psp.mobile.model.request.OtpRequest;
import psp.mobile.model.request.PaymentRequest;
import psp.mobile.model.request.RegistrationRequest;
import psp.mobile.model.request.SetPinRequest;

public class MobileValidatorTestUtility {

	private MobileValidatorTestUtility() {
	}
	
	public static RegistrationRequest prepareRegistrationRequest(
			String firstName, String lastName, String middleName,
			String adharNumber, String mobileNumber, String email,
			String userName, String password) {
		RegistrationRequest request = new RegistrationRequest();
		request.setFirstName(firstName);
		request.setLastName(lastName);
		request.setMiddleName(middleName);
		request.setAdharNumber(adharNumber);
		request.setMobileNumber(mobileNumber);
		request.setEmail(email);
		request.setUserName(userName);
		request.setPassword(password);
		return request; 
	}
	
	public static LoginRequest prepareLoginRequest(String userName, String password) {
		LoginRequest request = new LoginRequest();
		request.setUserName(userName);
		request.setPassword(password);
		return request; 
	}
	
	public static ChangePasswordRequest prepareChangePasswordRequest(
			String userName, String oldPassword, String newPassword) {
		ChangePasswordRequest request = new ChangePasswordRequest();
		request.setUserName(userName);
		request.setOldPassword(oldPassword);
		request.setNewPassword(newPassword);
		return request; 
	}
	
	public static SetPinRequest prepareSetPinRequest(String oldPin, String newPin, Boolean isChangePin) {
		SetPinRequest request = new SetPinRequest();
		MobileCredentials mc = new MobileCredentials();
		mc.setDataValue(newPin);
		request.setIsChangePin(isChangePin);
		request.setMobileCredentials(mc);
		return request; 
	}

	public static PaymentRequest preparePaymentRequest(Double amount,
			String selfvirtualAddress, String thirdPartyVirtualAddress,String mPin) {
		PaymentRequest request = new PaymentRequest();
		request.setAmount(amount);
		request.setSelfVirtualAddr(selfvirtualAddress);
		request.setThirdPartyVirtualAddr(thirdPartyVirtualAddress);
		MobileCredentials mc = new MobileCredentials();
		mc.setDataValue(mPin);
		request.setMobileCredentials(mc);
		return request;
	}

	public static BalanceEnquiryRequest BalanceEnquiryRequest(String mPin) {
		BalanceEnquiryRequest request = new BalanceEnquiryRequest();
		MobileCredentials mc = new MobileCredentials();
		mc.setDataValue(mPin);
		request.setMobileCredentials(mc);
		return request;
	}

	public static GetAccountListRequest validateAccListRequest(String mmId,String otp) {
		GetAccountListRequest request = new GetAccountListRequest();
		request.setIsBenAccDetails(false);
		AcDetails ac = new AcDetails();
		ac.setAddrType("MOBILE");
		ac.setMmid(mmId);
		MobileCredentials mc = new MobileCredentials();
		mc.setDataValue(otp);
		request.setMobileCredentials(mc);
		request.setAcDetails(ac);
		return request;
	}
	
	public static CheckUserRequest validateCheckUserRequest(String mobileNumber) {
		CheckUserRequest request = new CheckUserRequest();
		request.setMobileNum(mobileNumber);
		return request;
	}
	
	public static GetTxnDetailsRequest validateGetTxnDetailsRequest(String fromDate,String toDate) {
		GetTxnDetailsRequest request = new GetTxnDetailsRequest();
		request.setFromDate(fromDate);
		request.setToDate(toDate);
		return request;
	}

	public static AddBeneficiaryRequest validateCheckAddBeneficiaryRequest(
			String nickName, String beneficiaryNote, String txnNote,Double amount) {
		AddBeneficiaryRequest request = new AddBeneficiaryRequest();
		request.setNickName(nickName);
		request.setBeneficiaryNote(beneficiaryNote);
		return request;
	}

	public static OtpRequest OtpRequestAccount(String addrType, String ifsc, String accounttype, String accountNum) {
		OtpRequest request = new OtpRequest();
		AcDetails ac = new AcDetails();
		ac.setAddrType(addrType);
		ac.setIfsc(ifsc);
		ac.setActype(accounttype);
		ac.setAcnum(accountNum);
		request.setAcDetails(ac);
		return request;
	}
	
	public static OtpRequest OtpRequestAadhaar(String addrType, String iNN, String uidNum) {
		OtpRequest request = new OtpRequest();
		AcDetails ac = new AcDetails();
		ac.setAddrType(addrType);
		ac.setIin(iNN);
		ac.setUidnum(uidNum);
		return request;
	}

	public static OtpRequest OtpRequestMobile(String addrType, String mobile, String mmid) {
		OtpRequest request = new OtpRequest();
		AcDetails ac = new AcDetails();
		ac.setAddrType(addrType);
		ac.setMobnum(mobile);
		ac.setMmid(mmid);
		return request;
	}

	public static OtpRequest OtpRequestCard(String addrType, String cardNum) {
		OtpRequest request = new OtpRequest();
		AcDetails ac = new AcDetails();
		ac.setAddrType(addrType);
		ac.setCardNum(cardNum);
		return request;
	}
}
