/**
 * 
 */
package psp.mobile.process.test;

import junit.framework.Assert;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import psp.constants.StatusCode;
import psp.db.test.AbstractServiceTest;
import psp.mobile.model.request.AddBankAccountRequest;
import psp.mobile.model.request.AddBeneficiaryRequest;
import psp.mobile.model.request.BalanceEnquiryRequest;
import psp.mobile.model.request.ChangePasswordRequest;
import psp.mobile.model.request.CheckUserRequest;
import psp.mobile.model.request.CollectPayConfirmRequest;
import psp.mobile.model.request.CollectPaymentRequest;
import psp.mobile.model.request.ForgetPasswordRequest;
import psp.mobile.model.request.GeneratePinRequest;
import psp.mobile.model.request.GetAccountListRequest;
import psp.mobile.model.request.GetAccountProvidersRequest;
import psp.mobile.model.request.GetBeneficiaryListRequest;
import psp.mobile.model.request.GetBeneficiaryRequest;
import psp.mobile.model.request.GetExistingAccountsRequest;
import psp.mobile.model.request.GetKeyRequest;
import psp.mobile.model.request.GetMerchantListRequest;
import psp.mobile.model.request.GetMerchantRequest;
import psp.mobile.model.request.GetNotificationDetailsRequest;
import psp.mobile.model.request.GetNotificationListRequest;
import psp.mobile.model.request.GetTokenRequest;
import psp.mobile.model.request.GetTxnDetailsRequest;
import psp.mobile.model.request.LoginRequest;
import psp.mobile.model.request.LogoutRequest;
import psp.mobile.model.request.OtpRequest;
import psp.mobile.model.request.PaymentRequest;
import psp.mobile.model.request.RegistrationRequest;
import psp.mobile.model.request.SetPinRequest;
import psp.mobile.model.response.AddBankAccountResponse;
import psp.mobile.model.response.BalanceEnquiryResponse;
import psp.mobile.model.response.ChangePasswordResponse;
import psp.mobile.model.response.CheckUserResponse;
import psp.mobile.model.response.ForgetPasswordResponse;
import psp.mobile.model.response.GetAccountListResponse;
import psp.mobile.model.response.GetAccountProvidersResponse;
import psp.mobile.model.response.GetExistingAccountsResponse;
import psp.mobile.model.response.GetKeyResponse;
import psp.mobile.model.response.GetMerchantListResponse;
import psp.mobile.model.response.GetMerchantResponse;
import psp.mobile.model.response.GetTokenResponse;
import psp.mobile.model.response.GetTxnsSummaryResponse;
import psp.mobile.model.response.LoginResponse;
import psp.mobile.model.response.LogoutResponse;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.model.response.OtpResponse;
import psp.mobile.model.response.PaymentResponse;
import psp.mobile.model.response.RegistrationResponse;
import psp.mobile.model.response.SetPinResponse;
import psp.mobile.process.factory.CoreProcessFactory;
import psp.mobile.process.factory.MobileCoreProcess;
import psp.mobile.process.util.MobileProcessUtility;

/**
 * @author prasadj
 *
 */
public class FactoryProcessValidatorTest extends AbstractServiceTest {

	@Autowired
	private CoreProcessFactory coreProcessFactory;
	
	@Test
	public void getProcessBeanNameTest(){
		Assert.assertEquals("AccountProvidersMpfb", MobileProcessUtility.getProcessBeanName(new GetAccountProvidersRequest()));
		Assert.assertEquals("OtpRequestMpfb", MobileProcessUtility.getProcessBeanName(new OtpRequest()));
		Assert.assertEquals("AccountListMpfb", MobileProcessUtility.getProcessBeanName(new GetAccountListRequest()));
		Assert.assertEquals("SetPinMpfb", MobileProcessUtility.getProcessBeanName(new SetPinRequest()));
		Assert.assertEquals("MakePaymentMpfb", MobileProcessUtility.getProcessBeanName(new PaymentRequest()));
		Assert.assertEquals("TxnDetailsMpfb", MobileProcessUtility.getProcessBeanName(new GetTxnDetailsRequest()));
		Assert.assertEquals("BalanceEnquiryMpfb", MobileProcessUtility.getProcessBeanName(new BalanceEnquiryRequest()));
		Assert.assertEquals("AddBankAccountMpfb", MobileProcessUtility.getProcessBeanName(new AddBankAccountRequest()));
		Assert.assertEquals("ChangePasswordMpfb", MobileProcessUtility.getProcessBeanName(new ChangePasswordRequest()));
		Assert.assertEquals("ForgetPasswordMpfb", MobileProcessUtility.getProcessBeanName(new ForgetPasswordRequest()));
		Assert.assertEquals("LoginMpfb", MobileProcessUtility.getProcessBeanName(new LoginRequest()));
		Assert.assertEquals("RegistrationMpfb", MobileProcessUtility.getProcessBeanName(new RegistrationRequest()));
		Assert.assertEquals("CheckUserMpfb", MobileProcessUtility.getProcessBeanName(new CheckUserRequest()));
		Assert.assertEquals("LogoutMpfb", MobileProcessUtility.getProcessBeanName(new LogoutRequest()));
		Assert.assertEquals("GetMerchantListMpfb", MobileProcessUtility.getProcessBeanName(new GetMerchantListRequest()));
		Assert.assertEquals("GetBeneficiaryListMpfb", MobileProcessUtility.getProcessBeanName(new GetBeneficiaryListRequest()));
		Assert.assertEquals("AddBeneficiaryMpfb", MobileProcessUtility.getProcessBeanName(new AddBeneficiaryRequest()));
		Assert.assertEquals("GetExistingAccsMpfb", MobileProcessUtility.getProcessBeanName(new GetExistingAccountsRequest()));
		Assert.assertEquals("GetKeyMpfb", MobileProcessUtility.getProcessBeanName(new GetKeyRequest()));
		Assert.assertEquals("GetBeneficiaryMpfb", MobileProcessUtility.getProcessBeanName(new GetBeneficiaryRequest()));
		Assert.assertEquals("GetKeyMpfb", MobileProcessUtility.getProcessBeanName(new GetKeyRequest()));
		Assert.assertEquals("GetMerchantMpfb", MobileProcessUtility.getProcessBeanName(new GetMerchantRequest()));
		Assert.assertEquals("GeneratePinMpfb", MobileProcessUtility.getProcessBeanName(new GeneratePinRequest()));
		Assert.assertEquals("CollectPaymentMpfb", MobileProcessUtility.getProcessBeanName(new CollectPaymentRequest()));
		Assert.assertEquals("CollectPayConfirmationMpfb", MobileProcessUtility.getProcessBeanName(new CollectPayConfirmRequest()));
		Assert.assertEquals("GetNotificationDetailsMpfb", MobileProcessUtility.getProcessBeanName(new GetNotificationDetailsRequest()));
		Assert.assertEquals("GetNotificationListMpfb", MobileProcessUtility.getProcessBeanName(new GetNotificationListRequest()));
	}
	
	@Test
	public void getAccountProvidersProcessValidatorTest(){
		GetAccountProvidersRequest request = new GetAccountProvidersRequest();
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		GetAccountProvidersResponse response = (GetAccountProvidersResponse)process.validateRequest();
		Assert.assertEquals(StatusCode.SUCCESS.getCode(), response.getStatusCode());
	}
	
	@Test
	public void getAccountProvidersProcessResponseTest(){
		GetAccountProvidersRequest request = new GetAccountProvidersRequest();
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		MessageResponse response = process.createResponseOnStatusCode(StatusCode.FIELD_VALIDATION_FAILED);
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getMessage(), response.getStatusMessage());
		Assert.assertEquals(GetAccountProvidersResponse.class.getSimpleName(), response.getClass().getSimpleName());
	}
	
	@Test
	public void getAddBankAccountProcessResponseTest(){
		AddBankAccountRequest request = new AddBankAccountRequest();
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		MessageResponse response = process.createResponseOnStatusCode(StatusCode.FIELD_VALIDATION_FAILED);
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getMessage(), response.getStatusMessage());
		Assert.assertEquals(AddBankAccountResponse.class.getSimpleName(), response.getClass().getSimpleName());
	}
	
	@Test
	public void getChangePasswordProcessResponseTest(){
		ChangePasswordRequest request = new ChangePasswordRequest();
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		MessageResponse response = process.createResponseOnStatusCode(StatusCode.FIELD_VALIDATION_FAILED);
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getMessage(), response.getStatusMessage());
		Assert.assertEquals(ChangePasswordResponse.class.getSimpleName(), response.getClass().getSimpleName());
	}
	
	@Test
	public void GetAccountListProcessResponseTest(){
		GetAccountListRequest request = new GetAccountListRequest();
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		MessageResponse response = process.createResponseOnStatusCode(StatusCode.FIELD_VALIDATION_FAILED);
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getMessage(), response.getStatusMessage());
		Assert.assertEquals(GetAccountListResponse.class.getSimpleName(), response.getClass().getSimpleName());
	}
	
	@Test
	public void getCheckUserProcessResponseTest(){
		CheckUserRequest request = new CheckUserRequest();
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		MessageResponse response = process.createResponseOnStatusCode(StatusCode.FIELD_VALIDATION_FAILED);
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getMessage(), response.getStatusMessage());
		Assert.assertEquals(CheckUserResponse.class.getSimpleName(), response.getClass().getSimpleName());
	}
	
	@Test
	public void getForgetPasswordProcessResponseTest(){
		ForgetPasswordRequest request = new ForgetPasswordRequest();
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		MessageResponse response = process.createResponseOnStatusCode(StatusCode.FIELD_VALIDATION_FAILED);
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getMessage(), response.getStatusMessage());
		Assert.assertEquals(ForgetPasswordResponse.class.getSimpleName(), response.getClass().getSimpleName());
	}
	
	@Test
	public void getBalanceEnquiryProcessResponseTest(){
		BalanceEnquiryRequest request = new BalanceEnquiryRequest();
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		MessageResponse response = process.createResponseOnStatusCode(StatusCode.FIELD_VALIDATION_FAILED);
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getMessage(), response.getStatusMessage());
		Assert.assertEquals(BalanceEnquiryResponse.class.getSimpleName(), response.getClass().getSimpleName());
	}
	
	@Test
	public void getLoginProcessResponseTest(){
		LoginRequest request = new LoginRequest();
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		MessageResponse response = process.createResponseOnStatusCode(StatusCode.FIELD_VALIDATION_FAILED);
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getMessage(), response.getStatusMessage());
		Assert.assertEquals(LoginResponse.class.getSimpleName(), response.getClass().getSimpleName());
	}
	
	@Test
	public void getLogoutProcessResponseTest(){
		LogoutRequest request = new LogoutRequest();
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		MessageResponse response = process.createResponseOnStatusCode(StatusCode.FIELD_VALIDATION_FAILED);
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getMessage(), response.getStatusMessage());
		Assert.assertEquals(LogoutResponse.class.getSimpleName(), response.getClass().getSimpleName());
	}
	
	@Test
	public void getPaymentProcessResponseTest(){
		PaymentRequest request = new PaymentRequest();
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		MessageResponse response = process.createResponseOnStatusCode(StatusCode.FIELD_VALIDATION_FAILED);
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getMessage(), response.getStatusMessage());
		Assert.assertEquals(PaymentResponse.class.getSimpleName(), response.getClass().getSimpleName());
	}
	
	@Test
	public void getOtpProcessResponseTest(){
		OtpRequest request = new OtpRequest();
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		MessageResponse response = process.createResponseOnStatusCode(StatusCode.FIELD_VALIDATION_FAILED);
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getMessage(), response.getStatusMessage());
		Assert.assertEquals(OtpResponse.class.getSimpleName(), response.getClass().getSimpleName());
	}
	
	@Test
	public void getRegistrationProcessResponseTest(){
		RegistrationRequest request = new RegistrationRequest();
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		MessageResponse response = process.createResponseOnStatusCode(StatusCode.FIELD_VALIDATION_FAILED);
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getMessage(), response.getStatusMessage());
		Assert.assertEquals(RegistrationResponse.class.getSimpleName(), response.getClass().getSimpleName());
	}
	
	@Test
	public void getSetPinProcessResponseTest(){
		SetPinRequest request = new SetPinRequest();
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		MessageResponse response = process.createResponseOnStatusCode(StatusCode.FIELD_VALIDATION_FAILED);
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getMessage(), response.getStatusMessage());
		Assert.assertEquals(SetPinResponse.class.getSimpleName(), response.getClass().getSimpleName());
	}
	
	@Test
	public void getExistingAccountsResponseTest(){
		GetExistingAccountsRequest request = new GetExistingAccountsRequest();
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		MessageResponse response = process.createResponseOnStatusCode(StatusCode.FIELD_VALIDATION_FAILED);
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getMessage(), response.getStatusMessage());
		Assert.assertEquals(GetExistingAccountsResponse.class.getSimpleName(), response.getClass().getSimpleName());
	}
	
	@Test
	public void getKeyResponseProcessTest(){
		GetKeyRequest request = new GetKeyRequest();
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		MessageResponse response = process.createResponseOnStatusCode(StatusCode.FIELD_VALIDATION_FAILED);
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getMessage(), response.getStatusMessage());
		Assert.assertEquals(GetKeyResponse.class.getSimpleName(), response.getClass().getSimpleName());
	}
	
	@Test
	public void getMerchantProcessTest(){
		GetMerchantRequest request = new GetMerchantRequest();
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		MessageResponse response = process.createResponseOnStatusCode(StatusCode.FIELD_VALIDATION_FAILED);
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getMessage(), response.getStatusMessage());
		Assert.assertEquals(GetMerchantResponse.class.getSimpleName(), response.getClass().getSimpleName());
	}
	
	@Test
	public void getTokenProcessTest(){
		GetTokenRequest request = new GetTokenRequest();
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		MessageResponse response = process.createResponseOnStatusCode(StatusCode.FIELD_VALIDATION_FAILED);
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getMessage(), response.getStatusMessage());
		Assert.assertEquals(GetTokenResponse.class.getSimpleName(), response.getClass().getSimpleName());
	}
	
	@Test
	public void getMerchantListProcessTest(){
		GetMerchantListRequest request = new GetMerchantListRequest();
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		MessageResponse response = process.createResponseOnStatusCode(StatusCode.FIELD_VALIDATION_FAILED);
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getMessage(), response.getStatusMessage());
		Assert.assertEquals(GetMerchantListResponse.class.getSimpleName(), response.getClass().getSimpleName());
	}
	
	@Test
	public void getTxnDetailsProcessTest(){
		GetTxnDetailsRequest request = new GetTxnDetailsRequest();
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		MessageResponse response = process.createResponseOnStatusCode(StatusCode.FIELD_VALIDATION_FAILED);
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getMessage(), response.getStatusMessage());
		Assert.assertEquals(GetTxnsSummaryResponse.class.getSimpleName(), response.getClass().getSimpleName());
	}
}