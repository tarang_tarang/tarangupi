package psp.mobile.process.test;

import junit.framework.Assert;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import psp.common.model.constants.ValidatorConstants;
import psp.constants.StatusCode;
import psp.db.test.AbstractServiceTest;
import psp.mobile.model.request.AddBeneficiaryRequest;
import psp.mobile.model.request.BalanceEnquiryRequest;
import psp.mobile.model.request.ChangePasswordRequest;
import psp.mobile.model.request.CheckUserRequest;
import psp.mobile.model.request.GetAccountListRequest;
import psp.mobile.model.request.GetTxnDetailsRequest;
import psp.mobile.model.request.LoginRequest;
import psp.mobile.model.request.OtpRequest;
import psp.mobile.model.request.PaymentRequest;
import psp.mobile.model.request.RegistrationRequest;
import psp.mobile.model.request.SetPinRequest;
import psp.mobile.model.response.AddBeneficiaryResponse;
import psp.mobile.model.response.BalanceEnquiryResponse;
import psp.mobile.model.response.ChangePasswordResponse;
import psp.mobile.model.response.CheckUserResponse;
import psp.mobile.model.response.GetTxnsSummaryResponse;
import psp.mobile.model.response.LoginResponse;
import psp.mobile.model.response.MessageResponse;
import psp.mobile.model.response.PaymentResponse;
import psp.mobile.model.response.RegistrationResponse;
import psp.mobile.process.factory.CoreProcessFactory;
import psp.mobile.process.factory.MobileCoreProcess;
import psp.mobile.process.util.MobileProcessUtility;
import psp.mobile.test.util.MobileValidatorTestUtility;

public class FactoryRequestValidatorTest extends AbstractServiceTest {

	@Autowired
	private CoreProcessFactory coreProcessFactory;
	
	//@Test
	public void getRegistrationFieldValidateSuccessTest(){
		
		String firstName = "Gowtham";
		String lastName = "Boj";
		String middleName = "Gambhir";
		String adharNumber = "123456789012";
		String mobileNumber = "1234567890";
		String email = "gowthamk29@gmail.com";
		String userName = "Gauti123";
		String password = "Gauti@123";
		RegistrationRequest request = MobileValidatorTestUtility
				.prepareRegistrationRequest(firstName, lastName, middleName,
						adharNumber, mobileNumber, email, userName, password);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		MessageResponse response = process.validateRequest();
		Assert.assertEquals(StatusCode.SUCCESS.getCode(), response.getStatusCode());
		
	}
	
	@Test
	public void getRegistrationFieldValidateEmptyTest(){
		
		String firstName = "";
		String lastName = "";
		String middleName = "";
		String adharNumber = "";
		String mobileNumber = "";
		String email = "";
		String userName = "";
		String password = "";
		RegistrationRequest request = MobileValidatorTestUtility
				.prepareRegistrationRequest(firstName, lastName, middleName,
						adharNumber, mobileNumber, email, userName, password);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		RegistrationResponse response = (RegistrationResponse) process.validateRequest();
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(ValidatorConstants.FIRST_NAME_MANDATORY_ERR_MSG, response.getFirstNameErrMsg());
		Assert.assertEquals(ValidatorConstants.LAST_NAME_MANDATORY_ERR_MSG, response.getLastNameErrMsg());
		Assert.assertEquals(ValidatorConstants.MOBILE_NUMBER_MANDATORY_ERR_MSG, response.getMobileNumberErrMsg());
		Assert.assertEquals(ValidatorConstants.AADHAR_NUMBER_MANDATORY_ERR_MSG, response.getAdharNumberErrMsg());
		Assert.assertEquals(ValidatorConstants.EMAIL_MANDATORY_ERR_MSG, response.getEmailErrMsg());
		Assert.assertEquals(ValidatorConstants.USER_NAME_MANDATORY_ERR_MSG, response.getUserNameErrMsg());
		Assert.assertEquals(ValidatorConstants.PASSWORD_MANDATORY_ERR_MSG, response.getPasswordErrMsg());
		
	}
	
	//@Test
	public void getRegistrationFieldValidateLengthTest(){
		
		String firstName = "Ga";
		String lastName = "Gowthamamghnmnmnmnmnmnwqqwqdfdsfsdfsdwwqqqwqw";
		String middleName = "Bo";
		String adharNumber = "213123";
		String mobileNumber = "1213123123123123";
		String email = "gowthamgowthamgowthamgowthamgowthamgowthamgowthamgowthamgowthsasdasdasam@gmail.com";
		String userName = "gawwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww";
		String password = "gow";
		RegistrationRequest request = MobileValidatorTestUtility
				.prepareRegistrationRequest(firstName, lastName, middleName,
						adharNumber, mobileNumber, email, userName, password);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		RegistrationResponse response = (RegistrationResponse) process.validateRequest();
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(ValidatorConstants.FIRST_NAME_LENGTH_ERR_MSG, response.getFirstNameErrMsg());
		Assert.assertEquals(ValidatorConstants.LAST_NAME_LENGTH_ERR_MSG, response.getLastNameErrMsg());
		Assert.assertEquals(ValidatorConstants.MIDDLE_NAME_LENGTH_ERR_MSG, response.getMiddleNameErrMsg());
		Assert.assertEquals(ValidatorConstants.MOBILE_NUMBER_LENGTH_ERR_MSG, response.getMobileNumberErrMsg());
		Assert.assertEquals(ValidatorConstants.EMAIL_LENGTH_ERR_MSG, response.getEmailErrMsg());
		Assert.assertEquals(ValidatorConstants.USER_NAME_LENGTH_ERR_MSG, response.getUserNameErrMsg());
		Assert.assertEquals(ValidatorConstants.PASSWORD_LENGTH_ERR_MSG, response.getPasswordErrMsg());
		
	}
	
	//@Test
	public void getRegistrationFieldValidateCharTest(){
		
		String firstName = "Go12";
		String lastName = "Boj32";
		String middleName = "Gambh23ir";
		String adharNumber = "1234wqe";
		String mobileNumber = "123er67890";
		String email = "gowthamk29mailcom";
		String userName = "gauti123?";
		String password = "Gauti@123 ";
		RegistrationRequest request = MobileValidatorTestUtility
				.prepareRegistrationRequest(firstName, lastName, middleName,
						adharNumber, mobileNumber, email, userName, password);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		RegistrationResponse response = (RegistrationResponse) process.validateRequest();
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(String.valueOf(response.getStatusCode()),"50101");
		Assert.assertEquals(ValidatorConstants.FIRST_NAME_EXPRESSION_ERR_MSG, response.getFirstNameErrMsg());
		Assert.assertEquals(ValidatorConstants.LAST_NAME_EXPRESSION_ERR_MSG, response.getLastNameErrMsg());
		Assert.assertEquals(ValidatorConstants.MIDDLE_NAME_EXPRESSION_ERR_MSG, response.getMiddleNameErrMsg());
		Assert.assertEquals(ValidatorConstants.MOBILE_NUMBER_EXPRESSION_ERR_MSG, response.getMobileNumberErrMsg());
		Assert.assertEquals(ValidatorConstants.EMAIL_EXPRESSION_ERR_MSG, response.getEmailErrMsg());
		Assert.assertEquals(ValidatorConstants.USER_NAME_EXPRESSION_ERR_MSG, response.getUserNameErrMsg());
		Assert.assertEquals(ValidatorConstants.PASSWORD_EXPRESSION_ERR_MSG, response.getPasswordErrMsg());
		
	}
	
	//@Test
	public void getLoginFieldValidateSuccessTest(){
		
		String userName = "Gauti123";
		String password = "Gauti@123";
		LoginRequest request = MobileValidatorTestUtility
				.prepareLoginRequest(userName, password);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		MessageResponse response = process.validateRequest();
		Assert.assertEquals(StatusCode.SUCCESS.getCode(), response.getStatusCode());
	}
	
	@Test
	public void getLoginFieldValidateEmptyTest(){
		
		String userName = "";
		String password = "";
		LoginRequest request = MobileValidatorTestUtility
				.prepareLoginRequest(userName, password);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		LoginResponse response = (LoginResponse) process.validateRequest();
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(ValidatorConstants.USER_NAME_MANDATORY_ERR_MSG, response.getUserNameErrMsg());
		Assert.assertEquals(ValidatorConstants.PASSWORD_MANDATORY_ERR_MSG, response.getPasswordErrMsg());
		

	}
	
	@Test
	public void getLoginFieldValidateLengthTest(){
		
		String userName = "gau";
		String password = "dsfsfsdsssssfssdfsdddddddddddddddddddddddddddddddd";
		LoginRequest request = MobileValidatorTestUtility
				.prepareLoginRequest(userName, password);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		LoginResponse response = (LoginResponse)process.validateRequest();
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(ValidatorConstants.USER_NAME_LENGTH_ERR_MSG, response.getUserNameErrMsg());
		Assert.assertEquals(ValidatorConstants.PASSWORD_LENGTH_ERR_MSG, response.getPasswordErrMsg());
	}
	
	//@Test
	public void getLoginFieldValidateCharTest(){
		
		String userName = "gGow@@@";
		String password = "dsfsfsdsssss";
		LoginRequest request = MobileValidatorTestUtility
				.prepareLoginRequest(userName, password);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		LoginResponse response = (LoginResponse)process.validateRequest();
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(ValidatorConstants.USER_NAME_EXPRESSION_ERR_MSG, response.getUserNameErrMsg());
		Assert.assertEquals(ValidatorConstants.PASSWORD_EXPRESSION_ERR_MSG, response.getPasswordErrMsg());
		
	}
	
	//@Test
	public void getChangePasswordFieldValidateSuccessTest(){
		
		String userName = "gauti123";
		String oldPassword = "Gauti@123";
		String newPassword = "Gauti@1234";
		ChangePasswordRequest request = MobileValidatorTestUtility
				.prepareChangePasswordRequest(userName, oldPassword, newPassword);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		MessageResponse response = process.validateRequest();
		Assert.assertEquals(StatusCode.SUCCESS.getCode(), response.getStatusCode());
	}
	
	@Test
	public void getChangePasswordFieldValidateEmptyTest(){
		
		String userName = "";
		String oldPassword = "";
		String newPassword = "";
		ChangePasswordRequest request = MobileValidatorTestUtility
				.prepareChangePasswordRequest(userName, oldPassword, newPassword);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		ChangePasswordResponse response = (ChangePasswordResponse) process.validateRequest();
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(ValidatorConstants.USER_NAME_MANDATORY_ERR_MSG, response.getUserNameErrMsg());
		Assert.assertEquals(ValidatorConstants.OLD_PASSWORD_MANDATORY_ERR_MSG, response.getOldPasswordErrMsg());
		Assert.assertEquals(ValidatorConstants.NEW_PASSWORD_MANDATORY_ERR_MSG, response.getNewPasswordErrMsg());
	}
	
	@Test
	public void getChangePasswordFieldValidateLengthTest(){
		
		String userName = "Gau";
		String oldPassword = "djnsdkjfnskdfjnskdjfskjdfnksjddddddddddddddddddddddddasddddf";
		String newPassword = "sdfsssssssssssssssssssssssssssssssssssssssssssdddddddasdasd";
		ChangePasswordRequest request = MobileValidatorTestUtility
				.prepareChangePasswordRequest(userName, oldPassword, newPassword);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		ChangePasswordResponse response = (ChangePasswordResponse) process.validateRequest();
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(ValidatorConstants.USER_NAME_LENGTH_ERR_MSG, response.getUserNameErrMsg());
		Assert.assertEquals(ValidatorConstants.OLD_PASSWORD_LENGTH_ERR_MSG, response.getOldPasswordErrMsg());
		Assert.assertEquals(ValidatorConstants.NEW_PASSWORD_LENGTH_ERR_MSG, response.getNewPasswordErrMsg());
		
	}
	
	//@Test
	public void getChangePasswordFieldValidateCharTest(){
		
		String userName = "Gau@@@//";
		String oldPassword = "djnsdkjfn";
		String newPassword = "GHGHGHbcvb";
		ChangePasswordRequest request = MobileValidatorTestUtility
				.prepareChangePasswordRequest(userName, oldPassword, newPassword);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		ChangePasswordResponse response = (ChangePasswordResponse) process.validateRequest();
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(ValidatorConstants.USER_NAME_EXPRESSION_ERR_MSG, response.getUserNameErrMsg());
		Assert.assertEquals(ValidatorConstants.OLD_PASSWORD_EXPRESSION_ERR_MSG, response.getOldPasswordErrMsg());
		Assert.assertEquals(ValidatorConstants.NEW_PASSWORD_EXPRESSION_ERR_MSG, response.getNewPasswordErrMsg());
	}
	
	@Test
	public void getSetPinValidateSuccessTest(){
		
		Boolean isChangePin = true;
		String oldPin = "1234";
		String newPin = "1234";
		SetPinRequest request = MobileValidatorTestUtility
				.prepareSetPinRequest(oldPin, newPin, isChangePin);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		MessageResponse response = process.validateRequest();
		Assert.assertEquals(StatusCode.SUCCESS.getCode(), response.getStatusCode());
	}
	
	
	
	@Test
	public void getSetPaymentValidateSuccessTest(){
		Double amount = 2000.0;
		String selfvirtualAddress = "zeeshan.khan@hdfc.com";
		String thirdPartyVirtualAddress = "zeeshan.khan@hdfc_wallet.hdfc";
		String mPin = "1234";
		PaymentRequest request = MobileValidatorTestUtility
				.preparePaymentRequest(amount, selfvirtualAddress,thirdPartyVirtualAddress, mPin);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		MessageResponse response = process.validateRequest();
		Assert.assertEquals(StatusCode.SUCCESS.getCode(), response.getStatusCode());

	}
	
	@Test
	public void getSetPaymentValidateEmptyTest(){
		Double amount = 0.0;
		String selfvirtualAddress = "";
		String thirdPartyVirtualAddress = "";
		String mPin = "";
		PaymentRequest request = MobileValidatorTestUtility
				.preparePaymentRequest(amount, selfvirtualAddress,thirdPartyVirtualAddress, mPin);		
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		PaymentResponse response = (PaymentResponse) process.validateRequest();
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(ValidatorConstants.AMOUNT_LENGTH_ERR_MSG, response.getAmountErrMsg());
		Assert.assertEquals(ValidatorConstants.SELF_VIRTUAL_ADDR_MANDATORY_ERR_MSG, response.getSelfVirtualAddErrMsg());
	}
	
	@Test
	public void getSetPaymentValidateLengthTest(){
		Double amount = 12123244444444444444444444444444444421233333333.12;
		String selfvirtualAddress = "asdasdadasdasdasdasdasdadasdasdasdasdasfdgdfddfasd@gmail";
		String thirdPartyVirtualAddress = "asdasdadasdasdasdasdasdadasdasdasdasdagdfgdfgsdasd@gmail";
		String mPin = "1231233333333333333333333333333";
		PaymentRequest request = MobileValidatorTestUtility
				.preparePaymentRequest(amount, selfvirtualAddress,thirdPartyVirtualAddress, mPin);		
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		PaymentResponse response = (PaymentResponse) process.validateRequest();
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(ValidatorConstants.AMOUNT_LENGTH_ERR_MSG, response.getAmountErrMsg());
		Assert.assertEquals(ValidatorConstants.SELF_VIRTUAL_ADDR_LENGTH_ERR_MSG, response.getSelfVirtualAddErrMsg());
		Assert.assertEquals(ValidatorConstants.THIRD_PARTY_VIRTUAL_ADDR_LENGTH_ERR_MSG, response.getThirdPartyVirtualAddrErrMsg());
	}
	
	@Test
	public void getSetPaymentValidateCharTest(){
		Double amount = 123.123123123;
		String selfvirtualAddress = "zeeshan.khan@hdfc-bank.hdfc.-";
		String thirdPartyVirtualAddress = "zeeshan.khan@hdfc-bank@hdfc";
		String mPin = "123mb1233";
		PaymentRequest request = MobileValidatorTestUtility
				.preparePaymentRequest(amount, selfvirtualAddress,thirdPartyVirtualAddress, mPin);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		PaymentResponse response = (PaymentResponse) process.validateRequest();
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(ValidatorConstants.AMOUNT_EXPRESSION_ERR_MSG, response.getAmountErrMsg());
		Assert.assertEquals(ValidatorConstants.SELF_VIRTUAL_ADDR_EXPRESSION_ERR_MSG, response.getSelfVirtualAddErrMsg());
		Assert.assertEquals(ValidatorConstants.THIRD_PARTY_VIRTUAL_ADDR_EXPRESSION_ERR_MSG, response.getThirdPartyVirtualAddrErrMsg());
	}
	
	@Test
	public void getOtpValidateSuccessTestForAccount(){
		
		String addrType = "ACCOUNT";
		String ifsc = "sbm4567";
		String accounttype = "SAVINGS";
		String accountNum = "123123123123";
		
		OtpRequest request = MobileValidatorTestUtility.OtpRequestAccount(addrType, ifsc, accounttype, accountNum);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		MessageResponse response = process.validateRequest();
		Assert.assertEquals(StatusCode.SUCCESS.getCode(), response.getStatusCode());
	}
	
	@Test
	public void getAccountListValidateSuccessTest(){
		
		String mmId = "1234567";
		String Otp = "1231237";
		GetAccountListRequest request = MobileValidatorTestUtility.validateAccListRequest(mmId, Otp);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		MessageResponse response = process.validateRequest();
		Assert.assertEquals(StatusCode.SUCCESS.getCode(), response.getStatusCode());
	}
	
	@Test
	public void getBalanceEnquiryValidateSuccessTest(){
		
		String mPin = "1234";
		BalanceEnquiryRequest request = MobileValidatorTestUtility.BalanceEnquiryRequest(mPin);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		MessageResponse response = process.validateRequest();
		Assert.assertEquals(StatusCode.SUCCESS.getCode(), response.getStatusCode());
	}

	@Test
	public void getBalanceEnquiryValidateEmptyTest(){
		
		String mPin = "";
		BalanceEnquiryRequest request = MobileValidatorTestUtility.BalanceEnquiryRequest(mPin);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		BalanceEnquiryResponse response = (BalanceEnquiryResponse) process.validateRequest();
		Assert.assertEquals(StatusCode.SUCCESS.getCode(), response.getStatusCode());
	}
	
	@Test
	public void getBalanceEnquiryValidateLengthTest(){
		
		String mPin = "12222222222222222222222222222222222222";
		BalanceEnquiryRequest request = MobileValidatorTestUtility.BalanceEnquiryRequest(mPin);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		BalanceEnquiryResponse response = (BalanceEnquiryResponse) process.validateRequest();
		Assert.assertEquals(StatusCode.SUCCESS.getCode(), response.getStatusCode());
	}
	
	@Test
	public void getBalanceEnquiryValidateCharTest(){
		
		String mPin = "12aa";
		BalanceEnquiryRequest request = MobileValidatorTestUtility.BalanceEnquiryRequest(mPin);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		BalanceEnquiryResponse response = (BalanceEnquiryResponse) process.validateRequest();
		Assert.assertEquals(StatusCode.SUCCESS.getCode(), response.getStatusCode());
	}
	
	@Test
	public void getTxnDetailsValidateSuccessTest(){
		
		String fromDate = "12-12-2014";
		String toDate = "12-12-2014";
		GetTxnDetailsRequest request = MobileValidatorTestUtility.validateGetTxnDetailsRequest(fromDate, toDate);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		MessageResponse response = process.validateRequest();
		Assert.assertEquals(StatusCode.SUCCESS.getCode(), response.getStatusCode());
	}

	@Test
	public void getTxnDetailsValidateEmptyTest(){
		
		String fromDate = "";
		String toDate = "";
		GetTxnDetailsRequest request = MobileValidatorTestUtility.validateGetTxnDetailsRequest(fromDate, toDate);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		MessageResponse response = process.validateRequest();
		Assert.assertEquals(StatusCode.SUCCESS.getCode(), response.getStatusCode());
	}
	
	@Test
	public void getTxnDetailsValidateLengthTest(){
		
		String fromDate = "12-12-121212";
		String toDate = "12-12-121212";
		GetTxnDetailsRequest request = MobileValidatorTestUtility.validateGetTxnDetailsRequest(fromDate, toDate);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		GetTxnsSummaryResponse response = (GetTxnsSummaryResponse) process.validateRequest();
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(ValidatorConstants.FROM_DATE_LENGTH_ERR_MSG, response.getFromDateErrMsg());
		Assert.assertEquals(ValidatorConstants.TO_DATE_LENGTH_ERR_MSG, response.getToDateErrMsg());
	}
	
	@Test
	public void getTxnDetailsValidateCharTest(){
		
		String fromDate = "1212/12/12";
		String toDate = "12/12/1212";
		GetTxnDetailsRequest request = MobileValidatorTestUtility.validateGetTxnDetailsRequest(fromDate, toDate);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		GetTxnsSummaryResponse response = (GetTxnsSummaryResponse) process.validateRequest();
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(ValidatorConstants.FROM_DATE_EXPRESSION_ERR_MSG, response.getFromDateErrMsg());
		Assert.assertEquals(ValidatorConstants.TO_DATE_EXPRESSION_ERR_MSG, response.getToDateErrMsg());
	}
	
	@Test
	public void getCheckUserValidateSuccessTest(){
		
		String mobileNumber = "1234567890";
		CheckUserRequest request = MobileValidatorTestUtility.validateCheckUserRequest(mobileNumber);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		MessageResponse response = process.validateRequest();
		Assert.assertEquals(StatusCode.SUCCESS.getCode(), response.getStatusCode());
		
	}
	
	@Test
	public void getCheckUserValidateEmptyTest(){
		
		String mobileNumber = "";
		CheckUserRequest request = MobileValidatorTestUtility.validateCheckUserRequest(mobileNumber);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		CheckUserResponse response = (CheckUserResponse) process.validateRequest();
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(ValidatorConstants.MOBILE_NUMBER_MANDATORY_ERR_MSG, response.getMobileNumErrMsg());
	}
	
	@Test
	public void getCheckUserValidateLengthTest(){
		
		String mobileNumber = "12345678901234567890";
		CheckUserRequest request = MobileValidatorTestUtility.validateCheckUserRequest(mobileNumber);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		CheckUserResponse response = (CheckUserResponse) process.validateRequest();
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(ValidatorConstants.MOBILE_NUMBER_LENGTH_ERR_MSG, response.getMobileNumErrMsg());
	}
	
	@Test
	public void getCheckUserValidateCharTest(){
		
		String mobileNumber = "123456ad90";
		CheckUserRequest request = MobileValidatorTestUtility.validateCheckUserRequest(mobileNumber);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		CheckUserResponse response = (CheckUserResponse) process.validateRequest();
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(ValidatorConstants.MOBILE_NUMBER_EXPRESSION_ERR_MSG, response.getMobileNumErrMsg());
	}
	
	@Test
	public void getAddBeneficiaryValidateSuccessTest(){
		
		String nickName = "Account 1";
		String beneficiaryNote = "sdfsdf0' _3-z0-";
		String txnNote = "sdfsdf3 WERR";
		Double amount = 20.00;
		AddBeneficiaryRequest request = MobileValidatorTestUtility
				.validateCheckAddBeneficiaryRequest(nickName, beneficiaryNote,txnNote, amount);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		AddBeneficiaryResponse response = (AddBeneficiaryResponse)process.validateRequest();
		Assert.assertEquals(StatusCode.SUCCESS.getCode(), response.getStatusCode());
	}
	
	@Test
	public void getAddBeneficiaryValidateEmptyTest(){
		
		String nickName = "";
		String beneficiaryNote = "";
		String txnNote = "";
		Double amount = 0.0;
		AddBeneficiaryRequest request = MobileValidatorTestUtility
				.validateCheckAddBeneficiaryRequest(nickName, beneficiaryNote,txnNote, amount);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		AddBeneficiaryResponse response = (AddBeneficiaryResponse)process.validateRequest();
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(ValidatorConstants.NICK_NAME_MANDATORY_ERR_MSG, response.getNickNameErrMsg());
	}
	
	@Test
	public void getAddBeneficiaryValidateLengthTest(){
		
		String nickName = "Ga";
		String beneficiaryNote = "ELe";
		String txnNote = "Pay";
		Double amount = 50000.55;
		AddBeneficiaryRequest request = MobileValidatorTestUtility
				.validateCheckAddBeneficiaryRequest(nickName, beneficiaryNote,txnNote, amount);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		AddBeneficiaryResponse response = (AddBeneficiaryResponse)process.validateRequest();
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(ValidatorConstants.NICK_NAME_LENGTH_ERR_MSG, response.getNickNameErrMsg());
		Assert.assertEquals(ValidatorConstants.BENEFICIARY_NOTE_LENGTH_ERR_MSG, response.getBeneficiaryNoteErrMsg());
	}
	
	@Test
	public void getAddBeneficiaryValidateCharTest(){
		
		String nickName = "Gowtham@123";
		String beneficiaryNote = "Its processed * successfully";
		String txnNote = "Its processed - successfully";
		Double amount = 20.223;
		AddBeneficiaryRequest request = MobileValidatorTestUtility
				.validateCheckAddBeneficiaryRequest(nickName, beneficiaryNote,txnNote, amount);
		MobileCoreProcess process = coreProcessFactory.getCoreProcess(MobileProcessUtility.getProcessBeanName(request) );
		process.setRequest(request);
		AddBeneficiaryResponse response = (AddBeneficiaryResponse)process.validateRequest();
		Assert.assertEquals(StatusCode.FIELD_VALIDATION_FAILED.getCode(), response.getStatusCode());
		Assert.assertEquals(ValidatorConstants.NICK_NAME_EXPRESSION_ERR_MSG, response.getNickNameErrMsg());
		Assert.assertEquals(ValidatorConstants.BENEFICIARY_NOTE_EXPRESSION_ERR_MSG, response.getBeneficiaryNoteErrMsg());
	}
	
}
