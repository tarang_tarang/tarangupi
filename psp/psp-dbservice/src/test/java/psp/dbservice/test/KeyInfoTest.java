package psp.dbservice.test;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import psp.db.test.AbstractServiceTest;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.KeyInfo;

public class KeyInfoTest  extends AbstractServiceTest{

	@Autowired
	private PspMgmtService pspMgmtService;
	
	/*@Test
	public void operateAccountProviderDetailsTest(){
		AccountProviderDetails acc = pspMgmtService.getAccountProvider(200l);
		List<AccountProviderDetails> updateDetails = new ArrayList<>();
		updateDetails.add(PspDaoDataPreparationUtility.prepareAccountProvider());
		updateDetails.add(acc);
		List<AccountProviderDetails> deleteList = new ArrayList<>();
		AccountProviderDetails deleteAcc = pspMgmtService.getAccountProvider(201l);
		deleteList.add(deleteAcc);
		pspMgmtService.operateAccountProviderDetails(updateDetails, deleteList);
		AccountProviderDetails details = pspMgmtService.getAccountProvider(201l);
		Assert.assertNull(details);		
	}*/
	
	@Test
	public void operateKeyInfoTest(){
		KeyInfo keyInfo = new KeyInfo();
		keyInfo.setXmlPayload("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4rIIEHkJ2TYgO/JUJQI/sxDgbDEAIuy9uTf4DItWeIMsG9AuilOj9R+dwAv8S6/9No/z0cwsw4UnsHQG1ALVIxFznLizMjaVJ7TJ+yTS9C9bYEFakRqH8b4jje7SC7rZ9/DtZGsaWaCaDTyuZ9dMHrgcmJjeklRKxl4YVmQJpzYLrK4zOpyY+lNPBqs+aiwJa53ZogcUGBhx/nIXfDDvVOtKzNb/08U7dZuXoiY0/McQ7xEiFcEtMpEJw5EB4o3RhE9j/IQOvc7l/BfD85+YQ5rJGk4HUb6GrQXHzfHvIOf53l1Yb0IX4v9q7HiAyOdggO+PVzXMSbrcFBrEjGZD7QIDAQAB");
		pspMgmtService.operateKeyInfo(keyInfo);
		keyInfo = pspMgmtService.getKeyInfo();
		Assert.assertNotNull(keyInfo);
		
	}
}
