package psp.dbservice.test;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import psp.db.test.AbstractServiceTest;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.TransactionDetails;

public class TransactionDetailsTest extends AbstractServiceTest {

	@Autowired
	private PspMgmtService pspMgmtService;
	
	@Test
	public void saveTransactionDetailsTest(){
		TransactionDetails txnDtls = getTransactionDetails("DebaK", "KaranA");
		Long id = pspMgmtService.saveTransactionDetails(txnDtls);		
		Assert.assertNotNull(id);		
	}
	
	@Test
	public void updateTranscationDetailsTest(){
		TransactionDetails txnDtls = getTransactionDetail("DebaK1", "KaranA1");
		Long id = pspMgmtService.saveTransactionDetails(txnDtls);		
		Assert.assertNotNull(id);		
		txnDtls = pspMgmtService.getTransactionDetailsByTxnId("12", "PAY");
		txnDtls.setStatus("Success");
		txnDtls.setUpiStatus("Success");
		pspMgmtService.updateTransactionDetails(txnDtls);
		txnDtls = pspMgmtService.getTransactionDetails(id);
		Assert.assertEquals("Success", txnDtls.getStatus());
		Assert.assertEquals("Success", txnDtls.getUpiStatus());
		Assert.assertEquals("1479", txnDtls.getAmount());		
	}
	
	private TransactionDetails getTransactionDetails(String fromAddr, String toAddr){
		TransactionDetails txnDtls = new TransactionDetails();
		txnDtls.setFromAddr(fromAddr+"@tarang");
		txnDtls.setToAddr(toAddr+"@tarang");
		txnDtls.setAmount("1479");
		txnDtls.setStatus("INITIATED");
		txnDtls.setTxnType("Payer");
		txnDtls.setInitiator(true);
		txnDtls.setTxnId("11");
		txnDtls.setTxnInitiationTime(new Date());
		txnDtls.setTxnMessage("hello");
		return txnDtls;
	}
	private TransactionDetails getTransactionDetail(String fromAddr, String toAddr){
		TransactionDetails txnDtls = new TransactionDetails();
		txnDtls.setFromAddr(fromAddr+"@tarang");
		txnDtls.setToAddr(toAddr+"@tarang");
		txnDtls.setAmount("1479");
		txnDtls.setStatus("INITIATED");
		txnDtls.setTxnType("PAY");
		txnDtls.setInitiator(true);
		txnDtls.setTxnId("12");
		txnDtls.setTxnInitiationTime(new Date());
		txnDtls.setTxnMessage("hello");
		return txnDtls;
	}
}
