package psp.dbservice.test;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import psp.constants.ServiceStatus;
import psp.db.test.AbstractServiceTest;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.GetAccountsUpiRequest;
import psp.dbservice.model.ReqAccountReference;
import psp.dbservice.model.SetCredentialUpiRequest;
import psp.dbservice.model.UpiRequest;

public class UpiRequestTest  extends AbstractServiceTest {

	@Autowired
	private PspMgmtService pspMgmtService;
	
	@Test
	public void getAccUpiRequestTest() {
		
		Set<ReqAccountReference> accounts = new HashSet<ReqAccountReference>();
		ReqAccountReference reference = new ReqAccountReference();
		reference.setIfsc("IFSC0101");
		reference.setMaskedNumber("3102102");
		accounts.add(reference);
		GetAccountsUpiRequest req = new GetAccountsUpiRequest();
		req.setTransactionId("12345");
		req.setRequestedTime(new Date());
		req.setAccounts(accounts);
		pspMgmtService.saveUpiRequest(req);
		GetAccountsUpiRequest req2 = (GetAccountsUpiRequest) pspMgmtService.getUpiRequestByTxnId("12345");
		Assert.assertNotNull(req2);
		Assert.assertEquals("12345", req2.getTransactionId());
		Iterator<ReqAccountReference> iterator = req2.getAccounts().iterator();
		ReqAccountReference ref = null;
		while (iterator.hasNext()) {
			ref =  (ReqAccountReference) iterator.next();
			Assert.assertEquals("IFSC0101", ref.getIfsc());
			Assert.assertEquals("3102102", ref.getMaskedNumber());
		}
	}
	
	@Test
	public void getAccUpiRequest01Test() {
		
		Set<ReqAccountReference> accounts = new HashSet<ReqAccountReference>();
		ReqAccountReference reference = new ReqAccountReference();
		reference.setIfsc("IFSC0101");
		reference.setMaskedNumber("3102102");
		accounts.add(reference);
		GetAccountsUpiRequest req = new GetAccountsUpiRequest();
		req.setTransactionId("12345");
		req.setRequestedTime(new Date());
		//req.setAccounts(accounts);
		pspMgmtService.saveUpiRequest(req);
		GetAccountsUpiRequest req2 = (GetAccountsUpiRequest) pspMgmtService.getUpiRequestByTxnId("12345");
		Assert.assertNotNull(req2);
		Assert.assertEquals("12345", req2.getTransactionId());
		/*Iterator<ReqAccountReference> iterator = req2.getAccounts().iterator();
		ReqAccountReference ref = null;
		while (iterator.hasNext()) {
			ref =  (ReqAccountReference) iterator.next();
			Assert.assertEquals("IFSC0101", ref.getIfsc());
			Assert.assertEquals("3102102", ref.getMaskedNumber());
		}*/
	}
	
	@Test
	public void saveSetCredsUpiRequestTest() {
		SetCredentialUpiRequest req = new SetCredentialUpiRequest();
		req.setTransactionId("1234");
		req.setRequestedTime(new Date());
		pspMgmtService.saveUpiRequest(req);
		UpiRequest upiRequest = pspMgmtService.getUpiRequestByTxnId("1234");
		Assert.assertNotNull(upiRequest);
		req = (SetCredentialUpiRequest) upiRequest;
		Assert.assertNotNull(req);
		Assert.assertEquals("1234", req.getTransactionId());
	}
	
	@Test
	public void updateUpiReqTest() {
		Set<ReqAccountReference> accounts = new HashSet<ReqAccountReference>();
		ReqAccountReference reference = new ReqAccountReference();
		reference.setIfsc("IFSC0101");
		reference.setMaskedNumber("3102102");
		accounts.add(reference);
		GetAccountsUpiRequest req = new GetAccountsUpiRequest();
		req.setTransactionId("12345");
		req.setRequestedTime(new Date());
		req.setAccounts(accounts);
		pspMgmtService.saveUpiRequest(req);
		GetAccountsUpiRequest req2 = (GetAccountsUpiRequest) pspMgmtService.getUpiRequestByTxnId("12345");
		Assert.assertNotNull(req2);
		Assert.assertEquals("12345", req2.getTransactionId());
		req2.setStatus(ServiceStatus.RECEIVED.name());
		pspMgmtService.updateUpiRequest(req2);
		req2 = (GetAccountsUpiRequest) pspMgmtService.getUpiRequestByTxnId("12345");
		Assert.assertNotNull(req2);
		Assert.assertEquals(ServiceStatus.RECEIVED.name(), req2.getStatus());
	}
	
	@Test
	public void deleteUpiReqTest() {
		Set<ReqAccountReference> accounts = new HashSet<ReqAccountReference>();
		ReqAccountReference reference = new ReqAccountReference();
		reference.setIfsc("IFSC0101");
		reference.setMaskedNumber("3102102");
		accounts.add(reference);
		GetAccountsUpiRequest req = new GetAccountsUpiRequest();
		req.setTransactionId("123456");
		req.setRequestedTime(new Date());
		req.setAccounts(accounts);
		pspMgmtService.saveUpiRequest(req);
		GetAccountsUpiRequest req2 = (GetAccountsUpiRequest) pspMgmtService.getUpiRequestByTxnId("123456");
		Assert.assertNotNull(req2);
		pspMgmtService.deleteUpiRequest(req2);
		Assert.assertNull(pspMgmtService.getUpiRequestByTxnId("123456"));
	}
	
}
