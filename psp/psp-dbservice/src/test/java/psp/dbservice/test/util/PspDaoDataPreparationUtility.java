package psp.dbservice.test.util;

import java.util.Date;

import psp.dbservice.model.AccountDetails;
import psp.dbservice.model.AccountProviderDetails;
import psp.dbservice.model.AccountReference;
import psp.dbservice.model.BeneficiaryAccountDetails;
import psp.dbservice.model.BeneficiaryDetails;
import psp.dbservice.model.DeviceDetails;
import psp.dbservice.model.LoginDetails;
import psp.dbservice.model.MobileAuditTrail;
import psp.dbservice.model.NotificationDetails;
import psp.dbservice.model.OtpUpiRequest;
import psp.dbservice.model.PspProvider;
import psp.dbservice.model.TransactionDetails;
import psp.dbservice.model.UpiMessagePacket;
import psp.dbservice.model.UserDetails;

public class PspDaoDataPreparationUtility {

	private PspDaoDataPreparationUtility() {
	}
	
	public static UserDetails prepareUserDetails(String userName, String aadhaar, String mob, String status) {
		UserDetails userDetails = new UserDetails();
		userDetails.setUserName(userName);
		userDetails.setAadhaarNumber(aadhaar);
		userDetails.setCreatedDate(new Date());
		userDetails.setEmail("tarang@tarangtech.com");
		userDetails.setFirstName("tarang");
		userDetails.setLastName("tarang");
		userDetails.setMiddleName("tarang");
		userDetails.setMobileNumber(mob);
		userDetails.setPassword("password");
		userDetails.setStatus(status);
		userDetails.setRnsMpaId("DCA800025D76B7B2B7A46414DF23111AE55F77F3585568AFF732CDF2D3B68A8A");
		return userDetails;
	}
	
	public static PspProvider preparePspProvider() {
		PspProvider provider = new PspProvider();
		provider.setCodes("1234");
		provider.setActive("1");
		provider.setLastModifedTs(new Date());
		provider.setName("psp provider");
		provider.setSpocName("psp");
		provider.setSpocEmail("psp@gmail.com");
		provider.setSpocPhone("9908068763");
		provider.setUrl("www.google.com");
		return provider;
		
	}
	
	public static MobileAuditTrail prepareMobileAuditTrail() {
		MobileAuditTrail mobileAudit = new MobileAuditTrail();
		mobileAudit.setApp("android");
		mobileAudit.setCapability("8gb");
		mobileAudit.setGeoCode("123456");
		mobileAudit.setIp("127.56.55.24");
		mobileAudit.setLocation("Ooty");
		mobileAudit.setMobile("9908068763");
		mobileAudit.setOs("android");
		mobileAudit.setRequestTime(new Date());
		mobileAudit.setServiceName("auditrails");
		mobileAudit.setTerminalId("12324235");
		mobileAudit.setType("Mobile");
		mobileAudit.setUserName("psp");
		return mobileAudit;
		
	}
	
	public static DeviceDetails prepareDeviceDetails(UserDetails user) {
		DeviceDetails details = new DeviceDetails();
		details.setApp("Tarang UPI Solution");
		details.setCapability("DE 61");
		details.setGeoCode("658");
		details.setIp("172.30.65.190");
		details.setLocation("WhiteField");
		details.setMobile(user.getMobileNumber());
		details.setOs("Android");
		details.setTerminalId("1234");
		details.setType("mobile");
		details.setUserDetails(user);
		return details;
	}
	
	public static AccountDetails prepareAcDetails(UserDetails user) {
		AccountDetails acDetails = new AccountDetails();
		acDetails.setAccountNumber("123465678");
		acDetails.setAccountType("SAVINGS");
		acDetails.setAddressType("sample@ta");
		acDetails.setIfsc("AXIS01321");
		acDetails.setMmid("123465");
		acDetails.setMobileNumber("990809525");
		acDetails.setUserDetails(user);
		acDetails.setVirtualAddress("sample@tarang");
		acDetails.setIsPinSet(false);
		return acDetails;
	}
	
	public static AccountReference prepareAccReference(AccountDetails acDetails) {
		AccountReference acReference = new AccountReference();
		acReference.setAeba("N");
		acReference.setIfsc("AXIS0132");
		acReference.setMaskedNumber("XXXXXXXXXX2365");
		acReference.setMmid("123456");
		acReference.setName("tarang");
		acReference.setReferenceNumber("123658");
		acReference.setAccountDetails(acDetails);
		return acReference;
	}
	
	public static AccountProviderDetails prepareAccountProvider(){
		AccountProviderDetails aProvider = new AccountProviderDetails();
		aProvider.setActive("Y");
		aProvider.setBankName("HDFC");
		aProvider.setIin("90135");
		aProvider.setLastModifedTs(new Date());
		aProvider.setProds("IMPS");
		aProvider.setSpocEmail("aaa@hdfc.com");
		aProvider.setSpocName("somebody");
		aProvider.setSpocPhone("9908520202");
		aProvider.setUrl("hdfc.com");
		return aProvider;
	}
	
	public static TransactionDetails prepTxnDetails(String payerVirtualAddr){
		TransactionDetails tDetails = new TransactionDetails();
		tDetails.setAmount("10");
		tDetails.setFromAddr(payerVirtualAddr);
		tDetails.setStatus("Initiated");
		tDetails.setToAddr("tarang2@tarangtech.com");
		tDetails.setTxnInitiationTime(new Date());
		tDetails.setTxnMessage("Initiated");
		return tDetails;
	}	
	
	public static LoginDetails prepareLoginDetails(String userName) {
        LoginDetails loginDetails = new LoginDetails();
        loginDetails.setUserName(userName);             
        loginDetails.setLoginTime(new Date(System.currentTimeMillis()));
        loginDetails.setExpiryTime(new Date(System.currentTimeMillis()+ 10 * 60 * 1000));       
        return loginDetails;
	}
	
	public static OtpUpiRequest prepareOtpDetails(String mobileNum, String mmid){
		OtpUpiRequest otp=new OtpUpiRequest();
		otp.setMmid(mmid);
		otp.setMobNum(mobileNum);	
		otp.setUidNum("12364587958");
		otp.setIin("901345");
		otp.setAcNum("2678892001828");
		otp.setIfsc("HDFC0288100");
		otp.setAcType("SAVINGS");		
		return otp;
	}
	
	public static BeneficiaryDetails prepareBeneficiaryDetails(){
		BeneficiaryDetails bnf = new BeneficiaryDetails();
		BeneficiaryAccountDetails bnfAcDtls = new BeneficiaryAccountDetails();
		bnfAcDtls.setAeba("Y");
		bnfAcDtls.setIfsc("HDFC1234");
		bnfAcDtls.setMaskedNumber("****12345");
		bnfAcDtls.setMmid("1470369");
		bnfAcDtls.setName("DebaKA");
		bnfAcDtls.setReferenceNumber("1");			
		bnf.setAddressType("Mobile");
		bnf.setAddress("9741500833");
		bnf.setDescription("Hello");
		bnf.setNickName("Debu");
		bnf.setUserName("DebaK2");
		bnf.setAccountDetails(bnfAcDtls);
		return bnf;
	}
	
	public static BeneficiaryDetails prepareBeneficiaryDetail(){
		BeneficiaryDetails bnf = new BeneficiaryDetails();
		BeneficiaryAccountDetails bnfAcDtls = new BeneficiaryAccountDetails();
		bnfAcDtls.setAeba("N");
		bnfAcDtls.setIfsc("HDFC4321");
		bnfAcDtls.setMaskedNumber("****12345");
		bnfAcDtls.setMmid("24444");
		bnfAcDtls.setName("Deba");
		bnfAcDtls.setReferenceNumber("1");			
		bnf.setAddressType("Mobile");
		bnf.setAddress("9751500833");
		bnf.setDescription("How are u");
		bnf.setNickName("Deba");
		bnf.setUserName("DebaK3");
		bnf.setAccountDetails(bnfAcDtls);
		return bnf;
	}
	
	public static BeneficiaryDetails prepareBenficiaryDetail(){
		BeneficiaryDetails bnf = new BeneficiaryDetails();
		BeneficiaryAccountDetails bnfAcDtls = new BeneficiaryAccountDetails();
		bnfAcDtls.setAeba("Y");
		bnfAcDtls.setIfsc("HDFC1234");
		bnfAcDtls.setMaskedNumber("****12345");
		bnfAcDtls.setMmid("1470369");
		bnfAcDtls.setName("DebaKA");
		bnfAcDtls.setReferenceNumber("1");			
		bnf.setAddressType("Mobile");
		bnf.setAddress("9741500833");
		bnf.setDescription("Hello");
		bnf.setNickName("Debu123");
		bnf.setUserName("DebaK3");
		bnf.setAccountDetails(bnfAcDtls);
		return bnf;
	}
	
	public static NotificationDetails prepareNotificationDetails(UpiMessagePacket upiMessagePacket) {
		NotificationDetails notificationDetails = new NotificationDetails();
		notificationDetails.setGeneratedTime(new Date());
		notificationDetails.setNotificationId("Collect01");
		notificationDetails.setNotificationMessage("Restuarant Bill");
		notificationDetails.setNotificationtType("COLLECT");
		notificationDetails.setRegistrationId("DASASASASASASA");
		notificationDetails.setStatus("INITIATED");
		notificationDetails.setUserName("tarang1");
		notificationDetails.setUpiMessagePacket(upiMessagePacket);
		return notificationDetails;
	}
}