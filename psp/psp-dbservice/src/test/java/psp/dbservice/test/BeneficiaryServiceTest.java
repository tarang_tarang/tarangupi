/**
 * 
 */
package psp.dbservice.test;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.upi.system_1_2.AddressType;

import psp.db.test.AbstractServiceTest;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.BeneficiaryAccountDetails;
import psp.dbservice.model.BeneficiaryDetails;
/**
 * @author prasadj
 *
 */
public class BeneficiaryServiceTest extends AbstractServiceTest {

	@Autowired
	private PspMgmtService pspMgmtService;
	
	@Test
	public void saveBeneficiaryDetailsTest(){
		BeneficiaryDetails bnf = getBeneficiaryDetailsByMobile("selfUserx", "Sachinx");
		Long id = pspMgmtService.saveBeneficiaryDetails(bnf);
		Assert.assertNotNull(id);
		Assert.assertNotNull(bnf.getAccountDetails().getId());
	}
	
	
	@Test(expected = Exception.class)
	public void saveBeneficiaryDetails02Test(){
		BeneficiaryDetails bnf = getBeneficiaryDetailsByMobile("selfUser", "Sachin");
		Long id = pspMgmtService.saveBeneficiaryDetails(bnf);
		Assert.assertNotNull(id);
		Assert.assertNotNull(bnf.getAccountDetails().getId());
		
		bnf = getBeneficiaryDetailsByMobile("selfUser", "Sachin");
		pspMgmtService.saveBeneficiaryDetails(bnf);
	}
	
	@Test
	public void getBeneficiaryDetailsTest(){
		BeneficiaryDetails bnf = getBeneficiaryDetailsByAadhaar("selfUser", "Dhoni");
		Long id = pspMgmtService.saveBeneficiaryDetails(bnf);
		
		BeneficiaryDetails rstBnf = pspMgmtService.getBeneficiaryDetails(id);
		Assert.assertNotNull(rstBnf);	
		Assert.assertNotNull(rstBnf.getId());	
		Assert.assertEquals(AddressType.AADHAAR.name(), bnf.getAddressType());
		Assert.assertEquals("9751500833", rstBnf.getAddress());
		Assert.assertEquals("school team", bnf.getDescription());
		Assert.assertEquals("Dhoni", bnf.getNickName());
		Assert.assertEquals("selfUser", bnf.getUserName());
		
		BeneficiaryAccountDetails bad = rstBnf.getAccountDetails();
		Assert.assertNotNull(bad);
		Assert.assertNotNull(bad.getId());
		Assert.assertEquals("Y", bad.getAeba());
		Assert.assertEquals("ICICI0012", bad.getIfsc());
		Assert.assertEquals("****12333", bad.getMaskedNumber());
		Assert.assertEquals("2444401", bad.getMmid());
		Assert.assertEquals("M S Dhoni", bad.getName());
		Assert.assertEquals("1234567891", bad.getReferenceNumber());		
	}
	
	@Test
	public void getBeneficiaryDetailsByNickNameTest(){
		BeneficiaryDetails bnf = getBeneficiaryDetailsByAadhaar("selfUser0", "Dhoni0");
		pspMgmtService.saveBeneficiaryDetails(bnf);

		BeneficiaryDetails rstBnf = pspMgmtService.getBeneficiaryDetails("selfUser0", "Dhoni0");
		Assert.assertNotNull(rstBnf);	
		Assert.assertNotNull(rstBnf.getId());	
		Assert.assertEquals(AddressType.AADHAAR.name(), bnf.getAddressType());
		Assert.assertEquals("9751500833", rstBnf.getAddress());
		Assert.assertEquals("school team", bnf.getDescription());
		Assert.assertEquals("Dhoni0", bnf.getNickName());
		Assert.assertEquals("selfUser0", bnf.getUserName());
		
		BeneficiaryAccountDetails bad = rstBnf.getAccountDetails();
		Assert.assertNotNull(bad);
		Assert.assertNotNull(bad.getId());
		Assert.assertEquals("Y", bad.getAeba());
		Assert.assertEquals("ICICI0012", bad.getIfsc());
		Assert.assertEquals("****12333", bad.getMaskedNumber());
		Assert.assertEquals("2444401", bad.getMmid());
		Assert.assertEquals("M S Dhoni", bad.getName());
		Assert.assertEquals("1234567891", bad.getReferenceNumber());		
	}
	
	
	@Test
	public void getBeneficiaryDetailsByNickName02Test(){
		BeneficiaryDetails bnf = getBeneficiaryDetailsByAadhaar("selfUser1", "Dhoni");
		pspMgmtService.saveBeneficiaryDetails(bnf);
		BeneficiaryDetails rstBnf = pspMgmtService.getBeneficiaryDetails("selfUserx", "Dhoni");
		Assert.assertNull(rstBnf);	
	}
	
	@Test
	public void getBeneficiaryDetailsByNickName03Test(){
		BeneficiaryDetails bnf = getBeneficiaryDetailsByAadhaar("selfUser1", "Dhoni2");
		pspMgmtService.saveBeneficiaryDetails(bnf);
		BeneficiaryDetails rstBnf = pspMgmtService.getBeneficiaryDetails("selfUser", "Dhoni2");
		Assert.assertNull(rstBnf);	
	}
	
	@Test
	public void getBeneficiaryDetailsByNickName04Test(){
		BeneficiaryDetails bnf = getBeneficiaryDetailsByAadhaar("selfUser3", "Dhoni3");
		pspMgmtService.saveBeneficiaryDetails(bnf);
		BeneficiaryDetails rstBnf = pspMgmtService.getBeneficiaryDetails("selfUser3", "Dhoni4");
		Assert.assertNull(rstBnf);	
	}
	
	private BeneficiaryDetails getBeneficiaryDetailsByAadhaar(String userName, String nickName){
		BeneficiaryDetails bnf = new BeneficiaryDetails();
		BeneficiaryAccountDetails bnfAcDtls = new BeneficiaryAccountDetails();
		bnfAcDtls.setAeba("Y");
		bnfAcDtls.setIfsc("ICICI0012");
		bnfAcDtls.setMaskedNumber("****12333");
		bnfAcDtls.setMmid("2444401");
		bnfAcDtls.setName("M S Dhoni");
		bnfAcDtls.setReferenceNumber("1234567891");	
		
		bnf.setAddress("9751500833");
		bnf.setDescription("school team");
		bnf.setNickName(nickName);
		bnf.setUserName(userName);
		bnf.setAccountDetails(bnfAcDtls);
		bnf.setAddressType(AddressType.AADHAAR.name());
		return bnf;
	}
	
	private BeneficiaryDetails getBeneficiaryDetailsByMobile(String userName, String nickName){
		BeneficiaryDetails bnf = new BeneficiaryDetails();
		BeneficiaryAccountDetails bnfAcDtls = new BeneficiaryAccountDetails();
		bnfAcDtls.setAeba("N");
		bnfAcDtls.setIfsc("HDFC4321");
		bnfAcDtls.setMaskedNumber("****12345");
		bnfAcDtls.setMmid("24444");
		bnfAcDtls.setName("Sachin Tendulkar");
		bnfAcDtls.setReferenceNumber("1234567890");	
		bnf.setAccountDetails(bnfAcDtls);
		
		bnf.setAddress("9751500833");
		bnf.setDescription("school friend");
		bnf.setNickName(nickName);
		bnf.setUserName(userName);
		bnf.setAddressType(AddressType.MOBILE.name());
		return bnf;
	}
	
	@Test
	public void getBeneficiaryDetailsByNickName05Test(){
		BeneficiaryDetails bnf = getBeneficiaryDetailsByAadhaar("selfUser6", "Dhoni6");
		pspMgmtService.saveBeneficiaryDetails(bnf);
		BeneficiaryDetails rstBnf = pspMgmtService.getBeneficiaryDetailsByNickName("Dhoni6");
		Assert.assertNotNull(rstBnf);	
	}
}