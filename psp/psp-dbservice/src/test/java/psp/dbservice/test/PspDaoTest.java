package psp.dbservice.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import psp.common.model.AccountSummary;
import psp.common.model.BankSummary;
import psp.db.test.AbstractServiceTest;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.AccountDetails;
import psp.dbservice.model.AccountProviderDetails;
import psp.dbservice.model.AccountReference;
import psp.dbservice.model.DeviceDetails;
import psp.dbservice.model.OtpUpiRequest;
import psp.dbservice.model.TransactionDetails;
import psp.dbservice.model.UpiMessagePacket;
import psp.dbservice.model.UserDetails;
import psp.dbservice.test.util.PspDaoDataPreparationUtility;

public class PspDaoTest extends AbstractServiceTest {
	
	@Autowired
	private PspMgmtService pspMgmtService;
	
	private String userName = "tarang";
	
	private String password = "password";
	
	private String aadhaarNumber = "12364587958";
	
	private String mobileNumber = "123654789";
	
	private String status = "Registered";
	
	private String mmid = "ABCD123";
	
	private String iin = "901345";
	
	private String acNum = "2678892001828";
	
	private String ifsc = "HDFC0288100";
	
	private String acType = "SAVINGS";	
	
	private String mobileNum = "9741500833";
	
	private String mmid2 = "PQRS456";
	
	private String terminalId = "1234";
		
	@Test
	public void saveUserDetailsTest() {
		Assert.assertNotNull(pspMgmtService.saveUserDetails(PspDaoDataPreparationUtility.prepareUserDetails(userName, aadhaarNumber, mobileNumber, status)));
	}
	
	@Test
	public void saveDeviceDetailsTest() {
		Long id = pspMgmtService.saveUserDetails(PspDaoDataPreparationUtility.prepareUserDetails(userName, aadhaarNumber, mobileNumber, status));
		Assert.assertNotNull(id);
		UserDetails usDetails = pspMgmtService.getUserDetails(id);
		Assert.assertNotNull(usDetails);
		Assert.assertNotNull(pspMgmtService.saveDeviceDetails(PspDaoDataPreparationUtility.prepareDeviceDetails(usDetails)));
	}
	
	@Test
	public void savePspProvider() {
		Assert.assertNotNull(pspMgmtService.savePspProvider(PspDaoDataPreparationUtility.preparePspProvider()));
	}
	
	/*@Test
	public void saveMobileAuditTrail() {
		Assert.assertNotNull(pspMgmtService.saveMobileAuditTrail(PspDaoDataPreparationUtility.prepareMobileAuditTrail()));
	}*/
	
	@Test
	public void saveAccountDetailsTest() {
		Long id = pspMgmtService.saveUserDetails(PspDaoDataPreparationUtility.prepareUserDetails(userName, aadhaarNumber, mobileNumber, status));
		Assert.assertNotNull(id);
		UserDetails usDetails = pspMgmtService.getUserDetails(id);
		Assert.assertNotNull(usDetails);
		Assert.assertNotNull(pspMgmtService.saveAccountDetails(PspDaoDataPreparationUtility.prepareAcDetails(usDetails)));
	}
	
	@Test
	public void getAccountSummaryByUserDetailsIdTest() {
		Long id = pspMgmtService.saveUserDetails(PspDaoDataPreparationUtility.prepareUserDetails(userName, aadhaarNumber, mobileNumber, status));
		Assert.assertNotNull(id);
		UserDetails usDetails = pspMgmtService.getUserDetails(id);
		Assert.assertNotNull(usDetails);
		Long accId = pspMgmtService.saveAccountDetails(PspDaoDataPreparationUtility.prepareAcDetails(usDetails));
		Assert.assertNotNull(accId);		
		Assert.assertNotNull(pspMgmtService.saveAccountReference(PspDaoDataPreparationUtility.prepareAccReference(pspMgmtService.getAccountDetails(accId))));
		AccountSummary accountSummary = pspMgmtService.getAccountSummaryByUserDetailsId(id);
		Assert.assertNotNull(accountSummary);
		Assert.assertEquals("sample@tarang", accountSummary.getVirtualAddress());
		Assert.assertEquals("N", accountSummary.getAeba());
		Assert.assertEquals("AXIS0132", accountSummary.getIfsc());
		Assert.assertEquals(false, accountSummary.getIsPinSet());
		Assert.assertEquals("XXXXXXXXXX2365", accountSummary.getMaskedAccnumber());
		Assert.assertEquals("tarang", accountSummary.getName());	
	}
	
	@Test
	public void saveAccountReferenceTest() {
		Long id = pspMgmtService.saveUserDetails(PspDaoDataPreparationUtility.prepareUserDetails(userName, aadhaarNumber, mobileNumber, status));
		Assert.assertNotNull(id);
		UserDetails usDetails = pspMgmtService.getUserDetails(id);
		Assert.assertNotNull(usDetails);
		Long accId = pspMgmtService.saveAccountDetails(PspDaoDataPreparationUtility.prepareAcDetails(usDetails));
		Assert.assertNotNull(accId);		
		Assert.assertNotNull(pspMgmtService.saveAccountReference(PspDaoDataPreparationUtility.prepareAccReference(pspMgmtService.getAccountDetails(accId))));
	}
	
	@Test
	public void updateUserDetailsTest() {
		Long id = pspMgmtService.saveUserDetails(PspDaoDataPreparationUtility.prepareUserDetails(userName, aadhaarNumber, mobileNumber, status));
		Assert.assertNotNull(id);
		UserDetails usDetails = pspMgmtService.getUserDetails(id);
		usDetails.setMobileNumber("12365478");
		usDetails.setAadhaarNumber("123658974122");
		pspMgmtService.updateUserDetails(usDetails);
		usDetails = pspMgmtService.getUserDetails(id);
		Assert.assertNotNull(usDetails);
		Assert.assertEquals("12365478", usDetails.getMobileNumber());
	}
	
	@Test
	public void updateDeviceDetailsTest() {
		Long id = pspMgmtService.saveUserDetails(PspDaoDataPreparationUtility.prepareUserDetails(userName, aadhaarNumber, mobileNumber, status));
		Assert.assertNotNull(id);
		UserDetails usDetails = pspMgmtService.getUserDetails(id);
		Assert.assertNotNull(usDetails);
		Long deviceId = pspMgmtService.saveDeviceDetails(PspDaoDataPreparationUtility.prepareDeviceDetails(usDetails));
		Assert.assertNotNull(deviceId);
		DeviceDetails details = pspMgmtService.getDeviceDetails(deviceId);
		details.setMobile("99080265");
		details.setGeoCode("23654");
		pspMgmtService.updateDeviceDetails(details);
		details = pspMgmtService.getDeviceDetails(deviceId);
		Assert.assertEquals("99080265", details.getMobile());
		Assert.assertEquals("23654", details.getGeoCode());
	}
	
	@Test
	public void updateAccountDetailsTest() {
		Long id = pspMgmtService.saveUserDetails(PspDaoDataPreparationUtility.prepareUserDetails(userName, aadhaarNumber, mobileNumber, status));
		Assert.assertNotNull(id);
		UserDetails usDetails = pspMgmtService.getUserDetails(id);
		Assert.assertNotNull(usDetails);
		Long accId = pspMgmtService.saveAccountDetails(PspDaoDataPreparationUtility.prepareAcDetails(usDetails));
		Assert.assertNotNull(accId);
		AccountDetails acDetails = pspMgmtService.getAccountDetails(accId);
		acDetails.setAccountNumber("12345");
		acDetails.setAccountType("Current");
		pspMgmtService.updateAccountDetails(acDetails);
		acDetails = pspMgmtService.getAccountDetails(accId);
		Assert.assertEquals("12345", acDetails.getAccountNumber());
		Assert.assertEquals("Current", acDetails.getAccountType());
	}
	
	@Test
	public void updateAccountReferenceTest() {
		Long id = pspMgmtService.saveUserDetails(PspDaoDataPreparationUtility.prepareUserDetails(userName, aadhaarNumber, mobileNumber, status));
		Assert.assertNotNull(id);
		UserDetails usDetails = pspMgmtService.getUserDetails(id);
		Assert.assertNotNull(usDetails);
		Long accId = pspMgmtService.saveAccountDetails(PspDaoDataPreparationUtility.prepareAcDetails(usDetails));
		Assert.assertNotNull(accId);
		Long acRefId = pspMgmtService.saveAccountReference(PspDaoDataPreparationUtility.prepareAccReference(pspMgmtService.getAccountDetails(accId)));
		Assert.assertNotNull(acRefId);
		AccountReference aReference = pspMgmtService.getAccountReference(acRefId);
		aReference.setMmid("12365875");
		aReference.setAeba("N");
		pspMgmtService.updateAccountReference(aReference);
		aReference = pspMgmtService.getAccountReference(acRefId);
		Assert.assertEquals("12365875", aReference.getMmid());
		Assert.assertEquals("N", aReference.getAeba());
	}
	
	@Test
	public void saveAccProviderTest() {
		Assert.assertNotNull(pspMgmtService.saveAccountProvider(PspDaoDataPreparationUtility.prepareAccountProvider()));
	}
	
	@Test
	public void saveTxnDetailsTest() {
		Assert.assertNotNull(pspMgmtService.saveTransactionDetails(PspDaoDataPreparationUtility.prepTxnDetails("tarang25@tarangtech.com")));
	}
	
	@Test
	public void updateAccProviderTest(){
		Long id = pspMgmtService.saveAccountProvider(PspDaoDataPreparationUtility.prepareAccountProvider());
		Assert.assertNotNull(id);
		AccountProviderDetails aProvider = pspMgmtService.getAccountProvider(id);
		aProvider.setActive("N");
		pspMgmtService.updateAccountProvider(aProvider);
		aProvider = pspMgmtService.getAccountProvider(id);
		Assert.assertEquals("N", aProvider.getActive());
	}
	
	@Test
	public void updateTxnDetailsTest(){
		Long id = pspMgmtService.saveTransactionDetails(PspDaoDataPreparationUtility.prepTxnDetails("tarang19@tarangtech.com"));
		Assert.assertNotNull(id);
		TransactionDetails tDetails = pspMgmtService.getTransactionDetails(id);
		tDetails.setStatus("Completed");
		pspMgmtService.updateTransactionDetails(tDetails);
		tDetails = pspMgmtService.getTransactionDetails(id);
		Assert.assertEquals("Completed", tDetails.getStatus());
	}
	
	@Test
	public void getUserTest(){
		Long id = pspMgmtService.saveUserDetails(PspDaoDataPreparationUtility.prepareUserDetails(userName, aadhaarNumber, mobileNumber, status));
		Assert.assertNotNull(id);
		UserDetails user = pspMgmtService.getUser(userName, mobileNumber, aadhaarNumber);
		Assert.assertNotNull(user);
	}	
	
	@Test
	public void getUserByNameTest(){
		Long id = pspMgmtService.saveUserDetails(PspDaoDataPreparationUtility.prepareUserDetails("testUser", "12364587958", mobileNumber, status));
		Assert.assertNotNull(id);
		UserDetails user = pspMgmtService.getUserByName("testUser");
		Assert.assertEquals("testUser", user.getUserName());		
		//Assert.assertEquals("12364587958", user.getAadhaarNumber());
		//Assert.assertEquals("123654789", user.getMobileNumber());
		Assert.assertEquals("Registered", user.getStatus());			
	}
	
	@Test
	public void getBankSummaryTest(){
		saveAccProviderTest();
		List<BankSummary> bankList = pspMgmtService.getBankSummary();
		Assert.assertNotNull(bankList);
		Assert.assertEquals("HDFC", bankList.get(0).getBankName());
	}
	
	@Test
	public void getDeviceDetailsByUserIdTest() {
		Long id = pspMgmtService.saveUserDetails(PspDaoDataPreparationUtility.prepareUserDetails(userName, aadhaarNumber, mobileNumber, status));
		Assert.assertNotNull(id);
		UserDetails usDetails = pspMgmtService.getUserDetails(id);
		Assert.assertNotNull(usDetails);
		Long deviceId = pspMgmtService.saveDeviceDetails(PspDaoDataPreparationUtility.prepareDeviceDetails(usDetails));
		Assert.assertNotNull(deviceId);
		DeviceDetails details = pspMgmtService.getDeviceDetailsByUserId(id);
		Assert.assertNotNull(details);
	}
	
	@Test
	public void verifyUserTest() {
		Long id = pspMgmtService.saveUserDetails(PspDaoDataPreparationUtility.prepareUserDetails(userName, aadhaarNumber, mobileNumber, status));
		Assert.assertNotNull(id);
		Assert.assertNotNull(pspMgmtService.verifyUser(userName, password));
	}	
	
	@Test
	public void getLoginDetailsByNameTest(){	
		Long id = pspMgmtService.saveUserDetails(PspDaoDataPreparationUtility.prepareUserDetails(userName, aadhaarNumber, mobileNumber, status));
		Assert.assertNotNull(id);
		Long id1 = pspMgmtService.saveLoginDetails(PspDaoDataPreparationUtility.prepareLoginDetails(userName));
		Assert.assertNotNull(id1);
		UserDetails user=pspMgmtService.getNonDeletedUser(userName);
		Assert.assertEquals(userName, user.getUserName());
	}
	
	@Test
	public void getAccountProviderDetailsTest(){
		List<AccountProviderDetails> aProviders = pspMgmtService.getAccountProviderDetails();
		Assert.assertNotNull(aProviders);
	}
	
	@Test
	public void deleteAccountProviderListTest(){
		List<AccountProviderDetails> providerDetails = pspMgmtService.getAccountProviderDetails();
		pspMgmtService.deleteAccountProviderList(providerDetails);
		AccountProviderDetails providerDetail = pspMgmtService.getAccountProvider(200l);
		Assert.assertNull(providerDetail);
	}
	
	@Test
	public void getOtpReqDtailsTest(){
		Long id=pspMgmtService.saveOtpRequest(PspDaoDataPreparationUtility.prepareOtpDetails(mobileNumber, mmid));
		Assert.assertNotNull(id);
		OtpUpiRequest otp1=pspMgmtService.getOtpDetailsByMobile(mobileNumber, mmid);
		Assert.assertEquals(mobileNumber, otp1.getMobNum());
		Assert.assertEquals(mmid, otp1.getMmid());
		Assert.assertEquals(acNum, otp1.getAcNum());
		Assert.assertEquals(aadhaarNumber, otp1.getUidNum());
		OtpUpiRequest otp3=pspMgmtService.getOtpDetailsByAccount(acNum, ifsc, acType);
		Assert.assertEquals(mobileNumber, otp3.getMobNum());
		Assert.assertEquals(acNum, otp3.getAcNum());
		Assert.assertEquals(acType, otp3.getAcType());
		Assert.assertEquals(ifsc, otp3.getIfsc());
		Assert.assertEquals(aadhaarNumber, otp3.getUidNum());		
		OtpUpiRequest otp2=pspMgmtService.getOtpDetailsByAadhar(aadhaarNumber, iin);		
		Assert.assertEquals(mobileNumber, otp2.getMobNum());
		Assert.assertEquals(acNum, otp2.getAcNum());
		Assert.assertEquals(aadhaarNumber, otp2.getUidNum());
		Assert.assertEquals(iin, otp2.getIin());
	}
	
	@Test
	public void updateOtpReqDetailsTest(){
		Long id = pspMgmtService.saveOtpRequest(PspDaoDataPreparationUtility.prepareOtpDetails(mobileNum, mmid2));
		Assert.assertNotNull(id);
		OtpUpiRequest otp1 = pspMgmtService.getOtpDetailsByMobile(mobileNum, mmid2);
		otp1.setMobNum("0145777890");
		otp1.setMmid("XUAV174");
		otp1.setUidNum("15364587958");		
		otp1.setIfsc("HDFC1288100");		
		pspMgmtService.updateOtpDetails(otp1);
		otp1 = pspMgmtService.getOtpDetailsByAccount(acNum, "HDFC1288100", acType);
		Assert.assertEquals("0145777890", otp1.getMobNum());
		Assert.assertEquals("XUAV174", otp1.getMmid());		
	}
	
	//@Test
	public void isRegistiredUserTest(){
		Long id = pspMgmtService.saveUserDetails(PspDaoDataPreparationUtility.prepareUserDetails("Karan", "157888219036", "2424242424", status));
		Assert.assertNotNull(id);
		//User doesn't have mobile device
		DeviceDetails device = pspMgmtService.getDeviceDetailsByTerminalId("1A2BC3D4");
		Assert.assertNull(device);
		//User has mobile device	
		UserDetails user = PspDaoDataPreparationUtility.prepareUserDetails("Deba", "12345ABCD5678", "9908068773", "ACTIVE");
		id=pspMgmtService.saveUserDetails(user);
		Assert.assertNotNull(id);
		Long deviceId = pspMgmtService.saveDeviceDetails(PspDaoDataPreparationUtility.prepareDeviceDetails(user));
		Assert.assertNotNull(deviceId);
		device = pspMgmtService.getDeviceDetailsByTerminalId(terminalId);
		//Assert.assertEquals("9908068773", device.getMobile());
		Assert.assertEquals("658", device.getGeoCode());
		Assert.assertEquals("WhiteField", device.getLocation());		
	}
	
	@Test
	public void operateAccountProviderDetailsTest(){
		AccountProviderDetails acc = pspMgmtService.getAccountProvider(200l);
		List<AccountProviderDetails> updateDetails = new ArrayList<>();
		updateDetails.add(PspDaoDataPreparationUtility.prepareAccountProvider());
		updateDetails.add(acc);
		List<AccountProviderDetails> deleteList = new ArrayList<>();
		AccountProviderDetails deleteAcc = pspMgmtService.getAccountProvider(201l);
		deleteList.add(deleteAcc);
		pspMgmtService.operateAccountProviderDetails(updateDetails, deleteList);
		AccountProviderDetails details = pspMgmtService.getAccountProvider(201l);
		Assert.assertNull(details);		
	}
	
	/*@Test
	public void updateLoginExpiryTimeTest(){
		Long id = pspMgmtService.saveLoginDetails(PspDaoDataPreparationUtility.prepareLoginDetails("psp"));
		Assert.assertNotNull(id);
		LoginDetails loginDetails1 = pspMgmtService.getLoginDetailsByUserName("psp");		
		Assert.assertTrue(new Date().getTime() - loginDetails1.getLoginTime().getTime() < 1000);
		Long oldExpiaryTime = loginDetails1.getExpiryTime().getTime();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {			
			e.printStackTrace();
		}
		MobileAuditTrail mobAuditTrail = PspDaoDataPreparationUtility.prepareMobileAuditTrail();
		pspMgmtService.saveMobileAuditTrail(mobAuditTrail);
		LoginDetails loginDetails2 = pspMgmtService.getLoginDetails(id);
		Long newExpiaryTime = loginDetails2.getExpiryTime().getTime();
		Assert.assertTrue(newExpiaryTime - oldExpiaryTime > 2000);
		
	}*/
	
	@Test
	public void getMerchantByNameTest() {
		Assert.assertNotNull(pspMgmtService.getMerchantByName("Reliance"));
	}
	
	@Test
	public void getMerchantByIdTest() {
		Assert.assertNotNull(pspMgmtService.getMerchantById(1l));
	}
	
	@Test
	public void getTxnDetailsTest() {
		Assert.assertNotNull(pspMgmtService.saveTransactionDetails(PspDaoDataPreparationUtility.prepTxnDetails("tarang15@tarangtech.com")));
		List<TransactionDetails> txnList = pspMgmtService.getTransactionDetailsByDateAndVirtualAddr("", "", "tarang15@tarangtech.com");
		Assert.assertEquals(1, txnList.size());
	}
	
	@Test
	public void saveUpiMessagePacketTest() {
		UpiMessagePacket upiMessagePacket = new UpiMessagePacket();
		upiMessagePacket.setTxnId("TARANG1457690492923");
		upiMessagePacket.setMessagePacket(prepareMsgPkt());
		pspMgmtService.saveUpiMessagePacket(upiMessagePacket);
		
		upiMessagePacket = pspMgmtService.getUpiMessagePacketByTxnId("TARANG1457690492923");
		Assert.assertNotNull(upiMessagePacket);
	}
	
	@Test 
	public void saveNotificationDetailsTest() {
		UpiMessagePacket upiMessagePacket = new UpiMessagePacket();
		upiMessagePacket.setTxnId("TARANG1457690492923");
		upiMessagePacket.setMessagePacket(prepareMsgPkt());
		pspMgmtService.saveUpiMessagePacket(upiMessagePacket);
		
		upiMessagePacket = pspMgmtService.getUpiMessagePacketByTxnId("TARANG1457690492923");
		Assert.assertNotNull(upiMessagePacket);
		Assert.assertNotNull(pspMgmtService.saveNotificationDetails(PspDaoDataPreparationUtility.prepareNotificationDetails(upiMessagePacket)));
	}
	
	private String prepareMsgPkt() {
		String str = "<?xml version='1.0' encoding='UTF-8' standalone='no'?><ns2:ReqPay xmlns:ns2='http://npci.org/upi/schema/'><Head msgId='HENSVVR4QZS7X1UGPY7JGZZ444PL9T2C3QM' orgId='700025' ts='2016-03-11T15:31:32Z' ver='1.0'/><Meta><Tag name='PAYREQSTART' value='2016-03-11T15:31:32Z'/><Tag name='PAYREQEND' value='2016-03-11T15:31:32Z'/></Meta><Txn id='TARANG1457690492923' note='Req pay' refId='TARANG1457690492923' refUrl='https://121.244.157.134:443/' ts='2016-03-11T15:31:32Z' type='PAY'/><Payer addr='prasadj@tarang' code='' name='Prasad Jorige' seqNum='1' type='PERSON'><Info><Identity type='ACCOUNT' verifiedName='prasadj@tarang'/><Rating verifiedAddress='TRUE'/></Info><Device><Tag name='MOBILE' value='919741500833'/><Tag name='GEOCODE' value='12.9667,77.5667'/><Tag name='LOCATION' value='Sarjapur Road, Bangalore, KA, IN'/><Tag name='IP' value='169.254.74.34'/><Tag name='TYPE' value='Device'/><Tag name='ID' value='123456788'/><Tag name='OS' value='Android 4.4'/><Tag name='APP' value='CC 1.0 '/><Tag name='CAPABILITY' value='011001'/></Device><Ac addrType='ACCOUNT'><Detail name='IFSC' value='AACA5456646'/><Detail name='ACTYPE' value='SAVINGS'/><Detail name='ACNUM' value='1288000011118'/></Ac><Creds><Cred subType='SMS' type='OTP'><Data code='NPCI' ki='20150822'>1234</Data></Cred></Creds><Amount curr='INR' value='12'/></Payer><Payees><Payee addr='manas@tarang' code='' name='manas' seqNum='1' type='PERSON'><Amount curr='INR' value='12'/></Payee></Payees><Signature xmlns='http://www.w3.org/2000/09/xmldsig#'><SignedInfo><CanonicalizationMethod Algorithm='http://www.w3.org/TR/2001/REC-xml-c14n-20010315'/><SignatureMethod Algorithm='http://www.w3.org/2001/04/xmldsig-more#rsa-sha256'/><Reference URI=''><Transforms><Transform Algorithm='http://www.w3.org/2000/09/xmldsig#enveloped-signature'/></Transforms><DigestMethod Algorithm='http://www.w3.org/2001/04/xmlenc#sha256'/><DigestValue>GoXCjTn242zXoumRFa+Uh6bnAfc0mCuWeQ0u/ggFcbI=</DigestValue></Reference></SignedInfo><SignatureValue>nXcMnpRnAacIHVHHmvsu21/+E9cn+Tg7y2IuNhDLRKlMa7YO0Bj5XvhesTUD/jPRSilANYOhiIgj"
				+ "YwSHOJWADF91UMZ9CmhxN9iBPao8AYc1MWs0eUqtpMjT6HpK6Jq0h+fdJxupW2XJ35qVUJ3oPU1np/aXJ1q1yGNZuiD+absoZwdiRSEuZuqVjU5l52nwlmrO9fenhjuIKwW9kRvgI7v/6CmjszS6+zbS6WNTZdYZDPzC4sMSPB9IqGpdaQjbnCkZVYBayXXJbNsEgsqz4x+9xjhGroqWxtfL7rU8Wff1FvaD1Sh0jcCtR8umjDmziVXPF5QkZnSRZed1e7pHRQ==</SignatureValue><KeyInfo><KeyValue><RSAKeyValue><Modulus>37/4j/OKssWuINKwfg5IjaxlFIXxViyCf6/qQS7WSvczlAIoCMhsRCkCwsgTZx1lfQWZAfmylyocqfA4l8S8Y7vROkDSUHCL+xxJva0D301znUquA4yeFhfIM72NjFH/+o4z3E9/KqEd9PIerXm5XFMaFikpDskc76Zw0wz6YqsouBSm234QrLsZflwcLIpzbogBVbheUuKspFBK/yY+9Pr6t9MqvRdXed0BdHQfSR84e7iszaM7zwBqsDkyooLO9VVb2QEulhyTPYv8n4nIsFNe3zORVL+IKWGvzUFJx+Udt4kfNnl2JXHG0yOX8c5BeCDneMrowOtxQmQbBgxnHQ==</Modulus><Exponent>AQAB</Exponent></RSAKeyValue></KeyValue></KeyInfo></Signature></ns2:ReqPay>";
		return str;
	}
}