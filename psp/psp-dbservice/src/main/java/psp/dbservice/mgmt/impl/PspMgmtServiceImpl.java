package psp.dbservice.mgmt.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import psp.common.PropertyReader;
import psp.common.model.AccountSummary;
import psp.common.model.BankSummary;
import psp.dbservice.dao.PspDao;
import psp.dbservice.mgmt.PspMgmtService;
import psp.dbservice.model.AccountDetails;
import psp.dbservice.model.AccountProviderDetails;
import psp.dbservice.model.AccountReference;
import psp.dbservice.model.BeneficiaryDetails;
import psp.dbservice.model.DeviceDetails;
import psp.dbservice.model.KeyInfo;
import psp.dbservice.model.LoginDetails;
import psp.dbservice.model.Merchant;
import psp.dbservice.model.MobileAuditTrail;
import psp.dbservice.model.NotificationDetails;
import psp.dbservice.model.OtpUpiRequest;
import psp.dbservice.model.PspProvider;
import psp.dbservice.model.TransactionDetails;
import psp.dbservice.model.UpiMessagePacket;
import psp.dbservice.model.UpiRequest;
import psp.dbservice.model.UserDetails;

@Component("pspMgmtService")
@Transactional (rollbackFor = Exception.class)
public class PspMgmtServiceImpl implements PspMgmtService {

	@Autowired
	private PspDao pspDao;
	
	@Autowired
	private PropertyReader propertyReader;
	
	public PspMgmtServiceImpl(){		
	}
	
	@Override
	public Long saveUserDetails(UserDetails userDetails) {
		return pspDao.saveUserDetails(userDetails);
	}

	@Override
	public void updateUserDetails(UserDetails userDetails) {
		pspDao.updateUserDetails(userDetails);
	}

	@Override
	public UserDetails getUserDetails(Long id) {
		return pspDao.getUserDetails(id);
	}

	@Override
	public Long savePspProvider(PspProvider pspProvider) {
		return pspDao.savePspProvider(pspProvider);
	}
	
	@Override
	public void updatePspProvider(PspProvider PspProvider) {
         pspDao.updatePspProvider(PspProvider);
	}

	@Override
	public PspProvider getPspProvider(Long id) {
		return pspDao.getPspProvider(id);
	}

	@Override
	public void saveMobileAuditTrail(MobileAuditTrail mobileAuditTrail) {		
		 pspDao.saveMobileAuditTrail(mobileAuditTrail);	
		 LoginDetails login = pspDao.getLoginDetailsByTerminalIdAndUserName(mobileAuditTrail.getTerminalId(), mobileAuditTrail.getUserName());
		 if(null != login) {
			 Calendar cal = Calendar.getInstance();
			 cal.add(Calendar.MINUTE, propertyReader.getLoginExpiryDuration());
			 login.setExpiryTime(new Date(cal.getTimeInMillis()));		
			 pspDao.updateLoginDetails(login); 
		 }
	}

	@Override
	public void updateMobileAuditTrail(MobileAuditTrail mobileAuditTrail) {
		pspDao.updateMobileAuditTrail(mobileAuditTrail);
	}

	@Override
	public MobileAuditTrail getMobileAuditTrail(Long id) {
		return pspDao.getMobileAuditTrail(id);
	}
	
	@Override
	public Long saveDeviceDetails(DeviceDetails deviceDetails) {
		return pspDao.saveDeviceDetails(deviceDetails);
	}

	@Override
	public void updateDeviceDetails(DeviceDetails deviceDetails) {
		pspDao.updateDeviceDetails(deviceDetails);
	}

	@Override
	public DeviceDetails getDeviceDetails(Long id) {
		return pspDao.getDeviceDetails(id);
	}

	@Override
	public DeviceDetails getDeviceDetailsByUserId(Long userId) {
		return pspDao.getDeviceDetailsByUserId(userId);
	}

	@Override
	public Long saveAccountDetails(AccountDetails accountDetails) {
		return pspDao.saveAccountDetails(accountDetails);
	}

	@Override
	public void updateAccountDetails(AccountDetails accountDetails) {
		pspDao.updateAccountDetails(accountDetails);
	}

	@Override
	public AccountDetails getAccountDetails(Long id) {
		return pspDao.getAccountDetails(id);
	}

	@Override
	public Long saveAccountReference(AccountReference accountReference) {
		return pspDao.saveAccountReference(accountReference);
	}

	@Override
	public void updateAccountReference(AccountReference accountReference) {
		pspDao.updateAccountReference(accountReference);
	}

	@Override
	public AccountReference getAccountReference(Long id) {
		return pspDao.getAccountReference(id);
	}

	@Override
	public Long saveAccountProvider(AccountProviderDetails accProvider) {
		return pspDao.saveAccountProvider(accProvider);
	}

	@Override
	public void updateAccountProvider(AccountProviderDetails accProvider) {
		pspDao.saveOrupdateAccountProvider(accProvider);
		
	}

	@Override
	public AccountProviderDetails getAccountProvider(Long id) {
		return pspDao.getAccountProvider(id);
	}

	@Override
	public Long saveTransactionDetails(TransactionDetails txnDetails) {
		return pspDao.saveTransactionDetails(txnDetails);
	}

	@Override
	public void updateTransactionDetails(TransactionDetails txnDetails) {
		pspDao.updateTransactionDetails(txnDetails);
		
	}

	@Override
	public TransactionDetails getTransactionDetails(Long id) {
		return pspDao.getTransactionDetails(id);
	}

	@Override
	public List<BankSummary> getBankSummary() {
		return pspDao.getBankSummary();
	}

	@Override
	public UserDetails getUser(String userName, String mobileNumber, String aadhaarNo) {
		return pspDao.getUser(userName, mobileNumber, aadhaarNo);
	}

	@Override
	public void saveOrUpdateAccountProviderList(List<AccountProviderDetails> accProviders) {
		for(AccountProviderDetails acDetails : accProviders) {
			pspDao.saveOrupdateAccountProvider(acDetails);
		}
	}

	@Override
	public UserDetails getNonDeletedUser(String userName) {
		return pspDao.getNonDeletedUser(userName);
	}

	@Override
	public UserDetails getUserByName(String userName) {		
		return pspDao.getUserByName(userName);
	}

	@Override
	public void saveUserAndDeviceDetails(UserDetails userDetails, DeviceDetails deviceDetails) {
		pspDao.saveUserDetails(userDetails);
		pspDao.saveDeviceDetails(deviceDetails);
	}

	@Override
	public UserDetails verifyUser(String userName, String password) {
		return pspDao.verifyUser(userName, password);
	}

	@Override
	public Long saveLoginDetails(LoginDetails loginDetails) {
		return pspDao.saveLoginDetails(loginDetails);
	}

	@Override
	public void updateLoginDetails(LoginDetails loginDetails) {
		pspDao.updateLoginDetails(loginDetails);
	}

	@Override
	public LoginDetails getLoginDetails(Long id) {
		return pspDao.getLoginDetails(id);
	}

	@Override
	public LoginDetails getLoginDetailsByUserName(String userName) {
		return pspDao.getLoginDetailsByUserName(userName);
	}

	@Override
	public List<AccountProviderDetails> getAccountProviderDetails() {
		return pspDao.getAccountProviderDetails();
	}
	@Override
	public void deleteAccountProviderList(List<AccountProviderDetails> accProviders) {
		pspDao.deleteAccountProviderList(accProviders);
	}

	@Override
	public Long saveOtpRequest(OtpUpiRequest otpRequest) {		
		return pspDao.saveOtpRequest(otpRequest);
	}

	@Override
	public OtpUpiRequest getOtpDetailsByMobile(String mobNum, String mmid) {
		return pspDao.getOtpDetailsByMobile(mobNum, mmid);
	}

	@Override
	public OtpUpiRequest getOtpDetailsByAadhar(String aadharNum, String iin) {
		return pspDao.getOtpDetailsByAadhar(aadharNum, iin);
	}

	@Override
	public OtpUpiRequest getOtpDetailsByAccount(String actNum, String ifsc, String acType) {		
		return pspDao.getOtpDetailsByAccount(actNum, ifsc, acType);
	}

	@Override
	public void updateOtpDetails(OtpUpiRequest otp) {
		pspDao.updateOtpDetails(otp);
	}

	@Override
	public List<TransactionDetails> getTransactionDetailsByDateAndVirtualAddr(String fromDate,String toDate, String virtualAddr) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		Date fromDt = null;
		Date toDt = null;
		try {
			if (fromDate != null && !fromDate.equals("")) {
				fromDt = formatter.parse(fromDate);
			}
			if (toDate != null && !toDate.equals("")) {
				toDt = formatter.parse(toDate);
			}
			return pspDao.getTransactionDetailsByDateAndVirtualAddr(fromDt, toDt, virtualAddr);
		} catch (ParseException e) {
			return null;
		}
	}
	
	@Override
	public AccountDetails getAccountDetailsByUserDetailsId(Long id) {
		return pspDao.getAccountDetailsByUserId(id);
	}
	
	@Override
	public DeviceDetails getDeviceDetailsByTerminalId(String terminalId) {		
		return pspDao.getDeviceDetailsByTerminalId(terminalId);
	}

	@Override
	public AccountSummary getAccountSummaryByUserDetailsId(Long userId) {
		return pspDao.getAccountSummaryByUserId(userId);
	}

	@Override
	public void deleteLoginDetails(LoginDetails loginDetails) {
		pspDao.deleteLoginDetails(loginDetails);
	}
	
	@Override
	public void operateAccountProviderDetails(List<AccountProviderDetails> updateList, List<AccountProviderDetails> deleteList) {
		if (!updateList.isEmpty() && updateList.size() > 0) {
			saveOrUpdateAccountProviderList(updateList);
		}
		if (!deleteList.isEmpty() && deleteList.size() > 0) {
			pspDao.deleteAccountProviderList(deleteList);
		}
	}

	@Override
	public Long saveBeneficiaryDetails(BeneficiaryDetails bnfDetails) {		
		return pspDao.saveBeneficiaryDetails(bnfDetails);
	}

	@Override
	public BeneficiaryDetails getBeneficiaryDetails(Long id) {
		return pspDao.getBeneficiaryDetails(id);
	}
	
	@Override
	public BeneficiaryDetails getBeneficiaryDetails(String userName, String nickName) {
		return pspDao.getBeneficiaryDetails(userName, nickName);
	}
	
	@Override
	public BeneficiaryDetails getBeneficiaryDetailsByNickName(String nickName) {
		return pspDao.getBeneficiaryDetailsByNickName(nickName);
	}

	@Override
	public List<BeneficiaryDetails> getBeneficiaryDetailsByUserName(String userName) {		
		return pspDao.getBeneficiaryDetailsByUserName(userName);
	}
	@Override
	public OtpUpiRequest getOtpReqDetailsByTxnId(String txnId) {
		return pspDao.getOtpReqDetailsByTxnId(txnId);
	}

	@Override
	public void deleteOtpReqDetails(OtpUpiRequest otpRequestDetails) {
		pspDao.deleteOtpReqDetails(otpRequestDetails);
	}

	@Override
	public UpiRequest getUpiRequestByTxnId(String txnId) {
		return pspDao.getUpiRequestByTxnId(txnId);
	}

	@Override
	public void updateUpiRequest(UpiRequest upiRequest) {
		pspDao.updateUpiRequest(upiRequest);
	}

	@Override
	public void saveUpiRequest(UpiRequest upiRequest) {
		pspDao.saveUpiRequest(upiRequest);
	}

	@Override
	public void deleteUpiRequest(UpiRequest upiRequest) {
		pspDao.deleteUpiRequest(upiRequest);
	}

	@Override
	public void deleteUpiRequestByTxnId(String txnId) {
		pspDao.deleteUpiRequestByTxnId(txnId);
	}

	@Override
	public KeyInfo getKeyInfo() {
		return pspDao.getKeyInfo();
	}

	@Override
	public void operateKeyInfo(KeyInfo keyInfo) {
		pspDao.deleteKeyInfo();
		pspDao.saveKeyInfo(keyInfo);
	}

	@Override
	public List<Merchant> getMerchantList() {
		return pspDao.getMerchantList();
	}

	@Override
	public Merchant getMerchantByName(String name) {
		return pspDao.getMerchantByName(name);
	}

	@Override
	public Merchant getMerchantById(Long id) {
		return pspDao.getMerchantById(id);
	}

	@Override
	public AccountDetails getAccountDetailsByVirtualAddr(String addr) {
		return pspDao.getAccountDetailsByVirtualAddr(addr);
	}

	@Override
	public TransactionDetails getTransactionDetailsByTxnId(String txnId, String type) {
		return pspDao.getTransactionDetailsByTxnId(txnId, type);
	}

	@Override
	public UpiMessagePacket getUpiMessagePacketByTxnId(String txnId) {
		return pspDao.getUpiMessagePacketByTxnId(txnId);
	}

	@Override
	public void saveUpiMessagePacket(UpiMessagePacket upiMessagePacket) {
		pspDao.saveUpiMessagePacket(upiMessagePacket);
	}

	@Override
	public Long saveNotificationDetails(NotificationDetails notificationDetails) {
		return pspDao.saveNotificationDetails(notificationDetails);		
	}

	@Override
	public void deleteUpiMessagePacket(UpiMessagePacket upiMessagePacket) {
		pspDao.deleteUpiMessagePacket(upiMessagePacket);		
	}

	@Override
	public List<NotificationDetails> getInitiatedNotificationsByUserName(String userName) {
		return pspDao.getInitiatedNotificationsByUserName(userName);
	}

	@Override
	public void updateNotificationDetailsByNotificationId(String notificationId) {
		pspDao.updateNotificationDetailsByNotificationId(notificationId);		
	}

	@Override
	public NotificationDetails getNotificationDetailsByNotificationId(String notificationId) {
		return pspDao.getNotificationDetailsByNotificationId(notificationId);
	}

	@Override
	public void updateNotificationDetails(NotificationDetails notificationDetails) {
		pspDao.updateNotificationDetails(notificationDetails);		
	}

}