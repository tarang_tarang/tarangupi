package psp.dbservice.model;

import java.util.Date;

public class UserDetails implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	private String mobileNumber;
	
	private String email;
	
	private String aadhaarNumber;
	
	private String userName;
	
	private String password;
	
	private String firstName;
	
	private String lastName;
	
	private String middleName;
	
	private Date createdDate;
	
	private String status;
	
	private String rnsMpaId;
	
	public UserDetails() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAadhaarNumber() {
		return aadhaarNumber;
	}

	public void setAadhaarNumber(String aadhaarNumber) {
		this.aadhaarNumber = aadhaarNumber;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRnsMpaId() {
		return rnsMpaId;
	}

	public void setRnsMpaId(String rnsMpaId) {
		this.rnsMpaId = rnsMpaId;
	}	

}
