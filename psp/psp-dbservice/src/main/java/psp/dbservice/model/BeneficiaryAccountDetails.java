package psp.dbservice.model;

import java.io.Serializable;

public class BeneficiaryAccountDetails implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	private String referenceNumber;
	
	private String maskedNumber;
	
	private String ifsc;
	
	private String mmid;
	
	private String name;
	
	private String aeba;
	
	public BeneficiaryAccountDetails(){		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getMaskedNumber() {
		return maskedNumber;
	}

	public void setMaskedNumber(String maskedNumber) {
		this.maskedNumber = maskedNumber;
	}

	public String getIfsc() {
		return ifsc;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	public String getMmid() {
		return mmid;
	}

	public void setMmid(String mmid) {
		this.mmid = mmid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAeba() {
		return aeba;
	}

	public void setAeba(String aeba) {
		this.aeba = aeba;
	}
	
}