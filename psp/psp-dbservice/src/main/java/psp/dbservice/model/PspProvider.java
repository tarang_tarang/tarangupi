/**
 * 
 */
package psp.dbservice.model;

import java.util.Date;

/**
 * @author prasadj
 *
 */
public class PspProvider implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	
	private String name;
	
	private String codes;
	
	private String active;
	
	private String spocPhone;
	
	private String spocEmail;
	
	private String spocName;
	
	private String url;
	
	private Date lastModifedTs;
	
	public PspProvider(){
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCodes() {
		return codes;
	}

	public void setCodes(String codes) {
		this.codes = codes;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getSpocPhone() {
		return spocPhone;
	}

	public void setSpocPhone(String spocPhone) {
		this.spocPhone = spocPhone;
	}

	public String getSpocEmail() {
		return spocEmail;
	}

	public void setSpocEmail(String spocEmail) {
		this.spocEmail = spocEmail;
	}

	public String getSpocName() {
		return spocName;
	}

	public void setSpocName(String spocName) {
		this.spocName = spocName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Date getLastModifedTs() {
		return lastModifedTs;
	}

	public void setLastModifedTs(Date lastModifedTs) {
		this.lastModifedTs = lastModifedTs;
	}
	
}