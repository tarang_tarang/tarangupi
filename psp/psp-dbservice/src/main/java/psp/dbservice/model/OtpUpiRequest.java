package psp.dbservice.model;


@SuppressWarnings("serial")
public class OtpUpiRequest extends UpiRequest{

	private String mobNum;
	
	private String mmid;
	
	private String iin;
	
	private String uidNum;
	
	private String ifsc;
	
	private String acType;
	
	private String acNum;

	public OtpUpiRequest(){
	}
	
	public String getMobNum() {
		return mobNum;
	}

	public void setMobNum(String mobNum) {
		this.mobNum = mobNum;
	}

	public String getMmid() {
		return mmid;
	}

	public void setMmid(String mmid) {
		this.mmid = mmid;
	}

	public String getIin() {
		return iin;
	}

	public void setIin(String iin) {
		this.iin = iin;
	}

	public String getUidNum() {
		return uidNum;
	}

	public void setUidNum(String uidNum) {
		this.uidNum = uidNum;
	}

	public String getIfsc() {
		return ifsc;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	public String getAcType() {
		return acType;
	}

	public void setAcType(String acType) {
		this.acType = acType;
	}

	public String getAcNum() {
		return acNum;
	}

	public void setAcNum(String acNum) {
		this.acNum = acNum;
	}
	
}