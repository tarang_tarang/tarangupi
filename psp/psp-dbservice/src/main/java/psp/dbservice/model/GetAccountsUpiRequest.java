/**
 * 
 */
package psp.dbservice.model;

import java.util.Set;

/**
 * @author prasadj
 *
 */
@SuppressWarnings("serial")
public class GetAccountsUpiRequest extends UpiRequest {

	private Set<ReqAccountReference> accounts;

	public GetAccountsUpiRequest(){
	}
	
	public Set<ReqAccountReference> getAccounts() {
		return accounts;
	}

	public void setAccounts(Set<ReqAccountReference> accounts) {
		this.accounts = accounts;
	}

}