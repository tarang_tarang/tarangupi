/**
 * 
 */
package psp.dbservice.model;

import java.util.Date;

/**
 * @author prasadj
 *
 */
public class AccountProviderDetails implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	
	private String bankName;
	
	private String iin;
	
	private String active;
	
	private String spocPhone;
	
	private String spocEmail;
	
	private String spocName;
	
	private String url;
	
	private String prods;
	
	private Date lastModifedTs;
	
	public AccountProviderDetails(){
	}

	public AccountProviderDetails(Long id, String bankName){
		this.id = id;
		this.bankName = bankName;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getIin() {
		return iin;
	}

	public void setIin(String iin) {
		this.iin = iin;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getSpocPhone() {
		return spocPhone;
	}

	public void setSpocPhone(String spocPhone) {
		this.spocPhone = spocPhone;
	}

	public String getSpocEmail() {
		return spocEmail;
	}

	public void setSpocEmail(String spocEmail) {
		this.spocEmail = spocEmail;
	}

	public String getSpocName() {
		return spocName;
	}

	public void setSpocName(String spocName) {
		this.spocName = spocName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getProds() {
		return prods;
	}

	public void setProds(String prods) {
		this.prods = prods;
	}

	public Date getLastModifedTs() {
		return lastModifedTs;
	}

	public void setLastModifedTs(Date lastModifedTs) {
		this.lastModifedTs = lastModifedTs;
	}
	
}