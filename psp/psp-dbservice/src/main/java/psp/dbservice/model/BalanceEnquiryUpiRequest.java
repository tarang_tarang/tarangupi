package psp.dbservice.model;

public class BalanceEnquiryUpiRequest extends UpiRequest {

	private static final long serialVersionUID = 1L;
	
	private String balance;
	
	public BalanceEnquiryUpiRequest() {
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

}
