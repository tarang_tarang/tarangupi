package psp.dbservice.model;

public class BeneficiaryDetails implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	private String address;

	private String addressType;
	
	private String nickName;
	
	private String description;

	private String userName;
	
	private BeneficiaryAccountDetails accountDetails;
	
	public BeneficiaryDetails(){		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public BeneficiaryAccountDetails getAccountDetails() {
		return accountDetails;
	}

	public void setAccountDetails(BeneficiaryAccountDetails beneficiaryAccountDetails) {
		this.accountDetails = beneficiaryAccountDetails;
	}

}