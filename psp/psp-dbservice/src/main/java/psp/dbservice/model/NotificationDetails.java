package psp.dbservice.model;

import java.io.Serializable;
import java.util.Date;

public class NotificationDetails implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	private String notificationId;
	
	private String registrationId;
	
	private String userName;
	
	private String notificationtType;
	
	private String notificationMessage;
	
	private Date generatedTime;
	
	private String status;
	
	private UpiMessagePacket upiMessagePacket;
	
	public NotificationDetails() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(String notificationId) {
		this.notificationId = notificationId;
	}

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String username) {
		this.userName = username;
	}

	public String getNotificationtType() {
		return notificationtType;
	}

	public void setNotificationtType(String notificationtType) {
		this.notificationtType = notificationtType;
	}

	public String getNotificationMessage() {
		return notificationMessage;
	}

	public void setNotificationMessage(String notificationMessage) {
		this.notificationMessage = notificationMessage;
	}

	public Date getGeneratedTime() {
		return generatedTime;
	}

	public void setGeneratedTime(Date generatedTime) {
		this.generatedTime = generatedTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public UpiMessagePacket getUpiMessagePacket() {
		return upiMessagePacket;
	}

	public void setUpiMessagePacket(UpiMessagePacket upiMessagePacket) {
		this.upiMessagePacket = upiMessagePacket;
	}	

}
