/**
 * 
 */
package psp.dbservice.model;

import java.io.Serializable;

/**
 * @author manasp
 *
 */
public class KeyInfoReq implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Long id;
	
	private String code;
	
	private String ki;
	
	private String type;
	
	private String keyValue;
	
	public KeyInfoReq(){
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getKi() {
		return ki;
	}

	public void setKi(String ki) {
		this.ki = ki;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getKeyValue() {
		return keyValue;
	}

	public void setKeyValue(String keyValue) {
		this.keyValue = keyValue;
	}
	
}