package psp.dbservice.model;

import java.io.Serializable;

public class KeyInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	private String xmlPayload;
		
	public KeyInfo(){		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getXmlPayload() {
		return xmlPayload;
	}

	public void setXmlPayload(String xmlPayload) {
		this.xmlPayload = xmlPayload;
	}
	
}