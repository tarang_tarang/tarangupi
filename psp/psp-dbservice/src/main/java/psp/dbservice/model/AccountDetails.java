package psp.dbservice.model;

public class AccountDetails implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	private String virtualAddress;

	private String addressType;
	
	private String ifsc;
	
	private String accountType;
	
	private String accountNumber;
	
	private String mobileNumber;
	
	private String mmid;
	
	private Boolean isPinSet;
	
	private Boolean isAccDetailsVerified;
	
	private UserDetails userDetails;
	
	public AccountDetails() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVirtualAddress() {
		return virtualAddress;
	}

	public void setVirtualAddress(String virtualAddress) {
		this.virtualAddress = virtualAddress;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getIfsc() {
		return ifsc;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getMmid() {
		return mmid;
	}

	public void setMmid(String mmid) {
		this.mmid = mmid;
	}

	public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetails user) {
		this.userDetails = user;
	}

	public Boolean getIsPinSet() {
		return isPinSet;
	}

	public void setIsPinSet(Boolean isPinSet) {
		this.isPinSet = isPinSet;
	}

	public Boolean getIsAccDetailsVerified() {
		return isAccDetailsVerified;
	}

	public void setIsAccDetailsVerified(Boolean isAccDetailsVerified) {
		this.isAccDetailsVerified = isAccDetailsVerified;
	}

	

}
