package psp.dbservice.model;

import java.util.Set;

public class GetTokenUpiRequest extends UpiRequest {

	private static final long serialVersionUID = 1L;
	
	private String keyType;
	
	private Set<KeyInfoReq> keyInfoReqs;
	
	public GetTokenUpiRequest() {
	}

	public String getKeyType() {
		return keyType;
	}

	public void setKeyType(String keyType) {
		this.keyType = keyType;
	}

	public Set<KeyInfoReq> getKeyInfoReqs() {
		return keyInfoReqs;
	}

	public void setKeyInfoReqs(Set<KeyInfoReq> keyInfoReqs) {
		this.keyInfoReqs = keyInfoReqs;
	}

}