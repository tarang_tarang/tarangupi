/**
 * 
 */
package psp.dbservice.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author prasadj
 *
 */
public class UpiRequest implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	private String messageId;
	
	private String transactionId;
	
	private String status;
	
	private Date requestedTime;
	
	private Date responseTimeFromUpi;
	
	private String result;
	
	private String errMsg;
	
	public UpiRequest(){		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getRequestedTime() {
		return requestedTime;
	}

	public void setRequestedTime(Date requestedTime) {
		this.requestedTime = requestedTime;
	}

	public Date getResponseTimeFromUpi() {
		return responseTimeFromUpi;
	}

	public void setResponseTimeFromUpi(Date responseTimeFromUpi) {
		this.responseTimeFromUpi = responseTimeFromUpi;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

}