/**
 * 
 */
package psp.dbservice.model;

import java.util.Date;

/**
 * @author prasadj
 *
 */
public class TransactionDetails implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;

	private String fromAddr;
	
	private String toAddr;

	private String amount;

	private String status;

	private String txnType;
	
	private Boolean initiator;
	
	private String txnId;
	
	private String upiStatus;
	
	private String upiErrorCode;
	
	private String txnMessage;

	private Date txnInitiationTime;
	
	private Date txnConfirmationTime;
	
	public TransactionDetails(){
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFromAddr() {
		return fromAddr;
	}

	public void setFromAddr(String fromAddr) {
		this.fromAddr = fromAddr;
	}

	public String getToAddr() {
		return toAddr;
	}

	public void setToAddr(String toAddr) {
		this.toAddr = toAddr;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTxnType() {
		return txnType;
	}

	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}

	public Boolean getInitiator() {
		return initiator;
	}

	public void setInitiator(Boolean initiator) {
		this.initiator = initiator;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public String getUpiStatus() {
		return upiStatus;
	}

	public void setUpiStatus(String upiStatus) {
		this.upiStatus = upiStatus;
	}

	public String getUpiErrorCode() {
		return upiErrorCode;
	}

	public void setUpiErrorCode(String upiErrorCode) {
		this.upiErrorCode = upiErrorCode;
	}

	public String getTxnMessage() {
		return txnMessage;
	}

	public void setTxnMessage(String txnMessage) {
		this.txnMessage = txnMessage;
	}

	public Date getTxnInitiationTime() {
		return txnInitiationTime;
	}

	public void setTxnInitiationTime(Date txnInitiationTime) {
		this.txnInitiationTime = txnInitiationTime;
	}

	public Date getTxnConfirmationTime() {
		return txnConfirmationTime;
	}

	public void setTxnConfirmationTime(Date txnConfirmationTime) {
		this.txnConfirmationTime = txnConfirmationTime;
	}
	
}