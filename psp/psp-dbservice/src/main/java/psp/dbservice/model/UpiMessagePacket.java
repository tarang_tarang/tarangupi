package psp.dbservice.model;

import java.io.Serializable;

public class UpiMessagePacket implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	private String txnId;
	
	private String messagePacket;
		
	public UpiMessagePacket() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public String getMessagePacket() {
		return messagePacket;
	}

	public void setMessagePacket(String messagePacket) {
		this.messagePacket = messagePacket;
	}

}
