package psp.dbservice.model;

import java.util.Date;

public class LoginDetails implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	private Date loginTime;
	
	private Date expiryTime;
	
	private String userName;			
	
	private String terminalId;
	
	public LoginDetails() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public Date getExpiryTime() {
		return expiryTime;
	}

	public void setExpiryTime(Date expiryTime) {
		this.expiryTime = expiryTime;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}
	
}