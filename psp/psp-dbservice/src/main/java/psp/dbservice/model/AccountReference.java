package psp.dbservice.model;

public class AccountReference implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	private String referenceNumber;
	
	private String maskedNumber;
	
	private String ifsc;
	
	private String mmid;
	
	private String name;
	
	private String aeba;
	
	private String credentailType;
	
	private String credentailSubType;
	
	private String credentailDataType;
	
	private Integer credentailDataLength;
	
	private AccountDetails accountDetails;
	
	public AccountReference() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getMaskedNumber() {
		return maskedNumber;
	}

	public void setMaskedNumber(String maskedNumber) {
		this.maskedNumber = maskedNumber;
	}

	public String getIfsc() {
		return ifsc;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	public String getMmid() {
		return mmid;
	}

	public void setMmid(String mmid) {
		this.mmid = mmid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAeba() {
		return aeba;
	}

	public void setAeba(String aeba) {
		this.aeba = aeba;
	}

	public String getCredentailType() {
		return credentailType;
	}

	public void setCredentailType(String credentailType) {
		this.credentailType = credentailType;
	}

	public String getCredentailSubType() {
		return credentailSubType;
	}

	public void setCredentailSubType(String credentailSubType) {
		this.credentailSubType = credentailSubType;
	}

	public String getCredentailDataType() {
		return credentailDataType;
	}

	public void setCredentailDataType(String credentailDataType) {
		this.credentailDataType = credentailDataType;
	}

	public Integer getCredentailDataLength() {
		return credentailDataLength;
	}

	public void setCredentailDataLength(Integer credentailDataLength) {
		this.credentailDataLength = credentailDataLength;
	}

	public AccountDetails getAccountDetails() {
		return accountDetails;
	}

	public void setAccountDetails(AccountDetails accountDetails) {
		this.accountDetails = accountDetails;
	}	

}
