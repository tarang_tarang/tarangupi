package psp.dbservice.dao;

import java.util.Date;
import java.util.List;

import psp.common.model.AccountSummary;
import psp.common.model.BankSummary;
import psp.dbservice.model.AccountDetails;
import psp.dbservice.model.AccountProviderDetails;
import psp.dbservice.model.AccountReference;
import psp.dbservice.model.BeneficiaryDetails;
import psp.dbservice.model.DeviceDetails;
import psp.dbservice.model.KeyInfo;
import psp.dbservice.model.LoginDetails;
import psp.dbservice.model.Merchant;
import psp.dbservice.model.MobileAuditTrail;
import psp.dbservice.model.NotificationDetails;
import psp.dbservice.model.OtpUpiRequest;
import psp.dbservice.model.PspProvider;
import psp.dbservice.model.TransactionDetails;
import psp.dbservice.model.UpiMessagePacket;
import psp.dbservice.model.UpiRequest;
import psp.dbservice.model.UserDetails;

public interface PspDao {

	Long saveUserDetails(UserDetails userDetails);

	void updateUserDetails(UserDetails userDetails);

	UserDetails getUserDetails(Long id);
	
	Long savePspProvider(PspProvider pspProvider);
	
	void updatePspProvider(PspProvider pspProvider);

	PspProvider getPspProvider(Long id);
	
	void saveMobileAuditTrail(MobileAuditTrail mobileAuditTrail);
	
	void updateMobileAuditTrail(MobileAuditTrail mobileAuditTrail);
	
	MobileAuditTrail getMobileAuditTrail(Long id);
	
	Long saveDeviceDetails(DeviceDetails deviceDetails);

	void updateDeviceDetails(DeviceDetails deviceDetails);

	DeviceDetails getDeviceDetails(Long id);
	
	DeviceDetails getDeviceDetailsByUserId(Long userId);
	
	Long saveAccountDetails(AccountDetails accountDetails);

	void updateAccountDetails(AccountDetails accountDetails);

	AccountDetails getAccountDetails(Long id);
	
	Long saveAccountReference(AccountReference accountReference);

	void updateAccountReference(AccountReference accountReference);

	AccountReference getAccountReference(Long id);
	
	Long saveAccountProvider(AccountProviderDetails accProvider);

	void saveOrupdateAccountProvider(AccountProviderDetails accProvider);

	AccountProviderDetails getAccountProvider(Long id);
	
	Long saveTransactionDetails(TransactionDetails txnDetails);

	void updateTransactionDetails(TransactionDetails txnDetails);

	TransactionDetails getTransactionDetails(Long id);
	
	List<BankSummary> getBankSummary();
	
	UserDetails getUser(String userName, String mobileNumber, String aadhaarNo);
	
	UserDetails	getNonDeletedUser(String userName);
	
	UserDetails	getUserByName(String userName);
	
	UserDetails verifyUser(String userName, String password);
	
	Long saveLoginDetails(LoginDetails loginDetails);

	void updateLoginDetails(LoginDetails loginDetails);

	LoginDetails getLoginDetails(Long id);
	
	LoginDetails getLoginDetailsByUserName(String userName);	

	List<AccountProviderDetails> getAccountProviderDetails();
	
	void deleteAccountProviderList(List<AccountProviderDetails> accProviders);	

	Long saveOtpRequest(OtpUpiRequest otpRequest);	
	
	OtpUpiRequest getOtpDetailsByMobile(String mobNum, String mmid);
	
	OtpUpiRequest getOtpDetailsByAadhar(String aadharNum, String iin);
	
	OtpUpiRequest getOtpDetailsByAccount(String actNum, String ifsc, String acType);
	
	void updateOtpDetails(OtpUpiRequest otp);	

	List<TransactionDetails> getTransactionDetailsByDateAndVirtualAddr(Date fromDate, Date toDate, String virtualAddr);
	
	AccountDetails getAccountDetailsByUserId(Long id);

	DeviceDetails getDeviceDetailsByTerminalId(String terminalId);
	
	AccountSummary getAccountSummaryByUserId(Long id);
	
	void deleteLoginDetails(LoginDetails loginDetails);
	
	LoginDetails getLoginDetailsByTerminalIdAndUserName(String terminalId, String userName);
	
	Long saveBeneficiaryDetails(BeneficiaryDetails bnfDetails);
	
	BeneficiaryDetails getBeneficiaryDetails(Long id);
	
	List<BeneficiaryDetails> getBeneficiaryDetailsByUserName(String userName);
	
	BeneficiaryDetails getBeneficiaryDetails(String userName, String nickName);
	
	BeneficiaryDetails getBeneficiaryDetailsByNickName(String nickName);
	
	OtpUpiRequest getOtpReqDetailsByTxnId(String txnId);
	
	void deleteOtpReqDetails(OtpUpiRequest otpRequestDetails);
	
	UpiRequest getUpiRequestByTxnId(String txnId);
	
	void updateUpiRequest(UpiRequest upiRequest);
	
	void saveUpiRequest(UpiRequest upiRequest);
	
	void deleteUpiRequest(UpiRequest upiRequest);
	
	void deleteUpiRequestByTxnId(String txnId);
	
	KeyInfo getKeyInfo();
	
	void saveKeyInfo(KeyInfo keyInfo);
	
	void deleteKeyInfo();
	
	List<Merchant> getMerchantList();
	
	Merchant getMerchantByName(String name);
	
	Merchant getMerchantById(Long id);
	
	AccountDetails getAccountDetailsByVirtualAddr(String addr);
	
	TransactionDetails getTransactionDetailsByTxnId(String txnId, String type);
	
	UpiMessagePacket getUpiMessagePacketByTxnId(String txnId);
	
	void saveUpiMessagePacket(UpiMessagePacket upiMessagePacket);
	
	void deleteUpiMessagePacket(UpiMessagePacket upiMessagePacket);
	
	Long saveNotificationDetails(NotificationDetails notificationDetails);
	
	List<NotificationDetails> getInitiatedNotificationsByUserName(String userName);
	
	void updateNotificationDetailsByNotificationId(String notificationId);
	
	NotificationDetails getNotificationDetailsByNotificationId(String notificationId);
	
	void updateNotificationDetails(NotificationDetails notificationDetails);
	
}