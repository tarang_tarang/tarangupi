package psp.dbservice.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Component;

import psp.common.model.AccountSummary;
import psp.common.model.BankSummary;
import psp.constants.NotificationStatus;
import psp.constants.UserStatus;
import psp.dbservice.dao.PspDao;
import psp.dbservice.model.AccountDetails;
import psp.dbservice.model.AccountProviderDetails;
import psp.dbservice.model.AccountReference;
import psp.dbservice.model.BeneficiaryDetails;
import psp.dbservice.model.DeviceDetails;
import psp.dbservice.model.KeyInfo;
import psp.dbservice.model.LoginDetails;
import psp.dbservice.model.Merchant;
import psp.dbservice.model.MobileAuditTrail;
import psp.dbservice.model.NotificationDetails;
import psp.dbservice.model.OtpUpiRequest;
import psp.dbservice.model.PspProvider;
import psp.dbservice.model.TransactionDetails;
import psp.dbservice.model.UpiMessagePacket;
import psp.dbservice.model.UpiRequest;
import psp.dbservice.model.UserDetails;

@SuppressWarnings("unchecked")
@Component
public class PspDaoImpl implements PspDao {

	@Autowired
	private HibernateTemplate hibernateTemplate;	
	
	public PspDaoImpl() {
	}

	@Override
	public Long saveUserDetails(UserDetails userDetails) {
		return (Long) hibernateTemplate.save(userDetails);
	}

	@Override
	public void updateUserDetails(UserDetails userDetails) {
		hibernateTemplate.update(userDetails);
	}

	@Override
	public UserDetails getUserDetails(Long id) {
		return hibernateTemplate.get(UserDetails.class, id);
	}

	@Override
	public Long savePspProvider(PspProvider pspProvider) {
		return (Long) hibernateTemplate.save(pspProvider);
	}
	
	@Override
	public void updatePspProvider(PspProvider pspProvider) {
        hibernateTemplate.update(pspProvider);
	}

	@Override
	public PspProvider getPspProvider(Long id) {
		return hibernateTemplate.get(PspProvider.class, id);
	}
	
	@Override
	public void saveMobileAuditTrail(MobileAuditTrail mobileAuditTrail) {
		hibernateTemplate.save(mobileAuditTrail);
	}

	@Override
	public void updateMobileAuditTrail(MobileAuditTrail mobileAuditTrail) {
		hibernateTemplate.update(mobileAuditTrail);
	}

	@Override
	public MobileAuditTrail getMobileAuditTrail(Long id) {
		return hibernateTemplate.get(MobileAuditTrail.class, id);
	}
	
	@Override
	public Long saveDeviceDetails(DeviceDetails deviceDetails) {
		return (Long) hibernateTemplate.save(deviceDetails);
	}

	@Override
	public void updateDeviceDetails(DeviceDetails deviceDetails) {
		hibernateTemplate.update(deviceDetails);	
	}

	@Override
	public DeviceDetails getDeviceDetails(Long id) {
		return hibernateTemplate.get(DeviceDetails.class, id);
	}

	@Override
	public DeviceDetails getDeviceDetailsByUserId(Long userId) {
		List<DeviceDetails>  deviceDetails = (List<DeviceDetails>)hibernateTemplate.findByNamedQuery("getDeviceDetailsByUserId", userId);
		return ((deviceDetails != null && deviceDetails.size() > 0) ? deviceDetails.get(0) : null);
	}

	@Override
	public Long saveAccountDetails(AccountDetails accountDetails) {
		return (Long) hibernateTemplate.save(accountDetails);
	}

	@Override
	public void updateAccountDetails(AccountDetails accountDetails) {
		hibernateTemplate.update(accountDetails);
	}

	@Override
	public AccountDetails getAccountDetails(Long id) {
		return hibernateTemplate.get(AccountDetails.class, id);
	}

	@Override
	public Long saveAccountReference(AccountReference accountReference) {
		return (Long) hibernateTemplate.save(accountReference);
	}

	@Override
	public void updateAccountReference(AccountReference accountReference) {
		hibernateTemplate.update(accountReference);
	}

	@Override
	public AccountReference getAccountReference(Long id) {
		return hibernateTemplate.get(AccountReference.class, id);
	}

	@Override
	public Long saveAccountProvider(AccountProviderDetails accProvider) {
		return (Long) hibernateTemplate.save(accProvider);
	}

	@Override
	public void saveOrupdateAccountProvider(AccountProviderDetails accProvider) {
		hibernateTemplate.saveOrUpdate(accProvider);
	}

	@Override
	public AccountProviderDetails getAccountProvider(Long id) {
		return hibernateTemplate.get(AccountProviderDetails.class, id);
	}

	@Override
	public Long saveTransactionDetails(TransactionDetails txnDetails) {
		return (Long) hibernateTemplate.save(txnDetails);
	}

	@Override
	public void updateTransactionDetails(TransactionDetails txnDetails) {
		hibernateTemplate.update(txnDetails);
	}

	@Override
	public TransactionDetails getTransactionDetails(Long id) {
		return hibernateTemplate.get(TransactionDetails.class, id);
	}

	@Override
	public List<BankSummary> getBankSummary() {
		return (List<BankSummary>) hibernateTemplate.findByNamedQuery("getBankSummary");
	}

	@Override
	public UserDetails getUser(String userName, String mobileNumber,String aadhaarNo) {
		  List<UserDetails> users = (List<UserDetails>) hibernateTemplate.findByNamedQuery("getUser", userName, mobileNumber, aadhaarNo, UserStatus.DELETE.name());
		return (users != null && !users.isEmpty()) ? users.get(0) : null;
	}

	@Override
	public UserDetails getNonDeletedUser(String userName) {		
		List<UserDetails> users = (List<UserDetails>) hibernateTemplate.findByNamedQuery("getNonDeletedUser", userName, UserStatus.DELETE.name());
		return (users != null && !users.isEmpty()) ? users.get(0) : null;
	}

	@Override
	public UserDetails getUserByName(String userName) {
		List<UserDetails> users = (List<UserDetails>) hibernateTemplate.findByNamedQuery("getUserByName", userName); 
		return (users != null && !users.isEmpty()) ? users.get(0) : null;
	}

	@Override
	public UserDetails verifyUser(String userName, String password) {
		List<UserDetails> users = (List<UserDetails>) hibernateTemplate.findByNamedQuery("verifyUser", userName, password, UserStatus.DELETE.name());
		return (users != null && !users.isEmpty()) ? users.get(0) : null;
	}

	@Override
	public Long saveLoginDetails(LoginDetails loginDetails) {		
		return (Long) hibernateTemplate.save(loginDetails);
	}

	@Override
	public void updateLoginDetails(LoginDetails loginDetails) {		
		hibernateTemplate.update(loginDetails);
	}

	@Override
	public LoginDetails getLoginDetails(Long id) {
		return hibernateTemplate.get(LoginDetails.class, id);
	}

	@Override
	public LoginDetails getLoginDetailsByUserName(String userName) {
		List<LoginDetails> loggedinUser = (List<LoginDetails>) hibernateTemplate.findByNamedQuery("getLoginDetailsByUserName", userName);
		return (loggedinUser != null && !loggedinUser.isEmpty()) ? loggedinUser.get(0) : null;
	}
	
	@Override
	public List<AccountProviderDetails> getAccountProviderDetails() {
		return (List<AccountProviderDetails>) hibernateTemplate.findByNamedQuery("getAccountProviderDetails");
	}

	@Override
	public void deleteAccountProviderList(List<AccountProviderDetails> accProviders) {
		hibernateTemplate.deleteAll(accProviders);
	}
	
	@Override
	public Long saveOtpRequest(OtpUpiRequest otpDetails) {		
		return (Long) hibernateTemplate.save(otpDetails);
	}

	@Override
	public OtpUpiRequest getOtpDetailsByMobile(String mobNum, String mmid) {		
		List<OtpUpiRequest> otps = (List<OtpUpiRequest>)hibernateTemplate.findByNamedQuery("getOtpDetailsByMobile", mobNum, mmid);
		return (otps != null && !otps.isEmpty()) ? otps.get(0) : null;
	}

	@Override
	public OtpUpiRequest getOtpDetailsByAadhar(String aadharNum, String iin) {
		List<OtpUpiRequest> otps = (List<OtpUpiRequest>)hibernateTemplate.findByNamedQuery("getOtpDetailsByAadhar", aadharNum, iin);
		return (otps != null && !otps.isEmpty()) ? otps.get(0) : null;
	}

	@Override
	public OtpUpiRequest getOtpDetailsByAccount(String acNum, String ifsc, String actType) {
		List<OtpUpiRequest> otps = (List<OtpUpiRequest>)hibernateTemplate.findByNamedQuery("getOtpDetailsByAccount", acNum, ifsc, actType);
		return (otps != null && !otps.isEmpty()) ? otps.get(0) : null;
	}

	@Override
	public void updateOtpDetails(OtpUpiRequest otpDetails) {		
		hibernateTemplate.update(otpDetails);
	}

	@Override
	public List<TransactionDetails> getTransactionDetailsByDateAndVirtualAddr(Date fromDate, Date toDate, String virtualAddr) {		
		List<TransactionDetails> details =(List<TransactionDetails>) hibernateTemplate.findByNamedQuery("getTransactionDetailsByDateAndVirtualAddr", virtualAddr);
		return (details != null && !details.isEmpty()) ? details: null;
	}
	
	@Override
	public AccountDetails getAccountDetailsByUserId(Long id) {
		List<AccountDetails> accDetails = (List<AccountDetails>) hibernateTemplate.findByNamedQuery("getAccountDetailsByUserId", id);
		return (accDetails != null && !accDetails.isEmpty()) ? accDetails.get(0) : null;
	}

	@Override
	public DeviceDetails getDeviceDetailsByTerminalId(String terminalId) {
		List<DeviceDetails> deviceDetails = (List<DeviceDetails>) hibernateTemplate.findByNamedQuery("getDeviceDetailsByTerminalId", terminalId);
		return (deviceDetails != null && !deviceDetails.isEmpty()) ? deviceDetails.get(0) : null;
	}
	
	@Override
	public AccountSummary getAccountSummaryByUserId(Long id) {
		List<AccountSummary> accSummaryList = (List<AccountSummary>) hibernateTemplate.findByNamedQuery("getAccountSummaryByUserId", id);
		return (accSummaryList != null && !accSummaryList.isEmpty()) ? accSummaryList.get(0) : null;
	}

	@Override
	public void deleteLoginDetails(LoginDetails loginDetails) {
		hibernateTemplate.delete(loginDetails);
	}

	@Override
	public LoginDetails getLoginDetailsByTerminalIdAndUserName(String terminalId, String userName) {
		List<LoginDetails> loggedinUser = (List<LoginDetails>) hibernateTemplate.findByNamedQuery("getLoginDetailsByTerminalIdAndUserName", terminalId, userName);
		return (loggedinUser != null && !loggedinUser.isEmpty()) ? loggedinUser.get(0) : null;
	}

	@Override
	public Long saveBeneficiaryDetails(BeneficiaryDetails bnfDetails) {	
		return (Long) hibernateTemplate.save(bnfDetails);
	}
	
	@Override
	public BeneficiaryDetails getBeneficiaryDetails(Long id) {	
		return hibernateTemplate.get(BeneficiaryDetails.class, id);
	}

	@Override
	public List<BeneficiaryDetails> getBeneficiaryDetailsByUserName(String userName) {
		List<BeneficiaryDetails> bnfsDetails = (List<BeneficiaryDetails>) hibernateTemplate.findByNamedQuery("getBeneficiaryDetailsByUserName", userName);
		return bnfsDetails;
	}

	@Override
	public BeneficiaryDetails getBeneficiaryDetails(String userName, String nickName) {
		List<BeneficiaryDetails> bnfsDetails = (List<BeneficiaryDetails>) hibernateTemplate.findByNamedQuery("getBeneficiaryDetails", userName, nickName);
		return (bnfsDetails != null && !bnfsDetails.isEmpty()) ? bnfsDetails.get(0) : null;
	}

	@Override
	public BeneficiaryDetails getBeneficiaryDetailsByNickName(String nickName) {
		List<BeneficiaryDetails> bnfsDetails = (List<BeneficiaryDetails>) hibernateTemplate.findByNamedQuery("getBeneficiaryDetailsByNickName", nickName);
		return (bnfsDetails != null && !bnfsDetails.isEmpty()) ? bnfsDetails.get(0) : null;
	}

	@Override
	public OtpUpiRequest getOtpReqDetailsByTxnId(String txnId) {
		List<OtpUpiRequest> otpDetails = (List<OtpUpiRequest>) hibernateTemplate.findByNamedQuery("getOtpReqDetailsByTxnId", txnId);
		return (otpDetails != null && !otpDetails.isEmpty()) ? otpDetails.get(0) : null;
	}

	@Override
	public void deleteOtpReqDetails(OtpUpiRequest otpRequestDetails) {
		hibernateTemplate.delete(otpRequestDetails);
	}

	@Override
	public UpiRequest getUpiRequestByTxnId(String txnId) {
		List<UpiRequest> reqdetails = (List<UpiRequest>) hibernateTemplate.findByNamedQuery("getUpiRequestByTxnId", txnId);
		return (reqdetails != null && !reqdetails.isEmpty()) ? reqdetails.get(0) : null;
	}

	@Override
	public void updateUpiRequest(UpiRequest upiRequest) {
		hibernateTemplate.update(upiRequest);
	}

	@Override
	public void saveUpiRequest(UpiRequest upiRequest) {
		hibernateTemplate.persist(upiRequest);
		
	}

	@Override
	public void deleteUpiRequest(UpiRequest upiRequest) {
		hibernateTemplate.delete(upiRequest);
	}

	@Override
	public void deleteUpiRequestByTxnId(final String txnId) {
		hibernateTemplate.execute(new HibernateCallback<Integer>() {
			public Integer doInHibernate(Session session) {
				Query q = hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("delete from UpiRequest as t where t.transactionId = :id ");
				q.setParameter("id", txnId);
				return q.executeUpdate();
			}
		});		
	}
	
	@Override
	public KeyInfo getKeyInfo() {
		List<KeyInfo> keyInfos = (List<KeyInfo>) hibernateTemplate.findByNamedQuery("getKeyInfo");
		return (keyInfos != null && !keyInfos.isEmpty()) ? keyInfos.get(0) : null;
	}

	@Override
	public void saveKeyInfo(KeyInfo keyInfo) {
		hibernateTemplate.saveOrUpdate(keyInfo);
	}

	@Override
	public void deleteKeyInfo() {
		hibernateTemplate.execute(new HibernateCallback<Integer>() {
			public Integer doInHibernate(Session session) {
				Query q = hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("delete from KeyInfo ");
				return q.executeUpdate();
			}
		});		
	}

	@Override
	public List<Merchant> getMerchantList() {
		return  (List<Merchant>) hibernateTemplate.findByNamedQuery("getMerchantList");		
	}

	@Override
	public Merchant getMerchantByName(String name) {
		List<Merchant> merDetails =  (List<Merchant>) hibernateTemplate.findByNamedQuery("getMerchantByName", name);
		return (merDetails != null && !merDetails.isEmpty()) ? merDetails.get(0) : null;
	}

	@Override
	public Merchant getMerchantById(Long id) {
		List<Merchant> merDetails =  (List<Merchant>) hibernateTemplate.findByNamedQuery("getMerchantById", id);
		return (merDetails != null && !merDetails.isEmpty()) ? merDetails.get(0) : null;
	}

	@Override
	public AccountDetails getAccountDetailsByVirtualAddr(String addr) {
		List<AccountDetails> accDetails =  (List<AccountDetails>) hibernateTemplate.findByNamedQuery("getAccountDetailsByVirtualAddr", addr);
		return (accDetails != null && !accDetails.isEmpty()) ? accDetails.get(0) : null;
	}

	@Override
	public TransactionDetails getTransactionDetailsByTxnId(String txnId, String type) {
		List<TransactionDetails> txnDetails =  (List<TransactionDetails>) hibernateTemplate.findByNamedQuery("getTranscationDetailsByTxnId", txnId, type);
		return (txnDetails != null && !txnDetails.isEmpty()) ? txnDetails.get(0) : null;
	}

	@Override
	public UpiMessagePacket getUpiMessagePacketByTxnId(String txnId) {
		List<UpiMessagePacket> messagePackets =  (List<UpiMessagePacket>) hibernateTemplate.findByNamedQuery("getUpiMessagePacketByTxnId", txnId);
		return (messagePackets != null && !messagePackets.isEmpty()) ? messagePackets.get(0) : null;
	}

	@Override
	public void saveUpiMessagePacket(UpiMessagePacket upiMessagePacket) {
		hibernateTemplate.persist(upiMessagePacket);
	}

	@Override
	public void deleteUpiMessagePacket(UpiMessagePacket upiMessagePacket) {
		hibernateTemplate.delete(upiMessagePacket);
	}

	@Override
	public Long saveNotificationDetails(NotificationDetails notificationDetails) {
		return (Long) hibernateTemplate.save(notificationDetails);
	}

	@Override
	public List<NotificationDetails> getInitiatedNotificationsByUserName(String userName) {
		return (List<NotificationDetails>) hibernateTemplate.findByNamedQuery("getInitiatedNotificationsByUserName", 
				userName, NotificationStatus.INITIATED.name());
	}

	@Override
	public void updateNotificationDetailsByNotificationId(final String notificationId) {
		hibernateTemplate.execute(new HibernateCallback<Integer>() {
			public Integer doInHibernate(Session session) {
				Query q = hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("update NotificationDetails as t set t.status = :status where t.notificationId = :notificationId");
				q.setParameter("status", NotificationStatus.NOTIFIED.name());
				q.setParameter("notificationId", notificationId);
				return q.executeUpdate();
			}
		});	
	}

	@Override
	public NotificationDetails getNotificationDetailsByNotificationId(String notificationId) {
		List<NotificationDetails> notification =  (List<NotificationDetails>) hibernateTemplate.findByNamedQuery("getNotificationsByNotificationId", notificationId);
		return (notification != null && !notification.isEmpty()) ? notification.get(0) : null;
	}

	@Override
	public void updateNotificationDetails(NotificationDetails notificationDetails) {
		hibernateTemplate.update(notificationDetails);
	}
	
}