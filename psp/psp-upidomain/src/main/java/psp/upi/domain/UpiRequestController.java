/**
 * 
 */
package psp.upi.domain;

import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import psp.constants.ServiceRequestName;
import psp.upi.process.UpiProcessor;
import psp.util.JsonUtil;

/**
 * @author manasp
 *
 */
@RestController
@RequestMapping("/")
public class UpiRequestController implements ServiceRequestName{

	private static final Logger LOGGER = Logger.getLogger(UpiRequestController.class.getName());
	
	@Autowired
	private UpiProcessor upiProcessor;

	@RequestMapping(value = "/{api}/{version}/urn:txnid:{txnId}", method = RequestMethod.POST, headers = "Accept=application/xml", produces = "application/xml")
	@ResponseBody 
	public String processUpiRespRequest(HttpServletRequest request, @PathVariable String api, @PathVariable String version, @PathVariable String txnId) {
		LOGGER.info("started processUpiRespRequest");
		return upiProcessor.doProcess(api, JsonUtil.readJsonData(request));
	}
	
}