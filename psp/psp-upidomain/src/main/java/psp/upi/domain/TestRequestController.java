/**
 * 
 */
package psp.upi.domain;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import psp.upi.process.TestRequestProcessor;

/**
 * @author manasp
 *
 */
@RestController
@RequestMapping("/test")
public class TestRequestController {

	private static final Logger LOGGER = Logger.getLogger(TestRequestController.class.getName());
	
	@Autowired
	private TestRequestProcessor testRequestProcessor;
	
	@RequestMapping(value = "/{serviceName}", method = RequestMethod.POST, headers = "Accept=application/json", produces = "application/json")
	@ResponseBody 
	public Object registration(@PathVariable String serviceName) {
		LOGGER.info("registration request started from mobile to psp");
		return testRequestProcessor.requestByServiceName(serviceName);
	}
	
}